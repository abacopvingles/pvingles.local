<?php 
    defined('RUTA_BASE') or die(); $idgui = uniqid(); 
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<style type="text/css">
    .pnlvocabulario{
        border: 1px solid #88bdd8;
        padding: 1em 1ex;
        font-size: 1.2em;
        margin-bottom: 1ex;
        position: relative;
    }
    .pnlvocabulario .btnremovevoca{
        position: absolute;
        right: 0px;
        display: none;
    }
    .pnlvocabulario.edit .btnremovevoca{
        position: absolute;
        right: 0px;
        display: block;
        top:0;
    }
    .pnlvocabulario .texto
    {
       border:0px; 
    }
    .pnlvocabulario.edit .texto, .pnlvocabulario.edit .textoarea{
        min-width: 50px;
        height: 32px;
        cursor: pointer;
        display: inline-block;
        border:1px solid #ccc;
    }
    .pnlvocabulario.edit .textoarea{
         min-width: 100%;
        height: 32px;
    }
    .pnlvocabulario .icon1{
        color:#000;
        cursor: pointer;
    }
    .pnlvocabulario .icon2{
        color:#fac;
        cursor: pointer;
    }

</style>
<?php 
$vocAdmin=null;
$vocdocente=null;
$texto='';
if(!empty($this->datos))
    foreach ($this->datos as $voc){
        if($voc["orden"]=='0'){  $vocAdmin=$voc; }
        if($voc["idpersonal"]===$usuarioAct["dni"]&&$rolActivo!=1){  $vocdocente=$voc; }
        $texto.=$texto.$voc['texto'];
    }
    $texto=str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$texto);

if($rolActivo=='Alumno'){ ?>
<div class="container-fluid">
 <?php echo  $texto;  ?>    
</div>
<?php }else{ ?>
<div class="container-fluid" id="vocaall<?php echo $idgui; ?>">
    <div class="addallpnlvocabulario">
    <div class="row pnlvocabulario edit hidden" id="idclone<?php echo $idgui; ?>">       
        <div class="col-md-4 col-sm-6 col-xs-12">
            <img class="img-responsive img-thumbnails istooltip cargarimagenall" src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/levels/nofoto.jpg" width="170px" data-tipo="image" data-url="" alt="" title="<?php echo JrTexto::_('change image'); ?>">
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="form-group" idioma="EN">
                    <label for="vocabulary"><?php echo JrTexto::_('Vocabulary') ?>:</label>
                    <div class="text-center">
                    <span class="texto ingles"><?php echo JrTexto::_('text') ?> </span>                    
                        <i class="pronunciar icon1 fa fa-volume-up"></i>
                        <i class="pronunciar icon2 fa fa-volume-up deletrear"></i>
                    </div>                 
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="form-group" idioma="ES">
                    <label for="word" ><?php echo JrTexto::_('Vocabulary') ?> (ES):</label>                    
                    <div class="text-center">
                        <span class="texto spanish"><?php echo JrTexto::_('text') ?></span>
                        <i class="pronunciar icon1 fa fa-volume-up"></i>
                        <i class="pronunciar icon2 fa fa-volume-up deletrear"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                <div class="textoarea ingles"><?php echo JrTexto::_('text') ?> </div>
            </div>            
        </div>
        <span class="btnremovevoca btn btn-danger"><i class="fa fa-trash"></i></span>            
    </div>
    <?php echo  $texto;  ?>
    </div>
    <div class="nova col-md-12 col-sm-12 col-xs-12 text-center">
        <a href="#" class="btn btn-warning btnvocaclone"> <i class="fa fa-plus"></i> <?php echo JrTexto::_('Add word'); ?></a>
        <a href="#" class="btn btn-primary " id="btnsavevoca<?php echo $idgui; ?>"> <i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></a>
    </div>
    <div class="nova">
        <script type="text/javascript">
        $(document).ready(function(){
            $('.pnlvocabulario.edit span.texto').click(function(ev){
                ev.preventDefault();
                if($(this).find('input').length>0){
                    $(this).find('input').focus();
                    return;
                }
                var txt=$(this).text();
                var txtold='<?php echo JrTexto::_('text') ?>';
                var input='<input type="text" class="form-control" placeholder="'+txt+'" value="'+(txt!=txtold?txt:'')+'">';
                $(this).html(input);
                $(this).find('input').focus();
            });
            $('.pnlvocabulario.edit span.texto').on('focusout','input',function(ev){
                ev.preventDefault();                
                var txt=$(this).val();
                $(this).closest('.texto').html(txt);
            });

            $('.pnlvocabulario.edit .textoarea').click(function(ev){
                ev.preventDefault();
                if($(this).find('textarea').length>0){
                    $(this).find('textarea').focus();
                    return;
                }
                var txt=$(this).text();
                var txtold='<?php echo JrTexto::_('text') ?>';
                var input='<textarea class="form-control" placeholder="'+txt+'" style="width:100%">'+(txt!=txtold?txt:'')+'</textarea>';
                $(this).html(input);
                $(this).find('textarea').focus();
            });
            $('.pnlvocabulario.edit .textoarea').on('blur','textarea',function(ev){
                console.log('sadasd');
                ev.preventDefault();
                var txt=$(this).val();
                $(this).closest('.textoarea').html(txt);
            });

            $('.btnvocaclone').click(function(){
                var clone=$('#idclone<?php echo $idgui; ?>').clone(true);
                var idtmp=Date.now();
                clone.removeClass('hidden');
                clone.removeAttr('id');
                clone.find('img').addClass('image'+idtmp).attr('data-url','.image'+idtmp);               
                $('vocaall<?php echo $idgui; ?> .addallpnlvocabulario').append(clone);
                
            });            
            if($('.pnlvocabulario.edit').length==1) $('.btnvocaclone').trigger('click');

            $('#btnsavevoca<?php echo $idgui; ?>').click(function(){
                var texto=$('#vocaall<?php echo $idgui; ?>').clone(true);
                texto.find('.hidden').remove();
                texto.find('.nova').remove();
                var data = new Array();
                data.idNivel = $('#actividad-header').find('#txtNivel').val();
                data.idUnidad = $('#actividad-header').find('#txtunidad').val();
                data.idActividad = $('#actividad-header').find('#txtsesion').val();
                data.texto= texto.html();
                data.tool = 'V';
                var res = xajax__('', 'Tools', 'saveTools', data);
                if(res){console.log(res);}
            });

            $('.btnremovevoca').click(function(e){
                $(this).closest('.pnlvocabulario').remove();
            })

        });
        </script>
    </div>
</div>
<?php } ?>
<!--div class="container-fluid" id="voca-tool">
    <div class="row voca-container" id="admin-voca">
        <div class="to_clone" data-edit="pnl0<?php //echo $idgui; ?>">
            <h2 class="col-md-12 col-sm-12 col-xs-12 title-voca"><?php //echo JrTexto::_('check these out') ?>...</h2>
            <?php //if($rolActivo==1){?>
            <div class="col-md-12 col-sm-12 col-xs-12 nopreview">
                <div class="btn-toolbar" role="toolbar" >
                    <div class="btn btn-xs btn-group btnedithtml<?php //echo $idgui; ?>"><i class="fa fa-text-width"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnsavehtml<?php //echo $idgui; ?> disabled" data-id="<?php //echo @$vocAdmin["idtool"] ?>">
                        <i class="fa fa-save"></i>
                    </div>
                    <div class="btn btn-xs btn-group btnborrarhtml<?php //echo $idgui; ?>" data-id="<?php //echo @$vocAdmin["idtool"] ?>">
                        <i class="fa fa-trash"></i>
                    </div>
                </div>
            </div>
            <?php //} ?>
            <div class="col-md-12 col-sm-12 col-xs-12 voca-main">
                <?php //if($rolActivo==1){?>
                <textarea id="txtarea_pnl0<?php //echo $idgui; ?>" style="display: none">
                   <?php //echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$vocAdmin["texto"]); ?>
                </textarea>
                <?php //} ?>
                <div class="tbl-voca panel_pnl0<?php //echo $idgui; ?>">
                    <?php //echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$vocAdmin["texto"]); ?>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
	//var TXTAREA_INICIAL_VOCAB = $('.vocabulary-main textarea').clone();
	/*$('.btnborrarhtml<?php //echo $idgui; ?>').click(function(e){
        e.preventDefault();
        var obj=$(this).closest('.to_clone');
        var pnl=obj.data('edit');        
        var res = xajax__('', 'Tools', 'eliminar', $(this).data('id'));
        $(obj).find('#textarea_'+pnl).html('<?php echo $txtini;?>');
        $(obj).find('.tbl-voca.panel_'+pnl).html('<?php echo $txtini;?>');
        $(this).addClass('disabled').attr('data-id',0);
        $(this).siblings('[data-id]').addClass('disabled').attr('data-id',0);
        $('.other-add'+pnl).show();
        $('.other-add'+pnl+' .addMyvoca').show();
        $('.d_'+pnl).hide();
    });

	var iniciarVocabulario = function(idVocabul){
		$(idVocabul+'	.btn.btnedithtml<?php echo $idgui; ?>').trigger('click');
		$(idVocabul+'	.panel<?php echo $idgui; ?>').hide();
		$(idVocabul+'	.btnedithtml<?php echo $idgui; ?>').addClass('disabled');
	};

	var formatoVocabul = function(){
		$('#voca-tool table,#voca-tool th,#voca-tool tr,#voca-tool td').removeAttr('style');

        $('#voca-tool td').find('img').each(function(index, el) {
            $(this).addClass('table-img');
            $(this).parents('td').css('width', $(this).outerWidth() );
        });
	};

	
	var asignarDimensionModal = function($elem_tool){
		var height = $(document).outerHeight();
		var idModal = $elem_tool.parents('.modal').attr('id');
		$('#'+idModal+' #modalcontent').css({
			'max-height': (height-180)+'px',
			'overflow' : 'auto',
		})
	};

	var previewVocabulario = function(){
		$('#voca-tool .nopreview').hide();
		$('#admin-vocabulary .btnsavehtml<?php echo $idgui; ?>, #other-vocabulary .btnsavehtml<?php echo $idgui; ?>').trigger('click');
	};

	var guardarVocabulario = function( $textarea ,idpk,btn){
        var data = new Array();
        data.idNivel = $('#actividad-header').find('select#txtNivel').val();
        data.idUnidad = $('#actividad-header').find('select#txtunidad').val();
        data.idActividad = $('#actividad-header').find('select#txtsesion').val();
        data.texto= $textarea.val();
        data.tool = 'V';
        data.pkIdlink= idpk||0;


        var res = xajax__('', 'Tools', 'saveTools', data);
        if(res){
            $(btn).attr('data-id',res);
            $(btn).siblings('[data-id]').attr('data-id',res);
        }
    };

	var vertinymcevoc=function(id,txt=null){
		if(txt!=''&&txt!=undefined&&txt!=null){
			$(id).html(txt);
		}
		tinymce.init({
			relative_urls : false,
			convert_newlines_to_brs : true,
			menubar: false,
			statusbar: false,
			verify_html : false,
			content_css : URL_BASE+'/static/tema/css/bootstrap.min.css',
			selector: id,
			height: 400,
			plugins:["table chingoinput chingoimage chingoaudio chingovideo textcolor" ], 
			toolbar: 'undo redo | table | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor | chingoinput chingoimage chingoaudio chingovideo'
		});
	};

	 $('#voca-tool #admin-voca, #voca-tool #other-voca')
        .on('click', '.btnsavehtml<?php //echo $idgui; ?>', function(e){
            if($(this).hasClass('disabled')){
                return false;
            }
            $(this).addClass('disabled');
            var obj=$(this).closest('.to_clone');
            var pnl=obj.data('edit');
            var idContainer = ' #' + $(this).parents('.voca-container').attr('id');
            $(idContainer+' .btnedithtml<?php echo $idgui; ?>').removeClass('disabled');
            tinyMCE.triggerSave();
            var txt=$(idContainer+' #txtarea_'+pnl).val();
            $(idContainer+' .panel_'+pnl).show();
            $(idContainer+' .panel_'+pnl).html(txt);
            tinymce.remove(idContainer+' textarea');   
            $(idContainer+' textarea').hide();
            $(idContainer+' .btnborrarhtml<?php echo $idgui; ?>').removeClass('disabled');            
            btn=$(this);
            $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').addClass('disabled');
            formatoVocabul();
            guardarVocabulario( $(idContainer+' textarea'),$(this).data('id'),btn);
            
        })
        .on('click', '.btnedithtml<?php //echo $idgui; ?>', function(e){
            var idContainer = ' #' + $(this).parents('.voca-container').attr('id');
            if($(this).hasClass('disabled')){
                return false;
            }else{
                var obj=$(this).closest('.to_clone');
                var pnl=obj.data('edit');
                $(idContainer+' .panel_'+pnl).hide();
                $(idContainer+' textarea').show();
                $(idContainer+' .btnedithtml<?php echo $idgui; ?>').addClass('disabled');
                $(idContainer+' .btnsavehtml<?php echo $idgui; ?>').removeClass('disabled');
                vertinymcevoc('#'+$(idContainer+' textarea').attr('id'));
            }
        });

    $('#voca-tool #other-voca')
        .on('click', '.btn.addMyvoca', function(e){
            e.preventDefault();
            var objid=$(this).data('edit');
            $('.d_'+objid).show().removeClass('hidden');
            $('.other-remove'+objid+' .btn').removeClass('disabled');
            $(this).hide();           
        })
        .on('click', '.btn.removevoca', function(e){
            e.preventDefault();
            $('#other-voca').find('.cloned').remove();
            $('.btn.addMyvoca').removeClass('hidden');
        });

	$('.preview').click(function(e) {
		previewVocabulario();
	});

	$('.btnsaveActividad').click(function(e){
		previewVocabulario();
	});


	iniciarVocabulario('#admin-vocabulary');
	asignarDimensionModal( $('#voca-tool') );*/
</script-->
