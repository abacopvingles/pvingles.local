<?php defined('_BOOT_') or die(''); ?>
<?php $usuarioActivo=NegSesion::getUsuario(); 
$usuariodni=@$usuarioActivo["dni"];
$verion=!empty(_version_)?_version_:'1.0';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate" />
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/examenes/general.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $verion; ?>"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js?vs=<?php echo $verion; ?>"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
</head>
<body>
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="sidebar_right" /></jrdoc:incluir>
<header class="static">
    <div class="container">              
        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav menus-examenes">
                <li class="menu-home"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>" >
                       <?php echo JrTexto::_("Home");?>
                    </a> 
                </li>

                <li class="menu-home active"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>" >
                       <?php echo JrTexto::_("Assessments");?>
                    </a> 
                </li>

                <li class="menu-home"> 
                    <a class="hvr-buzz-out exa-menu" href="rubricas/seguridad.php?id=<?=$usuariodni?>" >
                       <?php echo JrTexto::_("Rubricas");?>
                    </a> 
                </li>

                <li class="menu-setting"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/ver/" >
                       <?php echo JrTexto::_("Settings");?>
                    </a> 
                </li>


                <li class="menu-question"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/preguntas/" >
                       <?php echo JrTexto::_("Questions");?>
                    </a> 
                </li>
                <li class="menu-preview"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/preview/" >
                       <?php echo JrTexto::_("Preview");?>
                    </a> 
                </li>
                <li class="menu-reports"> 
                    <a class="hvr-buzz-out exa-menu" href="<?php echo $documento->getUrlBase()?>/examenes/reportes/" >
                       <?php echo JrTexto::_("Reports");?>
                    </a> 
                </li> 
            </ul>
        </nav> 
    </div>
</header>
<div class="container">
    <jrdoc:incluir tipo="recurso" />    
</div>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<jrdoc:incluir tipo="docsJs" />
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="modaltitle"></h4>
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="font-size: 1.2em; font-weight: bolder; color: #006E84;">Getting ready...</span></div>
        </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
</body>
</html>