<?php defined('_BOOT_') or die(''); 
$verion=!empty(_version_)?_version_:'1.0';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate" />
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_flat.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/alumno/general.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/alumno/diplomado.css?vs=<?php echo $verion; ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <!--script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js?vs=<?php echo $verion; ?>"></script-->
    <!--script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/js/inicio.js?vs=?vs=<?php echo $verion; ?>"></script-->
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
    <script src="<?php echo $documento->getUrlStatic()?>/js/alumno/alumno.js?vs=<?php echo $verion; ?>"></script>
    
</head>
<body id="diplomado-tmpl">

<link href="<?php echo $documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<div id="loader-wrapper">
    <div id="loader" class="loader-diplomado">
        <img src="<?php echo $documento->getUrlTema()?>/alumno/img/bg1.png" class="img-responsive">
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {    
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);
});
</script>

<div class="container">
    <jrdoc:incluir tipo="mensaje" />
    <jrdoc:incluir tipo="recurso" />
</div>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js"></script>
<jrdoc:incluir tipo="docsJs" />
<jrdoc:incluir tipo="docsCss" />
</body>
</html>