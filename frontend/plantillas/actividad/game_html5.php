<?php
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta."ini_app.php");
    defined("RUTA_BASE") or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $nivel=empty($_GET["nivel"])?1:$_GET["nivel"];
    $unidad=empty($_GET["unidad"])?1:$_GET["unidad"];
    $actividad=empty($_GET["actividad"])?1:$_GET["actividad"];
    $idgame=empty($_GET["idgame"])?null:$_GET["idgame"];
    $idgui = uniqid();
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_sopaletras.css">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<div class="plantilla plantilla-recursoexterno" data-idgui="<?php echo $idgui ?>">    
    <div class="row nopreview" data-idrecurso="<?php echo $actividad."_".$idgui ?>">
        <div class="col-md-4 col-sm-4 addcontentdinamico" data-type="pdf" title="PDF" data-content="Agregar un archivo pdf">
            <i class="fa btn btn-block btn-primary  fa-file-pdf-o fa-2x "> <br><span>PDF</span></i> 
        </div>
        <div class="col-md-4 col-sm-4 addcontentdinamico"data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" >
            <i class="fa btn btn-block btn-primary fa-file-code-o fa-2x "> <br><span>Html</span></i> 
        </div>
        <div class="col-md-4 col-sm-4 addcontentdinamico"data-type="scorm1.2" title="SCORM 1.2" data-content="Agregar contenido SCORM 1.2 - seleccione un zip" >
            <i class="fa btn btn-block btn-primary fa-gg fa-2x "> <br><span>Scorm 2.1</span></i> 
        </div>
    </div>
    <iframe src="" id="iframecontgame" class="inpreview" style="margin:0; padding:0; height:100%; min-height: 400px; display:block; width:100%; border:none;"></iframe>   
    <a class="btn btn-primary save-ganecontentdinamico">
        <i class="fa fa-floppy-o"></i> 
        <?php echo JrTexto::_('Save').' '.JrTexto::_('content') ?>
    </a>
        
</div>
<script>

$(document).ready(function(){
   $('.addcontentdinamico').on('click',function(ev){
        ev.preventDefault();
        var _this=$(this);
        var type=_this.attr('data-type')||'';
        var _parent=_this.parent();
        var datalink=_parent.attr('data-link')||'';
        _this.attr('data-oldmedia',datalink)
        _this.siblings().children('i.btn-success').removeClass('btn-success').addClass('btn-primary'); 
        idgame=_parent.attr('data-idrecurso');
        switch (type){
            case 'pdf':
                __subirfileok({ file:_this.children('i'), typefile:'pdf',guardar:true,dirmedia:'gamerecurso/pdf/',nombre:idgame},
                    function(rs){
                        _parent.attr('data-type','pdf');
                        _parent.attr('data-link',rs.media); 
                        $('#iframecontgame').attr('src',rs.media); 
                        _this.children('i').removeClass('btn-primary').addClass('btn-success');                      
                        $(".btn.save-ganecontentdinamico").trigger('click');
                    });
                break;
            case 'html':
                __subirfileok({ file:_this.children('i'), typefile:'html',guardar:true,dirmedia:'gamerecurso/html/',nombre:idgame},
                    function(rs){
                        _parent.attr('data-type','html');
                        _parent.attr('data-link',rs.media);
                        $('#iframecontgame').attr('src',rs.media);
                        _this.children('i').removeClass('btn-primary').addClass('btn-success');
                        $(".btn.save-ganecontentdinamico").trigger('click');
                    });
                break;
            case 'scorm1.2':
                __subirfileok({ file:_this.children('i'), typefile:'scorm1.2',guardar:true,dirmedia:'gamerecurso/scorm/',nombre:idgame},
                    function(rs){
                        _parent.attr('data-type','scorm1.2');
                        _parent.attr('data-link',rs.media);
                        $('#iframecontgame').attr('src',rs.media);
                        _this.children('i').removeClass('btn-primary').addClass('btn-success');
                        $(".btn.save-ganecontentdinamico").trigger('click');
                    });
                break;
        }
   })
    $(".btn.save-ganecontentdinamico").click(function(e) {
        e.preventDefault();
        msjes={
            'attention' : '<?php echo JrTexto::_("Attention");?>',
            'guardado_correcto' : '<?php echo JrTexto::_("Game").' '.JrTexto::_("saved successfully");?>'
        }

        var data={
          idgame:$('#idgame').val(),
          nivel:'<?php echo $nivel;?>',
          unidad:'<?php echo $unidad;?>',
          actividad:'<?php echo $actividad;?>',
          texto:$('.games-main').html(),
          titulo:$('.titulo-game').val(),
          descripcion:$('.descripcion-game').val()
        }
        var idgame=saveGame(data,msjes);
        $('#idgame').val(idgame);
    });
});

</script>
