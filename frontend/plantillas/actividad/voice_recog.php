<?php
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta."ini_app.php");
    defined("RUTA_BASE") or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $nivel=empty($_GET["nivel"])?1:$_GET["nivel"];
    $unidad=empty($_GET["unidad"])?1:$_GET["unidad"];
    $actividad=empty($_GET["actividad"])?1:$_GET["actividad"];
    $idgui = uniqid();
?>
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Voice">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-pronunciacion" data-idgui="<?php echo $idgui ?>">
<input type="hidden" name="teacherRecording" id="teacherRecording" value="">
<input type="hidden" name="isSaved" id="isSaved" value="">
<input type="hidden" name="cleanedD" id="cleanedD" value="">

  <div class="row text-center">
    <h3 id="spoken"></h3>
  </div>
  <div class="row">
    <div style="margin-left:2em; margin-right:2em;" id="waveform"></div>
  </div>
  <div class="row text-center">
    <button id="replay" type="button" class="btn btn-primary" onclick="wavesurfer.playPause()">play/pause</button>
  </div>
  <div id="togglePrev" class="hidden">
      <hr>
      <div class="row">
        <div class="row text-center">
          <div class="hidden">
            <select id="grammars"></select>
            <div id="current-status">Loading page</div>
          </div>
          <span id="recording-indicator" style="border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; width: 20px; height: 20px; background: red;"></span>
          <div class="col-sm-6">
            <h5 id="youSaid"><b> You said: </b><p id="output"></p></h5>
          </div>
          <div class ="col-sm-6">
            <h5 class="left" id="score"></h5>
          </div>
        </div>
        <div class="row">
          <div style="margin-left:2em; margin-right:2em;" id="waveformAlumno"></div>
        </div>
        <div class="row text-center">
          <span id="recording-indicator" style="border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; width: 20px; height: 20px; background: red;"></span>

          <button  id="alumnoTry" type="button" class="btn btn-primary">start recording</button>
          <button onclick="wavesurferA.playPause()" type="button" class="btn btn-primary">Listen what you said</button>
        </div>
      </div>
    </div>
  </div>
  <div class="row nopreview">

        <div style="margin-right:2em; margin-left:2em;" class="row justify-content-center">
          <div class="col-sm-6 col-sm-offset-1">
            <label for="nuevaPalabra">Enter the word/phrase you spoke:
            </label>
            <input name="nuevaPalabra" class="form-control" id="nuevaPalabra">
          </div>
          <div class="col-sm-5" style="padding-top:1.7em;">
            <button onclick="agregarGramatica()" class="btn btn-primary center" > Press me when you finish writing</button>
          </div>
        </div>
        <div class="row justify-content-center">
          <button style="margin-left: 25vw; margin-top: 3em;margin-bottom: 1.7em;"
                  type="button" class="btn btn-primary center" id="toggle" onclick="recordTeacherVoice()">Start Recording <i class="fa fa-microphone" aria-hidden="true"></i>
          </button>
        </div>
    </div>
</div>
<script src="../../static/js/audio/wavesurfer/dist/wavesurfer.js"></script>
<script src="../../static/js/audio/pocketsphinx/webapp/js/audioRecorder.js"></script>
<script src="../../static/js/audio/pocketsphinx/webapp/js/callbackManager.js"></script>
<script src="../../static/js/audio/voice_recog.js" ></script>
