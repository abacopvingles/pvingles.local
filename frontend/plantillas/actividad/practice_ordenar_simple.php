<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_ordenar.css">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Ordenar_simple">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">

<div class="plantilla plantilla-ordenar ord_simple" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
    <div class="row nopreview" id="lista-elem-edit<?php echo $idgui; ?>">
        <div class="col-xs-12 elem-edit" id="elem_0">
            <div class="col-xs-12 zona-edicion">
                <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                    <div class="col-xs-6">
                        <a href="#" class="btn btn-default btn-block selectmedia" data-tipo="image" data-url=".img_0" data-tooltip="tooltip" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                        <img src="" class="hidden img_0">
                    </div>
                    <div class="col-xs-6">
                        <a href="#" class="btn btn-default btn-block selectmedia"  data-tipo="audio" data-url=".audio_0" data-tooltip="tooltip" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                        <audio src="" class="hidden audio_0"></audio>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-9 inputs-elem">
                    <input type="text" class="form-control txt-words" name="txtWord" placeholder="<?php echo ucfirst(JrTexto::_('write')).' '.JrTexto::_('something'); ?>">
                    <div class="row fix-part-elem">
                        <label class="col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('fix')).' '.JrTexto::_('a part'); ?>: </label>
                        <div class="col-xs-12 col-sm-9 list-elem-part">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row nopreview">
        <div class="col-xs-12 text-center botones-creacion">
            <a href="#" class="btn btn-success generar-elem" data-clone-from="#to_generate_ord_<?php echo $idgui; ?>" data-clone-to="#ejerc-ordenar<?php echo $idgui; ?>"><i class="fa fa-rocket"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate'); ?></a>
        </div>

        <div class="col-xs-12 text-center botones-editar" style="display: none;">
            <a href="#" class="btn btn-success back-edit"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
        </div>

        <div class="hidden col-xs-12 element" id="to_generate_ord_<?php echo $idgui; ?>">
            <div class="col-xs-12 col-sm-4 multimedia hide">
                <img src="" class="img-responsive hide">
                <a href="#" class="btn btn-orange btn-block hide" data-tooltip="tooltip"><i class="fa fa-play"></i></a>
            </div>
            <div class="col-xs-12 col-sm-8 texto">
                <div class="drag">
                </div>
                <div class="drop">
                </div>
            </div>
        </div>
    </div>
    <div class="row ejerc-ordenar" id="ejerc-ordenar<?php echo $idgui; ?>" style="display: none;">
        
    </div>
    
    <audio src="" class="hidden" id="audio-ordenar<?php echo $idgui; ?>" style="display: none;"></audio>
</div>
<script> $(document).ready(function(){ initOrdenarSimple('<?php echo $idgui; ?>', true); }); </script>