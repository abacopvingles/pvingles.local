<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(dirname(__FILE__))).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
$documento =&JrInstancia::getDocumento();
$met=empty($_GET["met"])?exit(0):$_GET["met"];
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Practice">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="nwrite">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<div class="plantilla plantilla-nlsw" id="tmp_nlsw<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>">
    <div class="row nopreview text-center">
        <a href="#" class="btn btn-primary btnadd_record" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-microphone-slash"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('record');?></a>
        <a href="#" class="btn btn-primary btnadd_audio" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-audio-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('audio');?></a>
        <a href="#" class="btn btn-primary btnadd_video" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-video-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('video'); ?></a>
        <a href="#" class="btn btn-primary btnadd_image" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-file-picture-o"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('image'); ?></a>
        <a href="#" class="btn btn-primary btnadd_texto" data-idgui="<?php echo $idgui; ?>">
            <i class="fa fa-text-width"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('text'); ?></a>
    </div>
    <div id="preguntasDocente<?php echo $idgui; ?>">
      
    </div>
    <hr>
    <div class="respuestaAlumno">
        <label class="showonpreview_nlsw hidden"><?php echo ucfirst(JrTexto::_("Student's answer")); ?> </label>
        <span class="agregarrespuesta nopreview">
            <a href="#" class="btn btn-xs btn-warning btnadd_textorespuesta" data-idgui="<?php echo $idgui; ?>">
                <i class="fa fa-sign-in"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('Answer'); ?></a>
        </span>
        <div class="msjRespuesta"></div>
        <textarea style="width: 100%" rows="5" id="textarea<?php echo $idgui; ?>" data-tipo="texto" class="grabarfile showonpreview_nlsw hidden txtrespuestaAlumno"></textarea>
        <!--div class="col-md-12 text-center showonpreview_nlsw hidden">
            <a href="#" class="btn btn-primary btnsend_data"><?php echo ucfirst(JrTexto::_('Send')); ?> <i class="fa fa-envelope-o"></i></a>
        </div-->
    </div>
</div>