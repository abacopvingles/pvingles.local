<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
?>
<!-- 
<script type="text/javascript" src="<?php echo URL_BASE;?>/static/media/diccionario/diccionario.json">
  
</script> -->

<!-- <div class="container" id="container">
    <div class="preloader">
        <p>Cargando</p>
    </div>
</div> -->
<style type="text/css">
  .loader-page {
    position: absolute;
    z-index: 25000;
    background: rgb(255, 255, 255);
    left: 0px;
    top: 0px;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    transition:all .3s ease;
  }
  .loader-page::before {
    content: "";
    position: absolute;
    border: 2px solid rgb(50, 150, 176);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176,0);
    border-top: 2px solid rgba(50, 150, 176,0);
    animation: rotarload 1s linear infinite;
    transform: rotate(0deg);
  }
  @keyframes rotarload {
      0%   {transform: rotate(0deg)}
      100% {transform: rotate(360deg)}
  }
  .loader-page::after {
    content: "";
    position: absolute;
    border: 2px solid rgba(50, 150, 176,.5);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176, 0);
    border-top: 2px solid rgba(50, 150, 176, 0);
    animation: rotarload 1s ease-out infinite;
    transform: rotate(0deg);
  }
</style>
<!-- <div class="loader-page"></div> -->
<div class="row" id="diccionario">
    <div class="col-md-12">
        <div class="panel" >      
            <div class="panel-body">
                <div class="col-xs-12">
                    <h1>
                        <?php echo JrTexto::_('Dictionary');?> 
                        <!--<small> 
                            <i class="fa fa-angle-double-right"></i> Subtext for header
                        </small>-->
                    </h1>
                </div>
                <form id="frmdiccionario">
                    <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                      <label><?php echo  ucfirst(JrTexto::_("Language"))?></label>
                        <div class="input-group">
                          <select class="form-control" id="lenguaje">
                            <option value="" selected disabled>---SELECIONAR---</option>
<!--                             <option value="1">English - Spanish</option>
                            <option value="2">Spanish - English</option> -->
                            <option value="3">English</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                      <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
                        <div class="input-group">
                          <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                          <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                        </div>
                    </div>
                    
                </form>
            </div>
            <div class="panel-body" id="resultado">
                
            </div>  
         </div>
     </div>
</div>
<script type="text/javascript">
$(window).on('load', function () {
    $(".loader-page").css({visibility:"hidden",opacity:"0"});
     
});
</script>
<script type="text/javascript">

$(document).ready(function() {
  var cont2 = 0;
    $('#lenguaje').on('change', function(){
      var valor = $('#lenguaje').val();
      if (valor==3) {
        if (cont2==0) {
            var script2=document.createElement('script');
            script2.type='text/javascript';
            script2.src='<?php echo URL_BASE;?>/static/media/diccionario/diccionarioen.json';
            $("body").append(script2);
            cont2++; 
          }
      }
    });

    $('.btnbuscar').on('click', function(){
        var valor = $('#lenguaje').val();
        if (valor==3) {

              var html='';
              var lenguaje = $('#lenguaje').val();
              var valor = $('#texto').val();
              var parametros = {"texto":valor,"lenguaje":lenguaje};
              for (var i = 0; i < diccionarioend.length; i++) {
                if (valor == diccionarioend[i].ingles) {
                  html += '<div class="row"><div class="col-xs-12 col-sm-12 col-md-12 " style="font-size:30px;text-transform: capitalize;color:#057879;background-color: white;text-align:center"><span class="texto">'+diccionarioend[i].ingles+'</span> <i class="fa fa-volume-up pronunciar"></i></div></div>';
                  var datos1 = diccionarioend[i].datos1;
                  console.log(datos1);
                  if (datos1!=undefined) {
                    for (var j = 0; j < datos1.length; j++) {
                      var tipo = datos1[j].tipo||'';
                      if (tipo!='') { html +='<div class="row" style="margin-left:0px;width: 100%;"><div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Type:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+tipo+'</div>';}
                      var significado = datos1[j].significado||'';
                      if (significado!='') { html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Meaning:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+significado+'</div>';}
                      var definicion = datos1[j].definicion||'';
                      if (definicion!='') { 
                        var definicion1 = datos1[j].definicion1||'';
                        if (definicion1!='') { 
                          var definicion2 = datos1[j].definicion2||'';
                          if (definicion2!='') {
                            var definicion10 = datos1[j].definicion10||'';
                            if (definicion10!='') {
                              var definicion3 = datos1[j].definicion3||'';
                              var definicion4 = datos1[j].definicion4||'';
                              var definicion5 = datos1[j].definicion5||'';
                              var definicion6 = datos1[j].definicion6||'';
                              var definicion7 = datos1[j].definicion7||'';
                              var definicion8 = datos1[j].definicion8||'';
                              var definicion9 = datos1[j].definicion9||'';

                              html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Definition:</div><div class="row" style="margin-left:0px;width: 100%;background-color:#cadbff"><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion1+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion2+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion3+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion4+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion5+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion6+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion7+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion8+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion9+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion10+'</div>';
                            }else{
                              html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Definition:</div><div class="row" style="margin-left:0px;width: 100%;background-color:#cadbff"><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion1+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion2+'</div>';
                            }
                          }else{
                            html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Definition:</div><div class="row" style="margin-left:0px;width: 100%;background-color:#cadbff"><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion+'</div><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion1+'</div>';
                          }   
                        }else{
                          html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Definition:</div><div class="row" style="margin-left:0px;width: 100%;background-color:#cadbff"><div class="col-xs-1 col-sm-1 col-md-1" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold"><i class="fa fa-space-shuttle"></i></div><div class="col-xs-11 col-sm-11 col-md-11" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+definicion+'</div>';
                        }
                      
                      }
                      var datos2 = datos1[j].ejemplo;   
                      if (datos2!=undefined) {
                        html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Examples:</div>';
                        for (var k = 0; k < datos2.length; k++) {
                          var ejemplo1 = datos2[k].ejemplo1||'';
                          if (ejemplo1!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo1+'</div>';} 
                          var ejemplo2 = datos2[k].ejemplo2||'';
                          if (ejemplo2!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo2+'</div>';}  
                          var ejemplo3 = datos2[k].ejemplo3||'';
                          if (ejemplo3!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo3+'</div>';}  
                          var ejemplo4 = datos2[k].ejemplo4||'';
                          if (ejemplo4!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo4+'</div>';}    
                          var ejemplo5 = datos2[k].ejemplo5||'';
                          if (ejemplo5!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo5+'</div>';}  
                          var ejemplo6 = datos2[k].ejemplo6||'';
                          if (ejemplo6!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo6+'</div>';} 
                          var ejemplo7 = datos2[k].ejemplo7||'';
                          if (ejemplo7!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo7+'</div>';} 
                          var ejemplo8 = datos2[k].ejemplo8||'';
                          if (ejemplo8!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo8+'</div>';} 
                          var ejemplo9 = datos2[k].ejemplo9||'';
                          if (ejemplo9!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo9+'</div>';} 
                          var ejemplo10 = datos2[k].ejemplo10||'';
                          if (ejemplo10!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i> '+ejemplo10+'</div>';} 
                        }

                      }     
                      html+='</div><br>'   

                    }
                  }
                  $('#resultado').html(html);
                  return;
                }
              } 

            } 
        //  $.ajax({
        //     url: './static/media/diccionario/prueba.php',
        //     type: 'post',
        //     data: parametros,
        //     success:function(resp){
        //         $('#resultado').html(resp); 
        //      }
        // });
        
     });

 });
 </script>