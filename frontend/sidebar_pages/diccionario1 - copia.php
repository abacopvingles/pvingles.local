<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
?>
<!-- 
<script type="text/javascript" src="<?php echo URL_BASE;?>/static/media/diccionario/diccionario.json">
  
</script> -->

<!-- <div class="container" id="container">
    <div class="preloader">
        <p>Cargando</p>
    </div>
</div> -->
<style type="text/css">
  .loader-page {
    position: absolute;
    z-index: 25000;
    background: rgb(255, 255, 255);
    left: 0px;
    top: 0px;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    transition:all .3s ease;
  }
  .loader-page::before {
    content: "";
    position: absolute;
    border: 2px solid rgb(50, 150, 176);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176,0);
    border-top: 2px solid rgba(50, 150, 176,0);
    animation: rotarload 1s linear infinite;
    transform: rotate(0deg);
  }
  @keyframes rotarload {
      0%   {transform: rotate(0deg)}
      100% {transform: rotate(360deg)}
  }
  .loader-page::after {
    content: "";
    position: absolute;
    border: 2px solid rgba(50, 150, 176,.5);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176, 0);
    border-top: 2px solid rgba(50, 150, 176, 0);
    animation: rotarload 1s ease-out infinite;
    transform: rotate(0deg);
  }
</style>
<!-- <div class="loader-page"></div> -->
<div class="row" id="diccionario">
    <div class="col-md-12">
        <div class="panel" >      
            <div class="panel-body">
                <div class="col-xs-12">
                    <h1>
                        <?php echo JrTexto::_('Dictionary');?> 
                        <!--<small> 
                            <i class="fa fa-angle-double-right"></i> Subtext for header
                        </small>-->
                    </h1>
                </div>
                <form id="frmdiccionario">
                    <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                      <label><?php echo  ucfirst(JrTexto::_("Language"))?></label>
                        <div class="input-group">
                          <select class="form-control" id="lenguaje">
                            <option value="" selected disabled>---SELECIONAR---</option>
                            <option value="1">English - Spanish</option>
                            <!-- <option value="2">Spanish - English</option> -->
                            <!-- <option value="3">English</option> -->
                          </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 form-group">
                      <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
                        <div class="input-group">
                          <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                          <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                        </div>
                    </div>
                    
                </form>
            </div>
            <div class="panel-body" id="resultado">
                
            </div>  
         </div>
     </div>
</div>
<script type="text/javascript">
$(window).on('load', function () {
    $(".loader-page").css({visibility:"hidden",opacity:"0"});
     
});
</script>
<script type="text/javascript">

$(document).ready(function() {
  var cont = 0;
  var cont1 = 0;
  var cont2 = 0;
    $('#lenguaje').on('change', function(){
      var valor = $('#lenguaje').val();
      if (valor==1) {
        if (cont ==0) {
          var script=document.createElement('script');
          script.type='text/javascript';
          script.src='<?php echo URL_BASE;?>/static/media/diccionario/diccionario.json';
          $("body").append(script); 
          cont++;
        }
      }
    });

    $('.btnbuscar').on('click', function(){
        var valor = $('#lenguaje').val();
        if (valor==1) {
          var html='';
          var lenguaje = $('#lenguaje').val();
          var valor = $('#texto').val();
          var parametros = {"texto":valor,"lenguaje":lenguaje};
          for (var i = 0; i < diccionarioen.length; i++) {
            if (valor == diccionarioen[i].ingles) {
              html += '<div class="row"><div class="col-xs-12 col-sm-12 col-md-12 " style="font-size:30px;text-transform: capitalize;color:#057879;background-color: white;text-align:center"><span class="texto">'+diccionarioen[i].ingles+'</span> <i class="fa fa-volume-up pronunciar"></i></div></div>';
              var datos1 = diccionarioen[i].datos1;
              if (datos1.length&&datos1!=undefined) {
               
                for (var j = 0; j < datos1.length; j++) {
                  var tipo = datos1[j].tipo||'';
                  if (tipo!='') { html +='<div class="row" style="margin-left:0px;width: 100%;"><div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;color:#057879"><i class="fa fa-angle-double-right"></i>Type:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+tipo+'</div>';}
                  var pronunciacion = datos1[j].pronunciacion||'';
                  if (pronunciacion!='') { html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Pronunciation:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+pronunciacion+'</div>';}
                  var datos2 = datos1[j].datos2;
                  if (datos2.length&&datos2!=undefined) {
                    for (var k = 0; k < datos2.length; k++) {
                      var espaniol = datos2[k].espaniol||'';
                      if(espaniol!=''){html +='<div class="row" style="background-color:#d9feff;margin-left:0px;width: 100%;"><div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;;color:#057879"><i class="fa fa-angle-double-right"></i>Translation:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'+espaniol+'</div>'}
                      var significado = datos2[k].significado||'';
                      if(significado!=''){html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;;color:#057879"><i class="fa fa-angle-double-right"></i>Meaning:</div><div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;">'+significado+'</div>';}
                      var datos3 = datos2[k].ejemplos;
                      if (datos3.length&&datos3!=undefined) {
                        html +='<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;background-color:#d9feff;color:#057879"><i class="fa fa-angle-double-right"></i>Examples:</div>';
                        for (var l = 0; l < datos3.length; l++) {
                          var ejemplo_1 = datos3[l].ejemplo_1||'';
                          if (ejemplo_1!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'+ejemplo_1+'</div>';} 
                          var ejemplo_2 = datos3[l].ejemplo_2||'';
                          if (ejemplo_2!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'+ejemplo_2+'</div>';}  
                          var ejemplo_3 = datos3[l].ejemplo_3||'';
                          if (ejemplo_3!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'+ejemplo_3+'</div>';}  
                          var ejemplo_4 = datos3[l].ejemplo_4||'';
                          if (ejemplo_4!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'+ejemplo_4+'</div>';}    
                          var ejemplo_5 = datos3[l].ejemplo_5||'';
                          if (ejemplo_5!='') {html+='<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'+ejemplo_5+'</div>';}     
                        }
                        html+='</div  ><br>';
                      }
                    }
                  }

                }
              }
              $('#resultado').html(html); 
              return;  
            }
          }
        }

        //  $.ajax({
        //     url: './static/media/diccionario/prueba.php',
        //     type: 'post',
        //     data: parametros,
        //     success:function(resp){
        //         $('#resultado').html(resp); 
        //      }
        // });
        
     });

 });
 </script>