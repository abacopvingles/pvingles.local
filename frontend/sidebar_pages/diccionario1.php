<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();
?>
<!-- 
<script type="text/javascript" src="<?php echo URL_BASE;?>/static/media/diccionario/diccionario.json">
  
</script> -->

<!-- <div class="container" id="container">
    <div class="preloader">
        <p>Cargando</p>
    </div>
</div> -->
<style type="text/css">
  .loader-page {
    position: absolute;
    z-index: 25000;
    background: rgb(255, 255, 255);
    left: 0px;
    top: 0px;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    transition:all .3s ease;
  }
  .loader-page::before {
    content: "";
    position: absolute;
    border: 2px solid rgb(50, 150, 176);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176,0);
    border-top: 2px solid rgba(50, 150, 176,0);
    animation: rotarload 1s linear infinite;
    transform: rotate(0deg);
  }
  @keyframes rotarload {
      0%   {transform: rotate(0deg)}
      100% {transform: rotate(360deg)}
  }
  .loader-page::after {
    content: "";
    position: absolute;
    border: 2px solid rgba(50, 150, 176,.5);
    width: 60px;
    height: 60px;
    border-radius: 50%;
    box-sizing: border-box;
    border-left: 2px solid rgba(50, 150, 176, 0);
    border-top: 2px solid rgba(50, 150, 176, 0);
    animation: rotarload 1s ease-out infinite;
    transform: rotate(0deg);
  }
</style>

<!-- <div class="loader-page"></div> -->
<div class="row" id="diccionario">
    <div class="col-md-12">
        <div class="panel" >      
            <div class="panel-body">
                <div class="col-xs-12">
                    <h1>
                        <?php echo JrTexto::_('Dictionary');?> 
                        <!--<small> 
                            <i class="fa fa-angle-double-right"></i> Subtext for header
                        </small>-->
                    </h1>
                </div>
            </div>
            <div class="panel-body" id="resultado">
                Aqui estará el diccionario offline solo English - Spanish
            </div>  
         </div>
     </div>
</div>
