<?php 
$SD=DIRECTORY_SEPARATOR;
$ruta=dirname(dirname(__FILE__)).$SD;
require_once($ruta.'ini_app.php');
defined('RUTA_BASE') or die();
$Sitio=new Sitio();
$Sitio->iniciar();

JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
$usuarioAct = NegSesion::getUsuario();
if($usuarioAct["rol"]=='Alumno'){
	$oNegAlumno=new NegAlumno;
	$datos=$oNegAlumno->buscar(array('dni'=>$usuarioAct["dni"]));
}
else{
	$oNegPersonal=new NegPersonal;
	$datos=$oNegPersonal->buscar(array('dni'=>$usuarioAct["dni"]));
}
$datos=$datos[0];
?>
<div class="profile">
	<h2 class="profile-header"><?php echo JrTexto::_('My profile');?></h2>
	<div class="profile-body">
		<form class="form-horizontal">

			<div class="form-group">
				<label for="txtDNI" class="col-sm-12"><?php echo JrTexto::_('DNI'); ?></label>
				<div class="col-sm-12" style="cursor:no-drop">
					<span class="info">
                        <span><?php echo $datos["dni"]; ?></span>
                    </span> 
				</div>
			</div>

            <div class="form-group">
                <label for="txtLastName" class="col-sm-12"><?php echo JrTexto::_('Paternal Surname') ?>:</label>
                <div class="col-sm-12 dataedit cape_paterno" data-tipo="text"  style="cursor:pointer" title="<?php echo JrTexto::_('edit') ?>">
                    <span class="info" data-campo="ape_paterno" >
                        <span><?php echo $datos["ape_paterno"]; ?></span> 
                        <i class="fa fa-edit"></i>
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label for="txtLastName" class="col-sm-12"><?php echo JrTexto::_('Maternal Surname') ?>:</label>
                <div class="col-sm-12 dataedit cape_materno" data-tipo="text"  style="cursor:pointer" title="<?php echo JrTexto::_('edit') ?>">
                    <span class="info" data-campo="ape_materno" >
                        <span><?php echo $datos["ape_materno"]; ?></span> 
                        <i class="fa fa-edit"></i>
                    </span>
                </div>
            </div>

			<div class="form-group">
				<label for="txtLastName" class="col-sm-12"><?php echo JrTexto::_('Name') ?>:</label>
				<div class="col-sm-12 dataedit cnombre" data-tipo="text"  style="cursor:pointer" title="<?php echo JrTexto::_('edit') ?>">
					<span class="info" data-campo="nombre" >
                        <span><?php echo $datos["nombre"]; ?></span> 
                        <i class="fa fa-edit"></i>
                    </span>
				</div>
			</div>

			<div class="form-group">
				<label for="txtLastName" class="col-sm-12"><?php echo JrTexto::_('Date of birth'); ?>:</label>
				<div class="col-sm-12 dataedit cfechanac" data-tipo="date" style="cursor:pointer" title="<?php echo JrTexto::_('edit') ?>">
                    <span class="info" data-campo="fechanac" >
                        <span class="data"><?php echo $datos["fechanac"]; ?> 
                        <i class="fa fa-edit"></i> </span>
                        <span class="date" id="vfechanac" style="display:none">
                            <input type='text' data-campo="fechanac" value="<?php echo $datos["fechanac"]; ?>" class='txt txtdate form-control' />
                        </span>
                    </span>               
                </div>
			</div>

			<div class="form-group">
				<label for="txtEmail" class="col-sm-12"><?php echo JrTexto::_('email'); ?>:</label>
				<div class="col-sm-12 dataedit cemail" data-tipo="text"  style="cursor:pointer" title="<?php echo JrTexto::_('edit') ?>">
					<span class="info" data-campo="email" >
                        <span><?php echo $datos["email"]; ?></span> 
                        <i class="fa fa-edit"></i>
                    </span>
				</div>
			</div>
<!--
			<div class="form-group">
				<label for="imgPhoto" class="col-sm-12"><?php echo JrTexto::_('photo'); ?></label>
				<div class="col-sm-12">
					<div class="input-group">
					    <input type="text" class="form-control" id="imgPhoto" name="imgPhoto">
					    <span class="input-group-btn">
					    	<button class="btn btn-default btn-blue" type="button"><?php echo JrTexto::_('select'); ?>...</button>
					    </span>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-primary center-block"><?php echo JrTexto::_('update'); ?></button>
				</div>
			</div>
-->
		</form>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('.txtdate').datetimepicker({
      format: 'YYYY-MM-DD',
      viewMode: 'years'
    });

    $('.dataedit').click(function(){
        if(!$(this).hasClass("enedit")){
            $(this).addClass('enedit');
            var tipo=$(this).attr('data-tipo');
            $info= $('.info',this);
            if(tipo=='text'){
                $info.html('<input type="text"  required="required" data-campo="'+$info.attr('data-campo')+'" class="txt form-control edittxt" value="'+$info.text().trim()+'" >');
                $('input',$info).focus();
            }else if(tipo='date'){
                $('span',$info).hide('fast');
                $('.date',$info).css('display', 'block');          
            }
        }
    });
    
    
    $('.dataedit').on('keypress','input.txt',function(e){  
          if(e.which==13)  $(this).trigger("blur");
    });

    $('.dataedit').on('change','.cbo',function(e){  
          $(this).trigger("blur");
    });
      
    $('.dataedit').on('blur','input.txt, .txtdate, .cbo',function(e){
         var campo=$(this).attr('data-campo');
         var value=$(this).val().trim();
         var data={'campo':campo,'value':value};
         padre=$('.c'+campo);
         $(padre).removeClass('enedit');
         tipo=$(padre).attr('data-tipo');
         var res = xajax__('', 'usuario', 'setUsuario',data);
         
         if(tipo=='text'){
          $(this).parent().html(value + ' <i class="fa fa-edit"></i>');
         }
         else if(tipo=='date'){
          pd=$(this).parent();
          $(pd).siblings('.data').show('fast').html(value+' <i class="fa fa-edit"></i>'); 
          $(pd).hide('fast');             
         }
         else if(tipo=='cbo'){
          cbo=$(this).parent();
          txt=$(":selected",this).text();
          $(cbo).siblings('.data').show('fast').html(txt+' <i class="fa fa-edit"></i>');
          $(cbo).hide('fast');
         }
    });



      $('#formRecupClave').bind({
          submit: function() {
            xajax__('', 'sesion', 'solicitarCambioClave', xajax.getFormValues('formRecupClave'));
         }
    });

});
</script>