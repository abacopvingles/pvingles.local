<link href="<?php echo $this->documento->getUrlTema()?>/css/preload.css" rel="stylesheet">
<div id="loader-wrapper">
    <div id="loader" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    	<img src="<?php echo $this->documento->getUrlStatic()?>/tema/css/images/cargando.gif" class="img-responsive" >
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<?php $fecha = date('Y-m-d H:i:s'); ?>
<script type="text/javascript">
$(document).ready(function() {
    if(userinfo = localStorage.getItem("userinfo")){
        var fecha = '<?php echo $fecha ?>';
        var jsonParseado = JSON.parse(userinfo);
        jsonParseado.fecha =fecha;
        var stringJSON = JSON.stringify(jsonParseado);
        localStorage.setItem("userinfo", stringJSON);
    }
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);    
});
</script>