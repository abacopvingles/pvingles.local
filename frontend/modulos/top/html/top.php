<style type="text/css">
    a.changerol , a.changerol:hover{
         font-size: 12px !important;
       /* color: #c13838 !important;
        background: transparent !important;
        text-decoration: none !important;*/
    }
    .custommenu { position:relative; z-index:98; }
    .licustom { width:initial; }
    @media(max-width:768px){
        .custommenu { position:absolute!important; top:0; right:0; z-index:1000; }
    }
    @media(max-width:375px) { .licustom { width:60px; } .licustom a { position:absolute!important; } }
</style>
<header>
    <div class="container-fluid">
        <div class="navbar-header navbar-static-top"> 
            <!-- <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button>  -->
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img style="position:relative; z-index:99; display:inline;" src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo_minedu.png" class="hvr-wobble-skew_ img-responsive" alt="logo">
            </a>
        </div>       
        <nav class="custommenu" id="bs-navbar"> 
            <ul class="nav navbar-nav navbar-right menutop1" style="float:right;"> 
                <li class="dropdown" id="account"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img  src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php 
                                $roles=$this->usuarioAct["roles"]; 
                                $idrol_=$this->usuarioAct["idrol"];
                                $verrol=$this->usuarioAct["rol"];
                                if(count($roles)>1){                                    
                                    $verrol.='<br><a href="'.$this->documento->getUrlBase().'/sesion/cambiarrol" class=" btn btn-red changerol">'.JrTexto::_('Change Role').'</a><br>';
                                }else{ $verrol=$this->usuarioAct["rol"];}
                                ?>
                                
                                 <?php echo $verrol; ?>
                                <a class="btn btn-blue btn-profile" href="<?php echo $this->documento->getUrlBase() ?>/personal/perfil">
                                    <?php echo ucfirst(JrTexto::_('my profile')); ?>
                                </a>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/personal/cambiarclave/?id=<?php echo $this->usuarioAct["dni"];?>" class="btn btn-default"><?php echo ucfirst(JrTexto::_('change password')); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/sesion/salir" class="btn btn-default"><?php echo ucfirst(JrTexto::_('log out')); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
                <li class="licustom"> 
                    <a  class="hvr-buzz-out" href="#" id="getting-started">
                        <i class="fa fa-th-large"></i>
                    </a> 
                </li> 
            </ul>
        </nav> 
    </div>
</header>
<script>
    $(document).ready(function(){
        $(window).resize(function(){
            if($(window).width() > 375){
                $('.licustom').show();
            } 
        });
        $('#account').on('click','.dropdown-toggle',function(){
            // console.log("asd",$(window).width() <= 375);
            if($(window).width() <= 375){
                console.log("asdasasd222");
                $('.licustom').hide();
            }
        });
        $('body').click(function(){
            if($(window).width() <= 375){
                if($('#account').hasClass("open")){
                    $('.licustom').show();
                }
            }
        });
        $(window).trigger("resize");
    });
</script>