<!-- modal que se clonara con el contenido-->
<div class="modal fade" id="modalclone" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close cerrarmodal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="modaltitle"></h4>
      </div>
      <div class="modal-body" id="modalcontent">
        <div style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>
      </div>
      <div class="modal-footer" id="modalfooter">        
        <button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
<?php
$user = NegSesion::getUsuario();
$id = $user['idHistorialSesion'];
$tipousuario = $user['idrol'] == '3' ? 'A' : 'P';
$idusuario = $user['idpersona'];
$lugar = 'P';
$fechaentrada = $user['fechaentrada'];
$fechasalida = date('Y-m-d H:i:s');
?>
<script type="text/javascript">

var id= <?php echo empty($id)?'false':$id; ?>;
var tipousuario= '<?php echo $tipousuario ?>';
var idusuario= <?php echo $idusuario ?>;
var lugar= '<?php echo $lugar ?>';
var fechaentrada= '<?php echo $fechaentrada ?>';
var fechasalida= '<?php echo $fechasalida ?>';

var editarHistoriaSesion_footer = function(){

$.ajax({
        url: _sysUrlBase_+'/historial_sesion/editar2',
        async: false,
        type: 'POST',
        dataType: 'json',
        data: {'idhistorialsesion': id, 'fechasalida': fechasalida,'tipousuario': tipousuario, 'idusuario': idusuario,'lugar':lugar ,'fechaentrada': fechaentrada},
    })
    .done(function(resp) {
        if(resp.code=='ok'){
            console.log(resp);
        } else {
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
};
$(document).ready(function(){
  $(window).on('beforeunload', function(){
    editarHistoriaSesion_footer();
  });
});

</script>