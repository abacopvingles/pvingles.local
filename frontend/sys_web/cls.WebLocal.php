<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-10-2018 
 * @copyright	Copyright (C) 25-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
class WebLocal extends JrWeb
{
	private $oNegLocal;
	private $oNegGeneral;
	private $oNegUgel;
	private $oNegDre;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLocal = new NegLocal;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegUgel = new NegUgel;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegDre = new NegMin_dre;
		$this->usuarioAct = NegSesion::getUsuario();
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Local', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["vacantes"])&&@$_REQUEST["vacantes"]!='')$filtros["vacantes"]=$_REQUEST["vacantes"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			else $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			$this->dress=$this->oNegDre->buscar();
			$this->iddress=-1;
			$this->idugel=-1;
			$this->ugeles=$this->oNegUgel->buscar();
			if(!empty($filtros["idugel"])){
				$this->idugel=$filtros["idugel"];
				if(!empty($this->ugeles))
				foreach($this->ugeles as $ugel){
					if($ugel["idugel"]==$this->idugel)
					$this->iddress=$ugel["iddepartamento"];
				}
			}else if(!empty($this->ugeles[0])){
				$this->iddress=$this->ugeles[0]["iddepartamento"];
				$this->ugeles=$this->oNegUgel->buscar(array('iddepartamento'=>$this->iddress));
			}
			$this->datos=$this->oNegLocal->buscar($filtros);
			$this->fktipo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipolocal'));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Local'), true);
			$this->esquema = 'local-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Local', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Local').' /'.JrTexto::_('New'), true);
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				$pais='PE';
				$this->idpais=$pais;
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));
				if(!empty($this->fkdepartamento[0])){
					$depa=$this->fkdepartamento[0]["departamento"];
					$this->iddepa=$depa;
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=$this->fkprovincia[0]["provincia"];
						$this->idpro=$pro;
						$this->fkdistrito=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>$pro,'distrito'=>'all'));
					}
				}
			}
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try {
			global $aplicacion;
			$this->esquema = 'importar/local-importar';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importardatos(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 		exit(0);
			}else{
				if(!empty($_POST["datosimportados"])){
					$dt=json_decode($_POST["datosimportados"]);
					if(!empty($dt)){
						$this->datos=array();
						foreach($dt as $v){
							$this->datos[]=$this->oNegLocal->importar(@$v->iddre,@$v->dre,@$v->idugel,@$v->ugel,@$v->idiiee,@$v->iiee,@$v->direccion,@$v->ubigeo,@$v->tipo,@$v->nvacantes,$this->usuarioAct["idproyecto"]);
						}
					}
				}
			}
			echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("Datos Importados")));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Local', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegLocal->idlocal = @$_GET['id'];
			$this->datos = $this->oNegLocal->dataLocal;
			$this->pk=@$_GET['id'];
			$this->fkpais=$this->oNegUbigeo->buscar(array('pais'=>'all'));
			if(!empty($this->fkpais[0])){
				$pais='PE';
				$this->idpais=$pais;
				$idubigeo=str_pad($this->datos["id_ubigeo"],6,'0');
				$this->fkdepartamento=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>'all'));
				if(!empty($this->fkdepartamento[0])){
					$depa=substr($idubigeo, 0,2);
					$this->iddepa=$depa;
					$this->fkprovincia=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>'all'));
					if(!empty($this->fkprovincia[0])){
						$pro=substr($idubigeo, 2,2);
						$this->idpro=$pro;
						$this->fkdistrito=$this->oNegUbigeo->buscar(array('pais'=>$pais,'departamento'=>$depa,'provincia'=>$pro,'distrito'=>'all'));
						$this->iddis=substr($idubigeo, -2);
					}
				}
			}
			$this->documento->setTitulo(JrTexto::_('Local').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->ugeles=$this->oNegUgel->buscar();           
			$this->fktipo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipolocal'));
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'local-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function infolocal(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;

			$filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];

			$resultado = $this->oNegLocal->infolocal($filtros);
			echo json_encode(array('code'=>'ok','data'=>$resultado));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Local', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["vacantes"])&&@$_REQUEST["vacantes"]!='')$filtros["vacantes"]=$_REQUEST["vacantes"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			else $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			$this->datos=$this->oNegLocal->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarLocal(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdlocal)) {
				$this->oNegLocal->idlocal = $frm['pkIdlocal'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
			$this->oNegLocal->nombre=@$txtNombre;
			$this->oNegLocal->direccion=@$txtDireccion;
			$this->oNegLocal->id_ubigeo=@$txtId_ubigeo;
			$this->oNegLocal->tipo=@$txtTipo;
			$this->oNegLocal->vacantes=@$txtVacantes;
			$this->oNegLocal->idugel=@$txtIdugel;
			$this->oNegLocal->idproyecto=!empty($txtIdproyecto)?$txtIdproyecto:$usuarioAct["idproyecto"];

            if($accion=='_add') {
            	$res=$this->oNegLocal->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Local')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegLocal->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Local')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveLocal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdlocal'])) {
					$this->oNegLocal->idlocal = $frm['pkIdlocal'];
				}
				
					$this->oNegLocal->nombre=@$frm["txtNombre"];
					$this->oNegLocal->direccion=@$frm["txtDireccion"];
					$this->oNegLocal->id_ubigeo=@$frm["txtId_ubigeo"];
					$this->oNegLocal->tipo=@$frm["txtTipo"];
					$this->oNegLocal->vacantes=@$frm["txtVacantes"];
					$this->oNegLocal->idugel=@$frm["txtIdugel"];
					$this->oNegLocal->idproyecto=!empty($frm["txtIdproyecto"])?$frm["txtIdproyecto"]:$this->usuarioAct["idproyecto"];
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegLocal->agregar();
					}else{
						$res=$this->oNegLocal->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegLocal->idlocal);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDLocal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLocal->__set('idlocal', $pk);
				$this->datos = $this->oNegLocal->dataLocal;
				$res=$this->oNegLocal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLocal->__set('idlocal', $pk);
				$res=$this->oNegLocal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegLocal->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}