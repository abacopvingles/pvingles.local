<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-06-2017 
 * @copyright	Copyright (C) 15-06-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
class WebTarea_asignacion_alumno extends JrWeb
{
	private $oNegTarea_asignacion_alumno;
	protected $oNegMatricula;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
		$this->oNegMatricula = new NegAcad_matricula;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_asignacion_alumno', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegTarea_asignacion_alumno->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_asignacion_alumno'), true);
			$this->esquema = 'tarea_asignacion_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_asignacion_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Tarea_asignacion_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Tarea_asignacion_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegTarea_asignacion_alumno->iddetalle = @$_GET['id'];
			$this->datos = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Tarea_asignacion_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegTarea_asignacion_alumno->iddetalle = @$_GET['id'];
			$this->datos = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_asignacion_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'tarea_asignacion_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tarea_asignacion_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveTarea_asignacion_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIddetalle'])) {
					$this->oNegTarea_asignacion_alumno->iddetalle = $frm['pkIddetalle'];
				}
				
				$this->oNegTarea_asignacion_alumno->__set('idtarea_asignacion',@$frm["txtIdtarea_asignacion"]);
					$this->oNegTarea_asignacion_alumno->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegTarea_asignacion_alumno->__set('mensajedevolucion',@$frm["txtMensajedevolucion"]);
					$this->oNegTarea_asignacion_alumno->__set('notapromedio',@$frm["txtNotapromedio"]);
					$this->oNegTarea_asignacion_alumno->__set('estado',@$frm["txtEstado"]);
					
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegTarea_asignacion_alumno->agregar();
					}else{
						$res=$this->oNegTarea_asignacion_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTarea_asignacion_alumno->iddetalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

		public function xGetxIDTarea_asignacion_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_asignacion_alumno->__set('iddetalle', $pk);
				$this->datos = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;
				$res=$this->oNegTarea_asignacion_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_asignacion_alumno->__set('iddetalle', $pk);
				$res=$this->oNegTarea_asignacion_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}  
}