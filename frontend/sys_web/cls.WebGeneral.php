<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-07-2017 
 * @copyright	Copyright (C) 18-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
class WebGeneral extends JrWeb
{
	private $oNegGeneral;
		public function __construct()
	{
		parent::__construct();		
		$this->oNegGeneral = new NegGeneral;
			}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('General', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			$filtros["idgeneral"]=!empty($_REQUEST["idgeneral"])?$_REQUEST["idgeneral"]:"";
			$filtros["codigo"]=!empty($_REQUEST["codigo"])?$_REQUEST["codigo"]:"";
			$filtros["nombre"]=!empty($_REQUEST["nombre"])?$_REQUEST["nombre"]:"";
			$filtros["tipo_tabla"]=!empty($_REQUEST["tipo_tabla"])?$_REQUEST["tipo_tabla"]:"";
			$filtros["mostrar"]=!empty($_REQUEST["mostrar"])?$_REQUEST["mostrar"]:"";
			
			$this->datos=$this->oNegGeneral->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('General'), true);
			$this->esquema = 'general-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('General', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('General').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('General', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegGeneral->idgeneral = @$_GET['id'];
			$this->datos = $this->oNegGeneral->dataGeneral;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('General').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'general-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('General', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["idgeneral"])&&@$_REQUEST["idgeneral"]!='')$filtros["idgeneral"]=$_REQUEST["idgeneral"];
			if(isset($_REQUEST["codigo"])&&@$_REQUEST["codigo"]!='')$filtros["codigo"]=$_REQUEST["codigo"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo_tabla"])&&@$_REQUEST["tipo_tabla"]!='')$filtros["tipo_tabla"]=$_REQUEST["tipo_tabla"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegGeneral->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarGeneral(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkIdgeneral)) {
				$this->oNegGeneral->idgeneral = $frm['pkIdgeneral'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegGeneral->codigo=@$txtCodigo;
				$this->oNegGeneral->nombre=@$txtNombre;
				$this->oNegGeneral->tipo_tabla=@$txtTipo_tabla;
				$this->oNegGeneral->mostrar=@$txtMostrar;
					
            if($accion=='_add') {
            	$res=$this->oNegGeneral->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegGeneral->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarPersonalizado(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario();
            @extract($_POST);

            $accion='_add';            
            if(!empty($idgeneral)) {
            	$haygeneral=$this->oNegGeneral->buscar(array('idgeneral'=>$idgeneral));
            	if(!empty($haygeneral[0])){
            		$this->oNegGeneral->idgeneral = $idgeneral;				
					$accion='_edit';	
            	}				
			}
            	
			$this->oNegGeneral->codigo=$codigo;
			$this->oNegGeneral->nombre=$nombre;
			$this->oNegGeneral->tipo_tabla=$tipo_tabla;
			$this->oNegGeneral->mostrar=$mostrar;
					
            if($accion=='_add') {
            	$res=$this->oNegGeneral->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegGeneral->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('General')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}




	
	// ========================== Funciones xajax ========================== //
	public function xSaveGeneral(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdgeneral'])) {
					$this->oNegGeneral->idgeneral = $frm['pkIdgeneral'];
				}
				
				$this->oNegGeneral->codigo=@$frm["txtCodigo"];
					$this->oNegGeneral->nombre=@$frm["txtNombre"];
					$this->oNegGeneral->tipo_tabla=@$frm["txtTipo_tabla"];
					$this->oNegGeneral->mostrar=@$frm["txtMostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegGeneral->agregar();
					}else{
									    $res=$this->oNegGeneral->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegGeneral->idgeneral);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDGeneral(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGeneral->__set('idgeneral', $pk);
				$this->datos = $this->oNegGeneral->dataGeneral;
				$res=$this->oNegGeneral->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGeneral->__set('idgeneral', $pk);
				$res=$this->oNegGeneral->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegGeneral->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}