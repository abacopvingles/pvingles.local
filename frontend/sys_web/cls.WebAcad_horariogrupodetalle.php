<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-01-2018 
 * @copyright	Copyright (C) 11-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
class WebAcad_horariogrupodetalle extends JrWeb
{
	private $oNegAcad_horariogrupodetalle;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_horariogrupodetalle = new NegAcad_horariogrupodetalle;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idhorario"])&&@$_REQUEST["idhorario"]!='')$filtros["idhorario"]=$_REQUEST["idhorario"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["fecha_finicio"])&&@$_REQUEST["fecha_finicio"]!='')$filtros["fecha_finicio"]=$_REQUEST["fecha_finicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["idhorariopadre"])&&@$_REQUEST["idhorariopadre"]!='')$filtros["idhorariopadre"]=$_REQUEST["idhorariopadre"];
			if(isset($_REQUEST["diasemana"])&&@$_REQUEST["diasemana"]!='')$filtros["diasemana"]=$_REQUEST["diasemana"];
			
			$this->datos=$this->oNegAcad_horariogrupodetalle->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_horariogrupodetalle'), true);
			$this->esquema = 'acad_horariogrupodetalle-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_horariogrupodetalle').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_horariogrupodetalle->idhorario = @$_GET['id'];
			$this->datos = $this->oNegAcad_horariogrupodetalle->dataAcad_horariogrupodetalle;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_horariogrupodetalle').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_horariogrupodetalle-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_horariogrupodetalle', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idhorario"])&&@$_REQUEST["idhorario"]!='')$filtros["idhorario"]=$_REQUEST["idhorario"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["fecha_finicio"])&&@$_REQUEST["fecha_finicio"]!='')$filtros["fecha_finicio"]=$_REQUEST["fecha_finicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["color"])&&@$_REQUEST["color"]!='')$filtros["color"]=$_REQUEST["color"];
			if(isset($_REQUEST["idhorariopadre"])&&@$_REQUEST["idhorariopadre"]!='')$filtros["idhorariopadre"]=$_REQUEST["idhorariopadre"];
			if(isset($_REQUEST["diasemana"])&&@$_REQUEST["diasemana"]!='')$filtros["diasemana"]=$_REQUEST["diasemana"];
						
			$this->datos=$this->oNegAcad_horariogrupodetalle->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarAcad_horariogrupodetalle(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            if(!empty($idhorario)){
				$this->oNegAcad_horariogrupodetalle->idhorario = $idhorario;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();   	
			$this->oNegAcad_horariogrupodetalle->idgrupoauladetalle=$idgrupoauladetalle;
			$this->oNegAcad_horariogrupodetalle->fecha_finicio=$fecha_finicio;
			$this->oNegAcad_horariogrupodetalle->fecha_final=$fecha_final;
			$this->oNegAcad_horariogrupodetalle->descripcion=$descripcion;
			$this->oNegAcad_horariogrupodetalle->color=$color;
			$this->oNegAcad_horariogrupodetalle->idhorariopadre=$idhorariopadre;
			$this->oNegAcad_horariogrupodetalle->diasemana=$diasemana;
					
            if($accion=='_add') {
            	$res=$this->oNegAcad_horariogrupodetalle->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Acad_horariogrupodetalle')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_horariogrupodetalle->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Acad_horariogrupodetalle')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function eliminarhorario(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);           
            if(!empty($idhorario)){
				$this->oNegAcad_horariogrupodetalle->idhorario = $idhorario;
				$this->oNegAcad_horariogrupodetalle->eliminar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Schedule')).' '.JrTexto::_('Delete Record'),'newid'=>$idhorario)); 
			}           
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function eliminarhorarioxpadre(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);           
            if(!empty($idhorariopadre)){
				$this->oNegAcad_horariogrupodetalle->eliminarxpadre($idhorariopadre);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Schedule')).' '.JrTexto::_('Delete Record'))); 
			}           
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_horariogrupodetalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdhorario'])) {
					$this->oNegAcad_horariogrupodetalle->idhorario = $frm['pkIdhorario'];
				}
				
				$this->oNegAcad_horariogrupodetalle->idgrupoauladetalle=@$frm["txtIdgrupoauladetalle"];
					$this->oNegAcad_horariogrupodetalle->fecha_finicio=@$frm["txtFecha_finicio"];
					$this->oNegAcad_horariogrupodetalle->fecha_final=@$frm["txtFecha_final"];
					$this->oNegAcad_horariogrupodetalle->descripcion=@$frm["txtDescripcion"];
					$this->oNegAcad_horariogrupodetalle->color=@$frm["txtColor"];
					$this->oNegAcad_horariogrupodetalle->idhorariopadre=@$frm["txtIdhorariopadre"];
					$this->oNegAcad_horariogrupodetalle->diasemana=@$frm["txtDiasemana"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_horariogrupodetalle->agregar();
					}else{
									    $res=$this->oNegAcad_horariogrupodetalle->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_horariogrupodetalle->idhorario);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_horariogrupodetalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_horariogrupodetalle->__set('idhorario', $pk);
				$this->datos = $this->oNegAcad_horariogrupodetalle->dataAcad_horariogrupodetalle;
				$res=$this->oNegAcad_horariogrupodetalle->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_horariogrupodetalle->__set('idhorario', $pk);
				$res=$this->oNegAcad_horariogrupodetalle->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_horariogrupodetalle->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}