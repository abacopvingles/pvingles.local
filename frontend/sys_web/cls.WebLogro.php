<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLogro', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
class WebLogro extends JrWeb
{
	private $oNegLogro;
	private $oNegAlumno_logro;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLogro = new NegLogro;
		$this->oNegAlumno_logro = new NegAlumno_logro;
        $this->usuarioAct = NegSesion::getUsuario();
				
	}

	public function defecto()
    {
        try {
            global $aplicacion;

			$this->logros= $this->oNegAlumno_logro->buscar(array('id_alumno'=>$this->usuarioAct['dni']));

            $this->documento->setTitulo(JrTexto::_('Achievements'), true);
            $this->idLogro = (isset($_GET['id']))?$_GET['id']:0;
            $this->breadcrumb = [
                [ 'texto'=> 'Achievements' ],
            ];
            $this->documento->plantilla = 'alumno/general';
            $this->esquema = 'alumno/logros';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id_logro"])&&@$_REQUEST["id_logro"]!='')$filtros["id_logro"]=$_REQUEST["id_logro"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			$this->datos=$this->oNegLogro->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Logro'), true);
			$this->esquema = 'academico/logro';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	
	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Logro').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegLogro->id_logro = @$_GET['id'];
			$this->datos = $this->oNegLogro->dataLogro;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Logro').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;				
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'academico/logro-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;			
			$filtros=array();
			if(isset($_REQUEST["id_logro"])&&@$_REQUEST["id_logro"]!='')$filtros["id_logro"]=$_REQUEST["id_logro"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["imagen"])&&@$_REQUEST["imagen"]!='')$filtros["imagen"]=$_REQUEST["imagen"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];	
			$this->datos=$this->oNegLogro->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarLogro(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';
            if(!empty($idlogro)) {
				$this->oNegLogro->id_logro = $idlogro;
				$accion='_edit';
			}
			$this->oNegLogro->titulo=@$titulo;
			$this->oNegLogro->descripcion=@$descripcion;
			$this->oNegLogro->tipo=@$tipo;
			$this->oNegLogro->imagen=@$imagen;
			$this->oNegLogro->puntaje=@$puntaje;
			$this->oNegLogro->estado=@$estado;					
            if($accion=='_add') {
            	$res=$this->oNegLogro->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Logro')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegLogro->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Logro')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function addcampo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST);
            if(!empty($idlogro)&&!empty($campo)&&isset($valor)){
				$this->oNegLogro->id_logro = $idlogro;
				$res=$this->oNegLogro->setCampo($idlogro,$campo,$valor);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Acad_cursodetalle')).' '.JrTexto::_('update successfully')));		
				exit(0);
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data not exists')));           	 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

    public function eliminar(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST);
            if(!empty($idlogro)){            	
				$this->oNegLogro->eliminar2($idlogro);				
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Certificate')).' '.JrTexto::_('update successfully')));		
				exit(0);
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data not exists')));           	 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

	
	// ========================== Funciones xajax ========================== //
	public function xSaveLogro(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_logro'])) {
					$this->oNegLogro->id_logro = $frm['pkId_logro'];
				}
				
				$this->oNegLogro->titulo=@$frm["txtTitulo"];
					$this->oNegLogro->descripcion=@$frm["txtDescripcion"];
					$this->oNegLogro->tipo=@$frm["txtTipo"];
					$this->oNegLogro->imagen=@$frm["txtImagen"];
					$this->oNegLogro->puntaje=@$frm["txtPuntaje"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegLogro->agregar();
					}else{
									    $res=$this->oNegLogro->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegLogro->id_logro);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDLogro(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLogro->__set('id_logro', $pk);
				$this->datos = $this->oNegLogro->dataLogro;
				$res=$this->oNegLogro->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLogro->__set('id_logro', $pk);
				$res=$this->oNegLogro->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegLogro->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}