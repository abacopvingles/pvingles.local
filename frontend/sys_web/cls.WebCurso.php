<?php
/**
 * @autor       Abel Chingo Tello, ACHT
 * @fecha       2017-07-31
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_ubicacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTest_asignacion', RUTA_BASE, 'sys_negocio');
#JrCargador::clase('sys_negocio::NegNota_detalle', RUTA_BASE, 'sys_negocio');
#JrCargador::clase('sys_negocio::NegAsistencia', RUTA_BASE, 'sys_negocio');

class WebCurso extends JrWeb
{
    private $oNegCurso;
    private $oNegMatricula;
    private $oNegCursoDetalle;
    private $oNegGrupoauladetalle;
    private $oNegAcad_horariogrupodetalle;
    private $oNegNiveles;
    private $oNegTarea;
    private $oNegTarea_asignacion;
    private $oNegTarea_asignacion_alumno;
    private $oNegBitacora_smartbook;
    private $oNegPersonal;
    private $oNegAlumno;
    private $oNegExamenes;
    private $oNegExamen_ubicacion;
    private $oNegExamen_ubicacion_alumno;
    private $oNegAulasvirtuales;
    private $oNegNotaquiz;
    private $MINIMO_PARA_PASAR=75;
    #private $oNegNota_detalle;
    #private $oNegAsistencia;

    public function __construct()
    {
        parent::__construct();
        $this->usuarioAct = NegSesion::getUsuario();

        $this->oNegCurso = new NegAcad_curso;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegCursoDetalle = new NegAcad_cursodetalle;
        $this->oNegGrupoauladetalle = new NegAcad_grupoauladetalle;
        $this->oNegAcad_horariogrupodetalle = new NegAcad_horariogrupodetalle;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegBitacora_smartbook = new NegBitacora_smartbook;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegAlumno = new NegAlumno;
        $this->oNegExamenes = new NegExamenes;
        $this->oNegExamen_ubicacion = new NegExamen_ubicacion;
        $this->oNegNotaquiz = new NegNotas_quiz;
        $this->oNegTarea = new NegTarea;
        $this->oNegTest_asignacion = new NegTest_asignacion;
        #$this->oNegAsistencia = new NegAsistencia;
        $this->avanzaen=65;
    }

    public function defecto()
    {
        return $this->curso();
    }

    public function curso()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            $this->idCurso = (isset($_REQUEST['id']))?$_REQUEST['id']:null;

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')) /*, 'link'=> '/curso/?id='.$this->idCurso*/ ],
                [ 'texto'=> $this->cursoActual['nombre']],
            ];
            $this->documento->setTitulo(JrTexto::_('Course')." ".@$this->cursoActual['nombre']);
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;
            if($rol==3||$rol==2){
                $this->Test=$this->oNegTest_asignacion->buscar(array('idcurso'=>$this->cursoActual["idcurso"]));
                //var_dump($this->Test);
                //$this->tieneRolAdmin =$rol==1?true:false;
                $temas=$this->oNegCursoDetalle->sesiones($this->cursoActual["idcurso"],0);
                //var_dump($temas);
                $temastmp=array();
                $i=0;
                foreach($temas as $tem){
                    $tem["progreso"]=0;
                    $tem["nactividad"]=0;
                    $tem["link"]='';
                    if($tem["tiporecurso"]=='E'){
                        $nq=$this->oNegNotaquiz->buscar(array('idrecurso'=>$tem["idrecurso"],'idproyecto'=>$this->usuarioAct["idproyecto"],'idalumno'=>$this->usuarioAct["idpersona"]));
                        if(!empty($nq[0])){
                            $ex=$nq[0];                            
                            $extipo=$ex["calificacion_en"];
                            $nota=$ex["nota"];
                            $caltotal=$ex["calificacion_total"];
                            $calmin=$ex["calificacion_min"]; //if($extipo=='P')
                            $progreso=($nota*$caltotal)/100; //elseif()
                            //$progreso=$progreso>=100?100:$progreso;     
                            $progreso=$progreso>=0?100:$progreso;
                            $progreso=number_format((float)$progreso, 1, '.', '');
                            $tem["progreso"]=$progreso;                        
                        }
                        $tem["link"]=$this->documento->getUrlBase()."/examenes/resolver/?idexamen=".$tem["idrecurso"];
                    }else{ 
                        //var_dump($tem,$nextbloqueado);                       
                        $tem=$this->getProgreso2($tem);                        
                    }                                       
                    $temastmp[]=$tem;
                    $i++;
                }
                $this->newsylabus=$temastmp;
                $this->documento->script('curso', '/js/alumno/');
                $this->documento->script('slick.min', '/libs/sliders/slick/');
                $this->documento->stylesheet('slick', '/libs/sliders/slick/');
                $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
                $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
                $this->documento->script('jquery-confirm.min', '/libs/alert/');
                $this->documento->plantilla = 'alumno/curso';
                $this->esquema = 'alumno/curso';
            }else{
                return $aplicacion->redir();
            }
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function miprogreso()
    {
        try {
            global $aplicacion;
            $this->documento->script('slick.min', '/libs/sliders/slick/');
                $this->documento->stylesheet('slick', '/libs/sliders/slick/');
                $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
                $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
                $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;
            $miscursos=array();
            if($rol==3){ // como alumno
                $miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            }else if($rol==2){ // como docente
                $miscursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                //$miscursos=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"])); 
            }else{ // cualquier otro rol
                return $aplicacion->error(JrTexto::_('access denegate'));
            }
            $haycursos=array();
            $curidsel=0;
            $curnombre='';
            $this->cursoSel=array();
            if(!empty($miscursos)){
                foreach($miscursos as $k => $cur){
                    if(empty($haycursos[$cur["idcurso"]])){
                        $haycursos[$cur["idcurso"]]=$cur;
                        $curidsel=empty($curidsel)?$cur["idcurso"]:$curidsel;
                        $curnombre=empty($curnombre)?$cur["nombre"]:$curnombre;
                        $this->cursoSel=empty($this->cursoSel)?$cur:$this->cursoSel;
                    }
                }
            }else{
                $this->documento->redir();   
            }           

            $this->idCurso = (isset($_REQUEST['idcurso']))?$_REQUEST['idcurso']:$curidsel;
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')) , 'link'=> '/curso/?id='.$this->idCurso],
                [ 'texto'=> $curnombre],
            ];

            $this->documento->setTitulo(JrTexto::_('My Progress'));
            $this->Test=$this->oNegTest_asignacion->buscar(array('idcurso'=>$this->cursoActual["idcurso"]));
            $temas=$this->oNegCursoDetalle->sesiones($this->idCurso,0);
               
                $temastmp=array();
                $i=0;
                foreach($temas as $tem){
                    $tem["progreso"]=0;
                    $tem["nactividad"]=0;
                    $tem["link"]='';
                    if($tem["tiporecurso"]=='E'){
                        $nq=$this->oNegNotaquiz->buscar(array('idrecurso'=>$tem["idrecurso"],'idproyecto'=>$this->usuarioAct["idproyecto"],'idalumno'=>$this->usuarioAct["idpersona"]));
                        if(!empty($nq[0])){
                            $ex=$nq[0];                            
                            $extipo=$ex["calificacion_en"];
                            $nota=$ex["nota"];
                            $caltotal=$ex["calificacion_total"];
                            $calmin=$ex["calificacion_min"]; //if($extipo=='P')
                            $progreso=($nota*$caltotal)/100; //elseif()
                            //$progreso=$progreso>=100?100:$progreso;     
                            $progreso=$progreso>=0?100:$progreso;
                            $progreso=number_format((float)$progreso, 1, '.', '');
                            $tem["progreso"]=$progreso;                        
                        }
                        $tem["link"]=$this->documento->getUrlBase()."/examenes/resolver/?idexamen=".$tem["idrecurso"];
                    }else{
                        $tem=$this->getProgreso2($tem); 
                    }
                    $temastmp[]=$tem;
                    $i++;
                }
                $this->newsylabus=$temastmp;             
                $this->documento->plantilla = 'alumno/curso';
                $this->esquema = 'reportealumno/miprogreso';
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    private function getProgreso2($det){
        try{ 
            $det["progreso"]=0;
            $progreso=0;
            $p=0;
            $det["link"]='';
            if($det["tiporecurso"]=='E'){
                $nq=$this->oNegNotaquiz->buscar(array('idrecurso'=>$det["idrecurso"],'idproyecto'=>$this->usuarioAct["idproyecto"],'idalumno'=>$this->usuarioAct["idpersona"]));
                if(!empty($nq[0])){
                   
                    $ex=$nq[0];                            
                    $extipo=$ex["calificacion_en"];
                    $nota=$ex["nota"];
                    $caltotal=$ex["calificacion_total"];
                    $calmin=$ex["calificacion_min"]; //if($extipo=='P')
                    $progreso=($nota*$caltotal)/100; //elseif()
                    //$progreso=$progreso>100?100:$progreso;
                    $progreso=$progreso>=0?100:$progreso; // 100 solo si hizo el examen
                    $progreso=number_format((float)$progreso, 1, '.', '');                    
                    $det["progreso"]=$progreso;                        
                }
                $det["link"]=$this->documento->getUrlBase()."/examenes/resolver/?idexamen=".$det["idrecurso"];
            }else{
                $p=$this->oNegBitacora_smartbook->getProgresoPromedio(array('idcurso'=>$this->idCurso, 'idusuario'=>$this->usuarioAct['idpersona'], 'idsesion'=>$det['idrecurso'])); 
                $p=number_format((float)$p, 1, '.', ''); 
                $det["progreso"]=$p;
                $det["link"]=$this->documento->getUrlBase()."/smartbook/ver2/?idcursodetalle=".$det["idcursodetalle"]."&idrecurso=".$det["idrecurso"];            
                if(!empty($det['hijo'])){
                    $hijos=array();
                    $p=0;
                    $nhijos=count($det['hijo']);
                    $ps=0;
                    $j=0;
                    foreach($det['hijo'] as $hi){
                        if($j==0)$nb=false;
                        $j++;
                        $tmphijo=$this->getProgreso2($hi);
                        $pr=(float) $tmphijo["progreso"];                    
                        if($pr<$this->avanzaen) $nb=true;
                        $ps= $ps + $pr;
                        $hijos[] = $tmphijo;
                    } 
                    $p=(float)$ps/$nhijos;                
                    $p= $p>=100?100:$p;                   
                    $p=number_format((float)$p, 1, '.', '');                
                    $det["progreso"]=$p;
                    $det["hijo"]=$hijos;
                }
            }
            return $det;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function informacion()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }

            /* Datos generales del docente */
            $this->oNegGrupoauladetalle->idgrupoauladetalle = $this->cursoActual['idgrupoauladetalle'];
            $grupoAula_det = $this->oNegGrupoauladetalle->getXid();
            $this->oNegPersonal->dni = $grupoAula_det['iddocente'];
            $this->docente = $this->oNegPersonal->getXid();

            /* Datos generales del horario */
            $this->horario = $this->oNegAcad_horariogrupodetalle->buscar(array("idgrupoauladetalle"=>$this->cursoActual['idgrupoauladetalle']));

            $this->documento->setTitulo(JrTexto::_('Information'), true);
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')) , 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('information')) /*, 'link'=> '/curso/informacion/?id='.$this->idCurso*/ ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/curso_info';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function horario()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            $rol=$this->usuarioAct["rol"];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->script('moment.min', '/libs/fullcalendar/');
            $this->documento->script('datetimepicker', '/libs/datepicker/');
            $this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('locale-all', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
            $this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
            $this->documento->stylesheet('datetimepicker', '/libs/datepicker/');
            $this->documento->script('eventos', '/js/alumno/');
            $this->documento->script('horario', '/js/alumno/');
            
            $this->documento->setTitulo(JrTexto::_('Schedule'), true);
            
            if(strtolower($rol)=="alumno"){
                $this->documento->plantilla = 'alumno/curso';
                $this->esquema = 'alumno/horario';
            }else{
                return $aplicacion->redir();
            }

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')) , 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('schedule')) /*, 'link'=> '/curso/horario/?id='.$this->idCurso*/ ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];

            $this->grupoBtns = [
                ['nombre'=> ucfirst(JrTexto::_('Full schedule')), 'link'=>'/agenda/horario/', 'activo'=>'' ],
            ];

            $this->titulo = ucfirst(JrTexto::_('schedule'));
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/horario';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function companieros()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            
            /* Todos sus compañeros de aula */
            $this->matriculados = array();
            $matriculas = $this->oNegMatricula->buscar(array("idgrupoauladetalle"=>$this->cursoActual['idgrupoauladetalle']));
            if(!empty($matriculas)){
                foreach ($matriculas as $i=>$m) {
                    if($this->usuarioAct['idpersona']!=$m['idalumno']){
                        $this->oNegAlumno->dni = $m['idalumno'];
                        $this->matriculados[] = $this->oNegAlumno->getXid();
                    }
                }
            }

            $this->defaultImg=array(
                "M"=> $this->documento->getUrlStatic()."/media/usuarios/default_m.png",
                "F"=> $this->documento->getUrlStatic()."/media/usuarios/default_f.png",
            );
            $this->documento->setTitulo(JrTexto::_('Classmates'), true);
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')) , 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('classmates')) /*, 'link'=> '/curso/companieros/?id='.$this->idCurso*/ ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/curso_companieros';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function academico()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }

            /*$this->notas_det = $this->oNegNota_detalle->buscar(array("idcurso"=>$this->idCurso, "idalumno"=>$this->usuarioAct['dni'], "idmatricula"=>$this->cursoActual['idmatricula'], "estado"=>1,"metodologia"=>1));
            $this->grupoAulaDet = $this->oNegGrupoauladetalle->buscar(array("idgrupoauladetalle"=>$this->cursoActual['idgrupoauladetalle']));
            $this->asistencia_alum = $this->oNegAsistencia->buscar(array("idgrupoauladetalle"=>$this->cursoActual['idgrupoauladetalle'], "idalumno"=>$this->usuarioAct['dni']));
            if(!empty($this->asistencia_alum)){
                $this->fechaI=$this->grupoAulaDet[0]['fecha_inicio'];
                $this->fechaF=$this->grupoAulaDet[0]['fecha_final'];
                $d_start    = new DateTime(date($this->fechaI)); 
                $d_end      = new DateTime(date( $this->fechaF)); 
                $diff = $d_start->diff($d_end); 
                $this->weeks=floor($diff->days/7);
            } 
            if(!empty($this->notas_det)){
                foreach ($this->notas_det as $i=>$n) {
                    if($n['tipo']=='N' || $n['tipo']=='U' || $n['tipo']=='L'){

                    }else if($n['tipo']=='E'){

                    }
                }
            }*/ 
            $this->documento->setTitulo(JrTexto::_('Academic'), true);
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')) /*, 'link'=> '/curso/academico/?id='.$this->idCurso*/ ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/academico1';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function asistencia()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
           
            
            $this->documento->setTitulo(JrTexto::_('Academic'), true);
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')) , 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('assists & record'))],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/academico';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function tarea()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
            }
           

            /*if(!empty($this->idCurso)){
                $this->sesiones=$this->oNegCursoDetalle->sesiones($this->idcurso,0);
            }*/
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')), 'link'=> '/curso/academico/?id='.$this->idCurso],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];

            $pend_final = $this->listarNuevasDevueltasYFinalizadas(true);
            // var_dump($pend_final);
            $this->tareasPend = $pend_final["pendientes"];
            $this->tareasFin = $pend_final["finalizadas"];

            $this->documento->script('curso', '/js/alumno/');

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'tarea/tarea-list-alum';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function getidrecursos($recursos){
        $idrecursos=array();
        if(!empty($recursos))
        foreach($recursos as $rec){
            if($rec["tiporecurso"]=='L')
            $idrecursos[]=intVal($rec["idrecurso"]);
            if(!empty($rec["hijo"])){
                $hijos=$this->getidrecursos($rec["hijo"]);
                if(!empty($hijos))
                $idrecursos= array_merge($idrecursos,$hijos);
            }
        }
       return $idrecursos;
    }

    public function listarNuevasDevueltasYFinalizadas($return=false)
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
          
            if(empty($_REQUEST["idcurso"]) && !$return){
                throw new Exception(JrTexto::_('Error in filtering'));
            }
         
            $idcurso = !empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:$this->idCurso;
           
            $recursos=$this->oNegCursoDetalle->sesiones($idcurso,0);
            
            $idrecursos=$this->getidrecursos($recursos);
            if(empty($idrecursos)){
                if($return){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
                $data=array('code'=>'ok','data'=>array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]));
                echo json_encode($data);
                exit(0);
            }
            $allidrecursos=join(',',$idrecursos);

            #Declaracion de clases e Instanciacion de objetos:
              //JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
              JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
              JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
              //$this->oNegTarea = new NegTarea;
              $this->oNegTarea_asignacion = new NegTarea_asignacion;
              $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;

            $usuarioAct = NegSesion::getUsuario();
            $this->todoTareas=$this->oNegTarea->buscar(array('allrecursos'=>$allidrecursos,'eliminado'=>0,'idproyecto'=>$usuarioAct["idproyecto"]));
            if(!empty($this->todoTareas))
            foreach ($this->todoTareas as $t) {
                $cant_asiganaciones=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea']));
                if(!empty($asignaciones)) {
                    foreach ($asignaciones as $a) {
                        $hoy = new DateTime(date('Y-m-d H:i:s'));
                        $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
                        $fechahoy=date('d-m-Y');
                        $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
                        $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
                        $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
                        $asignacion_alumno=$this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'idalumno'=>$usuarioAct['idpersona'] , /*'estado'=>['N','D','E','P']*/));
                        if(!empty($asignacion_alumno)){
                            $a['asignacion_alumno'] = $asignacion_alumno[0];
                            if($fecha_hora>$hoy && ($asignacion_alumno[0]['estado']=='N' || $asignacion_alumno[0]['estado']=='D') ){
                                $this->tareasPend[] = array_merge($t,$a);
                            }
                            if($fecha_hora<=$hoy || $asignacion_alumno[0]['estado']=='E' || $asignacion_alumno[0]['estado']=='P'){
                                $this->tareasFin[] = array_merge($t,$a);
                            }
                        } 
                    }
                } /*else {
                    $this->tareasPend[] = $t;
                }*/
            }

            $respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, );
            if($return){ return $respuesta; }
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($return){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }


    public function examenes()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            $this->documento->setTitulo(JrTexto::_('Homework'), true);

            $usuarioAct = NegSesion::getUsuario();
            $this->url = URL_SMARTQUIZ.'?id='.@$usuarioAct['usuario'].'&pr='.IDPROYECTO.'&u='.@$usuarioAct['usuario'].'&p='.@$usuarioAct['clave']; 

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('assessments')), 'link'=> '/curso/examenes/?id='.$this->idCurso ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'examenes/iframe';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function silabus()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            $this->documento->setTitulo(JrTexto::_('Silabus'), true);
            $this->breadcrumb = [
               // [ 'texto'=> ucfirst(JrTexto::_('teacher')), 'link'=> '/docente/?id='.$this->idDocente ],
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('information')), 'link'=> '/curso/informacion/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('silabus')), 'link'=> '/curso/silabus/?id='.$this->idCurso ],
                [ 'texto'=> $this->cursoActual['nombre'] ],
            ];
            $this->documento->script('curso', '/js/alumno/');
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/curso_silabus';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    // ========================== Funciones AJAX ========================== //
    public function getNivelesxIdpadre($return=false)
    {
        try {
            $MINIMO_PARA_PASAR = $this->avanzaen; //Porcentaje minimo para pasar
            $idCursoDet = !empty($_REQUEST['idpadre'])?$_REQUEST['idpadre']:0;
            $this->idCurso = !empty($_REQUEST['idcurso'])?$_REQUEST['idcurso']:$this->idCurso;
            $this->oNegCursoDetalle->setLimite(0,1000);
            $sylabus = $this->oNegCursoDetalle->buscar(array('idcurso'=>$this->idCurso, 'idpadre'=>$idCursoDet));
            $urlredir = null;
            $isNextLocked = true;
            if(!empty($sylabus)){
                $arrExamEntradaYSalida = $this->identificarExamPrimerYUltimo($sylabus);
                $this->tieneRolAdmin = NegSesion::tieneRol('administrador');
                $tieneResultadoExam = false;
                foreach ($sylabus as $i=>$sy) {
                    $sylabus[$i]['bloqueado'] = true;
                    if($i==0 || $this->tieneRolAdmin || $this->tieneBitacora($sy) || $tieneResultadoExam || $isNextUnlocked) { 
                        $sylabus[$i]['bloqueado'] = false; 
                        $tieneResultadoExam = false;
                    }

                    $t = $sy['tiporecurso'];
                    if($t=='N' || $t=='U' || $t=='L'){
                        $sylabus[$i]['link'] = $this->documento->getUrlBase().'/curso';
                        $progreso = $this->getProgreso($sy);
                        $sylabus[$i]['progreso'] = $progreso;
                        $isNextUnlocked = $progreso>$this->avanzaen;
                    }else if($t=='E'){
                        $sylabus[$i]['link'] = $this->documento->getUrlBase().'/examenes/resolver';
                        $resultado = $this->oNegExamenes->getResultado($sy['idrecurso'], $this->usuarioAct['usuario']);
                        $sylabus[$i]['tipo_examen'] = array_key_exists($i, $arrExamEntradaYSalida)?$arrExamEntradaYSalida[$i]:'-';
                        if(!empty($resultado)){
                            $sylabus[$i]['resultado'] = $resultado;
                            $sylabus[$i]['progreso'] = ((float)$resultado['puntaje']/(float)$resultado['calificacion_max'])*100;
                            $tieneResultadoExam = true;
                        }
                    }else if($t=='EU'){
                        $sylabus[$i]['link'] = $this->documento->getUrlBase().'/examenes/resolver';
                        $resultado = $this->oNegExamenes->getResultado($sy['idrecurso'], $this->usuarioAct['usuario']);
                        if(!empty($resultado)){
                            $sylabus[$i]['resultado'] = $resultado;
                            $sylabus[$i]['progreso'] = ((float)$resultado['puntaje']/(float)$resultado['calificacion_max'])*100;
                        }
                    }
                }
            } else {
                $this->oNegCursoDetalle->idcursodetalle = $idCursoDet;
                $curso_det = $this->oNegCursoDetalle->getXid();
                $sesion = $this->oNegNiveles->buscar(array('idnivel'=>$curso_det['idrecurso']));
                $s = !empty($sesion)?$sesion[0]['idnivel']:0;
                $unidad = $this->oNegNiveles->buscar(array('idnivel'=>$sesion[0]['idpadre']));
                $u = !empty($unidad)?$unidad[0]['idnivel']:0;
                $nivel = $this->oNegNiveles->buscar(array('idnivel'=>$unidad[0]['idpadre']));
                $n = !empty($nivel)?$nivel[0]['idnivel']:0;
                $urlredir = $this->documento->getUrlBase().'/smartbook/ver/'.$n.'/'.$u.'/'.$s.'/?idcurso='.$this->idCurso;
            }

            if($return){ return $sylabus; }
            echo json_encode(array("code"=>"ok", "data"=>$sylabus, "redir"=>$urlredir)); exit();
        } catch (Exception $e) {
            if($return){ throw new Exception($e->getMessage()); }
            echo json_encode(array("code"=>"error", "msje"=>$e->getMessage() )); exit();
        }
    }

    public function getCantidadNotif()
    {
        try {

            #Declaracion de clases e Instanciacion de objetos:
              JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');
              $this->oNegAulasvirtuales = new NegAulasvirtuales;

            if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Course not found")); }
            $usuarioAct = NegSesion::getUsuario();     
            
            $tareas = $this->listarNuevasDevueltasYFinalizadas(true);
            $aulas=$this->oNegAulasvirtuales->buscar(array("idpersona"=>$usuarioAct['idpersona']));
            $examenes = array(); /*Estos exámenes serán practicas que asigna el docente al alumno. Estas asignaciones deben estar guardadas en BD de SmartLearn con idexamen de un examen pertenciente a Smartquiz.*/

            $data = array(
                "tarea" => count(@$tareas["pendientes"]),
                "aulavirtual" => count(@$aulas),
                "examen" => count(@$examenes),
            );
            echo json_encode(array("code"=>"ok", "data"=>$data)); exit();
        } catch (Exception $e) {
            echo json_encode(array("code"=>"error", "msje"=>$e->getMessage() )); exit();
        }
    }

    // ========================== Funciones Privadas ========================== //
    private function tieneAcceso()
    {        
        $this->idCurso = (isset($_REQUEST['id']))?$_REQUEST['id']:null;
          if(empty($this->idCurso)){ throw new Exception(JrTexto::_("Course not found")); }        
        $idrol=$this->usuarioAct["idrol"];
        $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"],'idcurso'=>$this->idCurso));
        if(empty($cursosProyecto[0])){ throw new Exception(JrTexto::_("Course not found")); } 
        $this->cursoActual=$cursosProyecto[0];
        if($idrol==1) return true; // administrador
        else if($idrol==2){ // docente
            return true; // quitar esta linea si quiero solo cursos matriculados.
            $this->miscursos=$this->oNegGrupoaulaDetalle->buscar(array('idcurso'=>$this->idcurso,'iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));
            if(!empty($this->miscursos[0])) return true;
            else { throw new Exception(JrTexto::_("You do not have assigned courses"));} 
        }else if($idrol==3){ // alumno
            $cursoMat = $this->oNegMatricula->buscar(array('idcurso'=>$this->idcurso,'idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));          
            if(!empty($cursoMat[0])) return true;
            else { throw new Exception(JrTexto::_("You do not have assigned courses 2")); }
        }else return false;        
    }

    private function getProgreso($curso_det)
    {
        try {
            $suma = $progreso_prom = 0.0;
            $cont = 0;
            if($curso_det['tiporecurso']=='L'){ $sesiones[] = $curso_det; }
            else{
                $sesiones = $this->oNegCursoDetalle->getSoloSesiones($this->idCurso, $curso_det['idcursodetalle']);
            }

            if(!empty($sesiones)){
                foreach ($sesiones as $i=>$s) {
                    if($s['tiporecurso']=='E'){ //verifica solo si dio el examen, NO su puntaje.
                        $resultado = $this->oNegExamenes->getResultado($s['idrecurso'], $this->usuarioAct['usuario']);
                        if(!empty($resultado)){ $suma += 100.0; }
                    }if($s['tiporecurso']=='L'){
                        $suma += (float) $this->oNegBitacora_smartbook->getProgresoPromedio(array('idcurso'=>$this->idCurso, 'idusuario'=>$this->usuarioAct['idpersona'], 'idsesion'=>$s['idrecurso']));
                    }
                }
                $progreso_prom = $suma/count($sesiones);
            }

            return round($progreso_prom,1); 
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function tieneBitacora($curso_det)
    {
        try {
            $tieneBitacoras = false;

            if($curso_det['tiporecurso']=='L'){ $sesiones[] = $curso_det; }
            else{
                $sesiones = $this->oNegCursoDetalle->getSoloSesiones($this->idCurso, $curso_det['idcursodetalle']);
            }

            $bitacoras = array();
            if(!empty($sesiones)){
                foreach ($sesiones as $i=>$s) {
                    $b = $this->oNegBitacora_smartbook->buscar(array( 'idcurso'=>$this->idCurso, 'idusuario'=>$this->usuarioAct['idpersona'], 'idsesion'=>$s['idrecurso'] ));
                    if(!empty($b)) { $bitacoras[] = $b[0]; }
                }
            }

            if(!empty($bitacoras)) {
                $tieneBitacoras = true;
            }
            return $tieneBitacoras;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getExamenUbicacion()
    {
        try {

            $exam_ubicacion = $this->oNegExamen_ubicacion->buscar(array("idcurso"=>$this->idCurso, "activo"=>1, "tipo"=>'N' ));

            $this->getHijosYResultado_Exam($exam_ubicacion);

            return $exam_ubicacion;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getHijosYResultado_Exam(&$exam_ubicacion)
    {
        try {
            JrCargador::clase('sys_negocio::NegExamen_ubicacion_alumno', RUTA_BASE, 'sys_negocio');
            $this->oNegExamen_ubicacion_alumno = new NegExamen_ubicacion_alumno;

            $resultado = $result_SQ = null;
            $idexam_ubic_alum = -1;

            if(!empty($exam_ubicacion)) {
                $exam_ubicacion = $exam_ubicacion[0];
                $exam_ubic_hijos = $this->oNegExamen_ubicacion->buscar( array("idexam_prerequisito"=>$exam_ubicacion["idexamen_ubicacion"], "activo"=>1) );
                $exam_ubicacion["exam_hijos"] = !empty($exam_ubic_hijos)?$exam_ubic_hijos:null;
                $resultado = $this->oNegExamen_ubicacion_alumno->buscar(array("idexamen"=>$exam_ubicacion['idexamen_ubicacion'], "idalumno"=>$this->usuarioAct['dni'] ));
                if(empty($resultado)) {
                    $result_SQ = $this->oNegExamenes->getResultado($exam_ubicacion['idrecurso'], $this->usuarioAct['usuario']);
                }
            }

            if(!empty($resultado)){
                $idexam_ubic_alum = $resultado[0]['idexam_alumno'];
            }else if(!empty($result_SQ)){
                $this->oNegExamen_ubicacion_alumno->idexamen=@$exam_ubicacion['idexamen_ubicacion'];
                $this->oNegExamen_ubicacion_alumno->idalumno=@$this->usuarioAct['idpersona'];
                $this->oNegExamen_ubicacion_alumno->estado='T';
                $this->oNegExamen_ubicacion_alumno->resultado=json_encode(@$result_SQ);
                $this->oNegExamen_ubicacion_alumno->fecha=@date('Y-m-d H:i:s');
                
                $idexam_ubic_alum=$this->oNegExamen_ubicacion_alumno->agregar();
            }

            if($idexam_ubic_alum>0) {
                $exam_ubic_alum = $this->oNegExamen_ubicacion_alumno->buscar(array("idexam_alumno"=>$idexam_ubic_alum));
                $exam_ubicacion['resultado'] = $exam_ubic_alum[0];
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function getExamHijoXEscala(&$exam_ubicacion, $escalaObtenida)
    {
        try {

            if(empty($exam_ubicacion["exam_hijos"]) || $exam_ubicacion["resultado"]["estado"]=='O'){
                $exam_ubicacion=null;
                return false; 
            }


            $puntaje = $escalaObtenida["puntaje"];

            $exam_ubicacion = $this->oNegExamen_ubicacion->buscar(array("idcurso"=>$this->idCurso, "tipo"=>'U', "rango_min_menorA"=>$puntaje, "rango_max_mayorA"=>$puntaje));
            
            $this->getHijosYResultado_Exam($exam_ubicacion);

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function evaluarExamenUbicacion($exam_ubicacion)
    {
        try {
            $exam_result = $exam_ubicacion["resultado"];
            if( empty($exam_result) ) {  return false; }

            $resultado = json_decode($exam_result["resultado"], true);
            $puntaje = (float) $resultado["puntaje"];
            $escalas = json_decode($resultado["calificacion_max"], true);
            $escalaObtenida = $habilitarXescalas = null;

            if(!empty($escalas)){
                foreach ($escalas as $e) {
                    if( (float)$e["min"]<$puntaje && (float)$e["max"]>=$puntaje ) {
                        $escalaObtenida = $e;
                    }
                    if( $puntaje>(float)$e["max"] || !empty($escalaObtenida) ){
                        $habilitarXescalas[ $e["min"] ] = $e;
                    }
                }
            }
            $escalaObtenida["puntaje"] = $puntaje;
            if($exam_result["estado"]=="T"){

                $habilito = $this->habilitarNiveles($escalaObtenida, $habilitarXescalas);

                if($habilito){
                    $this->oNegExamen_ubicacion_alumno->setCampo($exam_result["idexam_alumno"],'estado','TU');
                }
            }

            return $escalaObtenida;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function habilitarNiveles($escalaObtenida, $habilitarXescalas=array())
    {
        try {
            $returnValue = false;
            if(empty(@$escalaObtenida["idcursodetalle"])){ return $returnValue; }
            $idcursodetalle = $escalaObtenida["idcursodetalle"];
            $sesiones = null;
            #Declaracion de Clases e Instanciación de objetos:
              JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE, 'sys_negocio');
              $this->oNegBitac_alum_smbook = new NegBitacora_alumno_smartbook;

            if(!empty($habilitarXescalas)){
                ksort($habilitarXescalas);

                foreach ($habilitarXescalas as $k=>$e) {
                    $sesiones = $this->oNegCursoDetalle->getSoloSesiones($this->idCurso, $e["idcursodetalle"]);


                    if(!empty($sesiones)){
                        foreach ($sesiones as $s) {
                            $this->oNegBitac_alum_smbook->idcurso = @$this->idCurso;
                            $this->oNegBitac_alum_smbook->idsesion = @$s["idrecurso"];
                            $this->oNegBitac_alum_smbook->idusuario = @$this->usuarioAct['idpersona'];
                            $this->oNegBitac_alum_smbook->estado = 'P';
                            $this->oNegBitac_alum_smbook->regfecha = @date('Y-m-d H:i:s');
                            $resp = $this->oNegBitac_alum_smbook->agregar();
                            if($e["idcursodetalle"]==$idcursodetalle){ break; }
                        }
                    }
                }
            }

            if(@$resp>0) $returnValue = true;

            return $returnValue;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function identificarExamPrimerYUltimo($arrSylabus)
    {
        try {
            $arrPrimerYUltimo = array();
            for ($i=0; $i < count($arrSylabus); $i++) {
                if($arrSylabus[$i]["tiporecurso"]=='E'){
                    $arrPrimerYUltimo[$i] = 'E'; /* "E": Exam. Entrada */
                    break;
                }
            }
            for ($j=count($arrSylabus)-1; $j >= 0; $j--) {
                if($arrSylabus[$j]["tiporecurso"]=='E'){
                    $arrPrimerYUltimo[$j] = 'S'; /* "S": Exam. Salida */
                    break;
                }
            }

            return $arrPrimerYUltimo;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function reemplazarvalortabla()
    {
        // $this->todoTareas=$this->oNegTarea->buscar($filtrosTarea);
        $this->oNegTarea->actualizartabla("tarea","idactividad");
    }
}