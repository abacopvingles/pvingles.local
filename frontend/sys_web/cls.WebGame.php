<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGame', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');

JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
class WebGame extends JrWeb
{
	private $oNegGame;
	private $oNegHerramientas;

    private $oNegTarea_archivos;
    private $oNegTarea;  
    private $oNegTarea_asignacion;
    private $oNegTarea_asignacion_alumno;
    private $oNegTarea_respuesta;

	public function __construct()
	{
		parent::__construct();	
		$this->usuarioAct = NegSesion::getUsuario();	
		$this->oNegGame = new NegGame;
		$this->oNegHerramientas = new NegHerramientas;

        $this->oNegTarea_archivos = new NegTarea_archivos;
        $this->oNegTarea = new NegTarea;    
        $this->oNegTarea_asignacion = new NegTarea_asignacion;
        $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        $this->oNegTarea_respuesta = new NegTarea_respuesta;
	}

	public function ver(){
		try{
			global $aplicacion;			
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->script('tools_games', '/js/new/');
            $this->documento->script('plugin_visor', '/js/');
            $usuarioAct = NegSesion::getUsuario();
            $idgame=!empty($_REQUEST["idgame"])?$_REQUEST["idgame"]:9;
            $idgame=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):$idgame;
			$this->datos=$this->oNegHerramientas->buscar(Array('id'=>$idgame,'tool'=>'G'));	
            $this->rolUsuarioAct = $usuarioAct['rol'];
            $arrDatosTarea = $this->fnTareaArchivo($idgame);
            $this->idTareaArchivo = @$_GET['idtarea_archivos'];
            $this->tarea = @$arrDatosTarea['tarea'];
            $this->tarea_archivo = @$arrDatosTarea['tarea_archivo'];
            $this->tarea_asignacion_alumno = @$arrDatosTarea['tarea_asignacion_alumno'];
            $this->tarea_respuesta = @$arrDatosTarea['tarea_respuesta'];

			$this->documento->plantilla='verblanco';
			$this->esquema = 'tools/game_ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			/*$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();*/
            return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function crear(){
		try{
			global $aplicacion;
			$this->documento->script('inicio', '/js/new/');			
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('editactividad', '/js/new/');
            $this->documento->script('tools_games', '/js/new/');
            $this->documento->script('plugin_visor', '/js/');
            $idgame=!empty($_GET["idgame"])?$_GET["idgame"]:null;
            $this->nivel=!empty($_GET["nivel"])?$_GET["nivel"]:null;
            $this->unidad=!empty($_GET["unidad"])?$_GET["unidad"]:null;
            $this->actividad=!empty($_GET["actividad"])?$_GET["actividad"]:null;
            $usuarioAct = NegSesion::getUsuario(); 
            $idpersonal=$usuarioAct["dni"];
            $filtros['idNivel']=$this->nivel;
            $filtros['idunidad']=$this->unidad;
            $filtros['idactividad']=$this->actividad;
            /*$filtros['idpersonal']=$idpersonal;*/
            $filtros['tool']='G';
            $filtros['id']=$idgame;
            if(!empty($idgame)){
				$this->datos=$this->oNegHerramientas->buscar($filtros);
            }else{
				$this->datos=null;
			}
			$this->documento->plantilla='verblanco';
			$this->esquema = 'tools/games';
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

    private function fnTareaArchivo($idGame=0)
    {
        try {
            global $aplicacion;
            if(isset($_GET['idtarea_archivos'])){
                if(empty($_GET['idtarea_archivos'])){ throw new Exception(JrTexto::_("Cannot load homework game")."."); }
                $idTareaArchivo = $_GET['idtarea_archivos'];
                $usuarioAct = NegSesion::getUsuario();
                $adjunto=$tarea=$asig_alumno=$respuesta=$archivoAdjunto= array();
                $adjunto = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$idTareaArchivo, /*'tablapadre'=> 'T'*/]);
                
                if($usuarioAct['rol']=='Alumno' && empty($_GET['idtarea_asignacion'])){ throw new Exception(JrTexto::_("Cannot load homework game").". ".JrTexto::_("Assignment was not found")."."); }
                
                $filtrosAsig['idtarea_asignacion']=(!empty(@$_GET['idtarea_asignacion']))? @$_GET['idtarea_asignacion']:0;
                $filtrosAsig['idalumno']=($usuarioAct['rol']=='Alumno')? $usuarioAct['idpersona']:@$_GET['idalumno'];
                $asig_alumno=$this->oNegTarea_asignacion_alumno->buscar($filtrosAsig);
                
                if(!empty($asig_alumno)){
                    $asig_alumno= $asig_alumno[0];
                    $respuesta = $this->oNegTarea_respuesta->buscarConArchivos(['idtarea_asignacion_alumno'=>$asig_alumno['iddetalle']], ['idtarea_archivos_padre'=> $idTareaArchivo]);
                }else{ throw new Exception(JrTexto::_("This attachment is not assigned to you")); }

                if(!empty($respuesta)){ 
                    $respuesta= $respuesta[0];
                    if(!empty($respuesta['respuesta_archivos'])){
                        $adjunto=$respuesta['respuesta_archivos'];
                    }else{
                        $adjunto = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$idTareaArchivo, 'tablapadre'=> 'T']);
                    }
                }

                if(!empty($adjunto)){
                    $adjunto = $adjunto[0];
                    $filtrar = ['idtarea'=>-1];
                    if($adjunto['tablapadre']=='T'){ $filtrar['idtarea']=$adjunto['idpadre']; }
                    elseif($adjunto['tablapadre']=='R'){ 
                        $tarea_archivo = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$adjunto['idtarea_archivos_padre']]);
                        $filtrar['idtarea']=$tarea_archivo[0]['idpadre'];
                    }
                    if($usuarioAct['rol']!='Alumno'){ $filtrar['iddocente']=$usuarioAct['dni']; }
                    $tarea = $this->oNegTarea->buscar($filtrar);
                }else{ throw new Exception(JrTexto::_("Game not found for this homework")); }

                if(!empty($tarea)){
                    $tarea = $tarea[0];
                    $archivoAdjunto = $adjunto;
                }else{ throw new Exception(JrTexto::_("Game does not belong to your homework")); }

                $resp = ['tarea'=>$tarea, 'tarea_archivo'=>$archivoAdjunto, 'tarea_asignacion_alumno'=>$asig_alumno, 'tarea_respuesta'=>$respuesta];
                return $resp;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	// ========================== Funciones xajax ========================== //
	public function xSaveGame(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				var_dump($frm);
				$rutabase=$this->documento->getUrlBase();				 
				$texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
				if(empty($texto)) return;
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegGame->__set('idnivel',@$frm['idNivel']);
				$this->oNegGame->__set('idunidad',@$frm['idUnidad']);
				$this->oNegGame->__set('idactividad',@$frm['idActividad']);
				$this->oNegGame->__set('idpersonal',@$usuarioAct["dni"]);
				$this->oNegGame->__set('texto',$texto);
				$this->oNegGame->__set('orden',($usuarioAct["rol"]==1?0:1));
				$res=$this->oNegGame->agregar();
				if(!empty($res)) {
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml( JrTexto::_("game").' '.JrTexto::_("saved successfully") ),'success');
					$oRespAjax->setReturnValue($this->oNegGame->idtool);
				}
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGame->__set('idtool', $pk);
				$res=$this->oNegGame->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}