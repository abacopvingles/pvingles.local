<?php
set_time_limit(0);
defined('RUTA_BASE') or die();

JrCargador::clase('sys_negocio::NegResumen', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno',RUTA_BASE, 'sys_negocio');

class WebResumen extends JrWeb {
	protected $oNegMinedu;
	protected $oNegGrado;
    protected $oNegSeccion;
    protected $oNegResumen;
    protected $oNegNotas_quiz;
    protected $oNegActividad_alumno;
    
    public function __construct(){
        parent::__construct();
        $this->oNegMinedu = new NegMinedu;
		$this->oNegGrado = new NegMin_grado;
        $this->oNegSeccion = new NegMin_sesion;
        $this->oNegResumen = new NegResumen;
        $this->oNegNotas_quiz = new NegNotas_quiz;
        $this->oNegActividad_alumno = new NegActividad_alumno;
    }
    public function defecto(){
        return "empty";
    }

    public function testCampo(){
        $this->documento->plantilla = 'blanco';
        try{
            $result = $this->oNegNotas_quiz->setIdnota(196);
            if($result){
                $this->oNegNotas_quiz->nota = 22.13; //22.13
                $this->oNegNotas_quiz->editar();
            }
            $result = $this->oNegActividad_alumno->setId(27816);
            if($result){
                $this->oNegActividad_alumno->porcentajeprogreso = 99;//100;
                $this->oNegActividad_alumno->editar();
            }
            echo json_encode(array('code'=>'ok','msj' => 'Listo'));
            exit(0);
        }catch(Exception $e){
            print($e->getMessage());
            return 0;
        }
    }
    
    public function resumenMinedu(){
        $this->documento->plantilla = 'blanco';

        try{

            $result = 'Reports Complete';
            $secciones = $this->oNegSeccion->buscar();
            $grados = $this->oNegGrado->buscar();
            $dre = $this->oNegResumen->regiones();

            //generar reporte seccion:
            if(!empty($dre)){
                foreach($dre as $key => $value){
                    if(isset($value['ugeles']) && !empty($grados) && !empty($secciones)){
                        foreach($value['ugeles'] as $key_u => $value_u){
                            $locales = $this->oNegResumen->getlocales(array('idugel' =>$value_u['idugel'],'idproyecto'=> 3));
                            if(!empty($locales)){
                                foreach($locales as $colegio){
                                    foreach($grados as $g){
                                        foreach($secciones as $s){
                                            
                                            $datas = array('id_ubigeo'=>$value['id_ubigeo'],'iddre'=>$value['iddre'],'idugel'=>$value_u['idugel'],'idlocal'=>$colegio['idlocal'],'idgrado'=>$g['idgrado'], 'idsesion' => $s['idsesion'],'sesion' => $s['descripcion']);
                                            
                                            $result_u = $this->oNegResumen->seccion_ubicacion($datas);
                                            $result_e = $this->oNegResumen->seccion_entrada($datas); // 0.1000
                                            $result_s = $this->oNegResumen->seccion_salida($datas); // 0.2000
                                            $result_b = $this->oNegResumen->seccion_bimestre($datas); //0.4

                                            $result_t = $this->oNegResumen->seccion_trimestre($datas);
                                            $result_time = $this->oNegResumen->seccion_tiempo($datas);
                                            $result_progress = $this->oNegResumen->seccion_progreso($datas);
                                        }// end foreach seccion
                                        $datas = array('iddre' => $value['iddre'],'id_ubigeo'=>$value['id_ubigeo'],'idugel' => $value_u['idugel'], 'idlocal' => $colegio['idlocal'],'idgrado' => $g['idgrado'], 'grado' => $g['descripcion']);
                                        $result_grado = $this->oNegResumen->grado_ubicacion($datas);
                                    }//end foreach grados
                                    $datas = array('iddre' => $value['iddre'],'id_ubigeo'=>$value['id_ubigeo'],'idugel' => $value_u['idugel'], 'idlocal' => $colegio['idlocal'], 'local' => $colegio['nombre']);
                                    $result_local = $this->oNegResumen->local_resumen($datas);
                                }//end foreach colegio
                            }
                            $datas = array('iddre' => $value['iddre'],'id_ubigeo'=>$value['id_ubigeo'],'idugel' => $value_u['idugel'], 'idlocal' => $colegio['idlocal'], 'ugel' => $value_u['descripcion']);
                            $result_ugel = $this->oNegResumen->ugel_resumen($datas);
                        }//end foreach ugel
                    } 
                    $datas = array('iddre' => $value['iddre'],'id_ubigeo'=>$value['id_ubigeo'],'dre' => $value['dre']);
                    $result_dre = $this->oNegResumen->dre_resumen($datas);
                }
                

            }
            

            echo json_encode(array('code'=>'ok','message'=>$result));
            exit(0);
        }catch(Exception $e){
            echo $e->getMessage();
            exit(0);
        }
    }
}
?>