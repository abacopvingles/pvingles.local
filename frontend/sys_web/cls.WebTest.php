<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-01-2019 
 * @copyright	Copyright (C) 29-01-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTest', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTest_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTest_criterios', RUTA_BASE, 'sys_negocio');
class WebTest extends JrWeb
{
	private $oNegTest;
	private $oNegCurso;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTest = new NegTest;
		$this->oNegCurso = new NegAcad_curso;
		$this->NegTestasignacion = new NegTest_asignacion;
		$this->NegTestcriterios = new NegTest_criterios;
		$this->usuarioAct = NegSesion::getUsuario();
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegTest->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Test'), true);
			$this->esquema = 'test-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function resolver(){
		try{
			global $aplicacion;
			$filtros=array();
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["idasignacion"])&&@$_REQUEST["idasignacion"]!='')$filtros["idasignacion"]=$_REQUEST["idasignacion"];
			
			JrCargador::clase('sys_negocio::NegTest_alumno', RUTA_BASE, 'sys_negocio');
			$NegTest_alumno = new NegTest_alumno;

		
			$this->test=$this->NegTestasignacion->buscar($filtros);
			$testCriterios=$this->NegTestcriterios->buscar($filtros);
			$filtros["idalumno"]=$this->usuarioAct["idpersona"];			
			$testalumno=$NegTest_alumno->buscar($filtros);

			$this->criterios=array();
			$this->idtestasigancion=!empty($this->test["idtestasigancion"])?$this->test["idtestasigancion"]:0;
			if(!empty($testCriterios)&&!empty($testalumno)){				
				foreach ($testCriterios as $k => $v){
					foreach ($testalumno as $kk => $vv) {
						if($v["idtest"]==$vv["idtest"]&&$v["idtestcriterio"]==$vv["idtestcriterio"]){
							$v["idtestalumno"]=$vv["idtestalumno"];
							$v["idtestasigancion"]=$vv["idtestasigancion"];
							$v["puntaje"]=$vv["puntaje"];
						}
					}
					$this->criterios[]=$v;					
				}
			}else{
				$this->criterios=$testCriterios;
			}			
			$this->esquema = 'test-resolver';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->Asignaciones=array();
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Test').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTest->idtest = @$_GET['id'];
			$this->datos = $this->oNegTest->dataTest;
			$this->Asignaciones=$this->NegTestasignacion->buscar(array('idtest'=>@$_GET['id']));
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Test').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	

			$this->fkcursos=$this->oNegCurso->buscarxproyecto(array('idproyecto'=>$this->usuarioAct["idproyecto"],'orderby'=>'nombre','estado'=>1));

			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'test-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["titulo"])&&@$_REQUEST["titulo"]!='')$filtros["titulo"]=$_REQUEST["titulo"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegTest->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarTest(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idTest)) {
				$this->oNegTest->idtest = $idTest;
				$accion='_edit';
			}
            //$usuarioAct = NegSesion::getUsuario();
            //JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");
			$this->oNegTest->titulo=@$txtTitulo;
			$this->oNegTest->puntaje=@$txtPuntaje;
			$this->oNegTest->mostrar=@$txtMostrar;
					
            if($accion=='_add'){
            	$res=$this->oNegTest->agregar();
            	if(!empty($_FILES['imagen'])){
					$file=$_FILES["imagen"];
					$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
					$newfoto='test_'.$res.'.'.$ext;
					$dir_media=RUTA_BASE . 'static' . SD . 'media';
					if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)){
						$this->oNegTest->idtest=$res;
						$this->oNegTest->imagen='/static/media/'.$newfoto;
						$this->oNegTest->editar();
					}
				}
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	if(!empty($_FILES['imagen'])){
					$file=$_FILES["imagen"];
					$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
					$newfoto='test_'.$idTest.'.'.$ext;
					$dir_media=RUTA_BASE . 'static' . SD . 'media';
					if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)){$this->oNegTest->imagen='/static/media/'.$newfoto;}
				}
            	$res=$this->oNegTest->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminarTest(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);
            }
            @extract($_POST);
            $accion='_add';
            if(!empty($idtest)){
            	$this->oNegTest->__set('idtest', $idtest);
            	$res = $this->oNegTest->eliminar();
            	echo json_encode(array('code'=>'ok'));
			}else{	echo json_encode(array('code'=>'Error'));}           
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}



	
	// ========================== Funciones xajax ========================== //
	public function xSaveTest(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtest'])) {
					$this->oNegTest->idtest = $frm['pkIdtest'];
				}
				JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");
				$this->oNegTest->titulo=@$frm["txtTitulo"];
					$this->oNegTest->puntaje=@$frm["txtPuntaje"];
					$this->oNegTest->mostrar=@$frm["txtMostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
					
						$txtImagen=NegTools::subirImagen("imagen_",@$frm["txtImagen"], "test",false,100,100 );
						$this->oNegTest->__set('imagen',@$txtImagen);
										    $res=$this->oNegTest->agregar();
					}else{
					
						$archivo=basename($frm["txtImagen_old"]);
						if(!empty($frm["txtImagen"])) $txtImagen=NegTools::subirImagen("imagen_",@$frm["txtImagen"], "test",false,100,100 );
						$this->oNegTest->__set('imagen',@$txtImagen);
						@unlink(RUTA_SITIO . SD ."static/media/test/".$archivo);
										    $res=$this->oNegTest->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTest->idtest);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTest(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest->__set('idtest', $pk);
				$this->datos = $this->oNegTest->dataTest;
				$res=$this->oNegTest->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest->__set('idtest', $pk);
				$res=$this->oNegTest->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegTest->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}