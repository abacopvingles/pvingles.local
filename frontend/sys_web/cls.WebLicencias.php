<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018 
 * @copyright	Copyright (C) 17-04-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_estudio', RUTA_BASE, 'sys_negocio');
class WebLicencias extends JrWeb
{
	protected $oNegbib_estudio;
	public function __construct()
	{
		parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegbib_estudio = new Negbib_estudio;
	}
	public function defecto(){
		return $this->lista_menulicencias();
	}
	public function lista_menulicencias()
	{
		try {
			global $aplicacion;
			// var_dump('hola');

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Licencias'), true);
			$user=$this->usuarioAct;
			$this->user = $this->usuarioAct;

			$this->esquema = 'academico/list_licencia';
	        $this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		
	}
	public function listadolicencias(){
		try{
			global $aplicacion;
			$filtros=array();
			$usuarioAct = NegSesion::getUsuario();
			// $filtros["idproyecto"]=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			// if(!empty($_REQUEST["idproyecto"]))$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			//$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			//$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			// $this->datos=$this->oNegPersonal->buscar($filtros);
			$this->documento->setTitulo(JrTexto::_('Licenses').' /', true);
			// $vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:'1';
			$this->esquema = 'academico/licencia_vista';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Licenses').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');	
			$this->documento->script('datetimepicker', '/libs/datepicker/');
			$this->documento->stylesheet('datetimepicker', '/libs/datepicker/');		
			$this->esquema = 'academico/licencia-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function reportelicencias()
	{
		try {
			global $aplicacion;
			// var_dump('hola');

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Licencias'), true);
			$user=$this->usuarioAct;
			$this->user = $this->usuarioAct;

			$this->esquema = 'academico/reportelicencias';
	        $this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		
	}

}
?>