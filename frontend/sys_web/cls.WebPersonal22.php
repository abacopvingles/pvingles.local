<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-10-2018 
 * @copyright	Copyright (C) 26-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_apoderado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_educacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_experiencialaboral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_referencia', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_metas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_record', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebPersonal extends JrWeb
{
	private $oNegPersonal;
	private $oNegGeneral;
	private $oNegUbigeo;
	private $oNegUgel;
	private $oNegRoles;
	private $oNegAcad_matricula;
	private $oNegAcad_grupoaula;
	private $oNegHorariogrupoaula;
	private $oNegAcad_grupoauladetalle;	
	private $oNegAcad_curso;
	private $oNegPersona_rol;
	private $oNegPerRecord;	
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegPerApoderado = new NegPersona_apoderado;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerEducacion = new NegPersona_educacion;
		$this->oNegPerExplaboral = new NegPersona_experiencialaboral;
		$this->oNegPerReferencia = new NegPersona_referencia;
		$this->oNegPerMetas = new NegPersona_metas;
		$this->oNegPerRecord = new NegPersona_record;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegUgel = new NegUgel;
		$this->oNegRoles = new NegRoles;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegGrupoaula = new NegAcad_grupoaula;
		$this->oNegGrupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegHorariogrupoaula = new NegAcad_horariogrupodetalle;
		$this->oNegCurso = new NegAcad_curso;
		$this->oNegPersona_rol = new NegPersona_rol;		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$usuarioAct = NegSesion::getUsuario();			
			$this->idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			if(!empty($_REQUEST["idproyecto"]))$this->idproyecto=$_REQUEST["idproyecto"];	
			//if(!NegSesion::tiene_acceso('Personal', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["tipousuario"])&&@$_REQUEST["tipousuario"]!='')$filtros["tipousuario"]=$_REQUEST["tipousuario"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			
			$this->datos=$this->oNegPersonal->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Personal'), true);
			$this->esquema = 'personal-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Personal', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Personal', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegPersonal->idpersona = @$_GET['id'];
			$this->datos = $this->oNegPersonal->dataPersonal;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Personal').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'personal-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Personal', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idpersona"])&&@$_REQUEST["idpersona"]!='')$filtros["idpersona"]=$_REQUEST["idpersona"];
			if(isset($_REQUEST["tipodoc"])&&@$_REQUEST["tipodoc"]!='')$filtros["tipodoc"]=$_REQUEST["tipodoc"];
			if(isset($_REQUEST["dni"])&&@$_REQUEST["dni"]!='')$filtros["dni"]=$_REQUEST["dni"];
			if(isset($_REQUEST["ape_paterno"])&&@$_REQUEST["ape_paterno"]!='')$filtros["ape_paterno"]=$_REQUEST["ape_paterno"];
			if(isset($_REQUEST["ape_materno"])&&@$_REQUEST["ape_materno"]!='')$filtros["ape_materno"]=$_REQUEST["ape_materno"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fechanac"])&&@$_REQUEST["fechanac"]!='')$filtros["fechanac"]=$_REQUEST["fechanac"];
			if(isset($_REQUEST["sexo"])&&@$_REQUEST["sexo"]!='')$filtros["sexo"]=$_REQUEST["sexo"];
			if(isset($_REQUEST["estado_civil"])&&@$_REQUEST["estado_civil"]!='')$filtros["estado_civil"]=$_REQUEST["estado_civil"];
			if(isset($_REQUEST["ubigeo"])&&@$_REQUEST["ubigeo"]!='')$filtros["ubigeo"]=$_REQUEST["ubigeo"];
			if(isset($_REQUEST["urbanizacion"])&&@$_REQUEST["urbanizacion"]!='')$filtros["urbanizacion"]=$_REQUEST["urbanizacion"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["celular"])&&@$_REQUEST["celular"]!='')$filtros["celular"]=$_REQUEST["celular"];
			if(isset($_REQUEST["email"])&&@$_REQUEST["email"]!='')$filtros["email"]=$_REQUEST["email"];
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["regusuario"])&&@$_REQUEST["regusuario"]!='')$filtros["regusuario"]=$_REQUEST["regusuario"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["token"])&&@$_REQUEST["token"]!='')$filtros["token"]=$_REQUEST["token"];
			if(isset($_REQUEST["rol"])&&@$_REQUEST["rol"]!='')$filtros["rol"]=$_REQUEST["rol"];
			if(isset($_REQUEST["foto"])&&@$_REQUEST["foto"]!='')$filtros["foto"]=$_REQUEST["foto"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			if(isset($_REQUEST["idioma"])&&@$_REQUEST["idioma"]!='')$filtros["idioma"]=$_REQUEST["idioma"];
			if(isset($_REQUEST["tipousuario"])&&@$_REQUEST["tipousuario"]!='')$filtros["tipousuario"]=$_REQUEST["tipousuario"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
						
			$this->datos=$this->oNegPersonal->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarPersonal(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdpersona)) {
				$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
				$this->oNegPersonal->tipodoc=@$txtTipodoc;
					$this->oNegPersonal->dni=@$txtDni;
					$this->oNegPersonal->ape_paterno=@$txtApe_paterno;
					$this->oNegPersonal->ape_materno=@$txtApe_materno;
					$this->oNegPersonal->nombre=@$txtNombre;
					$this->oNegPersonal->fechanac=@$txtFechanac;
					$this->oNegPersonal->sexo=@$txtSexo;
					$this->oNegPersonal->estado_civil=@$txtEstado_civil;
					$this->oNegPersonal->ubigeo=@$txtUbigeo;
					$this->oNegPersonal->urbanizacion=@$txtUrbanizacion;
					$this->oNegPersonal->direccion=@$txtDireccion;
					$this->oNegPersonal->telefono=@$txtTelefono;
					$this->oNegPersonal->celular=@$txtCelular;
					$this->oNegPersonal->email=@$txtEmail;
					$this->oNegPersonal->idugel=@$txtIdugel;
					$this->oNegPersonal->regusuario=@$txtRegusuario;
					$this->oNegPersonal->regfecha=@$txtRegfecha;
					$this->oNegPersonal->usuario=@$txtUsuario;
					$this->oNegPersonal->clave=@$txtClave;
					$this->oNegPersonal->token=@$txtToken;
					$this->oNegPersonal->rol=@$txtRol;
					$this->oNegPersonal->foto=@$txtFoto;
					$this->oNegPersonal->estado=@$txtEstado;
					$this->oNegPersonal->situacion=@$txtSituacion;
					$this->oNegPersonal->idioma=@$txtIdioma;
					$this->oNegPersonal->tipousuario=@$txtTipousuario;
					$this->oNegPersonal->idlocal=@$txtIdlocal;
					
            if($accion=='_add') {
            	$res=$this->oNegPersonal->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegPersonal->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Personal')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSavePersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdpersona'])) {
					$this->oNegPersonal->idpersona = $frm['pkIdpersona'];
				}
				
				$this->oNegPersonal->tipodoc=@$frm["txtTipodoc"];
					$this->oNegPersonal->dni=@$frm["txtDni"];
					$this->oNegPersonal->ape_paterno=@$frm["txtApe_paterno"];
					$this->oNegPersonal->ape_materno=@$frm["txtApe_materno"];
					$this->oNegPersonal->nombre=@$frm["txtNombre"];
					$this->oNegPersonal->fechanac=@$frm["txtFechanac"];
					$this->oNegPersonal->sexo=@$frm["txtSexo"];
					$this->oNegPersonal->estado_civil=@$frm["txtEstado_civil"];
					$this->oNegPersonal->ubigeo=@$frm["txtUbigeo"];
					$this->oNegPersonal->urbanizacion=@$frm["txtUrbanizacion"];
					$this->oNegPersonal->direccion=@$frm["txtDireccion"];
					$this->oNegPersonal->telefono=@$frm["txtTelefono"];
					$this->oNegPersonal->celular=@$frm["txtCelular"];
					$this->oNegPersonal->email=@$frm["txtEmail"];
					$this->oNegPersonal->idugel=@$frm["txtIdugel"];
					$this->oNegPersonal->regusuario=@$frm["txtRegusuario"];
					$this->oNegPersonal->regfecha=@$frm["txtRegfecha"];
					$this->oNegPersonal->usuario=@$frm["txtUsuario"];
					$this->oNegPersonal->clave=@$frm["txtClave"];
					$this->oNegPersonal->token=@$frm["txtToken"];
					$this->oNegPersonal->rol=@$frm["txtRol"];
					$this->oNegPersonal->foto=@$frm["txtFoto"];
					$this->oNegPersonal->estado=@$frm["txtEstado"];
					$this->oNegPersonal->situacion=@$frm["txtSituacion"];
					$this->oNegPersonal->idioma=@$frm["txtIdioma"];
					$this->oNegPersonal->tipousuario=@$frm["txtTipousuario"];
					$this->oNegPersonal->idlocal=@$frm["txtIdlocal"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegPersonal->agregar();
					}else{
									    $res=$this->oNegPersonal->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegPersonal->idpersona);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDPersonal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$this->datos = $this->oNegPersonal->dataPersonal;
				$res=$this->oNegPersonal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegPersonal->__set('idpersona', $pk);
				$res=$this->oNegPersonal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegPersonal->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	
	public function actualizartabla(){
		
		//$this->oNegPersonal->actualizartabla("acad_curso","idusuario");
		//$this->oNegPersonal->actualizartabla("acad_grupoauladetalle","iddocente");
		//$this->oNegPersonal->actualizartabla("acad_matricula","idalumno");
		//$this->oNegPersonal->actualizartabla("acad_matricula","idusuario");
		//$this->oNegPersonal->actualizartabla("actividad_alumno","idalumno");
		//$this->oNegPersonal->actualizartabla("historial_sesion","idusuario");
		//$this->oNegPersonal->actualizartabla("bitacora_alumno_smartbook","idusuario");
		//$this->oNegPersonal->actualizartabla("alumno_logro","id_alumno");
		//$this->oNegPersonal->actualizartabla("notas_alumno","identificador");
		//$this->oNegPersonal->actualizartabla("notas_archivo","iddocente");
		//$this->oNegPersonal->actualizartabla("recursos","idpersonal");
		//$this->oNegPersonal->actualizartabla("notas_quiz","idalumno");
		//UPDATE `abacoedu_smartlearn`.`bitacora_alumno_smartbook` SET `idcurso` = '35' WHERE idcurso=2 //Pasar el historial del curso
	}     
}