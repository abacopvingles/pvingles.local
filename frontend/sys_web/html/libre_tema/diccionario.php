<div class="row" id="diccionario">
    <div class="col-md-12">
	    <div class="panel" >      
	      	<div class="panel-body">
		        <form id="frmdiccionario">
		        	<div class="col-xs-12 col-sm-12 col-md-12 form-group">
		              <label><?php echo  ucfirst(JrTexto::_("Language"))?></label>
		                <div class="input-group">
		                  <select class="form-control" id="lenguaje">
		                  	<option value="1">English - Spanish</option>
		                  	<option value="2" disabled>Spanish - English</option>
		                  </select>
		                </div>
		            </div>
		          	<div class="col-xs-12 col-sm-12 col-md-12 form-group">
		              <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
		                <div class="input-group">
		                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
		                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
		                </div>
		            </div>
		            
		        </form>
	        </div>
      	</div>
    </div>
    <div class="row" id="resultado">
    </div>	
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('.btnbuscar').on('click', function(){
		var lenguaje = $('#lenguaje').val();
		if (lenguaje ==1) {
			var valor = $('#texto').val();
			var parametros = {"texto":valor,"lenguaje":lenguaje};
			$.ajax({
				url: './../static/media/diccionario/prueba.php',
				type: 'post',
				data: parametros,
				success:function(resp){
					console.log(resp);
					for(var key in resp) {
						console.log(key);
	                    // $('#msgid').append(key);
	                    // $('#msgid').append('=' + data[key] + '<br />');
	                }
					// var valor = resp;
					// console.log(valor);
				}
			});
		}
		
	});
});
</script>