<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style type="text/css">
    iframe#url-externa { background: white; border: 0; width: 100%; height: calc(100vh - 60px);  }
</style>

<div class="row" id="alum-libre_palabras">
    <div class="col-xs-12 text-right">
        <small><strong><?php echo JrTexto::_("Source") ?>: </strong><?php echo @$this->origen; ?></small>
    </div>
    <div class="col-xs-12">
        <iframe src="<?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $this->datos[0]["url_iframe"]); ?>" id="url-externa">
            <div class="text-center"><i class="fa fa-cog"></i> <?php echo JrTexto::_("Loading") ?>...</div>
        </iframe>
    </div>
</div>