<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style type="text/css">
    .nombreworkbook{
        color: #d20d0d;
    text-shadow: black 0.1em 0.05em 0.1em;
    font-size: 2.5em;
    font-weight: bold;
  
    }
    a.aworkbook{
        font-size:1.5em;
        color: #0d2365;
        text-shadow: #2952a9 0.1em 0.1em 0.2em;
        text-align:center;
    }
    a.hover{
        color: white; 
        text-shadow: black 0.1em 0.1em 0.2em;
    }
    a.aworkbook p{
        text-decoration: underline;
    }
    .cdcircle{
        position: absolute;
        left: calc(50% - 2rem);
        width: 4rem;
        height: 4rem;
        bottom: calc(50% - 1rem);
        background-color: #fffbfb;
        font-size: 2em;
        text-align: center;
        border-radius: 50%;
    }
    .panel{
        margin-bottom: 1em !important;
    }
    .panel-heading{
        padding:0px;
    }
    
    .aworkbookcd{
        background-image: url('');
        background-size: 100%;
       /* transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -webkit-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -moz-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -o-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -ms-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);*/
    }
</style>

<div class="row" id="alum-libre_tipo">
<?php 
$ipanel=['panel-primary','panel-success','panel-warning','panel-danger','panel-info'];
if(!empty($this->cursos)) { 
    $i=0;
    
    foreach ($this->cursos as $t) {
        $isgray=false;
        $btnsubir='';
       
        if($this->idrol==3){
            if ($t['idcurso']==4 || $t['idcurso']==5 ) {
                $isgray='cursor: not-allowed;filter: grayscale(100%);';
            }
            // $isgray='cursor: not-allowed;filter: grayscale(100%);';
            // if(!empty($this->cursosMatric))
            // foreach ($this->cursosMatric as $c) {
            //     if ($t['idcurso']===$c['idcurso']) {
            //         $isgray='';
            //         break;
            //     }
            // }
        }else{
            if ($this->idrol==2) {
                if ($t['idcurso']==4 || $t['idcurso']==5) {
                    $isgray='cursor: not-allowed;filter: grayscale(100%);display:none;';
                }
  
            }
        }
        $btnsubirmp3='';
        if($this->idrol==1){
            $btnsubirmp3='<a  href="'.$this->documento->getUrlBase().'/tema_libre/contenidomp3/?audio='.$t['nombre'].'" class="btn btn-danger btn-sm"><i class="fa fa-upload"></i> '.JrTexto::_('Upload').'</a>';
            $btnsubir='<a class="btn btn-danger btn-sm btnsubirworkbook" data-nombre="'.$t['nombre'].'" data-tipofile="pdf" data-dirmedia="woorkbooks/pdf/" ><i class="fa fa-upload"></i> '.JrTexto::_('Upload').'</a>';
        }else{
            if($t['idcurso']>36) continue;
        }
        
        ?>
        <div class="col-md-6 col-sm-12" >
            <div class="panel <?php echo $ipanel[floor($i/2)];  ?>" style='<?php echo !empty($isgray)?$isgray:''; ?>'>
                <div class="text-center nombreworkbook panel-heading"><?php echo $t['nombre']; ?></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 btn-panel-container panelcont-xs panelconainer text-center" >
                            <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
                                <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/woorkbooks/pdf/'.$t['nombre'].".pdf";?>' >

                                <a  class="aworkbook hvr-float-shadow">
                                    <input type='image' src="<?php echo $this->documento->getUrlBase().$t["imagen"];?>" width="100%" class="img img-resonsive img-thumbnail" style="min-height: 100px; <?php echo !empty($isgray)?$isgray:''; ?>">
                                    <p><?php echo JrTexto::_('Workbooks'); ?></p>
                                </a>
                            </form> 
                            <?php echo $btnsubir; ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 btn-panel-container panelcont-xs text-center"  style="position:relative " >
                            <a href="<?php echo empty($isgray)?$this->documento->getUrlBase().'/tema_libre/contenidomp3alumno/?audio='.$t['nombre']:'#';?>" class="aworkbook hvr-float-shadow aworkbookcd" >
                                <div style="position:relative">
                                    <img src="<?php echo $this->documento->getUrlBase().$t["imagen"];?>" width="100%" height="100%" class="img img-resonsive img-thumbnail img-circle " style="min-height: 100px; <?php echo !empty($isgray)?$isgray:''; ?>">
                                    <img src=".././static/media/woorkbooks/resources/img/B2.png" width="100%" height="100%" class=" " style="min-height: 100px; opacity:0.3; position:absolute; left:0px; top:0px; <?php echo !empty($isgray)?$isgray:''; ?>">
                                </div>
                                <!--span class="cdcircle img-circle" style></span-->
                                <p><?php echo JrTexto::_('Audios'); ?></p>
                            </a>
                            <?php echo $btnsubirmp3;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php 
    }
} ?>
</div>
<script>
$(document).ready(function(){    
    $('.btnsubirworkbook').click(function(ev){
        __syssubirfile($(this));
    })
})
</script>