<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style type="text/css">
    .nombreworkbook{
        color: #d20d0d;
    text-shadow: black 0.1em 0.05em 0.1em;
    font-size: 2.5em;
    font-weight: bold;
  
    }
    a.aworkbook{
        font-size:1.5em;
        color: #0d2365;
        text-shadow: #2952a9 0.1em 0.1em 0.2em;
        text-align:center;
    }
    a.hover{
        color: white; 
        text-shadow: black 0.1em 0.1em 0.2em;
    }
    a.aworkbook p{
        text-decoration: underline;
    }
    .cdcircle{
        position: absolute;
        left: calc(50% - 2rem);
        width: 4rem;
        height: 4rem;
        bottom: calc(50% - 1rem);
        background-color: #fffbfb;
        font-size: 2em;
        text-align: center;
        border-radius: 50%;
    }
    .panel{
        margin-bottom: 1em !important;
    }
    .panel-heading{
        padding:0px;
    }
    
    .aworkbookcd{
        background-image: url('');
        background-size: 100%;
    }
    .img-thumbnail {
    	max-width: initial;
    }
</style>

<div class="row" id="ebooks">
<?php 
$ipanel=['panel-primary','panel-success','panel-warning','panel-danger','panel-info'];
if($this->idrol==2){
		?>
		<div class="col-md-12 col-sm-12" >
	        <div class="panel panel-primary">
	            <div class="text-center nombreworkbook panel-heading"><?php echo JrTexto::_('English Teaching Methodology Texts')?></div>
	            <div class="panel-body">
	                <div class="row">
	                    <div class="col-xs-12 col-sm-12 col-md-12 btn-panel-container panelcont-xs panelcontainer text-center" >
	                        <div class="row">
		                		<?php
			                	for ($i=0; $i <7 ; $i++) { 
		                			?>
				                	<div class="col-xs-12 col-sm-4 col-md-4">
				                		<a href="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf/?link='.$this->documento->getUrlStatic().'/media/ebooks/pdf/M'.($i+1).'.pdf';?>" class="aworkbook hvr-float-shadow">
					                        <img src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/M'.($i+1)?>.jpg" width="200px"  class="img img-resonsive img-thumbnail" style="max-height: 250px;">
					                        <p><?php echo ($i > 4) ? JrTexto::_('Methodology Text').JrTexto::_(' '.($i+1)).' (* '.JrTexto::_('New').')' : JrTexto::_('Methodology Text').JrTexto::_(' '.($i+1)); ?></p>
					                    </a>
				                	</div>
				                    <?php
			                	
			                	}  
			                	?>
			                	<div class="col-xs-12 col-sm-4 col-md-4">
			                		<a href="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf/?link='.$this->documento->getUrlStatic().'/media/ebooks/pdf/I1'.'.pdf';?>" class="aworkbook hvr-float-shadow">
				                        <img src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/I1'?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 250px;">
				                        <p><?php echo JrTexto::_('Grammar Reference Book').JrTexto::_(' 1'); ?></p>
				                    </a>
			                	</div>
			                	<div class="col-xs-12 col-sm-4 col-md-4">
			                		<a href="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf/?link='.$this->documento->getUrlStatic().'/media/ebooks/pdf/I2'.'.pdf';?>" class="aworkbook hvr-float-shadow">
				                        <img src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/I2'?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 250px;">
				                        <p><?php echo JrTexto::_('Grammar Reference Book').JrTexto::_(' 2'); ?></p>
				                    </a>
			                	</div>
			                	<div class="col-xs-12 col-sm-4 col-md-4">
			                		<a href="#" data-source="<?php echo $this->documento->getUrlSitio()?>/sidebar_pages/diccionario2" class="aworkbook hvr-float-shadow slide-sidebar-right">
				                        <img src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/D1'?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 250px;">
				                        <p><?php echo JrTexto::_('English Monolingual Dictionary'); ?></p>
				                    </a>
			                	</div>
		                	</div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		<?php  
}else{
	if($this->idrol==3){
		
		?>
		
		<div class="col-md-12 col-sm-12" >
	        <div class="panel panel-primary">
	            <div class="text-center nombreworkbook panel-heading"><?php echo JrTexto::_('e-books')?></div>
	            <div class="panel-body">
	            	<div class="">
						<ul class="nav nav-tabs">
						    <li class="active"><a data-toggle="tab" href="#A1">A1</a></li>
						    <li><a data-toggle="tab" href="#A2">A2</a></li>
						    <li><a data-toggle="tab" href="#B1">B1</a></li>
						</ul>
						<div class="tab-content">
						    <div id="A1" class="tab-pane fade in active">
						    	<div class="row">
					                <div class="col-xs-12 col-sm-12 col-md-12 btn-panel-container panelcont-xs panelconainer text-center" >
					                	<div class="row">
					                		<?php
						                	for ($i=0; $i <5 ; $i++) { 
						                		if($i == 1 || $i==3){
						                	?>
						                	<div class="col-xs-12 col-sm-3 col-md-3">
							                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
						                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A1/A1-'.($i+1).'.pdf';?>' >

						                            <a href class="aworkbook hvr-float-shadow">
						                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A1/A1-'.($i+1)?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 200px;">
						                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A1 -'.($i+1)); ?></p>
						                            </a>
						                        </form>
						                	</div>
						                    <?php
						                    	}else{
						                    		if ($i==2) {
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 0px;">
											                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A1/A1-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A1/A1-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A1 -'.($i+1)); ?></p>
										                            </a>
										                        </form>
										                	</div>
							                    				
							                    		<?php
						                    		}else{
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2">
											                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A1/A1-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A1/A1-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A1 -'.($i+1)); ?></p>
										                            </a>
										                        </form>
										                	</div>
							                    				
							                    		<?php
						                    		}
						                    		
						                    	}
						                	}  
						                	?>
					                	</div>
					                </div>
					            </div>
						    </div>
						    <div id="A2" class="tab-pane fade">
						   		<div class="row">
					                <div class="col-xs-12 col-sm-12 col-md-12 btn-panel-container panelcont-xs panelconainer text-center" >
					                	<div class="row">
					                		<?php
						                	for ($i=0; $i <5 ; $i++) { 
												if($i == 1 || $i==3){	
						                	?>
						                	<div class="col-xs-12 col-sm-3 col-md-3">
							                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
						                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A2/A2-'.($i+1).'.pdf';?>' >

						                            <a href class="aworkbook hvr-float-shadow">
						                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A2/A2-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
						                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A2 -'.($i+1)); ?></p>
						                            </a>
						                        </form>
						                	</div>
						                    <?php
						                    	}else{
						                    		if ($i==2) {
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 0px;">
											                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A2/A2-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A2/A2-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A2 -'.($i+1)); ?></p>
										                            </a>
										                        </form>
										                	</div>
							                    				
							                    		<?php
						                    		}else{
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2">
											                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/A2/A2-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                            	<input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/A2/A2-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' A2 -'.($i+1)); ?></p>
										                            </a>
										                        </form>
										                	</div>
							                    				
							                    		<?php
						                    		}
						                    		
						                    	}
						                	}  
						                	?>
					                	</div>
					                </div>
					            </div>
						    </div>
						    <div id="B1" class="tab-pane fade">
								<div class="row">
					                <div class="col-xs-12 col-sm-12 col-md-12 btn-panel-container panelcont-xs panelconainer text-center" >
					                	<div class="row">
					                		<?php
						                	for ($i=0; $i <5 ; $i++) { 
												if($i == 1 || $i==3){	
						                	?>
						                	<div class="col-xs-12 col-sm-3 col-md-3">
							                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
						                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/B1/B1-'.($i+1).'.pdf';?>' >

						                            <a href class="aworkbook hvr-float-shadow">
						                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/B1/B1-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
						                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' B1 -'.($i+1)); ?></p>
						                            </a>
						                        </form>
						                	</div>
						                    <?php
						                    	}else{
						                    		if ($i==2) {
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2" style="padding-left: 0px;">
											                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/B1/B1-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                            	<input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/B1/B1-'.($i+1)?>.jpg"   class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' B1 -'.($i+1)); ?></p>
										                            </a>
										                        </form>
										                	</div>
							                    				
							                    		<?php
						                    		}else{
						                    			?>
							                    			<div class="col-xs-12 col-sm-2 col-md-2">
							                    				<form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
										                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/B1/B1-'.($i+1).'.pdf';?>' >

										                            <a href class="aworkbook hvr-float-shadow">
										                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/B1/B1-'.($i+1)?>.jpg"  class="img img-resonsive img-thumbnail" style="max-height: 200px;">
										                                <p><?php echo JrTexto::_('e-book').JrTexto::_(' B1 -'.($i+1)); ?></p>
										                            </a>
										                        </form> 
										                	</div>
							                    				
							                    		<?php
						                    		}
						                    		
						                    	}
						                	}  
						                	?>
					                	</div>
					                </div>
					            </div>						    	  
						    </div>
						</div>
					</div>
	               
	            </div>
	        </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	        <div class="panel panel-primary">
	            <div class="text-center nombreworkbook panel-heading"><?php echo JrTexto::_('Grammar Rules'); ?></div>
	            <div class="panel-body">
	               <div class="col-xs-12 col-sm-6 col-md-6" align="center">
	                    <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
                            <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/ebooks/pdf/I3'.'.pdf';?>' >

                            <a href class="aworkbook hvr-float-shadow">
                                <input type='image' src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/I3'?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 200px;">
                                <p><?php echo JrTexto::_('Grammar Reference Book'); ?></p>
                            </a>
                        </form> 
                	</div>
                	<div class="col-xs-12 col-sm-6 col-md-6" align="center">
                		<a href="#" data-source="<?php echo $this->documento->getUrlSitio()?>/sidebar_pages/diccionario1" class="aworkbook hvr-float-shadow slide-sidebar-right">
	                        <img src="<?php echo $this->documento->getUrlStatic().'/media/ebooks/image/D2'?>.jpg" class="img img-resonsive img-thumbnail" style="max-height: 200px;">
	                        <p><?php echo JrTexto::_('English-Spanish Dictionary'); ?></p>
	                    </a>
                	</div>
	            </div>
	        </div>
	    </div>
		<?php 
	}

}?>
</div>