
<!DOCTYPE html>
<html>
    <head>
        <title>MP3</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta charset="utf-8"/>
        <!-- <link href="../../resources/plantilla/css/one-page-wonder.min.css" rel="stylesheet"> -->
        <link href="../../static/media/woorkbooks/resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../../static/media/woorkbooks/resources/css/fileinput.min.css" rel="stylesheet"/>
        <!-- <link rel="stylesheet" type="text/css" href="../../static/media/woorkbooks/resources/DataTables/datatables.min.css"/> -->
        <link rel="stylesheet" type="text/css" href="../../static/libs/datatable1.10/media/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../static/libs/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css"/>
        <!-- <link href="../../static/libs/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" /> -->
        <script src="../../static/tema/js/bootstrap.min.js"></script>
        <style type="text/css">
            .navbar-brand {
                padding: 0;
                position: absolute;
            }
        </style>
    </head>
    <body>
        <div class="container p-5">
            <div class="row">
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Importar MP3</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <form enctype="multipart/form-data">
                                                <div class="file-loading">
                                                    <input id="file-es" name="file-es[]" type="file" multiple>
                                                </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div style="margin-top: 20%">
                                <div class="col-md-6 col-md-offset-3">  
                                    <b> REPRODUCTOR MP3 </b>
                                </div>
                                <div class="col-md-6 col-md-offset-3">  
                                   <audio id='audio' preload="auto" controls> 
                                        <!-- <source type="audio/mpeg"> -->
                                    </audio>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <br>
                <div class="col-sm-6">
                    <div class="row" id="recursos_audios">
                                        <span style="font-weight: bold;">LISTA DE AUDIOS:</span>
                                        <!-- <div class="panel panel-default"> -->
                                        <?php
                                            $carpeta = $_REQUEST["audio"];
                                            
                                            //$ruta = URL_RAIZ.'static/audios/'.$carpeta;
                                            $ruta = 'static/media/woorkbooks/audios/'.$carpeta;
                                            if (is_dir($ruta)){
                                                // Abre un gestor de directorios para la ruta indicada
                                                echo '<div class="panel">';
                                                echo '<div class="panel-body">';
                                                $gestor = opendir($ruta);
                                                echo '<table id="example" class="table table-striped table-responsive" style="width:100%;">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $cont = 1;
                                                // Recorre todos los elementos del directorio
                                                while (($archivo = readdir($gestor)) !== false)  {
                                                        
                                                    $extension = strtolower(substr($archivo, -3));
                                                    if ($extension == 'mp3') {
                                                        $ruta_completa = $ruta . "/" . $archivo;

                                                        // Se muestran todos los archivos y carpetas excepto "." y ".."
                                                        if ($archivo != "." && $archivo != "..") {
                                                            // Si es un directorio se recorre recursivamente
                                                            echo '<tr>';
                                                            echo '<td>';
                                                            echo $cont++;
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                                echo $archivo;
                                                            } else {
                                                                echo $archivo;
                                                            }
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                                echo "<button type='button' class='btn btn-default' id='mp3' value='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</button>";
                                                            } else {
                                                                echo "<button type='button' class='btn btn-default' id='mp3' value='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</button>";
                                                            }
                                                            echo '</td>';
                                                            echo '</tr>';
                                                        }
                                                    }           
                                                }
                                                if ($cont ==0) {
                                                    echo '<tr>';
                                                    echo '<td colspan="3">';
                                                    echo 'No hay datos en el directorio';
                                                    echo '</td>';
                                                    echo '</tr>;';
                                                }
                                                echo '</tbody>';
                                                echo '<tfoot>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</tfoot>';

                                                echo '</table>';
                                                echo '</div>';
                                                echo '</div>';
                                                // Cierra el gestor de directorios
                                                closedir($gestor);
                                            } else {
                                                echo "No es una ruta de directorio valida<br/>";
                                            }
                                            
                                            ?>
                                        <!-- </div> -->
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- <script src="../../static/media/woorkbooks/resources/plantilla/vendor/jquery/jquery.min.js"></script> -->
        <!-- <script src="../../resources/plantilla/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
        <script type="text/javascript" src="../../static/media/woorkbooks/resources/js/jquery.js"></script>
        <script type="text/javascript" src="../../static/media/woorkbooks/resources/js/fileinput.min.js"></script>
        <script type="text/javascript" src="../../static/media/woorkbooks/resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../static/media/woorkbooks/resources/js/locales/es.js"></script>
        <script type="text/javascript" src="../../static/libs/datatable1.10/media/js/jquery.dataTables.min.js"></script>
        <!-- <script type="text/javascript" src="../../static/libs/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js"></script> -->
        <script>
        var myVar;
            var ruta = '<?php $carpeta = $_REQUEST["audio"]; echo $carpeta;?>'; 
            console.log(ruta);    
        $('#file-es').fileinput({
            language: 'es',
            uploadUrl: '../../static/media/woorkbooks/includes/controller/importar-mp3.php?audios='+ruta,
            allowedFileExtensions: ['mp3'],
            uploadAsync: false,
            overwriteInitial: false,
            minFileCount: 1,
            maxFileCount: 5,
            maxFileSize: 1048576,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the markup
        });
        clearTimeout(myVar);
       $ ( '#file-es' ). on ( 'filebatchuploadsuccess' , function ( event , data ) { 
            myVar = setTimeout(function(){ location.reload(); }, 1000);
            
        });
       $(document).ready(function() {
            $('#example').dataTable( {
              "pageLength": 8,
            } );
            $('.dataTables_length').hide();
            var url = '';
             $(document).on('click','#mp3',function(event) {

                var url = $(this).val();
                console.log(url);
                $('#audio').attr("src",'../../'+url);
                 $('#audio')[0].play();
            });
            

            
        } );
        </script>
    </body>
        
</html>