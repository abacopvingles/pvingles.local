<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style>
    .panel-title { font-size: 28px; }
    audio::-webkit-media-controls-timeline,
    audio::-webkit-media-controls-current-time-display,
    audio::-webkit-media-controls-mute-button,
    audio::-internal-media-controls-download-button,
    audio::-internal-media-controls-overflow-button{ display: none !important; }
</style>

<div class="row" id="palabras_table-frm">
    <input type="hidden" name="hIdTema" id="hIdTema" value="<?php echo $this->temas[0]['idtema']; ?>">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"><?php echo $this->temas[0]['tipo_nombre'].': '.$this->temas[0]['nombre']; ?></h2>
            </div>
            <div class="panel-body">
                <div id="edicion" class="<?php echo !empty($this->palabras)?'hidden':''; ?>">
                    <div class="col-xs-12 text-center">
                        <buttton class="btn btn-default btn-cancelar"><i class="fa fa-times"></i> <?php echo ucfirst(JrTexto::_("Cancel")); ?></buttton>
                        <buttton class="btn btn-success btn-guardar"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_("Save")); ?></buttton> <br> <br>
                    </div>
                    <div class="col-xs-12">
                        <textarea name="txtEditor" id="txtEditor"></textarea>
                    </div>
                </div>
                <div id="vista_previa" class="<?php echo empty($this->palabras)?'hidden':''; ?>">
                    <div class="col-xs-12 text-center">
                        <buttton class="btn btn-primary btn-editar"><i class="fa fa-pencil"></i> <?php echo ucfirst(JrTexto::_("Edit")); ?></buttton> <br> <br>
                    </div>
                    <div class="col-xs-12" id="contenido">
                        <?php if(!empty($this->palabras)){ ?>
                        <center><table border="1">
                            <tbody>
                                <?php $i=0; foreach ($this->palabras as $idagrupacion=>$arrPalabras) { ?>
                                <tr data-idagrupacion="<?php echo $idagrupacion; ?>">
                                    <?php foreach ($arrPalabras as $j=>$p) { ?>
                                    <td data-idagrupacion="<?php echo $p['idagrupacion']; ?>" data-idpalabra="<?php echo $p['idpalabra']; ?>" data-idcorrelativo="<?php echo $i.$j; ?>" >
                                        <p><?php echo $p["palabra"]; ?></p>
                                        <?php if(!empty($p["audio"])){
                                            $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $p["audio"]);
                                            $arrNombre = explode('/' , $src);
                                            $alt = $arrNombre[ count($arrNombre)-1 ];
                                        ?>
                                        <audio src="<?php echo $src; ?>" data-alt="<?php echo $alt; ?>" class="mce-object-audio" data-mce-object="audio" style="width: 135px;" controls="true" controlsList="nofullscreen nodownload noremoteplayback"></audio>
                                        <?php } ?>
                                    </td>
                                    <?php $i++; } ?>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table></center>
                        <?php } ?>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
var isPalabrasEmpty = <?php echo empty($this->palabras)?'true':'false'; ?>;

var initEditor = function() {
    tinymce.init({
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        convert_newlines_to_brs : true,
        menubar: false,
        statusbar: false,
        verify_html : false,
        content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
        selector: '#txtEditor',
        height: 350,
        paste_auto_cleanup_on_paste : true,
        paste_word_valid_elements: "table, thead, tbody, tfooter, tr, td, th",
        /*paste_preprocess : function(pl, o) {
           var html='<div>'+o.content+'</div>';
           var txt =$(html).text();
           o.content = txt;
        },
        paste_postprocess : function(pl, o) {
            o.node.innerHTML = o.node.innerHTML;
            o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
        },*/
        plugins:["chingoaudio paste table" ], // textcolor lists advlist
        toolbar: ' undo redo  | removeformat table | chingoaudio ', // styleselect fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify |  bold italic underline | bullist numlist outdent indent
        advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
    });
};

var formatoText2Tabla = function (text) {
    var $html = $('<div class="temp-wrapper">'+text+'</div>');
    var index = 0;
    $html.find('tbody tr').each(function(i, tr) {
        $(tr).find('td').each(function(j, td) {
            let $celda = $(td);
            let $img_audio = $celda.find('img.mce-object-audio');
            if($img_audio.length>0){
                /* Reemplazar <img> por <audio> */
                let $p = $img_audio.closest('p');
                var src = $img_audio.attr('data-src');
                var alt = $img_audio.attr('alt');
                $celda.append('<audio src="'+src+'" data-alt="'+alt+'" class="mce-object-audio" data-mce-object="audio" style="width: 135px;" controls="true" controlsList="nofullscreen nodownload noremoteplayback"></audio>');
                $img_audio.remove();
                if( $p.text().trim()=='' ){ $p.remove(); }
            }
            $celda.attr('data-idcorrelativo', i.toString()+j.toString());
        });

        /* agrega 'idagrupacion' a <td>'s de un mismo <tr> */
        let idAgrupacion = $(tr).attr('data-idagrupacion');
        ++index;
        if(typeof idAgrupacion=='undefined') {
            let rand = Math.floor(Math.random()*Math.pow(10,10));
            idAgrupacion = rand.toString()+index.toString();
        }
        $(tr).attr('data-idagrupacion', idAgrupacion); 
        $(tr).find('td').attr('data-idagrupacion', idAgrupacion);
    });
    $html.find('table').attr('border',1);
    text = $html.html();
    return text;  
};

var formatoTexto = function (text) {
    var $html = $('<div class="temp-wrapper">'+text+'</div>');
    $html.find('audio.mce-object-audio').each(function(k, audio) {
        /* Reemplazar <audio> por <img> */
        let $audio = $(audio);
        let $celda = $audio.closest('td');
        let $p = $audio.closest('p');
        var src = $audio.attr('src');
        var alt = $audio.attr('data-alt');
        $celda.append('<img data-mce-object="audio" controls="true" class="mce-object-audio" data-src="'+src+'" alt="'+alt+'" data-mce-selected="1">');
        $audio.remove();
        if( $p.text().trim()=='' ){ $p.remove(); }
    });
    text = $html.html();

    return text; 
};

var tablaToArray = function () {
    var array = [];
    var $tbl = $("#vista_previa #contenido table");
    $tbl.find('tbody td').each(function(i, td) {
        let $celda = $(td);
        let node = {
            "idpalabra" : $celda.attr('data-idpalabra'),
            "idtema" : $('#hIdTema').val(),
            "palabra" : $celda.find('p').text().trim(),
            "audio" : $celda.find('audio').attr('src') || null,
            "idagrupacion" : $celda.attr('data-idagrupacion'),
            "idcorrelativo" : $celda.attr('data-idcorrelativo'),
        }
        array.push(node);
    });
    return array;
};

var guardarTabla = function () {
    var $tbl = $("#vista_previa #contenido table");
    var arrPalabras =  tablaToArray();
    $.ajax({
        url: _sysUrlBase_+'/libre_palabras/xGuardarArray',
        type: 'POST',
        dataType: 'json',
        data: {"arrPalabras": JSON.stringify(arrPalabras)},
    }).done(function(resp) {
        if(resp.code=='ok') {
            var arrIDs = resp.data;
            $.each(arrIDs, function(index, nodo) {
                $tbl.find('td[data-idcorrelativo="'+nodo.idcorrelativo+'"]').attr('data-idpalabra',nodo.idpalabra);
            });
            mostrar_notificacion('<?php echo ucfirst(JrTexto::_("Success")); ?>', '<?php echo ucfirst(JrTexto::_("Saved successfully")); ?>', 'success');
        } else {
            mostrar_notificacion('Error', resp.msj, 'error');
        }
    }).fail(function(err) {
        console.log("error", err);
    }).always(function() {});
    
};

var showEdicion = function (mostrar) {
    mostrar = (typeof mostrar=="undefined")?true:mostrar;
    if(mostrar) {
        $("#edicion").removeClass('hidden');
        initEditor();
    } else {
        $('#edicion').addClass('hidden');
    }
};

var showVistaPrevia = function (mostrar) {
    mostrar = (typeof mostrar=="undefined")?true:mostrar;
    if(mostrar) {
        $("#vista_previa").removeClass('hidden');
        tinymce.remove('#txtEditor');
    } else {
        $("#vista_previa").addClass('hidden');
    }
};

$(document).ready(function() {

    $(".btn-guardar").click(function(e) {
        tinyMCE.triggerSave();
        var txt=$('#txtEditor').val();
        txt = formatoText2Tabla(txt);
        $("#vista_previa #contenido").html(txt);
        showVistaPrevia();
        showEdicion(false);
        guardarTabla();
    });

    $(".btn-editar").click(function(e) {
        e.preventDefault();
        var txtHtml = $("#vista_previa #contenido").html();
        txtHtml = formatoTexto(txtHtml);
        $("#txtEditor").val(txtHtml);
        $("#txtEditor").html(txtHtml);
        showEdicion();
        showVistaPrevia(false);
    });

    $(".btn-cancelar").click(function(e) {
        e.preventDefault();
        $.confirm({
            title: '<?php echo JrTexto::_("Cancel"); ?>',
            content: '<?php echo JrTexto::_("Are you sure you want to discard changes?"); ?>',
            confirmButton: '<?php echo JrTexto::_("Yes"); ?>',
            cancelButton: '<?php echo JrTexto::_("No"); ?>',
            confirmButtonClass: 'btn-primary',
            cancelButtonClass: 'btn-danger',
            confirm: function(){
                showVistaPrevia(); 
                showEdicion(false);
            },
            cancel: function(){  }
        });
    });

    if(isPalabrasEmpty) {
        initEditor();
    }
    
});
</script>