<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style>
    .input-group { margin-top: 0px; }
    iframe#url-externa { border: 0; height: 415px; width: 100%; }
</style>

<div class="row" id="palabras_iframe-frm">
    <input type="hidden" name="hIdTema" id="hIdTema" value="<?php echo $this->temas[0]['idtema']; ?>">
    <input type="hidden" name="hIdPalabra" id="hIdPalabra" value="<?php echo @$this->palabras['idpalabra']; ?>">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title"><?php echo $this->temas[0]['tipo_nombre'].': '.$this->temas[0]['nombre']; ?></h2>
            </div>
            <div class="panel-body">
                <div id="edicion">
                    <div class="col-xs-12 padding-0 form-group">
                        <div class="col-xs-10 col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon" id="lblOrigen"><?php echo JrTexto::_("Source") ?></span>
                                <input type="text" class="form-control" id="txtOrigen" name="txtOrigen" value="<?php echo @$this->origen; ?>">
                            </div><hr>
                        </div>
                        <div class="col-xs-2 col-sm-2">
                           <button class="btn btn-info btn-preview"><?php echo JrTexto::_("Preview") ?></button>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                           <textarea name="txtEditor" id="txtEditor"><?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), @$this->palabras['url_iframe']); ?></textarea>
                        </div>
                    </div>
                </div>

                <div id="vista_previa" class="hidden">
                    <div class="col-xs-12 text-center">
                        <buttton class="btn btn-default btn-volver-editar"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back to edit"); ?></buttton>
                        <buttton class="btn btn-success btn-guardar"><i class="fa fa-save"></i> <?php echo JrTexto::_("Save"); ?></buttton>
                    </div>
                    <div class="col-xs-12 padding-0" id="contenido-html">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

var initEditor = function() {
    tinymce.init({
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        convert_newlines_to_brs : true,
        menubar: false,
        statusbar: false,
        verify_html : false,
        content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
        selector: '#txtEditor',
        height: 350,
        paste_auto_cleanup_on_paste : true,
        paste_word_valid_elements: "table, thead, tbody, tfooter, tr, td, th",
        /*paste_preprocess : function(pl, o) {
           var html='<div>'+o.content+'</div>';
           var txt =$(html).text();
           o.content = txt;
        },
        paste_postprocess : function(pl, o) {
            o.node.innerHTML = o.node.innerHTML;
            o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
        },*/
        plugins:["paste table textcolor lists advlist chingoaudio chingoimage chingovideo" ],  
        toolbar: ' undo redo | removeformat styleselect fontselect fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify |  bold italic underline | bullist numlist outdent indent |  table | chingoimage chingoaudio chingovideo',
        advlist_number_styles: "default,lower-alpha,lower-greek,lower-roman,upper-alpha,upper-roman",
    });
};

var showEdicion = function (mostrar) {
    mostrar = (typeof mostrar=="undefined")?true:mostrar;
    if(mostrar) {
        $("#edicion").removeClass('hidden');
        initEditor();
    } else {
        $('#edicion').addClass('hidden');
    }
};

var showVistaPrevia = function (mostrar) {
    mostrar = (typeof mostrar=="undefined")?true:mostrar;
    if(mostrar) {
        $("#vista_previa").removeClass('hidden');
        tinymce.remove('#txtEditor');
    } else {
        $("#vista_previa").addClass('hidden');
    }
};

$(document).ready(function() {
	$('.btn-preview').click(function(e) {
		tinyMCE.triggerSave();
		var txt = $('#txtEditor').val();
        $("#vista_previa #contenido-html").html(txt);
		showVistaPrevia();
        showEdicion(false);
		return false;
	});

	$('.btn-volver-editar').click(function(e) {
		var txtHtml = $("#vista_previa #contenido-html").html();
		$("#txtEditor").val(txtHtml);
        $("#txtEditor").html(txtHtml);
        showEdicion();
        showVistaPrevia(false);
		return false;
	});

	$('.btn-guardar').click(function(e) {
		var html = $("#vista_previa #contenido-html").html();
		$.ajax({
	        url: _sysUrlBase_+'/libre_palabras/guardarLibre_palabras',
	        type: 'POST',
	        dataType: 'json',
	        data: {
                'pkIdpalabra': $('#hIdPalabra').val(),
                'txtIdtema': $('#hIdTema').val(),
                'txtOrigen': $('#txtOrigen').val(),
                'txtUrl_iframe': html
            },
	    }).done(function(resp) {
	        if(resp.code=='ok') {
                $('#hIdPalabra').val(resp.newid);
	            mostrar_notificacion('<?php echo ucfirst(JrTexto::_("Success")); ?>', '<?php echo ucfirst(JrTexto::_("Saved successfully")); ?>', 'success');
	        } else {
	            mostrar_notificacion('Error', resp.msj, 'error');
	        }
	    }).fail(function(err) {
	        console.log("error", err);
	    }).always(function() {});
		return false;
	});

	initEditor();
});
</script>