<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.(JrTexto::_('Manual of pedagogical orientations')).'</a>'; }
        else{ $enlace .= (JrTexto::_('Manual of pedagogical orientations')); }
        $enlace .= '</li>';
        $enlace .= '<li style="float:right" ><a href="#" class="btn btn-xs btn-primary btnvideohelp" style="color:#fff;" data-modal="si" href="javascript:void(0)" data-titulo="'.JrTexto::_("Manual of pedagogical orientations").'"><i class="fa fa-question-circle"></i> '. JrTexto::_('Help').'</a></li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<style type="text/css">
    .nombreguiapedagogica{
        color: #d20d0d;
    text-shadow: black 0.1em 0.05em 0.1em;
    font-size: 2.5em;
    font-weight: bold;
  
    }
    a.aguiapedagogica{
        font-size:1.5em;
        color: #0d2365;
        text-shadow: #2952a9 0.1em 0.1em 0.2em;
        text-align:center;
    }
    a.hover{
        color: white; 
        text-shadow: black 0.1em 0.1em 0.2em;
    }
    a.aguiapedagogica p{
        text-decoration: underline;
    }
    .cdcircle{
        position: absolute;
        left: calc(50% - 2rem);
        width: 4rem;
        height: 4rem;
        bottom: calc(50% - 1rem);
        background-color: #fffbfb;
        font-size: 2em;
        text-align: center;
        border-radius: 50%;
    }
    .panel{
        margin-bottom: 1em !important;
    }
    .panel-heading{
        padding:0px;
    }
    
    /*.aguiapedagogicacd{
        background-image: url('');
        background-size: 100%;*/
       /* transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -webkit-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -moz-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -o-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);
        -ms-transform: rotate(17deg) scale(0.957) skew(-2deg) translate(0px);*/
    /*}*/
</style>

<div class="row" id="alum-libre_tipo">
<?php 
$ipanel=['panel-primary','panel-success','panel-warning','panel-danger','panel-info'];
if(!empty($this->cursos)) { 
    $i=0;
    
    foreach ($this->cursos as $t) {
        $isgray=false;
        $btnsubirpdf1='';
        $btnsubirpdf2='';
        if ($t['idcurso']==4 || $t['idcurso']==5) {
            $isgray='cursor: not-allowed;filter: grayscale(100%);display:none';
        }
        

        if($this->idrol==1){
            $btnsubirpdf1='<a class="btn btn-danger btn-sm btnsubirguiapedagogica" data-nombre="'.$t['nombre'].'" data-tipofile="pdf" data-dirmedia="guia/pdf1/" ><i class="fa fa-upload"></i> '.JrTexto::_('Upload').'</a>';
            $btnsubirpdf2='<a class="btn btn-danger btn-sm btnsubirguiapedagogica" data-nombre="'.$t['nombre'].'" data-tipofile="pdf" data-dirmedia="guia/pdf2/" ><i class="fa fa-upload"></i> '.JrTexto::_('Upload').'</a>';
        }else if($t['idcurso']>36) continue;
        
        ?>
        <div class="col-md-6 col-sm-12" >
            <div class="panel <?php echo $ipanel[floor($i/2)]; ?>" style='<?php echo !empty($isgray)?$isgray:''; ?>'>
                <div class="text-center nombreguiapedagogica panel-heading"><?php echo $t['nombre']; ?></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 btn-panel-container panelcont-xs panelcontainer text-center" >
                           <!--  <a  class="aguiapedagogica hvr-float-shadow">
                                <img src="<?php echo $this->documento->getUrlBase().$t["imagen"];?>" width="100%" class="img img-resonsive img-thumbnail" style="min-height: 100px; <?php echo !empty($isgray)?$isgray:''; ?>">
                                <p><?php echo 'Manual pedagógico (Español)'/*JrTexto::_('Guide A');*/ ?></p>
                            </a> -->
                            <form method="post" action="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf2/';?>" >
                                <input type="hidden" name="link" value='<?php echo $this->documento->getUrlStatic().'/media/guia/pdf1/'.$t['nombre'].".pdf";?>' >

                                <a class="aguiapedagogica hvr-float-shadow">
                                    <input type='image' src="<?php echo $this->documento->getUrlBase().$t["imagen"];?>" width="100%" class="img img-resonsive img-thumbnail" style="min-height: 100px; <?php echo !empty($isgray)?$isgray:''; ?>">
                                    <p><?php echo 'Manual pedagógico (Español)'/*JrTexto::_('Guide A');*/ ?></p>
                                </a>
                            </form> 
                            <?php echo $btnsubirpdf1; ?>
                        </div>
                       
                        <div class="col-xs-12 col-sm-6 col-md-6 btn-panel-container panelcont-xs panelcontainer text-center" >
                            <?php  $isgray='cursor: not-allowed;filter: grayscale(100%);';?>
                            <a href="<?php echo $t['idcurso']==31?$this->documento->getUrlBase().'/biblioteca/verpdf/?link='.$this->documento->getUrlStatic().'/media/guia/pdf2/'.$t['nombre'].".pdf":'#';?>" class="aguiapedagogica hvr-float-shadow">
                                <img src="<?php echo $this->documento->getUrlBase().$t["imagen"];?>" width="100%" class="img img-resonsive img-thumbnail" style="min-height: 100px; <?php echo $t['idcurso']!=31?$isgray:''; ?>">
                                <p><?php echo 'Pedagogical manual (English)'/*JrTexto::_('Guide B');*/ ?></p>
                            </a>
                            <?php echo $btnsubirpdf2; ?>
                            <?php  $isgray='';?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    <?php 
    }
} ?>
</div>
<div id='video123' style="display: none;">
    <video controls class="sombra col-md-12 col-xs-12 col-sm-12" style="" src="<?php echo $this->documento->getUrlBase().'/static/media/guia/video/mopfinish.mp4';?>">

    </video>
</div>
<script>
$(document).ready(function(){    
    $('.btnsubirguiapedagogica').click(function(ev){
        __syssubirfile($(this));
    })
    $('.btnvideohelp').click(function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var enmodal=$(this).attr('data-modal')||'no';
       
       
       
       
        var ventana='videoayuda';
        var claseid=ventana+'_1234';
        var titulo='<?php echo JrTexto::_("Manual of pedagogical orientations");?>';
        openModal('lg',titulo,false,true,claseid,{header:true,footer:false,borrarmodal:true,callback:function(modal){

            modal.find('#modalcontent').html($('#video123').html());
            modal.find('.modal-footer').remove();
        }});
      })
})
</script>