<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Companies"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Bolsa_empresas", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Partners").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body table-responsive">
            <table class="table table-striped ">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                  <th><?php echo JrTexto::_("Representante") ;?></th>
                  <th><?php echo JrTexto::_("Logo") ;?></th>
                  <th><?php echo JrTexto::_("Direccion") ;?></th>
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5c0152661656e='';
changefoto=false;
function refreshdatos5c0152661656e(){
    tabledatos5c0152661656e.ajax.reload();
}
$(document).ready(function(){  
  var estados5c0152661656e={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5c0152661656e='<?php echo ucfirst(JrTexto::_("Companies"))." - ".JrTexto::_("edit"); ?>';
  var draw5c0152661656e=0;

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5c0152661656e();
  });
  tabledatos5c0152661656e=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      "pageLength":50,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
        {'data': '<?php echo JrTexto::_("Representante") ;?>'},
       // {'data': '<?php echo JrTexto::_("Rason_social") ;?>'},
       // {'data': '<?php echo JrTexto::_("Ruc") ;?>'},
        {'data': '<?php echo JrTexto::_("Logo") ;?>'},
        {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
        //{'data': '<?php echo JrTexto::_("Telefono") ;?>'},
        //{'data': '<?php echo JrTexto::_("Usuario") ;?>'},
        //{'data': '<?php echo JrTexto::_("Clave") ;?>'},
        //{'data': '<?php echo JrTexto::_("Correo") ;?>'},
        //{'data': '<?php echo JrTexto::_("Estado") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/bolsa_empresas/buscarjson/?json=true',
        type: "post",
        data:function(d){
            d.json=true
            d.texto=$('#texto').val(),
            draw5c0152661656e=d.draw;
        },
        "dataSrc":function(json){
          var data=json.data;
          json.draw = draw5c0152661656e;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var acc='<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/bolsa_empresas/editar/?id='+data[i].idempresa+'" data-titulo="'+tituloedit5c0152661656e+'"><i class="fa fa-edit"></i></a>';
            acc+='<a class="btn btn-xs btnvermodal"  href="'+_sysUrlBase_+'/bolsa_empresas/socios/?idempresa='+data[i].idempresa+'" data-titulo="<?php echo JrTexto::_("Partners"); ?>"><i class="fa fa-user"></i></a>';
            //acc+='<a class="btn btn-adduser btn-xs" href="javascript:;" data-id="'+data[i].idempresa+'" ><i class="fa fa-user"></i></a>';
            acc+='<a class="btn btn-addcurso btn-xs"  href="'+_sysUrlBase_+'/bolsa_empresas/cursos/?idempresa='+data[i].idempresa+'" data-titulo="'+tituloedit5c0152661656e+'"><i class="fa fa-book"></i></a>';
            acc+='<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idempresa+'" ><i class="fa fa-trash-o"></i></a>';
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': (data[i].ruc==''||data[i].ruc==null?'':'RUC: '+data[i].ruc+'<br>')+data[i].nombre+'<br>'+data[i].rason_social,
              //'<?php echo JrTexto::_("Rason_social") ;?>': data[i].rason_social,
              //'<?php echo JrTexto::_("Ruc") ;?>': data[i].ruc,
              '<?php echo JrTexto::_("Representante") ;?>': data[i].representante,
              '<?php echo JrTexto::_("Logo") ;?>': (data[i].logo==''||data[i].logo==null?'':'<img src="'+_sysUrlBase_+data[i].logo+'" class="img" style="max-width:50px; max-height:50px">'),
              '<?php echo JrTexto::_("Direccion") ;?>': (data[i].telefono==null||data[i].telefono==''?'':'tel: '+data[i].telefono+'<br>')+(data[i].direccion==''||data[i].direccion==null?'-':data[i].direccion),
              //'<?php echo JrTexto::_("Telefono") ;?>': data[i].telefono,
              //'<?php echo JrTexto::_("Usuario") ;?>': data[i].usuario,
              //'<?php echo JrTexto::_("Clave") ;?>': data[i].clave,
              //'<?php echo JrTexto::_("Correo") ;?>': data[i].correo,
              //'<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
              '<?php echo JrTexto::_("Actions") ;?>':acc
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'bolsa_empresas', 'setCampo', id,campo,data);
          if(res) tabledatos5c0152661656e.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5c0152661656e';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Partners';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');
    if(enmodal=='no'){
      return redir(url);
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{footer:false,borrarmodal:true,callback:function(_modal){}}); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){
        var res = xajax__('', 'bolsa_empresas', 'eliminar', id);
        if(res) tabledatos5c0152661656e.ajax.reload();
      }
    });
  });
});
</script>