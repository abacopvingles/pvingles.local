<?php
defined('RUTA_BASE') or die();
if(isset($this->usuarioAct)){
	echo '<input id="iddocente" type="hidden" value="'.$this->usuarioAct['idpersona'].'" />';
}else{
	echo '<input id="iddocente" type="hidden" value="-1" />';
}


?>

<style>
#cpanel_docente .widget-box .widget-body{
    height: auto;
}
#contenedor-pnls-info .widget-box .widget-body{
    min-height: 460px;
    max-height: 470px;
}
#pnl-datosfiltro .listado-alums{
    max-height: 100px; 
    overflow: auto;
}
.bg-lilac { background-color: #a286ff; color: #fff; }
.bg-green2 { background-color: #2ed353; color: #ffffff; }
.bg-pink { background-color: #ffb283; color: #ffffff; }

.widget-es-box { min-height:500px; max-height:600px; overflow:scroll; }
.progress { margin:0; height:10px; }
.widget-header>.widget-title { text-transform:none!important; }
.widget-body-custom{ min-height:400px;}
.select-ctrl-wrapper:after{right:0;}
</style>

<div class="cpanel-docente">
	<div class="row">
		<div class="col-xs-12 col-sm-4 col-md-3">
			<div class="widget-box" id="pnl-filtros">
				<div class="widget-body widget-body-custom">
					<div class="widget-main">
						<div class="row">
							<div class="col-xs-6 col-sm-12">
								<label><?php echo JrTexto::_('School'); ?></label>
								<div class="form-group select-ctrl-wrapper select-azul">
									<select name="opcColegio" id="idcolegio" class="form-control select-ctrl ">
										<option value="-1">- <?php echo JrTexto::_('Select school'); ?>
											<?php
												if(!empty($this->miscolegios)){
													foreach ($this->miscolegios  as $c) {
														if(empty($cursos)) $cursos=$c["cursos"];
														echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
													}
												} 
											?>
										</option>
									</select>
								</div>
							
							</div>
							<div class="col-xs-6 col-sm-12">
								<label><?php echo JrTexto::_('Courses'); ?></label>
								<div class="form-group select-ctrl-wrapper select-azul">
									<select name="opcCourse" id="idcurso" class="form-control select-ctrl ">
										<option value="-1">- <?php echo JrTexto::_('Select course'); ?> -</option>
									</select>
								</div>
							
							</div>
							<div class="col-xs-6 col-sm-12">
								<label><?php echo JrTexto::_('Grades'); ?></label>
								<div class="form-group select-ctrl-wrapper select-azul">
									<select name="opcAlumno" id="idgrados" class="form-control select-ctrl ">
										<option value="-1">- <?php echo JrTexto::_('Select grade'); ?> -</option>
									</select>
								</div>
							
							</div>
							<div class="col-xs-6 col-sm-12">
								<label><?php echo JrTexto::_('Section'); ?></label>
								<div class="form-group select-ctrl-wrapper select-azul">
									<select name="opcSection" id="idseccion" class="form-control select-ctrl ">
										<option value="-1">- <?php echo JrTexto::_('Select section'); ?> -</option>
									</select>
								</div>
							
							</div>
                            <div class="col-md-12">
                            	<button id="buscarfiltros" class="btn btn-primary"><?php echo JrTexto::_('Search'); ?></button>
                            </div>
						</div>
						<!--end row-->
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-9" id="contenedor-pnls-info">
        	<div class="col-md-12 col-xs-12">
        		<div class="form-group" style="max-width:150px; display:inline-block;">
                    <label for="sel1"><?php echo JrTexto::_('Filter by date'); ?>:</label>
                    <input type="text" id="fechavalue" class="form-control" id="datepicker01" />
                </div>
                <button id="buscarfecha" class="btn btn-primary"><?php echo JrTexto::_('Search'); ?></button>
        	</div>
        	<div class="col-md-12" id="contenedor-alumno">
        		<div class="col-sm-12 widget-es-box">
        			<div class="widget-header bg-blue">
        				<h4 class="widget-title">
			                <i class="fa fa-clock-o"></i>
			                <span><?php echo JrTexto::_('Time on the Virtual Platform'); ?></span>
			            </h4>
        			</div>
        			<div class="widget-body" style="height:initial!important;width: 98.5%;">
		                <div class="row">
		                    <div class="col-xs-12">
		                        <div class="card card-time time-acumulado">
		                            
		                            <div class="card-info">
		                            	<div class="col-xs-12  resultado_tiempos" style="border: 1px solid gray; border-radius: 0.5em;">
		                            		<div class="table-responsive">
		                            			<table id="tiemposalumnos" class="table table-striped">
		                            				<thead>
		                            					<tr>
		                            						<th style="max-width:200px;"><?php echo JrTexto::_('Names'); ?></th>
		                            						<th><?php echo JrTexto::_('Accumulated Total Time'); ?></th>
		                            						<th><?php echo JrTexto::_('Time on the virtual platform'); ?></th>
		                            						<th><?php echo JrTexto::_('Test Time'); ?></th>
		                            						<th><?php echo JrTexto::_('SmartBook Time'); ?></th>
		                            						<th><?php echo JrTexto::_('Activities Time') ?></th>
		                            					</tr>
		                            				</thead>
		                            				<tbody>
		                            					
		                            				</tbody>
		                            			</table>
		                            		</div>
		                            	</div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                <!--end row widget-->
        			</div>
        		</div>
        		<div class="col-sm-12 widget-es-box">
        			<div class="widget-header bg-green2">
        				<h4 class="widget-title">
			                <i class="fa fa-clock-o"></i>
			                <span><?php echo JrTexto::_('Activities Progress'); ?></span>
			            </h4>
        			</div>
        			<div class="widget-body" style="height:initial!important;width: 98.5%;">
		                <div class="row">
		                    <div class="col-xs-12" style="border: 1px solid gray; border-radius: 0.5em;">
                        		<div class="table-responsive">
                        			<table class="table table-striped" id="progresoxactividadalumno">
                        				<thead>
                        					<tr>
                        						<th><?php echo JrTexto::_('Names'); ?></th>
                        						<th>Listening</th>
                        						<th>Reading</th>
                        						<th>Writing</th>
                        						<th>Speaking</th>
                        					</tr>
                        				</thead>
                        				<tbody>
				
                        				</tbody>
                        			</table>
                        		</div>
		                    </div>
		                </div>
		                <!--end row widget-->
        			</div>
        		</div>
        		<!--endbox-->
        		<div class="col-sm-12 widget-es-box">
        			<div class="widget-header bg-red">
        				<h4 class="widget-title">
			                <i class="fa fa-clock-o"></i>
			                <span><?php echo JrTexto::_('Tests Progress'); ?></span>
			            </h4>
        			</div>
        			<div class="widget-body" style="height:initial!important;width: 98.5%;">
		                <div class="row">
		                    <div class="col-xs-12" style="border: 1px solid gray; border-radius: 0.5em;">
                        		<div class="table-responsive">
                        			<table class="table table-striped" id="progresoxexamenesalumno">
                        				<thead>
                        					<tr>
                        						<th><?php echo JrTexto::_('Names'); ?></th>
                        						<th>Listening</th>
                        						<th>Reading</th>
                        						<th>Writing</th>
                        						<th>Speaking</th>
                        					</tr>
                        				</thead>
                        				<tbody>
                        					
                        				</tbody>
                        			</table>
                        		</div>
		                    </div>
		                </div>
		                <!--end row widget-->
        			</div>
        		</div>
        		<!--endbox-->
        	</div>
        </div>
	</div>
</div>

<script type="text/javascript">
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;
console.log(datoscurso);

var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');                 
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');                
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}

var busqueda = function(filtros, ultimo = null){
	if(filtros != null){
		/**Cargar los tiempos... */
		$.ajax({
			url: _sysUrlBase_+'/docente/tiempos_grupo',
			type: 'POST',
			dataType: 'json',
			data: filtros,
		}).done(function(resp) {
			if(resp.code=='ok'){
				// console.log(resp.data);
				
				if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){
					var table = $('#tiemposalumnos');
					var table_body = $('#tiemposalumnos').find('tbody');
					var row = '';

					if(ultimo != true && resp.data.tiemposanterior != null && Object.keys(resp.data.tiemposanterior).length > 0){
						ultimo =null;
					}
					
					table_body.html(" ");
					for(var i in resp.data.tiemposactual){
						if(typeof resp.data.tiemposactual[i] != 'function'){
							if(ultimo != null){
								row = "<tr><td style='max-width:200px; overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'><div style='height:30px;' >"+resp.data.tiemposactual[i].alumno+
								"</div><div style='height:30px;'>Fecha Actual</div><div>Hasta la fecha:<br>"+$('#fechavalue').val()+"</div></td><td><div style='height:30px;'></div><div style='height:30px;'>"+resp.data.tiemposactual[i].total+"</div><div>"+resp.data.tiemposanterior[i].total+"</div></td> <td><div style='height:30px;'></div><div style='height:30px;'>"+resp.data.tiemposactual[i].tiempopv+
								"</div><div>"+resp.data.tiemposanterior[i].tiempopv+"</div></td><td><div style='height:30px;'></div><div style='height:30px;'>"+resp.data.tiemposactual[i].smartquiz+"</div><div>"+resp.data.tiemposanterior[i].smartquiz+"</div></td> <td><div style='height:30px;'></div><div style='height:30px;'>"+resp.data.tiemposactual[i].smartbook+
								"</div><div>"+resp.data.tiemposanterior[i].smartbook+"</div></td><td><div style='height:30px;'></div><div style='height:30px;'>"+resp.data.tiemposactual[i].homework+"</div><div>"+resp.data.tiemposanterior[i].homework+"</div></td></tr>";
							}else{
								row = "<tr><td style='max-width:200px; overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'>"+resp.data.tiemposactual[i].alumno+
								"</td><td>"+resp.data.tiemposactual[i].total+"</td> <td>"+resp.data.tiemposactual[i].tiempopv+
								"</td><td>"+resp.data.tiemposactual[i].smartquiz+"</td> <td>"+resp.data.tiemposactual[i].smartbook+
								"</td><td>"+resp.data.tiemposactual[i].homework+"</td></tr>";
							}
							table_body.append(row);
						}
					}
				}//end if
			}else{
				return false;
			}
		}).fail(function(xhr, textStatus, errorThrown) {
			console.log("%c ERROR","font-size:40px;");
			return false;
		});
		/**Cargar los progresos... */
		$.ajax({
			url: _sysUrlBase_+'/docente/progresoxactividad_grupo',
			type: 'POST',
			dataType: 'json',
			data: filtros,
		}).done(function(resp) {
			if(resp.code=='ok'){
				var drawhab = new Object;
				var color1 = '#36a2eb';
				var color2 = '#ff6384';
				
				if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){
					
					var table = $('#progresoxactividadalumno');
					var table_body = $('#progresoxactividadalumno').find('tbody');
					var row = '';
					if(ultimo != true && resp.data.tiemposanterior != null && Object.keys(resp.data.tiemposanterior).length > 0){
						ultimo =null;
					}
					table_body.html(" ");
					for(var i in resp.data.tiemposactual){
						if(typeof resp.data.tiemposactual[i] != 'function'){
							if(ultimo != null){
								row = "<tr><td><div style='height:30px;'>"+resp.data.tiemposactual[i]['alumno']+
								"</div><div style='height:30px;'>Fecha actual</div><div>Hasta la fecha:<br>"+$('#fechavalue').val()+"</div></td><td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['4'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['4'])+"% </div><div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['4'])+"%'></div></div><div>"+(resp.data.tiemposanterior[i]['4'])+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['5'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['5'])+"% </div><div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['5'])+"%'></div></div><div>"+(resp.data.tiemposanterior[i]['5'])+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['6'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['6'])+"% </div> <div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['6'])+"%'></div></div><div>"+(resp.data.tiemposanterior[i]['6'])+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['7'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['7'])+"% </div><div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['7'])+"%'></div></div><div>"+(resp.data.tiemposanterior[i]['7'] )+"</div></div></td>";

								
							}else{
								row = "<tr><td>"+resp.data.tiemposactual[i]['alumno']+
								"</td><td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['4'] * 100)+"%'></div></div>"+
								(resp.data.tiemposactual[i]['4'])+"%</td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['5'] * 100)+"%'></div></div>"+
								(resp.data.tiemposactual[i]['5'])+"%</td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['6'] * 100)+"%'></div></div>"+
								(resp.data.tiemposactual[i]['6'])+"%</td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['7'] * 100)+"%'></div></div>"+
								(resp.data.tiemposactual[i]['7'])+"%</td>"
							}
							table_body.append(row);
						}//end if tiempos actuales
					}//end foreach resultado
				}
				
				
			}else{
				return false;
			}
		}).fail(function(xhr, textStatus, errorThrown) {
			console.log("%c ERROR","font-size:40px;");
			return false;
		});
		/**Cargar los progresos2... */
		$.ajax({
			url: _sysUrlBase_+'/docente/progresoxexamenes_group',
			type: 'POST',
			dataType: 'json',
			data: filtros,
		}).done(function(resp) {
			if(resp.code=='ok'){
				if(resp.data.tiemposanterior != null && Object.keys(resp.data.tiemposanterior).length > 0){
					//make some code here
					
				}//end if
				if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){
					
					var table = $('#progresoxexamenesalumno');
					var table_body = $('#progresoxexamenesalumno').find('tbody');
					var row = '';
					table_body.html(" ");
					for(var i in resp.data.tiemposactual){
						if(typeof resp.data.tiemposactual[i] != 'function'){
							if(ultimo != null){
								row = "<tr><td><div style='height:30px;'>"+resp.data.tiemposactual[i]['alumno']+
								"</div><div style='height:30px;'>Fecha actual</div><div style='height:30px;'>Hasta la fecha:<br>"+$('#fechavalue').val()+"</div></td><td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['4'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['4'])+"% </div><div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['4'])+"%'></div></div><div>"+(resp.data.tiemposanterior[i]['4'])+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['5'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['5'])+"% </div><div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['5'])+"%'></div></div><div>"+resp.data.tiemposanterior[i]['5']+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['6'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['6'])+"% </div> <div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['6'])+"%'></div></div><div>"+resp.data.tiemposanterior[i]['6']+"%</div></div></td>"+
								"<td><div style='height:30px;'></div><div style='height:30px;'><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['7'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['7'])+"% </div> <div><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposanterior[i]['7'])+"%'></div></div><div>"+resp.data.tiemposanterior[i]['7']+"</div></div></td>"
							}else{
								row = "<tr><td>"+resp.data.tiemposactual[i]['alumno']+
								"</td><td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['4'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['4'])+"% </td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['5'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['5'])+"% </td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['6'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['6'])+"% </td>"+
								"<td><div class='progress actual'><div class='progress-bar actual progress-bar-success progress-bar-striped active' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100' style='width:"+(resp.data.tiemposactual[i]['7'])+"%'></div></div>"+
								(resp.data.tiemposactual[i]['7'])+"% </td>"
							}
							
							table_body.append(row);
						}//end if tiempos actuales
					}//end foreach resultado
				}
			}else{
				return false;
			}
		}).fail(function(xhr, textStatus, errorThrown) {
			console.log("%c ERROR","font-size:40px;");
			return false;
		});
	}//end if validation
};

$('document').ready(function(){
	var filtros = new Object;
	// countTextObject = $('#view_Listening').find('.countPercentage');
	// countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[idcurso_total][4]) );

	$('#fechavalue').datetimepicker({ format:'YYYY-MM-DD' });
	$('#contenedor-alumno').slick({});

	$("#idcolegio").change(function(){
		var idcolegio=$(this).val()||'';
        for (var i = 0; i < datoscurso.length; i++) {
           if(datoscurso[i].idlocal==idcolegio){              
                datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
               actualizarcbo($(this),datoscurso[i].cursos);
           }
        }
	});
	$("#idcurso").change(function(){
		var idcurso=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;                
                actualizarcbo($(this),grados);
            }
	});
	$("#idgrados").change(function(){
		var idgrados=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        var idcurso=$('#idcurso').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;
                for (var h = 0; h < grados.length; h++){
                    if(grados[h].idgrado==idgrados){
                        var secciones=grados[h].secciones;
                        secciones.sort(function(a, b){return a.seccion - b.seccion;});
                        actualizarcbo($(this),secciones);
                    }
                }
                
            }
	});

	$('#buscarfiltros').on("click", function(){
		//accionar el evento de busqueda del filtro
		$('#fechavalue').val('');
		var iddocente = $('#iddocente').val();
		var idlocal = $('#idcolegio').val();
		var idcurso = $('#idcurso').val();
		var idgrado = $('#idgrados').val();
		var idgrupoauladetalle = $('#idseccion').val();

		filtros = new Object;
		if(iddocente != -1 && idlocal != -1 && idcurso != -1 && idgrado != -1 && idgrupoauladetalle != -1){
			filtros.iddocente = iddocente;
			filtros.idlocal = idlocal;
			filtros.idcurso = idcurso;
			filtros.idgrado = idgrado;
			filtros.idgrupoauladetalle = idgrupoauladetalle;
			busqueda(filtros);
		}else{
			//Mostrar un alerta....
			alert("Seleccione todo los campos");
		}

	});
	$('#buscarfecha').on('click',function(){
		//accionar el evento de busqueda defecha para los reportes
		var iddocente = $('#iddocente').val();
		var idlocal = $('#idcolegio').val();
		var idcurso = $('#idcurso').val();
		var idgrado = $('#idgrados').val();
		var idgrupoauladetalle = $('#idseccion').val();
		var fecha = $('#fechavalue').val();
		filtros = new Object;
		if(iddocente != -1 && idlocal != -1 && idcurso != -1 && idgrado != -1 && idgrupoauladetalle != -1){
			filtros.iddocente = iddocente;
			filtros.idlocal = idlocal;
			filtros.idcurso = idcurso;
			filtros.idgrado = idgrado;
			filtros.idgrupoauladetalle = idgrupoauladetalle;
			filtros.fecha = fecha;
			busqueda(filtros,true);
		}else{
			//Mostrar un alerta....
			alert("Seleccione todo los campos");
		}
	});

});
</script>