<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>" onsubmit="return false;">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">  
          <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="idgrupoauladetalle" class="form-control idgrupoauladetalle">
                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php if(!empty($this->miscursos))
                    foreach ($this->miscursos as $fk) { ?>
                      <option data-idcurso="<?php echo $fk["idcurso"]?>" value="<?php echo $fk["idgrupoauladetalle"]?>" ><?php echo $fk["strcurso"]." : ".$fk["aniopublicacion"]." : ".$fk["strlocal"] ?></option><?php } ?>
                </select>
              </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-6 form-group">
              <label><?php echo  ucfirst(JrTexto::_("student")." (".JrTexto::_('Name')."/DNI)")?></label>
              <div class="input-group" style="margin:0px">
                <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo JrTexto::_('Search') ?> <i class="fa fa-search"></i></span>  
              </div>
            </div> 
          </div>
          <button id="actualuarmatricula123" class="hide">actualizar</button>         
          </form>  
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide " >
    <div class="panel" id="vista<?php echo $idgui; ?>">
    </div>
  </div>
</div>
<div class="form-view" id="ventanatabla_<?php echo $idgui; ?>" >
  <div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="panel">         
         <div class="panel-body table-striped table-responsive">
            <table class="table " style="min-width:100%">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Students") ;?></th>
                  <th><?php echo JrTexto::_("Activities") ;?></th>                           
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  var edithtml_all=function(obj){
    console.log(obj);
     tinyMCE.init({
      relative_urls : false,
      convert_newlines_to_brs : true,
      menubar: false,
      statusbar: false,
      verify_html : false,
      content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
      selector: '#'+obj.id,
      height: 300,          
      plugins:["textcolor advlist " ], 
      toolbar: 'undo redo | advlist | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor'
    });
}


var tabledatos5bd4788e236d6='';
var estados5bd4788e236d6={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
var tituloedit5bd4788e236d6='<?php echo ucfirst(JrTexto::_("acad_matricula"))." - ".JrTexto::_("edit"); ?>';
var draw5bd4788e236d6=0;
  function refreshdatos5bd4788e236d6(){
      tabledatos5bd4788e236d6.ajax.reload();
  }
$(document).ready(function(){
  tabledatos5bd4788e236d6=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "pageLength": 50,
      "searching": false,
      "processing": false,
      "serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Students") ;?>'},
        {'data': '<?php echo JrTexto::_("Activities") ;?>'},
       /* {'data': '<?php //echo JrTexto::_("Telephone") ;?>'},
        {'data': '<?php //echo JrTexto::_("usuario") ;?>'},
        {'data': '<?php //echo JrTexto::_("email") ;?>'},
        {'data': '<?php //echo JrTexto::_("Fecha_matricula") ;?>'},
        {'data': '<?php //echo JrTexto::_("Estado") ;?>'},*/
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/actividad_alumno/actividadoffline/?json=true',
        type: "post",
        data:function(d){
            d.json=true;
            d.texto=$('#texto').val();
            var select=$('select[name="idgrupoauladetalle"]');
            var idcurso=select.find('option:selected').attr('data-idcurso');          
            d.idgrupoauladetalle=select.val()||-1;
            d.solooffline=true;
            d.idcurso=idcurso;
            draw5bd4788e236d6=d.draw;
            d.iddocente='<?php echo $this->usuarioAct["idpersona"]; ?>';
        },
        "dataSrc":function(json){
          
          var data=json.data;             
          console.log(data);
          json.draw = draw5bd4788e236d6;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();          
          for(var i=0;i< data.length; i++){
            var actividades=data[i].actividades;
            var htmlact='';
            if(actividades.length)
              $.each(actividades,function(ii,v){
                htmlact='';
                if(v.length){
                  //htmlact+='<div>';
                  $.each(v,function(iii,vv){
                    var solucionhtml=vv["html_solucion"];
                    solucionhtml=solucionhtml.replace('__xRUTABASEx__',_sysUrlBase_);
                      htmlact+='<div> <a href="#" data-progreso="'+vv.porcentajeprogreso+'" data-tipofile="'+vv["tipofile"]+'" class="calificarejericiooff" data-idver="ejerver'+i+'_'+ii+'_'+iii+'">'+vv.titulo+' <span style="display:none" class="data-file">'+vv["file"]+'</span></a>';
                    htmlact+='<div class="hidden" id="ejerver'+i+'_'+ii+'_'+iii+'">'+solucionhtml+'</div>';
                     htmlact+='</div>';
                  })
                  //htmlact+='</div>';  
                }                
              })
          console.log(data[i]);
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Students") ;?>':'DNI:'+data[i].dni+" <br><span class='stralumno' data-idalumno='"+data[i].idalumno+"'>"+ data[i].stralumno+'</span>', //data[i].idalumno,
              '<?php echo JrTexto::_("Activities") ;?>':htmlact,             
              /*'<?php// echo JrTexto::_("email") ;?>':data[i].alumno_email,
              '<?php //echo JrTexto::_("Fecha_matricula") ;?>': data[i].fecha_registro,
              '<?php //echo JrTexto::_("Estado") ;?>': data[i].estado,*/
              '<?php echo JrTexto::_("Actions") ;?>' :
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/perfil/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Ficha'); ?> '+data[i].stralumno+'"><i class="fa fa-eye"></i></a>',
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/formulario/?idpersona='+data[i].idalumno+'&idrol=3&datareturn=false&buscarper=no" data-titulo="<?php //echo JrTexto::_('Student')." ".JrTexto::_('Edit'); ?>"><i class="fa fa-pencil"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash-o"></i></a>'

            });
          }
          return datainfo; 
        }, error: function(d){console.log(d)}
      },
      "language": { "url": _sysIdioma_.toUpperCase()!='EN'?(_sysUrlBase_+"/static/libs/datatable1.10/idiomas/"+_sysIdioma_.toUpperCase()+".json"):''}
      
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'setCampo', id,campo,data);
          if(res) tabledatos5bd4788e236d6.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd4788e236d6';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_matricula';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{footer:false,borrarmodal:true,callback:function(_modal){}});
  }).on('click','.calificarejericiooff',function(i,v){
    var btn=$(this);
    var btntr=btn.closest('tr');    
    var titulo=$(this).attr('data-titulo')||'';   
    var url='&plt=modal';
    var htmltmp=$(this).attr('data-idver');
    openModal('lg','','',true,'',{footer:false,borrarmodal:true,callback:function(_modal){
      htmltmp=$('#'+htmltmp);
      htmltmp.find('.showonpreview_nlsw').show();
      htmltmp.find('#btns_control_alumno').hide();
      htmltmp=htmltmp.html();
      htmltmp=htmltmp.replace('__xRUTABASEx__',_sysUrlBase_); 
      var nom=btntr.find('span.stralumno').text();
      _modal.find('.modal-header').find('#modaltitle').html(nom);
      _modal.find('.modal-body').html(htmltmp);

      htmlfooter='<div class="row"><div class="col-md-4 col-sm-12"></div><div class="col-md-4 col-sm-12 form-group text-left "><label><?php echo JrTexto::_('Qualification'); ?></label><div class="cajaselect">';
      htmlfooter+='<select name="progresoalumno" class="form-control progresoalumno">';
      var progreso=btn.attr('data-progreso');
      for(i=100;i>0;i--){  htmlfooter+='<option value="'+i+'" '+(progreso==i?'selected="selected"':'')+'>'+i+'</option>';}

      htmlfooter+='</select></div><div class="col-md-4 col-sm-12"></div></div><div class="col-md-12"><button type="button" class="btn btn-primary btnsavenota"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button><button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close'); ?></button></div>';
      _modal.find('#modalfooter').html(htmlfooter);
      var textareatmp=_modal.find('.respuestaAlumno').children('textarea');
       textareatmp.hide();
       textareatmp.siblings('.mce-tinymce').remove();
       textareatmp.siblings('.msjRespuesta').html(textareatmp.val());
       _modal.find('.btnadd_recordAlumno').hide();
      
      _modal.find('.btnsavenota').on('click',function(ev){

       htmlsave=_modal.find('.modal-body').clone();
       htmlsave.find('.showonpreview_nlsw').show();
       htmlsave.find('#btns_control_alumno').show();
       htmlsave.find('.btnadd_recordAlumno').show();
       var progreso=_modal.find('select.progresoalumno').val();
       var data=new FormData();
        data.append('iddetalle_actividad',htmlsave.children('div.textosave').attr('data-iddetalle'));
        data.append('idalumno',btntr.find('span.stralumno').attr('data-idalumno'));
        data.append('progreso',progreso);
        data.append('habilidades',htmlsave.find('input.selected-skills').val());
        data.append('estado','T');
        htmlsave2=htmlsave.html();
        htmlsave2=htmlsave2.replace(_sysUrlBase_,'__xRUTABASEx__'); 
        data.append('html_solucion',htmlsave2);
        data.append('file',btn.attr('data-file'));
        data.append('tipofile',btn.attr('data-tipofile'));
        sysajax({fromdata:data,url:_sysUrlBase_+'/actividad_alumno/ajax_agregar',callback:function(){}})
        btn.attr('data-progreso',progreso);
        _modal.find('.cerrarmodal').click();
      })

    }});
  })

  $('#frmEstudiantes<?php echo $idgui;?>').on('change','select.idgrupoauladetalle',function(ev){
       refreshdatos5bd4788e236d6();
  })
 
  $('.btnbuscar').click(function(ev){
    refreshdatos5bd4788e236d6();
  });
  
});
</script>