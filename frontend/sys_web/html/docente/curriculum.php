<div class="row" id="breadcrumb">
    <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
            <?php foreach ($this->breadcrumb as $b) {
            $enlace = '<li>';
            if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
            else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
            $enlace .= '</li>';
            echo $enlace;
            } ?>
        </ol>
    </div>
</div>

<div class="row" id="curriculum"> 
    <div class="col-xs-12">
        <div class="panel">
            <div class="panel-body">
                <ul class="nav nav-tabs" style="">
                    <li class="active"><a data-toggle="tab" href="#personalInformation"><?php echo JrTexto::_("Personal Information"); ?></a></li>
                    <li><a data-toggle="tab" href="#education"><?php echo JrTexto::_("Education"); ?></a></li>
                    <li><a data-toggle="tab" href="#workExperience"><?php echo JrTexto::_("Work experience"); ?></a></li>
                    <li><a data-toggle="tab" href="#references"><?php echo JrTexto::_("References"); ?></a></li>
                </ul>
                
                <div class="row">
                    <div class="tab-content">
                    <div id="personalInformation" class="tab-pane fade in active col-xs-12 col-sm-12">
                    <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Personal Information"); ?></h3></div>
                    <div class="col-xs-4 col-sm-3">
                        <img src="<?php echo !empty($this->docente['foto'])?$this->docente['foto']:$this->documento->getUrlStatic().'/media/usuarios/user_avatar.jpg'; ?>" alt="course_cover" class="img-responsive center-block border-turquoise-dark caratula">
                    </div>
                    <div class="col-xs-8 col-sm-8">
                        
                        <h3 class="col-xs-12 ultima_actividad">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_("Teacher's name"); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo $this->docente['ape_paterno'].' '.$this->docente['ape_materno'].' '.$this->docente['nombre']; ?></h4></div>
                        </h3>
                        <!--h3 class="col-xs-12  ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_("DNI"); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo '12345678'; ?></h4></div>
                        </h3-->
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('telephone'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo !empty($this->docente['telefono'])?$this->docente['telefono']:!empty($this->docente['celular'])?$this->docente['celular']:'---'; ?></h4></div>
                        </h3>
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('E-mail'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo !empty($this->docente['email'])?$this->docente['email']:'---'; ?></h4></div>
                        </h3>
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Civil Status'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php 
                            switch ($this->docente['estado_civil']) {
                                case 'C': echo ucfirst(JrTexto::_('Married')); break;
                                case 'D': echo ucfirst(JrTexto::_('Divorced')); break;
                                case 'S': echo ucfirst(JrTexto::_('Single')); break;
                                case 'V': echo ucfirst(JrTexto::_('Widowed')); break;
                                default: echo '---'; break;
                            } ?></h4></div>
                        </h3>
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Date of birthday'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo !empty($this->docente['fechanac'])?$this->docente['fechanac']:'---'; ?></h4></div>
                        </h3>
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Sex'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php if(!empty($this->docente['sexo'])){ echo (@$this->docente['sexo']=='M')?JrTexto::_('Male'):JrTexto::_('Female'); }else{ echo '---'; } ?></h4></div>
                        </h3>
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-6 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Address'); ?></h4></div>
                            <div class="col-xs-6 texto_info"><h4><?php echo !empty($this->docente['direccion'])?$this->docente['direccion']:'---'; ?></h4></div>
                        </h3>
                    </div>
                    </div>
                    <div id="education" class="tab-pane fade col-xs-12 col-sm-12">
                         <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Education"); ?></h3></div>
                         <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-4 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Primaria'); ?></h4></div>
                            <div class="col-xs-4 texto_info"><h4><?php echo JrTexto::_('Colegio "Maria Reyna"'); ?></h4></div>
                        </h3>  
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-4 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Secundaria'); ?></h4></div>
                            <div class="col-xs-4 texto_info"><h4><?php echo JrTexto::_('Colegio "Maria Reyna"'); ?></h4></div>
                        </h3>  
                        <h3 class="col-xs-12 ultima_actividad" style="margin:0">
                            <div class="col-xs-4 texto_info"><h4 style="font-weight: bold"><?php echo JrTexto::_('Universidad'); ?></h4></div>
                            <div class="col-xs-4 texto_info"><h4><?php echo JrTexto::_('"Universidad Nacional Pedro Ruiz Gallo"'); ?></h4></div>
                        </h3>                            
                    </div>
                    <div id="workExperience" class="tab-pane fade col-xs-12 col-sm-12">
                         <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("Work experience"); ?></h3></div>                            
                    </div>
                    <div id="references" class="tab-pane fade col-xs-12 col-sm-12">
                         <div class="bg-turquoise col-xs-12 nombre border-bottom border-turquoise-dark"><h3><?php echo JrTexto::_("References"); ?></h3></div>                            
                    </div>
                    </div>
                </div>
            </div>
         </div>
    </div> 
</div>
