<style type="text/css">
.panelcont-xs>.btn-panel{
    font-size:1.5em !important;
}
@media (max-width: 1200px){
    .panelcont-xs>.btn-panel{
        font-size:1.2em !important;
    }
}
@media (max-width: 1100px){
    .panelcont-xs>.btn-panel{
        font-size:0.8em !important;
    }
}
@media (max-width: 980px){
    .panelcont-xs>.btn-panel{
        font-size:1.5em !important;
    }
}
#pnl-cursos .panel-body{
    padding-bottom: 10px;
    padding-top: 10px;
}

.imggris {
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    filter: grayscale(100%);
}
.btn-panel-container {
    min-height:180px;
}
@media (max-height: 800px) and (min-width: 1250px){
    #inicio .btn-panel-container.panelcont-xs>.btn-panel, #inicio #botones-menu div.btn_submenu {
        font-size:1.6em;
        height: 135px;
    }
}

</style>
<div class="container">
<div class="" id="inicio"> 
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="panel border-red" id="pnl-cursos">
                <div class="panel-heading bg-red">
                    <h4 class="panel-titulo">
                        <i class="fa fa-bookmark"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('My English Courses'); ?>
                    </h4>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <?php if(!empty($this->cursos)){ ?>
                    <div class="row slider-cursos" style="margin-bottom: 0;">
                        <?php foreach ($this->cursos as $c) { if($c["idcurso"]){
                            $nopermite=array();
                            //<?php echo ($c["idcurso"]==4||$c["idcurso"]==5)?'not-allowed':'pointer'
                            ?>
                           <!--if($c["idcurso"]!=31)-->
                            <a style="cursor:pointer" href="<?php echo $this->documento->getUrlBase().'/curso/?id='.$c['idcurso']; ?>" class=" col-xs-6 col-sm-3 item-curso" title="<?php echo $c['nombre'] ?>">
                            <img src="<?php echo $this->documento->getUrlBase().$c['imagen']; ?>" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center"><?php echo $c['nombre']; ?><br>
                            <div class="text-center" style="font-size: 0.6ex; color: #347ab7;"><?php //echo $c["strcurso"]; ?> <br><?php //echo $c["strdocente"]; ?></div></div>
                        </a>
                        <?php }} ?>
                    </div>
                    <?php }else{ ?>
                    <h3 class="text-center bolder color-grey-dark" style="height: 144px; width: 100%"><?php echo JrTexto::_('You do not have assigned courses'); ?></h3>
                    <?php } ?>
                </div>
            </div>
        </div> 
    </div>  
    <div class="row text-center">
        
        <!--div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs text-center">
            <a href="<?php echo $this->documento->getUrlBase();?>/examenes" class="pnlblock1 btn-block btn-green btn-panel hvr-float-shadow">
                <i class="btn-icon btn-icon fa fa-list"></i>
                <?php echo ucfirst(JrTexto::_('Exams')); ?>
            </a>
        </div-->
        <div class="col-md-3 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/tarea" class="pnlblock1 btn-block btn-green btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-tasks"></i>
                <?php echo ucfirst(JrTexto::_('Activities assignment')); ?>
            </a>
        </div>
        
        <div class="col-md-3 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/ebooks" class=" pnlblock1 btn-block btn-yellow btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-address-book-o"></i>
                <?php echo ucfirst(JrTexto::_('Reference books on methodology and grammar')); ?>
            </a>
        </div>
        <!-- <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca" class="btn-block btn-red btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-book"></i>
                <?php echo ucfirst(JrTexto::_('Library')); ?>
            </a>
        </div> -->
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/library/library" class="btn-block btn-red btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-book"></i>
                <?php echo ucfirst(JrTexto::_('Library')); ?>
            </a>
        </div>
        <!--div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/docente/panelcontrol" class=" pnlblock1 btn-block btn-purple btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-tachometer"></i>
                <?php //echo ucfirst(JrTexto::_('Tracking Students')); ?>
            </a>
        </div-->
        
        <!-- div class="col-md-3 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php //echo $this->documento->getUrlBase();?>/smartcourse" class="btn-block btn-blue btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-briefcase"></i>
                <?php //echo ucfirst(JrTexto::_("Teacher's portfolio")); ?>
            </a>
        </div-->
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn-block btn-turquoise btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-bar-chart"></i>
                <?php echo ucfirst(JrTexto::_("Reports")); ?>
            </a>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/academico" class="btn-block btn-orange btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-graduation-cap"></i>
                <?php echo ucfirst(JrTexto::_('Academic')); ?>
                <span class="badge badge-primary" id="badgeexayact" style="background: #066dc5; right: -0.6em; top: -0.6em;">0</span>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 btn-panel-container panelcont-xs"  >
            <a href="<?php echo $this->documento->getUrlBase();?>/Workbooks/" class="btn btn-block btn-blue btn-panel hvr-float-shadow" style="background-color: #3910DE;">
                <i class="btn-icon fa fa-file-word-o"></i>
                <?php echo ucfirst(JrTexto::_('Workbooks')); ?>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="<?php echo $this->documento->getUrlBase();?>/guiapedagogica/" class="btn btn-block btn-orange btn-panel hvr-float-shadow">
                <i class="btn-icon fa fa-folder-open"></i>
                <?php echo ucfirst(JrTexto::_('Manual of Pedagogical Orientations')); ?>
            </a>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs">
            <a href="#" data-source="<?php echo $this->documento->getUrlSitio()?>/sidebar_pages/diccionario" class="btn btn-block btn-yellow-dark btn-panel hvr-float-shadow slide-sidebar-right">
                <i class="btn-icon fa fa-flag"></i>
                <?php echo ucfirst(JrTexto::_('Dictionary')); ?>
            </a>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs ">
            <a href="#"  data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion" class="btn btn-block btn-primary btn-panel hvr-float-shadow slide-sidebar-right">
                <i class="btn-icon fa fa-bullhorn"></i>
                <?php echo ucfirst(JrTexto::_('Pronunciation'));?>
            </a>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs"> 
            <a href="<?php echo $this->documento->getUrlBase();?>/smartcourse/cursos/ver/?idcurso=37&returnlink=<?php echo $this->documento->getUrlBase();?>" class="btn btn-block btn-green btn-panel hvr-float-shadow" >
                <i class="btn-icon fa fa-handshake-o"></i>
                <?php echo ucfirst(JrTexto::_('Digital manual')); ?>
            </a>
        </div>
        
        
    </div>    
</div>
</div>
<div class="hidden">
    <div class="examendeubicacion">
        <div class="titulo"><h1><?php echo JrTexto::_('Take the Placement Test!'); ?></h1></div>
        <div class="content">
            <div class="row">          
                <div class="col-md-12 text-center">                       
                        <h3><?php echo JrTexto::_('This test provides important information <br>that allows the students to be placed <br>in the appropriate level of english.'); ?></h3>
                    <a href="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.$this->idexamenUbicacion;?>&tipoexamen=EU" class="btn btn-success hvr-float-shadow"><?php echo JrTexto::_('Start'); ?> <i class="btn-icon fa fa-hand-o-right"></i></a>            
                </div>
                <div class="col-md-12"><br><hr><br></div>
            <div class="col-md-6 text-left">
                <a class="btnsaltar btn btn-primary" href="#" role="button"><?php echo JrTexto::_('Skip'); ?> </a>
            </div>
            <div class="col-md-6 text-right">
                <a class="btntomarotrodia btn btn-primary" href="#" role="button"><?php echo JrTexto::_('Take it another day'); ?></a>
            </div>           
            </div>
        </div>                        
    </div>
</div>
<?php
$storage_flag = ($this->localstorage_flag == true) ? 'true' : 'false';
$idpersona = $this->user['idpersona'];
$idhistorial = $this->user['idHistorialSesion'];
$fecha = date('Y-m-d H:i:s');
$idproyecto = $this->user['idproyecto'];
?>
<script type="text/javascript">
$(document).ready(function(){
    var localstorage_flag = <?php echo $storage_flag ?>;
    var idpersona = <?php echo $idpersona ?>;
    var idproyecto = <?php echo $idproyecto ?>;
    var idhistorial = <?php echo $idhistorial ?>;
    var fecha = '<?php echo $fecha ?>' ;
    

    if(localstorage_flag == true){
        //registro actual de sesion actualizar fecha de salida al actual (evitar cualquier error)
        $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': idhistorial,'fechasalida' : fecha},
        }).done(function(resp) {
        }).fail(function(xhr, textStatus, errorThrown) { 
            return false; 
        });

        if(userinfo = localStorage.getItem("userinfo")){
            // console.log("existe storage");
            var jsonParseado = JSON.parse(userinfo);
            //verificar si es el mismo id que el anterior de no serlo cambiarlo y actualizar
            if(jsonParseado.idusuario == idpersona){
                //buscar el id del hitorial para actualizar su fecha de salida al actual
                $.ajax({
                    url: _sysUrlBase_+'/tools/insertarfechahistorial',
                    type: 'POST',
                    dataType: 'json',
                    data: {'idhistorialsesion': jsonParseado.idhistorial ,'fecha' : jsonParseado.fecha, 'idproyecto': idproyecto},
                    async : false
                }).done(function(resp) {
                    console.log(resp);
                }).fail(function(xhr, textStatus, errorThrown) { 
                    return false; 
                });
            }
            var json = new Object();
            json.idusuario = idpersona;
            json.idhistorial = idhistorial; //agarrar el ultimo id de historial sesion
            json.fecha = '<?php echo $fecha ?>';//date('Y-m-d H:i:s');
            var stringJSON = JSON.stringify(json);

            //lineas para realizar cuando existe storage
            localStorage.setItem("userinfo", stringJSON);
        }else{
            var json = new Object();
            json.idusuario = idpersona;
            json.idhistorial = idhistorial; //agarrar el ultimo id de historial sesion
            json.fecha = '<?php echo $fecha ?>';//date('Y-m-d H:i:s');
            var stringJSON = JSON.stringify(json);
            
            $.ajax({
                url: _sysUrlBase_+'/tools/insertarfechahistorial',
                type: 'POST',
                dataType: 'json',
                data: {'idpersona': idpersona,'fecha' : json.fecha, 'idproyecto': idproyecto},
                async : false
            }).done(function(resp) {
                console.log(resp);
            }).fail(function(xhr, textStatus, errorThrown) {
                return false; 
            });
            //crear web storage
            // sessionStorage.userinfoSesion = stringJSON;
            localStorage.setItem("userinfo", stringJSON);
        }
    }

    $('.showmodal').on('click',function(){
        $htmlid=$($(this).attr('data-idmodal'));
        if($htmlid.length){
            titulo=$htmlid.children('h4').html();
            htmlid=$htmlid.children('h4').hide().parent().html();           
            sysmodal({titulo:titulo,htmltxt:htmlid});
        }        
    })
    var linktmp='';
    var showexamenubi='<?php echo $this->showExaUbicacion;?>';
    var idexamenubicacion=parseInt(<?php echo $this->idexamenUbicacion;?>);    
    var isdemo=<?php echo !empty($this->esdemo)?1:0; ?>;

    $('.slider-cursos a').click(function(ev){       
        if(showexamenubi!=''&&isdemo==0){
            ev.preventDefault();
            linktmp=$(this).attr('href'); 
            sysmodal({titulo:$('.examendeubicacion .titulo').text(),htmltxt:$('.examendeubicacion .content').html()});
        }
    })
   
    $('.modal').on('click','.btnsaltar',function(ev){
        ev.preventDefault();
        let dt=new FormData();
            dt.append("tipo", "EU");
            dt.append('datos',JSON.stringify({'idexamen':idexamenubicacion,'fecha':'<?php echo date('Y-m-d') ?>','accion':'No'}));          
            sysajax({url:_sysUrlBase_+'/persona_setting/guardarPersona_setting',fromdata:dt,callback:function(rs){ 
                if(linktmp!='') window.location.href=linktmp;
            }
        })

    }).on('click','.btntomarotrodia',function(ev){
        ev.preventDefault();
        let dt=new FormData();
            dt.append("tipo", "EU");
            dt.append('datos',JSON.stringify({'idexamen':idexamenubicacion,'fecha':'<?php echo date('Y-m-d') ?>','accion':'MA'}));
            sysajax({url:_sysUrlBase_+'/persona_setting/guardarPersona_setting',fromdata:dt,callback:function(rs){
                if(linktmp!='') window.location.href=linktmp;
            }
        })
    })


    var buscaractividadesoffline=function(n){
        var fd = new FormData();   
        $.ajax({
            url: _sysUrlBase_+'/actividad_alumno/nactividadoffline',
            type: "POST",
            data:  fd,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            beforeSend:function(){
                $('#badgeexayact').html('<i class="fa fa-spinner fa-spin"></i>')
            },
            success: function(data){
                if(data.code==='ok'){
                    $('#badgeexayact').html(n+data.nactividades);
                }else{
                    $('#badgeexayact').remove();                
                }
            },
            error: function(xhr,status,error){
               $('#badgeexayact').html('Error');
            }               
        });
    }


    var fd = new FormData();   
    $.ajax({
        url: _sysUrlBase_+'/actividad_alumno/nexamenesoffline',
        type: "POST",
        data:  fd,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend:function(){
            $('#badgeexayact').html('<i class="fa fa-spinner fa-spin"></i>')
        },
        success: function(data){
            if(data.code==='ok'){
                buscaractividadesoffline(data.nexamenes);
            }else{
                buscaractividadesoffline(0);
            }
        },
        error: function(xhr,status,error){
          buscaractividadesoffline(0);
        }               
    });
    localStorage.setItem('esdemo',isdemo);
})
</script>