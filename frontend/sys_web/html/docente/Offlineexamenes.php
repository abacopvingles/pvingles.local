<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>" onsubmit="return false;">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">  
          <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="idgrupoauladetalle" class="form-control idgrupoauladetalle">
                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php if(!empty($this->miscursos))
                    foreach ($this->miscursos as $fk) { ?>
                      <option data-idcurso="<?php echo $fk["idcurso"]?>" value="<?php echo $fk["idgrupoauladetalle"]?>" ><?php echo $fk["strcurso"]." : ".$fk["aniopublicacion"]." : ".$fk["strlocal"] ?></option><?php } ?>
                </select>
              </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-6 form-group">
              <label><?php echo  ucfirst(JrTexto::_("student")." (".JrTexto::_('Name')."/DNI)")?></label>
              <div class="input-group" style="margin:0px">
                <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo JrTexto::_('Search') ?> <i class="fa fa-search"></i></span>  
              </div>
            </div> 
          </div>
          <button id="actualuarmatricula123" class="hide">actualizar</button>         
          </form>  
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide " >
    <div class="panel" id="vista<?php echo $idgui; ?>">
    </div>
  </div>
</div>
<div class="form-view" id="ventanatabla_<?php echo $idgui; ?>" >
  <div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="panel">         
         <div class="panel-body table-striped table-responsive">
            <table class="table " style="min-width:100%">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Students") ;?></th>
                  <th><?php echo JrTexto::_("Exam") ;?></th>                           
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
<section id="msjes_idioma_php" class="hidden" style="display: none !important; ">
    <input type="hidden" id="you_failed_exam" value='<?php echo ucfirst(JrTexto::_('You failed the assessment')); ?>'>
    <input type="hidden" id="you_passed_exam" value='<?php echo ucfirst(JrTexto::_('You passed the assessment')); ?>'>
    <input type="hidden" id="your_score_below" value='<?php echo ucfirst(JrTexto::_('Your score is below the preset scales')); ?>'>
    <input type="hidden" id="out_of" value='<?php echo JrTexto::_('out of'); ?>'>
    <input type="hidden" id="your_result_in_scale" value='<?php echo ucfirst(JrTexto::_('your result is located on the scale')); ?>'>
</section>
<script type="text/javascript">
 var edithtml_all=function(obj){
     tinyMCE.init({
      relative_urls : false,
      convert_newlines_to_brs : true,
      menubar: false,
      statusbar: false,
      verify_html : false,
      content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
      selector: '#'+obj.id,
      height: 300,          
      plugins:["textcolor advlist " ], 
      toolbar: 'undo redo | advlist | styleselect |  bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  forecolor backcolor'
    });
}


var tabledatos5bd4788e236d6='';
var estados5bd4788e236d6={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
var tituloedit5bd4788e236d6='<?php echo ucfirst(JrTexto::_("acad_matricula"))." - ".JrTexto::_("edit"); ?>';
var draw5bd4788e236d6=0;
  function refreshdatos5bd4788e236d6(){
      tabledatos5bd4788e236d6.ajax.reload();
  }
$(document).ready(function(){
  cargarMensajesPHP('#msjes_idioma_php');
  tabledatos5bd4788e236d6=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "pageLength": 50,
      "searching": false,
      "processing": false,
      "serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Students") ;?>'},
        {'data': '<?php echo JrTexto::_("Exam") ;?>'},
        /*{'data': '<?php //echo JrTexto::_("Telephone") ;?>'},
        {'data': '<?php //echo JrTexto::_("usuario") ;?>'},
        {'data': '<?php //echo JrTexto::_("email") ;?>'},
        {'data': '<?php //echo JrTexto::_("Fecha_matricula") ;?>'},
        {'data': '<?php //echo JrTexto::_("Estado") ;?>'},*/
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/actividad_alumno/examenesoffline/?json=true',
        type: "post",
        data:function(d){
            d.json=true;
            d.texto=$('#texto').val();
            var select=$('select[name="idgrupoauladetalle"]');
            var idcurso=select.find('option:selected').attr('data-idcurso');          
            d.idgrupoauladetalle=select.val()||-1;
            d.solooffline=true;
            d.idcurso=idcurso;
            draw5bd4788e236d6=d.draw;
            d.iddocente='<?php echo $this->usuarioAct["idpersona"]; ?>';
        },
        "dataSrc":function(json){
          var data=json.data;                       
          json.draw = draw5bd4788e236d6;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();          
          for(var i=0;i< data.length; i++){            
            var htmlexa='';
            var examenes=data[i].examenes;          
            if(examenes.length)
              $.each(examenes,function(ii,v){                
                htmlexa='<div> <a href="#" class="calificarexamenesoff" data-id="'+v.idnota+'">'+v.nombre+'</a>';
              })
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Students") ;?>':'DNI:'+data[i].dni+" <br><span class='stralumno' data-idalumno='"+data[i].idalumno+"'>"+ data[i].stralumno+'</span>', //data[i].idalumno,
              '<?php echo JrTexto::_("Exam") ;?>':htmlexa,             
              /*'<?php// echo JrTexto::_("email") ;?>':data[i].alumno_email,
              '<?php //echo JrTexto::_("Fecha_matricula") ;?>': data[i].fecha_registro,
              '<?php //echo JrTexto::_("Estado") ;?>': data[i].estado,*/
              '<?php echo JrTexto::_("Actions") ;?>' :
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/perfil/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Ficha'); ?> '+data[i].stralumno+'"><i class="fa fa-eye"></i></a>',
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/formulario/?idpersona='+data[i].idalumno+'&idrol=3&datareturn=false&buscarper=no" data-titulo="<?php //echo JrTexto::_('Student')." ".JrTexto::_('Edit'); ?>"><i class="fa fa-pencil"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash-o"></i></a>'

            });
          }
          return datainfo; 
        }, error: function(d){console.log(d)}
      },
      "language": { "url": _sysIdioma_.toUpperCase()!='EN'?(_sysUrlBase_+"/static/libs/datatable1.10/idiomas/"+_sysIdioma_.toUpperCase()+".json"):''}
      
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'setCampo', id,campo,data);
          if(res) tabledatos5bd4788e236d6.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd4788e236d6';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_matricula';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{footer:false,borrarmodal:true,callback:function(_modal){}});
  }).on('click','.calificarexamenesoff',function(i,v){
    var btn=$(this);
    var idnota=$(this).attr('data-id');
    var titulo=$(this).html(); 
    var data=new FormData();
        data.append('idnota',idnota);
    sysajax({fromdata:data,url:_sysUrlBase_+'/notas_quiz/buscarjson',callback:function(rs){
      var html='';
      var totalquestion=0;
      var npreguntas=0;
      var cpor='E';
      var ctotal=100;
      var cen='P';   
      var cmin=51;
      var pxpregunta=0
      var preg={};
      var pregoff={};
      var data='';
      $.each(rs.data,function(i,v){
        data=v;    
        preg=JSON.parse(v.preguntas);
        pregoff=JSON.parse(v.preguntasoffline);
         cen=v.calificacion_en||'P';
         $.each(preg,function(ii,vv){
         var htmltmp=vv;
          htmltmp=htmltmp.replace('__xRUTABASEx__',_sysUrlBase_);
          htmltmp=htmltmp.replace('__xRUTABASEx__',_sysUrlBase_);          
          if($(htmltmp).hasClass('tmpl-ejerc')) npreguntas++;
         })
        i=0;       
        $.each(pregoff,function(ii,vv){
          html+='<div data-id="'+ii+'" class="verquestion '+(i==0?'active':'hidden')+'"><h1 class="text-center">'+(i+1)+'/<span class="totalquestion">0<span></h1><div class="question">'+preg[ii]+'</div></div>';
          totalquestion++;
          i++;
        })
      })
      var url='&plt=modal';
      //var htmltmp=$(this).attr('data-idver');
      openModal('lg','','',true,'',{footer:false,borrarmodal:true,callback:function(_modal){
        html=html.replace('__xRUTABASEx__',_sysUrlBase_);
        html=html.replace('__xRUTABASEx__',_sysUrlBase_);      
        _modal.find('.modal-header').find('#modaltitle').html(titulo);
        _modal.find('.modal-body').html(html);
        _modal.find('.modal-body').find('.totalquestion').text(totalquestion);
        _modal.find('.modal-body').find('.showonpreview_nlsw').show();
        _modal.find('.modal-body').find('#btns_control_alumno').hide();
        var textareatmp=_modal.find('.respuestaAlumno').children('textarea');
       textareatmp.hide();
       textareatmp.siblings('.mce-tinymce').remove();
       textareatmp.siblings('.msjRespuesta').html(textareatmp.val());
       _modal.find('.btnadd_recordAlumno').hide();

        htmlfooter='<div class="row"><div class="col-md-4 col-sm-12"></div><div class="col-md-4 col-sm-12 form-group text-left "><label><?php echo JrTexto::_('Qualification'); ?></label><div class="cajaselect">';

      htmlfooter+='<select name="progresoalumno" class="form-control progresoalumno">';
      var progreso=btn.attr('data-progreso');
      for(i=100;i>=0;i--){  htmlfooter+='<option value="'+i+'" '+(progreso==i?'selected="selected"':'')+'>'+i+'%</option>';}

      htmlfooter+='</select></div><div class="col-md-4 col-sm-12"></div></div><div class="col-md-12"><button type="button" class="btn btn-primary btnsavenota"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save'); ?></button><button type="button" class="btn btn-default cerrarmodal" data-dismiss="modal"><i class="fa fa-close"></i> <?php echo JrTexto::_('Close'); ?></button></div>';
      _modal.find('#modalfooter').html(htmlfooter);

      _modal.find('.btnsavenota').on('click',function(ev){
       htmlsave=_modal.find('.modal-body').clone();
       htmlsave.find('.showonpreview_nlsw').show();
       htmlsave.find('#btns_control_alumno').show();
       htmlsave.find('.btnadd_recordAlumno').show();
       var progreso=_modal.find('select.progresoalumno').val();
       var verquestionactive=_modal.find('.verquestion.active');
       var nextquestion=verquestionactive.next();
       pxpregunta=ctotal/npreguntas;
       var totalitem=100;
       var itemsgood=progreso;
       var itemsbad=100-progreso;
       var puntajeacumulado=(pxpregunta*progreso)/100;
       verquestionactive.children('.question').children('.tmpl-ejerc').attr('data-itemsbad',itemsbad);
       verquestionactive.children('.question').children('.tmpl-ejerc').attr('data-itemsgood',parseInt(itemsgood));
       verquestionactive.children('.question').children('.tmpl-ejerc').attr('data-score',puntajeacumulado);
       idpreg=verquestionactive.attr('data-id')||'';

       var html2=verquestionactive.children('.question').html();
       htmltmp=html2.replace(_sysUrlBase_,'__xRUTABASEx__');      

       preg[idpreg]=htmltmp;
       var pregoff2={};
        $.each(pregoff,function(ii,vv){
          if(ii!=idpreg) pregoff2[ii]=vv;
        })
        pregoff=pregoff2;

        calcularpuntaje({data:data,preg:preg,pregoff:pregoff,npreguntas:npreguntas});
      if(nextquestion.length){
        nextquestion.addClass('active').removeClass('hidden');
        verquestionactive.addClass('hidden').removeClass('active');
        var scorepregunta=nextquestion.children('.question').children('.tmpl-ejerc').attr('data-score');
        var pselect=(100*scorepregunta)/pxpregunta;
        _modal.find('select.progresoalumno').val(Math.floor(pselect));
      }else{
        _modal.find('.cerrarmodal').click();
        btn.addClass('hidden');
      }

       
      })
      }});

    }})
  })

  $('#frmEstudiantes<?php echo $idgui;?>').on('change','select.idgrupoauladetalle',function(ev){
       refreshdatos5bd4788e236d6();
  })
 
  $('.btnbuscar').click(function(ev){
    refreshdatos5bd4788e236d6();
  });

  var calcularpuntaje=function(data){
    var datos=data.data;
    var  cpor='E';
    var  cen=datos.calificacion_en||'P';
    var  ctotal=parseInt(datos.calificacion_total||100);
    var  cmin=parseInt(datos.calificacion_min||51);
    var npreguntas= data.npreguntas;
    var pxpregunta=0;
    var pacumuladatotal=0;
    var habilidades={};
    var datah={};
    var arrEscala = [];
    var allpreguntas=data.preg;
    if(cpor=='E' && cen=='A'){
        var escalaAlfab = $.parseJSON(ctotal);
        ctotal = 100;
        cmin = 51; /* el minimo para aprobar Alfanumerico */
    }

    if(cpor=='Q' && cen=='N'){
        ctotal = 0;
        $.each(allpreguntas,function(ii,vv){
          if($(vv).hasClass('tmpl-ejerc')){
            var plantilla= $(vv);
            var puntaje = plantilla.attr('data-puntaje')||0;
            ctotal = parseFloat(ctotal) + parseFloat(puntaje); 
          } 
        })
    } else {
        pxpregunta=ctotal/npreguntas;
    }

    $.each(allpreguntas,function(ii,vv){
      htmltmp=vv.replace('__xRUTABASEx__',_sysUrlBase_);
      vv=htmltmp.replace('__xRUTABASEx__',_sysUrlBase_);
      if($(vv).hasClass('tmpl-ejerc')){
        var plantilla= $(vv);                
        if(cpor=='Q' && cen=='N'){
            var puntaje = plantilla.attr('data-puntaje')||0;
            pxpregunta = puntaje;
        }
        var tmpl=plantilla.attr('data-tmpl')||'';
        if(tmpl!=''){
            var totalitems=plantilla.attr('data-items')||0;
            var itemsgood=plantilla.attr('data-itemsgood')||0;
            var itemsbad=plantilla.attr('data-itemsbad')||0;
            var tpl_skill=$(vv).attr('data-habilidades')||'';           
            var puntajeacumulado=plantilla.attr('data-score')||0;
            pacumuladatotal=pacumuladatotal+parseInt((plantilla.attr('data-score')||0))
            var skills=tpl_skill.split('|');
            $.each(skills,function(key,val){
                if(val!=''){
                    if(datah[val]==''||datah[val]==undefined){
                        datah[val]={'n':1,'p':parseFloat(puntajeacumulado),'pt':parseFloat(pxpregunta)};
                    }else{
                        datah[val].n++;
                        datah[val].p=parseFloat(datah[val].p)+parseFloat(puntajeacumulado);
                        datah[val].pt=parseFloat(datah[val].pt)+parseFloat(pxpregunta);
                    }
                    habilidades[val]=datah[val];
                }                       
            });           
        }        
      } 
    })
    datos.pxpregunta=pxpregunta;
    datos.pacumuladatotal=pacumuladatotal.toFixed(2);
    var sb='';
    var html='';
    if(pacumuladatotal>cmin) {
        var resultado_text = 'pass';
        html='<div class="color-green2"><h2>'+MSJES_PHP.you_passed_exam+'</h2>';
    } else {
        var resultado_text = 'fail';
        html= '<div class="color-red"><h2>'+MSJES_PHP.you_failed_exam+'</h2>';
    }

    if(cen=='P') html+='<h3> '+(pacumuladatotal.toFixed(2))+'% de '+ctotal+'% </h3>';
    else if(cen=='A'){
        var ptosAcum = pacumuladatotal.toFixed(2);
        if(ptosAcum>100) ptosAcum = 100;
        var obj = {};

        $.each(escalaAlfab, function(i, escala) {
            if( ptosAcum>parseFloat(escala.min) && ptosAcum<=parseFloat(escala.max) ){
                obj = {
                    'nombre' : escala.nombre,
                    'puntaje_min' : escala.min,
                    'ptosAcum' : ptosAcum,
                };
            }
        });
        if($.isEmptyObject(obj)){
            html += '<h3> '+MSJES_PHP.your_score_below+'. <br>'+ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h3>';
        } else {
            html += '<h3> '+MSJES_PHP.your_result_in_scale+': "<b>'+obj.nombre+'</b>". </h3><h4>'+obj.ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h4>';
        }
    }
    else html+='<h3> '+(pacumuladatotal.toFixed(2))+' '+MSJES_PHP.out_of+' '+ctotal+'pts </h3>';
    html+='</div>';
    datos.infototalcalificacion=html;
   // datos.dataresultado=resultado_text;
    datos.datapuntaje=pacumuladatotal.toFixed(2);
    var jsonHabilidades={};
    $.each(habilidades,function(key,val){
        var p=val.p||0;
        var pt=parseFloat(val.pt)||0;
        if(p>0&&pt>0){
            var val1=(p*100)/pt;
            jsonHabilidades[key]=val1.toFixed(0);
        }else{
           jsonHabilidades[key]=0;
        }
    });

    datos.preguntas=JSON.stringify(allpreguntas);
    datos.preguntasoffline=data.pregoff!={}?JSON.stringify(data.pregoff):'';
    datos.puntajeXHab=JSON.stringify(jsonHabilidades);
    var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
    var fd = new FormData();
        fd.append("idcursodetalle", datos.idcursodetalle);
        fd.append('preguntas', datos.preguntas); 
        fd.append("idrecurso", datos.idrecurso);
        fd.append("idalumno", datos.idalumno);
        fd.append('tipo', datos.tipo);
        fd.append('nota', datos.datapuntaje);
        fd.append('notatexto', datos.infototalcalificacion);
        fd.append('regfecha', datos.regfecha);
        fd.append("idproyecto", datos.idproyecto);
        fd.append('calificacion_en',datos.calificacion_en);
        fd.append("calificacion_total", datos.calificacion_total);            
        fd.append('calificacion_min', datos.calificacion_min);
        fd.append("tiempo_total", datos.tiempo_total); 
        fd.append('tiempo_realizado', datos.tiempo_realizado);
        fd.append('calificacion', datos.calificacion);
        fd.append("habilidades", datos.habilidades); 
        fd.append('habilidad_puntaje', datos.puntajeXHab);   
        fd.append('intento', datos.intento);
        fd.append('idexamenalumnoquiz', datos.idexamenalumnoquiz);
        fd.append('idexamenproyecto', datos.idexamenproyecto);
        fd.append('idexamenidalumno', datos.idexamenidalumno);
        fd.append('preguntasoffline',datos.preguntasoffline);
        fd.append('update',true);                
        $.ajax({
            url: _sysUrlBase_+'/notas_quiz/guardarNotas_quiz',
            type: "POST",
            data:  fd,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            success: function(data){
                if(data.code==='ok'){
                    mostrar_notificacion(msjeAttention, data.msj, 'success');
                }else{
                    mostrar_notificacion(msjeAttention, data.msj, 'warning');
                }
            },
            error: function(xhr,status,error){
                mostrar_notificacion(msjeAttention, status, 'warning');                       
                return false
            }               
        });
  };  
});
</script>