<?php 
    $arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
    $cantHabilidades = count($this->habilidades);
    $tiempos = [
        0=>['id'=>'tiempo_total', 'title'=>JrTexto::_('Total accumulated time'), 'col'=>'col-xs-12'],
        1=>['id'=>'tiempo_actividades', 'title'=>JrTexto::_('Time accumulated in activities'), 'col'=>'col-xs-12'],
        2=>['id'=>'tiempo_teacher_resrc', 'title'=>JrTexto::_('Time accumulated in Smartbook'), 'col'=>'col-xs-12'],
        3=>['id'=>'tiempo_games', 'title'=>JrTexto::_('Time accumulated in games'), 'col'=>'col-xs-12'],
        4=>['id'=>'tiempo_examenes', 'title'=>JrTexto::_('Time accumulated in exams'), 'col'=>'col-xs-12'],
    ];
?>
<style>
    #cpanel_docente .widget-box .widget-body{
        height: auto;
    }
    #contenedor-pnls-info .widget-box .widget-body{
        min-height: 460px;
        max-height: 470px;
    }
    #pnl-datosfiltro .listado-alums{
        max-height: 100px; 
        overflow: auto;
    }
    #chart_progreso_linear .chart-leyenda,
    #chart_progreso_pie .chart-leyenda{
        border: 1px solid;
        border-color: rgba(0,0,0,0.2);
    }
    #chart_progreso_linear .chart-leyenda .caja-color,
    #chart_progreso_pie .chart-leyenda .caja-color{
        display: inline-block;
        height: 1em;
        width: 1.5em; 
    }
    #chart_progreso_linear .chart-leyenda span,
    #chart_progreso_pie .chart-leyenda span{
        display: inline-block;
    }

    .card{
        display: block;
        width: 100%;
        border: 1px solid #ccc;
        padding: 0;
    }
    .card .card-title{
        font-size: 15px;
        font-weight: bold;
        border-bottom: 1px solid #ccc;
        padding-left: 15px;
        line-height: 2.2;
    }
    .card .card-info{
        font-size: 2.5em;
        text-shadow: 2px 1px 3px #aaa;
        font-weight: bolder;
        text-align: center;
        border-bottom: 1px solid #efefef;
    }
    .card .card-info:last-child{ border-bottom:none; }

    .card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }

    .card.card-time .card-info .anio:after,
    .card.card-time .card-info .mes:after,
    .card.card-time .card-info .dia:after,
    .card.card-time .card-info .hora:after,
    .card.card-time .card-info .min:after,
    .card.card-time .card-info .seg:after{
        font-size: 0.7em;
    }
    .card.card-time .card-info .anio:after{ content: "y"; }
    .card.card-time .card-info .mes:after { content: "m"; }
    .card.card-time .card-info .dia:after { content: "d"; }
    .card.card-time .card-info .hora:after{ content: "h"; }
    .card.card-time .card-info .min:after { content: "m"; }
    .card.card-time .card-info .seg:after { content: "s"; }

    .card .card-info .promedio{
        line-height: 75px;
    }
    .card .card-info .comentario span{
        display: block;
    }
    .card .card-info .comentario span.letras{
        font-size: .5em;
    }

    kbd{
        margin-right: 4px;
        margin-bottom: 4px;
        display: inline-block;
    }

    .slick-prev { left: 10px !important; }
    .slick-next { right: 10px !important; }
    .slick-slider { margin-bottom: 5px !important; }

    #chart-lines .ct-label{ font-size: 1em !important; }
    #chart-pie .ct-label{ fill: #fff; font-size: 1.2em !important; font-weight: bolder; }
    
    .ct-series { fill: none; }
    .ct-label { font-family: Arial, Helvetica, sans-serif; }
    <?php foreach ($arrColoresHab as $key=>$color) { ?>
    .ct-series-<?php echo chr(97+$key) ?> .ct-line,
    .ct-series-<?php echo chr(97+$key) ?> .ct-point,
    .ct-series-<?php echo chr(97+$key) ?> .ct-bar, 
    .ct-series-<?php echo chr(97+$key) ?> .ct-line, 
    .ct-series-<?php echo chr(97+$key) ?> .ct-point, 
    .ct-series-<?php echo chr(97+$key) ?> .ct-slice-donut{
      stroke: <?php echo $color; ?>;
    }
    .ct-series-<?php echo chr(97+$key) ?> .ct-slice-pie{
      fill: <?php echo $color; ?>;
    }
    <?php } ?>

    .numeracion{ font-style: italic; font-size: 1.5em; margin-right: 5px; }
    .numeracion>span:first-child{ font-size: 1.7em; font-style: italic; }
    .numeracion>span:nth-child(2):before{ content: '/ '; }
    
    .bg-lilac { background-color: #a286ff; color: #fff; }
    .bg-green2 { background-color: #2ed353; color: #ffffff; }
    .bg-pink { background-color: #ffb283; color: #ffffff; }
</style>

<div class="" id="cpanel_docente">
    <!--div class="row" id="breadcrumb"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
            <?php foreach (@$this->breadcrumb as $b) {
            $enlace = '<li>';
            if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
            else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
            $enlace .= '</li>';
            echo $enlace;
            } ?>
        </ol>
    </div> </div-->
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3">
            <?php if(empty(@$this->idCurso)){ ?>
            <div class="widget-box" id="pnl-filtros">
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-xs-6 col-sm-12 form-group select-ctrl-wrapper select-azul">
                                <select name="opcColegio" id="opcColegio" class="form-control select-ctrl ">
                                    <option value="-1">- <?php echo JrTexto::_('Select School'); ?> -</option>
                                    <?php foreach ($this->locales as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-12 form-group select-ctrl-wrapper select-azul">
                                <select name="opcAula" id="opcAula" class="form-control select-ctrl ">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-12 form-group select-ctrl-wrapper select-azul">
                                <select name="opcGrupo" id="opcGrupo" class="form-control select-ctrl ">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-12 form-group select-ctrl-wrapper select-azul">
                                <select name="opcAlumno" id="opcAlumno" class="form-control select-ctrl ">
                                    <option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>
                                </select>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-block filtrar"><?php echo ucfirst(JrTexto::_('filter')); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <div class="widget-box <?php echo !empty(@$this->idCurso)?"hidden":""; ?>" id="pnl-datosfiltro" style="display:none;">
                <div class="widget-body">
                    <div class="widget-main">
                        <div class="row">
                            <div class="col-xs-12">
                                <img src="" alt="foto" class="img-responsive center-block foto-alumno" style="max-height: 100px;">
                            </div>
                            <div class="col-xs-12" style="padding: 0;">
                                <label class="col-xs-12">Student(s):</label>
                                <ul class="col-xs-12 list-unstyled listado-alums"> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if(!empty(@$this->idCurso)){ ?>
        <input type="hidden" name="hIdCurso" id="hIdCurso" value="<?php echo $this->idCurso; ?>">
        <input type="hidden" name="hIdAlumno" id="hIdAlumno" value="<?php echo $this->usuarioAct["dni"]; ?>">
        <input type="hidden" name="hIdAlumno2" id="hIdAlumno2" value="<?php echo $this->usuarioAct["idpersona"]; ?>">
        <input type="hidden" name="hIdGrupo" id="hIdGrupo" value="<?php echo $this->cursoActual["idgrupoauladetalle"]; ?>">
        <?php } ?>

        <div class="col-xs-12 <?php echo empty(@$this->idCurso)?"col-sm-8 col-md-9":''; ?>" id="contenedor-pnls-info">
            
            <div class="col-xs-12">
                <div class="widget-box" id="tiempos">
                    <div class="widget-header bg-lilac">
                        <h4 class="widget-title">
                            <i class="fa fa-clock-o"></i>
                            <span><?php echo JrTexto::_('Time on the virtual platform'); ?></span>
                        </h4>
                        <!--
                        <button class="btn btn-lilac pull-right config-tiempos hidden" title="<?php echo JrTexto::_('Time settings'); ?>"><i class="fa fa-cog"></i></button>
                        -->
                    </div>
                    <div class="widget-body">
                        <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                        <div class="row contenido" style="display: initial;">
                            <?php foreach ($tiempos as $t) { ?>
                            <div class="<?php echo $t['col']; ?>" id="<?php echo $t['id']; ?>" style="padding: 0;" >
                                <div class="card card-time">
                                    <div class="card-title"><?php echo $t['title']; ?></div>
                                    <div class="card-info">
                                        <div class="col-xs-12 col-sm-6 resultado_tiempos">
                                            <div class="info-tiempo tiempo-obtenido">
                                                <span class="anio" style="display: none;">00</span>
                                                <span class="mes" style="display: none;">00</span>
                                                <span class="dia" style="display: none;">00</span>
                                                <span class="hora">00</span>
                                                <span class="min">00</span>
                                                <span class="seg">00</span>
                                            </div>
                                            <div class="info-tiempo tiempo-optimo" title="<?php echo JrTexto::_('Optimal time'); ?>">
                                                <span class="anio" style="display: none;">00</span>
                                                <span class="mes" style="display: none;">00</span>
                                                <span class="dia" style="display: none;">00</span>
                                                <span class="hora">00</span>
                                                <span class="min">00</span>
                                                <span class="seg">00</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 porcentaje_tiempos color-success">
                                            <p class="porcentaje">100%</p>
                                            <p class="mensaje" style="font-size:12px"><?php echo JrTexto::_('Missing time to reach optimum time'); ?>: <span>00:00:00</span></p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 comentario color-warning">
                                            <span><i class="fa fa-thumbs-up"></i></span>
                                            <span class="letras">BUENO</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="col-xs-12">
                <div class="widget-box" id="progreso_examenes">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-line-chart"></i>
                            <span><?php echo JrTexto::_('Progress in Exams'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12" style="padding: 0;">
                                    <div class="col-xs-12 card" id="promedio_puntaje">
                                        <div class="card-title"><?php echo JrTexto::_('Average of obtained scores'); ?> &nbsp; 
                                            <small>(<?php echo JrTexto::_('Out of 100 points'); ?>)</small></div>
                                        <div class="card-info">
                                            <div class="col-xs-12 col-sm-offset-4 col-sm-3 text-center promedio">00</div>
                                            <div class="col-xs-12 col-sm-5 comentario">
                                                <span><i class="fa fa-thumbs-up"></i></span>
                                                <span class="letras">BUENO</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 text-center" id="lista-habilidades-exam" style="margin-top: 1em; padding: 0;">
                                <?php /*$i=-1; 
                                $col = round(12/$cantHabilidades);
                                if($col<1) $col =1;
                                foreach ($this->habilidades as $hab) { 
                                    $i++;
                                    if($i>5) $i=0; ?>
                                    <div class="col-xs-3 col-sm-3 col-xs-offset-1 circle-skill" data-value="0" data-texto="<?php echo $hab['nombre'];?>" data-ccolorout="<?php echo $arrColoresHab[$i]; ?>" data-idhabilidad="<?php echo $hab['idmetodologia'];?>"></div>
                                <?php }*/ ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="widget-box" id="tareas">
                    <div class="widget-header bg-blue">
                        <h4 class="widget-title">
                            <i class="fa fa-briefcase"></i>
                            <span><?php echo JrTexto::_('Progress in homework'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main" style="padding: 0;">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12" style="padding: 0;">
                                    <div class="col-xs-12 card" id="promedio_puntaje_tarea">
                                        <div class="card-title"><?php echo JrTexto::_('Average of obtained scores'); ?> &nbsp; 
                                            <small>(<?php echo JrTexto::_('Out of 100 points'); ?>)</small></div>
                                        <div class="card-info">
                                            <div class="col-xs-12 col-sm-offset-4 col-sm-3 text-center promedio">00</div>
                                            <div class="col-xs-12 col-sm-5 comentario">
                                                <span><i class="fa fa-thumbs-up"></i></span>
                                                <span class="letras">---</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="grafico" id="tarea-chart-lines" style="width: 100%; height: 340px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="widget-box" id="progreso_actividades">
                    <div class="widget-header bg-green2">
                        <h4 class="widget-title">
                            <i class="fa fa-line-chart"></i>
                            <span><?php echo JrTexto::_('Progress in Activities'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12 text-center" id="lista-habilidades-sesion" style="margin-top: 1em; padding: 0;">
                                <?php /* $i=-1; 
                                $col = round(12/$cantHabilidades);
                                if($col<1) $col =1;
                                foreach ($this->habilidades as $hab) {
                                    $i++;
                                    if($i>5) $i=0; ?>
                                    <div class="col-xs-3 col-sm-3 col-xs-offset-1 circle-skill" data-value="0" data-texto="<?php echo $hab['nombre'];?>" data-ccolorout="<?php echo $arrColoresHab[$i]; ?>" data-idhabilidad="<?php echo $hab['idmetodologia'];?>"></div>
                                <?php }*/ ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php if(empty(@$this->idCurso)){ ?>
            <div class="col-xs-12">
                <div class="widget-box" id="mejorar_aprendizaje">
                    <div class="widget-header bg-lemon">
                        <h4 class="widget-title">
                            <i class="fa fa-bar-chart"></i>
                            <span><?php echo JrTexto::_('General Summary of Progress'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12 barras-progreso" style="padding: 0;">
                                    <div class="col-xs-12 skill-progress-header hide-on-capture" style="padding: 0;">
                                        <div class="col-xs-10">
                                            <label class="pull-left sr-only">0%</label>
                                            <label class="pull-right sr-only">100%</label>
                                        </div>
                                        <div class="col-xs-2 text-center">
                                            <label><?php echo ucfirst(JrTexto::_('improve')); ?></label>
                                        </div>
                                    </div>
                                    <div id="lista-habilidades-resumen">
                                    <?php /*$i=-1;
                                    foreach ($this->habilidades as $hab) { ?>
                                    <div class="col-xs-12 skill-progress" style="padding: 0;" data-idhabilidad="<?php echo $hab["idmetodologia"]; ?>">
                                        <div class="col-xs-10">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                                    <span class="nombre capitalize"><?php echo JrTexto::_($hab["nombre"]); ?></span>
                                                    <span class="porcentaje"> (0.0%)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 text-center hide-on-capture">
                                            <input type="checkbox" class="checkbox-ctrl chk-mejora" name="chkMejora_<?php echo $hab["idmetodologia"]; ?>" id="chkMejora_<?php echo $hab["idmetodologia"]; ?>">
                                        </div>
                                    </div>
                                    <?php }*/ ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 hide-on-capture" style="padding: 0;"><hr>
                                    <div class="col-xs-12 col-sm-6">
                                        <button type="button" class="btn btn-lg btn-block btn-yellow mej-automat" data-estado="inicial"><i class="fa fa-magic"></i> <span><?php echo JrTexto::_('Automatic improvement'); ?></span></button>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <button type="button" class="btn btn-lg btn-block btn-primary mej-manual" data-estado="inicial"><i class="fa fa-hand-paper-o"></i> <span><?php echo JrTexto::_('Manual improvement'); ?></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            <div class="col-xs-12">
                <div class="widget-box" id="chart_progreso_linear">
                    <div class="widget-header bg-pink">
                        <h4 class="widget-title">
                            <i class="fa fa-bar-chart"></i>
                            <span><?php echo JrTexto::_('Chart of progress since last tracking'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 chart-leyenda">
                                        <div class="col-xs-12 text-center"><b><?php echo JrTexto::_('Legend');?></b></div>
                                    <?php foreach ($this->habilidades as $key=>$hab) { ?>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="caja-color" style="background: <?php echo $arrColoresHab[$key];?>; width: 1.5em; height: 1em;display: inline-block;"></div>
                                            <span style="display: inline-block;"><?php echo $hab['nombre']; ?></span>
                                        </div>
                                    <?php } ?>
                                    </div>
                                    <div class="col-xs-12 grafico" id="chart-lines"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="widget-box" id="chart_progreso_pie">
                    <div class="widget-header bg-pink">
                        <h4 class="widget-title">
                            <i class="fa fa-pie-chart"></i>
                            <span><?php echo JrTexto::_('Chart of progress since last tracking'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="col-xs-12 chart-leyenda">
                                        <div class="col-xs-12 text-center"><b><?php echo JrTexto::_('Legend');?></b></div>
                                    <?php foreach ($this->habilidades as $key=>$hab) { ?>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="caja-color" style="background: <?php echo $arrColoresHab[$key];?>; width: 1.5em; height: 1em;display: inline-block;"></div>
                                            <span style="display: inline-block;"><?php echo $hab['nombre']; ?></span>
                                        </div>
                                    <?php } ?>
                                    </div>
                                    <div class="col-xs-12 grafico" id="chart-pie"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php if(empty(@$this->idCurso)){ ?>
            <div class="col-xs-12">
                <div class="widget-box" id="contactar">
                    <div class="widget-header bg-primary">
                        <h4 class="widget-title">
                            <i class="fa fa-envelope-o"></i>
                            <span><?php echo JrTexto::_('Contact'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row inicial"><div class="col-xs-12"><h3 class="text-center"><?php echo JrTexto::_('Press the "Filter" button to start'); ?>.</h3></div></div>
                            <div class="row contenido" style="display: none;">
                                <div class="col-xs-12">
                                    <p><?php echo ucfirst(JrTexto::_('send this report to')); ?> <strong><?php echo JrTexto::_('student'); ?> / <?php echo JrTexto::_('classroom'); ?> / <?php echo JrTexto::_('group'); ?></strong> <?php echo JrTexto::_('through'); ?>:</p>

                                    <form class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3">
                                                <input type="checkbox" class="checkbox-ctrl" disabled="disabled" checked="true">&nbsp;
                                                The Virtual Platform
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3">
                                                <input type="checkbox" class="checkbox-ctrl" id="chkEmailPersonal" name="chkEmailPersonal">&nbsp;
                                                <?php echo JrTexto::_('Personal email'); ?>:
                                            </label>
                                            <div class="col-sm-9 lista-emails">
                                                <kbd>(alumnocc@hotmail.com)</kbd>
                                            </div>
                                        </div>
                                        <div class="form-group" style="border-bottom: 1px solid #ddd;">
                                            <label class="col-sm-3">
                                                <input type="checkbox" class="checkbox-ctrl" id="chkOtroEmail" name="chkOtroEmail">&nbsp;
                                                <?php echo JrTexto::_('Other'); ?>:
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="input-group" style="margin-top: 0px;">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input type="text" class="form-control" name="txtEmailOtro" id="txtEmailOtro" placeholder="example1@mail.com , example2@mail.com , ..." disabled="disabled">
                                                </div>
                                                <small class="help-block color-info"><i class="fa fa-info-circle"></i> <?php echo ucfirst(JrTexto::_('emails must be separated by commas')); ?>( , ).</small>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group" style="margin-top: 0px;">
                                                    <span class="input-group-addon"><?php echo ucfirst(JrTexto::_('Subject')); ?></span>
                                                    <input type="text" class="form-control" name="txtAsunto" id="txtAsunto" value="Progress information">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="input-group" style="margin-top: 0px;">
                                                    <span class="input-group-addon"><?php echo ucfirst(JrTexto::_('Message')); ?></span>
                                                    <textarea rows="5" class="form-control" name="txtMensaje" id="txtMensaje">This is your progress report at <?php echo date('Y/m/d h:i'); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                                <button class="btn btn-lg btn-blue enviar-correo">
                                                    <i class="fa fa-paper-plane"></i>&nbsp;<?php echo ucfirst(JrTexto::_('send')); ?>
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        
        </div>
    </div>
</div>
<section id="capturas_paneles_png" class="hidden"></section>
<script>
var FILTRO = {};
var INFO_CORREO = {};
var HABILIDAD={ <?php foreach ($this->habilidades as $hab) {
echo "'".$hab["idmetodologia"]."':'".$hab["nombre"]."', ";
} ?> };
var COLOR_HABILIDAD = JSON.parse('<?php echo json_encode($arrColoresHab); ?>');
var ESCALAS = [];
var COMENTARIOS ={
    'bueno': {'icon': 'fa fa-thumbs-up', 'nombre': '<?php echo ucfirst(JrTexto::_("good"));?>', 'color':'success', 'minimo':75, 'maximo':100 },
    'regular': {'icon': 'fa fa-thumbs-o-up', 'nombre': '<?php echo ucfirst(JrTexto::_("regular"));?>', 'color': 'warning', 'minimo':50, 'maximo':75 },
    'malo': {'icon': 'fa fa-thumbs-down', 'nombre': '<?php echo ucfirst(JrTexto::_("bad"));?>', 'color': 'danger', 'minimo':0, 'maximo':50 },
};
ESCALAS = COMENTARIOS;
var HABILIDADES_TODAS = {}; /* Guardará todas las habilidades que se obtengan de Examenes o Actividades */

var actualizarHabilidadesTodas = function(idHabilidad, jsonValue) {
    if(!HABILIDADES_TODAS.hasOwnProperty(idHabilidad)) {
        HABILIDADES_TODAS[idHabilidad] = {
            "idhabilidad" : idHabilidad,
            "nombre" : jsonValue.nombre,
        };
    }
};

function toSeconds( time ) {
    var parts = time.split(':');
    return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]); 
}

function toHHMMSS(sec) {
    var sec_num = parseInt(sec, 10); 
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
};

var fnAjaxFail = function(xhr, textStatus, errorThrown) {
    console.log("error"); 
    console.log(xhr.responseText);
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
};

var isJSON = function(str){
    try { JSON.parse(str); } 
    catch (e) { return false; }
    return true;
};

var dateTimeToJson = function(strDateTime){
    var hasDate=false, hasTime=false;
    if(strDateTime.indexOf('-')!=-1){ hasDate=true; }
    if(strDateTime.indexOf(':')!=-1){ hasTime=true; }
    if(hasDate && hasTime){
        var fecha = strDateTime.split(' ')[0];
        var time = strDateTime.split(' ')[1];
        var arrFecha = fecha.split('-');
        var arrTime = time.split(':');
    } else if(hasDate){
        var fecha = strDateTime.split(' ')[0];
        var arrFecha = fecha.split('-');
    } else if(hasTime){
        var time = strDateTime.split(' ')[0];
        var arrTime = time.split(':');
    } else {
        return false;
    }

    if(arrFecha===undefined){ var arrFecha=[0,0,0]; }
    if(arrTime===undefined){ var arrTime=[0,0,0]; }
    return {
        'y':arrFecha[0], 'm':arrFecha[1], 'd':arrFecha[2],
        'h':arrTime[0], 'i':arrTime[1], 's':arrTime[2],
    };
};

var mostrarCargando = function(selector=''){
    if(selector.trim()!=''){
        $(selector).find('.inicial h3').text('<?php echo JrTexto::_('Loading'); ?>...');
        $(selector).find('.inicial').show();
        $(selector).find('.contenido').hide();
    }
};

var mostrarInfoTiempo = function($card, jsonTiempo){
    var tiempoJson=dateTimeToJson(jsonTiempo);
    if(!tiempoJson) return false;
    if(tiempoJson.y>0) {$card.find('.anio').text(tiempoJson.y).show();} else {$card.find('.anio').text('00').hide();}
    if(tiempoJson.m>0) {$card.find('.mes').text(tiempoJson.m).show(); } else {$card.find('.mes').text('00').hide();}
    if(tiempoJson.d>0) {$card.find('.dia').text(tiempoJson.d).show();} else {$card.find('.dia').text('00').hide();}
    $card.find('.hora').text(tiempoJson.h);
    $card.find('.min').text(tiempoJson.i);
    $card.find('.seg').text(tiempoJson.s);
};

var analizarTiempos = function (tObtenido, tOptimo, $card) {
    var json = dateTimeToJson(tObtenido);
    var porcentaje = 100.0;
    var tiempofaltante = '00:00:00';
    var segObtenidos = parseInt(json.s) + (parseInt(json.i)*60) + (parseInt(json.h)*60*60) + (parseInt(json.d)*24*60*60) + (parseInt(json.m)*2592000) + (parseInt(json.y)*31104000);
    var segOptimo = toSeconds(tOptimo);
    if(segOptimo>0){ porcentaje = (segObtenidos*100)/segOptimo; }
    if(segOptimo>segObtenidos){ tiempofaltante = toHHMMSS(segOptimo - segObtenidos); }
    var calif = 'malo';
    if(porcentaje>100){ porcentaje=100; }
    $.each(COMENTARIOS, function(index, esc) {
        if(porcentaje>esc.minimo && porcentaje<=esc.maximo){
            calif= index;
        }
    });
    var indice = 0;
    $.each(ESCALAS, function(index, escala) {
        if(porcentaje>escala.minimo && porcentaje<=escala.maximo){
            indice= index;
        }
    });

    $card.find('.porcentaje_tiempos .porcentaje').html((porcentaje).toFixed(2)+'%');
    $card.find('.porcentaje_tiempos .mensaje span').html(tiempofaltante);
    if(porcentaje==100){ $card.find('.porcentaje_tiempos .mensaje').hide(); }
    else{ $card.find('.porcentaje_tiempos .mensaje').show(); }

    $card.find('.porcentaje_tiempos').removeClass(function (index, css) {
        return (css.match(/(^|\s)color\S+/g) || []).join(' ');
    }).addClass('color-'+COMENTARIOS[calif].color);

    $card.find('.comentario').removeClass(function (index, css) {
        return (css.match(/(^|\s)color\S+/g) || []).join(' ');
    }).addClass('color-'+COMENTARIOS[calif].color);
    $card.find('.comentario i').attr('class', COMENTARIOS[calif].icon);
    $card.find('.comentario .letras').text(ESCALAS[indice].nombre);
};

var datosAlumnosXFiltro = function (idBuscar=null, tipo='') {
    mostrarCargando('#contactar');
    if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
    $.ajax({
        url: _sysUrlBase_+'/alumno/json_datosxalumno/',
        async: false,
        type: 'POST',
        dataType: 'json',
        data: {'idbuscar': idBuscar, 'tipo':tipo, 'idgrupo': FILTRO.grupo, 'idcurso':FILTRO.idcurso },
    }).done(function(resp) {
        if(resp.code=='ok'){
            if(tipo=='A') var a_quien = '<?php echo (JrTexto::_('student')); ?>';
            if(tipo=='G') var a_quien = '<?php echo (JrTexto::_('group')); ?>';
            $('#contactar p strong').html(a_quien);
            $('#contactar .lista-emails').html('');
            $('#pnl-datosfiltro .listado-alums').html('');
            $('#pnl-datosfiltro').hide();
            var alumnos = resp.data;
            var ruta_img = _sysUrlStatic_+'/media';
            $.each(alumnos, function(index, alum) {
                var nivel = (alum.ultima_actividad!=null)? alum.ultima_actividad.nivel : false;
                var unidad = (alum.ultima_actividad!=null)? alum.ultima_actividad.unidad : false;
                var sesion = (alum.ultima_actividad!=null)? alum.ultima_actividad.sesion : false;
                /***** info-alumno *******/
                if(tipo=='A' && alum.foto.length>0){ 
                    $('#pnl-datosfiltro img.foto-alumno').attr('src', ruta_img+'/image/'+alum.foto);
                }else{
                    $('#pnl-datosfiltro img.foto-alumno').attr('src', ruta_img+'/imagenes/user_avatar.jpg');
                }
                if(tipo=='G'){ 
                    $('#pnl-datosfiltro img.foto-alumno').attr('src', ruta_img+'/imagenes/group_avatar.png');
                }
                if(nivel && unidad && sesion){
                    var ultima_act= '<small class="ultima-actividad" data-idnivel="'+nivel.idnivel+'" data-idunidad="'+unidad.idnivel+'" data-idactividad="'+sesion.idnivel+'" data-orden="'+sesion.orden+'"><abbr title="<?php echo ucfirst(JrTexto::_('Last completed activity')); ?>" class="initialism">'+nivel.nombre+' - '+unidad.nombre+' - '+sesion.nombre+'</abbr></small>';
                } else {
                    var ultima_act= '<small><abbr title="<?php echo ucfirst(JrTexto::_('Last completed activity')); ?>" class="initialism"><?php echo ucfirst(JrTexto::_('No activities completed')); ?></abbr></small>'
                }
                $('#pnl-datosfiltro .listado-alums').append('<li style="margin-bottom:15px;"><div>- '+alum.ape_paterno+' '+alum.ape_materno+' '+alum.nombre+'</div>&nbsp;&nbsp;'+ultima_act+'</li>');
                $('#pnl-datosfiltro').show('fast');


                /***** contactar *****/
                $('#contactar .lista-emails').append('<kbd><span>'+alum.email+'</span><input type="hidden" value="'+alum.ape_paterno+' '+alum.ape_materno+' '+alum.nombre+'"></kbd>');
                $('#contactar .inicial').hide();
                $('#contactar .contenido').show();
            });
            
            $('#contactar .inicial').hide();
            $('#contactar .contenido').show();
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail);
};

var tiempoXFiltro = function(idBuscar=null, tipo=''){
    mostrarCargando('#tiempos');
    var str_url = '';
    if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
    
    if(tipo=='A') str_url = _sysUrlBase_+'/historial_sesion/tiempoxalumno/';
    else if(tipo=='G') str_url = _sysUrlBase_+'/historial_sesion/tiempoxgrupo/';
    if(tipo=='A'){
        idBuscar = $('#hIdAlumno2').val();
    }

    if($('#pnl-datosfiltro .ultima-actividad').length>0){
        var orden=0;
        var ultima_actividad={};
        $('#pnl-datosfiltro .ultima-actividad').each(function(index, el) {
            var $this=$(el);
            if(parseInt($this.data('orden')) > orden){
                ultima_actividad={
                    'idnivel':$this.data('idnivel'),
                    'idunidad':$this.data('idunidad'),
                    'idactividad':$this.data('idactividad'),
                };
            }
        });
    }else{
        $('#tiempos .inicial h3').html('<p><?php echo JrTexto::_('The students or students have not worked any activity'); ?>.</p><p><?php echo JrTexto::_('You can not verify times for this students or students'); ?>.</p>')
        return false;
    }
    if(str_url=='') return false;
    $.ajax({
        url: str_url,
        type: 'POST',
        async:false,
        dataType: 'json',
        data: {'id': idBuscar, 'tipo': tipo, 'ultima_actividad':JSON.stringify(ultima_actividad), 'idcurso':FILTRO.idcurso},
    }).done(function(resp) {
        console.log(resp);
        if(resp.code=='ok'){
            var tiempo = resp.data;
            var configProf = resp.config_docente;
            var tiempoJson = {};
            /*if($('#tiempos .contenido').hasClass('slick-initialized')){
                 $('#tiempos .contenido').slick('unslick');
            }*/
            $.each(tiempo, function(key, val) {
                if(key=='total'){ var $pnl_tiempo=$('#tiempo_total'); var optimo=configProf.tiempototal; }
                else if(key=='actividades'){ var $pnl_tiempo=$('#tiempo_actividades'); var optimo=configProf.tiempoactividades; }
                else if(key=='juegos'){ var $pnl_tiempo=$('#tiempo_games'); var optimo=configProf.tiempogames; }
                else if(key=='teacherresrc'){ var $pnl_tiempo=$('#tiempo_teacher_resrc'); var optimo=configProf.tiempoteacherresrc; }
                else if(key=='examenes'){ var $pnl_tiempo=$('#tiempo_examenes'); var optimo=configProf.tiempoexamenes; }
                
                if( isJSON(configProf.escalas) ) {
                    var arr = JSON.parse(configProf.escalas);
                    if(arr.length>0){ ESCALAS = arr; }
                }
                
                if($pnl_tiempo!==undefined){
                    $cardObtenido = $pnl_tiempo.find('.tiempo-obtenido');
                    $cardOptimo = $pnl_tiempo.find('.tiempo-optimo');
                    mostrarInfoTiempo($cardObtenido, val);
                    mostrarInfoTiempo($cardOptimo, optimo);
                    analizarTiempos(val, optimo, $pnl_tiempo);
                }
            });

            $('#tiempos .inicial').hide();
            $('#tiempos .contenido').show();
            //$('#tiempos .contenido').slick();
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail);
};

var habilidadesXFiltro = function(idBuscar=null, tipo=''){
    mostrarCargando('#mejorar_aprendizaje');
    var str_url = '';
    if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
    if(tipo=='A') var str_url = _sysUrlBase_+'/actividad_alumno/progresoxalumno/';
    else if(tipo=='G') var str_url = _sysUrlBase_+'/actividad_alumno/progresoxgrupo/';

    if(str_url=='') return false;
    $.ajax({
        url: str_url,
        type: 'POST',
        async: false,
        dataType: 'json',
        data: {'id': idBuscar, 'idcurso':FILTRO.idcurso},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var jsonProgreso = resp.data;
            INFO_CORREO['actividades'] = resp.data;

            var $contenedorSkills = $('#lista-habilidades-sesion');
            var strSkill = '';
            $.each(resp.data, function(idSkill, value) {
                let indexColor = idSkill % COLOR_HABILIDAD.length;
                strSkill += '<div class="col-xs-6 padding-0"><div class="col-xs-4 col-sm-4 col-xs-offset-1 circle-skill" data-value="'+value.promedio_porcentaje+'" data-texto="" data-ccolorout="'+COLOR_HABILIDAD[indexColor]+'" data-idhabilidad="'+idSkill+'"></div> <div class="col-xs-7 col-sm-7 text-left"><br>'+value.nombre+'</div></div>'

                actualizarHabilidadesTodas(idSkill, value);
            });
            $contenedorSkills.html(strSkill);

            $('#progreso_actividades .inicial').hide();
            $('#progreso_actividades .contenido').show();
            $('#progreso_actividades .circle-skill').graficocircle();
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail);
};

var examenesXFiltro = function(idBuscar=null, tipo=''){
    mostrarCargando('#progreso_examenes');
    var str_url = '';
    if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
    if(tipo=='A') str_url = _sysUrlBase_+'/examenes/promedioxalumno/';
    else if(tipo=='G') str_url = _sysUrlBase_+'/examenes/promedioxgrupo/';

    if(str_url=='') return false;
    $.ajax({
        url: str_url,
        async: false,
        type: 'POST',
        dataType: 'json',
        data: {'id': idBuscar, 'idcurso':FILTRO.idcurso},
    }).done(function(resp) {
        if (resp.code="ok") {
            INFO_CORREO['examenes'] = resp.data;
            var promXHab = resp.data.promXHab;
            var promedio = (resp.data.promPuntaje).toFixed(2);
            var calif='bad';
            $('#progreso_examenes #promedio_puntaje .promedio').text(promedio);
            if(promedio<=50){calif='malo';}
            else if(promedio>50 && promedio<=75){calif='regular';}
            else if(promedio>75 && promedio<=100){calif='bueno';}
            var indice = 2;
            $.each(ESCALAS, function(index, escala) {
                if(promedio>escala.minimo && promedio<=escala.maximo){
                    indice= index;
                }
            });
            $('#progreso_examenes #promedio_puntaje .comentario').removeClass(function (index, css) {
                return (css.match(/(^|\s)color\S+/g) || []).join(' ');
            }).addClass('color-'+COMENTARIOS[calif].color);
            $('#progreso_examenes #promedio_puntaje .comentario i').attr('class', COMENTARIOS[calif].icon);
            if(ESCALAS[indice]){
                var escala_nombre = ESCALAS[indice].nombre;
            }else{
                var escala_nombre = ESCALAS['malo'].nombre;
            }
            $('#progreso_examenes #promedio_puntaje .comentario .letras').text(escala_nombre);
            var $contenedorSkills = $('#lista-habilidades-exam');
            var strSkill = '';
            $.each(promXHab, function(idSkill, value) {
                let indexColor = idSkill % COLOR_HABILIDAD.length;
                strSkill += '<div class="col-xs-6 padding-0"><div class="col-xs-4 col-sm-4 col-xs-offset-1 circle-skill" data-value="'+value.promedio+'" data-texto="" data-ccolorout="'+COLOR_HABILIDAD[indexColor]+'" data-idhabilidad="'+idSkill+'"></div> <div class="col-xs-7 col-sm-7 text-left"><br>'+value.nombre+'</div></div>'
                actualizarHabilidadesTodas(idSkill, value);
            });
            $contenedorSkills.html(strSkill);

            $('#progreso_examenes .inicial').hide();
            $('#progreso_examenes .contenido').show();
            $('#progreso_examenes .circle-skill').graficocircle();
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail);
};

var resumenExamenesYActividades = function(){
    var arrPromedio = {};
    var strSkill = '';
    $.each(HABILIDADES_TODAS, function(idSkill, value) {
        var promedio = 0.0, suma = 0.0, cant = 0;
        var $circleExamen = $('#progreso_examenes .circle-skill[data-idhabilidad="'+idSkill+'"]');
        var $circleActividad = $('#progreso_actividades .circle-skill[data-idhabilidad="'+idSkill+'"]');
        if( $circleExamen.length ){
            suma += parseFloat($circleExamen.data('value'));
            cant++;
        }
        if( $circleActividad.length ){
            suma += parseFloat($circleActividad.data('value'));
            cant++;
        }
        if(cant>0) promedio = (suma/cant).toFixed(2);

        var color = 'progress-bar-info';
        if(promedio<=50){ color = 'progress-bar-danger'; }
        else if(promedio>50 && promedio<=75){ color = 'progress-bar-warning'; }
        else if(promedio<=100){ color = 'progress-bar-success'; } 

        strSkill += '<div class="col-xs-12 skill-progress" style="padding: 0;" data-idhabilidad="<?php echo $hab["idmetodologia"]; ?>">';
        strSkill += '<div class="col-xs-10"> <div class="progress">';
        strSkill += '<div class="progress-bar '+color+'" role="progressbar" aria-valuenow="0" aria-valuemin="'+promedio+'" aria-valuemax="100" style="width: '+promedio+'%">';
        strSkill += '<span class="nombre capitalize">'+value.nombre+'</span>';
        strSkill += '<span class="porcentaje"> ('+promedio+'%)</span>';
        strSkill += '</div> </div> </div>';
        strSkill += '<div class="col-xs-2 text-center hide-on-capture">';
        strSkill += '<input type="checkbox" class="checkbox-ctrl chk-mejora" name="chkMejora_'+idSkill+'" id="chkMejora_'+idSkill+'">';
        strSkill += '</div>  </div>';
    });
    $('#mejorar_aprendizaje #lista-habilidades-resumen').html(strSkill);

    $('#mejorar_aprendizaje .chk-mejora').attr('disabled', 'disabled');
    $('#mejorar_aprendizaje .inicial').hide();
    $('#mejorar_aprendizaje .contenido').show();
};

var puntajeXHabilidad = function () {
    var porcentXhab = {};
    $('#progreso_actividades .circle-skill').each(function(index, el) {
        var $this = $(this);
        var idSkill = $this.attr('data-idhabilidad');
        var key = HABILIDADES_TODAS[idSkill].nombre;
        var valor= $this.attr('data-value');
        porcentXhab[key] = parseFloat(valor);
    });
    INFO_CORREO['resumen'] = porcentXhab;
    return porcentXhab;
};

var generarChart = function(idBuscar=null, tipo=''){
    var jsonPtjexHab = puntajeXHabilidad();
    mostrarCargando('#chart_progreso_linear');
    mostrarCargando('#chart_progreso_pie');
    $.ajax({
        url: _sysUrlBase_+'/reporte/ultima_sesion/',
        type: 'POST',
        dataType: 'json',
        data: {'id': idBuscar, 'tipo': tipo, 'informacion': JSON.stringify(jsonPtjexHab), 'idcurso':FILTRO.idcurso},
    }).done(function(resp) {
        if(resp.code=='ok'){
            $('#chart_progreso_linear #chart-lines').html('');
            var reporte_ultimo=resp.data.ultimo_reporte;
            var today = new Date();
            var fecha_hoy=today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            if(reporte_ultimo==null){
                $('#chart_progreso_linear #chart-lines').html('<h3><center><?php echo JrTexto::_('There is no previous report found to compare'); ?></h3></center>');
            }else{
                var arrInformacion = JSON.parse(reporte_ultimo.informacion);
                var arrPorcentajes=[];
                var arrPorcHoy=[];
                var SuppressForeignObjectPlugin = function(chart) {
                    chart.supportsForeignObject = false;
                };
                $.each(arrInformacion, function(index, val) {
                    var nuevo = [];
                    nuevo.push(val.porcentaje);
                    nuevo.push(jsonPtjexHab[val.idhabilidad]);
                    if( typeof jsonPtjexHab[val.idhabilidad] != 'undefined' ) {
                        arrPorcHoy.push(jsonPtjexHab[val.idhabilidad].toFixed(1));
                    }
                    arrPorcentajes.push(nuevo);
                });
                var dataLines = {
                    labels: [reporte_ultimo.fechacreacion, fecha_hoy],
                    series: arrPorcentajes
                };
                var options = { 
                    width: 750, 
                    height: 400, 
                    plugins: [ SuppressForeignObjectPlugin ] 
                };
                new Chartist.Line('#chart_progreso_linear #chart-lines', dataLines, options);
                
                var dataPie ={
                    series: arrPorcHoy,
                };
                var optPie = {
                    /*donut: true, 
                    donutWidth: 80,*/ 
                    width: 750,
                    height:400, 
                    labelInterpolationFnc: function(value) {
                        return value + '%';
                    },
                    plugins: [ SuppressForeignObjectPlugin ]
                }
                new Chartist.Pie('#chart_progreso_pie #chart-pie', dataPie, optPie);
            }

            $('#chart_progreso_linear .inicial').hide();
            $('#chart_progreso_linear .contenido').show();
            $('#chart_progreso_pie .inicial').hide();
            $('#chart_progreso_pie .contenido').show();
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail);
};

var progresoTareas = function(idBuscar=null, tipo=''){
    mostrarCargando('#tareas');
    var str_url = '';
    if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
    if(tipo=='A'){ 
        var str_url = _sysUrlBase_+'/tarea/xSeguimientoAlumno/';
        dataPost = { 'idalumno': idBuscar, 'idgrupo':FILTRO.grupo, 'idcurso':FILTRO.idcurso};
    }else if(tipo=='G'){ 
        var str_url = _sysUrlBase_+'/tarea/xSeguimientoGrupo/';
        dataPost = { 'idgrupo':idBuscar, 'idcurso':FILTRO.idcurso};
    }

    $.ajax({
        url: str_url,
        type: 'POST',
        dataType: 'json',
        data: dataPost,
    }).done(function(resp) {
        if(resp.code=='ok'){
            var arrValoresChart = [];
            var promedioTareas = 0.0;
            var $card = $('#tareas #promedio_puntaje_tarea');
            if(resp.data.length==0){ $("#tareas .inicial h3").text("<?php echo JrTexto::_("There is no data to display"); ?>"); return false; }
            $.each(resp.data, function(i, d) {
                promedioTareas+=parseFloat(d.promedioAsignaciones);
                var row = [d.tarea.nombre+': '+d.asignacion.fechaentrega, parseFloat(d.promedioAsignaciones) ];
                arrValoresChart.push(row);
            });
            promedioTareas = promedioTareas/resp.data.length;
            $card.find('.promedio').text(promedioTareas.toFixed(2));
            if(promedioTareas<=50){calif='malo';}
            else if(promedioTareas>50 && promedioTareas<=75){calif='regular';}
            else if(promedioTareas>75){calif='bueno';}

            var indice = calif;
            $.each(ESCALAS, function(index, escala) {
                if(promedioTareas>=escala.minimo && promedioTareas<=escala.maximo){
                    indice= index;
                    return false;
                }
            });

            $card.find('.comentario').removeClass(function (index, css) {
                return (css.match(/(^|\s)color\S+/g) || []).join(' ');
            }).addClass('color-'+COMENTARIOS[calif].color);
            $card.find('.comentario i').attr('class', COMENTARIOS[calif].icon);

            $card.find('.comentario .letras').text(ESCALAS[indice].nombre);

            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(function(){
                var data = new google.visualization.DataTable();
                data.addColumn('string', '<?php echo JrTexto::_('Last presentation day'); ?>');
                data.addColumn('number', '<?php echo JrTexto::_('Score'); ?>');
                data.addRows(arrValoresChart);

                var options = {
                    /*title: '<?php echo JrTexto::_('Average of obtained scores').' ('.JrTexto::_('Out of 100 points').')'; ?>: '+promedioTareas.toFixed(2),*/
                    hAxis: {
                        title: '<?php echo JrTexto::_('Homework'); ?>'
                    },
                    vAxis: {
                        title: '<?php echo JrTexto::_('Score'); ?>'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('tarea-chart-lines'));

                chart.draw(data, options);
            });

            $('#tareas .inicial').hide();
            $('#tareas .contenido').show();
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error');
        }
    }).fail(fnAjaxFail);
};

var filtrarXAlumno = function(idAlumno = FILTRO.alumno){
    HABILIDADES_TODAS = {}
    datosAlumnosXFiltro(idAlumno, 'A');
    tiempoXFiltro(idAlumno, 'A');
    examenesXFiltro(idAlumno, 'A');
    habilidadesXFiltro(idAlumno, 'A');
    resumenExamenesYActividades();
    generarChart(idAlumno, 'A');
    progresoTareas(idAlumno, 'A');
};

var filtrarXGrupo = function(idGrupo = FILTRO.grupo){
    HABILIDADES_TODAS = {}
    datosAlumnosXFiltro(idGrupo, 'G');
    tiempoXFiltro(idGrupo, 'G');
    examenesXFiltro(idGrupo, 'G');
    habilidadesXFiltro(idGrupo, 'G');
    resumenExamenesYActividades();
    generarChart(idGrupo, 'G');
    progresoTareas(idGrupo, 'G');
};

var activarBtnMejoraManual = function(flag){
    if(flag){
        $('#mejorar_aprendizaje .chk-mejora').prop('checked',false);
        $('#mejorar_aprendizaje .chk-mejora').removeAttr('disabled');
        $('#mejorar_aprendizaje .btn.mej-manual').attr('data-estado', 'aplicar');
        $('#mejorar_aprendizaje .btn.mej-manual span').text('<?php echo JrTexto::_('Apply improvement'); ?>');
    } else {
        //$('#mejorar_aprendizaje .chk-mejora').prop('checked',false);
        $('#mejorar_aprendizaje .chk-mejora').attr('disabled','disabled');
        $('#mejorar_aprendizaje .btn.mej-manual').attr('data-estado','inicial');
        $('#mejorar_aprendizaje .btn.mej-manual span').text('<?php echo JrTexto::_('Manual improvement'); ?>');
    }
};

var aplicarMejoraManual = function(){
    var codigo = 0;
    var arrHabMejorar = [];
    $('#mejorar_aprendizaje input.chk-mejora').each(function(index, el) {
        if($(this).is(':checked')){
            var idHab = $(this).attr('id').split('_').pop();
            arrHabMejorar.push(idHab);
        }
    });
    var jsonHabMejorar = JSON.stringify(arrHabMejorar);
    if(FILTRO.filtrado_por == 'ALUM'){ 
        var str_url = _sysUrlBase_+'/matricula_alumno/auto_asignacion/';
        codigo = FILTRO.alumno; 
    }
    if(FILTRO.filtrado_por == 'GRUP'){ 
        var str_url = _sysUrlBase_+'/matricula_alumno/auto_asignacionxgrupo/';
        codigo = FILTRO.grupo; 
    }
    
    if(codigo==0) return false;

    $.ajax({
        url: str_url,
        type: 'POST',
        dataType: 'json',
        data: {
            'hab_mejorar': jsonHabMejorar, 
            'filtrado_por': FILTRO.filtrado_por,
            'id': codigo, 
            'idcurso':FILTRO.idcurso
        },
        beforeSend: function () {
            $('#mejorar_aprendizaje .btn').attr('disabled', 'disabled');
        }
    }).done(function(resp) {
        if(resp.code=='ok'){
            mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.data.mensaje, 'success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', '<?php echo JrTexto::_('Success'); ?>', 'success'); /*trolleando al jefe >:v */
            //mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.mensaje, 'error'); 
        }
        activarBtnMejoraManual(false);
    }).fail(fnAjaxFail).always(function() {
        $('#mejorar_aprendizaje .btn').removeAttr('disabled');
    });
};

var aplicarMejoraAutomatica = function (jsonPorcentxHab) {
    var codigo = 0;
    var arrHabMejorar = [];
    $('#mejorar_aprendizaje input.chk-mejora').each(function(index, el) {
        if($(this).is(':checked')){
            var idHab = $(this).attr('id').split('_').pop();
            arrHabMejorar.push(idHab);
        }
    });
    var jsonHabMejorar = JSON.stringify(arrHabMejorar);
    if(FILTRO.filtrado_por == 'ALUM'){ 
        var str_url = _sysUrlBase_+'/matricula_alumno/asignacion_automatica/';
        codigo = FILTRO.alumno; 
    }
    if(FILTRO.filtrado_por == 'GRUP'){ 
        var str_url = _sysUrlBase_+'/matricula_alumno/asignacion_automaticaxgrupo/';
        codigo = FILTRO.grupo; 
    }
    
    if(codigo==0) return false;
    $.ajax({
        url: str_url,
        type: 'POST',
        dataType: 'json',
        data: {
            'id': codigo,
            'porcentajeXHab': JSON.stringify(jsonPorcentxHab), 
            'idcurso':FILTRO.idcurso
        },
        beforeSend: function () {
            $('#mejorar_aprendizaje .btn').attr('disabled', 'disabled');
        }
    }).done(function(resp) {
        if(resp.code=='ok'){
            mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.data.mensaje, 'success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', '<?php echo JrTexto::_('Success'); ?>', 'success'); /*trolleando al jefe >:v */
            //mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    }).fail(fnAjaxFail).always(function() {
        $('#mejorar_aprendizaje .btn').removeAttr('disabled');
    });
};

var enviarCorreo = function(formData){
    $.ajax({
        url: _sysUrlBase_+'/sendemail/enviar_phpmailer',
        type: "POST",
        data:  formData,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend: function(XMLHttpRequest){
            $('#procesando').show('fast');
            $('#contactar .enviar-correo').attr('disabled','disabled');
        },
        success: function(data)
        {
            if(data.code==='ok'){
                console.log('mensaje enviado');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
            }
            $('#procesando').hide('fast');
            $('#contactar .enviar-correo').removeAttr('disabled');
            return false;
        },
        error: function(xhr,status,error){
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
            $('#procesando').hide('fast');
            return false;
        },
    }).always(function() {
        $('#contactar .enviar-correo').removeAttr('disabled');
    });
};

var enviarViaPlataforma = function(){
    console.log('falta la función de Correo enviarViaPlataforma();');
};

var prepararCorreo = function(){
    var arrEmails = [];
    if($('#contactar #chkOtroEmail').is(':checked')){
        var correos = $('#txtEmailOtro').val().trim();
        if(correos.length){ 
            arrCorreos = correos.split(',');
            $.each(arrCorreos, function(index, val) {
                var nuevo = {'email': val, 'nombre':'',};
                arrEmails.push(nuevo);
            });
        }
    }
    if($('#contactar #chkEmailPersonal').is(':checked')){ 
        $('#contactar .lista-emails kbd').each(function() {
            var alum = {
                'email' : $(this).find('span').text(),
                'nombre' : $(this).find('input').val(),
            };
            arrEmails.push(alum);
        });
    }
    var msje=$('#txtMensaje').val();
    var asunto=$('#txtAsunto').val();
    var arrAdjuntos = [];
    $('#capturas_paneles_png img').each(function(index, el) {
        var $img = $(el);
        var nuevoSrc= {
            'tipo': 'base64',
            'src': $img.attr('src'),
        };
        arrAdjuntos.push(nuevoSrc);
    });
    /*    
        msje+='<h2><strong>';
        msje+=' Informe de progreso ';
        msje+='</strong></h2><p>';
        msje+=' El resultado de actvidades por habilidad es: ';
        msje+='</p><ul> ';
        $.each(INFO_CORREO.actividades, function(idHab, valor) {
            msje+='<li>';
            msje+=HABILIDAD[idHab]+': '+valor.promedio_porcentaje+'% ';
            msje+='</li>';
        });
        msje+='</ul><hr><p>';
        msje+=' El resultado de examenes es: ';
        msje+='</p> <p><center>';
        msje+=' En promedio: '+INFO_CORREO.examenes.promPuntaje+'% (en base a 100%) ';
        msje+='</center> <p>';
        msje+=' Por habilidad: ';
        msje+='</p><ul>';
        $.each(INFO_CORREO.examenes.promXHab, function(idHab, valor) {
            msje+='<li>';
            msje+=HABILIDAD[idHab]+': '+valor.promedio+'% ';
            msje+='</li>';
        });
        msje+='</ul><hr><p>';
        msje+=' En resumen y promediando actividades y examenes, los resultados son los siguientes: ';
        msje+='</p><ul>';
        $.each(INFO_CORREO.resumen, function(idHab, valor) {
            msje+='<li>';
            msje+=HABILIDAD[idHab]+': '+valor+'% ';
            msje+='</li>';
        });
        msje+='</ul><hr> <strong><center>';
        msje+='Se recomienda trabajar en las habilidades donde su porcentaje es menor a 50%.';
        msje+='</center></strong>';
    */
    var formData = new FormData();
    formData.append("paraemail", JSON.stringify(arrEmails));
    formData.append("msje", msje);
    formData.append("asunto", asunto);
    formData.append("adjuntos", JSON.stringify(arrAdjuntos));
    formData.append("idcurso", FILTRO.idcurso);
    enviarCorreo(formData);
};

var capturar = function($targetElem, esUltimo){
    var nodesToRecover = [];
    var nodesToRemove = [];

    if($targetElem.find('svg[class^="ct-chart"]').length==0){
        var svgElem = $targetElem.find('svg');
        var width = $(svgElem[0]).width();
        var height = $(svgElem[0]).height();
        
        svgElem.each(function(index, node) {
            var parentNode = node.parentNode;
            var svg = parentNode.innerHTML;

            var canvas = document.createElement('canvas');
            
            canvas.width = width;
            canvas.height = height;

            /**** Conviertiendo los SVG a canvas *****/
            canvg(canvas, svg, { 
                useCORS: false, 
                ignoreMouse: true,
            });

            nodesToRecover.push({
                parent: parentNode,
                child: node
            });
            parentNode.removeChild(node);

            nodesToRemove.push({
                parent: parentNode,
                child: canvas
            });
            parentNode.appendChild(canvas);
        });
    } else {
        /***** Chartist.js to Image ****/
        var idChart = $targetElem.find('.grafico').attr('id');
        var chartDivNode = document.getElementById(idChart);
        var chartSvgNode = chartDivNode.children[0];
        var section = document.getElementById('capturas_paneles_png');
        var random = parseInt(Math.random()*10000000000);
        var width = $(chartSvgNode[0]).width();
        var height = $(chartSvgNode[0]).height();
        var canvas = document.createElement('canvas');
        canvas.setAttribute("id", "canvas_"+random);
        canvas.width = width;
        canvas.height = height;
        $targetElem.find('.grafico').before(canvas);
        var canvasChart = document.getElementById("canvas_"+random);
        
        SVG2Bitmap(chartSvgNode, canvasChart);

        $targetElem.find('.grafico').hide();
    }
    console.log($targetElem);
    html2canvas($targetElem, {
        onrendered: function(canvas) {
            var ctx = canvas.getContext('2d');
            ctx.webkitImageSmoothingEnabled = false;
            ctx.mozImageSmoothingEnabled = false;
            ctx.imageSmoothingEnabled = false;

            var data = canvas.toDataURL();
            var img = document.createElement('img');
            img.src = data;

            var section = document.getElementById('capturas_paneles_png');
            section.appendChild(img);

            
            /**** Devolviendo los canvas a su estado SVG original *****/
            nodesToRemove.forEach(function(pair) {
                pair.parent.removeChild(pair.child);
            });

            nodesToRecover.forEach(function(pair) {
                pair.parent.appendChild(pair.child);
            });

            if(esUltimo) {
                $('#contenedor-pnls-info').slick(); 
                $('.hide-on-capture').show();
                $('.widget-body .grafico').siblings('canvas').remove();
                $('.widget-body .grafico').show();
                prepararCorreo();
            }

        }
    });
};

var capturaPaneles_Enviar = function(arrSelectores){
    $('#contenedor-pnls-info').slick('unslick');
    $('.hide-on-capture').hide();
    $('#capturas_paneles_png').html('<h3><strong>Resultado de progreso</strong></h3>');
    var cant = arrSelectores.length;
    var esUltimo = false;
    $.each(arrSelectores, function(index, panel) {
        if(cant-1 == index){ esUltimo = true; }
        capturar($(panel), esUltimo);
    });
};

var numerarPaneles = function(){
    var $paneles = $('#contenedor-pnls-info .widget-box');
    var total = $paneles.length;
    $paneles.each(function(index, elem) {
        var num = index+1;
        $(elem).find('.widget-header').append('<div class="pull-right numeracion"><span>'+num+'</span><span>'+total+'</span></div>');
    });
};

$(document).ready(function() {
    FILTRO["idcurso"] = ( $("#hIdCurso").length==1 )?$("#hIdCurso").val():0;
    
    $('#pnl-filtros').on('click', '.btn.filtrar', function(e) {
        e.preventDefault();
        FILTRO = {
            'grupo' : $('#opcGrupo').val(),
            'alumno' : $('#opcAlumno').val(),
        };

        activarBtnMejoraManual(false);

        if(FILTRO.alumno!=-1){ 
            filtrarXAlumno();
            FILTRO['filtrado_por'] = 'ALUM';
        }else if(FILTRO.grupo!=-1){ 
            $('#contactar p strong').html('<?php echo (JrTexto::_('group')); ?>');
            filtrarXGrupo();
            FILTRO['filtrado_por'] = 'GRUP';
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', '<?php echo JrTexto::_('You have not applied any filter'); ?>.', 'warning'); 
            return false;
        }
    }).on('change', '#opcColegio', function(e) {
        e.preventDefault();
        var $this = $(this);
        var idColegio = $this.val();
        if(idColegio==null || idColegio==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
            $('#pnl-filtros #opcAula').html(optGrup);
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/ambiente/getMisLocales/',
            type: 'POST',
            dataType: 'json',
            data: {'idlocal':idColegio},
            beforeSend: function(){ $('#pnl-filtros #opcAula').attr('disabled','disabled'); }
        }).done(function(resp) {
            if(resp.code=='ok'){
                var aulas = resp.data;
                var options='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
                $.each(aulas, function(key, val) {
                    options+='<option value="'+val.idambiente+'">'+val.numero+'</option>';
                });
                $('#pnl-filtros #opcAula').html(options);
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function() {
            $('#pnl-filtros #opcAula').removeAttr('disabled');
        });   
    }).on('change', '#opcAula', function(e) {
        e.preventDefault();
        var $this = $(this);
        var idAula = $this.val();
        var idLocal = $('#opcColegio').val();
        if(idAula==null || idAula==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
            $('#pnl-filtros #opcGrupo').html(optGrup);

            var optAlum='<option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>';
            $('#pnl-filtros #opcAlumno').html(optAlum);
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/acad_grupoauladetalle/buscarjson/',
            type: 'POST',
            dataType: 'json',
            data: {
                'idlocal':idLocal,
                'idambiente':idAula,
                'iddocente':'<?php echo $this->usuarioAct["dni"]; ?>',
            },
            beforeSend: function(){ 
                $('#pnl-filtros #opcGrupo').attr('disabled','disabled'); 
                $('#pnl-filtros #opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                /***************    Grupos    ***************/
                var grupos = resp.data;
                var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
                $.each(grupos, function(key, gr) {
                    optGrup+='<option value="'+gr.idgrupoauladetalle+'">'+gr.strgrupoaula+' '+gr.nombre+' '+' ('+gr.strtipogrupo+')</option>';
                });
                $('#pnl-filtros #opcGrupo').html(optGrup);

                /***************    Alumnos    ***************/
                /*var alumnos = resp.data.alumnos;
                var optAlum='<option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>';
                $.each(alumnos, function(key, al) {
                    optAlum+='<option value="'+al.dni+'" data-email="'+al.email+'">'+al.ape_paterno+' '+al.ape_materno+' '+al.nombre+'</option>';
                });
                $('#pnl-filtros #opcAlumno').html(optAlum);*/
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $('#pnl-filtros #opcGrupo').removeAttr('disabled');
            $('#pnl-filtros #opcAlumno').removeAttr('disabled');
        }); 
    }).on('change', '#opcGrupo', function(e) {
        var $this=$(this);
        var idGrupo = $this.val();
        if(idGrupo<=0){
            var optAlum = '<option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>';
            $('#pnl-filtros #opcAlumno').html(optAlum);
            return false;
        }
        var list = '';
        $.ajax({
            url: _sysUrlBase_+'/acad_matricula/buscarjson/',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {
                'idgrupoauladetalle':idGrupo,
            },
            beforeSend: function(){
                $('#pnl-filtros #opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                /***************    Alumnos    ***************/
                var matriculas = resp.data;
                var optAlum = '<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
                if(matriculas.length) {
                    $.each(matriculas, function(key, al) {
                        optAlum += '<option value="'+al.idalumno+'" data-email="'+al.alumno_email+'">'+al.stralumno+'</option>';
                    });
                } else {
                    optAlum = '<option value="-1"><?php echo JrTexto::_('There are no students in this group') ?></option>';
                }
                $('#pnl-filtros #opcAlumno').html(optAlum);
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $('#pnl-filtros #opcAlumno').removeAttr('disabled');
        }); 
    });

    $('#mejorar_aprendizaje').on('click', '.mej-automat', function(e) {
        e.preventDefault();
        activarBtnMejoraManual(false);
        var porcentXhab = puntajeXHabilidad();
        aplicarMejoraAutomatica(porcentXhab);
    }).on('click', '.mej-manual', function(e) {
        e.preventDefault();
        if($(this).attr('data-estado')=='inicial'){
            activarBtnMejoraManual(true);
        } else {
            /* when [data-estado="aplicar"] */
            aplicarMejoraManual();
        }
    });

    $('#contactar').on('click', '.enviar-correo', function(e) {
        e.preventDefault();
        enviarViaPlataforma();
        var pnls=['#progreso_examenes', '#tareas', '#progreso_actividades', '#mejorar_aprendizaje', '#chart_progreso_linear', '#chart_progreso_pie'];
        capturaPaneles_Enviar(pnls);
    }).on('change', '#chkOtroEmail', function(e) {
        e.preventDefault();
        if($(this).is(':checked')){ $('#contactar #txtEmailOtro').removeAttr('disabled'); }
        else { $('#contactar #txtEmailOtro').attr('disabled', 'disabled'); }
    });

    numerarPaneles();
    $('#contenedor-pnls-info').slick();

    <?php if(!empty(@$this->idCurso)){ ?>
    FILTRO.alumno = $("#hIdAlumno2").val();
    FILTRO.grupo = $("#hIdGrupo").val();
    
    filtrarXAlumno();
    <?php } ?>
/*
    $('#tiempos').on('click', '.btn.config-tiempos', function(e) {
        e.preventDefault();
        var $modal = $('#modalclone').clone();
        $modal.attr('id', 'mdl_config_tiempos');
        $modal.find('.modal-dialog.modal-lg').css('width','80%');
        //$modal.find('.modal-dialog.modal-lg').removeClass('modal-lg');
        $modal.find('#modaltitle').text('<?php echo JrTexto::_('Optimal time of stay on the platform'); ?>');
        $modal.find('#modalfooter').find('.cerrarmodal').addClass('pull-left');
        $modal.find('#modalfooter').append('<button id="btn-saveConfiguracion_docente" type="submit" class="btn btn-success pull-right" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>');
        $('body').append($modal);
        $('#mdl_config_tiempos').modal({keyboard:false, backdrop:'static'});
        $.get(_sysUrlBase_+'/configuracion_docente/modal_config_tiempos/', function(data) {
            $modal.find('#modalcontent').html(data);
        });
    });

    $('body').on('hidden.bs.modal','#mdl_config_tiempos', function(e) {
        e.preventDefault();
        $('#mdl_config_tiempos').remove();
    }).on('click', '#mdl_config_tiempos #btn-saveConfiguracion_docente', function(e) {
        e.preventDefault();
        var $contenedor = $('#mdl_config_tiempos');
        var arrConfigs=[];
        $contenedor.find('#tblConfiguracion tbody>tr').each(function(index, elem) {
            var $fila = $(elem);
            var nuevo={
                'idconfigdocente':  ($fila.attr('data-idconfigdocente')=='null')?null:$fila.attr('data-idconfigdocente'),
                'idnivel':  $fila.attr('data-idnivel'),
                'idunidad':  $fila.attr('data-idunidad'),
                'idactividad':  $fila.attr('data-idactividad'),
                'tiempototal': $fila.find('input.tiempo-total').val(),
                'tiempoactividades': $fila.find('input.tiempo-actividad').val(),
                'tiempoteacherresrc': $fila.find('input.tiempo-tresrc').val(),
                'tiempogames': $fila.find('input.tiempo-game').val(),
            };
            arrConfigs.push(nuevo);
        });

        $.ajax({
            url: _sysUrlBase_+'/configuracion_docente/xGuardarConfigDocente/',
            type: 'POST',
            dataType: 'json',
            data: {'arrConfigs': JSON.stringify(arrConfigs)},
        }).done(function(resp) {
            if(resp.code='ok'){
                mostrar_notificacion('<?php echo JrTexto::_('Success'); ?>!', resp.data.mensaje, 'success');
                $contenedor.modal('hide');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            } else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function() {
            $('#mejorar_aprendizaje .btn').removeAttr('disabled');
        });
    });
*/
});
</script>