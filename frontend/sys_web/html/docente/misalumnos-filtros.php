<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">  
          <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="idgrupoauladetalle" class="form-control idgrupoauladetalle">
                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php if(!empty($this->miscursos))
                    foreach ($this->miscursos as $fk) { ?><option value="<?php echo $fk["idgrupoauladetalle"]?>" ><?php echo $fk["strcurso"]." : ".$fk["aniopublicacion"]." : ".$fk["strlocal"] ?></option><?php } ?>
                </select>
              </div>
            </div> 
            <div class="col-xs-12 col-sm-6 col-md-6 form-group">
              <label><?php echo  ucfirst(JrTexto::_("student")." (".JrTexto::_('Name')."/DNI)")?></label>
              <div class="input-group" style="margin:0px">
                <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                <span class="input-group-addon btn btnbuscar"><?php echo JrTexto::_('Search') ?> <i class="fa fa-search"></i></span>  
              </div>
            </div> 
          </div>
          <button id="actualuarmatricula123" class="hide">actualizar</button>         
          </form>  
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide " >
    <div class="panel" id="vista<?php echo $idgui; ?>">
    </div>
  </div>
</div>
<div class="form-view" id="ventanatabla_<?php echo $idgui; ?>" >
  <div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="panel">         
         <div class="panel-body table-striped table-responsive">
            <table class="table " style="min-width:100%">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idalumno") ;?></th>
                  <th><?php echo JrTexto::_("Sexo") ;?></th>
                  <th><?php echo JrTexto::_("Telephone") ;?></th>
                  <th><?php echo JrTexto::_("usuario") ;?></th>
                  <th><?php echo JrTexto::_("email") ;?></th>
                  <th><?php echo JrTexto::_("Fecha_matricula") ;?></th>
                  <th><?php echo JrTexto::_("Estado") ;?></th>
                  <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
var tabledatos5bd4788e236d6='';
var estados5bd4788e236d6={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
var tituloedit5bd4788e236d6='<?php echo ucfirst(JrTexto::_("acad_matricula"))." - ".JrTexto::_("edit"); ?>';
var draw5bd4788e236d6=0;
  function refreshdatos5bd4788e236d6(){
      tabledatos5bd4788e236d6.ajax.reload();
  }
  var vista=window.localStorage.vistapnl||1;
  var cargarvista<?php echo $idgui; ?>=function(view){
    var _vista=view||vista||1;
    var frmtmp=document.getElementById('frmpersonal<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);
    formData.append('vista',_vista);
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/personal/listado',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(dt){$('#vista<?php echo $idgui; ?>').html(dt);}
    }
    sysajax(data);
  }
  var buscargrupoauladetalle<?php echo $idgui; ?>=function(){
    var fd2= new FormData();
    var idiiee=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbiiee').val()||-1;
    var idcurso=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbcurso').val()||-1;
    var idgrado=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbgrado').val()||-1;
    var idsecion=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbseccion').val()||-1;
    fd2.append("idcurso", idcurso);
    fd2.append("idlocal", idiiee);
    fd2.append("idgrado", idgrado);
    fd2.append("idsesion", idsecion);
    fd2.append("sql2", true);
    sysajax({
    fromdata:fd2,
    url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
    callback:function(rs){
      if(rs.code=='ok'||rs.code==200){
        var dt=rs.data[0]||-1;
        if(dt!=-1){
          $('.idteacher').val(dt.iddocente);
          $('.teachername').val(dt.strdocente);
          $('#idgrupoauladetalle').val(dt.idgrupoauladetalle)
        }else{
          $('.idteacher').val('');
          $('.teachername').val('');
          $('#idgrupoauladetalle').val('');
        }
      }
      refreshdatos5bd4788e236d6();
    }
    })
  }

  $(document).ready(function(){
    $('#actualuarmatricula123').click(function(ev){
      ev.preventDefault();
      $('#vista<?php echo $idgui; ?>').parent('div').addClass('hide');
      $('#ventanatabla_<?php echo $idgui; ?>').show();
      refreshdatos5bd4788e236d6();
    })

    $('#frmEstudiantes<?php echo $idgui;?>').on('change','select.idgrupoauladetalle',function(ev){
         buscargrupoauladetalle<?php echo $idgui; ?>();
    })
    $('#frmEstudiantes<?php echo $idgui;?> select.idgrupoauladetalle').trigger("change");
  
    $('.btnbuscar').click(function(ev){
      refreshdatos5bd4788e236d6();
    });

  tabledatos5bd4788e236d6=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "pageLength": 50,
      "searching": false,
      "processing": false,
      "serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Idalumno") ;?>'},
        {'data': '<?php echo JrTexto::_("Sexo") ;?>'},
        {'data': '<?php echo JrTexto::_("Telephone") ;?>'},
        {'data': '<?php echo JrTexto::_("usuario") ;?>'},
        {'data': '<?php echo JrTexto::_("email") ;?>'},
        {'data': '<?php echo JrTexto::_("Fecha_matricula") ;?>'},
        {'data': '<?php echo JrTexto::_("Estado") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/acad_matricula/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.texto=$('#texto').val(),
            d.idgrupoauladetalle=$('select[name="idgrupoauladetalle"]').val()||-1;
            draw5bd4788e236d6=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5bd4788e236d6;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idalumno") ;?>':'DNI:'+data[i].dni+" <br>"+ data[i].stralumno, //data[i].idalumno,
              '<?php echo JrTexto::_("Sexo") ;?>':data[i].sexo,
              '<?php echo JrTexto::_("Telephone") ;?>':data[i].telefono,
              '<?php echo JrTexto::_("usuario") ;?>':data[i].usuario,
              '<?php echo JrTexto::_("email") ;?>':data[i].alumno_email,
              '<?php echo JrTexto::_("Fecha_matricula") ;?>': data[i].fecha_registro,
              '<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
              '<?php echo JrTexto::_("Actions") ;?>' :
              '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/perfil/?id='+data[i].idalumno+'&idrol=3" data-titulo="<?php echo JrTexto::_('Ficha'); ?> '+data[i].stralumno+'"><i class="fa fa-eye"></i></a>'
             // '<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/formulario/?idpersona='+data[i].idalumno+'&idrol=3&datareturn=false&buscarper=no" data-titulo="<?php //echo JrTexto::_('Student')." ".JrTexto::_('Edit'); ?>"><i class="fa fa-pencil"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idalumno+'&idrol=3" data-titulo="<?php //echo JrTexto::_('Change')." ".JrTexto::_('password'); ?>"><i class="fa fa-key"></i></a>'+
              //'<a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash-o"></i></a>'

            });
          }
          return datainfo }, error: function(d){console.log(d)}
      },
      "language": { "url": _sysIdioma_.toUpperCase()!='EN'?(_sysUrlBase_+"/static/libs/datatable1.10/idiomas/"+_sysIdioma_.toUpperCase()+".json"):''}
      
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'setCampo', id,campo,data);
          if(res) tabledatos5bd4788e236d6.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd4788e236d6';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_matricula';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{footer:false,borrarmodal:true,callback:function(_modal){}});
  });
  
 

   $('.btnagregarnewmatricula').click(function(ev){
    ev.preventDefault();
    var href=$(this).attr('href');
    var formData = new FormData();
    sysajax({
      fromdata:formData,
      url:href,
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(dt){
        $('#vista<?php echo $idgui; ?>').parent('div').removeClass('hide');
        $('#vista<?php echo $idgui; ?>').html(dt);
        $('#ventanatabla_<?php echo $idgui; ?>').hide();
      }
    })
   })
});
</script>