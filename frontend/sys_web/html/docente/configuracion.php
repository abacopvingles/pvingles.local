<style>
#configuracion_docente{
    margin-top: 12px;
}
#configuracion_docente .panel-body{
    max-height: 460px;
    overflow: auto;
}

#configuracion_docente .form-footer{
    padding: 15px;
    text-align: center;
    border-top: 1px solid #e5e5e5;
}
</style>

<div class="row"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/docente/panelcontrol"><?php echo JrTexto::_('Student Progress Tracking'); ?></a></li>              
        <li class="active"><?php echo JrTexto::_('Setting'); ?></li>
    </ol>
</div> </div>

<div class="panel panel-primary" id="configuracion_docente">
    <div class="panel-heading"><?php echo JrTexto::_('Setting'); ?></div>
    
    <div class="panel-body">
        <div class="row form-group" id="levels" style="padding-top: 1ex; ">
            <div class="col-xs-12 col-sm-6 select-ctrl-wrapper select-azul">
                <select name="opcIdnivel" id="level-item" class="form-control select-ctrl">
                    <option value="-1" >- <?php echo JrTexto::_("Select Level")?> -</option>
                    <?php if(!empty($this->niveles))
                    foreach ($this->niveles as $nivel){?>
                    <option value="<?php echo $nivel["idnivel"]; ?>"><?php echo $nivel["nombre"]?></option>
                    <?php }?>               
                </select>
            </div>
            <div class="col-xs-12 col-sm-6 select-ctrl-wrapper select-azul">
                <select name="opcIdunidad" id="unit-item" class="form-control select-ctrl">
                    <option value="-1" >- <?php echo JrTexto::_("Select Unit")?> -</option>             
                </select>
            </div>
        </div>

        
    </div>

    <div class="panel-body"> 
        <table class="table table-striped table-bordered" id="tblConfiguracion">
            <thead>
                <tr>
                    <th style="width: 30%;"></th>
                    <th colspan="4">
                        <label class="col-xs-12 text-center"><?php echo JrTexto::_('Optimal time'); ?></label>
                    </th>
                    <th>
                        <label class="col-xs-12 text-center"><?php echo JrTexto::_('Progress Scales'); ?></label>
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th style="width: 14%;">
                        <label class="col-sm-12 text-center">Actividades</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralAct" id="txtTiempoGeneralAct" placeholder="HH:MM:SS" value="00:00:00">
                        </div>
                        <div class="col-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos act" data-aplicaclase="tiempo-actividad"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th style="width: 16%;">
                        <label class="col-sm-12 text-center">Teacher Resources</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralTR" id="txtTiempoGeneralTR" placeholder="HH:MM:SS" value="00:00:00">
                        </div>
                        <div class="col-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos tresrc" data-aplicaclase="tiempo-tresrc"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th style="width: 14%;">
                        <label class="col-sm-12 text-center">Games</label>
                        <div class="col-sm-9" style="padding:0;">
                            <input type="text" class="form-control tiempo-general" name="txtTiempoGeneralG" id="txtTiempoGeneralG" placeholder="HH:MM:SS" value="00:00:00">
                        </div>
                        <div class="col-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos game" data-aplicaclase="tiempo-game"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                    <th style="width: 12%;">
                        <label class="col-sm-12 text-center">Total</label>
                    </th>
                    <th style="width: 14%;">
                        <div class="col-sm-9" style="padding:0;">
                            <button class="btn btn-grey btn-block" id="escalas-general"><?php echo JrTexto::_('Empty'); ?></button>
                        </div>
                        <div class="col-xs-12 col-sm-3" style="padding:0;">
                            <button class="btn btn-primary btn-block aplicar-todos escalas" data-aplicaclase="btnescalas"><i class="fa fa-level-down"></i></button>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="6">Elija un nivel y/o actividad</td></tr>
            </tbody>
        </table>
    </div>

    <div class="form-footer text-center">
        <a href="javascript:history.back()" class="btn btn-lg btn-default cerrarmodal" data-dismiss="modal"><i class=" fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
        <button id="btn-saveConfiguracion_docente" type="submit" class="btn btn-lg btn-success"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
    </div>
</div>


<section id="mdlEscalasRendimiento" class="hidden">
    <div class="row" id="escalas-rendimiento">
        <div class="col-xs-12 cabecera-escalas sr-only">
            <h3><?php echo JrTexto::_('Scales'); ?></h3>
        </div>
        <div class="col-xs-12 contendeor-escalas">
            <div class="col-xs-12 form-group escala">
                <div class="col-xs-5 col-sm-7">
                    <input type="text" class="form-control nombre" placeholder="<?php echo JrTexto::_('Scale name'); ?>">
                </div>
                <div class="col-xs-3 col-sm-2">
                    <input type="text" class="form-control min porcentaje" readonly="readonly">
                </div>
                <div class="col-xs-3 col-sm-2">
                    <input type="text" class="form-control max porcentaje" readonly="readonly">
                </div>
            </div>
            <div class="col-xs-12 form-group escala">
                <div class="col-xs-5 col-sm-7">
                    <input type="text" class="form-control nombre" placeholder="<?php echo JrTexto::_('Scale name'); ?>">
                </div>
                <div class="col-xs-3 col-sm-2">
                    <input type="text" class="form-control min porcentaje" readonly="readonly">
                </div>
                <div class="col-xs-3 col-sm-2">
                    <input type="text" class="form-control max porcentaje" readonly="readonly">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="row-escala" class="hidden">
    <div class="col-xs-12 form-group escala">
        <div class="col-xs-5 col-sm-7">
            <input type="text" class="form-control nombre" placeholder="<?php echo JrTexto::_('Scale name'); ?>">
        </div>
        <div class="col-xs-3 col-sm-2">
            <input type="text" class="form-control min porcentaje" readonly="readonly">
        </div>
        <div class="col-xs-3 col-sm-2">
            <input type="text" class="form-control max porcentaje" readonly="readonly">
        </div>
        <div class="col-xs-1 col-sm-1">
            <button class="btn btn-danger eliminar_escala"><i class="fa fa-trash"></i></button>
        </div>
    </div>
</section>



<script>
function toSeconds( time ) {
    var parts = time.split(':');
    return (+parts[0]) * 60 * 60 + (+parts[1]) * 60 + (+parts[2]); 
}

function toHHMMSS(sec) {
    var sec_num = parseInt(sec, 10); 
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}
$(document).ready(function() {
    $(".tiempo-general").mask("99:99:99",{placeholder:"00:00:00"});

    var fnAjaxFail = function(xhr, textStatus, errorThrown) {
        console.log("error"); 
        console.log(xhr.responseText);
        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    };

    var leerniveles=function(data){
        try{
            var res = xajax__('', 'niveles', 'getxPadre', data);
            if(res){ return res; }
            return false;
        }catch(error){
            return false;
        }       
    };

    var currentRequest = null;
    var listarActividades = function (idnivel, idunidad) {
        if(idunidad==''){
            var idpadre=idnivel;
            var tipo = 'U';
        }else{
            var idpadre=idunidad;
            var tipo = 'L';
        }
        currentRequest = $.ajax({
            url: _sysUrlBase_+'/configuracion_docente/tiemposXactividad/',
            type: 'POST',
            dataType: 'json',
            data: {'tipo':tipo,'idpadre':idpadre},
            beforeSend: function(){
                if(currentRequest != null) {
                    currentRequest.abort();
                }
                $('#tblConfiguracion tbody').html('<tr><td colspan="6"><h3 class="text-center"><i class="fa fa-cog fa-spin fa-fw"></i><span class="">Loading...</span></h3></td></tr>');
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                var html='';
                var actividades=resp.data;
                var claseA=$('#tblConfiguracion .btn.aplicar-todos.act').data('aplicaclase');
                var claseTR=$('#tblConfiguracion .btn.aplicar-todos.tresrc').data('aplicaclase');
                var claseG=$('#tblConfiguracion .btn.aplicar-todos.game').data('aplicaclase');
                $.each(actividades, function(i, v) {
                    html+='<tr data-idnivel="'+idnivel+'" data-idunidad="'+v['idpadre']+'" data-idactividad="'+v['idnivel']+'" data-idconfigdocente="'+v['idconfigdocente']+'">'+
                        '<td>'+v['nombre']+'</td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseA+'" name="txtTiempoActividad_'+v['idnivel']+'" id="txtTiempoActividad_'+v['idnivel']+'" value="'+v['tiempoactividades']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseTR+'" name="txtTiempoTeacherRsrc_'+v['idnivel']+'" id="txtTiempoTeacherRsrc_'+v['idnivel']+'" value="'+v['tiempoteacherresrc']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control '+claseG+'" name="txtTiempoGames_'+v['idnivel']+'" id="txtTiempoGames_'+v['idnivel']+'" value="'+v['tiempogames']+'" placeholder="HH:MM:SS"></div></td>'+
                        '<td><div class="col-xs-12 pull-right"><input type="text" class="form-control tiempo-total" name="txtTiempoTotal_'+v['idnivel']+'" id="txtTiempoTotal_'+v['idnivel']+'" value="'+v['tiempototal']+'" placeholder="HH:MM:SS" readonly="readonly"></div></td>'+
                        '<td><button class="btn btn-block btnescalas" name="btnEscalas_'+v['idnivel']+'" id="btnEscalas_'+v['idnivel']+'" data-escalas=\''+v['escalas']+'\'></button></td>';
                });
                if(html==''){
                    html='<tr><td colspan="6" class="text-center"><?php echo JrTexto::_('There are no results to show'); ?></td></tr>';
                }
                $('#tblConfiguracion tbody').html(html);
                $(".tiempo-actividad").mask("99:99:99",{placeholder:"00:00:00"});
                actualizarColorBotones($('.btnescalas'));
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.mensaje, 'error');
            }
            currentRequest=null;
        }).fail(fnAjaxFail);
    };

    var cargarListados = function(){
        var nivel = $('#level-item').val();
        var unidad = ($('#unit-item').val()!=='-1')?$('#unit-item').val():'';
        listarActividades(nivel, unidad);
    };

    var addniveles=function(data,obj){
        var objini=obj.find('option:first').clone();
        obj.find('option').remove();
        obj.append(objini);
        if(data!==false){
            var html='';
            $.each(data,function(i,v){
                html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
            });
            obj.append(html);
        }
        id=obj.attr('id');
        //if(id==='unit-item') cargarListados();
    };

    var calcularPorcentajes = function ($contenedor){
        var $escala = $contenedor.find('.escala');
        if($escala.length==0) return false;
        var cantInput = $escala.length;
        var division = 100/cantInput;
        var porcentaje = 0.0;
        $escala.find('.porcentaje').each(function(index, elem) {
            var $this = $(elem);
            var p=porcentaje.toFixed(2)+'%';
            $this.val(p).attr('value', p);
            if($this.hasClass('min')){ porcentaje+=division; }
        });
    };

    var isJSON = function(str){
        try { JSON.parse(str); } 
        catch (e) { return false; }
        return true;
    };

    var actualizarColorBotones = function($btn){
        $btn.each(function(index, elem) {
            var escalas = $(elem).attr('data-escalas') || '';
            var arrEscalas=[];
            if(isJSON(escalas)){
                arrEscalas = JSON.parse(escalas);
            }
            if(arrEscalas.length>0){
                $(elem).addClass('btn-green').removeClass('btn-grey');
                $(elem).text('<?php echo JrTexto::_('Completed'); ?>');
            } else {
                $(elem).addClass('btn-grey').removeClass('btn-green');
                $(elem).text('<?php echo JrTexto::_('Empty'); ?>');
            }
        });
    };

    $('#level-item').change(function(){
        var idnivel=$(this).val();
        var data={tipo:'U','idpadre':idnivel}
        var donde=$('#unit-item');
        if(idnivel!=='' || idnivel!=='-1') addniveles(leerniveles(data),donde);
        else addniveles(false,donde);
        donde.trigger('change');
    });

    $('#unit-item').change(function(){
        cargarListados();
        $('table .tiempo-general').val('00:00:00');
    });

    $('.btn.aplicar-todos').click(function(e) {
        e.preventDefault();
        var clase=$(this).data('aplicaclase');
        if($(this).hasClass('escalas')){
            var jsonEscalas = $(this).closest('th').find('#escalas-general').attr('data-escalas');
            $('table tbody .btnescalas').attr('data-escalas', jsonEscalas);
            $(this).closest('th').find('#escalas-general').attr('data-escalas','');
            actualizarColorBotones( $('*[data-escalas]') );
            $('#btn-saveConfiguracion_docente').trigger('click');
        }else{
            var nuevo_tiempo=$(this).closest('th').find('input.tiempo-general').val();
            $('.'+clase).each(function(index, el) {
                $(this).attr('value', nuevo_tiempo).val(nuevo_tiempo);
            });
            $('table .tiempo-general').val('00:00:00');
            $('.'+clase).trigger('change');
        }
    });

    $('input.tiempo-general').keypress(function(e) {
        if(e.which==13){
            e.preventDefault();
            $(this).closest('th').find('.btn.aplicar-todos').trigger('click');
        }
    });

    $('#tblConfiguracion').on('change', '.tiempo-actividad, .tiempo-tresrc, .tiempo-game', function(event) {
        var total=0;
        var $fila=$(this).closest('tr');
        $fila.each(function(){
            $(this).find('input.form-control').not('[readonly]').each(function(index, el) {
                total += toSeconds( $(this).val() );
            });
            $(this).find('input.form-control[readonly]').val( toHHMMSS(total) );
        });
    }).on('keypress', '.tiempo-actividad, .tiempo-tresrc, .tiempo-game', function(e)  {
        if(e.which==13){ 
            e.preventDefault(); 
            $(this).trigger('change');
        }
    });

    $('#btn-saveConfiguracion_docente').on('click', function(e) {
        e.preventDefault();
        var $contenedor = $('#configuracion_docente');
        var arrConfigs=[];
        $contenedor.find('#tblConfiguracion tbody>tr').each(function(index, elem) {
            var $fila = $(elem);
            var nuevo={
                'idconfigdocente':  ($fila.attr('data-idconfigdocente')=='null')?null:$fila.attr('data-idconfigdocente'),
                'idnivel':  $fila.attr('data-idnivel'),
                'idunidad':  $fila.attr('data-idunidad'),
                'idactividad':  $fila.attr('data-idactividad'),
                'tiempototal': $fila.find('input.tiempo-total').val(),
                'tiempoactividades': $fila.find('input.tiempo-actividad').val(),
                'tiempoteacherresrc': $fila.find('input.tiempo-tresrc').val(),
                'tiempogames': $fila.find('input.tiempo-game').val(),
                'escalas': $fila.find('.btn.btnescalas').attr('data-escalas'),
            };
            arrConfigs.push(nuevo);
        });

        $.ajax({
            url: _sysUrlBase_+'/configuracion_docente/xGuardarConfigDocente/',
            type: 'POST',
            dataType: 'json',
            data: {'arrConfigs': JSON.stringify(arrConfigs)},
        }).done(function(resp) {
            if(resp.code='ok'){
                mostrar_notificacion('<?php echo JrTexto::_('Success'); ?>!', resp.data.mensaje, 'success');
                $contenedor.modal('hide');
                /*setTimeout(function(){
                    window.location.reload();
                }, 1000);*/
            } else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function() {
            $('#mejorar_aprendizaje .btn').removeAttr('disabled');
        });
    })

    $('#escalas-general')

    $('body').on('click', '#escalas-general, .btnescalas', function(e) {
        e.preventDefault();
        var id=$(this).attr('id');
        var arrEscalas = ( isJSON($(this).attr('data-escalas')) )?JSON.parse($(this).attr('data-escalas')):[];
        var $modal = $('#modalclone').clone();
        $modal.attr('id', 'mdl_escalas_rendimiento');
        //$modal.find('.modal-dialog.modal-lg').css('width','80%');
        //$modal.find('.modal-dialog.modal-lg').removeClass('modal-lg');
        $modal.find('#modaltitle').text('<?php echo JrTexto::_('Edit').' - '.JrTexto::_('Scales'); ?>');
        $modal.find('#modalfooter').css('text-align', 'center');
        $modal.find('#modalfooter').find('.cerrarmodal').addClass('pull-left');
        $modal.find('#modalfooter').append('<button class="btn btn-primary agregar_escala"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add scale'); ?>');
        $modal.find('#modalfooter').append('<button id="btn-saveEscalas" class="btn btn-success pull-right" data-target="'+id+'"><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>');
        $('body').append($modal);
        $('#mdl_escalas_rendimiento').modal({keyboard:false, backdrop:'static'});

        $modal.find('#modalcontent').html($('#mdlEscalasRendimiento').html());
        if(arrEscalas.length>0){
            $modal.find('#modalcontent .contendeor-escalas').html('');
            $.each(arrEscalas, function(index, escala) {
                var $filaEscala = $('#row-escala').children().clone();
                $filaEscala.find('.nombre').val(escala.nombre);
                $filaEscala.find('.min.porcentaje').val(escala.minimo.toFixed(2)+'%');
                $filaEscala.find('.max.porcentaje').val(escala.maximo.toFixed(2)+'%');
                if(index<2){ $filaEscala.find('.eliminar_escala').parent().remove(); }
                $modal.find('#modalcontent .contendeor-escalas').append($filaEscala);
            });
        }else{
            calcularPorcentajes($('#mdl_escalas_rendimiento'));
        }
    }).on('click', '.btn.agregar_escala', function(e) {
        e.preventDefault();
        var $nuevo_escala = $('#row-escala').html();
        $('#mdl_escalas_rendimiento').find('.contendeor-escalas').append($nuevo_escala);
        calcularPorcentajes($('#mdl_escalas_rendimiento'));
    }).on('click', '.btn.eliminar_escala', function(e) {
        e.preventDefault();
        $(this).closest('.escala').remove();
        calcularPorcentajes($('#mdl_escalas_rendimiento'));
    }).on('click', '#btn-saveEscalas', function(e) {
        e.preventDefault();
        var arrEscalas = [];
        var puedeContinuar =true;
        /* validando escalas vacias */
        $('#mdl_escalas_rendimiento').find('.escala').each(function(index, el) {
            if($(this).find('.nombre').val().trim().length==0){
                $(this).addClass('has-error');
                puedeContinuar = false;
            }
        });
        if(!puedeContinuar){ return false; }

        $('#mdl_escalas_rendimiento').find('.escala').each(function(index, el) {
            if($(this).find('.nombre').val().trim().length==0){
                $(this).addClass('has-error');
                puedeContinuar = false;
                return false;
            }
            var new_escala = {
                'nombre': $(this).find('.nombre').val(),
                'minimo': parseFloat($(this).find('.min.porcentaje').val().replace('%','')),
                'maximo': parseFloat($(this).find('.max.porcentaje').val().replace('%','')),
            };
            arrEscalas.push(new_escala);
        });
        
        var idTarget = $(this).attr('data-target');
        $('#'+idTarget).attr('data-escalas', JSON.stringify(arrEscalas));
        actualizarColorBotones($('*[data-escalas]'));

        if(idTarget!='escalas-general'){
            $('#btn-saveConfiguracion_docente').trigger('click');
        }
        $('#mdl_escalas_rendimiento').modal('hide');
    }).on('hidden.bs.modal', '#mdl_escalas_rendimiento', function (e) {
        $('#mdl_escalas_rendimiento').remove();
    }).on('keyup', '.modal .escala .nombre', function(e) {
        if($(this).val().trim().length>0){
            $(this).closest('.escala').removeClass('has-error');
        }else{
            $(this).closest('.escala').addClass('has-error');
        }
    });;
});
</script>