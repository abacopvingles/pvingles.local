<?php 
	$arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
?>
<style type="text/css">
	.namelevel{
		text-shadow: 1px 1px 3px rgba(150, 150, 150, 1);
		font-size: 3em;
		font-weight: bold;
		color: #b32709;
	}
	.txtinfolevel{
		padding: 1ex;
	}

	.btn-square{
		min-height: 90px;
	}

	.row >.item-recurso{
		text-align: center;
		padding: 1ex;
	}
	.row >.item-recurso .panel{
		text-align: center;
		padding: 0.2ex;
		margin:0.05ex 0.1ex;
	}
	.row >.item-recurso .panel-body{
		padding: 0.1ex;
	}
		.slick-slide{
		position: relative;
	}

	.titulo{
		border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
		width: 85%;
		background-color: rgba(255, 255, 255, 0.53);
    	color: #000;
	}
	.autor{
		border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
		width: 85%; text-align: right; 
		background-color: rgba(255, 255, 255, 0.53);
    	color: #000;
	}
	.caratula{
		border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 70%; height: 68%	
	}
	
	.filtros-habilidades .form-group{
		margin-bottom: 9px;
	}
	.btn-panel-container{
		font-size: 13px;
	}

	.btn-panel-container>.btn-panel{
		text-align: center;
		text-decoration: none;
	}
    
    .btn-panel-container {
		min-height:180px;
	}
	@media (min-width: 1250px) and (max-height: 800px){ 
		.namelevel{		
			font-size: 2em;		
		}
		.widget-body{
			max-height: 270px;
		}
		.btn-square{
			min-height: 77px;
		}
		.widget-main{
			margin: 1px;
			padding: 1px;
		}
	}
	@media (min-height: -width: 992px) {

		    #smartasktareacss{
		    	margin-left: -9px;
		    }
	}
@media (max-width : 320px){

#logo>img {
    max-height: 52px;
    margin-top: 5px;
}
}
.slick-slider{
        margin-bottom: 0.5ex !important;
    }
.btn-panel-container.panelcont-xs>.btn-panel {
    font-size:1.55em;
}
</style>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel" style="border: 1px solid #dad7d7;  margin-top: 1ex; margin-bottom:0px;  ">
				<div class="panel-heading bg-red" style="color:#fff; font-size: 0.9em;">
					<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('My English Courses'); ?></h3>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">			
				   <div class="row">
				   		<div class="col-md-12">
                        <div class="slick-items">
					    <?php if(!empty($this->cursos)){
		                    $cnivel=count($this->cursos);
		                    $inivel=0;
		                    foreach ($this->cursos as $nivel){ $inivel++; //var_dump($nivel);?>
                            <div class="slick-item">
							<div class="hvr-float" style="margin: 0.25ex 0.5ex; border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);">
							   <a href="<?php echo $this->documento->getUrlBase(); ?>/recursos/listar/<?php echo $nivel["idcurso"] ?>" style="text-decoration: none; color:#000;"> 
								<div><img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/levels/<?php echo strtolower($nivel["nombre"]); ?>.png" class="img img-responsive"></div>
								<div class="namelevel text-center"><?php echo $nivel["nombre"]; ?></div>
								<div class="txtinfolevel">
									<div><strong><?php echo JrTexto::_('Total') ?>:</strong><span> <?php echo @$nivel["nunidad"]; ?> </span><?php echo JrTexto::_('Units') ?></div>
									<div><strong><?php echo JrTexto::_('Total') ?>:</strong><span> <?php echo @$nivel["nactividad"]; ?> </span> <?php echo JrTexto::_('Activities') ?></div>
								</div>
								</a>
							</div>
                            </div>
							<?php }}?>
                            </div>
						</div>
					</div>
				</div>								
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/examenes/examenesxcurso" class="btn-block btn-green btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-list"></i>
                        <?php echo ucfirst(JrTexto::_('Smartquiz')); ?>
                    </a>
                </div>
                <!--div class="col-md-6 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php //echo $this->documento->getUrlBase();?>/rubricas/seguridad.php?id=<?php //echo $this->iddoc?>" class="btn-block btn-blue btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-list-ol"></i>
                        <?php //echo ucfirst(JrTexto::_('Smartquality')); ?>
                    </a>
                </div-->
        	
				<!--div class="col-md-6 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/smartclass/" class="btn-block btn-lilac btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-university"></i>
                        <?php echo ucfirst(JrTexto::_('Smartclass')); ?>
                    </a>
                </div-->
                <div class="col-md-2 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">                       
                        <a href="#" data-source="<?php echo $this->documento->getUrlSitio()?>/sidebar_pages/diccionario" class="btn btn-block btn-yellow btn-panel hvr-float-shadow slide-sidebar-right">
		                <i class="btn-icon fa fa-flag"></i>
		                <?php echo ucfirst(JrTexto::_('Dictionary')); ?>
		            </a>
                </div>
                <div class="col-md-2 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/library" class="btn-block btn-red btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-book"></i>
                        <?php echo ucfirst(JrTexto::_('Library')); ?>
                    </a>
                </div>	
				
				
				<div class="col-md-2 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/smartbook" class="btn-block btn-orange btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-address-book-o"></i>
                        <?php echo ucfirst(JrTexto::_('Smartbook')); ?>
                    </a>
                </div>
                
        	
				<div class="col-md-3 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/academico" class="btn-block btn-info btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-graduation-cap"></i>
                        <?php echo ucfirst(JrTexto::_('Control Panel')); ?>
                    </a>
                </div>
                             	
      
				
				<!--div class="col-md-6 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/docente/panelcontrol" class="btn-block btn-lilac btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-tachometer"></i>
                        <?php echo ucfirst(JrTexto::_('Smartracking')); ?>
                    </a>
                </div-->
                <div class="col-md-3 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/workbooks/" class="btn btn-block btn-blue btn-panel hvr-float-shadow">
		                <i class="btn-icon fa fa-file-word-o"></i>
		                <?php echo ucfirst(JrTexto::_('Workbooks')); ?>
		            </a>
                </div>
                <div class=" col-md-3 col-sm-6 col-xs-12  btn-panel-container panelcont-xs">
		            <a href="<?php echo $this->documento->getUrlBase();?>/guiapedagogica/" class="btn btn-block btn-orange btn-panel hvr-float-shadow">
		                <i class="btn-icon fa fa-folder-open"></i>
		                <?php echo ucfirst(JrTexto::_('Manual of pedagogical orientations')); ?>
		            </a>
		        </div>

				<div class="col-md-3 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                    <a href="<?php echo $this->documento->getUrlBase();?>/smartcourse" class="btn-block btn-red btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-tachometer"></i>
                        <?php echo ucfirst(JrTexto::_('Smartcourse')); ?>
                    </a>
                </div>

                <div id="smartasktareacss" class="col-md-3 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
                	 <a href="<?php echo $this->documento->getUrlBase();?>/tarea" class="btn-block btn-green btn-panel hvr-outline-in">
                        <i class="btn-icon fa fa-briefcase"></i>
                        <?php echo ucfirst(JrTexto::_('Smartask')); ?>
                    </a>	                        
                </div>       	
				

          		
                	<!--div class="row">
						<div class="col-md-12 col-sm-12  col-xs-12 btn-panel-container panelcont-xs">
	                        <a href="<?php //echo $this->documento->getUrlBase();?>/rubricas/repor_rubricas1.php" class="btn-block btn-red btn-panel hvr-outline-in">
	                            <i class="btn-icon fa fa-tachometer"></i>
	                            <?php //echo ucfirst(JrTexto::_('Monitoring of Teacher Performance')); ?>
	                        </a>
	                    </div>
                	</div-->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.imagepay').graficocircle();
	$('header').show('fast').addClass('static')
	var leerniveles=function(data){
        try{
            var res = xajax__('', 'niveles', 'getxPadre', data);
            if(res){ return res; }
            return false;
        }catch(error){
            return false;
        }       
    };

    var addniveles=function(data,obj){
    	var objini=obj.find('option:first').clone();
    	obj.find('option').remove();
    	obj.append(objini);
    	var html='';
    	$.each(data,function(i,v){
    		html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';	    		
    	})
    	obj.append(html);
    	cargarexamenes();
    };

    var cargarexamenes=function(){
    	console.log('Aqui comienza a actualizar las habilidades');
    };

    $('#opcNivel').change(function(e){
    	var idnivel=$(this).val();
        var data={tipo:'U','idpadre':idnivel}
        var donde=$('#opcUnidad');
        addniveles(leerniveles(data),donde);
    });

    $('#opcUnidad').change(function(){
    	var idunidad=$(this).val();
        var data={tipo:'L','idpadre':idunidad}
        var donde=$('#opcActividad');
        addniveles(leerniveles(data),donde);
    });

    var optionslike={
            //dots: true,
            infinite: false,
            //speed: 300,
            //adaptiveHeight: true
            navigation: false,
            slidesToScroll: 1,
            centerPadding: '60px',
          slidesToShow: 5,
          responsive:[
              { breakpoint: 1200, settings: {slidesToShow: 5} },
              { breakpoint: 992, settings: {slidesToShow: 4 } },
              { breakpoint: 880, settings: {slidesToShow: 3 } },
              { breakpoint: 720, settings: {slidesToShow: 2 } },
              { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }       
          ]
        };
    var slikitems=$('.slick-items').slick(optionslike);
});
</script>
