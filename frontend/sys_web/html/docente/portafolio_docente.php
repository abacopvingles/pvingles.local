<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="container">
	<div class="row text-center">
		<div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/notas" class="btn-block btn-red btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-list-alt"></i>
	            <?php echo ucfirst(JrTexto::_('Record of scores')); ?>
	        </a>
	    </div>
		<div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/calificaciones" class="btn-block btn-info btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-table"></i>
	            <?php echo ucfirst(JrTexto::_('View scores and Reports')); ?>
	        </a>
	    </div>
		<div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/agenda" class="btn-block btn-green btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-calendar-plus-o"></i>
	            <?php echo ucfirst(JrTexto::_('Agenda')); ?>
	        </a>
	    </div>
	    <div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/agenda/horario" class="btn-block btn-orange btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-calendar"></i>
	            <?php echo ucfirst(JrTexto::_('Schedule')); ?>
	        </a>
	    </div>
	    <div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/tarea/" class="btn-block btn-blue btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-briefcase"></i>
	            <?php echo ucfirst(JrTexto::_('Pending Task')); ?>
	        </a>
	    </div>
	    <div class="col-md-4 col-sm-6  col-xs-12 btn-panel-container panelcont-xs">
	        <a href="<?php echo $this->documento->getUrlBase();?>/rubricas/seguridad.php?id=<?php echo $this->usuarioAct["dni"]?>" class="btn-block btn-lilac btn-panel hvr-outline-in">
	            <i class="btn-icon fa fa-tachometer"></i>
	            <?php echo ucfirst(JrTexto::_('SmartQuality results')); ?>
	        </a>
	    </div>
	</div>
</div>