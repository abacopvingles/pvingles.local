<?php
defined("RUTA_BASE") or die();
if(isset($this->idCurso)){
    echo '<input type="hidden" id="idcurso" value="'.$this->idCurso.'"/>';
}
if(isset($this->ultimavisita)){
    echo '<input type="hidden" id="timeUltimo" value="'.$this->ultimavisita.'"/>';
}
if(isset($this->actualvisita)){
    echo '<input type="hidden" id="timeActual" value="'.$this->actualvisita.'"/>';
}
?>
<style>
    #cpanel_docente .widget-box .widget-body{
        height: auto;
    }
    #contenedor-pnls-info .widget-box .widget-body{
        min-height: 460px;
        max-height: 470px;
    }
    #pnl-datosfiltro .listado-alums{
        max-height: 100px; 
        overflow: auto;
    }
    #chart_progreso_linear .chart-leyenda,
    #chart_progreso_pie .chart-leyenda{
        border: 1px solid;
        border-color: rgba(0,0,0,0.2);
    }
    #chart_progreso_linear .chart-leyenda .caja-color,
    #chart_progreso_pie .chart-leyenda .caja-color{
        display: inline-block;
        height: 1em;
        width: 1.5em; 
    }
    #chart_progreso_linear .chart-leyenda span,
    #chart_progreso_pie .chart-leyenda span{
        display: inline-block;
    }

    .card{ display: block; width: 100%; border: 1px solid #ccc; padding: 0;
    }
    .card .card-title{ color:black; font-size: 15px; font-weight: bold; border-bottom: 1px solid #ccc; padding-left: 15px; line-height: 2.2;
    }
    .card .card-info{
        font-size: 2.5em;
        text-shadow: 2px 1px 3px #aaa;
        font-weight: bolder;
        text-align: center;
        border-bottom: 1px solid #efefef;
    }
    .card .card-info:last-child{ border-bottom:none; }

    .card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }

    .card.card-time .card-info .anio:after,
    .card.card-time .card-info .mes:after,
    .card.card-time .card-info .dia:after,
    .card.card-time .card-info .hora:after,
    .card.card-time .card-info .min:after,
    .card.card-time .card-info .seg:after{
        font-size: 0.7em;
    }
    .card.card-time .card-info .anio:after{ content: "y"; }
    .card.card-time .card-info .mes:after { content: "m"; }
    .card.card-time .card-info .dia:after { content: "d"; }
    .card.card-time .card-info .hora:after{ content: "h"; }
    .card.card-time .card-info .min:after { content: "m"; }
    .card.card-time .card-info .seg:after { content: "s"; }

    .card .card-info .promedio{
        line-height: 75px;
    }
    .card .card-info .comentario span{
        display: block;
    }
    .card .card-info .comentario span.letras{
        font-size: .5em;
    }

    kbd{
        margin-right: 4px;
        margin-bottom: 4px;
        display: inline-block;
    }

    .slick-prev { left: 10px !important; }
    .slick-next { right: 10px !important; }
    .slick-slider { margin-bottom: 5px !important; }

    #chart-lines .ct-label{ font-size: 1em !important; }
    #chart-pie .ct-label{ fill: #fff; font-size: 1.2em !important; font-weight: bolder; }
    
    .ct-series { fill: none; }
    .ct-label { font-family: Arial, Helvetica, sans-serif; }

    .numeracion{ font-style: italic; font-size: 1.5em; margin-right: 5px; }
    .numeracion>span:first-child{ font-size: 1.7em; font-style: italic; }
    .numeracion>span:nth-child(2):before{ content: '/ '; }
    
    .bg-lilac { background-color: #a286ff; color: #fff; }
    .bg-green2 { background-color: #2ed353; color: #ffffff; }
    .bg-pink { background-color: #ffb283; color: #ffffff; }

    .widget-es-box { min-height:500px; max-height:600px; overflow:scroll; }
    #tiempos .card h4 { color:black; }
    .widget-title>span,.widget-title>i{color:white; }
    .text-custom1{ color:black; text-align:center; font-size:large; }
    .circleProgressBar1{
    padding:5px 0;
    }
    .circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
    margin:0 auto;
    }
</style>
<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li class="active">
          <?php 
              echo JrTexto::_("Tracking");
          ?>
        </li>
	  </ol>	
	</div>
</div>
<div class="col-md-12" id="contenedor-alumno">

    <!--start widget-->
    <div class="col-sm-12 widget-es-box" id="tiempos">
        <div class="widget-header bg-blue">
            <h4 class="widget-title"  style="text-transform: inherit;">
                <i class="fa fa-clock-o"></i>
                <span><?php echo JrTexto::_("Students Tracking"); ?></span>
            </h4>
            <div class="widget-body" style="height:initial!important;width: 98.5%;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card card-time time-acumulado">
                            <div class="card-title">
                                <?php echo JrTexto::_('Time in the Virtual Platform'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos" style="border: 1px solid black; border-radius: 0 0 0.5em 0.5em;"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Last visit tracking time date'); ?></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                        <div class="col-xs-6" style="border: 1px solid black; border-radius: 0.5em; margin-top: 5%;">
                            <h4 style="color:black;"><?php echo ucfirst(JrTexto::_('Sessions Progress')); ?></h4>
                            <div class="chart-container" id="chartresumeactivity" style="position: relative;  width:100%; ">
                                <canvas id="progressActivityResume" style="display: block; margin:0 auto; width:100%; height: 313px;"></canvas>
                            </div>
                        </div>
                        <div class="col-xs-6" style="border: 1px solid black; border-radius: 0.5em; margin-top: 5%;">
                            <h4 style="color:black;"><?php echo ucfirst(JrTexto::_('Tests Progress')); ?></h4>
                            <div class="chart-container" id="chartresumeexams" style="position: relative;  width:100%; ">
                                <canvas id="progressExamsResume" style="display: block; margin:0 auto; width:100%; height: 313px;"></canvas>
                            </div>
                        </div>
                    </div>
                    <!--endrow-->
                </div>
            </div>
        </div>
    </div>
    <!--end widget-->

    <!--start widget-->
    <div class="col-sm-12 widget-es-box" id="tiempos">
        <div class="widget-header bg-lilac">
            <h4 class="widget-title" style="text-transform: inherit;">
                <i class="fa fa-clock-o"></i>
                <span><?php echo JrTexto::_('Time in the Virtual Platform'); ?></span>
            </h4>
            <div class="widget-body" style="height:initial!important;width: 98.5%;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card card-time time-acumulado">
                            <div class="card-title">
                                <?php echo JrTexto::_('Total Accumulated Time'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Last visit tracking time date'); ?><br><?php echo JrTexto::_('to date'); ?> <span style="color: #77010f; text-decoration: underline;"><?php echo $this->ultimavisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?><br><span style="color: darkgreen; text-decoration: underline;"><?php echo $this->actualvisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                        <div class="card card-time time-tiempopv">
                            <div class="card-title">
                                <?php echo JrTexto::_('Time in the virtual platform'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Tracking time of the last visit'); ?><br><?php echo JrTexto::_('to date'); ?> <span style="color: #77010f; text-decoration: underline;"><?php echo $this->ultimavisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?><br><span style="color: darkgreen; text-decoration: underline;"><?php echo $this->actualvisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                        <div class="card card-time time-smartquiz">
                            <div class="card-title">
                                <?php echo JrTexto::_('Tests Time'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Last visit tracking time date'); ?><br><?php echo JrTexto::_('to date'); ?> <span style="color: #77010f; text-decoration: underline;"><?php echo $this->ultimavisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?><br><span style="color: darkgreen; text-decoration: underline;"><?php echo $this->actualvisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                        <div class="card card-time time-smartbook">
                            <div class="card-title">
                                <?php echo JrTexto::_('SmartBook Time'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Last visit tracking time date'); ?><br><?php echo JrTexto::_('to date'); ?> <span style="color: #77010f; text-decoration: underline;"><?php echo $this->ultimavisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?><br><span style="color: darkgreen; text-decoration: underline;"><?php echo $this->actualvisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                        <div class="card card-time time-homework">
                            <div class="card-title">
                                <?php echo JrTexto::_('Activities Time'); ?>
                            </div>
                            <div class="card-info">
                                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Last visit tracking time date'); ?><br><?php echo JrTexto::_('to date'); ?> <span style="color: #77010f; text-decoration: underline;"><?php echo $this->ultimavisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido ultimo" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <h4><?php echo JrTexto::_('Total Time'); ?><br><span style="color: darkgreen; text-decoration: underline;"><?php echo $this->actualvisita; ?></span></h4>
                                        <div class="info-tiempo tiempo-obtenido actual" style="color:black;">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--endcard-->
                    </div>
                    <!--endrow-->
                </div>
            </div>
        </div>
    </div>
    <!--end widget-->
    <!--START WIDGET PROGRESS SKILL ON ACTIVITY-->
    
    <!--start widget-->
    <div class="col-sm-12 widget-es-box" id="tiempos">
        <div class="widget-header bg-green2">
            <h4 class="widget-title" style="text-transform: inherit;">
                <i class="fa fa-clock-o"></i>
                <span><?php echo JrTexto::_('Sessions Progress'); ?></span>
            </h4>
            <div class="widget-body" style="height:initial!important;width: 98.5%;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="chart-container chartprogress-activity" style="position: relative;  width:100%; ">
                            <canvas id="progressActivity" style="display: block; margin:0 auto; height: 313px;"></canvas>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        
                        <div class="col-sm-6" style="background: #f9ecef; margin-top:15px;">
                            <div style="position: relative; width: 106%; left: -3%; top: -9px; padding: 5px; background: #9a8f92; border-radius: 1em 1em 0 0;"><h3><?php echo JrTexto::_('Last visit tracking time date'); ?></h3></div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Listening</p>
                                <div id="barListening" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Reading</p>
                                <div id="barReading" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Writing</p>
                                <div id="barWriting" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Speaking</p>
                                <div id="barSpeaking" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                        </div>
                        <div class="col-sm-6" style="background: #e5edf7; margin-top:15px;">
                            <div style="position: relative; width: 106%; left: -3%; top: -9px; padding: 5px; background: #8b9aad; border-radius: 1em 1em 0 0;"><h3><?php echo JrTexto::_('Total Time'); ?></h3></div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Listening</p>
                                <div id="barListening2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Reading</p>
                                <div id="barReading2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Writing</p>
                                <div id="barWriting2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-custom1">Speaking</p>
                                <div id="barSpeaking2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!--endrow-->
            </div>
        </div>
    </div>
    <!--end widget-->
    <!--END WIDGET PROGRESS SKILL ON ACTIVITY-->
    <!--START WIDGET PROGRESS SKILL ON ACTIVITY-->
    
    <!--start widget-->
    <div class="col-sm-12 widget-es-box" id="tiempos">
        <div class="widget-header bg-red">
            <h4 class="widget-title" style="text-transform: inherit;">
                <i class="fa fa-clock-o"></i>
                <span><?php echo JrTexto::_('Tests Progress'); ?></span>
            </h4>
            <div class="widget-body" style="height:initial!important;width: 98.5%;">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12">
                            <div class="chart-container " id="chartprogress-exams" style="position: relative;  width:100%; ">
                                <canvas id="progressExams" style="display: block; margin:0 auto; height: 313px;"></canvas>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            
                            <div class="col-sm-6" style="background: #f9ecef; margin-top:15px;">
                                <div style="position: relative; width: 106%; left: -3%; top: -9px; padding: 5px; background: #9a8f92; border-radius: 1em 1em 0 0;"><h3><?php echo JrTexto::_('Last visit tracking time date'); ?></h3></div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Listening</p>
                                    <div id="barExamsListening" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Reading</p>
                                    <div id="barExamsReading" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Writing</p>
                                    <div id="barExamsWriting" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Speaking</p>
                                    <div id="barExamsSpeaking" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                            </div>
                            <div class="col-sm-6" style="background: #e5edf7; margin-top:15px;">
                                <div style="position: relative; width: 106%; left: -3%; top: -9px; padding: 5px; background: #8b9aad; border-radius: 1em 1em 0 0;"><h3><?php echo JrTexto::_('Total Time'); ?></h3></div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Listening</p>
                                    <div id="barExamsListening2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Reading</p>
                                    <div id="barExamsReading2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Writing</p>
                                    <div id="barExamsWriting2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                                <div class="col-sm-12">
                                    <p class="text-custom1">Speaking</p>
                                    <div id="barExamsSpeaking2" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <!--endrow-->
            </div>
        </div>
    </div>
    <!--end widget-->
    <!--END WIDGET PROGRESS SKILL ON ACTIVITY-->
</div>

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">

function drawHabilidad(obj , option = null, value = 0, speed = null){
    var _anim = speed === null ? 1 : speed;
    var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
    option.animationSpeed = _anim;
    var bar=new RadialProgress(obj,option);
    bar.setValue(value);
    bar.draw(true);
}

function drawChart2(obj,dat,texto  = 'Comparison of beginning and final tests' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: false,
      title: {
        display: true,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            // console.log(tooltipModel.body[0].lines[0]);
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Difference');
            }
          }
        }
        // mode: 'index',
        // intersect: true,
        // filter: function (tooltipItem, data) {
        //     var label = data.labels[tooltipItem.index];
            
        //     // console.log(tooltipItem, data, label);
        //     console.log(label);
        //     // if (label != "hide") {
        //       return true;
        //     // } 
        // }  
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }
          // ci.data.datasets.forEach(function(e, i) {
          //   var meta = ci.getDatasetMeta(i);

          //   if (i !== index) {
          //     if (!alreadyHidden) {
          //       meta.hidden = meta.hidden === null ? !meta.hidden : null;
          //     } else if (meta.hidden === null) {
          //       meta.hidden = true;
          //     }
          //   } else if (i === index) {
          //     meta.hidden = null;
          //   }
          // });

          ci.update();
          // var index = legendItem.datasetIndex;
          // var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          
          

          // ci.update();
        }
      }//end legend
    }
  });
}

function drawChart(obj,dat,texto  = '' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: false,
      title: {
        display: true,
        text: texto
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      scales: {
            xAxes: [{
                stacked: false,
            }],
            yAxes: [{
                stacked: false
            }]
        }
    }
  });
}

function main(){
    /**Cargar los tiempos... */
    $.ajax({
        url: _sysUrlBase_+'/docente/tiempos',
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': $('#idcurso').val(), 'actual': $('#timeActual').val(), 'anterior': $('#timeUltimo').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            // console.log(resp.data);

            if(resp.data.tiemposanterior != null && Object.keys(resp.data.tiemposanterior).length > 0){
                var displaytime = resp.data.tiemposanterior.total.split(":");
                // console.log(displaytime);
                $('.time-acumulado').find('.ultimo').find('.hora').text(displaytime[0]);
                $('.time-acumulado').find('.ultimo').find('.min').text(displaytime[1]);
                $('.time-acumulado').find('.ultimo').find('.seg').text(displaytime[2]);

                displaytime = resp.data.tiemposanterior.tiempopv.split(":");
                $('.time-tiempopv').find('.ultimo').find('.hora').text(displaytime[0]);
                $('.time-tiempopv').find('.ultimo').find('.min').text(displaytime[1]);
                $('.time-tiempopv').find('.ultimo').find('.seg').text(displaytime[2]);
                displaytime = resp.data.tiemposanterior.smartbook.split(":");
                $('.time-smartbook').find('.ultimo').find('.hora').text(displaytime[0]);
                $('.time-smartbook').find('.ultimo').find('.min').text(displaytime[1]);
                $('.time-smartbook').find('.ultimo').find('.seg').text(displaytime[2]);
                displaytime = resp.data.tiemposanterior.homework.split(":");
                $('.time-homework').find('.ultimo').find('.hora').text(displaytime[0]);
                $('.time-homework').find('.ultimo').find('.min').text(displaytime[1]);
                $('.time-homework').find('.ultimo').find('.seg').text(displaytime[2]);
                
            }//end if
            
            if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){

                var displaytime = resp.data.tiemposactual.total.split(":");

                $('.time-acumulado').find('.actual').find('.hora').text(displaytime[0]);
                $('.time-acumulado').find('.actual').find('.min').text(displaytime[1]);
                $('.time-acumulado').find('.actual').find('.seg').text(displaytime[2]);
                
                displaytime = resp.data.tiemposactual.tiempopv.split(":");

                $('.time-tiempopv').find('.actual').find('.hora').text(displaytime[0]);
                $('.time-tiempopv').find('.actual').find('.min').text(displaytime[1]);
                $('.time-tiempopv').find('.actual').find('.seg').text(displaytime[2]);

                displaytime = resp.data.tiemposactual.smartbook.split(":");
                $('.time-smartbook').find('.actual').find('.hora').text(displaytime[0]);
                $('.time-smartbook').find('.actual').find('.min').text(displaytime[1]);
                $('.time-smartbook').find('.actual').find('.seg').text(displaytime[2]);

                displaytime = resp.data.tiemposactual.homework.split(":");
                $('.time-homework').find('.actual').find('.hora').text(displaytime[0]);
                $('.time-homework').find('.actual').find('.min').text(displaytime[1]);
                $('.time-homework').find('.actual').find('.seg').text(displaytime[2]);

                displaytime = resp.data.tiemposactual.smartquiz.split(":");
                $('.time-smartquiz').find('.atual').find('.hora').text(displaytime[0]);
                $('.time-smartquiz').find('.atual').find('.min').text(displaytime[1]);
                $('.time-smartquiz').find('.atual').find('.seg').text(displaytime[2]);

                

                
            }//end if
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        console.log("%c ERROR","font-size:40px;");
        return false;
    });
    /**Cargar los progresos... */
    $.ajax({
        url: _sysUrlBase_+'/docente/progresoxactividad',
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': $('#idcurso').val(), 'actual': $('#timeActual').val(), 'anterior': $('#timeUltimo').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var drawhab = new Object;
            var color1 = '#36a2eb';
            var color2 = '#ff6384';
            var color3 = '#d4b02f';
            var option = {colorFg:"#36a2eb",thick:15,fixedTextSize:0.3,colorText:"#000000" }

            if(resp.data.tiemposanterior != null && Object.keys(resp.data.tiemposanterior).length > 0){
                //hacer algo
                drawhab.listen = resp.data.tiemposanterior[4];
                drawhab.read = resp.data.tiemposanterior[5];
                drawhab.write = resp.data.tiemposanterior[6];
                drawhab.speak = resp.data.tiemposanterior[7];
                drawHabilidad(document.getElementById("barListening"),option,drawhab.listen ,0.15);
                drawHabilidad(document.getElementById("barReading"),option,drawhab.read ,0.15);
                drawHabilidad(document.getElementById("barWriting"),option,drawhab.write ,0.15);
                drawHabilidad(document.getElementById("barSpeaking"),option,drawhab.speak ,0.15);
            }else{
                drawhab.listen = 0;
                drawhab.read = 0;
                drawhab.write = 0;
                drawhab.speak = 0;
            }
            if(resp.data.tiemposactual != null && Object.keys(resp.data.tiemposactual).length > 0){
                //hacer algo
                drawhab.listennow = resp.data.tiemposactual[4];
                drawhab.readnow = resp.data.tiemposactual[5];
                drawhab.writenow = resp.data.tiemposactual[6];
                drawhab.speaknow = resp.data.tiemposactual[7];
                drawHabilidad(document.getElementById("barListening2"),option, drawhab.listennow,0.15);
                drawHabilidad(document.getElementById("barReading2"),option, drawhab.readnow,0.15);
                drawHabilidad(document.getElementById("barWriting2"),option, drawhab.writenow,0.15);
                drawHabilidad(document.getElementById("barSpeaking2"),option, drawhab.speaknow,0.15);
            }else{
                drawhab.listennow = 0;
                drawhab.readnow = 0;
                drawhab.writenow = 0;
                drawhab.speaknow = 0;
            }
            // var barChartData = {
            //     labels: ["Listening", "Reading", "Writing", "Speaking"],
            //     datasets: [{
            //         label: 'Previous',
            //         backgroundColor: color2,
            //         data: [
            //             drawhab.listen * 100,drawhab.read * 100,drawhab.write * 100,drawhab.speak * 100
            //         ]
            //     },
            //         {
            //         label: 'Current',
            //         backgroundColor: color1,
            //         data: [
            //             drawhab.listennow * 100,drawhab.readnow * 100,drawhab.writenow * 100,drawhab.speaknow * 100
            //         ]
            //     },{
            //         label: 'Difference',
            //         backgroundColor: color3,
            //         data: [
            //             Math.abs(drawhab.listennow - drawhab.listen) * 100, Math.abs(drawhab.readnow - drawhab.read) * 100, Math.abs(drawhab.writenow - drawhab.write) * 100, Math.abs(drawhab.speaknow - drawhab.speak) * 100
            //         ]
            //     }]

            // };

            var datosRadar = new Array();
            datosRadar[0] = drawhab.listen;
            datosRadar[1] = drawhab.read;
            datosRadar[2] = drawhab.write;
            datosRadar[3] = drawhab.speak;
            var datosRadar2 = new Array();
            datosRadar2[0] = drawhab.listennow;
            datosRadar2[1] = drawhab.readnow;
            datosRadar2[2] = drawhab.writenow;
            datosRadar2[3] = drawhab.speaknow;
            console.log(drawhab.writenow + " " +drawhab.write);
            var datosRadar3 = new Array();
            datosRadar3[0] = 0;
            datosRadar3[1] = 0;
            datosRadar3[2] = 0;
            datosRadar3[3] = 0;
            var datosRadar4 = new Array();
            datosRadar4[0] = 0;
            datosRadar4[1] = 0;
            datosRadar4[2] = 0;
            datosRadar4[3] = 0;

            for(var i = 0; i < 4; i++){
                if(datosRadar[i] > datosRadar2[i]){
                datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]) * 100;
                }else if(datosRadar[i] < datosRadar2[i]){
                datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]) * 100;
                }
            }

            var barChartData = {
                labels: ["Listening", "Reading", "Writing", "Speaking"],
                datasets: [{
                    label: 'Previous',
                    backgroundColor: color2,
                    stack:'Stack 0',

                    data: [
                        drawhab.listen * 100,drawhab.read *100,drawhab.write *100,drawhab.speak *100
                    ]
                },{
                    label: 'Current',
                    backgroundColor: color1,
                    stack:'Stack 1',
                    data: [
                        drawhab.listennow *100,drawhab.readnow *100,drawhab.writenow *100,drawhab.speaknow *100
                    ]
                },{
                    label: 'Difference',
                    backgroundColor: color3,
                    stack:'Stack 0',
                    data: [
                        datosRadar4[0], datosRadar4[1], datosRadar4[2] , datosRadar4[3]
                    ]
                },
                {
                    label: 'hide',
                    backgroundColor: '#d4b02f',
                    stack: 'Stack 1',
                    
                    data: [
                        datosRadar3[0], datosRadar3[1], datosRadar3[2] , datosRadar3[3]
                    ]
                }]

            };
            //var estilos = $(".chartprogress-activity").find('canvas').attr('style');
            $(".chartprogress-activity").html(" ").html('<canvas id="progressActivity" style="display: block; margin:0 auto; height: 313px;"></canvas>');
            $("#chartresumeactivity").html(" ").html('<canvas id="progressActivityResume" style="display: block; margin:0 auto; width:100%; height: 313px;"></canvas>');
            drawChart2("progressActivity",barChartData);
            drawChart2("progressActivityResume",barChartData);
            
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        console.log("%c ERROR","font-size:40px;");
        return false;
    });
    /**Cargar los progresos2... */
    $.ajax({
        url: _sysUrlBase_+'/docente/progresoxexamenes',
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': $('#idcurso').val(), 'actual': $('#timeActual').val(), 'anterior': $('#timeUltimo').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var drawhab = new Object;
            var color1 = '#36a2eb';
            var color2 = '#ff6384';
            var color3 = '#d4b02f';
            var calcular = function(v){
                return (v == 0) ? v : v/100;
            };
            
            var option = {colorFg:"#36a2eb",thick:15,fixedTextSize:0.3,colorText:"#000000" }
            if(Object.keys(resp.data.tiemposanterior).length > 0){
                //hacer algo
                drawhab.listen = resp.data.tiemposanterior[4];
                drawhab.read = resp.data.tiemposanterior[5];
                drawhab.write = resp.data.tiemposanterior[6];
                drawhab.speak = resp.data.tiemposanterior[7];
                drawHabilidad(document.getElementById("barExamsListening"),option,calcular(drawhab.listen) ,0.15);
                drawHabilidad(document.getElementById("barExamsReading"),option,calcular(drawhab.read) ,0.15);
                drawHabilidad(document.getElementById("barExamsWriting"),option,calcular(drawhab.write) ,0.15);
                drawHabilidad(document.getElementById("barExamsSpeaking"),option,calcular(drawhab.speak) ,0.15);
            }else{
                drawhab.listen = 0;
                drawhab.read = 0;
                drawhab.write = 0;
                drawhab.speak = 0;
            }
            if(Object.keys(resp.data.tiemposactual).length > 0){
                //hacer algo
                drawhab.listennow = resp.data.tiemposactual[4];
                drawhab.readnow = resp.data.tiemposactual[5];
                drawhab.writenow = resp.data.tiemposactual[6];
                drawhab.speaknow = resp.data.tiemposactual[7];

                drawHabilidad(document.getElementById("barExamsListening2"),option, calcular(drawhab.listennow),0.15);
                drawHabilidad(document.getElementById("barExamsReading2"),option, calcular(drawhab.readnow),0.15);
                drawHabilidad(document.getElementById("barExamsWriting2"),option, calcular(drawhab.writenow),0.15);
                drawHabilidad(document.getElementById("barExamsSpeaking2"),option, calcular(drawhab.speaknow),0.15);
            }else{
                drawhab.listennow = 0;
                drawhab.readnow = 0;
                drawhab.writenow = 0;
                drawhab.speaknow = 0;
            }
            var datosRadar = new Array();
            datosRadar[0] = drawhab.listen;
            datosRadar[1] = drawhab.read;
            datosRadar[2] = drawhab.write;
            datosRadar[3] = drawhab.speak;
            var datosRadar2 = new Array();
            datosRadar2[0] = drawhab.listennow;
            datosRadar2[1] = drawhab.readnow;
            datosRadar2[2] = drawhab.writenow;
            datosRadar2[3] = drawhab.speaknow;
            var datosRadar3 = new Array();
            datosRadar3[0] = 0;
            datosRadar3[1] = 0;
            datosRadar3[2] = 0;
            datosRadar3[3] = 0;
            var datosRadar4 = new Array();
            datosRadar4[0] = 0;
            datosRadar4[1] = 0;
            datosRadar4[2] = 0;
            datosRadar4[3] = 0;

            for(var i = 0; i < 4; i++){
                if(datosRadar[i] > datosRadar2[i]){
                datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
                }else if(datosRadar[i] < datosRadar2[i]){
                datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
                }
            }

            var barChartData = {
                labels: ["Listening", "Reading", "Writing", "Speaking"],
                datasets: [{
                    label: 'Previous',
                    backgroundColor: color2,
                    stack:'Stack 0',

                    data: [
                        drawhab.listen,drawhab.read ,drawhab.write ,drawhab.speak
                    ]
                },{
                    label: 'Current',
                    backgroundColor: color1,
                    stack:'Stack 1',
                    data: [
                        drawhab.listennow,drawhab.readnow ,drawhab.writenow ,drawhab.speaknow
                    ]
                },{
                    label: 'Difference',
                    backgroundColor: color3,
                    stack:'Stack 0',
                    data: [
                        datosRadar4[0], datosRadar4[1], datosRadar4[2] , datosRadar4[3]
                    ]
                },
                {
                    label: 'hide',
                    backgroundColor: '#d4b02f',
                    stack: 'Stack 1',
                    
                    data: [
                        datosRadar3[0], datosRadar3[1], datosRadar3[2] , datosRadar3[3]
                    ]
                }]

            };
            //var estilos = $(".chartprogress-activity").find('canvas').attr('style');
            $("#chartprogress-exams").html(" ").html('<canvas id="progressExams" style="display: block; margin:0 auto; height: 313px;"></canvas>');
            $("#chartresumeexams").html(" ").html('<canvas id="progressExamsResume" style="display: block; margin:0 auto; width:100%; height: 313px;"></canvas>');
            drawChart2("progressExams",barChartData);
            drawChart2("progressExamsResume",barChartData);
            
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        console.log("%c ERROR","font-size:40px;");
        return false;
    });
    //chartprogress-exams
}

$('document').ready(function(){

    /**progress activity */
    var barChartData = {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [ {
            label: 'Previous',
            
            data: [
                28,65,20,35
            ]
        },
        {
            label: 'Current',
            
            data: [
                20,52,10,30
            ]
        },
            {
                label: 'Difference',
                data: [
                    28,65,20,35
                ]
            }
        ]

    };

    

    drawChart("progressActivityResume",barChartData);
    drawChart("progressExamsResume",barChartData);

    /**progress activity */

    drawChart("progressActivity",barChartData);
    var option = {colorFg:"#36a2eb",thick:15,fixedTextSize:0.3,colorText:"#000000" }
    drawHabilidad(document.getElementById("barListening"),option, 0,0.15);
    drawHabilidad(document.getElementById("barReading"),option, 0,0.15);
    drawHabilidad(document.getElementById("barWriting"),option, 0,0.15);
    drawHabilidad(document.getElementById("barSpeaking"),option, 0,0.15);
    drawHabilidad(document.getElementById("barListening2"),option, 0,0.15);
    drawHabilidad(document.getElementById("barReading2"),option, 0,0.15);
    drawHabilidad(document.getElementById("barWriting2"),option, 0,0.15);
    drawHabilidad(document.getElementById("barSpeaking2"),option, 0,0.15);

     /** progreess exam*/

    //drawChart("progressExams",barChartData);
    var option = {colorFg:"#36a2eb",thick:15,fixedTextSize:0.3,colorText:"#000000" }
    drawHabilidad(document.getElementById("barExamsListening"),option, 0.25,0.15);
    drawHabilidad(document.getElementById("barExamsReading"),option, 0.15,0.15);
    drawHabilidad(document.getElementById("barExamsWriting"),option, 0.30,0.15);
    drawHabilidad(document.getElementById("barExamsSpeaking"),option, 0.45,0.15);
    drawHabilidad(document.getElementById("barExamsListening2"),option, 0.25,0.15);
    drawHabilidad(document.getElementById("barExamsReading2"),option, 0.15,0.15);
    drawHabilidad(document.getElementById("barExamsWriting2"),option, 0.30,0.15);
    drawHabilidad(document.getElementById("barExamsSpeaking2"),option, 0.45,0.15);

    main();

    $('#contenedor-alumno').slick({});

    /** Resumen */

});
</script>