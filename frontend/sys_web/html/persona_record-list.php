<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Persona_record"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                                
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbtiporecord" id="cbtiporecord" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                                                                              <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fecharegistro"))?> </span>
                            <input type='text' class="form-control border0" name="datefecharegistro" id="datefecharegistro" />           
                          </div>
                      </div>
                    </div>
                                                        
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbmostrar" id="cbmostrar" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Persona_record", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Persona_record").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idpersona") ;?></th>
                    <th><?php echo JrTexto::_("Tiporecord") ;?></th>
                    <th><?php echo JrTexto::_("Titulo") ;?></th>
                    <th><?php echo JrTexto::_("Descripcion") ;?></th>
                    <th><?php echo JrTexto::_("Archivo") ;?></th>
                    <th><?php echo JrTexto::_("Fecharegistro") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a73ac26d2659='';
function refreshdatos5a73ac26d2659(){
    tabledatos5a73ac26d2659.ajax.reload();
}
$(document).ready(function(){  
  var estados5a73ac26d2659={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a73ac26d2659='<?php echo ucfirst(JrTexto::_("persona_record"))." - ".JrTexto::_("edit"); ?>';
  var draw5a73ac26d2659=0;

  
  $('#cbtiporecord').change(function(ev){
    refreshdatos5a73ac26d2659();
  });
  $('#datefecharegistro').change(function(ev){
    refreshdatos5a73ac26d2659();
  });
  $('#cbmostrar').change(function(ev){
    refreshdatos5a73ac26d2659();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5a73ac26d2659();
  });
  tabledatos5a73ac26d2659=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idpersona") ;?>'},
            {'data': '<?php echo JrTexto::_("Tiporecord") ;?>'},
            {'data': '<?php echo JrTexto::_("Titulo") ;?>'},
            {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
            {'data': '<?php echo JrTexto::_("Archivo") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecharegistro") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/persona_record/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.tiporecord=$('#cbtiporecord').val(),
             d.fecharegistro=$('#datefecharegistro').val(),
             d.mostrar=$('#cbmostrar').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a73ac26d2659=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a73ac26d2659;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idpersona") ;?>': data[i].idpersona,
              '<?php echo JrTexto::_("Tiporecord") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="tiporecord"  data-id="'+data[i].idrecord+'"> <i class="fa fa'+(data[i].tiporecord=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a73ac26d2659[data[i].tiporecord]+'</a>',
              '<?php echo JrTexto::_("Titulo") ;?>': data[i].titulo,
                '<?php echo JrTexto::_("Descripcion") ;?>': data[i].descripcion,
                '<?php echo JrTexto::_("Archivo") ;?>': '<a href="'+data[i].archivo+' target="_blank"> <?php echo JrTexto::_("ver documento"); ?></a>',
                '<?php echo JrTexto::_("Fecharegistro") ;?>': data[i].fecharegistro,
              '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idrecord+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5a73ac26d2659[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/persona_record/editar/?id='+data[i].idrecord+'" data-titulo="'+tituloedit5a73ac26d2659+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idrecord+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'persona_record', 'setCampo', id,campo,data);
          if(res) tabledatos5a73ac26d2659.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a73ac26d2659';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Persona_record';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'persona_record', 'eliminar', id);
        if(res) tabledatos5a73ac26d2659.ajax.reload();
      }
    }); 
  });
});
</script>