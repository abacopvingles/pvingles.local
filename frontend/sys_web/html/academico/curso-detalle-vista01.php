<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->misdatos)) $datos=$this->misdatos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$tipo_=array('N'=>JrTexto::_('Level'),'U'=>JrTexto::_('Unit'),'L'=>ucfirst(JrTexto::_('Activity')),'E'=>JrTexto::_('Assessments'),'M'=>JrTexto::_('Menu'));

function addnivel($data,$css,$icn=0){
	//var_dump($data);
	$tipo_=array('N'=>JrTexto::_('Level'),'U'=>JrTexto::_('Unit'),'L'=>ucfirst(JrTexto::_('Activity')),'E'=>JrTexto::_('Assessments'),'M'=>JrTexto::_('Menu'));
	$tipo=$data["tiporecurso"];
	$tipotxt=$tipo_[$tipo];	
	$html='';
	$html='<tr class="'.$css.'" style="'.($tipo=='L'?'display:none':'').'" data-padre="'.$data["idpadre"].'" data-id="'.$data["idcursodetalle"].'" ';
	$html.='data-tipo="'.$data["tiporecurso"].'" data-idrecurso="'.$data["idrecurso"].'" data-idlogro="'.$data["idlogro"].'" data-orden="'.$data["orden"].'" ';
	$nhoras='<input type="time" value="'.(!empty($data["nhoras"])?$data["nhoras"]:'00:45').'" class="nhoras" style="max-width:75px" >';

	if($tipo=='U'||$tipo=='L'){
		$nses=!empty($data['hijo'])?count($data['hijo']):($tipo=='U'?0:1);		
		if($tipo=='U') {
			$nhoras=@$data["nhoras"];
			$nses=@$data["nsesiones"];
		}
		$html.='title="'.$tipotxt.'" ><td class="text-center numbertr">'.$icn.'</td><td class="addcontrols class_0'.$tipo.'">'.($tipotxt.' : '.$data["nombre"]).'</td><td class="text-center">'.$nses.'</td><td class="text-center infonhoras">'.$nhoras.'</td></tr>';
	}elseif($tipo=='E'){

		$html.='title="'.$tipotxt.'" ><td class="text-center numbertr">'.$icn.'</td><td class="addcontrols class_0'.$tipo.'">'.($tipotxt.' : '.$data["nombre"]).'</td><td class="text-center">-</td><td class="text-center">'.$nhoras.'</td></tr>';
	}elseif($tipo=='N'||$tipo=='M'){
		$html.='title="'.$tipotxt.'" ><td class="addcontrols class_0'.$tipo.'" colspan="4">'.($tipotxt.' : '.$data["nombre"]).'</td></tr>';
	}
	echo $html;
}
?>
<style type="text/css">
	tr.level_0 td:first-child{
		background-color: #337ab7 !important;
		font-size: 1.7em;
		color: #FFF;
	}
	tr.level_1 td:first-child, tr.level_1 td:nth-child(2){
		background-color: #337ab7 !important;
		font-size: 1.3em;
		padding-left: 1em;		
		color: #FFF;
	}
	tr.level_2{	display: none;}	
	.tdnombre2{
		    background-color: rgb(52, 119, 177);
		    font-size: 0.9em;
		    color: #FFF;
		    padding-left: 1.9em !important;

	}
	.tdcolor1{
		background-color: #337ab7;
		font-size: 1em;
		color: #000;
	}
	.tdcolor2{
		background-color: #337ab7;
		font-size: 1em;
		color: #000;
	}
	.accionesnivel{
		float: right;
	}
	.accionesnivel .fa-trash{
		color:red;
	}
	.accionesnivel .fa-pencil{
		color:#f3c894;
	}
	.titulomaterial{
		font-weight: bold;
		margin: 1em;
		position: relative;
	}
	.titulomaterial span{
		position: absolute;
		right: 1em;
	}
	.titulomaterial ul{
		list-style: none;
	}
	.titulomaterial ul li{
		padding: 1ex;
		position: relative;
	}
	.titulomaterial ul li span{
		position: absolute;
	    right: 1.5ex;
	    top: 1.5ex;
	}
	.paraclone{	display: none;}
	tr:not([data-idlogro="0"]) .btnpremio{color:#1ce824;}
	tr[data-idlogro="0"] .btnpremio, tr:not([data-idlogro]) .btnpremio{color:#fff;}
	tr.level_2 td:first-child, tr.level_2 td:nth-child(2){
		background-color: #62aeef !important;
		font-size: 1em;
		color: #FFF;
		padding-left: 2.5em;
	}
	tr.level_2[data-tipo="E"] td:first-child,tr.level_2[data-tipo="E"] td:nth-child(2){color: #b4f1b4 !important;}
</style>
    <div class="col-xs-12 col-sm-12 col-md-12"  id="ventana-<?php echo $idgui; ?>">
      	<div class="col-md-12">             
        	<div class="form-group">
           <label>II. Organización de Temas:</label>                   
        	</div>
        	<div style="position: absolute; right: 15px; top:-10px">
				<div class="btn-group">
					<a href="#" class="btn btn-primary btnimport btnnopadre" title="<?php echo ucfirst(JrTexto::_('Import content')); ?>"><i class="fa fa-download" ></i> <?php echo ucfirst(JrTexto::_('Import content')); ?></a>
					<a href="#" class="btn btn-warning btnadd btnnopadre" title="<?php echo ucfirst(JrTexto::_('Add Level')); ?>">
						<i class="fa fa-plus" ><i class="fa fa-plus"></i></i> <?php echo ucfirst(JrTexto::_('Add Level')); ?></a>
					<a href="#" class="btn btn-danger btnexamen btnnopadre" title="<?php echo ucfirst(JrTexto::_('Add assessment')); ?>"><i class="fa fa-list" ><i class="fa fa-plus"></i></i> <?php echo ucfirst(JrTexto::_('Add Assesment')); ?></a>
					<a href="#" style="display: none;" class="btn btn-success btnrefresh btnnopadre"><i class="fa fa-refresh"></i></i> <?php echo ucfirst(JrTexto::_('Refresh')); ?></a>
				</div>
			</div>
      	</div>
      	<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="panel">
		<table id="datos<?php echo $idgui;?>" class="table table-responsive table-striped">			
		<tbody>				
		<?php if(!empty($datos)){			
			$grupo_='T';			
			$ini=false;	
			$idpadre=0;
			$hijolevel=0;
			$icn=0;
			//var_dump($datos);
			foreach ($datos as $dato){
				$icn++;
				
				$tipo=$dato["tiporecurso"];	
				$html='<tr class="level_0 btnnopadre" data-tipo="'.$tipo.'" data-padre class="nohide">						
						<td rowspan="2" colspan="2" class="text-center addcontrols"><br>Título de La unidad</td>
						<td colspan="2" class="text-center">Duración</td>
					</tr>
					<tr data-padre >
						<td class="text-center">Sesiones</td>
						<td class="text-center">Horas<br>(HH:mm)</td>
					</tr>
				';
				if($ini==false){ $ini=true;
					if($tipo=='N'){	
						addnivel($dato,'level_0',$icn); 
						$html=str_replace('btnnopadre', 'btnsipadre', $html);
						echo str_replace('data-padre', 'data-padre="'.$dato['idcursodetalle'].'"', $html);
						$hijolevel=1;
					}else{
						echo str_replace('data-padre', 'data-padre="-1"', $html);
						addnivel($dato,'level_1',$icn);
						$hijolevel=2;
					}
				}else{					
					if($tipo=='N'){
						echo '<tr data-padre="'.$dato['idcursodetalle'].'"><td colspan="3"> </td></tr>';
						addnivel($dato,'level_0',$icn);
						$html=str_replace('btnnopadre', 'btnsipadre', $html);
						echo str_replace('data-padre', 'data-padre="'.$dato['idcursodetalle'].'"', $html);
						$hijolevel=1;
					}else{
						/*echo '<tr data-padre="-1"><td colspan="3"> </td></tr>';
						echo str_replace('data-padre', 'data-padre="-1"', $html);*/
						$nhoras='00:45';
						$hh1=0;
						$mm1=0;
						$nses=0;
						if($dato["tiporecurso"]=='U'){
							foreach ($dato["hijo"] as $h){								
								$h2=explode(":", $h["nhoras"]);
								$hh2=intval(!empty($h2[0])?$h2[0]:'00');
								$mm2=intval(!empty($h2[1])?$h2[1]:'45');
								$hh1=$hh1+$hh2;
								$mm1=$mm1+$mm2;
								if($h["tiporecurso"]=='L')$nses++;
							}
							if($mm1>=60){
								$hh1=$hh1+floor($mm1/60);
								$mm1=$mm1%60;
							}
							$dato["nsesiones"]=$nses;
							$dato["nhoras"]=str_pad($hh1, 2, "0", STR_PAD_LEFT).":".str_pad($mm1, 2, "0", STR_PAD_LEFT);
						}
						addnivel($dato,'level_1',$icn);
						$hijolevel=2;
					}
				}
				if(!empty($dato["hijo"])){ 
					$j=0;					
					foreach ($dato["hijo"] as $hijo){ $j++;
						addnivel($hijo,'level_'.($hijolevel),$j);
						if(!empty($hijo["hijo"])){ $z=0;
							foreach ($hijo["hijo"] as $hl){ $z++;
								addnivel($hl,'level_'.($hijolevel+1),$z);									
							}
						}
					}
				}				
			}
		}		
		?>
		    </tbody>    
		</table>​
			<div id="controls<?php echo $idgui; ?>" style="display: none;">
				<div class="accionesnivel">					
					<!--i class="btn btn-xs btnmove fa fa-arrow-left" title="<?php //echo ucfirst(JrTexto::_('Move')); ?>"></i>
					<i class="btn btn-xs btnmove fa fa-arrow-down" title="<?php //echo ucfirst(JrTexto::_('Move')); ?>"></i-->
					<i class="btn btn-xs btnimport fa fa-download" title="<?php echo ucfirst(JrTexto::_('Import')); ?>"></i>
					<i class="btn btn-xs btnadd fa fa-plus " title="<?php echo ucfirst(JrTexto::_('Add')); ?>"><i class="fa fa-plus"></i></i>
					<i class="btn btn-xs btnexamen fa fa-list " title="<?php echo ucfirst(JrTexto::_('Assesment')); ?>"><i class="fa fa-plus"></i></i>
					<i class="btn btn-xs btnpremio fa fa-trophy" title="<?php echo JrTexto::_('Assign certificate or prize');  ?>"></i>
					<i class="btn btn-xs btnvermas fa fa-chevron-up" title="<?php echo JrTexto::_('Show');  ?>"></i>
					<i class="btn btn-xs btneditar fa fa-pencil" title="<?php echo JrTexto::_('Edit');  ?>"></i>
					<i class="btn btn-xs btneliminar fa fa-trash" title="<?php echo JrTexto::_('Remove');  ?>"></i>	
					<i style="color:#f7970f;" data-move="top" class="btn btn-xs btnmove fa fa-arrow-up" title="<?php echo ucfirst(JrTexto::_('Move top')); ?>"></i>
					<i style="color:#f7970f;" data-move="buttom" class="btn btn-xs btnmove fa fa-arrow-down" title="<?php echo ucfirst(JrTexto::_('Move buttom')); ?>"></i>				
				</div>
			</div>		
		</div>
		</div>
	</div>	
	<div class="col-xs-12 col-sm-12 col-md-12"> 
      <div class="col-md-12">             
        <div class="form-group">       
          <label>III. Vínculo con otros Aprendizajes :</label>                             
          <textarea class="form-control" name="vinculosaprendizajes" id="vinculosaprendizajes<?php echo $idgui ?>" placeholder="Aqui debe agregar la descripcion del curso" rows="6"><?php echo @$this->infocurso['vinculosaprendizajes'];?></textarea> 
        </div>
      </div>              
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12"> 
      <div class="col-xs-12 col-sm-12 col-md-8">             
        <div class="form-group" >       
          <label>IV. Materiales y Recurso :</label>  
         	<?php 
         	if(!empty($this->infocurso['materialesyrecursos'])){
         	$mat=json_decode($this->infocurso['materialesyrecursos'])[0]; 
         	$mdoc=$mat->docente;
         	$mes=$mat->estudiante;
         	}
         	?>                           
          <div id="materiales<?php echo $idgui; ?>">
          	<div class="titulomaterial" id="matdoc">Para el docente: <span class="btn btn-xs btn-success btnaddmat"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add') ?></span>
          		<ul>
          			<li class="paraclone"><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>"></li>
          			<?php if(!empty($mdoc))
          				foreach ($mdoc as $v){?>
          					<li><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>" value="<?php echo $v; ?>"></li>
          			<?php } ?>
          			<li><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>"></li>
          		</ul>
          	</div>
          	<div class="titulomaterial" id="matest">Para el Estudiante: <span class="btn btn-xs btn-success btnaddmat"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add') ?></span>
          		<ul>
          			<li class="paraclone"><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>"></li>
          			<?php if(!empty($mes))
          				foreach ($mes as $v){?>
          					<li><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>" value="<?php echo $v; ?>"></li>
          			<?php } ?>
          			<li><span class="btn btn-xs btn-danger btnremovemat"><i class="fa fa-trash"></i></span> <input  class="form-control" placeholder="<?php echo JrTexto::_('Write Material'); ?>"></li>
          		</ul>
          	</div>
          </div>
        </div>  
      </div>
    </div>
<script type="text/javascript">
var txttipo_=<?php echo json_encode($tipo_); ?>;
var cargardetalle<?php echo $idgui ?>=function(){
};

var addjsondetalle<?php echo $idgui; ?>=function(json,idpadre){
	  var idpadre=idpadre||0;
	  var formData = new FormData();
	  formData.append('pkIdcurso', $('#idcurso').val());
	  formData.append('idpadre', idpadre);
	  formData.append('jsondetalle', JSON.stringify(json));  
	  var url=_sysUrlBase_+'/acad_curso/guardarcursodetalle';
	  $.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {       
	      if(data.code==='ok'){
	      	refreshvistapadre();	      	
	      }else{
	        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
	      }
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	  });  
}
var addcampodetalle<?php echo $idgui; ?>=function($id,campo,valor){
	var formData = new FormData();
	  formData.append('idcursodetalle', $id);
	  formData.append('campo', campo);
	  formData.append('valor', valor);
	  var url=_sysUrlBase_+'/acad_cursodetalle/addcampo';
	  $.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {   
	    	if(data.code=='Error')
   	 			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	  });
}
var updatematdocente<?php echo $idgui; ?>=function(){
		var mat=$('#materiales<?php echo $idgui; ?>');
		var matdoc=mat.find('#matdoc').find('input');
		var docente=[]
		var estudiante=[];
		$.each(matdoc,function(i,v){
			var txt=$(v).val();
			if(txt!='')docente.push(txt);
		});
		var matest=mat.find('#matest').find('input');
		$.each(matest,function(i,v){
			var txt=$(v).val();
			if(txt!='')estudiante.push(txt);
		});
		var rejson=[{docente,estudiante}];
		txtjson=JSON.stringify(rejson);
		addvalorcampo<?php echo $idgui; ?>('materialesyrecursos',txtjson);
}
var addvalorcampo<?php echo $idgui; ?>=function(campo,valor){
		var formData = new FormData();
		  formData.append('idcurso', <?php echo $this->idcurso;?>);
		  formData.append('campo', campo);
		  formData.append('valor', valor);  
		  var url=_sysUrlBase_+'/acad_curso/addcampo';
		  $.ajax({
		    url: url,
		    type: "POST",
		    data:  formData,
		    contentType: false,
		    processData: false,
		    dataType:'json',
		    cache: false,
		    beforeSend: function(XMLHttpRequest){ },      
		    success: function(data)
		    {   
		    	if(data.code=='Error')
       	 			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
		    },
		    error: function(e){ },
		    complete: function(xhr){ }
		  });
}

var addcursodetalle<?php echo $idgui; ?>=function($pnl,datos){
	var url=_sysUrlBase_+'/acad_cursodetalle/guardarcursodetalle';
	var formData = new FormData();
	  formData.append('idcursodetalle', datos.idcursodetalle||'');
	  formData.append('orden', datos.orden||0);
	  formData.append('idrecurso', datos.idrecurso);
	  formData.append('tiporecurso', datos.tiporecurso);
	  formData.append('idlogro', datos.idlogro||0);
	  formData.append('url', datos.url||'');
	  formData.append('idpadre', datos.idpadre);
	  formData.append('idcurso', datos.idcurso);
	  formData.append('txtjson', datos.txtjson||'{}');
	  formData.append('color', datos.color||'rgba(0,0,0,0)');
	  formData.append('esfinal', datos.esfinal||'0');
	$.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {  
	    	if(data.code=='Error'){
   	 			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');   	 			
	    	}else{
	    		console.log($pnl);
   	 			refreshvistapadre();
   	 		}
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	});
}

$(document).ready(function(){
	//$( "#datos<?php //echo $idgui; ?> tbody" ).sortable();
	$('#datos<?php echo $idgui; ?>').on('blur','input.nhoras',function(ev){
		$tr=$(this).closest('tr');
		var id=$tr.attr('data-id')||-1;
		var dataPadre=$tr.attr('data-padre');
		id=id=='0'?-1:id;	
		var formData = new FormData();
		formData.append('idcursodetalle', id);
		formData.append('valor', $(this).val()||'00:00');	
		formData.append('campo', 'nhoras');			
		var url=_sysUrlBase_+'/acad_cursodetalle/addcampo';
		$.ajax({
		    url: url,
		    type: "POST",
		    data:  formData,
		    contentType: false,
		    processData: false,
		    dataType:'json',
		    cache: false,
		    beforeSend: function(XMLHttpRequest){ },      
		    success: function(data){		    	
		       if(data.code=='Error'){
		       		mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
		       }else{
		       	var nhoras='00:45';
		       	var h1=0;
		       	var m1=0;
		       	$('tr[data-padre="'+dataPadre+'"]').each(function(i,v){
					var hh2=($(v).find('input.nhoras').val()||'00:45').split(':');
					var h2 = parseInt(hh2[0]||'00');
					var m2 = parseInt(hh2[1]||'45');
					h1=h1+h2;
					m1=m1+m2;
		       	})
		       	if(m1>=60){
		       		h1=h1+Math.floor(m1/60);
		       		m1=(m1%60)
		       	}
		       	h1=h1<10?('0'+h1):h1;
		       	m1=m1<10?('0'+m1):m1;
		       	$('tr[data-id="'+dataPadre+'"]').children('.infonhoras').text(h1+':'+m1);

		       }
		    },
		    error: function(e){ },
		    complete: function(xhr){ }
		 });
	}).on('click','.btnmove',function(ev){
		$tr=$(this).closest('tr');
		var id=$tr.attr('data-id')||-1;
		var dataPadre=$tr.attr('data-padre');
		id=id=='0'?-1:id;
		var move=$(this).attr('data-move');
		$tr.addClass('moviendo');

		var trs=$('tr[data-padre="'+dataPadre+'"]');
		var ntr=trs.length-1;
		var trmove='';
		var updateids={};
		trs.each(function(i,v){
			var row = $(v);			
			if(row.hasClass('moviendo')){
				var idrow=row.attr('data-id');
				var hijosrow=$('tr[data-padre="'+idrow+'"]').clone();				
				if(move=='top'){
				    if(trmove.prev().length>0){				    	
				    	var idtrmove=trmove.attr('data-id');
				    	$('tr[data-padre="'+idrow+'"]').remove();
						var hijostrmove=$('tr[data-padre="'+idtrmove+'"]').clone();		
						$('tr[data-padre="'+idtrmove+'"]').remove();

						trmove.insertAfter(row);
						trmove.children('.numbertr').text(i+1);
						row.children('.numbertr').text(i);						
						if(hijostrmove.length)hijostrmove.insertAfter(trmove);
						if(hijosrow.length)hijosrow.insertAfter(row);
						updateids[idrow]=i;
						updateids[idtrmove]=i+1;
					}
				}else{
					$('tr[data-padre="'+idrow+'"]').remove();
					rownext=row.next();
					var idrow=row.attr('data-id');
			    	var idtrmove=rownext.attr('data-id');
					var hijostrmove=$('tr[data-padre="'+idtrmove+'"]').clone();
					$('tr[data-padre="'+idtrmove+'"]').remove();
					row.insertAfter(rownext);
					rownext.children('.numbertr').text(i+1);
					row.children('.numbertr').text(i+2);
					if(hijosrow.length)hijosrow.insertAfter(row);
					if(hijostrmove.length)hijostrmove.insertAfter(rownext);
					updateids[idrow]=i+2;
					updateids[idtrmove]=i+1;
					
				}
				row.removeClass('moviendo');
			}
			trmove=row;
		})
		
		$.each(updateids,function(i,v){
			console.log(i,v);
			var formData = new FormData();
			formData.append('idcursodetalle', i);
			formData.append('valor', v);	
			formData.append('campo', 'orden');			
			var url=_sysUrlBase_+'/acad_cursodetalle/addcampo';
			$.ajax({
			    url: url,
			    type: "POST",
			    data:  formData,
			    contentType: false,
			    processData: false,
			    dataType:'json',
			    cache: false,
			    beforeSend: function(XMLHttpRequest){ },      
			    success: function(data){		    	
			       if(data.code=='Error'){
			       		mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
			       }
			    },
			    error: function(e){ },
			    complete: function(xhr){ }
			});
		});


	}).on('click','.btneliminar',function(ev){
		$tr=$(this).closest('tr');
		var id=$tr.attr('data-id')||$tr.attr('data-padre');
		id=id=='0'?-1:id;
		var idpadre=id==-1?0:id;		
		var formData = new FormData();
		formData.append('idcursodetalle', id);			
		var url=_sysUrlBase_+'/acad_cursodetalle/eliminardet';
		$.ajax({
		    url: url,
		    type: "POST",
		    data:  formData,
		    contentType: false,
		    processData: false,
		    dataType:'json',
		    cache: false,
		    beforeSend: function(XMLHttpRequest){ },      
		    success: function(data){  			       
		       if(data.code=='Error'){
		       		mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
		       }else{
		       	$tr.remove();
		       	$('tr[data-padre="'+idpadre+'"]').remove();
		       	if(refreshdatoscurso!=undefined){
		       		refreshdatoscurso();
		       	}
		       }
		    },
		    error: function(e){ },
		    complete: function(xhr){ }
		 });			
	}).on('click','.btneditar',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		$('#datos<?php echo $idgui; ?>').find('.btneditar<?php echo $idgui; ?>').removeClass('btneditar<?php echo $idgui; ?>');

		$(this).off('updatenivel');
		$(this).addClass('btneditar<?php echo $idgui; ?>');
		var pnl=$(this).closest('tr');
		var tipo=pnl.attr('data-tipo');
		var id=pnl.attr('data-id');
		var idrecurso=pnl.attr('data-idrecurso');
		var idcurso=$('#pkIdcurso').val();
		var url=_sysUrlBase_+'/acad_cursodetalle/nivelesedit/?id='+id+'&idcurso='+idcurso+'&idrecurso='+idrecurso+'&tipo='+tipo+'&plt=modal&fcall=btneditar<?php echo $idgui; ?>&vista=vista02&acc=editar';
		var titulo=$(this).attr('title')+' '+pnl.attr('title');
		var obj={
			url:url,
			titulo:titulo,
			borrar:true,
			cerrarconesc:true,
			backdrop:false
		};
		sysmodal(obj);
		$(this).on('updatenivel',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('updatenivel');
			$(this).removeClass('btneditar<?php echo $idgui; ?>');

		});
	}).on('click','.btnvermas',function(ev){
		$tr=$(this).closest('tr');
		id=$tr.attr('data-id')||$tr.attr('data-padre');			
		if($(this).hasClass('fa-chevron-down')){				
			$(this).removeClass('fa-chevron-down').addClass('fa-chevron-up');				
			$('tr[data-padre="'+id+'"]').hide('fast');
			$('tr.nohide').show('fast');
		}else{
			$(this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
			$('tr[data-padre="'+id+'"]').show('fast');				
		}
	}).on('click','.btnpremio',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		$('#datos<?php echo $idgui; ?>').find('.btnpremio<?php echo $idgui; ?>').removeClass('btnpremio<?php echo $idgui; ?>');
		$(this).off('addpremio');
		$(this).addClass('btnpremio<?php echo $idgui; ?>');
		var pnl=$(this).closest('tr');
		var tipo=pnl.attr('data-tipo');
		var id=pnl.attr('data-id');
		var idlogro=pnl.attr('data-idlogro')||0;
		var url=_sysUrlBase_+'/logro/listado/?plt=modal&idlogro='+idlogro+'&fcall=btnpremio<?php echo $idgui; ?>';
		var titulo=$(this).attr('title')+' '+pnl.attr('title')
		var obj={
			url:url,
			titulo:titulo,
			borrar:true,
			cerrarconesc:true,
			backdrop:false,
			showfooter:true,
		};
		sysmodal(obj);
		$(this).on('addpremio',function(ev){
			ev.preventDefault();
			ev.stopPropagation();				
			$(this).off('addpremio');
			$(this).removeClass('btnpremio<?php echo $idgui; ?>');
			var idlogro=$(this).attr('data-idlogro');
			pnl.attr('data-idlogro',idlogro);
			addcampodetalle<?php echo $idgui; ?>(id,'idlogro',idlogro);
		});
	});

	$('#materiales<?php echo $idgui; ?>').on('click','.btnaddmat',function(ev){
		var ul=$(this).closest('.titulomaterial').find('ul');
		li=ul.find('li.paraclone').clone();
		li.removeAttr('class');
		ul.append(li);
	}).on('click','.btnremovemat',function(ev){
		$(this).closest('li').remove();
		updatematdocente<?php echo $idgui; ?>();
	}).on('focusout','input',function(ev){
		updatematdocente<?php echo $idgui; ?>();
	});

	$('#ventana-<?php echo $idgui; ?>')
	.on('click','.btnimport',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var id=0;
		var tipo='All';
		$('#datos<?php echo $idgui; ?>').find('.btnimport<?php echo $idgui; ?>').removeClass('btnimport<?php echo $idgui; ?>');
		var pnl=$('table#datos<?php echo $idgui;?>');		
		if(!$(this).hasClass('btnnopadre')){ 
			pnl=$(this).closest('tr');
			id=pnl.attr('data-id');
			tipo=pnl.attr('data-tipo');
		}
		$(this).off('addimport');
		$(this).addClass('btnimport<?php echo $idgui; ?>');
		tipo_=tipo=='All'?'N':(tipo=='N'?'U':'L');
		var url=_sysUrlBase_+'/acad_curso/importar/?plt=modal&fcall=btnimport<?php echo $idgui; ?>&tipo='+tipo_;
		var titulo=$(this).attr('title');
		var obj={
			url:url,
			titulo:titulo,
			borrar:true,
			cerrarconesc:true,
			backdrop:false
		};
		sysmodal(obj);
		$(this).on('addimport',function(ev){
			ev.preventDefault();
			ev.stopPropagation();							
			$(this).off('addimport');
			$(this).removeClass('btnimport<?php echo $idgui; ?>');
			var strjson=$(this).attr('data-import');
			var json=JSON.parse(strjson);		
			$(this).removeAttr('data-import');
			addjsondetalle<?php echo $idgui; ?>(json,id);
		});
	}).on('click','.btnadd',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var id=0;
		var tipo='All';
		$('#datos<?php echo $idgui; ?>').find('.btnaddcontent<?php echo $idgui; ?>').removeClass('btnaddcontent<?php echo $idgui; ?>');
		var pnl=$('table#datos<?php echo $idgui;?>');
		var titulo=$(this).attr('title');
		if(!$(this).hasClass('btnnopadre')){ 
			pnl=$(this).closest('tr');
			id=pnl.attr('data-id');
			tipo=pnl.attr('data-tipo');
			titulo=$(this).attr('title');
		}
		$(this).off('addcontent');
		$(this).addClass('btnaddcontent<?php echo $idgui; ?>');
		tipo_=tipo=='All'?'N':(tipo=='N'?'U':'L');
		var idcurso=$('#pkIdcurso').val();
		var url=_sysUrlBase_+'/acad_cursodetalle/nivelesedit/?plt=modal&idcurso='+idcurso+'&fcall=btnaddcontent<?php echo $idgui; ?>&id='+id+'&vista=vista02&acc=nuevo&tipo='+tipo_;
		var obj={
			url:url,
			titulo:titulo,
			borrar:true,
			cerrarconesc:true,
			backdrop:false
		};
		sysmodal(obj);
		$(this).on('addcontent',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('addcontent');
			$(this).removeClass('btnaddcontent<?php echo $idgui; ?>');
			var strjson=$(this).attr('data-import');
			var json=JSON.parse(strjson);		
			$(this).removeAttr('data-import');
			addjsondetalle<?php echo $idgui; ?>(json,id);
		});
	}).on('click','.btnexamen',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var id=0;
		$('#datos<?php echo $idgui; ?>').find('.btnaddexamen<?php echo $idgui; ?>').removeClass('btnaddexamen<?php echo $idgui; ?>');
		var pnl=$('table#datos<?php echo $idgui;?>');
		var titulo=$(this).attr('title');
		if(!$(this).hasClass('btnnopadre')){ 
			pnl=$(this).closest('tr');
			id=pnl.attr('data-id');
			titulo=$(this).attr('title');
		}
		$(this).off('addassesment');
		$(this).addClass('btnaddexamen<?php echo $idgui; ?>');		
		var url=_sysUrlBase_+'/academico/examenes/?plt=modal&fcall=btnaddexamen<?php echo $idgui; ?>';
		var obj={
			url:url,
			titulo:titulo,
			borrar:true,
			cerrarconesc:true,
			backdrop:false
		};
		mimodal=sysmodal(obj);
		mimodal.find('.modal-dialog').css({'width':'90%'});
		$(this).on('addassesment',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('addassesment');
			$(this).removeClass('btnaddexamen<?php echo $idgui; ?>');
			var idexamen=$(this).attr('data-idexamen');
			var orden=$('tr[data-padre="'+id+'"]').length;	
			var datos={
				idcursodetalle:-1,
				idcurso:$('#idcurso').val(),
				orden:orden,
				idrecurso:idexamen,
				tiporecurso:'E',
				idlogro:0,
				idpadre:id,
				//txtjson:'{}'
			}
			addcursodetalle<?php echo $idgui; ?>(pnl,datos);
		});		
	});
	$('#vinculosaprendizajes<?php echo $idgui ?>').on('focusout',function(){
		var txt=$(this).val();
		addvalorcampo<?php echo $idgui; ?>('vinculosaprendizajes',txt);
	});

	var load<?php echo $idgui; ?>=function(){
		var addcontrols=$('#datos<?php echo $idgui; ?>').find('.addcontrols');
		var ic=0;
		$.each(addcontrols,function(i,v){
			var acciones1=$('#controls<?php echo $idgui; ?>').clone(true);
			var $tr=$(this).closest('tr');
			var tipo=$(this).closest('tr').attr('data-tipo');
			if(ic==0){
				acciones1.find('.fa').remove();
				ic++;
			}
			if($tr.hasClass('level_2')||$tr.attr('data-tipo')=="E"){
				acciones1.find('.btnvermas').remove();
				acciones1.find('.btnadd').remove();
				acciones1.find('.btnexamen').remove();
				acciones1.find('.btnimport').remove();
			}
			if($tr.hasClass('btnsipadre')){	
				acciones1.html('');
			}			
			if(tipo=='N') acciones1.find('i.fa-chevron-up').addClass('fa-chevron-up').addClass('fa-chevron-down');
			$(this).append(acciones1.html());
		});
	}
	load<?php echo $idgui; ?>();
});
</script>