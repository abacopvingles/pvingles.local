<style css>
  @media (max-width:768px){
    thead,tbody,.cthead,.ctbody {font-size: small!important;}
    .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th { white-space:initial!important; }
  }
</style>
<div class="container">
    <br> 
    <div class="panel panel-primary">
        <div class="panel-heading"><p>Question</p></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3><?php echo JrTexto::_('Course'); ?></h3>
                    <!-- <select id="curso">
                        <?php 
                        if(!empty($this->cursos)){
                            foreach($this->cursos as $v){
                                echo "<option value='{$v['idcurso']}'>{$v['nombre']}</option>";
                            }
                        }
                        ?>
                    </select>
                    <hr/> -->
                    <!-- <h4><?php echo JrTexto::_('Test'); ?></h4> -->
                 <!--    <select id="exam">
                        <?php
                            if(!empty($this->examen)){
                                foreach($this->examen as $v){
                                    echo "<option value='{$v['idexamen']}'>{$v['examen']}</option>";
                                }
                            }
                        ?>
                    </select> -->
                </div>
                <!-- <div class="col-md-12 col-sm-12">
                    <div class="col-sm-3">
                        <div class="card text-white bg-danger" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Listening</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_l"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['L'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-primary" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Reading</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_r"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['R'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-success" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Writing</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_w"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['W'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-warning" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Speaking</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_s"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['S'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table id="exam" class="table table-stripped">
                            <thead>
                                <th sort>id</th>
                                <th><?php echo JrTexto::_("Units"); ?></th>
                                <th>Total</th>
                                <th>Listen</th>
                                <th>Read</th>
                                <th>Write</th>
                                <th>Speak</th>
                            </thead>
                            <tbody>
                                <?php
                                // if(!empty($this->cursos)){
                                //     $i = 1;
                                //     foreach($this->cursos as $v){
                                //         echo "<tr>";
                                //         echo "<td id='total'>".$i."</td>";
                                //         echo "<td id='nombre'>{$v['nombre']}</td>";
                                //         echo "<td id='total'></td>";
                                //         echo "<td id='listen'></td>";
                                //         echo "<td id='read'></td>";
                                //         echo "<td id='write'></td>";
                                //         echo "<td id='speak'></td>";
                                //         echo "</tr>";
                                //         $i++;
                                //     }
                                // }
                                // if(!empty($this->examen)){
                                //     $i = 1;

                                //     // foreach($this->examen[0]['preguntas'] as $p){  
                                                                                
                                //         // echo "<td>{$p['nombre']}</td>";
                                //         // echo ($p['habilidades']['L'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                //         // echo ($p['habilidades']['R'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                //         // echo ($p['habilidades']['W'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                //         // echo ($p['habilidades']['S'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                        
                                //     // }
                                // }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
// var exam = <?php echo (!empty($this->examen)) ? json_encode($this->examen) : 'null' ?>;
var curso = <?php echo (!empty($this->cursos)) ? json_encode($this->cursos) : 'null' ?>;
$(document).ready(function(){
        // var id = $(this).val();
        var total = exam;
        var curso_total = curso;
        console.log(curso_total) 
        $.each(curso_total,function(e,i){
            console.log(i);
            $.ajax({
                url: _sysUrlBase_+'/academico/json_preguntas_examen',
                type: 'POST',
                dataType: 'json',
                async:false,
                data: {'idcurso': i.idcurso},
            }).done(function(resp){
                if(resp.code=='ok'){
                    exam = resp.data;
                    // console.log(exam);
                    // $('table tbody').html(' ');
                    var option = '';
                    var total_total = 0;
                    var total_L = 0;
                    var total_R = 0;
                    var total_W = 0;
                    var total_S = 0;
                    $.each(exam,function(f,j){
                        total_total += parseInt(j.total);
                        total_L += parseInt(j.general.L);
                        total_R += parseInt(j.general.R);
                        total_W += parseInt(j.general.W);
                        total_S += parseInt(j.general.S);
                       

                    });
                    // console.log(total_L)

                     option = option.concat('<tr>');
                    option = option.concat('<td>'+ (e) +'</td>');
                    option = option.concat('<td>'+ i.nombre +'</td>');
                    option = option.concat('<td>'+ total_total +'</td>');
                    option = option.concat('<td>'+ total_L +'</td>');
                    option = option.concat('<td>'+ total_R +'</td>');
                    option = option.concat('<td>'+ total_W +'</td>');
                    option = option.concat('<td>'+ total_S +'</td>');

                    option = option.concat('<tr>');
                    $('table tbody').append(option);
                    $('#exam').trigger('change');
                }else{
                    return false;
                }
            }).fail(function(xhr, textStatus, errorThrown){
                return false;
            });    
        });
        

});
</script>