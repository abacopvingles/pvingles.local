<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .breadcrumb {background-color:rgb(64, 113, 191); padding: 1ex;  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }
  .panel-user{ padding:0.4ex; border: 1px solid rgba(90, 137, 248, 0.41); position: relative;}
  .panel-user .item{ min-height:260px; overflow: auto;}
  .panel-user .pnlacciones{ display: none;}
  .panel-user.active .pnlacciones{ display: block; top: 0px;}
  .panel-user .nombre{ text-align: center; font-size: 1em;}
  .form-group{ margin-bottom: 0px; }
  .input-group { margin-top: 0px; }

</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('SmartQuiz'); ?></li>       
    </ol>
</div> </div>
<?php } ?>


<div class="row" id="ventana-<?php echo $idgui;?>">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>            
            <div class="col-xs-12 col-sm-4 col-md-2 text-center">
               <a class="btn btn-success"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add')?></a>
              </div>
            </div>

            
          </div>
      </div>
    </div>
  <div class="col-md-12 col-sm-12 col-xs-12" id="datalistado<?php echo $idgui;?>">
    <div id="cargando" style="display: none;">cargando</div>
      <div id="sindatos" style="display: none;">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Empty data')?>.</div>                
            </div>
        </div>
      </div>
      <div id="error" style="display: none;">Error</div>
      <div id="data">
      </div>
  </div>
  <div id="controles<?php echo $idgui;?>" style="display: none;">
    <div class="pnlacciones text-center">
     <a class="btn-selected btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected ')).' '.JrTexto::_('Assessment'); ?>"><i class="btnselected fa fa-hand-o-down"></i></a>      
      <a class="btnvermodal btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Edit')); ?>"><i class="fa fa-pencil"></i></a> 
      <!--a class="btn-eliminar btn btn-danger  btn-xs" title="<?php //echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-trash-o"></i></a-->
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos<?php echo $idgui; ?>='';

var addvalorcampo<?php echo $idgui; ?>=function(idlogro,campo,valor){  
  var formData = new FormData();
    formData.append('idlogro', idlogro);
    formData.append('campo', campo);
    formData.append('valor', valor);  
    var url=_sysUrlBase_+'/logro/addcampo';
    $.ajax({
      url: url,
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType:'json',
      cache: false,
      beforeSend: function(XMLHttpRequest){ },      
      success: function(data)
      {  
         if(data.code=='Error')
           mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      },
      error: function(e){ },
      complete: function(xhr){ }
    });
}

function refreshdatos<?php echo $idgui; ?>(){
   console.log('asdads');
    tabledatos<?php echo $idgui; ?>();
}
$(document).ready(function(){  
  var estados<?php echo $idgui; ?>={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}; 
  $('#ventana-<?php echo $idgui;?>')
  .on('click','.btnbuscar',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('keyup','#texto<?php echo $idgui; ?>',function(ev){
    if(e.keyCode == 13)
    refreshdatos<?php echo $idgui; ?>();
  }).on("mouseenter",'.panel-user', function(){
      if(!$(this).hasClass('active')){ 
        $(this).siblings('.panel-user').removeClass('active');
        $(this).addClass('active');
      }
  }).on("mouseleave",'.panel-user', function(){     
      $(this).removeClass('active');
  }).on('click','.panel-user', function(){      
      <?php if(!empty($ventanapadre)){?>
          var idexamen=$(this).attr('data-id');
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idexamen',idexamen);
            btn.trigger("addassesment");
            $(this).closest('.modal').find('.cerrarmodal').trigger('click');
          }
      <?php }  ?>     
  }).on("click",'.btn-selected', function(){
      $(this).addClass('active');
      var idexamen=$(this).closest('.panel-user').attr('data-id');
      <?php 
        if(!empty($ventanapadre)){?>
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idexamen',idexamen);
            btn.trigger("addassesment");
          }
          <?php }  ?>
     $(this).closest('.modal').find('.cerrarmodal').trigger('click');         
  });

  tabledatos<?php echo $idgui; ?>=function(){
    var frm=document.createElement('form');
    var formData = new FormData();
        formData.append('nombre', $('#teto-<?php echo $idgui;?>').val()); 
        formData.append('pr','smartlearn2017');
        var url= 'https://abacoeducacion.org/web/smartquiz/service/exam_list/?pr=smartlearn2017';
        console.log(url);
        $.ajax({
          url: url,
          type: "GET",
          //data:  formData,
          contentType: false,
          processData: false,
          dataType :'json',
          cache: false,
          beforeSend: function(XMLHttpRequest){
            $('#datalistado<?php echo $idgui;?> #cargando').show('fast').siblings('div').hide('fast');
          },      
          success: function(res)
          {                      
            if(res.code==200){
              var midata=res.data;
              if(midata.length){
                var html='';
                var controles=$('#controles<?php echo $idgui; ?>').html();
                $.each(midata,function(i,v){
                  var urlimg=_sysUrlStatic_+'/media/imagenes/cursos/';
                  var srcimg=_sysfileExists(v.portada)?(v.portada):(urlimg+'nofoto.jpg');
                  html+='<div class="col-md-2 col-sm-3 col-xs-12"><div class="panel-user" data-id="'+v.idexamen+'">'+controles;
                  html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%">'; 
                  html+='<div class="nombre"><strong>'+v.titulo+'</strong></div>';
                  html+='</div></div></div>';
                });
                $('#datalistado<?php echo $idgui; ?> #data').html(html).show('fast').siblings('div').hide('fast');
              }else     
                $('#datalistado<?php echo $idgui; ?> #sindatos').show('fast').siblings('div').hide('fast');
            }else{
              $('#datalistado<?php echo $idgui; ?> #error').html(res.message).show('fast').siblings('div').hide('fast');
            }
          },
          error: function(e) 
          {
            $('#datalistado<?php echo $idgui; ?> #error').show('fast').siblings('div').hide('fast');
          }
        });
  }
  tabledatos<?php echo $idgui; ?>();
});
</script>