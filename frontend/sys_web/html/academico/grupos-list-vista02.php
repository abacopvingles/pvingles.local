<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->grupos)) $grupos=$this->grupos;
//var_dump($grupos);
?>
<style type="text/css">
	img.user-circle{
		display: block;
	    max-height: 120px;
	    max-width: 120px;
	    text-align: center;
	    margin: auto;
	    border: 1px solid #ccc;
	}
	.item-user{
		padding: 1ex;
		position: relative;
	}
	.pnlacciones{
    position: absolute;
    bottom: 0px;
    text-align: center;
    width: 100%;
    background: #469de8;
    /*margin: -11px;*/
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
    top:auto !important;
  }
</style>
<div class="col-md-12"  id="pnl2<?php echo $idgui; ?>">
<?php
//var_dump($personal);
$i=0;
$url=$this->documento->getUrlBase();
if(!empty($grupos)){
	foreach ($grupos as $rw){ $i++;?>
<div class="col-md-4 item-user cls<?php echo $idgui ?>" id="item-user<?php echo $idgui ?>">
<div class="panel-user">
    <div class="row">
      <h4 class=" col-xs-12 border-turquoise color-turquoise text-center titulo"><i><?php echo $rw["nombre"]; ?></i></h4>
      <div class="col-md-12">
        <table class="table table-responsive">
          <tr><th><?php echo " N° ".JrTexto::_("vacantes") ?></th><th>:</th><td><?php echo $rw["nvacantes"];?></td></tr>
          <!--tr><th><?php //echo JrTexto::_('Local') ?></th><th>:</th><td><?php //echo $rw["strlocal"]."<br>".$rw["strambiente"]; ?></td></tr-->
          <tr><th><?php echo JrTexto::_('Type') ?></th><th>:</th><td><?php echo $rw["strtipo"]; ?></td></tr>
          <tr><th><?php echo JrTexto::_('Cursos') ?></th><th>:</th><td>
          <?php 
            if(!empty($rw['detalle'])){
              foreach($rw["detalle"] as $gpd){
               echo $gpd['strcurso']."(".$gpd["strdocente"].")<br>";
              }
            }else echo "-";
           ?></td></tr>
        </table>
      </div>
        <div class="col-xs-12">
          <!--img class="img-circle img-responsive user-circle" src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo !empty($per["foto"])?$per["foto"]:'user_avatar.jpg' ?>" style="max-width: 100%; min-width: 150px; max-height: 150px; min-height: 100px;" --></div>
        
    </div>
    <div class="pnlacciones text-center"  data-idgrupoaula="<?php echo $rw["idgrupoaula"]; ?>"  data-idgrupoauladet="<?php //echo $rw["idgrupoauladetalle"]; ?>">
        <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php //echo $dni;?>" data-titulo="<?php echo JrTexto::_('Ficha')." "; ?>"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
        <a class="btn-editar btn btn-warning btn-xs " href="javascript:void(0)" data-titulo="<?php echo JrTexto::_('Edit'); ?>"> <i class="fa fa-pencil"></i></a>
        <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php //echo $dni;?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
        <a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php //echo $dni;?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a-->
        <!--a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/acad_grupoaula/horario/?idgrupoauladetalle=<?php echo $rw["idgrupoauladetalle"];?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a>
        <a class="btn-eliminar btn btn-danger  btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('delete'); ?>"><i class="fa fa-trash-o"></i></a-->
    </div>
</div>
</div>
<?php }
}else{ ?>
<div class="titulo text-center">
  <div class="panel-user">
    <div class="pnel-body"><br>
  <h4><?php echo JrTexto::_('Empty data') ?></h4>
   <a href="<?php echo $this->documento->getUrlBase(); ?>/acad_grupoaula/formulario" id="btnadd<?php echo $idgui; ?>" class="btn btn-warning "><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add') ?></a>
   <br><br>
   </div>
 </div>
</div>
<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.cls<?php echo $idgui ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });

  $('#pnl2<?php echo $idgui; ?>').on('click','.btn-eliminar',function(ev){
    var idgrupoaula=$(this).closest('.pnlacciones').attr('data-idgrupoaula');
    var idgrupoauladetalle=$(this).closest('.pnlacciones').attr('data-idgrupoauladet');
    var btn=$(this);
    var formData = new FormData();    
    formData.append('idgrupoaula', idgrupoaula);
    formData.append('idgrupoauladetalle',idgrupoauladetalle );
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_grupoauladetalle/eliminar_grupoauladetalle',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      callback:function(rs){        
        btn.closest('.cls<?php echo $idgui ?>').remove();        
      }
    }
    sysajax(data);
    return false;
  }).on('click','.btn-editar',function(ev){
    var idgrupoaula=$(this).closest('.pnlacciones').attr('data-idgrupoaula');
    //var idgrupoauladetalle=$(this).closest('.pnlacciones').attr('data-idgrupoauladet');
    window.location.href=_sysUrlBase_+'/acad_grupoaula/formulariogrupo/?idgrupoaula='+idgrupoaula;
    //console.log(idgrupoaula,  idgrupoauladetalle);
  });
});
</script>