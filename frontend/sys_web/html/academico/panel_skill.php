<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.slick-items{
		position: relative;
	}
	.slick-item .image{
		overflow: hidden;
		width: 100%		
	}
	.slick-item{
		text-align: center !important;	

	}
	.slick-slide img ,.slick-item img{
		display: inline-block;
		text-align: center;
	}
	.slick-item .titulo{
		font-size: 1.6em;
		text-align: center;
	}	
</style>
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Balance reporting skills'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Certificate'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">
          <input type="hidden" name="fkcbrol" value="3">
          <input type="hidden" name="idgrupoauladetalle" id="idgrupoauladetalle" value="">
          <div class="row">
                      
             <div class="slick-item"> 
             	<div class="col-sm-4">
             		<a href="<?php echo $this->documento->getUrlBase();?>/academico/preguntas_examen" class="hvr-float">
                    	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
                        <div class="titulo"><?php echo JrTexto::_('Question Report');?></div>
                    </a>
             	</div>
             	<div class="col-sm-4">
             		<a href="<?php echo $this->documento->getUrlBase();?>/academico/examenes_totales" class="hvr-float">
	                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
	                    <div class="titulo"><?php echo JrTexto::_('General Exam Skills');?></div>
	                </a>
             	</div>
             	<div class="col-sm-4">
             		<a href="<?php echo $this->documento->getUrlBase();?>/academico/curso_total" class="hvr-float">
	                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
	                    <div class="titulo"><?php echo JrTexto::_('General exam skills by courses');?></div>
	                </a>
             	</div>
                
            </div> 
            <div class="slick-item"> 
            	<div class="col-sm-4"> 
	                <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidades" class="hvr-float">
	                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
	                    <div class="titulo"><?php echo JrTexto::_('Habilidades');?></div>
	                </a>
	            </div>
	            <div class="col-sm-4"> 
	                <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneral" class="hvr-float">
	                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
	                    <div class="titulo"><?php echo JrTexto::_('General Skills');?></div>
	                </a>
	            </div>
	            <div class="col-sm-4"> 
	                <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneralesxunidades" class="hvr-float">
	                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
	                    <div class="titulo"><?php echo JrTexto::_('General skills by units');?></div>
	                </a>
	            </div>   
            </div>
            <div class="slick-item"> 
		        <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneralesxcurso" class="hvr-float">
                	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
                    <div class="titulo"><?php echo JrTexto::_('General skills by courses');?></div>
                </a>             
		    </div> 
		</div>  
           <!--  <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="fkcbcurso" class="form-control fkcbcurso">
                
                    <?php if(!empty($this->cursos))
                    foreach ($this->cursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
                </select>
              </div>
            </div> -->
</div>
<button id="actualuarmatricula123" class="hide">actualizar</button>
           </div>
          </form>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 hide " >
      <div class="panel" id="vista<?php echo $idgui; ?>">
      </div>
    </div>
  </div>

