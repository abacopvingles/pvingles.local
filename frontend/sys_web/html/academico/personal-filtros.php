<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Personal'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Personal'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmpersonal<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">          
          <div class="row">
              <div class="col-xs-6 col-sm-4 col-md-3 form-group">
                  <label><?php echo JrTexto::_('Rol'); ?></label>
                  <div class="cajaselect">            
                  <select id="fkcbrol" name="rol" class="form-control" >                  
                    <?php if(!empty($this->fkrol))
                      foreach ($this->fkrol as $fkrol) { 
                        if($this->idRolUser==10){
                          if($fkrol["idrol"]==1||$fkrol["idrol"]==10||$fkrol["idrol"]==5||$fkrol["idrol"]==4||$fkrol["idrol"]==3) continue;
                        }else if($fkrol["idrol"]==3) continue ?>
                        <option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$this->idrol?"selected":""; ?> ><?php echo ucfirst($fkrol["rol"]); ?></option><?php } ?>                        
                  </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
            <label><?php echo JrTexto::_('Gender'); ?></label>
            <div class="cajaselect">
              <select id="fkcbsexo" name="sexo" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
            <label><?php echo JrTexto::_('Civil status'); ?></label>
            <div class="cajaselect">
              <select id="fkcbestado_civil" name="estado_civil" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                  <?php 
                  if(!empty($this->fkestado_civil))
                  foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>
                </select>
            </div>
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
              <label><?php echo ucfirst(JrTexto::_('State'));?></label>
              <div class="cajaselect">
                <select name="estado" id="cbestado" class="form-control">
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label>
              <div class="cajaselect">
                <select id="fkcbdre" name="dre" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                    <?php 
                    if(!empty($this->dress))
                    foreach ($this->dress as $fk) { ?><option value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
              <div class="cajaselect">
                <select id="fkcbugel" name="ugel" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
              <div class="cajaselect">
                <select id="fkcbiiee" name="iiee" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                </select>
              </div>
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-3 form-group pnldocente<?php echo $idgui;?>" >
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select id="fkcbcurso" name="curso" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                    <?php if(!empty($this->fkcursos))
                    foreach ($this->fkcursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
                </select>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 form-group">
              <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 text-center pnlnodocente<?php echo $idgui;?>"><br>
              <!--a class="btn btn-warning btnchangevista<?php echo $idgui; ?>" href="#" data-titulo="<?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?>">
                <i class="fa fa-drivers-license-o"></i> </a-->
                <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "formulario"))."?return=personal&accion=add&plt=".$this->documento->plantilla;?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
                <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php echo JrTexto::_("Importar"); ?>"><i class="fa fa-upload"></i> <?php echo JrTexto::_('Importar')?></a>
                <!--a class="btn btn-primary btnvermodal" data-modal="si" href="<?php //echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php //echo JrTexto::_("Personal").' - '.JrTexto::_("import"); ?>"><i class="fa fa-cloud-upload"></i> <?php //echo JrTexto::_('Import')?></a-->
              </div>
            </div>
            </div>
          </form>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 pnlvista" id="vista<?php echo $idgui; ?>">        
    </div>
    <script type="text/javascript">
      var vista=window.localStorage.vistapnl||1;
      var cargarvista<?php echo $idgui; ?>=function(view){
        var _vista=view||vista||1;
        var frmtmp=document.getElementById('frmpersonal<?php echo $idgui; ?>');
        var formData = new FormData(frmtmp);
        formData.append('vista',_vista);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/listado',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'html',
          callback:function(dt){$('#vista<?php echo $idgui; ?>').html(dt);}
        }
        sysajax(data);
      }
      cargarvista<?php echo $idgui; ?>(vista);
      $(document).ready(function(){
        $('.btnchangevista<?php echo $idgui; ?>').click(function(){
          var _vista=window.localStorage.vistapnl||1;
          vista=_vista==1?2:1;
          window.localStorage.setItem('vistapnl',vista);
          cargarvista<?php echo $idgui; ?>(vista);
        });
      })
    </script>
    </div>
</div>

<script type="text/javascript">
var tabledatos5976271b3962a='';

$(document).ready(function(){
  $('#fkcbsexo').change(function(ev){
    cargarvista<?php echo $idgui; ?>();
  });
  $('#fkcbestado_civil').change(function(ev){
    cargarvista<?php echo $idgui; ?>();
  });

  $('#fkcbcurso').change(function(ev){
    cargarvista<?php echo $idgui; ?>();
  });

  $('#fkcbrol').change(function(ev){
    var rol=$(this).val()||'';
    if(rol!='1' && rol!='9'){
      $('#fkcbcurso').val('');
      $('.pnldocente<?php echo $idgui;?>').show();
      $('.pnlnodocente<?php echo $idgui;?>').removeClass('col-md-6').addClass('col-md-3');
      $('#fkcbcurso').closest('.pnldocente<?php echo $idgui;?>').hide();
      if(rol=='2') {       
        $('#fkcbcurso').closest('.pnldocente<?php echo $idgui;?>').show();
        $('#fkcbcurso').trigger('change');
      }else if(rol=='7'){
        $('#fkcbiiee').val('');
        $('#fkcbiiee').closest('.pnldocente<?php echo $idgui;?>').hide();
      }else if(rol=='8'){
        $('#fkcbiiee').val('');
        $('#fkcbugel').val('');
        $('#fkcbiiee').closest('.pnldocente<?php echo $idgui;?>').hide();
        $('#fkcbugel').closest('.pnldocente<?php echo $idgui;?>').hide();
      }
      cargarvista<?php echo $idgui; ?>();
    }else{
      $('.pnlnodocente<?php echo $idgui;?>').removeClass('col-md-3').addClass('col-md-6');
      $('.pnldocente<?php echo $idgui;?>').hide();
      cargarvista<?php echo $idgui; ?>();
    }    
  });
  $('#fkcbrol').trigger('change');
  $('#cbestado').change(function(ev){
    cargarvista<?php echo $idgui; ?>();
  });

  $('.btnbuscar').click(function(ev){
    cargarvista<?php echo $idgui; ?>();
  });

  $('#texto').keyup(function(e) {
    if(e.keyCode == 13) {
        cargarvista<?php echo $idgui; ?>();
    }
  });
  
  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'personal', 'setCampo', id,campo,data);
          if(res) cargarvista<?php echo $idgui; ?>();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var idrol=$('#fkcbrol').val();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'cargarvista<?php echo $idgui; ?>';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    url =url+'&rol='+idrol;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url+'&idrol='+$('#fkcbrol').val());
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true,tam:'85%'});    
  }).on('change','select#fkcbdre',function(ev){
      ev.preventDefault();
      var iddre=$(this).val();
      var fd2= new FormData();
      fd2.append("iddepartamento", iddre);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/ugel/buscarjson',
      callback:function(rs){
          $sel=$('#fkcbugel');
          $sel.html('<option value="" >All</option>');
          if(iddre=='') return $sel.trigger('change');
          dt=rs.data;
          $.each(dt,function(i,v){
            var selop='';
            $sel.append('<option value="'+v.idugel+'" >'+v.descripcion+'</option>');
          })
          cargarvista<?php echo $idgui; ?>();
      }})
  }).on('change','select#fkcbugel',function(ev){
      ev.preventDefault();
      var idugel=$(this).val();
      var fd2= new FormData();
      fd2.append("idugel", idugel);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/local/buscarjson',
      callback:function(rs){
          $sel=$('#fkcbiiee');
          $sel.html('<option value="" >All</option>');
          if(idugel=='') return $sel.trigger('change');
          dt=rs.data;
          $.each(dt,function(i,v){
            var selop='';
            $sel.append('<option value="'+v.idlocal+'" >'+v.nombre+'</option>');
            cargarvista<?php echo $idgui; ?>();
          })
      }})
  }).on('change','select#fkcbiiee',function(ev){
    cargarvista<?php echo $idgui; ?>();
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'personal', 'eliminar', id);
        if(res) cargarvista<?php echo $idgui; ?>();
      }
    }); 
  })
})
</script>