<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
//$usuarioAct = NegSesion::getUsuario();
//$grupo=!empty($this->datos)?$this->datos:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.slick-items{
		position: relative;
	}
	.slick-item .image{
		overflow: hidden;
		width: 100%		
	}
	.slick-item{
		text-align: center !important;	

	}
	.slick-slide img ,.slick-item img{
		display: inline-block;
		text-align: center;
	}
	.slick-item .titulo{
		font-size: 1.6em;
		text-align: center;
	}	
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li> 
        <li class="active"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academic"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="row">
	<div class="col-md-12">	
	<div class="panel" >
		<div class="panel-heading bg-blue" >
			<h3 class="panel-title" style="font-size: 1.8em;"><?php echo ucfirst(JrTexto::_('Management')); ?></h3>
			<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
		</div>
		<div class="panel-body">			
		   <div class="row">
		   		<div class="col-md-12">
			   		<div class="slick-items">					   	
	                    <div class="slick-item"> 
	                        <a href="<?php echo $this->documento->getUrlBase();?>/matricula" class="hvr-float">
	                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/estudiantes.png">
	                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Students')); ?></div>
	                        </a>
	                    </div>
	                    <div class="slick-item"> 

	                        <a href="<?php echo $this->documento->getUrlBase();?>/matricula/?ver=Offlineactivities" class="hvr-float">
	                        	<span class="badge badge-primary" id="nbadactividades" style="position: absolute; right: 1em; top:0px; background: #2978b5;">4</span>
	                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/activities.png">
	                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Review of activities')); ?></div>
	                            
	                        </a>
	                    </div>
	                    <div class="slick-item"> 	                    	
	                        <a href="<?php echo $this->documento->getUrlBase();?>/matricula/?ver=offlineexamenes" class="hvr-float">
	                        	<span class="badge badge-primary" id="nbadgeexamenes" style="position: absolute; right: 1em; top:0px; background: #e75f56;">0</span>
	                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/exam.png">
	                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Review of exam')); ?></div>	                            
	                        </a>
	                    </div>
						<div class="slick-item"> 
	                        <a href="<?php echo $this->documento->getUrlBase();?>/matricula/notas" class="hvr-float">
	                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/notas.png">
	                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Notes')); ?></div>
	                        </a>
	                    </div> 	                  		                   
	                </div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Balance reporting activities'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">
                          <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidades" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
		                            <div class="titulo"><?php echo JrTexto::_('Skills'); ?></div>
		                        </a>
		                    </div>
                            <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneral" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
		                            <div class="titulo"><?php echo JrTexto::_('General skills'); ?></div>
		                        </a>
		                    </div>                            
                            <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneralesxunidades" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
		                            <div class="titulo"><?php echo JrTexto::_('Skills by unit'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/reportes/reportehabilidadesgeneralesxcurso" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportehabilidades.png">
		                            <div class="titulo"><?php echo JrTexto::_('Skills by course'); ?></div>
		                        </a>
		                    </div>
                           

		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Balance reporting test'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">
				   			<div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/academico/examenes_totales" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
		                            <div class="titulo"><?php echo JrTexto::_('General skills'); ?></div>
		                        </a>
		                    </div>
                            <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/academico/preguntas_examen" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
		                            <div class="titulo"><?php echo JrTexto::_('Question report'); ?></div>
		                        </a>
		                    </div>                            
                            <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/academico/curso_total" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/reportes.png">
		                            <div class="titulo"><?php echo JrTexto::_('Skills by courses'); ?></div>
		                        </a>
		                    </div>
		                    		                   
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<script type="text/javascript">
$(document).ready(function(e){		
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').removeClass('static');
    var fd = new FormData();   
    $.ajax({
        url: _sysUrlBase_+'/actividad_alumno/nexamenesoffline',
        type: "POST",
        data:  fd,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend:function(){
        	$('#nbadgeexamenes').html('<i class="fa fa-spinner fa-spin"></i>')
        },
        success: function(data){
            if(data.code==='ok'){
            	$('#nbadgeexamenes').html(data.nexamenes);
            }else{
            	$('#nbadgeexamenes').remove();                
            }
        },
        error: function(xhr,status,error){
           $('#nbadgeexamenes').html('Error');
        }               
    });

    var fd = new FormData();   
    $.ajax({
        url: _sysUrlBase_+'/actividad_alumno/nactividadoffline',
        type: "POST",
        data:  fd,
        contentType: false,
        dataType :'json',
        cache: false,
        processData:false,
        beforeSend:function(){
        	$('#nbadactividades').html('<i class="fa fa-spinner fa-spin"></i>')
        },
        success: function(data){
            if(data.code==='ok'){
            	$('#nbadactividades').html(data.nactividades);
            }else{
            	$('#nbadactividades').remove();                
            }
        },
        error: function(xhr,status,error){
           $('#nbadactividades').html('Error');
        }               
    });
});
</script>