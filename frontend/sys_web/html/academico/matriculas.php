<?php
defined("RUTA_BASE") or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$persona=!empty($this->datos)?$this->datos:"";
$idproyecto=$this->idproyecto;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/importexcel/xlsx.full.min.js"></script>
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
</style>
<?php if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
    <?php if(!$ismodal&&$idproyecto==3){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlBase();?>/acad">&nbsp;<?php echo JrTexto::_("Matriculas"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Matriculate"); ?></li>
    <?php }else{ ?>
      <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
      <li class="active">&nbsp;<?php echo JrTexto::_("Matriculate"); ?></li>
    <?php } ?>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id="filtros<?php echo $idgui ?>">
  <div class="col-md-12">
  <form id="frm00<?php echo $idgui; ?>">
    <div class="panel">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-2">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Year")); ?> </label>
              <div class="cajaselect">
                <select name="anio" id="anio<?php echo $idgui;?>" class="form-control">
                  <?php for($anio=date('Y');$anio>=date('Y')-5;$anio--){?>
                    <option value="<?php echo $anio; ?>"><?php echo $anio; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-2">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("State")); ?> </label>
              <div class="cajaselect">
                <select name="estado" id="estado<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fkestados)) foreach ($this->fkestados as $fk) { ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fkestado?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-2">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> </label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>          
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Local")); ?> </label>
              <div class="cajaselect">
                <select name="idlocal" id="local<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>
                  <?php if(!empty($this->fklocales)) foreach ($this->fklocales as $fk) { ?>
                    <option value="<?php echo $fk["idlocal"]?>" <?php echo $fk["idlocal"]==@$this->fkidlocal?'selected="selected"':'';?> ><?php echo $fk["nombre"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Groups Aula")); ?> </label>
              <div class="cajaselect">
                <select name="idgrupoaula" id="idgrupoaula<?php echo $idgui;?>" class="form-control">                  
                  <?php if(!empty($this->gruposaula)) foreach ($this->gruposaula as $fk) { ?>
                    <option value="<?php echo $fk["idgrupoaula"]?>" <?php echo $fk["idgrupoaula"]==@$this->fkgrupoaula?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 text-center"><hr>
            <a class="btn btn-warning" id="btnchangevista<?php echo $idgui; ?>" href="javascript:void(0)" data-titulo="<?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?>">
            <i class="fa fa-drivers-license-o"></i> <?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?></a>
            <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "formulario"))."?return=personal&accion=add&plt=".$this->documento->plantilla;?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
            <a class="btn btn-primary btnimport<?php echo $idgui; ?>" data-modal="si" href="javascript:void(0)" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("import"); ?>"><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Import').' '.JrTexto::_('student')?></a>
          </div>
        </div>
      </div>
    </div>
  </form>
  </div>
</div>
<div class="row" id="pnlvista<?php echo $idgui; ?>">    
</div>
<button class="tmprecargarlista<?php echo $idgui; ?>" style="display: none"></button>
<input type="file" name="importjson" id="importjson" class="hide" accept="file_extension|*/xls|*/xlsx|media_type">
<div id="datosimport<?php echo $idgui; ?>" class=" hide col-md-12">
  <div class="panel panel-body">
  <table class="table table-striped table-responsive tableimport<?php echo $idgui; ?>">
    <tr>
      <th><?php echo JrTexto::_("Dni");?></th>
      <th><?php echo JrTexto::_("Name");?></th>
      <th><?php echo ucfirst(JrTexto::_("Father's last name")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></th>
      <th><?php echo ucfirst(JrTexto::_('Mobile number')); ?></th>
      <th><?php echo ucfirst(JrTexto::_('Telephone')); ?></th>
      <th><?php echo ucfirst(JrTexto::_('Actions')); ?></th>    
    </tr>
  </table>
  <div class="col-md-12 text-center">
    <a href="javascript:void(0)" class="btnsaveimport<?php echo $idgui; ?> btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Import') ?></a>
  </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  var idproyecto=parseInt('<?php echo $this->idproyecto;?>');
  var vista=window.localStorage.vistapnl||1;
  var cargarvista<?php echo $idgui;?>=function(view){
    vista=window.localStorage.vistapnl;
    var _vista=view||vista||1;
    var frmtmp = document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);    
    formData.append('vista',_vista);
    formData.append('plt','blanco');
    formData.append('idproyecto',idproyecto);
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_matricula/listado',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(data){        
        $('#pnlvista<?php echo $idgui; ?>').html(data);
      }
    }
    sysajax(data);
  }
  var cargargrupos<?php echo $idgui;?>=function(){
    ambtmp=$('#idgrupoaula<?php echo $idgui;?>');
    var frmtmp = document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);
    formData.append('idgrupoaula','');
    formData.append('idproyecto',idproyecto);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_grupoaula/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(data){
          var tr=data.data; 
          console.log(tr);
          $('option',ambtmp).remove();        
          if(tr!=''){              
              $.each(tr,function(i,v){                    
                ambtmp.append('<option value="'+v['idgrupoaula']+'">'+v["nombre"]+'</option>');
            });
          }
          $('#idgrupoaula<?php echo $idgui;?>').trigger('change');
        }
      }
      sysajax(data);
  }

  $('#tipo<?php echo $idgui;?>').change(function(ev){
    var vtmp_=$(this).val().toLowerCase();
    if(vtmp_=='v'){
      $('.notypev<?php echo $idgui; ?>').hide();
      $('#local<?php echo $idgui;?>').val('');
      $('#ambiente<?php echo $idgui;?>').val('');
    }else{
      $('.notypev<?php echo $idgui; ?>').show();
    }
     cargargrupos<?php echo $idgui;?>();
  });

  $('#idgrupoaula<?php echo $idgui;?>').change(function(ev){
    var idgrupoaula=$(this).val();
    if(idgrupoaula==''){
       $('.btnadd<?php echo $idgui; ?>').addClass('disabled').attr('disabled','disabled');
       $('.btnimport<?php echo $idgui; ?>').addClass('disabled').attr('disabled','disabled');
       $('#btnchangevista<?php echo $idgui; ?>').addClass('disabled').attr('disabled','disabled');
       $('#pnlvista<?php echo $idgui; ?>').html('');
    }else{
      $('.btnadd<?php echo $idgui; ?>').removeClass('disabled').removeAttr('disabled','disabled');
      $('.btnimport<?php echo $idgui; ?>').removeClass('disabled').removeAttr('disabled','disabled');
      $('#btnchangevista<?php echo $idgui; ?>').removeClass('disabled').removeAttr('disabled','disabled');
       cargarvista<?php echo $idgui;?>();
    }    
  });

  $('#local<?php echo $idgui;?>').change(function(ev){
     cargargrupos<?php echo $idgui;?>();
  });

  $('#ambiente<?php echo $idgui;?>').change(function(ev){
      cargargrupos<?php echo $idgui;?>();
  });

  $('#estado<?php echo $idgui;?>').change(function(ev){
    cargargrupos<?php echo $idgui;?>();
  });

  $('#btnchangevista<?php echo $idgui; ?>').click(function(){
    vista=window.localStorage.vistapnl||1;
    vista=vista==1?2:1;
    localStorage.setItem('vistapnl',vista);
    cargarvista<?php echo $idgui;?>(vista);
  });

  $('.tmprecargarlista<?php echo $idgui; ?>').on('recargarlista',function(ev){
    cargarvista<?php echo $idgui;?>();
  });


  $('.btnadd<?php echo $idgui; ?>').click(function(){
    var formData = new FormData(); 
    formData.append('plt','blanco');
    formData.append('fcall','btnadd<?php echo $idgui; ?>');
    formData.append('datareturn','true');
    formData.append('rol',3);
    var returnbtnobj=$(this);
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/personal/formulario',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(data){
        returnbtnobj.addClass('tmpbtnadd<?php echo $idgui; ?>');   
        $('#pnlvista<?php echo $idgui; ?>').html(data);
        returnbtnobj.on('returndata',function(){
          $(this).off('returndata');
          $(this).removeClass('tmpbtnadd<?php echo $idgui; ?>');
          var newdni=$(this).attr('data-returnidpersona');         
          $(this).removeAttr('data-return');
          matricular<?php echo $idgui?>(newdni);          
        });
      }
    }
    sysajax(data);
  });

  var matricular<?php echo $idgui?>=function(dnis,isjson){
    var isjson=isjson||false;
    var myForm = document.getElementById('frm00<?php echo $idgui; ?>'); 
    var formData = new FormData(myForm); 
    formData.append('idalumno', dnis);
    formData.append('idrol', 3);
    formData.append('isjson', isjson);
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_matricula/guardarAcad_matricula',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){
        cargarvista<?php echo $idgui;?>();  
      }
    }
    sysajax(data);
    return false;   
  };
  $('.btnimport<?php echo $idgui; ?>').click(function(ev){
    $('#importjson').trigger('click');
  })
  $('#idgrupoaula<?php echo $idgui;?>').trigger('change');

  $('#pnlvista<?php echo $idgui; ?>').on('click','.btn_removetr',function(ev){
    $(this).closest('tr').remove();
  }).on('click','.btnsaveimport<?php echo $idgui; ?>',function(ev){
    var tableimport=$('#pnlvista<?php echo $idgui; ?> table.tableimport<?php echo $idgui; ?>');
    if(tableimport.length){
      var tr=tableimport.find("tr.addregistroimporttr");
      var datosimportados=[];
      $.each(tr,function(i,v){
        var data={
          dni:$(v).find("td:eq(0)").text().trim(),
          nombre:$(v).find("td:eq(1)").text().trim(),
          ape_paterno:$(v).find("td:eq(2)").text().trim(),
          ape_materno:$(v).find("td:eq(3)").text().trim(),
          celular:$(v).find("td:eq(4)").text().trim(),
          telefono:$(v).find("td:eq(5)").text().trim(),
          rol:2
        }
        datosimportados.push(data);
      });
      var myForm = document.getElementById('frm00<?php echo $idgui; ?>'); 
      var formData = new FormData(myForm); 
      formData.append('alumnos', JSON.stringify(datosimportados));
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_matricula/guardarAcad_matriculavarios',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok : true,
        callback:function(data){
          cargarvista<?php echo $idgui;?>();  
        }
      }
      sysajax(data);
      return false;
    }    
  });
});
</script>


<script>
  //para importar json
var X = XLSX;
//var XW = {msg: 'xlsx',worker: './xlsxworker.js'};

var global_wb;
var process_wb = (function() {
  var OUT = document.getElementById('out');
  var to_json = function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function(sheetName) {
      var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});
      if(roa.length) result[sheetName] = roa;
    });
    return JSON.stringify(result, 2, 2);
  };

  return function process_wb(wb) {
    global_wb = wb;
    var output = to_json(wb);
    if(_isJson(output)){
      $table=$('#datosimport<?php echo $idgui; ?>').clone();
      $table.removeClass('hide');
      var hoja1=JSON.parse(output).Hoja1;
      var ix_=0;
      hoja1.forEach(function(v) {
        if(ix_>0){
        $tr='<tr class="addregistroimporttr">';
        v.forEach(function(v1){
          $tr+='<td>'+v1+'</td>';
        })
        $tr+='<td><i class="fa fa-trash btn_removetr"></i> </td>';
        $tr+='</tr>';
        $table.find('table').append($tr);
        } ix_++;
      });      
      $('#pnlvista<?php echo $idgui; ?>').html($table);
    }
  };
})();


var do_file = (function() {
  var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
  var use_worker = typeof Worker !== 'undefined';

  return function do_file(files) {
    rABS = true;//domrabs.checked;
    use_worker = true; //domwork.checked;
    var f = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
      var data = e.target.result;
      if(!rABS) data = new Uint8Array(data);
      
      else process_wb(X.read(data, {type: rABS ? 'binary' : 'array'}));
    };
    if(rABS) reader.readAsBinaryString(f);
    else reader.readAsArrayBuffer(f);
  };
})();


(function() {
  var xlf = document.getElementById('importjson');
  if(!xlf.addEventListener) return;
  function handleFile(e) { do_file(e.target.files); }
  xlf.addEventListener('change', handleFile, false);
})();
</script>