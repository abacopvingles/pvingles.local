<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
	.strinfo{
		padding: 0.7ex;
	}
	.strinfo strong{
		    min-width: 100px !important;
    		display: inline-block;
	}
</style>
<div class="col-md-12" id="pnl_<?php echo $idgui; ?>">
    <div class="panel" >      
      	<div class="panel-body " style="">
	      	<div class="row" id="pnl_information<?php echo $idgui;?>">
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Teacher')) ?></strong>:
	      			  <?php echo $this->horario["iddocente"]." - ".$this->horario["strdocente"]; ?> </div>
	      		</div>
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Groups')) ?></strong>:
	      			  <?php echo $this->horario["idcurso"]." - ".$this->horario["strgrupoaula"]; ?> </div>
	      		</div>
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Course')) ?></strong>:
	      			  <?php echo $this->horario["idcurso"]." - ".$this->horario["strcurso"]; ?> </div>
	      		</div>
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Date')) ?></strong>:
	      			  <?php echo "<b>".JrTexto::_('Start')." :</b>".date("Y/m/d", strtotime($this->horario["fecha_inicio"]))." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>".JrTexto::_('Finish')." :</b> ".date("Y-m-d",strtotime($this->horario["fecha_final"])); ?> </div>
	      		</div>
	      		
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Local')) ?></strong>:
	      			  <?php echo $this->horario["idlocal"]." - ".$this->horario["strlocal"]; ?> </div>
	      		</div>
	      		<div class="col-md-6 ">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Ambient')) ?></strong>:
	      			  <?php echo $this->horario["idambiente"]." - ".$this->horario["strambiente"]; ?></div> 
	      		</div>
	      		<div class="col-md-12">
	      			<hr>
	      		</div>  
	      		<div class="col-md-4">	      			
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Dial date')) ?></strong>:
	      			  <?php echo date("Y/m/d", strtotime($this->horario["hora_entrada"])); ?></div> 
	      		</div>
	      		<div class="col-md-4">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Check in time')) ?></strong>:
	      			  <?php echo date("H:i:s", strtotime($this->horario["hora_entrada"])); ?></div> 
	      		</div>	
	      		<div class="col-md-4">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Departure time')) ?></strong>:
	      			  <?php echo date("H:i:s", strtotime($this->horario["hora_salida"])); ?></div> 
	      		</div>
	      		<!--div class="col-md-3">
	      			  <div class="strinfo"><strong><?php echo ucfirst(JrTexto::_('Departure time')) ?></strong>:
	      			  <?php echo date("H:i:s", strtotime($this->horario["hora_salida"])); ?></div> 
	      		</div-->	      		
 				<div class="col-md-12">
	      			<hr>
	      		</div> 
 				<form id="frmmarcar<?php echo $idgui; ?>">
 					<?php //var_dump($this->entrada); ?>
 					<input type="hidden" name="idmarcacion" id="idmarcacion<?php echo $idgui; ?>" value="<?php echo @$this->entrada["idmarcacion"];?>">

 					<input type="hidden" name="idhorario" value="<?php echo $this->horario["idhorario"];?>">
 					<input type="hidden" name="idgrupodetalle" value="<?php echo $this->horario["idgrupoauladetalle"];?>">
 					<input type="hidden" name="idcurso" value="<?php echo $this->horario["idcurso"];?>">
 					<input type="hidden" name="iddocente" value="<?php echo $this->horario["iddocente"];?>">
 					<input type="hidden" name="fecha" value="<?php echo !empty($this->entrada["fecha"])?$this->entrada["fecha"]:date('Y-m-d');?>"> 					
 					<input type="hidden" name="tipomarcacion" id="tipomarcacion<?php echo $idgui; ?>" value="<?php echo $this->tipomarcacion;?>">
 					<?php if($this->tipomarcacion=='I'&& $this->marcar==true){ ?> 					
 					<div class="col-md-12 text-center">
 						<div class="form-group" style="width: 200px !important; margin: 1em auto;">
			               <label class="control-label"><?php echo JrTexto::_('Hour mark entrance') ?></label>			             
			               <input type="text"  name="horaingreso" id="horaingreso<?php echo $idgui; ?>" readonly="readonly" class="form-control microno1 text-center" value="<?php echo date("H:i:s");?>" data-fin="<?php echo date("H:i:s", strtotime('+15 minutes',strtotime($this->horario["hora_entrada"]))); ?>">
			            </div>
		      		</div>
		      		<?php }elseif($this->tipomarcacion=='O'){?> 
		      		<div class="col-md-4">	      			
	      			  <div class="strinfo"><strong>N° <?php echo JrTexto::_('Students') ?></strong>:
	      			  	<input type="text"  name="nalumnos" id="nalumnos<?php echo $idgui; ?>" class="form-control text-right" <?php echo $this->marcar==false?'readonly="readonly"':'';?> value="<?php echo !empty($this->entrada["nalumnos"])?$this->entrada["nalumnos"]:0;?>" >
	      			  </div> 
		      		</div>
		      		<div class="col-md-4">
		      			<div class="strinfo"><strong><?php echo JrTexto::_('Marked entry time') ?></strong>:
		      			  	<input type="text"  name="horaingreso" id="horaingreso<?php echo $idgui; ?>" readonly="readonly" class="form-control text-center" value="<?php echo !empty($this->entrada["horaingreso"])?$this->entrada["horaingreso"]:date('H:i:s');?>">
		      			 </div>
		      		</div>	
		      		<div class="col-md-4">
		      			<div class="strinfo"><strong><?php echo JrTexto::_('departure time to mark') ?></strong>:
	      			  		<input type="text"  name="horasalida" id="horasalida<?php echo $idgui; ?>" readonly="readonly" class="form-control <?php echo $this->marcar==true?'microno1':'';?> text-center" value="<?php echo !empty($this->entrada["horasalida"])?$this->entrada["horasalida"]:date('H:i:s');?>" data-fin="<?php echo date("H:i:s", strtotime('+15 minutes',strtotime($this->horario["hora_salida"]))); ?>">
	      			  	</div>
		      		</div>
		      		<div class="col-md-12">
		      			<div class="strinfo"><strong><?php echo JrTexto::_('Observation') ?></strong>:
		      				<textarea name="observacion" id="observacion<?php echo $idgui; ?>" class="form-control" <?php echo $this->marcar==false?'readonly="readonly"':'';?> ><?php echo !empty($this->entrada["observacion"])?$this->entrada["observacion"]:'';?></textarea>
	      			  	</div>
		      		</div>		      		
		      		<?php } if($this->marcar==true){?>
		      		<div class="col-md-12 text-center">
		      			<button type="button" class="btn btn-success btnSave<?php echo $idgui; ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Mark'); ?></button>
		      		</div>
		      		<?php }?>
 				</form>
          	</div>
          	<div class="col-md-12 hide" id="msjsuccess<?php echo $idgui;?>" >
      			<div class="alert alert-success">
				  <strong><?php echo JrTexto::_('Success');?>! </strong><?php echo JrTexto::_('Your dialing has been saved correctly'); ?>
				</div>
      		</div>
      		<?php if($this->marcar==false){?>
      		<div class="col-md-12 " id="msjsuccess2<?php echo $idgui;?>" >
      			<div class="alert alert-info">
				  <strong><?php echo JrTexto::_('Information');?>! </strong><?php echo @$this->msj; ?>
				</div>
      		</div>
      		<?php } ?>
        </div>
    </div>
</div>	
<script type="text/javascript">
	$(document).ready(function(){
		var formatomostrar='hh:mm:ss a';
		$('.btnSave<?php echo $idgui; ?>').click(function(){
			var tm=$('#tipomarcacion<?php echo $idgui; ?>').val();
			var horasalida='00:00:00';
			if(tm=='I') horaingreso=moment($('#horaingreso<?php echo $idgui; ?>').val(),formatomostrar).format('HH:mm:ss');
			if(tm=='O') horasalida=moment($('#horasalida<?php echo $idgui; ?>').val(),formatomostrar).format('HH:mm:ss');
			var frmmarcar=document.getElementById('frmmarcar<?php echo $idgui; ?>');			
			var formData = new FormData(frmmarcar);	
			if(tm=='I') formData.append('horaingreso',horaingreso);
			if(tm=='O') formData.append('horasalida',horasalida);
			formData.append('idioma',_sysIdioma_);
			var data={
			    fromdata:formData,
			    url:_sysUrlBase_+'/acad_marcacion/guardarmarcacion',
			    msjatencion:'<?php echo JrTexto::_('Attention');?>',
			    type:'json',
			    showmsjok:true,
			    callback:function(rs){			    	
			    	$('#idmarcacion<?php echo $idgui; ?>').val(rs.newid);
			    	//$('.btnSave<?php echo $idgui; ?>').hide();
			    	$('.microno1').trigger('termino');
			    	$('#pnl_information<?php echo $idgui;?>').hide();
			    	$('#msjsuccess<?php echo $idgui;?>').show().removeClass('hide');

			    }
			}
			sysajax(data);
			return false;
		});
		

		$('.microno1').achtcronometro({formato:formatomostrar});	

		$('#horamarcar<?php echo $idgui; ?>').on('termino',function(ev){
			$('.btnSave<?php echo $idgui; ?>').addClass('disabled').attr('disabled','disabled');
		})		


	});
</script>