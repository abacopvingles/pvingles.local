<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>


<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Bolsa_empresas'">&nbsp;<?php echo JrTexto::_('Bolsa_empresas'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
<div class="col-md-12">
  <div class="panel">
    <div class="panel-body">
      <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
        <input type="hidden" name="Idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
        <input type="hidden" name="accion" id="accion" value="<?php echo JrTexto::_($this->frmaccion);?>">
        <div class="row">
          <div class="col-md-12 col-sm-12">
          	<div class="col-md-12 form-group">
	            <div class="col-xs-12 col-sm-6 col-md-8">
	            	<label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Text to search')); ?></label>                    
	              <input type="text" class="form-control" name="dni" id="tmpdni<?php echo $idgui; ?>" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder="">
	            </div>
	            <div class="col-xs-12 col-sm-6 col-md-4"><br>
		          <a class="btn btn-info btnsearch<?php echo $idgui ?>" href="javascript:void(0)"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></a>          
		        </div>  
	        </div>
          	
            <div class="col-md-12 form-group">
            	<div class="col-xs-12 col-sm-12 col-md-12">
            		<label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
                	<input type="text" disabled id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Nombre de la empresa') ?>">	
            	</div>
                
            </div>

          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>


  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="Idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="accion" value="<?php echo JrTexto::_($this->frmaccion);?>">
		    <div class="col-sm-7 col-md-7">
		    		
	            	
		    </div>
	    
            <div class="col-md-12 col-sm-6 col-md-6 form-group">
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
	        		<label><?php echo JrTexto::_('Role');?> </label>  
	        	</div>
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
            		<div class="col-md-12 col-sm-12 col-md-12 ">
	            		<div class="col-md-12 col-sm-12 col-md-12">
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Student');?></label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="cantidadalumnos" name="cantalumno" required="required" class="form-control" value="0" >
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Cost').':';?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="costoalumnos" name="costalumno" required="required" class="form-control" value="0" >
	            			</div>
	               			
	            		</div>
	              			

	                </div>
	                
            	</div>
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
            		<div class="col-md-12 col-sm-12 col-md-12 ">
	            		<div class="col-md-12 col-sm-12 col-md-12">
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Teacher');?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="cantidaddocente" name="cantdocente" required="required" class="form-control" value="0" >
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Cost').':';?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="costodocente" name="costdocente" required="required" class="form-control" value="0" >
	            			</div>
	               			
	            		</div>
	              			

	                </div>
            	</div>
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
            		<div class="col-md-12 col-sm-12 col-md-12 ">
	            		<div class="col-md-12 col-sm-12 col-md-12">
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Director');?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="cantidaddirector" name="cantddirector	" required="required" class="form-control" value="0" >
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Cost').':';?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="costodirector" name="costddirector	" required="required" class="form-control" value="0" >
	            			</div>
	               			
	            		</div>
	              			

	                </div>
            	</div>
                <div class="col-md-12 col-sm-12 col-md-12 form-group">
	        		<label><?php echo JrTexto::_('Download');?></label>  
	        	</div>
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
            		<div class="col-md-12 col-sm-12 col-md-12 ">
	            		<div class="col-md-12 col-sm-12 col-md-12">
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Workbooks');?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="cantidadalumnos" name="cantalumno" required="required" class="form-control" value="0" >
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Cost').':';?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="costoalumnos" name="costalumno" required="required" class="form-control" value="0" >
	            			</div>
	               			
	            		</div>
	              			

	                </div>
	                
            	</div>
            	<div class="col-md-12 col-sm-12 col-md-12 form-group">
            		<div class="col-md-12 col-sm-12 col-md-12 ">
	            		<div class="col-md-12 col-sm-12 col-md-12">
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('M.O.P.');?></label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="cantidaddocente" name="cantdocente" required="required" class="form-control" value="0" >
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3" style="text-align: right;padding-top: 5px;">
	            				<label><?php echo JrTexto::_('Cost').':';?> </label> 
	            			</div>
	            			<div class="col-md-12 col-sm-3 col-md-3">
	            				<input type="text"  id="costodocente" name="costdocente" required="required" class="form-control" value="0" >
	            			</div>
	               			
	            		</div>
	              			

	                </div>
            	</div>
            </div>
            <div class="col-md-12 col-sm-6 col-md-6 form-group" style="padding-top: 40px; ">
            	<div class="col-xs-12 col-sm-12 col-md-12">
                	<div class="col-xs-12 col-sm-12 col-md-12">
                		<div class="col-xs-12 col-sm-3 col-md-3">
                			<label><?php echo ucfirst(JrTexto::_("First Date")); ?></label>
                		</div>
                		<div class="col-xs-12 col-sm-9 col-md-9 form-group">
                			<div class="input-group date datetimepicker">
				              <input type="text" class="form-control" name="fechade" id="fechade<?php echo $idgui; ?>" value="<?php $hoy = date("Y-m-d");  echo ucfirst(@$hoy); ?>"> 
				              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				            </div>
                		</div>
			         <!--  <div class="form-group">
			          
			          <div class="form-group">
			            
			          </div>
			          </div> -->
			        </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                	<div class="col-xs-12 col-sm-12 col-md-12">
                		<div class="col-xs-12 col-sm-3 col-md-3">
                			<label><?php echo ucfirst(JrTexto::_("Finish Date")); ?></label>
                		</div>
                		<div class="col-xs-12 col-sm-9 col-md-9 form-group">
                			<div class="input-group date datetimepicker">
				              <input type="text" class="form-control" name="fechahasta" id="fechahasta<?php echo $idgui; ?>" value="<?php $hoy = date("Y-m-d");  echo ucfirst(@$hoy); ?>"> 
				              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				            </div>
                		</div>
			         <!--  <div class="form-group">
			          
			          <div class="form-group">
			            
			          </div>
			          </div> -->
			        </div>
                </div>
            </div>
            <div class="col-md-12 form-group text-center">
            	<hr>
              <button type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_empresas'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button> 
              <br><br>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
$('#fechade<?php echo $idgui; ?>').datetimepicker({
      //lang:'es',
      //timepicker:false,
      format:'YYYY/MM/DD'
  });            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'bolsa_empresas', 'saveBolsa_empresas', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_empresas"))?>');
      }
     }
  });

 
  
});


</script>

