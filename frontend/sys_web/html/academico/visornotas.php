<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
// $ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>

<style>
    .nav-pills a{border: 1px solid #337ab7; border-radius: 0.5em;}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">

<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Score'); ?></li>
    </ol>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel" >      
            <div class="panel-body">
                <form id="frmEstudiantes<?php echo $idgui; ?>">
                    <input type="hidden" name="plt" value="blanco">
                    <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
                    <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">
                    <input type="hidden" name="fkcbrol" value="3">
                    <input type="hidden" name="idgrupoauladetalle" id="idgrupoauladetalle" value="">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group" >
                            <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label> 
                            <div class="cajaselect">
                                <select name="dre" class="form-control fkcbdre">
                                    <?php 
                                    if(!empty($this->dress))
                                    foreach ($this->dress as $fk) { ?><option value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                            <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
                            <div class="cajaselect">
                                <select  name="ugel" class="form-control fkcbugel">
                                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group"  >
                            <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
                            <div class="cajaselect">
                                <select  name="iiee" class="form-control fkcbiiee">
                                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                                </select>
                            </div>
                        </div>
                            
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                            <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
                            <div class="cajaselect">
                                <select  name="fkcbcurso" class="form-control fkcbcurso">
                                <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                                    <?php if(!empty($this->fkcursos))
                                    foreach ($this->fkcursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                            <label><?php echo JrTexto::_('Grade'); ?></label>
                            <div class="cajaselect">
                                <select  name="fkcbgrado" class="form-control fkcbgrado" >
                                    <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                                    <?php 
                                    if(!empty($this->grados))
                                    foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" ><?php echo $r["abrev"]." : ".$r["descripcion"] ?></option>
                                    <?php } ?>                        
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                            <label><?php echo JrTexto::_('Section'); ?></label>
                            <div class="cajaselect">
                                <select  name="sexo" class="form-control fkcbseccion" >
                                    <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                                    <?php 
                                    if(!empty($this->seccion))
                                    foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" ><?php echo $s["descripcion"] ?></option>
                                    <?php } ?>                        
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr style="padding: 0px; margin: 1ex;">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 form-group">
          
                            <label><?php echo  ucfirst(JrTexto::_("Assigned teacher"))?></label>
                            <div class="input-group" style="margin:0px; ">
                                <input type="hidden" id="idteacher" name="idteacher" >
                                <input type="text" name="teachername" id="teachername" readonly="readonly" style="border: 1px solid #4683af;" class="form-control  teachername" placeholder="<?php echo  ucfirst(JrTexto::_("Name teacher"))?>">
                            </div>
                        </div>
                    <!--end row-->
                    </div>
                <!--end form-->
                </form>
            </div>
        <!--end panel-->
        </div>
    </div>
</div>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
    <div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="panel">
            <ul class="nav nav-pills">
                <li class="active"><a href="#bimestre" data-toggle="tab"><?php echo JrTexto::_("Bimonthly");?></a></li>
                <li><a href="#trimestre" data-toggle="tab"><?php echo JrTexto::_("Quarterly");?></a></li>
                <li><a href="#unidades" data-toggle="tab"><?php echo JrTexto::_("Unit");?></a></li>
            </ul>
            <div class="tab-content">
                <div id="bimestre" class="tab-pane fade in active">
                    <div id="tablanota-container" class="panel-body table-striped table-responsive">
                        <table class="table " id="tablanota" style="min-width:100%">
                            <thead>
                                <tr class="headings">
                                    <th>#</th>
                                    <th><?php echo JrTexto::_("Student") ;?></th>
                                    <th><?php echo JrTexto::_("Bimonthly")." 1" ;?></th>
                                    <th><?php echo JrTexto::_("Bimonthly")." 2" ;?></th>
                                    <th><?php echo JrTexto::_("Bimonthly")." 3" ;?></th>
                                    <th><?php echo JrTexto::_("Bimonthly")." 4" ;?></th>
                                    <th><?php echo JrTexto::_("Final Test");?></th>
                                    <th><?php echo JrTexto::_("Average Score") ;?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="trimestre" class="tab-pane fade">
                    <div id="tablanota2-container" class="panel-body table-striped table-responsive">
                        <table class="table " id="tablanota2" style="min-width:100%">
                            <thead>
                                <tr class="headings">
                                    <th>#</th>
                                    <th><?php echo JrTexto::_("Student") ;?></th>
                                    <th><?php echo JrTexto::_("Quarterly")." 1" ;?></th>
                                    <th><?php echo JrTexto::_("Quarterly")." 2" ;?></th>
                                    <th><?php echo JrTexto::_("Quarterly")." 3" ;?></th>
                                    <th><?php echo JrTexto::_("Final Test");?></th>
                                    <th><?php echo JrTexto::_("Average Score") ;?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="unidades" class="tab-pane fade">
                    <div id="tablanota3-container" class="panel-body table-striped table-responsive">
                        <table class="table " id="tablanota3" style="min-width:100%">
                            <thead>
                                <tr class="headings">
                                    <th>#</th>
                                    <th><?php echo JrTexto::_("Student") ;?></th>
                                    <th><?php echo JrTexto::_("unit")." 1" ;?></th>
                                    <th><?php echo JrTexto::_("unit")." 2" ;?></th>
                                    <th><?php echo JrTexto::_("unit")." 3" ;?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>


<script type="text/javascript">
var tabledatos5bd4788e236d6='';

function dibujarDatatable(tablename){
    if ($.fn.DataTable.isDataTable(tablename)) {
        $(tablename).DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $(tablename).DataTable({
        "pageLength": 50,
        "searching": false,
        "processing": false
    });
}
function searchresultUnit(){
    $.ajax({
        url: _sysUrlBase_+'/reportes/notasunidadmatricula',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoauladetalle': $('#idgrupoauladetalle').val(),'idcurso' : $('.fkcbcurso').val()}
    }).done(function(resp){
        if(resp.code=='ok'){
            var objeto = resp.data;
            var row = '<tr>';
            var headers = '<th>#</th><th>\<?php echo JrTexto::_("Student")?></th>';
            var body = '';
            if(Object.keys(objeto).length != 0){
                var cantidad = Object.keys(objeto[0].notasunidad).length;
                for(var i = 1; i <= cantidad; i++){
                    headers = headers.concat('<th>\<?php echo JrTexto::_("Unit")?>'+i+'</th>');
                }
                objeto.forEach(function(elemento,indice){
                    row = '<tr><td>'+(indice+1)+'</td><td>'+elemento.nombrealumno+'</td>';
                    elemento.notasunidad.forEach(function(e,i){
                        row = row.concat('<td>'+e.nota+'</td>');
                    });
                    row = row.concat('</tr>');
                    body = body.concat(row);
                });
                var tmp3 = $('<table></table>');
                tmp3.attr('id','tablanota3');
                tmp3.addClass($('#tablanota3').attr('class'));
                tmp3.append('<thead><tr class="headings">'+headers+'</tr></thead><tbody>'+body+'</tbody>');
                $('#tablanota3_wrapper').remove();
                $('#tablanota3-container').append(tmp3);
                dibujarDatatable('#tablanota3');
            }
        }else{
            alert(resp.msj);
        }
    }).fail(function(xhr){
        alert(xhr.responseText);
    });
}
var searchresult = function(isTri = null){
    var _data = (isTri != null) ? {'trimestre':isTri,'idgrupoauladetalle': $('#idgrupoauladetalle').val()} : {'idgrupoauladetalle': $('#idgrupoauladetalle').val()};
    $.ajax({
        url: _sysUrlBase_+'/reportes/notasmatricula',
        type: 'POST',
        dataType: 'json',
        data: _data
    }).done(function(resp) {
        if(resp.code=='ok'){
            var ide = 'tablanota';
            if(isTri == true){
                ide = 'tablanota2';
            }
            var tmp = $('<table></table>');
            tmp.attr('id',ide);
            tmp.addClass($('#'+ide).attr('class'));
            tmp.append('<thead>'+$('#'+ide).find('thead').html()+'</thead><tbody></tbody>');
            if(Object.keys(resp.data).length > 0){
                for(var i in resp.data){
                    var fila = '';
                    if(typeof resp.data[i] == 'object'){
                        var index =  parseInt(i) + 1;
                        if(isTri == true){
                            fila = '<tr><td>'+(index)+'</td><td>'+resp.data[i].nombrealumno+'</td><td>'+resp.data[i].examen_T1+'</td><td>'+resp.data[i].examen_T2+'</td><td>'+resp.data[i].examen_T3+'</td><td>'+resp.data[i].examen_salida+'</td><td>'+resp.data[i].promedio_T+'</td></tr>';
                        }else{
                            fila = '<tr><td>'+(index)+'</td><td>'+resp.data[i].nombrealumno+'</td><td>'+resp.data[i].examen_B1+'</td><td>'+resp.data[i].examen_B2+'</td><td>'+resp.data[i].examen_B3+'</td><td>'+resp.data[i].examen_B4+'</td><td>'+resp.data[i].examen_salida+'</td><td>'+resp.data[i].promedio_B+'</td></tr>';
                        }
                        tmp.find('tbody').append(fila);
                    }
                }//end for
            }//end object keys
            
            $('#'+ide+'_wrapper').remove();
            $('#'+ide+'-container').append(tmp);
            dibujarDatatable('#'+ide);
        }else {
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
};



var buscargrupoauladetalle<?php echo $idgui; ?>=function(){
    var fd2= new FormData();
    var idiiee=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbiiee').val()||-1;
    var idcurso=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbcurso').val()||-1;
    var idgrado=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbgrado').val()||-1;
    var idsecion=$('#frmEstudiantes<?php echo $idgui;?> select.fkcbseccion').val()||-1;
    fd2.append("idcurso", idcurso);
    fd2.append("idlocal", idiiee);
    fd2.append("idgrado", idgrado);
    fd2.append("idsesion", idsecion);
    fd2.append("sql2", true);
    sysajax({
        fromdata:fd2,
        url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
        callback:function(rs){
        if(rs.code=='ok'||rs.code==200){
            var dt=rs.data[0]||-1;
            console.log(dt.idgrupoauladetalle);
            if(dt!=-1){
                $('.idteacher').val(dt.iddocente);
                $('.teachername').val(dt.strdocente);
                $('#idgrupoauladetalle').val(dt.idgrupoauladetalle);
                if(typeof dt.idgrupoauladetalle != 'undefined'){
                    searchresult();
                    searchresult(true);
                    searchresultUnit();
                }
            }else{
                // $('#tablanota').find('tbody').html(' ');
                var tmp = $('<table></table>');
                var tmp2 = $('<table></table>');
                var tmp3 = $('<table></table>');
                tmp.attr('id','tablanota');
                tmp.addClass($('#tablanota').attr('class'));
                tmp.append('<thead>'+$('#tablanota').find('thead').html()+'</thead><tbody></tbody>');
                
                tmp2.attr('id','tablanota2');
                tmp2.addClass($('#tablanota2').attr('class'));
                tmp2.append('<thead>'+$('#tablanota2').find('thead').html()+'</thead><tbody></tbody>');
                
                tmp3.attr('id','tablanota3');
                tmp3.addClass($('#tablanota3').attr('class'));
                tmp3.append('<thead>'+$('#tablanota3').find('thead').html()+'</thead><tbody></tbody>');

                $('#tablanota_wrapper').remove();
                $('#tablanota-container').append(tmp);

                $('#tablanota2_wrapper').remove();
                $('#tablanota2-container').append(tmp2);

                $('#tablanota3_wrapper').remove();
                $('#tablanota3-container').append(tmp3);

                dibujarDatatable('#tablanota');
                dibujarDatatable('#tablanota2');
                dibujarDatatable('#tablanota3');

                $('.idteacher').val('');
                $('.teachername').val('');
                $('#idgrupoauladetalle').val('');
            }
        }
        // refreshdatos5bd4788e236d6();
        }
    })
}

$(document).ready(function(){
    dibujarDatatable('#tablanota');
    dibujarDatatable('#tablanota2');
    dibujarDatatable('#tablanota3');
    $('#frmEstudiantes<?php echo $idgui;?>').on('change','select.fkcbdre',function(ev){        
        ev.preventDefault();
        var iddre=$(this).val();
        var fd2= new FormData();
        fd2.append("iddepartamento", iddre);
        sysajax({
            fromdata:fd2,
            url:_sysUrlBase_+'/ugel/buscarjson',
            callback:function(rs){
                $sel=$('select.fkcbugel');
                dt=rs.data;
                $sel.html('');
                $.each(dt,function(i,v){$sel.append('<option value="'+v.idugel+'" >'+v.descripcion+'</option>'); })
                $sel.trigger("change");
                
            }
        });

    }).on('change','select.fkcbugel',function(ev){
        ev.preventDefault();
        var idugel=$(this).val();
        var fd2= new FormData();
        fd2.append("idugel", idugel);
        sysajax({
        fromdata:fd2,
        url:_sysUrlBase_+'/local/buscarjson',
        callback:function(rs){
            $sel=$('select.fkcbiiee');
            dt=rs.data;
            $sel.html('');
            $.each(dt,function(i,v){
              $sel.append('<option value="'+v.idlocal+'" >'+v.nombre+'</option>');
            })
            $sel.trigger("change");
        }});
    }).on('change','select.fkcbiiee, select.fkcbcurso , select.fkcbgrado, select.fkcbseccion',function(ev){
         buscargrupoauladetalle<?php echo $idgui; ?>();
    });
    $('#frmEstudiantes<?php echo $idgui;?> select.fkcbdre').trigger("change");
});
</script>