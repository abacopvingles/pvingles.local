<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$grupo=!empty($this->datos)?$this->datos:"";
$grupodetalle=!empty($this->datosgrupodetalle)?$this->datosgrupodetalle:"";
//var_dump($grupodetalle);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .titulo{
    font-weight: bold;
    font-size: 1.3em;
  }
  hr{
    margin-top: 0px;
    margin-bottom: 1ex;
    border: 0;
    border-top: 1px solid #00BCD4;
  }
</style>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Grupos de estudio cursos/horarios"); ?></li>
    </ol>
  </div>
</div>
<div class="row" >
  <div class="col-xs-12">
    <div class="panel" >      
      <div class="panel-body">
        <div class="col-md-12">
          <form id="frmgrupoaula<?php echo $idgui; ?>" method="POST">
          <input type="hidden" name="idgrupoaula" class="idgrupoaula" id="idgrupoaula<?php echo $idgui; ?>" value="<?php echo @$grupo["idgrupoaula"]; ?>">
          <input type="hidden" name="accion" id="acciongrupoaula<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
          <input type="hidden" name="idgrupoauladetalle" class="idgrupoauladetalle" id="idgrupoauladetalle<?php echo $idgui; ?>" value="<?php echo @$grupo["idgrupoauladetalle"]; ?>">
          <input type="hidden" name="acciongrupoauladetalle" id="acciongrupoauladetalle<?php echo $idgui; ?>" value="<?php echo @$this->acciongrupodetalle; ?>">
          
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> </label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                    <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Grupos")); ?> </label>
              <div class="cajaselect">
                <select name="grupos" id="idgrupoaula<?php echo $idgui;?>" class="form-control">                  
                  <?php if(!empty($this->grupos)) foreach ($this->grupos as $fk) { ?>
                    <option value="<?php echo $fk["idgrupoaula"]?>" <?php echo $fk["idgrupoaula"]==@$this->idgrupo?'selected="selected"':'';?> ><?php echo $fk["nombre"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Local")); ?> </label>
              <div class="cajaselect">
                <select name="local" id="local<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                   
                  <?php if(!empty($this->fklocales)) foreach ($this->fklocales as $fk) { ?>
                    <option value="<?php echo $fk["idlocal"]?>" <?php echo $fk["idlocal"]==@$this->fkidlocal?'selected="selected"':'';?> ><?php echo $fk["nombre"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Ambiente")); ?> </label>
              <div class="cajaselect">
                <select name="ambiente" id="ambiente<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fkambientes)) foreach ($this->fkambientes as $fk) { ?>
                    <option value="<?php echo $fk["idambiente"]?>" <?php echo $fk["idambiente"]==@$this->fkidambiente?'selected="selected"':'';?> ><?php echo $fk["numero"].' '.$fk["tipo_ambiente"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>"">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Docente")); ?> </label>
              <div class="cajaselect">
                <select name="docente" id="docente<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fkdocentes)) foreach ($this->fkdocentes as $fk) { ?>
                    <option value="<?php echo $fk["idpersonal"]?>" <?php echo $fk["idpersonal"]==@$this->fkidambiente?'selected="selected"':'';?> ><?php echo $fk["nombre_full"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 notypev<?php echo $idgui; ?>"">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Curso")); ?> </label>
              <div class="cajaselect">
                <select name="curso" id="curso<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>
                  <?php if(!empty($this->fkcursos)) foreach ($this->fkcursos as $fk) { ?>
                    <option value="<?php echo $fk["idcurso"]?>"  >
                      <?php echo $fk["nombre"]; ?>
                      </option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <!--div class="col-xs-12 col-sm-6 col-md-3 text-center"><br>
          <button id="reloadcalendar<?php echo $idgui; ?>" class="btn btn-success"><i class="fa fa-refresh"></i> <?php echo JrTexto::_('Reload calendar') ?></button>
       
          </div-->

         
          
          <!--div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Codigo")); ?></label>             
                <input type="text" name="nombre" id="nombre<?php echo $idgui; ?>" requerid data-value="<?php echo @$grupo["nombre"]; ?>" value="<?php echo @$grupo["nombre"]; ?>" class="form-control border0 nombregrupoaula" placeholder="<?php echo  date('Y')." - 01"?>">               
              </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <label>N° <?php echo ucfirst(JrTexto::_("Vacantes")); ?></label>             
                <input type="number" name="nvacantes" id="nvacantes<?php echo $idgui; ?>" data-value="<?php echo @$grupo["nvacantes"]; ?>" value="<?php echo @$grupo["nvacantes"]; ?>" class="form-control border0 nvacantesgrupoaula" placeholder="30">
              </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Estado")); ?> :</label>
                <div class="cajaselect">
                  <select name="estado" id="estado<?php echo $idgui;?>" class="form-control" data-value="<?php echo @$grupo["estado"] ?>">                  
                    <?php if(!empty($this->fkestadogrupos)) foreach ($this->fkestadogrupos as $fk){ ?>
                      <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fkidestadogrupo?'selected="selected"':'';?> ><?php echo ucfirst(JrTexto::_($fk["nombre"])); ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
          </div-->          
          </form>
          <div class="clearfix"></div>
        </div>        
      </div>
    </div>
  </div>
</div>
<div class="col-md-12" id="pnlvista<?php echo $idgui; ?>">

</div>
<button style="display: none;" class="tmpbtnrecargacal<?php echo $idgui; ?>"></button>
<script type="text/javascript">
	$(document).ready(function(){
    var idproyecto=parseInt('<?php echo $this->idproyecto;?>');
    var cargadocentes<?php echo $idgui ?>=function(){     
      var formData = new FormData();
      formData.append('rol',2);
      formData.append('idproyecto',idproyecto);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/personal/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok:false,
        callback:function(rs){
          dt=rs.data;
          //$('#docente<?php //echo $idgui;?> option:first-child').siblings('option').remove();
          $.each(dt,function(i,v){
            var option='<option value="'+v.idpersona+'">'+v.ape_paterno+' '+v.ape_materno+' '+v.nombre+'</option>';
            $('#docente<?php echo $idgui;?>').append(option);
          })   
        }
      }
      sysajax(data);
    }
    cargadocentes<?php echo $idgui ?>();
    var cargarvista<?php echo $idgui; ?>=function(){
        var tipo=$('#tipo<?php echo $idgui;?>').val();
        var idgrupoaula=$('#idgrupoaula<?php echo $idgui;?>').val();
        if(tipo=='V'){
            var formData = new FormData();
            formData.append('idgrupoaula',idgrupoaula);
            sysajax({
                fromdata:formData,
                url:_sysUrlBase_+'/acad_grupoaula/cursosvirtuales',
                type:'html',
                callback:function(rs){
                    $('#pnlvista<?php echo $idgui; ?>').html(rs);          
                }
            });
        }else{
          var frmtmp=document.getElementById('frmgrupoaula<?php echo $idgui; ?>');
          var formData = new FormData(frmtmp);     
          formData.append('idlocal',$('#local<?php echo $idgui; ?>').val());
          formData.append('iddocente',$('#docente<?php echo $idgui; ?>').val());
          formData.append('idcurso',$('#curso<?php echo $idgui; ?>').val());
          formData.append('strlocal',$('#local<?php echo $idgui; ?> option:selected').text().trim());
          formData.append('strdocente',$('#docente<?php echo $idgui; ?> option:selected').text().trim());
          formData.append('strcurso',$('#curso<?php echo $idgui; ?> option:selected').text().trim());
          formData.append('idgrupoaula',$('select#idgrupoaula<?php echo $idgui; ?>').val()); 
          formData.append('fcall','btnrecargacal<?php echo $idgui; ?>'); 
          formData.append('plt','blanco');
          var data={
            fromdata:formData,
            url:_sysUrlBase_+'/acad_grupoaula/horarios',
            msjatencion:'Attention',
            type:'html',
            callback:function(rs){
            $('#pnlvista<?php echo $idgui; ?>').html(rs);          
            }
          }
          sysajax(data);
        }
    }
    $('Select#idgrupoaula<?php echo $idgui;?>').on('change',function(){
      cargarvista<?php echo $idgui; ?>();
    });

    $('.tmpbtnrecargacal<?php echo $idgui; ?>').click(function(ev){
      var tmpdata=JSON.parse($(this).attr('data-return'));
      $('#idgrupoaula<?php echo $idgui; ?>').val(tmpdata.idgrupoaula);
      $('#curso<?php echo $idgui; ?>').val(tmpdata.idcurso);
      $('#idgrupoaula<?php echo $idgui; ?>').val(tmpdata.idgrupoaula);
      $('#docente<?php echo $idgui; ?>').val(tmpdata.iddocente);
      $('#local<?php echo $idgui; ?>').val(tmpdata.idlocal); 
      $('#nombre<?php echo $idgui; ?>').val(tmpdata.nombreaula); 
      cargarvista<?php echo $idgui; ?>();  
    });

    $('#reloadcalendar<?php echo $idgui; ?>').click(function(ev){
      cargarvista<?php echo $idgui; ?>();
    })

    var mostrarsave<?php echo $idgui; ?>=function(){
     
    } 
    

    var initcarga<?php echo $idgui ?>=function(){
      $('#nombre<?php echo $idgui; ?>').attr('data-value',$('#nombre<?php echo $idgui; ?>').val());
      $('#nvacantes<?php echo $idgui; ?>').attr('data-value',$('#nvacantes<?php echo $idgui; ?>').val());
      $('#tipo<?php echo $idgui; ?>').attr('data-value',$('#tipo<?php echo $idgui; ?>').val());
      $('#estado<?php echo $idgui; ?>').attr('data-value',$('#estado<?php echo $idgui; ?>').val());
      $('#comentario<?php echo $idgui; ?>').attr('data-value',$('#comentario<?php echo $idgui; ?>').val());
      mostrarsave<?php echo $idgui; ?>();
      var tmpv=$('#tipo<?php echo $idgui;?>').val().toString().trim();
      if(tmpv!='V') $('.notypev<?php echo $idgui; ?>').show();
      else  $('.notypev<?php echo $idgui; ?>').hide();

      cargarvista<?php echo $idgui; ?>();
    }

    initcarga<?php echo $idgui ?>();
 
    $('#showcomentario<?php echo $idgui; ?>').click(function(){
      $(this).toggleClass('active');
      if($(this).hasClass('active')){
        $('i',this).removeClass('fa-eye-slash').addClass('fa-eye');
        $('span',this).text($(this).attr('data-show'));
        $(this).siblings('textarea').hide();
      }else{
        $('i',this).removeClass('fa-eye').addClass('fa-eye-slash');
        $('span',this).text($(this).attr('data-hide'));
        $(this).siblings('textarea').show();
      }      
    })

    $('#btnreset<?php echo $idgui; ?>').click(function(ev){      
      $(this).closest('#savegrupoaula<?php echo $idgui; ?>').hide();
      setTimeout(function(){ $('#tipo<?php echo $idgui;?>').trigger('change'); },300);
    });

    $('#frmgrupoaula<?php echo $idgui; ?>').submit(function(){
      guardardatosdatosaula<?php echo $idgui; ?>();
      return false;
    });

    $('select#tipo<?php echo $idgui;?>').change(function(ev){
        ev.preventDefault();
        var tmpv=$(this).val().toString().trim();
        if(tmpv!='V') $('.notypev<?php echo $idgui; ?>').show();
        else  $('.notypev<?php echo $idgui; ?>').hide();        
        var formData = new FormData();
        formData.append('idproyecto',idproyecto);
        formData.append('tipo',tmpv);
        sysajax({
        fromdata:formData,
        url:_sysUrlBase_+'/acad_grupoaula/buscargruposjson',       
        type:'json',
        showmsjok:false,
        callback:function(rs){ 
         let dt=rs.data;
           $('#idgrupoaula<?php echo $idgui;?> option').remove();
           let options='';
           $.each(dt,function(i,d){
             options+='<option value="'+d.idgrupoaula+'">'+d.nombre+'</option>';
           })           
           $('select#idgrupoaula<?php echo $idgui;?>').html(options);
           $('select#idgrupoaula<?php echo $idgui;?>').trigger('change');

        }
      })
    })

    $('#local<?php echo $idgui;?>').change(function(ev){ 
      var vtmp_=$(this).val().toLowerCase();
      var ambtmp=$('select#ambiente<?php echo $idgui;?>');
      if(vtmp_==''){
        $('#ambiente<?php echo $idgui;?> option:first-child').siblings().remove();
        ambtmp.val('');
        cargarvista<?php echo $idgui;?>();
        return false;
      }else{
        var formData = new FormData();
        formData.append('idlocal',vtmp_);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/ambiente/buscarjson',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          callback:function(rs){
            var tr=rs.data;              
            $('option:first',ambtmp).siblings().remove();
            if(tr!=''){
                $.each(tr,function(i,v){                    
                  ambtmp.append('<option value="'+v['idambiente']+'">'+v["numero"]+' '+v['tipo_ambiente']+'</option>');
              });
            }
            cargarvista<?php echo $idgui;?>();
          }
        }
        sysajax(data);
        return false;
      }
    });
    $('#docente<?php echo $idgui;?>').change(function(ev){ cargarvista<?php echo $idgui; ?>(); });
    $('#curso<?php echo $idgui;?>').change(function(ev){ cargarvista<?php echo $idgui; ?>(); });
      var guardardatosdatosaula<?php echo $idgui; ?>=function(){
      var frmtmp=document.getElementById('frmgrupoaula<?php echo $idgui; ?>');
      var formData = new FormData(frmtmp);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_grupoaula/guardarAcad_grupoaula',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok:true,
        callback:function(rs){ 
          $('#idgrupoaula<?php echo $idgui;?>').val(rs.newid);
          $('#acciongrupoaula<?php echo $idgui; ?>').val('edit'); 
          initcarga<?php echo $idgui ?>();         
        }
      }
      sysajax(data);
      return false;
    }
});
</script>