<?php 
  $idgui = uniqid();
  defined("RUTA_BASE") or die(); 
  $ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
  $ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
	$datos=$this->datos;	
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('Course'); ?></li>      
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id='ventana<?php echo $idgui; ?>'>
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">                   
            <div class="col-xs-6 col-sm-4 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 ">              
              <div class="select-ctrl-wrapper select-azul">
                <select name="cbestado" id="cbestado<?php echo $idgui; ?>" class="form-control select-ctrl">
                    <!--option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option-->
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 text-center">
               <a class="btn btn-success btnagregar<?php echo $idgui; ?> btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("acad_curso", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Course").' - '.JrTexto::_("Add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
            </div>
          </div>
        </div>
    </div>
  </div>
	<div class="col-md-12 col-sm-12 col-xs-12" id="datalistado<?php echo $idgui; ?>">
		<div id="cargando"><?php echo JrTexto::_('loading') ?></div>
	    <div id="sindatos" style="display: none;">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, we could not find data')?>. <br><br>
                <a class="btn btn-success btn-lg btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("acad_curso", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Course").' - '.JrTexto::_("Add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>   
                </div>              
            </div>
        </div>
      </div>
      <div id="error" style="display: none;">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron panel-danger">
                <h1><strong>Error!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, data no found')?>.<br>

                </div>              
            </div>
        </div>
      </div>
	    <div id="data">data aqui</div>
	</div>
	<div id="controles<?php echo $idgui; ?>" style="display: none;">
		<div class="pnlacciones text-center">
      <?php if($ismodal){?>
      <a class="btn-selected btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected')).' '.JrTexto::_('Course'); ?>"><i class="fa fa-hand-o-down"></i></a><?php } ?>
			<a class="btn-editar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Edit')).' '.JrTexto::_('Course'); ?>"><i class="fa fa-pencil"></i></a> 
			<a class="btn-eliminar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Course'); ?>"><i class="fa fa-trash-o"></i></a>

		</div>
	</div>
</div>
<script type="text/javascript">
var tabledatos<?php echo $idgui; ?>='';
var ventanacur='#ventana<?php echo $idgui; ?>';
var ventanaold=ventanacur;

function checkdata<?php echo $idgui; ?>(){
  $datapnl=$('#datalistado<?php echo $idgui; ?> #data');
  if($datapnl.find('.mg1').length==0){
    $datapnl.hide(0);
    $datapnl.siblings('#sindatos').css('display','block').animateCss('zoomIn');
  }
}
function refreshdatos<?php echo $idgui; ?>(){
    tabledatos<?php echo $idgui; ?>();
}

function changevista(vista){
  sessionStorage.setItem(vista, vista);
}
function changeventana(ventana){  
  ventanaold=ventanacur;
  ventanacur=ventana;
  $(ventanaold).hide();
  $(ventanacur).css('display','block').animateCss('zoomInUp');
}

function checkdata<?php echo $idgui; ?>(){
  $datapnl=$('#datalistado<?php echo $idgui; ?> #data');
  if($datapnl.find('.mg1').length==0){
    $datapnl.hide(0);
    $sindatos=$datapnl.siblings('#sindatos');
    $sindatos.css('display','block').animateCss('zoomIn');
    $('.btnagregar<?php echo $idgui; ?>').hide();
  }else{
    $('.btnagregar<?php echo $idgui; ?>').show('fast');
  }
}
$(document).ready(function(){
  $('#datalistado<?php echo $idgui; ?>').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  }).on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  }).on('click','.btnvermodal',function(e){
      e.preventDefault();
      e.stopPropagation();
      var enmodal=$(this).attr('data-modal')||'no';
      var fcall=$(this).attr('data-fcall')||'refreshdatos<?php echo $idgui; ?>';
      var url=$(this).attr('href')
      if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
      else url+='?fcall='+fcall;
      var ventana=$(this).data('ventana')||'Personal';
      var claseid=ventana+'_<?php echo $idgui; ?>';
      var titulo=$(this).attr('data-titulo')||'';
      titulo=titulo.toString().replace('<br>',' ');     
      if(enmodal=='no'){
        return redir(url);         
      }
      url+='&plt=modal';
      $idtmp='pnl2_'+Date.now();
      var pd=$('#ventana<?php echo $idgui; ?>').parent();
      pd.append('<div id="'+$idtmp+'"></div>');
      donde = $('#'+$idtmp);
      openModal('lg',titulo,url,ventana,claseid); 
  }).on('click','.btn-editar',function(ev){
    var id=$(this).closest('.panel-user').attr('data-id');
    var url=_sysUrlBase_+'/acad_curso/editar/?id='+id;
    redir(url);
  }).on('click','.btn-eliminar',function(ev){
      var pnl=$(this).closest('.panel-user')
      var id=pnl.attr('data-id');
      var formData = new FormData();
      formData.append('idcurso', id);
      var url=_sysUrlBase_+'/acad_curso/eliminarcurso';  
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        dataType:'json',
        cache: false,
        beforeSend: function(XMLHttpRequest){ },      
        success: function(data)
        {       
          if(data.code==='ok'){
            var pnlremove=pnl.closest('.mg1');
                pnlremove.animateCss('zoomOut',function(){ pnlremove.remove(); checkdata<?php echo $idgui; ?>(); });
          }else{
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          }
        },
        error: function(e){ },
        complete: function(xhr){ }
      });        
  }).on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'personal', 'setCampo', id,campo,data);
          if(res) refreshdatos<?php echo $idgui; ?>()
        }
      });
  }).on('click','.btn-selected',function(ev){
    var id=$(this).closest('.panel-user').attr('data-id');
    var titulo=$(this).closest('.panel-user').find('.titulo').text().trim();   
    <?php if($ismodal){
      if(!empty($ventanapadre)){
    ?>
    var datareturn={id:id,nombre:titulo}; 
    var tmpobj=$('.tmp<?php echo $ventanapadre ?>')
    if(tmpobj.length){
      tmpobj.attr('data-return',JSON.stringify(datareturn));
      tmpobj.on('returndata').trigger('returndata');
    }    
    <?php } ?>
    $(this).closest('.modal').modal('hide');
    <?php } ?>  
  })
  var estados<?php echo $idgui; ?>={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}
  var tituloedit<?php echo $idgui; ?>='<?php echo ucfirst(JrTexto::_("Course"))." - ".JrTexto::_("edit"); ?>';

  tabledatos<?php echo $idgui; ?>=function(){
    $datapnl=$('#datalistado<?php echo $idgui; ?>');
    var formData = new FormData();
        formData.append('nombre', $('#texto<?php echo $idgui; ?>').val());
        formData.append('estado', $('#cbestado<?php echo $idgui; ?>').val());
    $.ajax({
      url: _sysUrlBase_+'/acad_curso/buscarjson/',
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      beforeSend: function(XMLHttpRequest){
        $datapnl.find('#cargando').siblings('div').hide(0);
        $datapnl.find('#cargando').css('display','block').animateCss('zoomIn');
      },      
      success: function(res)
      {     
        if(res.code==='ok'){
          var midata=res.data;
          if(midata){
            var html='';
            var controles=$('#controles<?php echo $idgui; ?>').html();
            $.each(midata,function(i,v){
              var urlimg=_sysUrlStatic_+'/media/imagenes/cursos/';
              if(_sysfileExists(_sysUrlBase_+v.imagen))	urlimg=_sysUrlBase_+v.imagen;
              else urlimg+='nofoto.jpg';             
              html+='<div class="col-md-3 col-sm-4 col-xs-12 mg1"><div class="panel-user hvr-grow" data-id="'+v.idcurso+'">'+controles;
              html+='<div class="item"><img class="img-responsive" src="'+urlimg+'" width="100%">'; 
              html+='<div class="texto"><div class="titulo">'+v.nombre+'</div>';
              html+='<div class="infor"><strong>Total: </strong><span> '+v.nunidad+' </span>Units</div> <div class="infor" ><strong>Total:</strong><span>'+v.nactividad+' </span> Activities</div>';
              html+='</div></div></div></div>';
            });
            $datapnl.find('#cargando').hide(0);
            $datapnl.find('#data').html(html).css('display','block').animateCss('zoomIn');
            checkdata<?php echo $idgui; ?>();
          }else{
            $datapnl.find('#cargando').hide(0);
            $datapnl.find('#sindatos').css('display','block').animateCss('bounceIn');
          }   
         
        }else{
          $datapnl.find('#cargando').siblings('div').hide(0);
           $datapnl.find('#error').css('display','block').animateCss('flash');
        }
      },
      error: function(e) 
      {
          $datapnl.find('#cargando').siblings('div').hide(0);
           $datapnl.find('#error').css('display','block').animateCss('flash');
      }      
    });
  };
  tabledatos<?php echo $idgui; ?>(); 
  $('#ventana<?php echo $idgui; ?>')
  .on('change','#cbestado<?php echo $idgui; ?>',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('click','.btnbuscar',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('keyup','#texto<?php echo $idgui; ?>',function(ev){
    if(ev.keyCode == 13)
    refreshdatos<?php echo $idgui; ?>();
  });
});
</script>