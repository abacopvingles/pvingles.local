<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->mismarcaciones)) $marcaciones=$this->mismarcaciones;
//var_dump($this->mismarcaciones);
?>
<style type="text/css">
  #panel<?php echo $idgui; ?>{

  }
  #btnadd<?php echo $idgui; ?>{
    position: absolute;
    right: 2em;
    top:1ex;
    z-index: 2;
  }
</style>
<div class="col-md-12">
<div class="panel">
	<div class="panel-body" id="panel<?php echo $idgui; ?>" >
    <!--a href="<?php //echo $this->documento->getUrlBase(); ?>/acad_grupoaula/formulario" id="btnadd<?php //echo $idgui; ?>" class="btn btn-warning btn-xs"><i class="fa fa-plus"></i> <?php //echo JrTexto::_('Add') ?></a-->
<table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
        	<th><?php echo ucfirst(JrTexto::_("Teacher")) ;?></th>
          <th><?php echo ucfirst(JrTexto::_("Course")); ?></th>
        	<th><?php echo ucfirst(JrTexto::_("Ambient")) ;?></th>
          <th><?php echo ucfirst(JrTexto::_("Date")) ;?></th>
          <th><?php echo ucfirst(JrTexto::_("Entry hour")) ;?></th>
          <th><?php echo ucfirst(JrTexto::_("Departure time"));?></th>  
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
  		$url=$this->documento->getUrlBase();
  		if(!empty($marcaciones))
  			foreach ($marcaciones as $rw){ 
  					$i++;  					
  					?>
  				<tr>
  					<td><?php echo $i; ?></td>
  					<td><?php echo $rw["strdocente"];?></td>
            <td><?php echo $rw["strcurso"];?></td>  					
  					<td><?php echo $rw["strlocal"]."<br>".$rw["strambiente"]; ?></td>		
            <td><?php echo $rw["fecha"]; ?></td>
            <td><?php echo $rw["horaingreso"];?></td>
            <td><?php echo $rw["horasalida"]=='00:00:00'?'-':$rw["horasalida"];?></td>
  					
  				</tr>
  		<?php }	?>
    </tbody>
</table>
	</div>       
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	
})
</script>