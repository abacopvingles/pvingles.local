<style css>
  @media (max-width:768px){
    thead,tbody,.cthead,.ctbody {font-size: small!important;}
    .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th { white-space:initial!important; }
  }
</style>
<div class="container">
    <br> 
    <div class="panel panel-primary">
        <div class="panel-heading"><p>Question</p></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3><?php echo JrTexto::_('Course'); ?></h3>
                    <select id="curso">
                        <?php 
                        if(!empty($this->cursos)){
                            foreach($this->cursos as $v){
                                echo "<option value='{$v['idcurso']}'>{$v['nombre']}</option>";
                            }
                        }
                        ?>
                    </select>
                    <hr/>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table id="exam" class="table table-stripped">
                            <thead>
                                <th>id</th>
                                <th><?php echo JrTexto::_("Exams"); ?></th>
                                <th>Total</th>
                                <th>Listen</th>
                                <th>Read</th>
                                <th>Write</th>
                                <th>Speak</th>
                            </thead>
                            <tbody>
                                <!--data dinamica-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var exam = <?php echo (!empty($this->examen)) ? json_encode($this->examen) : 'null' ?>;
$(document).ready(function(){
    $('#curso').change(function(){
        var id = $(this).val();
        var total = exam;
        $.ajax({
            url: _sysUrlBase_+'/academico/json_preguntas_examen',
            type: 'POST',
            dataType: 'json',
            data: {'idcurso': id},
        }).done(function(resp){
            if(resp.code=='ok'){
                exam = resp.data;
                console.log(resp);
                $('table tbody').html(' ');
                var option = '';
                exam.forEach(function(e,i){
                    console.log(e.total);   
                    option = option.concat('<tr>');
                    option = option.concat('<td>'+ (i+1) +'</td>');
                    option = option.concat('<td>'+ e.examen +'</td>');
                    option = option.concat('<td>'+ (parseInt(e.general.L)  + parseInt(e.general.R) + parseInt(e.general.W) + parseInt(e.general.S)) +'</td>');
                    option = option.concat('<td>'+ e.general.L +'</td>');
                    option = option.concat('<td>'+ e.general.R +'</td>');
                    option = option.concat('<td>'+ e.general.W +'</td>');
                    option = option.concat('<td>'+ e.general.S +'</td>');

                    option = option.concat('<tr>');

                });
                $('table tbody').append(option);
            }else{
                return false;
            }
        }).fail(function(xhr, textStatus, errorThrown){
            return false;
        });
    });
    $('#curso').trigger('change');
});
</script>