<?php $usuarioAct = NegSesion::getUsuario(); ?>
<style type="text/css">
	.slick-slide{
		position: relative;
	}

.cajaselect {  
   overflow: hidden;
   width: 230px;
   position:relative;
   font-size: 1.8em;
}
select#level-item {
   background: transparent;
   border: 2px solid #4683af;   
   padding: 5px;
   width: 250px;
   padding: 0.3ex 2ex; 
}
select:focus{ outline: none;}

.cajaselect::after{
   font-family: FontAwesome;
   content: "\f0dd";
  display: inline-block;
  text-align: center;
  width: 30px;
  height: 100%;
  background-color: #4683af;
  position: absolute;
  top: 0;
  right: 0px;
  pointer-events: none;
  color: antiquewhite;
  bottom: 0px;
}
</style>
<?php 
$idcurso=$this->cursoActual["idcurso"];
$tipo=array('M'=>'Theme','E'=>ucfirst(JrTexto::_('Exam')),'L'=>ucfirst(JrTexto::_('Activity')),'U'=>ucfirst(JrTexto::_('Unit')),'N'=>ucfirst(JrTexto::_('Level')));
function mashijos($hijo,$rb){ 
    $j=0; 
    $tipo=array('M'=>'Theme','E'=>ucfirst(JrTexto::_('Exam')),'L'=>ucfirst(JrTexto::_('Activity')),'U'=>ucfirst(JrTexto::_('Unit')),'N'=>ucfirst(JrTexto::_('Level')));
    if(!empty($hijo)){
      $htmlhijo='<h3 class="col-xs-12 lesson-title"><span id="namelesson"></span></h3>
      <div class="col-xs-12 lesson-wrapper">
          <div class="lesson-list" style="position: relative;">';
      $ji=0;
    foreach($hijo as $h){
      $ji++;
        $img=$h["imagen"];
        $img='<img class="caratula-libro" src="'.(!empty(@$h['imagen'])?str_replace('__xRUTABASEx__',$rb , $h['imagen']):$rb.'/static/media/imagenes/level_default.png').'" width="100" height="100">';
        $t=$h["tiporecurso"];
        $txttipoexamen='';
        $tienehijos=array();
        $txtj='<i class="fa fa-list"></i>';
        if($t!='E'){
            $j++;
            $tienehijos=!empty($h["hijo"])?$h["hijo"]:array();
            $txtj=str_pad($j,2,"0",STR_PAD_LEFT);
        }else{
            $tienehijos=array();
            if(!empty($h["txtjson"])){
                $tipoexa=json_decode($h["txtjson"]);
                $txttipoexamen='data-tipoexamen="'.$tipoexa->tipo.'"';
            }
        }
        $htmlhijo.='<div class="item-tools rname" data-iname="namelesson" title="'.$tipo[$t].": ".$h["nombre"].'" data-iddetalle="'.@$h["idcursodetalle"].'" data-idrecurso="'.@$h["idrecurso"].'">
                  <span>
                      <a href="'.@$h["link"].'" class="lesson-item hvr-float '.($ji==1?'active':'').'" data-linkedit="'.@$h["linkedit"].'"> '.$img.'
                          <!--<div class="lesson-name" style="position: absolute; z-index: 99; left: 30px;" >'.ucfirst(JrTexto::_('Activity')).'</div>-->
                          <div class="lesson-name" style="/*position: absolute; z-index: 99; left: 70px; bottom: 0px;*/">'.$txtj.'</div>
                      </a>                      
                      <div class="toolbottom hide" style="left:45%;">
                        <span class="btn-group-vertical text-center">
                          <span class="btn btn-xs btn-warning btneditnivel"  title="'.JrTexto::_('Edit').'"><i class="fa fa-edit"></i></span>
                          <!--span class="btn btn-xs btndeletenivel" title="'.JrTexto::_('Delete').'"><i class="fa fa-trash"></i></span-->
                        </span>
                      </div>                   
                    </span>
                    '.(!empty($tienehijos)?('<div class="hijos" style="display:none;">'.mashijos($tienehijos,$rb).'</div>'):'').'
                </div>';
    }
    $htmlhijo.='</div></div>';
    echo $htmlhijo;
  }
}
?>
<div class="container">
 	<div class="row" id="todas-unidades">     		
 		<div class="row" id="levels">
      <div class="col-md-6">
          <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlSitio(); ?>"><?php echo JrTexto::_("Home")?></a></li>                  
            <li class="active"><?php echo JrTexto::_("Activities")?></li>
          </ol>
      </div>
      <div class="col-md-6 cajaselect pull-right"> <select name="level" id="level-item" style="">
        <?php if(!empty($this->cursos))
                foreach ($this->cursos as $cur){?>
                <option value="<?php echo $cur["idcurso"]; ?>" <?php echo $cur["idcurso"]==$this->idcurso?'Selected':'';?>><?php echo $cur["idcurso"]." : ".$cur["nombre"]?></option>
        <?php }?>               
        </select>
      </div> 
    </div>
    <div class="row elementlistini" id="units">
        <h3 class="col-xs-12 unit-title"><span id="nameunit"><?php echo JrTexto::_('Selected Options'); ?></span></h3>
        <div class="col-xs-12 unit-wrapper">
            <div class="unit-list" style="position: relative;">
                <?php 
                $i=0;
                $activo=true;
                $hijos=array();
                $tienehijos=array();               
                if(!empty($this->newsylabus))
                  foreach ($this->newsylabus as $curd){ 
                  $img='';
                  if(!empty($curd["imagen"])){
                    $img='<img class="caratula-libro" src="'.(str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$curd["imagen"])).'" width="100" height="100">';
                  } 
                  $txti='<i class="fa fa-list"></i>';
                  $tienehijos=@$curd["hijo"];
                  if($curd["tiporecurso"]!='E'){                        
                    $i++;
                    $txti=str_pad($i,2,"0",STR_PAD_LEFT);                    
                  }             
              ?>                  
                <div class="item-tools rname" data-tiporecurso="<?php echo $curd["tiporecurso"]; ?>" data-iname="nameunit" title="<?php echo $tipo[$curd["tiporecurso"]].": ".$curd["nombre"]; ?>" data-iddetalle="<?php echo $curd["idcursodetalle"]; ?>" data-idrecurso="<?php echo $curd["idrecurso"]; ?>" >                     
                  <span>                        
                    <a href="<?php echo $curd['link']; ?>" class="hvr-float unit-item" data-linkedit="<?php echo @$curd["linkedit"];?>"><?php echo $img; ?>
                        <!--<div class="unit-name" style="position: absolute; z-index: 99; left: 30px;"><?php echo JrTexto::_('Unit').''; ?></div>-->
                        <div class="unit-name" style="/*position: absolute; z-index: 99; left: 70px; bottom: 0px;*/"><?php echo $txti; ?></div>
                    </a>                        
                  </span>
                  <?php if(!empty($tienehijos)){?><div class="hijos" style="display:none;"> <?php  mashijos($tienehijos,$this->documento->getUrlBase()); ?> </div><?php }
                  else if($curd["tiporecurso"]=='E') { ?>
                  <div class="toolbottom " style="left:45%;">
                        <span class="btn-group-vertical text-center">
                          <a  href="<?php echo $curd['linkedit']; ?>" class="btn btn-xs btn-warning btnediexamen"  title="<?php echo JrTexto::_('Edit');?>"><i class="fa fa-edit"></i></a>
                          <!--span class="btn btn-xs btndeletenivel" title="'.JrTexto::_('Delete').'"><i class="fa fa-trash"></i></span-->
                        </span>
                      </div>
                  <?php } ?>
                </div>
                <?php 
                if($activo==true){$activo=false; $mostrar=true; $hijos=@$curd["hijo"];}                    
              } ?>
            </div>
        </div>
    </div>
 	</div>    	
</div>

<div class="hidden" id="a1">
    	<div class="form-group">       
	    	<label class="control-label" for="txtNombre"><?php echo ucfirst(JrTexto::_('Activity'));?> <span class="required"> * :</span></label>
        <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Type the name of the activity here')?>"> <br>
        <div>
        <button class="btn btn-cancel cerrarmodal pull-left"></span><?php echo JrTexto::_('Cancel');?></span> <i class="fa fa-close"></i></button>
        <button class="btn btn-success savenewactividad pull-right"><span><?php echo JrTexto::_('Save and add exercise');?></span> <i class="fa fa-text-o"></i></button> 
        <button class="btn btn-primary savenewactividad pull-right"><span><?php echo JrTexto::_('Save');?></span> <i class="fa fa-save"></i></button>                  
        <div class="clearfix"></div>
        </div>
    	</div>
      <div class="clearfix"></div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
    $(window).on('resize',function(){
      if($(window).width() <= 768){
        $('.toolbottom').removeClass('hide');
      }else{
        if($('.toolbottom').hasClass('hide') == false){
          $('.toolbottom').addClass('hide');        
        }
      }
    });
    $(window).trigger('resize');
	$('#todas-unidades').on("change",'select#level-item',function(ev){
	  	var idnivel=$(this).val();
      var data={tipo:'U','idpadre':idnivel}
     redir(_sysUrlBase_+'/recursos/listar/'+idnivel);
  }).on("click",'.btneditnivel',function(ev){
    ev.preventDefault();
    ev.stopPropagation();
    var linkedit=$(this).parent().parent().siblings('a').attr('data-linkedit')||'';
    if(linkedit=='') return false;
    redir(linkedit);
  }).on("click",'.btnediexamen',function(ev){
    ev.preventDefault();
    ev.stopPropagation();
    //console.log($(this));
    var linkedit=$(this).attr('href')||'';
    if(linkedit=='') return false;
    redir(linkedit);
  }).on("click",'.item-tools',function(ev){//mostrar actividad
   // ev.preventDefault();
   // console.log($(this));
   
    var el=$(this);
    var hijos=el.children('.hijos');
    if(hijos.length==0){
      return;
    }
    ev.preventDefault();    
    var idet=el.attr('data-iddetalle');
    $hayhijos=$('#todas-unidades').find('#listado'+idet);
    el.siblings('.item-tools').children('span').children('a.active').removeClass('active');    
    el.children('span').children('a').addClass('active');
    var $list=el.closest('.elementlistini');    
    var name=el.attr('title'); 
    $('.row.detalleamostrar').hide();
    hijos.find('.toolbottom').addClass('hide');
    if($hayhijos.length){ // solo muestra  los hijos
      $hayhijos.show();        
    }else{ // agrega los hijos    
      $('#todas-unidades').append('<div id="listado'+idet+'" class="row detalleamostrar"><div id="lessons" class="elementlistini">'+hijos.html()+'</div></div>');
      $('#listado'+idet+' .lesson-list').slick(optionslike);
    }

    $('.elementlistini').each(function(i,v){
      $hayactivo=$(v).find('.item-tools > span > a.active');
     
      if($hayactivo.length){        
        var $list2=$(v).closest('.elementlistini');    
        var name=$hayactivo.closest('.item-tools').attr('title');
        $(v).children('h3').children('span').text(name);
      }
    })
	  /*var aobj=$(this).closest('.lesson-list').find('a');
	  $(aobj).removeClass('active');
	  $(this).find('a').addClass('active');
	  if(!$(this).hasClass('item-add'))
	  	redir(_sysUrlBase_+'/actividad/agregar2/?idnivel='+idnivel+'&txtUnidad='+idunidad+'&txtsesion='+idlesson);*/
    $(window).trigger('resize');
	}).on("mouseover",".item-tools.rname",function(){
    var $list=$(this).closest('.elementlistini');
    var name=$(this).attr('title');    
    $list.children('h3').children('span').text(name);
    $(this).find('.toolbottom').removeClass('hide');
  }).on("mouseout",".item-tools.rname",function(){
    var $list=$(this).closest('.elementlistini');
    var name='';   
    $(this).find('.toolbottom').addClass('hide');
    $itemtools=$(this).parent('div').children('.item-tools');
    $itemtools.each(function(i,v){
      $obj=$(v).children('span').children('a.active');
      if($obj.length){
        name=$(v).attr('title');
      }
    });
    $list.children('h3').children('span').text(name);
  });
  //initseleccion();  
  var optionslike={
	  	infinite: false,
   		navigation: false,
  		slidesToScroll: 1,
  		centerPadding: '60px',
  	  slidesToShow: 6,
  	  responsive:[
          { breakpoint: 1200, settings: {slidesToShow: 5} },
          { breakpoint: 992, settings: {slidesToShow: 4 } },
          { breakpoint: 880, settings: {slidesToShow: 3 } },
          { breakpoint: 720, settings: {slidesToShow: 2 } },
          { breakpoint: 320, settings: {slidesToShow: 1 ,arrows: false, centerPadding: '40px',} }	  	
 	  ]
 	};
 	var slikunidad=$('.unit-list').slick(optionslike);
  $('header').show('fast').addClass('static');
  $('select#level-item').on('change',function(ev){
    var nivel=$('select#level-item').val();
    redir(_sysUrlBase_+'/recursos/listar/'+nivel);
  });


  var initseleccion=function(){
    var leta=$('.item-tools').eq(0);
    var tr=leta.attr('data-tiporecurso');  
    leta.children('span').children('a').addClass('active'); 
      if(tr!='E'){
      var $list=leta.closest('.elementlistini');    
      var name=leta.attr('title');
      var idet=leta.attr('data-iddetalle');
      var hijos=leta.children('.hijos');
      $hayhijos=$('#todas-unidades').find('#listado'+idet);
      if(hijos.length){
        if($hayhijos.length){ // solo muestra  los hijos
          $hayhijos.show();
        }else{ // agrega los hijos            
          $('#todas-unidades').append('<div id="listado'+idet+'" class="row detalleamostrar"><div id="lessons" class="elementlistini">'+hijos.html()+'</div></div>');
          $('#listado'+idet+' .lesson-list').slick(optionslike);
        }
      }
    }

    $('.elementlistini').each(function(i,v){
      $itools=$(v).children('div').children('div').children('div').children('div').children('div');
      $hayactivo=$itools.children('span').children('a.active');
      var name="";
      if($hayactivo.length){        
        var $eleini=$(v).closest('.elementlistini');    
        var name=$itools.attr('title');
        $(v).children('h3').children('span').text(name);
      }
    })
  };
  initseleccion();
 });
</script>