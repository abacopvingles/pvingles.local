<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$grupo=!empty($this->datos)?$this->datos:"";
$grupodetalle=!empty($this->datosgrupodetalle)?$this->datosgrupodetalle:"";
$idproyecto=$this->idproyecto;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .titulo{
    font-weight: bold;
    font-size: 1.3em;
  }
  hr{
    margin-top: 0px;
    margin-bottom: 1ex;
    border: 0;
    border-top: 1px solid #00BCD4;
  }
</style>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <?php if(!$ismodal&&$idproyecto==3){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/acad_grupoaula">&nbsp;<?php echo JrTexto::_("Groups"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Edit"); ?></li>   
        <?php }else{ ?>
        <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Editar Grupos de estudio"); ?></li>
        <?php } ?>
    </ol>
  </div>
</div>

<div class="row" >
  <div class="col-xs-12">
    <div class="panel" >      
      <div class="panel-body">
        <div class="col-md-12">
          <form id="frmgrupoaula<?php echo $idgui; ?>">
          <input type="hidden" name="idgrupoaula" class="idgrupoaula" id="idgrupoaula<?php echo $idgui; ?>" value="<?php echo @$grupo["idgrupoaula"]; ?>">
          <input type="hidden" name="accion" id="acciongrupoaula<?php echo $idgui; ?>" value="<?php echo @$this->accion ?>">
          
          <div class="col-md-12 titulo"> - <?php echo JrTexto::_('Grupo Information') ?><hr></div>
          <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Name")); ?></label>             
                <input type="text" name="nombre" id="nombre<?php echo $idgui; ?>" requerid data-value="<?php echo @$grupo["nombre"]; ?>" value="<?php echo @$grupo["nombre"]; ?>" class="form-control border0 nombregrupoaula" placeholder="<?php echo  date('Y')." - 01"?>">               
              </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <label>N° <?php echo ucfirst(JrTexto::_("Vacantes")); ?></label>             
                <input type="number" name="nvacantes" id="nvacantes<?php echo $idgui; ?>" data-value="<?php echo @$grupo["nvacantes"]; ?>" value="<?php echo @$grupo["nvacantes"]; ?>" class="form-control border0 nvacantesgrupoaula" placeholder="30">
              </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> </label>
                <div class="cajaselect">
                  <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control" data-value="<?php echo @$grupo["tipo"] ?>">                 
                    <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                      <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                    <?php } ?>
                  </select>
                </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Estado")); ?> </label>
                <div class="cajaselect">
                  <select name="estado" id="estado<?php echo $idgui;?>" class="form-control" data-value="<?php echo @$grupo["estado"] ?>">                  
                    <?php if(!empty($this->fkestadogrupos)) foreach ($this->fkestadogrupos as $fk){ ?>
                      <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fkidestadogrupo?'selected="selected"':'';?> ><?php echo ucfirst(JrTexto::_($fk["nombre"])); ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <a href="javascript:void(0)" class="active" id="showcomentario<?php echo $idgui; ?>" style="display: inline-block;" data-show="<?php echo JrTexto::_('show Comment')?>" data-hide="<?php echo JrTexto::_('hide Comment')?>" ><i class="fa fa-eye"></i> <span><?php echo JrTexto::_('show Comment')?></span></a>
            <textarea name="comentario" class="form-control" id="comentario<?php echo $idgui; ?>" data-value="<?php echo @$grupo["comentario"] ?>" style="display: none;" ><?php echo @$grupo["comentario"] ?></textarea>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="savegrupoaula<?php echo $idgui ?>" style="display:"><br>
            <button type="button" style="margin-right:1ex;" class="btn btn-warning" href="#" onclick="window.history.back()" data-dismiss="modal"  ><i class="fa fa-reply"></i> <?php echo JrTexto::_('Back');?></a> 
            <button type="button"  style="margin-right:1ex;" class="btnvercursos btn btn-primary" href="#"><i class="fa fa-address-book"></i> <?php echo JrTexto::_('Ver cursos');?></a> 
            <button type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
          </div>
          </form>
          <div class="clearfix"></div>
        </div>       
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  var idgrupoaula=parseInt('<?php echo !empty($grupo["idgrupoaula"])?$grupo["idgrupoaula"]:0; ?>');
    $('#showcomentario<?php echo $idgui; ?>').click(function(){
      $(this).toggleClass('active');
      if($(this).hasClass('active')){
        $('i',this).removeClass('fa-eye-slash').addClass('fa-eye');
        $('span',this).text($(this).attr('data-show'));
        $(this).siblings('textarea').hide();
      }else{
        $('i',this).removeClass('fa-eye').addClass('fa-eye-slash');
        $('span',this).text($(this).attr('data-hide'));
        $(this).siblings('textarea').show();
      }      
    })
    $('#frmgrupoaula<?php echo $idgui; ?>').submit(function(ev){
        ev.preventDefault();
        var frmtmp=document.getElementById('frmgrupoaula<?php echo $idgui; ?>');
        var formData = new FormData(frmtmp);
        var data={
            fromdata:formData,
            url:_sysUrlBase_+'/acad_grupoaula/guardarAcad_grupoaula',
            msjatencion:'<?php echo JrTexto::_('Attention');?>',
            type:'json',
            showmsjok:true,
            callback:function(rs){
              idgrupoaula=rs.newid;
              $('#idgrupoaula<?php echo $idgui;?>').val(rs.newid);
              $('#acciongrupoaula<?php echo $idgui; ?>').val('edit');  
              $('.btnvercursos').show();                  
            }
        }
        sysajax(data);
        return false;
    });
    $('.btnvercursos').on('click',function(){
      window.location.href=_sysUrlBase_+'/acad_grupoaula/formaddcursos/?idgrupoaula='+idgrupoaula;
    })
    if(idgrupoaula==0){
      $('.btnvercursos').hide();
    }
})
</script>