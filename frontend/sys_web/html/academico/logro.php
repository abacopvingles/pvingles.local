<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('Certificates'); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="row" id="ventana-<?php echo $idgui;?>">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body ">      
         <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3 ">              
              <div class="select-ctrl-wrapper select-azul">
                <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                </select>
              </div>
            </div> 
            <div class="col-xs-6 col-sm-4 col-md-3 ">              
              <div class="select-ctrl-wrapper select-azul">
                <select name="cbtipo" id="cbtipo" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All Type"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Certificate"))?></option>
                    <option value="2"><?php echo ucfirst(JrTexto::_("Medalla"))?></option>                              
                </select>
              </div>
            </div>        
            <div class="col-xs-6 col-sm-4 col-md-4">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-4 col-md-2 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("logro", "agregar"));?>" title="<?php echo JrTexto::_("Certificate").' - '.JrTexto::_("Add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>

            
          </div>
      </div>
    </div>
  <div class="col-md-12 col-sm-12 col-xs-12" id="datalistado<?php echo $idgui;?>">
    <div id="cargando" style="display: none;">cargando</div>
      <div id="sindatos" style="display: none;">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron">
                <h1><strong>Opss!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, we could not find data')?>.</div>              
            </div>
        </div>
      </div>
      <div id="error" style="display: none;">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="jumbotron panel-danger">
                <h1><strong>Error!</strong></h1>                
                <div class="disculpa"><?php echo JrTexto::_('Sorry, data no found')?>.</div>              
            </div>
        </div>
      </div>
      <div id="data">
        <?php if(!empty($this->datos))$im=0;
        foreach ($this->datos as $dt){ $im++;
          $img=!empty($dt["imagen"])?($this->documento->getUrlBase().'/'.$dt["imagen"]):$this->documento->getUrlStatic().'/media/imagenes/cursos/nofoto.jpg';
          ?>
         <div class="col-md-3 col-sm-4 col-xs-12 mg1">
         <div class="panel-user hvr-grow" data-id="<?php echo $dt["id_logro"]; ?>">            
              <div class="pnlacciones text-center">
                <?php if($ismodal){?><a class="btn-selected btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected ')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-hand-o-down"></i></a><?php } ?>
                <a class="btn-changefoto btn btn-danger  btn-xs" data-tipo="image" title="<?php echo ucfirst(JrTexto::_('Change')).' '.JrTexto::_('Photo'); ?>"><i class="fa fa-photo"></i></a>
                <a class="btnvermodal btn btn-danger btn-xs" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("logro", "editar"));?>/?id=<?php echo $dt["id_logro"]; ?>" title="<?php echo ucfirst(JrTexto::_('Edit')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-pencil"></i></a> 
                <a class="btn-eliminar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-trash-o"></i></a>
              </div>
               <div class="item"><img class="img-responsive image<?php echo $idgui.$im; ?>" src="<?php echo $img; ?>" width="100%">
               <div class="texto"><div class="titulo"><?php echo ucfirst($dt["titulo"]) ;?></div></div>
               </div>
           </div>
          </div>
        <?php }?>
      </div>
  </div>
  <div id="controles<?php echo $idgui;?>" style="display: none;">
    <div class="pnlacciones text-center">
      <?php if($ismodal){?><a class="btn-selected btn btn-danger btn-xs" title="<?php echo ucfirst(JrTexto::_('Selected ')).' '.JrTexto::_('Certificate'); ?>"><i class="btnselected fa fa-hand-o-down"></i></a><?php } ?>
     <a class="btn-changefoto btn btn-danger  btn-xs" data-tipo="image" title="<?php echo ucfirst(JrTexto::_('Change')).' '.JrTexto::_('Photo'); ?>"><i class="fa fa-photo"></i></a>
      <a class="btnvermodal btn btn-danger btn-xs" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("logro", "editar"));?>/?id=<?php echo $dt["id_logro"]; ?>" title="<?php echo ucfirst(JrTexto::_('Edit')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-pencil"></i></a> 
      <a class="btn-eliminar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Remove')).' '.JrTexto::_('Certificate'); ?>"><i class="fa fa-trash-o"></i></a>
    </div>
  </div>
</div>

<script type="text/javascript">
var tabledatos<?php echo $idgui; ?>='';

var addvalorcampo<?php echo $idgui; ?>=function(idlogro,campo,valor){  
  var formData = new FormData();
  formData.append('idlogro', idlogro);
  formData.append('campo', campo);
  formData.append('valor', valor);  
  var url=_sysUrlBase_+'/logro/addcampo';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {  
       if(data.code=='Error')
         mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}
function checkdata<?php echo $idgui; ?>(){
  $datapnl=$('#datalistado<?php echo $idgui; ?> #data');
  if($datapnl.find('.mg1').length==0){
    $datapnl.hide(0);
    $datapnl.siblings('#sindatos').css('display','block').animateCss('zoomIn');
  }
}

function refreshdatos<?php echo $idgui; ?>(){
    tabledatos<?php echo $idgui; ?>();
}
$(document).ready(function(){  
  var estados<?php echo $idgui; ?>={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>'}; 
  $('#ventana-<?php echo $idgui;?>')
  .on('change','#cbtipo',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('change','#cbestado',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('click','.btnbuscar',function(ev){
    refreshdatos<?php echo $idgui; ?>();
  }).on('keyup','#texto',function(ev){
    if(e.keyCode == 13)
    refreshdatos<?php echo $idgui; ?>();
  }).on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'logro', 'setCampo', id,campo,data);
          if(res) tabledatos5a0467caf3d78.ajax.reload();
        }
      });
  }).on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos<?php echo $idgui; ?>';
    var url=$(this).attr('href');
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var titulo=$(this).attr('title')||'';   
    if(enmodal=='no'){
      return redir(url);
    }
    url+='&plt=modal';
    var obj={url:url,borrar:true,titulo:titulo,cerrarconesc:true,backdrop:false};
    console.log(obj);
    sysmodal(obj);
  }).on('click','.btn-eliminar',function(ev){ 
     ev.preventDefault();
     ev.stopPropagation();
     var pnl=$(this).closest('.panel-user');
     var idlogro=pnl.attr('data-id');     
     var formData = new FormData();
      formData.append('idlogro', idlogro);
      var url=_sysUrlBase_+'/logro/eliminar';
      $.ajax({
        url: url,
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        dataType:'json',
        cache: false,
        beforeSend: function(XMLHttpRequest){ },      
        success: function(data)
        {  
           if(data.code=='Error')
             mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
           else {
            var pnlremove=pnl.closest('.mg1');
            pnlremove.animateCss('zoomOut',function(){
              pnlremove.remove();
              checkdata<?php echo $idgui; ?>();
            });
          }
        },
        error: function(e){ },
        complete: function(xhr){ }
      });
  }).on("click",'.btn-changefoto', function(ev){
      ev.preventDefault();
      ev.stopPropagation();
        var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
        var pnl=$(this).closest('.panel-user');
        var img=pnl.find('img');
        var tmpid='id_'+Date.now();
        $(this).attr('data-url','.'+tmpid);
        $(img).addClass(tmpid); 
        selectedfile(ev,$(this),txt);              
        $(img).load(function(ev){         
          var idlogro=pnl.attr('data-id');
          var url=$(this).attr('src');      
          url=url.replace(_sysUrlBase_,'');
          $(this).removeClass(tmpid);
          addvalorcampo<?php echo $idgui; ?>(idlogro,'imagen',url);         
        });
  }).on("mouseenter",'.panel-user', function(){
      if(!$(this).hasClass('active')){ 
        $(this).siblings('.panel-user').removeClass('active');
        $(this).addClass('active');
      }
  }).on("mouseleave",'.panel-user', function(){     
      $(this).removeClass('active');
  }).on("click",'.btn-selected', function(){
      $(this).addClass('active');
      var idlogro=$(this).closest('.panel-user').attr('data-id');
      <?php 
        if(!empty($ventanapadre)){?>
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idlogro',idlogro);
            btn.trigger("addpremio");
          }
          $(this).closest('.modal').find('.cerrarmodal').trigger('click'); 
      <?php } ?>             
  }).on('click','.panel-user', function(){      
      <?php if(!empty($ventanapadre)){?>
          var idlogro=$(this).attr('data-id');
          var btn=$('.<?php echo $ventanapadre; ?>');
          if(btn.length){         
            btn.attr('data-idlogro',idlogro);
            btn.trigger("addpremio");
            $(this).closest('.modal').find('.cerrarmodal').trigger('click');
          }
      <?php }  ?>
  }); 
  tabledatos<?php echo $idgui; ?>=function(){
    $datapnl=$('#datalistado<?php echo $idgui; ?>');
    var formData = new FormData();
        formData.append('titulo', $('#ventana-<?php echo $idgui;?> #texto').val());
        formData.append('estado', $('#ventana-<?php echo $idgui;?> #cbestado').val());
        formData.append('tipo', $('#ventana-<?php echo $idgui;?> #cbtipo').val());  
    $.ajax({
      url: _sysUrlBase_+'/logro/buscarjson/',
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      beforeSend: function(XMLHttpRequest){ 
        $datapnl.find('#cargando').siblings('div').hide(0);
        $datapnl.find('#cargando').css('display','block').animateCss('zoomIn');
      },      
      success: function(res)
      {     
        if(res.code==='ok'){
          var midata=res.data;
          if(midata.length){
            var html='';
            var controles=$('#controles<?php echo $idgui; ?>').html();
            $.each(midata,function(i,v){
              var urlimg=_sysUrlStatic_+'/media/imagenes/cursos/';
              var srcimg=_sysfileExists(_sysUrlBase_+'/'+v.imagen)?(_sysUrlBase_+'/'+v.imagen):(urlimg+'nofoto.jpg');
              html+='<div class="col-md-3 col-sm-4 col-xs-12 mg1"><div class="panel-user hvr-grow" data-id="'+v.id_logro+'">'+controles;
              html+='<div class="item"><img class="img-responsive" src="'+srcimg+'" width="100%">'; 
              html+='<div class="texto"><div class="titulo">'+v.titulo+'</div></div>';
              html+='</div></div></div>';
            });
            $datapnl.find('#cargando').hide(0);
            $datapnl.find('#data').html(html).css('display','block').animateCss('zoomIn');           
          }else {
            $datapnl.find('#cargando').hide(0);
            $datapnl.find('#sindatos').css('display','block').animateCss('bounceIn');             
          }            
        }else{
           $datapnl.find('#cargando').siblings('div').hide(0);
           $datapnl.find('#error').css('display','block').animateCss('flash');  
        }
      },
      error: function(e) 
      {   $datapnl.find('#cargando').siblings('div').hide(0);
          $datapnl.find('#error').css('display','block').animateCss('flash');  
      }      
    });
  };
  checkdata<?php echo $idgui; ?>();
});

</script>