<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->misdatos)) $datos=$this->misdatos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$tipo_=array('N'=>JrTexto::_('Level'),'U'=>JrTexto::_('Unit'),'L'=>JrTexto::_('Lesson'),'M'=>'Lesson');
function addnivel($data,$css){
	$tipo_=array('N'=>JrTexto::_('Level'),'U'=>JrTexto::_('Unit'),'L'=>JrTexto::_('Lesson'),'E'=>'Assessments');
	$tipo=$data["tiporecurso"];
	$img=!empty($data["imagen"])?$data["imagen"]:(URL_BASE.'/static/media/imagenes/cursos/nofoto.jpg');
	$img=str_replace('__xRUTABASEx__',URL_BASE,$img);	
	$tipotxt=$tipo_[$tipo];	
	$html='<div class="col-md-3 col-sm-4 col-xs-12 padding1">';
	$html.='<div class="panel-user hvr-grow" data-id="'.$data["idcursodetalle"].'" data-idrecurso="'.$data["idrecurso"].'" data-idcurso="'.$data["idcurso"].'" ';
	$html.='data-tipo="'.$data["tiporecurso"].'" data-showpnl="'.$css.$data["idcursodetalle"].'" data-idlogro="'.$data["idlogro"].'" >';
	$html.='<div class="pnlacciones text-center" >';
	$html.='<a class="btnchangefoto btn btn-danger  btn-xs" data-tipo="image" data-url=".imagepnluser'.$data["idcursodetalle"].'" title="'.ucfirst(JrTexto::_('Change')).' '.JrTexto::_('Photo').'"><i class="fa fa-photo"></i></a>';
	$html.='<a class="btnpremio btn btn-danger  btn-xs" title="'.ucfirst(JrTexto::_('premium')).'"><i class="fa fa-trophy"></i></a>';
	$html.='<a class="btneditar btn btn-danger  btn-xs" title="'.ucfirst(JrTexto::_('Edit')).' '.$tipotxt.'"><i class="fa fa-pencil"></i></a>';
	$html.='<a class="btneliminar btn btn-danger  btn-xs" title="'.ucfirst(JrTexto::_('Remove')).' '.$tipotxt.'"><i class="fa fa-trash-o"></i></a>';
	$html.='</div><div class="item"><img class="img-responsive imageload imagepnluser'.$data["idcursodetalle"].'" src="'.$img.'" width="100%">';
	$html.='<div class="texto"><div class="titulo"><strong>'.$data["nombre"].'</strong></div></div></div></div></div>';
	return $html;
}
?>
<style type="text/css">
	.nombrecurso{	color: #9c0606;
	    webkit­shape­inside: rectangle(0,0,100%,100%,100%,100%);
	    font-size: 1.5em;
	    text-align: center;
  	}
  	.pnlacciones{	   
	    top: 0px;
	    height: auto !important;
	}
	.padding1{
		padding: 1ex;
	}
	.img-vista{height: 100px; overflow: hidden;}

	.panel-user:not([data-idlogro="0"]) .btnpremio{
		color:#1ce824;
	}
	.panel-user[data-idlogro="0"] .btnpremio, .panel-user:not([data-idlogro]) .btnpremio{
		color:#fff;
	}
	.showpnl.level_02, .showpnl.level_03{
		display: none;
	}
</style>
<div class="col-xs-12 col-sm-12 col-md-12" id="pnlver2<?php echo $idgui; ?>" >
  	<div class="col-md-12">             
    	<div class="form-group"><label>II. Organización de Temas:</label></div>    	
  	</div>
  	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="panel">		
			<div class="col-md-12 col-sm-12 col-xs-12" id="datos<?php echo $idgui; ?>">
				<div id="cargando" style="display: none">cargando</div>
			    <div id="sindatos" style="display: none">sindatos</div>
			    <div id="error" style="display: none">sindatos</div>
			    <div id="data">
			    	<?php 
			    	$html1=$html2=$html3='';
			    	if(!empty($datos)){
			    		$html1.='<div class="row showpnl level_01" id="pnllevel_01" data-tipo="'.@$datos[0]["tiporecurso"].'" data-idpadre="0"><div class="col-md-12 "><div class="addcontrols btn-group pull-right"></div></div>';
			    		foreach ($datos as $dato){
			    			if(!empty($dato['hijo'])){
			    				$html2.='<div class="row showpnl level_02" id="pnllevel_02'.$dato["idcursodetalle"].'" data-return="pnllevel_01" data-tipo="'.@$dato["hijo"][0]["tiporecurso"].'" data-idpadre="'.$dato["idcursodetalle"].'">';
			    				$html2.='<div class="col-md-12 "><div class="addcontrols btn-group pull-right"></div></div>';
			    				foreach ($dato['hijo'] as $dat){
			    					if(!empty($dat['hijo'])){
			    						$html3.='<div class="row showpnl level_03" id="pnllevel_03'.$dat["idcursodetalle"].'"  data-tipo="'.@$dat["hijo"][0]["tiporecurso"].'" data-idpadre="'.$dat["idcursodetalle"].'" ';
			    						$html3.=' data-return="pnllevel_02'.$dato["idcursodetalle"].'">';
			    						$html3.='<div class="col-md-12 "><div class="addcontrols btn-group pull-right"></div></div>';
					    				foreach ($dat['hijo'] as $da){
					    					$html3.=addnivel($da,'pnllevel_04');
					    				}
					    				$html3.='</div>';
					    			}
					    			$html2.=addnivel($dat,'pnllevel_03');
			    				}
			    				$html2.='</div>';
			    			}
			    			$html1.=addnivel($dato,'pnllevel_02');
			    		}
			    		$html1.='</div>';
			    	}else{
			    		$html1.='<div class="row showpnl level_01" id="pnllevel_01" data-tipo="N" data-idpadre="0"><div class="col-md-12 "><div class="addcontrols btn-group pull-right"></div></div></div>';
			    	}
			    	echo $html1.$html2.$html3;
			    	?>
			    </div>
			</div>
			<div id="controls<?php echo $idgui; ?>" style="display: none;">
				<a href="#" class="btn btn-primary btnimport btnnopadre" title="<?php echo ucfirst(JrTexto::_('Import content'));?>">
					<i class="fa fa-download" ></i> <?php echo ucfirst(JrTexto::_('Import content'));?></a>
				<a href="#" class="btn btn-warning btnadd btnnopadre" title="<?php echo ucfirst(JrTexto::_('Add content'));?>">
					<i class="fa fa-plus" ><i class="fa fa-plus"></i></i> <?php echo ucfirst(JrTexto::_('Add content')); ?></a>
				<a href="#" class="btn btn-danger btnexamen btnnopadre" title="<?php echo ucfirst(JrTexto::_('Add assessment'));?>">
					<i class="fa fa-list" ><i class="fa fa-plus"></i></i> <?php echo ucfirst(JrTexto::_('Add Assesment')); ?></a>
				<a href="#" class="btn btn-success btnrefresh btnnopadre" style="display: none;"><i class="fa fa-refresh"></i> <?php echo ucfirst(JrTexto::_('Refresh')); ?></a>
				<a href="#" class="btn btn-success btnreturn btnnopadre"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('Back')); ?></a>				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var txttipo_=<?php echo json_encode($tipo_); ?>;
var getcursodetalle<?php echo $idgui; ?>=function(idcursodetalle,idrecurso,$pnl){
	var formData = new FormData();
	formData.append('idcursodetalle', idcursodetalle);
	formData.append('idrecurso', idrecurso);
	var url=_sysUrlBase_+'/acad_cursodetalle/buscarjson';
	$.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {  
	        if(data.code=='Error')
	       	 mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
	       	else{
	       		info=data.data[0];
	       		img=info.imagen.replace('__xRUTABASEx__',_sysUrlBase_);
	       		if($pnl.hasClass('showpnl')){	
	       			$orden=info.tiporecurso=='N'?'02':(info.tiporecurso=='U'?'03':'04');
	       			$html='<div class="col-md-3 col-sm-4 col-xs-12 padding1"><div class="panel-user hvr-grow" data-id="'+info.idcursodetalle+'" data-idrecurso="'+info.idrecurso+'" data-idcurso="'+info.idcurso+'" data-tipo="'+info.tiporecurso+'" data-showpnl="pnllevel_'+$orden+info.idcursodetalle+'" data-idlogro="'+info.idlogro+'"><div class="pnlacciones text-center">';
	$html+='<a class="btnchangefoto btn btn-danger  btn-xs" data-tipo="image" data-url=".imagepnluser'+info.idcursodetalle+'" title="<?php echo ucfirst(JrTexto::_('Change')).' '.JrTexto::_('Photo'); ?>"><i class="fa fa-photo"></i></a>';
	$html+='<a class="btnpremio btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('premium')); ?>'+txttipo_[info.tiporecurso]+'"><i class="fa fa-trophy"></i></a>';
	$html+='<a class="btneditar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Edit')); ?>'+txttipo_[info.tiporecurso]+'"><i class="fa fa-pencil"></i></a>';
	$html+='<a class="btneliminar btn btn-danger  btn-xs" title="<?php echo ucfirst(JrTexto::_('Remove')); ?>'+txttipo_[info.tiporecurso]+'"><i class="fa fa-trash-o"></i></a>';

	       			$html+='</div><div class="item"><img class="img-responsive imageload imagepnluser'+info.idcursodetalle+'" src="'+img+'" width="100%"><div class="texto"><div class="titulo"><strong>'+info.nombre+'</strong></div></div></div></div></div>';
	       			$pnl.append($html);
	       		}else if($pnl.hasClass('panel-user')){
	       			$pnl.find('img.imageload').attr('src',img);
	       			$pnl.find('.texto .titulo strong').text(info.nombre);
	       		}
	       	}
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	});
}
var addvalorcamponivel<?php echo $idgui; ?>=function(idnivel,campo,valor){	
	var formData = new FormData();
	formData.append('idnivel', idnivel);
	formData.append('campo', campo);
	formData.append('valor', valor);  
	var url=_sysUrlBase_+'/niveles/addcampo';
	$.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {  
	       if(data.code=='Error')
	       	 mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	});
}
var addcampodetalle<?php echo $idgui; ?>=function($id,campo,valor){
	  var formData = new FormData();
	  formData.append('idcursodetalle', $id);
	  formData.append('campo', campo);
	  formData.append('valor', valor);
	  var url=_sysUrlBase_+'/acad_cursodetalle/addcampo';
	  $.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {   
	    	if(data.code=='Error')
   	 			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	  });
}
var addcursodetalle<?php echo $idgui; ?>=function($pnl,datos){
	var url=_sysUrlBase_+'/acad_cursodetalle/guardarcursodetalle';
	var formData = new FormData();
	  formData.append('idcursodetalle', datos.idcursodetalle||'');
	  formData.append('orden', datos.orden||0);
	  formData.append('idrecurso', datos.idrecurso);
	  formData.append('tiporecurso', datos.tiporecurso);
	  formData.append('idlogro', datos.idlogro||0);
	  formData.append('url', datos.url||'');
	  formData.append('idpadre', datos.idpadre);
	  formData.append('idcurso', datos.idcurso);	  
	$.ajax({
	    url: url,
	    type: "POST",
	    data:  formData,
	    contentType: false,
	    processData: false,
	    dataType:'json',
	    cache: false,
	    beforeSend: function(XMLHttpRequest){ },      
	    success: function(data)
	    {   
	    	if(data.code=='Error')
   	 			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
   	 		else{
   	 			console.log(data);
   	 		}
	    },
	    error: function(e){ },
	    complete: function(xhr){ }
	});
}
var addjsondetalle<?php echo $idgui; ?>=function(json,idpadre){
  var formData = new FormData();
  formData.append('pkIdcurso', $('#pkIdcurso').val());
  formData.append('idpadre', idpadre);
	  formData.append('jsondetalle', JSON.stringify(json));  
  var url=_sysUrlBase_+'/acad_curso/guardarcursodetalle';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {       
      if(data.code==='ok'){
        refreshvistapadre();
      }else{
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      }
    },
    error: function(e){ },
    complete: function(xhr){ }
  });  
}
$(document).ready(function(){
	$('#pnlver2<?php echo $idgui; ?>').on("mouseenter",'.panel-user', function(){
	    if(!$(this).hasClass('active')){ 
			$(this).siblings('.panel-user').removeClass('active');
		    $(this).addClass('active');
		}
	}).on("click",'.panel-user', function(ev){
		ev.preventDefault();			
		var id=$(this).attr('data-id');
		var tipo=$(this).attr('data-tipo');
		var idshowpnl=$(this).attr('data-showpnl');
		var showpnl=$(this).closest('.showpnl');
		if($('#'+idshowpnl).length){		
			showpnl.hide();
			$('#'+idshowpnl).css('display','block').animateCss('zoomIn');
		}else{
			if(tipo!='L'){
				$orden=tipo=='N'?'02':(tipo=='U'?'03':'04');
				$tipotmp=tipo=='N'?'U':'L';
				showpnlid=showpnl.attr('id');
				controls=$('#controls<?php echo $idgui; ?>').html();
				html='<div class="row showpnl level_'+$orden+'" id="pnllevel_'+$orden+id+'" data-tipo="'+$tipotmp+'" data-return="'+showpnlid+'" style="display:block" data-idpadre="'+id+'"><div class="col-md-12"><div class="addcontrols btn-group pull-right">'+controls+'</div></div></div>';
				$('#datos<?php echo $idgui; ?> #data').append(html);
				showpnl.hide();
			}
		}
	}).on("mouseleave",'.panel-user', function(){			
		$(this).removeClass('active');
	})

		//$( "#datos<?php //echo $idgui; ?> tbody" ).sortable();
$('#datos<?php echo $idgui; ?>')
	.on('click','.btneliminar',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var pnl=$(this).closest('.panel-user');
		var id=pnl.attr('data-id');
		var formData = new FormData();
		formData.append('idcursodetalle', id);			
		var url=_sysUrlBase_+'/acad_cursodetalle/eliminardet';
		$.ajax({
		    url: url,
		    type: "POST",
		    data:  formData,
		    contentType: false,
		    processData: false,
		    dataType:'json',
		    cache: false,
		    beforeSend: function(XMLHttpRequest){ },      
		    success: function(data)
		    {  
				if(data.code=='Error')mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
				else{
		       		pnl.closest('.padding1').remove();
		       		var pd=pnl.closest('.showpnl').find('.panel-user');
		       		if(pd.length==0) pnl.closest('.showpnl').find('.btnreturn').trigger('click');
		       		$('.showpnl[data-idpadre="73"]').remove();
				}
			},
			error: function(e){ },
			complete: function(xhr){ }
		});			
	}).on("click",'.btnchangefoto', function(ev){
		ev.preventDefault();
		ev.stopPropagation();
	    var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
	    selectedfile(ev,this,txt);
	    var img=$(this).attr('data-url');
	    $(img).load(function(ev){
	    	var pnl=$(this).closest('.panel-user');
	    	var id=pnl.attr('data-idrecurso');
	    	var url=$(this).attr('src');    	
	    	url=url.replace(_sysUrlBase_,'__xRUTABASEx__');
	    	addvalorcamponivel<?php echo $idgui; ?>(id,'imagen',url);		    	
	    });
	}).on("click",'.btnreturn', function(ev){
		var showpnl=$(this).closest('.showpnl');
		var pnlreturn=showpnl.attr('data-return');
		if($('#'+pnlreturn).length){		
			showpnl.hide();
			$('#'+pnlreturn).css('display','block').animateCss('zoomIn');
			//$('#'+pnlreturn).show('fast');
		}
	}).on('click','.btneditar',function(ev){
		ev.preventDefault();
		ev.stopPropagation();			
		$('#datos<?php echo $idgui; ?>').find('.btneditar<?php echo $idgui; ?>').removeClass('btneditar<?php echo $idgui; ?>');
		$(this).off('addcontent');
		$(this).addClass('btneditar<?php echo $idgui; ?>');
		var pnl=$(this).closest('.panel-user');		
		var id=pnl.attr('data-id');
		var tipo=pnl.attr('data-tipo');
		var idcurso=$('#pkIdcurso').val();
		var url=_sysUrlBase_+'/acad_cursodetalle/nivelesedit/?plt=modal&idcurso='+idcurso+'&fcall=btneditar<?php echo $idgui; ?>&id='+id+'&vista=vista02&acc=editar&tipo='+tipo;
		var titulo=$(this).attr('title');			
		var obj={url:url,titulo:titulo,borrar:true,cerrarconesc:true,backdrop:false};
    	sysmodal(obj);
    	$(this).on('addcontent',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('addcontent');
			$(this).removeClass('btneditar<?php echo $idgui; ?>');
			var strjson=$(this).attr('data-addcontent');
			var json=JSON.parse(strjson);
			idcursodetalle=json.idcursodetalle;
			idrecurso=json.idrecurso
			$(this).removeAttr('data-addcontent');
			getcursodetalle<?php echo $idgui; ?>(idcursodetalle,idrecurso,pnl);				
		});
	}).on('click','.btnpremio',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var pnl=$(this).closest('.panel-user');
		var id=pnl.attr('data-id');
		$('#unidades<?php echo $idgui; ?>').find('.btnpremio<?php echo $idgui; ?>').removeClass('btnpremio<?php echo $idgui; ?>');
		$(this).off('addpremio');
		$(this).addClass('btnpremio<?php echo $idgui; ?>');	
		var idlogro=pnl.attr('data-idlogro')||0;
		var url=_sysUrlBase_+'/logro/listado/?plt=modal&idlogro='+idlogro+'&fcall=btnpremio<?php echo $idgui; ?>';
		var obj={url:url,titulo:$(this).attr('title'),borrar:true,cerrarconesc:true,backdrop:false,showfooter:true,};
    	sysmodal(obj);
    	$(this).on('addpremio',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('addpremio');
			$(this).removeClass('btnpremio<?php echo $idgui; ?>');
			var idlogro=$(this).attr('data-idlogro');
			pnl.attr('data-idlogro',idlogro);
			addcampodetalle<?php echo $idgui; ?>(id,'idlogro',idlogro);
		});			
	}).on('click','.btnadd',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var idpadre=0;
		var tipo='N';
		$('#datos<?php echo $idgui; ?>').find('.btnaddcontent<?php echo $idgui; ?>').removeClass('btnaddcontent<?php echo $idgui; ?>');
		pnl=$(this).closest('.showpnl');
		idpadre=pnl.attr('data-idpadre');
		tipo=pnl.attr('data-tipo');
		$(this).off('addcontent');
		txttitulo='';
		for (var prop in txttipo_){
			if(prop==tipo) txttitulo=txttipo_[prop];
		}
		var titulo=$(this).attr('title')+' '+txttitulo;
		$(this).addClass('btnaddcontent<?php echo $idgui; ?>');
		var idcurso=$('#pkIdcurso').val();
		var url=_sysUrlBase_+'/acad_cursodetalle/nivelesedit/?plt=modal&idcurso='+idcurso+'&fcall=btnaddcontent<?php echo $idgui; ?>&id='+idpadre+'&vista=vista02&acc=nuevo&tipo='+tipo;
		var obj={url:url,titulo:titulo,borrar:true,cerrarconesc:true,backdrop:false};
		sysmodal(obj);
		$(this).on('addcontent',function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			$(this).off('addcontent');
			$(this).removeClass('btnaddcontent<?php echo $idgui; ?>');
			var strjson=$(this).attr('data-addcontent');
			var json=JSON.parse(strjson);
			idcursodetalle=json.idcursodetalle;
			idrecurso=json.idrecurso
			$(this).removeAttr('data-addcontent');
			getcursodetalle<?php echo $idgui; ?>(idcursodetalle,idrecurso,pnl);
		});
	}).on('click','.btnexamen',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var pnl=$(this).closest('.panel-user');
		var id=pnl.attr('data-id');
		$('#unidades<?php echo $idgui; ?>').find('.btnassesment<?php echo $idgui; ?>').removeClass('btnassesment<?php echo $idgui; ?>');
		$(this).off('addassesment');
		$(this).addClass('btnassesment<?php echo $idgui; ?>');	
		var idlogro=pnl.attr('data-idlogro')||0;
		var url=_sysUrlBase_+'/academico/examenes/?plt=modal&idlogro='+idlogro+'&fcall=btnassesment<?php echo $idgui; ?>';
		var obj={url:url,titulo:'SmartQuiz',borrar:true,cerrarconesc:true,backdrop:false,showfooter:true,};
    	mimodal=sysmodal(obj);
    	mimodal.find('.modal-dialog').css({'width':'90%'});    		
    	$(this).on('addassesment',function(ev){
			ev.preventDefault();
			ev.stopPropagation();				
			$(this).off('addassesment');
			$(this).removeClass('btnassesment<?php echo $idgui; ?>');
			var idexamen=$(this).attr('data-idexamen');
			pnl.attr('data-idexamen',idlogro);
			var enpnl=pnl.attr('data-showpnl');
			var datos={idcursodetalle:-1,idcurso:pnl.attr('data-idcurso'),orden:0,idrecurso:idexamen,tiporecurso:'E',idlogro:0,idpadre:pnl.attr('data-id')};
			addcursodetalle<?php echo $idgui; ?>(enpnl,datos);
		});
	}).on('click','.btnimport',function(ev){
		ev.preventDefault();
		ev.stopPropagation();
		var idpadre=0;
		var tipo='N';
		$('#datos<?php echo $idgui; ?>').find('.btnimport<?php echo $idgui; ?>').removeClass('btnimport<?php echo $idgui; ?>');
		//var pnl=$('#datos<?php echo $idgui;?>');		
		//if(!$(this).hasClass('btnnopadre')){ 
		pnl=$(this).closest('.showpnl');
		idpadre=pnl.attr('data-idpadre');
		tipo=pnl.attr('data-tipo');
		//}
		$(this).off('addimport');
		$(this).addClass('btnimport<?php echo $idgui; ?>');
		//tipo_=tipo=='All'?'N':(tipo=='N'?'U':'L');
		var url=_sysUrlBase_+'/acad_curso/importar/?plt=modal&fcall=btnimport<?php echo $idgui; ?>&tipo='+tipo;
		var titulo=$(this).attr('title');
		var obj={url:url,titulo:titulo,borrar:true,cerrarconesc:true,backdrop:false};
		sysmodal(obj);
		$(this).on('addimport',function(ev){
			ev.preventDefault();
			ev.stopPropagation();							
			$(this).off('addimport');
			$(this).removeClass('btnimport<?php echo $idgui; ?>');
			var strjson=$(this).attr('data-import');
			var json=JSON.parse(strjson);		
			$(this).removeAttr('data-import');
			addjsondetalle<?php echo $idgui; ?>(json,idpadre);
		});
	});
	var load<?php echo $idgui; ?>=function(){
		console.log('controles');
		var addcontrols=$('#datos<?php echo $idgui; ?>').find('.addcontrols');
		$.each(addcontrols,function(i,v){
			var acciones1=$('#controls<?php echo $idgui; ?>').clone(true);
			var showpnl=$(this).closest('.showpnl');
			if(showpnl.hasClass('level_01'))acciones1.find('.btnreturn').remove();			
			$(this).append(acciones1.html());
		});
	}
	load<?php echo $idgui; ?>();
});
</script>