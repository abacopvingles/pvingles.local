<?php
defined("RUTA_BASE") or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$persona=!empty($this->datos)?$this->datos:"";
$idproyecto=!empty($this->idproyecto)?$this->idproyecto:3;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<script src="<?php echo $this->documento->getUrlStatic(); ?>/libs/importexcel/xlsx.full.min.js"></script>
<style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
</style>
<?php if(!$ismodal){ ?>
<div class="row" id="breadcrumb">
  <div class="col-md-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <!--li><a href="<?php //echo $this->documento->getUrlBase();?>/acad">&nbsp;<?php echo JrTexto::_("Matriculas"); ?></a></li-->
        <li class="active">&nbsp;<?php echo JrTexto::_("Student"); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Personal'); ?></li>
        <?php } ?> 
    </ol>
  </div>
</div>
<?php } ?>
<div class="row" id="filtros<?php echo $idgui ?>">
  <div class="col-md-12">
  <form id="frm00<?php echo $idgui; ?>">
    <div class="panel">
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("type")); ?> </label>
              <div class="cajaselect">
                <select name="tipo" id="tipo<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("All")); ?></option>                    
                  <?php if(!empty($this->fktipos)) foreach ($this->fktipos as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>" <?php echo $fk["codigo"]==@$this->fktipo?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Year")); ?> </label>
              <div class="cajaselect">
                <select name="anio" id="anio<?php echo $idgui;?>" class="form-control">                 
                  <?php                  
                  if(!empty($this->anios)) foreach ($this->anios as $fk) {  ?>
                  <option value="<?php echo $fk["anios"]?>" ><?php echo ucfirst($fk["anios"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>         
          <div class="col-xs-12 col-sm-4 col-md-4 notypev<?php echo $idgui; ?>">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Local")); ?> </label>
              <div class="cajaselect">
                <select name="idlocal" id="local<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---")); ?></option>
                  <?php if(!empty($this->fklocales)) foreach ($this->fklocales as $fk) { ?>
                    <option value="<?php echo $fk["idlocal"]?>" <?php echo $fk["idlocal"]==@$this->fkidlocal?'selected="selected"':'';?> ><?php echo $fk["nombre"] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Groups Aula")); ?> </label>
              <div class="cajaselect">
                <select name="idgrupoaula" id="idgrupoaula<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---")); ?></option>
                  <?php if(!empty($this->gruposaula)) foreach ($this->gruposaula as $fk) { ?>
                    <option value="<?php echo $fk["idgrupoaula"]?>" <?php echo $fk["idgrupoaula"]==@$this->fkgrupoaula?'selected="selected"':'';?>><?php echo ucfirst($fk["strgrupoaula"]); ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Course")); ?> </label>
              <div class="cajaselect">
                <select name="idcurso" id="idcurso<?php echo $idgui;?>" class="form-control">
                  <option value=""><?php echo ucfirst(JrTexto::_("---")); ?></option>                  
                </select>
              </div>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Search")); ?> </label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>" >
                  <span class="input-group-addon btn btnbuscar<?php echo $idgui; ?>"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 text-center"><hr>
            <a class="btn btn-warning" id="btnchangevista<?php echo $idgui; ?>" href="javascript:void(0)" data-titulo="<?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?>">
            <i class="fa fa-drivers-license-o"></i> <?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?></a>           
          </div>
        </div>
      </div>
    </div>
  </form>
  </div>
</div>
<div class="col-md-12" >
<div class="row" id="pnlvista<?php echo $idgui; ?>"> 
  <div class="text-center">
        <h3><?php echo ucfirst(JrTexto::_("Select a group please")); ?></h3>
    </div>
 </div>   
</div>
<button class="tmprecargarlista<?php echo $idgui; ?>" style="display: none"></button>
  <div id="nodata<?php echo $idgui; ?>" style="display: none">
    <div class="text-center">
        <h3><?php echo ucfirst(JrTexto::_("Select a group please")); ?></h3>
    </div>    
  </div>
<!--input type="file" name="importjson" id="importjson" class="hide" accept="file_extension|*/xls|*/xlsx|media_type">
<div id="datosimport<?php //echo $idgui; ?>" class=" hide col-md-12">
  <div class="panel panel-body">
    <table class="table table-striped table-responsive tableimport<?php //echo $idgui; ?>">
      <tr>
        <th><?php //echo JrTexto::_("Dni");?></th>
        <th><?php //echo JrTexto::_("Name");?></th>
        <th><?php //echo ucfirst(JrTexto::_("Father's last name")); ?></th>
        <th><?php //echo ucfirst(JrTexto::_("Mother's last name")); ?></th>
        <th><?php //echo ucfirst(JrTexto::_('Mobile number')); ?></th>
        <th><?php //echo ucfirst(JrTexto::_('Telephone')); ?></th>
        <th><?php //echo ucfirst(JrTexto::_('Actions')); ?></th>    
      </tr>
    </table>
    <div class="col-md-12 text-center">
      <a href="javascript:void(0)" class="btnsaveimport<?php //echo $idgui; ?> btn btn-primary"><i class="fa fa-save"></i> <?php //echo JrTexto::_('Save Import') ?></a>
    </div>
  </div>
</div-->

<script type="text/javascript">
$(document).ready(function(){
  var vista=window.localStorage.vistapnl||1;
  var idproyecto=parseInt(<?php echo $idproyecto;?>);
  var cargarvista<?php echo $idgui;?>=function(view){
    var idcurso=$('select#idcurso<?php echo $idgui;?>').val()||'';
    var texto=$('#texto<?php echo $idgui; ?>').val()||'';
    /*if(idcurso==''){     
      $('#pnlvista<?php //echo $idgui; ?>').html($('#nodata<?php //echo $idgui; ?>').html());
      return false;
    }*/
    vista=window.localStorage.vistapnl;
    var _vista=view||vista||1;
    var frmtmp = document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);   
    formData.append('vista',_vista);
    formData.append('plt','blanco');
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/alumno/listado',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'html',
      callback:function(data){
        $('#pnlvista<?php echo $idgui; ?>').html(data);
      }
    }
    sysajax(data);
  }

  var cargarcursos<?php echo $idgui;?>=function(){
    var ambtmp=$('select#idcurso<?php echo $idgui;?>');
    var idgrupoaula=$('select#idgrupoaula<?php echo $idgui;?>').val()||'';
    if(idgrupoaula==''){
      $('option:first-child',ambtmp).siblings().remove();
      return false;
    }
    $('select#idcurso<?php echo $idgui;?>').val('')
    var frmtmp = document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);   
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(data){
          var tr=data.data; 
          $('option:first-child',ambtmp).siblings().remove();
          if(tr!=''){
            var dt=[];
              $.each(tr,function(i,v){ 
                if(dt[v['idcurso']]==undefined){
                  ambtmp.append('<option value="'+v['idcurso']+'" '+((i==0)?'selected="selected"':'')+'>'+v["strcurso"]+'</option>');
                  dt[v['idcurso']]='a';
                }
            });
          }          
          $('#idcurso<?php echo $idgui;?>').trigger('change');
        }
      }
      sysajax(data);
  }

  var cargargrupos<?php echo $idgui;?>=function(){
    var ambtmp=$('select#idgrupoaula<?php echo $idgui;?>');
    var frmtmp = document.getElementById('frm00<?php echo $idgui; ?>');
    var formData = new FormData(frmtmp);
    formData.append('idgrupoaula','');
    formData.append('idproyecto',idproyecto);
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/acad_grupoaula/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        callback:function(data){
          var tr=data.data; 
          $('option:first-child',ambtmp).siblings().remove();
          if(tr!=''){
              $.each(tr,function(i,v){ 
                ambtmp.append('<option value="'+v['idgrupoaula']+'" '+((i==0)?'selected="selected"':'')+'>'+v["nombre"]+'</option>');
            });
          }
          //cargarcursos<?php echo $idgui;?>();
          $('select#idgrupoaula<?php echo $idgui;?>').trigger('change');
        }
      }
      sysajax(data);
  }
  cargargrupos<?php echo $idgui;?>();

  $('#pnlvista<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5976271b3962a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true,tam:'85%'});    
  });

  $('select#tipo<?php echo $idgui;?>').change(function(ev){
    var vtmp_=$(this).val().toLowerCase();
    if(vtmp_=='v'){
      $('.notypev<?php echo $idgui; ?>').hide();
      $('#local<?php echo $idgui;?>').val('');
      //$('#ambiente<?php //echo $idgui;?>').val('');
    }else{
      $('.notypev<?php echo $idgui; ?>').show();
    }
     cargargrupos<?php echo $idgui;?>();
  });

   $('#idcurso<?php echo $idgui;?>').change(function(ev){
       cargarvista<?php echo $idgui;?>();
  });

  $('select#idgrupoaula<?php echo $idgui;?>').change(function(ev){
     cargarcursos<?php echo $idgui;?>();   
  });

  $('select#local<?php echo $idgui;?>').change(function(ev){
     cargargrupos<?php echo $idgui;?>();
  });

  $('#btnchangevista<?php echo $idgui; ?>').click(function(){
    vista=window.localStorage.vistapnl||1;
    vista=vista==1?2:1;
    localStorage.setItem('vistapnl',vista);
    cargarvista<?php echo $idgui;?>(vista);
  });

  $('.tmprecargarlista<?php echo $idgui; ?>').on('recargarlista',function(ev){
    cargarvista<?php echo $idgui;?>();
  });

  $('.btnbuscar<?php echo $idgui; ?>').click(function(ev){
    cargarvista<?php echo $idgui;?>();
  });

  $('#texto<?php echo $idgui; ?>').keypress(function(event) {
    var keycode = event.keyCode || event.which;
    if(keycode == '13') {
      cargarvista<?php echo $idgui;?>();
      return false;
    }
  });

  $('selected#idgrupoaula<?php echo $idgui;?>').trigger('change');

  $('#pnlvista<?php echo $idgui; ?>').on('click','.btn_removetr',function(ev){
    $(this).closest('tr').remove();
  })
});
</script>