<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->cursos)) $cursos=$this->cursos;
//var_dump($grupos);
?>
<style type="text/css">
  #panel<?php echo $idgui; ?>{

  }
  #btnadd<?php echo $idgui; ?>{
    position: absolute;
    right: 2em;
    top:1ex;
    z-index: 2;
  }
  .form-control,.form-control[readonly]{    border: 1px solid #4583af !important;}
</style>
<div class="row">
<div class="panel">
	<div class="panel-body" id="panel<?php echo $idgui; ?>" >
    <!--a href="<?php //echo $this->documento->getUrlBase(); ?>/acad_grupoaula/formulario" id="btnadd<?php //echo $idgui; ?>" class="btn btn-warning btn-xs"><i class="fa fa-plus"></i> <?php //echo JrTexto::_('Add') ?></a-->
<table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
        	<th><?php echo JrTexto::_("Curso") ;?></th>         
        	<th><?php echo JrTexto::_("Docente") ;?></th>        	
          <th><?php echo JrTexto::_("Fecha de inicio") ;?></th>
          <th><?php echo JrTexto::_("Fecha final") ;?></th>                  
        	<th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
        $url=$this->documento->getUrlBase();     
  		if(!empty($cursos))
  			foreach ($cursos as $rw){ $i++;             
  			?>
  				<tr data-idcurso="<?php echo $rw["idcurso"]; ?>">
  					<td><?php echo $i; ?></td>
  					<td><?php echo $rw["nombre"];?></td>
  					<td>
              <div class="input-group">
                <input type="hidden" class="iddocente" name="iddocente" id="iddocente5b8a411c4f43d" value="<?php echo !empty($rw["iddocente"])?$rw["iddocente"]:0;?>">	
                <input type="text" readonly="readonly" name="strdocente" id="strdocente5b8a411c4f43d" class="form-control border0" placeholder="Nombre del docente" value="<?php echo @$rw["strdocente"];?>">
                <span class="input-group-addon btn btn-primary btnbuscardocenterow" data-modal="si" > <i class="fa fa-search"></i></span>  
              </div>
            </td>           
            <td><input type="date" class="fechainicio form-control border0" value="<?php echo !empty($rw["fecha_inicio"])?$rw["fecha_inicio"]:0;?>" ></td>
            <td><input type="date" class="fechafinal form-control border0" value="<?php echo !empty($rw["fecha_final"])?$rw["fecha_final"]:0;?>" ></td>
  					<td data-idgrupoaula="<?php echo $rw["idcurso"]; ?>" >  						
              <a class="btn-guardar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('Save'); ?>"> <i class="fa fa-save"></i></a>              
              <a class="btn-eliminar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('delete'); ?>" ><i class="fa fa-trash-o"></i></a>
  					</td>
  				</tr>
  		<?php }	?>
    </tbody>
</table>
	</div>       
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#table_<?php echo $idgui; ?>').DataTable({
		"searching": false,
    		"processing": false
		<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
	}).on('click','.btn-eliminar',function(ev){
    var idgrupoaula=$(this).closest('td').attr('data-idgrupoaula');
    var idgrupoauladetalle=$(this).closest('td').attr('data-idgrupoauladet');
    var btn=$(this);
    var formData = new FormData();    
    formData.append('idgrupoaula', idgrupoaula);
    formData.append('idgrupoauladetalle',idgrupoauladetalle );
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_grupoauladetalle/eliminar_grupoauladetalle',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      callback:function(rs){        
        btn.closest('tr').remove();
      }
    }
    sysajax(data);
    return false; 
  }).on('click','.btn-editar',function(ev){
    var idgrupoaula=$(this).closest('td').attr('data-idgrupoaula');
    var idgrupoauladetalle=$(this).closest('td').attr('data-idgrupoauladet');
    window.location.href=_sysUrlBase_+'/acad_grupoaula/formulariogrupo/?idgrupoaula='+idgrupoaula;
    //console.log(idgrupoaula,  idgrupoauladetalle);
  }).on('click','.btn-guardar',function(ev){
    var tmptr=$(this).closest('tr');
    var fd= new FormData();
    fd.append('txtIdgrupoaula',parseInt($('select[name="grupos"]').val()));
    fd.append('txtIdcurso',parseInt(tmptr.attr('data-idcurso')));
    fd.append('txtIddocente',tmptr.find('input.iddocente').val());
    fd.append('txtFecha_inicio',tmptr.find('input.fechainicio').val());
    fd.append('txtFecha_final',tmptr.find('input.fechafinal').val());
    fd.append('txtNombre','virtual'); 
    fd.append('txtFecha_final',tmptr.find('input.fechafinal').val()); 
    sysajax({ 
            fromdata:fd,
            showmsjok:true,
            url:_sysUrlBase_+'/acad_grupoauladetalle/guardarAcad_grupoauladetalle',
            callback:function(rs){
               dt=rs.data;
            }
        });

  })
  $('#btnadd<?php echo $idgui; ?>').click(function(){
    window.location.href=_sysUrlBase_+'/acad_grupoaula/formulario';  
  })

  	$('.btnbuscardocenterow').click(function(e){
 		var clstmp=Date.now();
 		var enmodal=$(this).attr('data-modal')||'no';
	    var fcall=$(this).attr('data-fcall')||clstmp;
	    var url=_sysUrlBase_+'/personal/?fcall='+fcall+'&datareturn=true';
	    var claseid='docente_'+clstmp;
	    var show={header:true,footer:false,borrarmodal:true};
	    var titulo=$(this).attr('data-titulo')||'';
	    titulo='<?php echo JrTexto::_('Teacher'); ?>';     
	    if(enmodal=='no'){ return redir(url); }
	    url+='&plt=modal';
	    $(this).addClass('tmp'+clstmp);
	    $(this).on('returndata',function(ev){
	    	$(this).removeClass('tmp'+clstmp);
	    	var tmpdata=JSON.parse($(this).attr('data-return'));
	    	$(this).siblings('[name="iddocente"]').val(tmpdata.id);
	    	$(this).siblings('[name="strdocente"]').val(tmpdata.dni+': '+tmpdata.nombre);
	    });
	    openModal('lg',titulo,url,true,claseid,show);
 	});

})
</script>