<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$imgcursodefecto='/static/media/nofoto.jpg';
if(!empty($frm['imagen'])) $imagen=$frm['imagen'];
else $imagen=$imgcursodefecto;
$urlmedia=$this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .infototal{ padding: 1em 1ex; }
  .infototal .titulo{ font-size: 1.2em; }
  .infototal .numberinfo{ font-size: 0.9em; color: #ca3c3c; }
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <?php if($this->documento->plantilla!='modal'){?>
  <div class="row" id="breadcrumb">
    <div class="col-xs-12">
      <div style="position: relative;">
      <ol class="breadcrumb">
          <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico/curso">&nbsp;<?php echo JrTexto::_('course'); ?></a></li>
          <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion).' '.JrTexto::_('Course'); ?></li> 
      </ol>
      <ul style="position: absolute; top:1ex; right: 1ex;">        
          <li class="btn btn-xs btn-default"><i class="fa fa-print"></i> <?php echo JrTexto::_('Print'); ?></li>
          <li class="btn btn-xs btn-default view01 btn-changeview<?php echo $idgui; ?>"  data-view02="<?php echo JrTexto::_('View').' 02';?>"  data-view01="<?php echo JrTexto::_('View').' Minedu';?>"><i class="fa fa-eye"></i> <span><?php echo JrTexto::_('View').' Minedu'; ?></span></li>  
      </ul>
     </div>
    </div>
  </div>
  <?php }?>
    <div class="panel">
      <?php if($this->documento->plantilla=='modal'){?>
      <div class="panel-heading bg-blue">
        <div><?php echo JrTexto::_('Course'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></div>
        <div class="clearfix"></div>
      </div>
      <?php } ?> 
      <div class="panel-body" id="pnl<?php echo $idgui ?>">
        <form id="frmdatosdelcurso" method="post" target="" enctype="multipart/form-data">
        <input type="hidden" name="idcurso" id="pkIdcurso" value="<?php echo $this->pk;?>">
        <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->pk;?>">              
        <div class="col-md-12">
        <div id="msj-interno">
        </div> 
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label style="">I. <?php echo JrTexto::_('Nombre del curso');?></label> 
                        <input type="text" autofocus name="nombre" required="required"  class="form-control" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder="<?php echo  ucfirst(JrTexto::_("Curso 01"))?>" >					           
                    </div>
                    <div class="col-md-12 col-sm-12 form-group">							        	
                        <label style=""><?php echo JrTexto::_('Autor');?></label> 
                        <input type="text" name="autor" value="<?php echo ucfirst(@$frm["autor"]); ?>"  class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Apellidos y Nombres del autor "))?>">					           
                    </div>
                    <div class="col-md-12 col-sm-12 form-group">
                        <label style=""><?php echo JrTexto::_('Fecha de publicación');?></label> 
                        <input type="date" name="aniopublicacion" class="form-control" value="<?php echo !empty($frm["aniopublicacion"])?$frm["aniopublicacion"]:date('Y-m-d'); ?>" >
                    </div>
                    <div class="col-md-12 form-group">
                        <label style=""><?php echo JrTexto::_('Reseña');?></label> 
                        <textarea name="descripcion" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Describe un resumen del curso"))?>"><?php echo @$frm['descripcion'];?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label><?php echo JrTexto::_("Imagen de curso") ?> </label> 
                        <img style="max-width: 200px; max-height: 150px;" src="<?php echo $urlmedia.$imagen; ?>" alt="foto" class="cargaimagen<?php echo $idgui ?> img-responsive thumbnail centrado" data-url=".cargaimagen<?php echo $idgui ?>"  data-tipo="image" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>">
                        <input type="hidden" name="imagen" id="imagen<?php echo $idgui ?>" value="<?php echo $imagen==$imgcursodefecto?'':$urlmedia.$imagen; ?>">        
                    </div>
                    <div class="col-12 col-sm-12 form-group text-left " style="padding-top:1em;">		        	
                        <label style=""><?php echo JrTexto::_('Color del curso');?> </label> <br>
                        <input type="" name="color" value="rgb(0,0,0,0)" class="vercolor form-control" >			       
                    </div>
                    <div class="col-md-12 form-group">                
                      <label><?php echo ucfirst(JrTexto::_("State")); ?> </label>                 
                      <div class="select-ctrl-wrapper select-azul">
                        <select name="estado" id="cbestado<?php echo $idgui ?>" class="form-control select-ctrl">                    
                        <option value="1"<?php @$frm['estado']=='1'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Publicado"))?></option>
                        <option value="0"<?php @$frm['estado']=='0'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Sin publicar"))?></option>
                      </select>                    
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
               <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Unit')); ?></label></div>
                  <div class="numberinfo"><span id="nunidad<?php echo $idgui ?>"><?php echo  (!empty($frm["nunidad"])?$frm["nunidad"]:'0').'</span> '.ucfirst(JrTexto::_('Unit')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Activities')); ?></label></div>
                  <div class="numberinfo"><span id="nactividad<?php echo $idgui ?>"><?php echo (!empty($frm["nactividad"])?$frm["nactividad"]:'0').'</span> '.ucfirst(JrTexto::_('Activities')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Assessment')); ?></label></div>
                  <div class="numberinfo"><span id="nexamen<?php echo $idgui; ?>"><?php echo (!empty($frm["nexamenes"])?$frm["nexamenes"]:'0').'</span> '.ucfirst(JrTexto::_('Assessment')); ?></div>
               </div>
            </div>
          </div>
          </div>
        </form>
       
          <div class="clearfix"><br></div>
          <div class="row">            
              <div class="col-md-12 text-center">
                <a id="btn-savecurso" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </a>
                <a class="btn btn-warning btn-close" href="<?php echo $this->documento->getUrlBase();?>/academico/curso" data-dismiss="modal">
                <i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>                
              </div>
          </div>
          <div class="clearfix"><hr></div>          
          <div class="row" id="pnldetalle<?php echo $idgui; ?>">
          </div>
          <div class="row" id="newventana<?php echo $idgui;?>">
                
          </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
/*var ventanaback='#pnldetalle<?php //echo $idgui; ?>';*/
var url_media='<?php echo $urlmedia;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:''},
    infoindice:'top',
    infoavance:0
}
var tmpjsoncurso=<?php echo !empty($frm['txtjson'])?$frm['txtjson']:'{}'; ?>;
try{
   $.extend(jsoncurso,tmpjsoncurso);
}catch(ex){};

var curvista=sessionStorage.getItem('vista')||'vista01';
sessionStorage.setItem('vista',curvista);
var recargarvista<?php echo $idgui; ?>=function(vista){
  vista=vista||sessionStorage.getItem('vista');
  var formData = new FormData();
  formData.append('idcurso', $('#idcurso').val());
  formData.append('vista', vista);
  formData.append('plt', 'blanco');  
  var url=url_media+'/acad_cursodetalle/vista';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    //dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {     
        $('#pnldetalle<?php echo $idgui; ?>').html(data);      
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}

var refreshdatoscurso=function(){
  var formData = new FormData();
  formData.append('idcurso', $('#idcurso').val());
  var url=_sysUrlBase_+'/acad_curso/buscarjson';
  $.ajax({
    url: url,
    type: "POST",
    data:  formData,
    contentType: false,
    processData: false,
    dataType:'json',
    cache: false,
    beforeSend: function(XMLHttpRequest){ },      
    success: function(data)
    {       
      if(data.code==='ok'){ 
        if(data.data[0]!=undefined){
          info=data.data[0];
          $('#nunidad<?php echo $idgui ?>').text(info.nunidad||0);
          $('#nactividad<?php echo $idgui ?>').text(info.nactividad||0);
          $('#nexamenes<?php echo $idgui ?>').text(info.nexamen||0);
        }             
                  
      }else{
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
      }
    },
    error: function(e){ },
    complete: function(xhr){ }
  });
}

var refreshvistapadre=function(){
   recargarvista<?php echo $idgui; ?>();
   refreshdatoscurso();
}

$(document).ready(function(){ 
  var idcurso=parseInt($('#idcurso').val()||'0');
  $('#frmdatosdelcurso').on('submit',function(ev){
        ev.preventDefault();
        let imagen=$('input#imagen<?php echo $idgui ?>').val()||'';
        let f=imagen.replace(url_media, '');
          $('input#imagen<?php echo $idgui ?>').val(f);
        if(idcurso==0){
            jsoncurso.estructura.color=$('input[name="color"]').val();
            jsoncurso.infoportada.titulo=$('input[name="nombre"]').val();
            jsoncurso.infoportada.descripcion=$('textarea[name="descripcion"]').val();
            jsoncurso.infoportada.image=f;
            jsoncurso.infoavance=15;
        }
        var frm = document.getElementById("frmdatosdelcurso");  
        var data=new FormData(frm);
        data.append('txtjson',JSON.stringify(jsoncurso));
        sysajax({   
                fromdata:data,
                url:url_media+'/smartcourse/acad_curso/guardarjson',               
                callback:function(rs){                  
                if(rs.code==200){
                    idcurso=rs.newid;                    
                    $('#idcurso').val(idcurso);
                    var data2=new FormData();
                        data2.append('idproyecto',idproyecto);
                        data2.append('idcurso',idcurso);
                    sysajax({
                            showmsjok:false,
                            fromdata:data2,
                            url:url_media+'/proyecto/proyecto_cursos/guardarProyecto_cursos',
                            callback:function(rs){refreshvistapadre();}
                    })
                }
            }
        })
    });




$('#pnl<?php echo $idgui ?>').on('click','#btn-savecurso',function(ev){
  $('#frmdatosdelcurso').trigger('submit');
}).on('click','#btn-verdetalle',function(ev){
  refreshvistapadre();
});

$('.btn-changeview<?php echo $idgui; ?>').on('click',function(ev){
   curvista=sessionStorage.getItem('vista')||'vista01';
   if(curvista=='vista01'){
      curvista='vista02';
      $(this).find('span').text($(this).attr('data-view02'));
   }else{
      curvista='vista01';
      $(this).find('span').text($(this).attr('data-view01'));
   }
   sessionStorage.setItem('vista',curvista);
   recargarvista<?php echo $idgui; ?>(curvista);
});


$('.cargaimagen<?php echo $idgui ?>').click(function(e){ 
  var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
  selectedfile(e,this,txt);
});

$('.cargaimagen<?php echo $idgui ?>').load(function() {
  var src = $(this).attr('src');
  if(src!=''){ $(this).siblings('input').val(src); }
});

var load<?php echo $idgui ?>=function(){ if(idcurso>0){ refreshvistapadre(); }}
load<?php echo $idgui ?>();   
});
</script>