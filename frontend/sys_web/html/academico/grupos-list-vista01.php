<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!="modal"?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->grupos)) $grupos=$this->grupos;
//var_dump($grupos);
?>
<style type="text/css">
  #panel<?php echo $idgui; ?>{

  }
  #btnadd<?php echo $idgui; ?>{
    position: absolute;
    right: 2em;
    top:1ex;
    z-index: 2;
  }
</style>
<div class="col-md-12">
<div class="panel">
	<div class="panel-body" id="panel<?php echo $idgui; ?>" >
    <!--a href="<?php //echo $this->documento->getUrlBase(); ?>/acad_grupoaula/formulario" id="btnadd<?php //echo $idgui; ?>" class="btn btn-warning btn-xs"><i class="fa fa-plus"></i> <?php //echo JrTexto::_('Add') ?></a-->
<table class="table table-striped table-responsive" id="table_<?php echo $idgui; ?>">
  	<thead>
    	<tr class="headings">
      		<th>#</th>
        	<th><?php echo JrTexto::_("Nombre") ;?></th>
          <th><?php echo " N° ".JrTexto::_("vacantes"); ?></th> 
        	<th><?php echo JrTexto::_("Type") ;?></th>        	
          <th><?php echo JrTexto::_("Estado") ;?></th>
          <th><?php echo JrTexto::_("Cursos") ;?></th>                  
        	<th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
    	</tr>
  	</thead>
  	<tbody>
  		<?php 
  		$i=0;
      $url=$this->documento->getUrlBase();     
  		if(!empty($grupos))
  			foreach ($grupos as $rw){ 
            $i++;             
  					?>
  				<tr>
  					<td><?php echo $i; ?></td>
  					<td><?php echo $rw["nombre"];?></td>
            <td><?php echo $rw["nvacantes"];?></td>
  					<td><?php echo $rw["strtipo"]; ?></td>           
            <td><?php echo $rw["strestadogrupo"]; ?></td>
            <td><?php echo count($rw["detalle"]); ?></td>
            <!--td><?php //echo $rw["fecha_inicio"]." <br> ".$rw["fecha_final"]; ?></td-->
  					<td data-idgrupoaula="<?php echo $rw["idgrupoaula"]; ?>" >
  						<!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/verficha/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('Ficha')." ".$fullnombre; ?>"><i class="fa fa-user"></i><i class="fa fa-eye"></i></a-->
              <a class="btn-editar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('Edit'); ?>"> <i class="fa fa-pencil"></i></a>
              <!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/cambiarclave/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>
              <a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/personal/notas/?id=<?php echo $rw["dni"]; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('notes');?>"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a-->
              <!--a class="btn btn-xs btnvermodal" data-modal="si" href="<?php echo $url; ?>/acad_grupoaula/horario/?idgrupoauladetalle=<?php echo $rw["idgrupoauladetalle"]; ?>" data-titulo="<?php echo JrTexto::_('View')." ".JrTexto::_('horario') ?>"><i class="fa fa-calendar"></i></a-->
              <!--a class="btn-eliminar btn btn-xs" href="javascript:void(0);" data-titulo="<?php echo JrTexto::_('delete'); ?>" ><i class="fa fa-trash-o"></i></a-->
  					</td>
  				</tr>
  		<?php }	?>
    </tbody>
</table>
	</div>       
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#table_<?php echo $idgui; ?>').DataTable({
		"searching": false,
    		"processing": false
		<?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
	}).on('click','.btn-eliminar',function(ev){
    var idgrupoaula=$(this).closest('td').attr('data-idgrupoaula');
    var idgrupoauladetalle=$(this).closest('td').attr('data-idgrupoauladet');
    var btn=$(this);
    var formData = new FormData();    
    formData.append('idgrupoaula', idgrupoaula);
    formData.append('idgrupoauladetalle',idgrupoauladetalle );
    var data={
      fromdata:formData,
      url:_sysUrlBase_+'/acad_grupoauladetalle/eliminar_grupoauladetalle',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      callback:function(rs){        
        btn.closest('tr').remove();
      }
    }
    sysajax(data);
    return false; 
  }).on('click','.btn-editar',function(ev){
    var idgrupoaula=$(this).closest('td').attr('data-idgrupoaula');
   // var idgrupoauladetalle=$(this).closest('td').attr('data-idgrupoauladet');
    window.location.href=_sysUrlBase_+'/acad_grupoaula/formulariogrupo/?idgrupoaula='+idgrupoaula;
    //console.log(idgrupoaula,  idgrupoauladetalle);
  });
  $('#btnadd<?php echo $idgui; ?>').click(function(){
    window.location.href=_sysUrlBase_+'/acad_grupoaula/formulario';  
  })
})
</script>