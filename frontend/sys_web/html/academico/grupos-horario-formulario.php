<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datosgrupoaula=!empty($this->datosgrupoaula)?$this->datosgrupoaula:"";
$esvirtual=ucfirst($this->fktipo)=='V'?true:false;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .titulo{
    font-weight: bold;
    font-size: 1.3em;
  }
  hr{
    margin-top: 0px;
    margin-bottom: 1ex;
    border: 0;
    border-top: 1px solid #00BCD4;
  }
  .datepicker {z-index: 9999! important}
  .generado.inactive{ display:none;}
  .generado>table>tr>td{padding: 0.5ex; vertical-align: middle;}
	.form-control,.form-control[readonly]{    border: 1px solid #4583af !important;}
</style>
<?php if(!$ismodal){?>
<div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/acad_grupoaula">&nbsp;<?php echo JrTexto::_("Groups"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Edit"); ?></li>       
    </ol>
  </div>
</div>
<?php } ?>
<div class="panel" >      
  	<div class="panel-body">
		<form id="frmhorario00<?php echo $idgui; ?>">
        	<input type="hidden" name="idgrupoaula" class="idgrupoaula" id="idgrupoaula<?php echo $idgui; ?>" value="<?php echo @$this->idgrupoaula  ?>">
        	<input type="hidden" name="idgrupoauladetalle" class="idgrupoauladetalle" id="idgrupoauladetalle<?php echo $idgui; ?>" value="<?php echo @$this->idgrupoauladetalle;  ?>">        	
        	<input type="hidden" name="nvacantes" id="nvacantes<?php echo $idgui; ?>" value="<?php echo @$datosgrupoaula["nvacantes"] ?>">
        	<input type="hidden" name="estado" id="estado<?php echo $idgui; ?>" value="<?php echo @$datosgrupoaula["estado"]  ?>">
        	<input type="hidden" name="comentario" id="comentario<?php echo $idgui; ?>" value="<?php echo @$datosgrupoaula["comentario"]  ?>">
        	<input type="hidden" name="tipo" id="tipo<?php echo $idgui; ?>" value="<?php echo $this->fktipo;  ?>">
           	<div class="col-xs-12 col-sm-6 col-md-6" >
              <div class="form-group" style="display:none">
                <label><?php echo ucfirst(JrTexto::_("Nombre")); ?></label>
                <input type="text" name="nombre" id="nombre<?php echo $idgui; ?>" required class="form-control border0" value="<?php echo @$datosgrupoaula["nombre"]; ?>" placeholder="<?php echo  ucfirst(JrTexto::_("Ej. Ingles 01"))?>">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 text-right">
              <div class="form-group">
                <label><?php echo ucfirst(JrTexto::_("Color in calendar")); ?></label><br>
                <input type="color" name="color" id="color<?php echo $idgui; ?>" value="<?php echo !empty($this->color)?$this->color:'#2b8ace'; ?>">
              </div>
            </div>
            <div class="clearfix"></div>
		   	<div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
              	<label><?php echo ucfirst(JrTexto::_("Course")); ?></label>
                <div class="input-group">
                  <input type="hidden" name="idcurso" id="idcurso<?php echo $idgui; ?>" value="<?php echo @$this->fkcurso; ?>">	
                  <input type="text" readonly="readonly" name="strcurso" id="strcurso<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Ingles 01"))?>" value="<?php echo @$this->strcurso; ?>">
                  <span class="input-group-addon btn btn-primary" data-modal="si" id="btnbuscarcurso<?php echo $idgui; ?>"> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
              	<label><?php echo ucfirst(JrTexto::_("Teacher")); ?></label>
                <div class="input-group">
                  <input type="hidden" name="iddocente" id="iddocente<?php echo $idgui; ?>" value="<?php echo @$this->fkdocente; ?>">	
                  <input type="text" readonly="readonly" name="strdocente" id="strdocente<?php echo $idgui; ?>" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Abel Chingo Tello"))?>"  value="<?php echo @$this->strdocente; ?>">
                  <span class="input-group-addon btn btn-primary" data-modal="si" id="btnbuscardocente<?php echo $idgui; ?>"> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 <?php echo $esvirtual?'hide':'';?>">
	            <div class="form-group">
	              <label><?php echo ucfirst(JrTexto::_("Local")); ?></label>
	              <div class="cajaselect">
	                <select name="local" id="local<?php echo $idgui;?>" class="form-control">			                  
	                  <?php 
	                  if(!empty($this->fklocales)) foreach ($this->fklocales as $fk) { ?>
	                    <option value="<?php echo $fk["idlocal"]?>" <?php echo $fk["idlocal"]==@$this->fklocal?'selected="selected"':'';?>><?php echo ucfirst($fk["nombre"]); ?></option>
	                  <?php } ?>
	                </select>
	              </div>
	            </div>
	         </div>
	         <div class="col-xs-12 col-sm-6 col-md-6 <?php echo $esvirtual?'hide':'';?>">	         	
	            <div class="form-group">
	              <label><?php echo ucfirst(JrTexto::_("Ambiente")); ?></label>
	              <div class="cajaselect">
	                <select name="ambiente" id="ambiente<?php echo $idgui;?>" class="form-control">
	                  <!--option value=""><?php //echo ucfirst(JrTexto::_("All")); ?></option-->
	                  <?php 
	                  if(!empty($this->fkambientes)) foreach ($this->fkambientes as $fk) { ?>
	                    <option value="<?php echo $fk["idambiente"]?>" <?php echo $fk["idambiente"]==@$this->fktipo?'selected="selected"':'';?>><?php echo $fk["numero"].' '.$fk["tipo_ambiente"]; ?></option>
	                  <?php } ?>
	                </select>
	              </div>
	            </div>
	        </div>
		    <div class="clearfix"></div>                                                         
            <div class="col-xs-12 col-sm-6 col-md-6">
	          	<div class="form-group">
	          		<label><?php echo ucfirst(JrTexto::_("Start Date")); ?> </label> <span><?php echo date('Y-m-d'); ?></span>
	          		<div class="input-group date datetimepicker">
	          			<input type="text" class="form-control fechadatetime<?php echo $idgui; ?>" required name="fecha_inicio" id="fecha_inicio<?php echo $idgui; ?>" value="<?php echo @$this->fechafin ;?>"> 
		              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	          		</div>	          
	          	</div>			          
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6"> 
	          	<div class="form-group">	          		
	          		<label><?php echo ucfirst(JrTexto::_("Finish Date")); ?> </label> <span><?php echo date('Y-m-d'); ?></span>
	          		<div class="input-group date datetimepicker">
	          			<input type="text" class="form-control fechadatetime<?php echo $idgui; ?>" required name="fecha_final" id="fecha_final<?php echo $idgui; ?>" value="<?php echo @$this->fechafin ;?>"> 
		              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	          		</div>
	          	</div>
	        </div>
		        <!--div class="col-xs-12 col-sm-6 col-md-4 text-center">
		        	<?php //echo JrTexto::_('Accumulated Hours'); ?><br><span id="houracumulada<?php echo $idgui ?>">00:00</span>
		        </div-->
	        <div class="clearfix"></div>
	        <div class="col-md-12 titulo"><br><a href="javascript:void(0);" class="btn btn-xs btn-warning pull-right btnaddhour<?php $idgui; ?>"><i class="fa fa-plus"></i><?php echo JrTexto::_('Add hour'); ?></a> - <?php echo JrTexto::_('Generar Horarios') ?><hr>	        	
	        </div>
	        <div class="col-xs-12 col-sm-12 col-md-12" id="addhour<?php echo $idgui; ?>">
	        	<div class="itemhora<?php echo $idgui; ?>" id="clonehour<?php echo $idgui; ?>" style="display: none;" data-padre="0">
		            <div class="col-xs-12 col-sm-6 col-md-3">
			            <div class="form-group">
			              <label><?php echo ucfirst(JrTexto::_("Day of week")); ?> </label>
			              <div class="cajaselect">
			                <select name="diasemana[]" class="form-control diasemana<?php  echo $idgui; ?>">
			                	<option value="0"><?php echo ucfirst("Sunday"); ?></option>
			                    <option value="1"><?php echo ucfirst("Monday"); ?></option>
			                    <option value="2"><?php echo ucfirst("Tuesday"); ?></option>
			                    <option value="3"><?php echo ucfirst("Wednesday"); ?></option>
			                    <option value="4"><?php echo ucfirst("Thursday"); ?></option>
			                    <option value="5"><?php echo ucfirst("Friday"); ?></option>
			                    <option value="6"><?php echo ucfirst("Saturday"); ?></option>				                    				                    
			                </select>
			              </div>
			            </div>
			        </div>
			        <div class="col-xs-12 col-sm-6 col-md-3">
			          	<div class="form-group">
			          	<label><?php echo ucfirst(JrTexto::_("start time")); ?></label>
			          	<div class="form-group">
			            	<div class="input-group date datetimepicker">
			              	<input type="text" class="form-control timeini<?php  echo $idgui; ?> time<?php  echo $idgui; ?>" name="hora_inicio[]"  value="<?php echo @$this->horaini; ?>"> 
			              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			            	</div>
			          	</div>
			          </div>			          
			        </div>
			        <div class="col-xs-12 col-sm-6 col-md-3">
			          	<div class="form-group">
			          	<label><?php echo ucfirst(JrTexto::_("Finish time")); ?></label>
			          	<div class="form-group">
			            	<div class="input-group date datetimepicker">
			              	<input type="text" class="form-control timefin<?php  echo $idgui; ?> time<?php echo $idgui; ?>" name="hora_final[]" value="<?php echo @$this->horafin; ?>"> 
			              	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			            	</div>
			          	</div>
			          	</div>			          
			        </div>
			        <div class="col-xs-12 col-sm-6 col-md-3 text-center"><br>
			        	<i class="btn btn-primary btnrandom<?php echo $idgui ?> fa fa-random" title="<?php echo JrTexto::_("Generate");?>"></i>
			        	<i class="btn btn-info btnver<?php echo $idgui ?> fa fa-eye" title="<?php echo JrTexto::_("See");?>"></i>
			        	<i class="btn btn-danger btneliminar<?php echo $idgui ?> fa fa-trash" title="<?php echo JrTexto::_("Delete");?>"></i>
			        </div>
			        <div class="clearfix"></div>
			        <div class="generado inactive panel-body">
			        	<table class="table table-responsive">
			        		<tr>
			        			<th class="numero">#</th>
			        			<th><?php echo JrTexto::_("Day of week") ?></th>
			        			<th><?php echo JrTexto::_("Date start") ?></th>
			        			<th><?php echo JrTexto::_("Date finish") ?></th>
			        			<th><?php echo JrTexto::_("color") ?></th>
			        			<th></th>
			        		</tr>
			        	</table>			        	
			        </div>
			        <div class="clone" style="display: none">
			        	<table>
			        		<tr>
			        			<td class="number"></td>
			        			<td>			        				        
			              			<div class="cajaselect">
						                <select class="form-control diasemanaclone">
						                	<option value="0"><?php echo ucfirst("Sunday"); ?></option>
						                    <option value="1"><?php echo ucfirst("Monday"); ?></option>
						                    <option value="2"><?php echo ucfirst("Tuesday"); ?></option>
						                    <option value="3"><?php echo ucfirst("Wednesday"); ?></option>
						                    <option value="4"><?php echo ucfirst("Thursday"); ?></option>
						                    <option value="5"><?php echo ucfirst("Friday"); ?></option>
						                    <option value="6"><?php echo ucfirst("Saturday"); ?></option>				                    				                    
						                </select>
			              			</div>				            		
				            	</td>
				            <td>				            	
			          			<div class="input-group date datetimepicker">
			          				<input type="text" class="form-control finiclone"  value="<?php echo @$this->fechafin ;?>"> 
				              		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			          			</div> 
				          	</td>
				          	<td>				          		
			          			<div class="input-group date datetimepicker">
			          				<input type="text" class="form-control ffinclone"  value="<?php echo @$this->fechafin ;?>"> 
				              		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
			          			</div>
				          	</td>
				          	<td><input type="color" class="color"></td>
				          	<td><i class="btn btn-danger eliminarclone fa fa-trash" title="<?php echo JrTexto::_("Delete");?>"></i></td>
				          </tr>
				        </table>	        		
			        </div>
		    	</div>
	        </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            	<hr>
                <a type="button" class="btn btn-danger left hidden" id="delete<?php echo $idgui ?>"><i class="fa fa-trash"></i> <?php echo ucfirst(JrTexto::_('Delete Hours')); ?></a>
                <!--a type="button" class="btn btn-warning btntestfechas<?php echo $idgui; ?>" > <?php echo ucfirst(JrTexto::_('test fechas')); ?></a-->
                <a type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-history"></i> <?php echo ucfirst(JrTexto::_('Cancel')); ?></a>
                <button type="submit" class="btn btn-success" id="submit<?php echo $idgui; ?>"><i class="fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save')); ?></button>
            </div>              
        </form>    		
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var idioma = (_sysIdioma_=='ES')?'es':'en';
	var tmpfechas<?php echo $idgui; ?>=[];   
    $('.fechadatetime<?php echo $idgui; ?>').datetimepicker({ /*/lang:'es',  //timepicker:false,*/ format:'YYYY/MM/DD'});
	$('.time<?php echo $idgui; ?>').datetimepicker({ /*/lang:'es', //timepicker:false,*/ format:'h:mm a' });
	var iclone<?php echo $idgui ?>=0;
	var diasemana<?php echo $idgui; ?>=["<?php echo JrTexto::_('Sunday'); ?>", 
				 "<?php echo JrTexto::_('Monday'); ?>","<?php echo JrTexto::_('Tuesday'); ?>","<?php echo JrTexto::_('Wednesday'); ?>","<?php echo JrTexto::_('Thursday'); ?>", "<?php echo JrTexto::_('Friday'); ?>", "<?php echo JrTexto::_('Saturday'); ?>"];
	var calendartime<?php echo $idgui ?>=function(){
		var tmpregistar=$('#addhour<?php echo $idgui; ?>').find('.tmpregistar<?php echo $idgui; ?>');
		tmpfechas<?php echo $idgui; ?>=[];
		if(tmpregistar.length){
			$.each(tmpregistar,function(i,v){
				var color=$(this).find('input.color').val();
				var fini_=moment($(this).find('input._fechaini').val(),'YYYY-MM-DD h:mm a').format('YYYY-MM-DD HH:mm:ss');
				var ffin_=moment($(this).find('input._fechafin').val(),'YYYY-MM-DD h:mm a').format('YYYY-MM-DD HH:mm:ss');
				var tmpaddfechas={
					idhorario:$(this).attr('data-idhorario'),
					idhorariopadre:$(this).attr('data-idpadre'),
					diasemana:$(this).find('select.diasemana').val(),
					title: $('#strcurso<?php echo $idgui; ?>').val(),
			        start: fini_,
			        end: ffin_,
			        backgroundColor:color,
			        color: color
				}
				tmpfechas<?php echo $idgui; ?>.push(tmpaddfechas);
			});
		}
 	}
 
 	<?php if(!empty($this->horarios)){ ?> 		
 		$horariostmp=<?php echo json_encode($this->horarios);?>;
 		var padresadd={};
 		if($horariostmp.length){
 			$.each($horariostmp,function(i,v){ 				
 				var idpadre=v.idhorariopadre;
 				if(padresadd[idpadre]==undefined){
 					padresadd[idpadre]={'datos':v,'hijos':[v]};
 				}else{
 					padresadd[idpadre]['hijos'].push(v);
 				}
 			});
 			$.each(padresadd,function(i,v){
 				dt=v.datos;
 				var hini=moment(dt.start).format('hh:mm a');
 				var hfin=moment(dt.end).format('hh:mm a');
 				var cl=$('#clonehour<?php echo $idgui; ?>').clone();
				cl.removeAttr('data-idhorariopadre').attr('data-id',dt.idhorariopadre);
				cl.removeAttr('style').removeAttr('id').removeAttr('data-padre');
				cl.find('select.diasemana<?php  echo $idgui; ?>').val(dt.diasemana);
				cl.find('input.timeini<?php  echo $idgui; ?>').val(hini);
				cl.find('input.timefin<?php  echo $idgui; ?>').val(hfin);				
				$.each(v.hijos,function(j,vv){
					var trclone=cl.find('.clone table tr').clone();					
					trclone.addClass('tmpregistar<?php echo $idgui; ?>');
					trclone.find('.diasemanaclone').val(vv.diasemana).addClass('diasemana').removeClass('diasemanaclone');
					trclone.find('.finiclone').val(vv.start).addClass('fechaoktmp _fechaini').removeClass('finiclone');
					trclone.find('.ffinclone').val(vv.end).addClass('fechaoktmp _fechafin').removeClass('ffinclone');
					trclone.find('.color').val(vv.color);
					trclone.attr('data-idhorario',vv.id);
					trclone.attr('data-idpadre',dt.idhorariopadre);
					trclone.find('.eliminarclone').removeClass('eliminarclone').addClass('eliminarhorario');
					cl.find('.generado table').append(trclone);
				});
				$('#addhour<?php echo $idgui; ?>').append(cl);
				$('.time<?php echo $idgui; ?>').datetimepicker({ format:'hh:mm a'});
				$('input.fechaoktmp',cl).datetimepicker({ format:'YYYY-MM-DD hh:mm a'});
 			});
 		}
 	<?php } ?>

 	$('#frmhorario00<?php echo $idgui; ?>').bind({    
     submit: function(event){
      	event.preventDefault();
      	calendartime<?php echo $idgui ?>();
      	var idcurso=$('#idcurso<?php echo $idgui; ?>').val();
      	var iddocente=$('#iddocente<?php echo $idgui; ?>').val();
      	if(idcurso==''||idcurso==0){
      		mostrar_notificacion('<?php echo JrTexto::_('Error') ?>', '<?php echo JrTexto::_('Course empty') ?>', 'danger');
      		return false;
      	}
      	if(iddocente==''||iddocente==0){
      		mostrar_notificacion('<?php echo JrTexto::_('Error') ?>', '<?php echo JrTexto::_('teacher empty') ?>', 'danger');
      		return false;
      	}
  		var frmtmp=document.getElementById('frmhorario00<?php echo $idgui; ?>');
        var formData = new FormData(frmtmp);
        formData.append('idlocal', $('#local<?php echo $idgui;?>').val());
        formData.append('idambiente', $('#ambiente<?php echo $idgui;?>').val()); 
        formData.append('fechashorario', JSON.stringify(tmpfechas<?php echo $idgui; ?>));
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_grupoaula/guardarhorario',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok:true,
          callback:function(rs){      
		    <?php if(!empty($ventanapadre)){ ?>
		    var datareturn=rs.datos; 
		    var tmpobj=$('.tmp<?php echo $ventanapadre ?>');
		    if(tmpobj.length){
		        tmpobj.attr('data-return',JSON.stringify(datareturn));
		        tmpobj.on('returndata').trigger('returndata');
		    } 
		    $('#frmhorario00<?php echo $idgui; ?>').closest('.modal').modal('hide'); 
		    <?php } ?>  
          }
        }
        sysajax(data);
     }
    }); 	

 	$('#btnbuscarcurso<?php echo $idgui; ?>').click(function(e){
 		var clstmp=Date.now();
 		var enmodal=$(this).attr('data-modal')||'no';
	    var fcall=$(this).attr('data-fcall')||clstmp;
	    var url=_sysUrlBase_+'/academico/curso/?fcall='+fcall;
	    var claseid='curso_'+clstmp;
	    var show={header:true,footer:false,borrarmodal:true};
	    var titulo=$(this).attr('data-titulo')||'';
	    titulo='<?php echo JrTexto::_('Course'); ?>';     
	    if(enmodal=='no'){
	      return redir(url);          
	    }
	    url+='&plt=modal';
	    $(this).addClass('tmp'+clstmp);
	    $(this).on('returndata',function(ev){
	    	$(this).removeClass('tmp'+clstmp);
	    	var tmpdata=JSON.parse($(this).attr('data-return'));
	    	$('#idcurso<?php echo $idgui; ?>').val(tmpdata.id);
	    	$('#strcurso<?php echo $idgui; ?>').val(tmpdata.nombre);
	    });
	    openModal('lg',titulo,url,true,claseid,show);
 	});

 	$('#btnbuscardocente<?php echo $idgui; ?>').click(function(e){
 		var clstmp=Date.now();
 		var enmodal=$(this).attr('data-modal')||'no';
	    var fcall=$(this).attr('data-fcall')||clstmp;
	    var url=_sysUrlBase_+'/personal/?fcall='+fcall+'&datareturn=true';
	    var claseid='docente_'+clstmp;
	    var show={header:true,footer:false,borrarmodal:true};
	    var titulo=$(this).attr('data-titulo')||'';
	    titulo='<?php echo JrTexto::_('Teacher'); ?>';     
	    if(enmodal=='no'){
	      return redir(url);          
	    }
	    url+='&plt=modal';
	    $(this).addClass('tmp'+clstmp);
	    $(this).on('returndata',function(ev){
	    	$(this).removeClass('tmp'+clstmp);
	    	var tmpdata=JSON.parse($(this).attr('data-return'));
	    	$('#iddocente<?php echo $idgui; ?>').val(tmpdata.id);
	    	$('#strdocente<?php echo $idgui; ?>').val(tmpdata.nombre);
	    });
	    openModal('lg',titulo,url,true,claseid,show);
 	});

	$('.btnaddhour<?php $idgui; ?>').click(function(){
		var idcurso=$('#idcurso<?php echo $idgui; ?>').val();
		if(idcurso==''||idcurso==0){
      		mostrar_notificacion('<?php echo JrTexto::_('Error') ?>', '<?php echo JrTexto::_('Course empty') ?>', 'danger');
      		return false;
      	}
		iclone<?php echo $idgui ?>--;
		var idpadre=iclone<?php echo $idgui ?>;
		var cl=$('#clonehour<?php echo $idgui; ?>').clone();
		cl.attr('data-id',idpadre).removeAttr('id');
		cl.removeAttr('style').removeAttr('data-padre');
		var fini=$('#fecha_inicio<?php echo $idgui; ?>').val();
		var ffin=$('#fecha_final<?php echo $idgui; ?>').val();
		var fini_=moment(fini,'YYYY-MM-DD');
		var ffin_=moment(ffin,'YYYY-MM-DD');
		var diasemana=cl.find('.diasemana<?php  echo $idgui; ?>').val();
		var timeini  =cl.find('.timeini<?php  echo $idgui; ?>').val();
		var timefin  =cl.find('.timefin<?php  echo $idgui; ?>').val();
		var color    =$('#color<?php echo $idgui; ?>').val();
		var curso    =$('#strcurso<?php echo $idgui; ?>').val();
		while(fini_<=ffin_){ 
			var trclone=cl.find('.clone table tr').clone();
			var midate=fini_.toDate();
			var ndiasemana=midate.getDay();
			if(ndiasemana==diasemana){
				var feini=fini_._i+' '+timeini;
				var fefin=fini_._i+' '+timefin;
				trclone.addClass('tmpregistar<?php echo $idgui; ?>');
				trclone.find('.diasemanaclone').val(ndiasemana).addClass('diasemana').removeClass('diasemanaclone');
				trclone.find('.finiclone').val(feini).addClass('fechaoktmp _fechaini').removeClass('finiclone');
				trclone.find('.ffinclone').val(fefin).addClass('fechaoktmp _fechafin').removeClass('ffinclone');
				trclone.find('.color').val(color);
				trclone.attr('data-idhorario',-1);
				trclone.attr('data-idpadre',idpadre);
				trclone.find('.eliminarclone').removeClass('eliminarclone').addClass('eliminarhorario');
				cl.find('.generado table').append(trclone);
			}
			fini_=moment(fini_.add(1, 'd').format('YYYY-MM-DD'));
		}
		$('input.fechaoktmp',cl).datetimepicker({ format:'YYYY-MM-DD hh:mm a'});
		$('#addhour<?php echo $idgui; ?>').append(cl);
		$('.time<?php echo $idgui; ?>').datetimepicker({format:'hh:mm a'});
	});

 	$('#addhour<?php echo $idgui; ?>').on('click','.btneliminar<?php echo $idgui ?>',function(ev){
 		var tr=$(this).closest('.itemhora<?php echo $idgui; ?>');
 		var idhorariopadre=parseInt(tr.attr('data-id'));
 		if(idhorariopadre<0) tr.remove();
 		else{
 			var formData = new FormData();
	        formData.append('idhorariopadre', idhorariopadre);
	        var data={
	          fromdata:formData,
	          url:_sysUrlBase_+'/acad_horariogrupodetalle/eliminarhorarioxpadre',
	          msjatencion:'<?php echo JrTexto::_('Attention');?>',
	          type:'json',
	          showmsjok:true,
	          callback:function(rs){      
			      tr.remove();
	          }
	        }
	        sysajax(data);
 		}
 	}).on('click','.btnrandom<?php echo $idgui ?>',function(ev){
 		$(this).attr('generado','si');
 		var pnl=$(this).closest('.itemhora<?php echo $idgui; ?>');
 		var pnlgenerar=pnl.find('.generado table'); 		
 		var fini=$('#fecha_inicio<?php echo $idgui; ?>').val();
		var ffin=$('#fecha_final<?php echo $idgui; ?>').val();
		var fini_=moment(fini,'YYYY-MM-DD');
		var ffin_=moment(ffin,'YYYY-MM-DD');
		var idpadre=pnl.attr('data-id');	  
		var diasemana=pnl.find('.diasemana<?php  echo $idgui; ?>').val();
		var timeini  =pnl.find('.timeini<?php  echo $idgui; ?>').val();
		var timefin  =pnl.find('.timefin<?php  echo $idgui; ?>').val();
		var color    =$('#color<?php echo $idgui; ?>').val();
		var curso    =$('#strcurso<?php echo $idgui; ?>').val();
		var html='';
		pnlgenerar.find('tr:first-child').siblings('tr').remove();
		while(fini_<=ffin_){ 
			var trclone=pnl.find('.clone table tr').clone();
			var midate=fini_.toDate();
			var ndiasemana=midate.getDay();
			if(ndiasemana==diasemana){
				var feini=fini_._i+' '+timeini;
				var fefin=fini_._i+' '+timefin;
				trclone.addClass('tmpregistar<?php echo $idgui; ?>');
				trclone.find('.diasemanaclone').val(ndiasemana).addClass('diasemana').removeClass('diasemanaclone');
				trclone.find('.finiclone').val(feini).addClass('fechaoktmp _fechaini').removeClass('finiclone');
				trclone.find('.ffinclone').val(fefin).addClass('fechaoktmp _fechafin').removeClass('ffinclone');
				trclone.find('.color').val(color);
				trclone.attr('data-idhorario',-1);
				trclone.attr('data-idpadre',idpadre);
				trclone.find('.eliminarclone').removeClass('eliminarclone').addClass('eliminarhorario');
				pnlgenerar.append(trclone);
			}
			fini_=moment(fini_.add(1, 'd').format('YYYY-MM-DD'));
		}
		$('input.fechaoktmp',pnlgenerar).datetimepicker({ format:'YYYY-MM-DD hh:mm a'});
 	}).on('click','.btnver<?php echo $idgui ?>',function(ev){
 		var pnl=$(this).closest('.itemhora<?php echo $idgui; ?>').find('.generado');
 			pnl.toggleClass('inactive');
 			if(pnl.hasClass('inactive'))
 				$(this).removeClass('fa-eye-slash').addClass('fa-eye');
 			else
 				$(this).removeClass('fa-eye').addClass('fa-eye-slash');

 	}).on('click','.eliminarhorario',function(ev){
 		var tr=$(this).closest('tr');
 		idhorario=tr.attr('data-idhorario');
 		if(idhorario=='-1') tr.remove();
 		else{
 			var formData = new FormData();
	        formData.append('idhorario', idhorario);
	        var data={
	          fromdata:formData,
	          url:_sysUrlBase_+'/acad_horariogrupodetalle/eliminarhorario',
	          msjatencion:'<?php echo JrTexto::_('Attention');?>',
	          type:'json',
	          showmsjok:true,
	          callback:function(rs){      
			      tr.remove();
	          }
	        }
	        sysajax(data);
 		}
 	});
  });
</script>