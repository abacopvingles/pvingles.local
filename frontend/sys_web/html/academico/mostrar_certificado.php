<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
// var_dump($this->persona);
?>
<style type="text/css">
	@media (min-width: 1200px){
		.container {
		    width: 1290px;
		}

	}
	@media (min-width: 768px){
		.container {
		    width: 1290px;
		}

	}
	.panel{
		    width: 1260px !important;
	}
	.certificado{
		/*  background-image: url(<?php echo $this->documento->getUrlBase().'/static/media/academico/certificado.png';?>);*/

	}
	.distanciapersonal{
		margin-top: 26%;
		
	}
	.nombrepersonal{
    color:black;
    font-size:  42px;
    margin-left: 27%;
  }
  .nombrepersonal1{
		color:black;
		font-size:  40px;
		margin-left: 27%;
	}
</style>
<br>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
  <div class="col-md-12 col-sm-12 col-xs-12 panel">
   <div class="panel">         
     <div class="panel-body">
     	<div class="certificado" style="width: 1224px;height: 792px;">
     		<img src="<?php echo $this->documento->getUrlBase();?>/static/media/academico/certificado.png" style='position: absolute;'> 
     	
        <?php if(!empty($this->persona))
        	$nombre = '';
        	$apellido = '';
          $i=0;
              foreach ($this->persona as $pe) { 
                if($i==0){
                  if ($pe["nombre"]!='' && $pe["nombre"]!='-') {
                    $nombre.= $pe["nombre"];
                    if ($pe["ape_paterno"]!='' && $pe["ape_paterno"]!='-') {
                      $apellido.= ' '.$pe["ape_paterno"];
                      if ($pe["ape_materno"]!='' && $pe["ape_materno"]!='-') {
                        $apellido.= ' '.$pe["ape_materno"];
                      }
                    }
                  }
                  $i++;
                }
              	
              	?>
        <?php } 
        ?>
           <?php

            if (strlen($nombre)<23) {
              $tam = 'nombrepersonal';
            }else{
              $tam = 'nombrepersonal1';  
            }
          ?>
          <?php
            if (strlen($apellido)<23) {
              $tam1 = 'nombrepersonal';
            }else{
              $tam1 = 'nombrepersonal1';  
            }
          ?>
          <div class="col-md-8 col-sm-8 col-xs-8 text-right <?php echo $tam;?> distanciapersonal" ><label><?php echo strtoupper($nombre);?></label></div>
          <div class="col-md-8 col-sm-8 col-xs-8 text-right <?php echo $tam1;?>" ><label><?php echo strtoupper($apellido);?></label></div>
        </div>
    </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="panel">         
      <a href="#" id='imprimir' class="btn btn-danger btn-sm  center-block" >
          <i class="btn-icon fa fa-upload"></i>
          <?php echo ucfirst(JrTexto::_('Print')); ?>
      </a>
    </div>
  </div>

<script type="text/javascript">
  $('#imprimir').click(function(){
    $('.certificado').imprimir();
  });
</script>