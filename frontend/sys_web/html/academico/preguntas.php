<style css>
  @media (max-width:768px){
    thead,tbody,.cthead,.ctbody {font-size: small!important;}
    .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th { white-space:initial!important; }
  }
</style>
<div class="container">
    <br> 
    <div class="panel panel-primary">
        <div class="panel-heading"><p>Question</p></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3><?php echo JrTexto::_('Course'); ?></h3>
                    <select id="curso">
                        <?php 
                        if(!empty($this->cursos)){
                            foreach($this->cursos as $v){
                                echo "<option value='{$v['idcurso']}'>{$v['nombre']}</option>";
                            }
                        }
                        ?>
                    </select>
                    <hr/>
                    <h4><?php echo JrTexto::_('Test'); ?></h4>
                    <select id="exam">
                        <?php
                            if(!empty($this->examen)){
                                foreach($this->examen as $v){
                                    echo "<option value='{$v['idexamen']}'>{$v['examen']}</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-sm-3">
                        <div class="card text-white bg-danger" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Listening</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_l"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['L'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-primary" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Reading</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_r"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['R'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-success" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Writing</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_w"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['W'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card text-white bg-warning" style="border-radius:0.5em; margin:10px auto; max-width: 18rem;">
                            <div class="card-header text-center" style="padding:0.5em; border-bottom:1px solid rgba(0,0,0,.125); background:rgba(0,0,0,.03)">Speaking</div>
                            <div class="card-body" style="padding:0.5em;">
                                <h4 class="card-title">Total</h4>
                                <p class="text-center" style="color: white!important; font-size:xx-large;" id="total_s"><?php echo (!empty($this->examen) && !empty($this->examen[0]['general'])) ? $this->examen[0]['general']['S'] : 0 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        <table id="preguntas" class="table table-stripped">
                            <thead>
                                <th>id</th>
                                <th><?php echo JrTexto::_("Question"); ?></th>
                                <th>Listen</th>
                                <th>Read</th>
                                <th>Write</th>
                                <th>Speak</th>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($this->examen)){
                                    $i = 1;
                                    foreach($this->examen[0]['preguntas'] as $p){                                            
                                        echo "<tr>";
                                        echo "<td>{$i}</td>";
                                        echo "<td>{$p['nombre']}</td>";
                                        echo ($p['habilidades']['L'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                        echo ($p['habilidades']['R'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                        echo ($p['habilidades']['W'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                        echo ($p['habilidades']['S'] == true) ? '<td><i class="fa fa-check" aria-hidden="true" style="color:green;"></i></td>' : '<td><i class="fa fa-times" aria-hidden="true" style="color:red;"></i></td>';
                                        echo "</tr>";
                                        ++$i;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var exam = <?php echo (!empty($this->examen)) ? json_encode($this->examen) : 'null' ?>;
$(document).ready(function(){
    $('#curso').change(function(){
        var id = $(this).val();
        $.ajax({
            url: _sysUrlBase_+'/academico/json_preguntas_examen',
            type: 'POST',
            dataType: 'json',
            data: {'idcurso': id},
        }).done(function(resp){
            if(resp.code=='ok'){
                exam = resp.data;

                $('#exam').html(' ');
                var option = '';
                exam.forEach(function(e,i){
                    option = option.concat('<option value="'+ e.idexamen +'">'+ e.examen +'</option>');
                });
                $('#exam').append(option);
                $('#exam').trigger('change');
            }else{
                return false;
            }
        }).fail(function(xhr, textStatus, errorThrown){
            return false;
        });
    });
    $('#exam').change(function(){
        var id = $(this).val();
        exam.forEach(function(elemento,indice){
            if(elemento.idexamen == id){
                $('#total_l').text(elemento.general.L);
                $('#total_r').text(elemento.general.R);
                $('#total_w').text(elemento.general.W);
                $('#total_s').text(elemento.general.S);
                $('#preguntas').find('tbody').html(' ');
                var rows = '';
                elemento.preguntas.forEach(function(e,i){
                    var v1 = (e.habilidades.L == true) ? '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>' : '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                    var v2 = (e.habilidades.R == true) ? '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>' : '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                    var v3 = (e.habilidades.W == true) ? '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>' : '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                    var v4 = (e.habilidades.S == true) ? '<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>' : '<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>';
                    rows = rows.concat('<tr><td>'+ (i+1) + '</td><td>'+ e.nombre +'</td><td>'+v1+'</td><td>'+v2+'</td><td>'+v3+'</td><td>'+v4+'</td></tr>');
                });
                $('#preguntas').find('tbody').append(rows);
            }
        });
    });
});
</script>