<style type="text/css">
	.pnlrol{
		display: inline-block;
	    width: 250px;	   
	    border: 1px solid #ddd;
	    cursor: pointer;
	}
	.img-responsive{
		display: inline-block;
	}
	.titulo00{
		color:rgb(165, 42, 42);
		margin-top: 10%;
		padding: 1.5ex;
	}
	.titulo{
		color:rgb(165, 42, 42);
		padding: 1ex;
	}

	.select-ctrl-wrapper:after {
    background: #555;
    border-radius: 0 4px 4px 0;
    color: #fff;
    content: "\f0d7";
    font: normal normal normal 34px/1 FontAwesome;
    padding: 0 5px;
    pointer-events: none;
    position: absolute;
    right: 0px;
    top: 0;
}

</style>
<div class="container">
	<div class="row text-center">
		<div class="titulo00"><h2><?php echo JrTexto::_('Select a role'); ?> </h2></div>
		<?php
		 if(!empty($this->roles))
		 	$nrol=count($this->roles);
		 foreach ($this->roles as $rolk => $rol){?>
		<div class="pnlrol hvr-float-shadow" data-idrol="<?php echo $rol["idrol"] ?>" data-rol="<?php echo $rol["rol"]; ?>" data-empresa="<?php echo $rol["idempresa"]; ?>" data-proyecto="<?php echo $rol["idproyecto"]; ?>" >
			<div class="imagen">
				<?php 
				$srcimg_=$this->documento->getUrlStatic().'/media/imagenes/rol_'.strtolower($rol["rol"]).".jpg";
				//$srcimg=is_file($srcimg_)?$srcimg_:($this->documento->getUrlStatic().'/media/imagenes/rol_default.jpg');
				$srcimg=$srcimg_;
				?>
				<img src="<?php echo $srcimg; ?>" class="img-responsive">
			</div>
			<div class="titulo"><?php echo $rol["rol"];?></div>
		</div>		 	
		 <?php } ?>
	</div>
</div>
<?php 
if(!empty($this->noroles)){?>
<div class="row text-center">
<div class="titulo00"><h2><?php echo JrTexto::_('Otros Roles'); ?> </h2></div>
<div class="col-xs-12 col-sm-12 col-md-4"></div>
	<div class="col-xs-12 col-sm-12 col-md-4">
		<div class="select-ctrl-wrapper select-azul">
			<select id="idrol2empresa" class="form-control select-ctrl " name="idrol2empresa">
				<option value=""><?php echo JrTexto::_('selected') ?></option>
				<?php  foreach ($this->noroles as $rolk => $rol){?>
				<option  value="<?php echo $rol["idrol"] ?>" data-rol="<?php echo $rol["rol"]; ?>" data-empresa="<?php echo $rol["idempresa"]; ?>" data-proyecto="<?php echo $rol["idproyecto"]; ?>"><?php echo $rol["empresa"]." - ".$rol["rol"]; ?></option>
				<?php } ?>
			</select>
		</div>
    </div>
</div>
<?php } ?>

<script type="text/javascript">
	var fncambiarrol=function(obj,idrol,rol,idempresa,idproyecto){
		if(idempresa!='') url=_sysUrlBase_+'/sesion/cambiarroljson/?idrol='+idrol+'&rol='+rol+'&idempresa='+idempresa+'&idproyecto='+idproyecto;
		else url=_sysUrlBase_+'/sesion/cambiarroljson/?idrol='+idrol+'&rol='+rol+'&idempresa='+idempresa+'&idproyecto='+idproyecto;
		var div=obj.context.localName;
		if(div!='div') div=obj.closest('div');
		else div=obj;
		div.children('.cargando').remove();
		var htmlcarga='<div class="cargando" style="width: 100%;height: 100%; position: absolute; top: 0%;left: 0%; background-color: #c5c5cb59; cursor: progress;"><i class="fa fa-spinner fa-spin fa-3x" style="position: relative; top: 45%;color: burlywood; font-size: 2em;"></i></div>'
		div.append(htmlcarga);
		sysajax({ // carga la edicion de los modulos
			showmsjok:false,
			url:url,
			callback:function(rs){
				if(rs.data==true){	
					var t1=this.location.origin;
					var t2=window.location.origin;
					try{t2=top.location.origin}catch(ex){t2='error'; /*Error de origin*/};
					if(t2=='error'){
						window.location.href=_sysUrlBase_;
					}else if(this.location!=top.location){						
						top.location.href=top.location.href;
					}else{
						top.location.href=_sysUrlBase_;
					}
				}
				div.children('.cargando').remove();
			}
		});
	}
	$('.pnlrol').click(function(ev){
		fncambiarrol($(this),$(this).attr('data-idrol'),$(this).attr('data-rol'),'','');		
	})

	$('#idrol2empresa').change(function(ev){
		var idrol=$(this).val()||'';
		var idempresa=$(this).children('option:selected').attr('data-empresa');
		var idproyecto=$(this).children('option:selected').attr('data-proyecto');
		var rol=$(this).children('option:selected').attr('data-rol');
		if(idrol!=''){console.log(idrol,idproyecto,idempresa);}
		fncambiarrol($(this),idrol,rol,idempresa,idproyecto);		
	})
</script>