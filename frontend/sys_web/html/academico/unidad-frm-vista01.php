<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
if(!empty($frm['imagen'])) $imagen=$this->documento->getUrlBase().$frm['imagen'];
else $imagen=$this->documento->getUrlStatic().'/media/imagenes/cursos/nofoto.jpg';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .infototal{ padding: 1em 1ex; }
  .infototal .titulo{ font-size: 1.2em; }
  .infototal .numberinfo{ font-size: 0.9em; color: #ca3c3c; }
</style>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <?php if($this->documento->plantilla!='modal'){?>
  <div class="row" id="breadcrumb">
    <div class="col-xs-12">
      <div style="position: relative;">
      <ol class="breadcrumb">
          <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/academico/curso">&nbsp;<?php echo JrTexto::_('course'); ?></a></li>
          <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion).' '.JrTexto::_('Course'); ?></li> 
      </ol>
      <ul style="position: absolute; top:1ex; right: 1ex;">        
          <li class="btn btn-xs btn-default"><i class="fa fa-print"></i> <?php echo JrTexto::_('Print'); ?></li>
          <li class="btn btn-xs btn-default view01 btn-changeview<?php echo $idgui; ?>"><i class="fa fa-eye"></i> <?php echo JrTexto::_('View'); ?> 01</li>  
      </ul>
     </div>
    </div>
  </div>
  <?php }?>
    <div class="panel">      
      <div class="panel-body" id="pnl<?php echo $idgui ?>">
        <div id="msj-interno"></div> 
          <input type="hidden" name="pkidlevel" id="pkidlevel" value="<?php echo JrTexto::_($this->pk);?>">          
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="col-md-12"><div class="form-group">       
                <label>Nombre Nivel:</label>                             
                <input type="text" class="form-control" name="nombre" id="nombre<?php echo $idgui ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder="<?php echo JrTexto::_('Name') ?>" >
              </div></div>
              <div class="col-md-12">             
                <div class="form-group">       
                  <label>I. Descripcion General:</label>
                  <textarea class="form-control" name="descripcioncurso" id="descripcion<?php echo $idgui ?>" placeholder="Aqui debe agregar la descripcion del curso" rows="6"><?php echo @$frm['descripcion'];?></textarea> 
                </div>  
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="col-md-12">
                  <div class="form-group"> <label><?php echo ucfirst(JrTexto::_("State")); ?>: </label>                 
                  <div class="select-ctrl-wrapper select-azul">
                    <select name="cbestado" id="cbestado<?php echo $idgui ?>" class="form-control select-ctrl">                    
                    <option value="1"<?php @$frm['estado']=='1'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"<?php @$frm['estado']=='0'?' selected="selected" ':'' ?>><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                  </select>
                  </div>
                </div>
              </div>
              <div class="col-md-12 text-center">
                <label for="titulo"><?php echo ucfirst(JrTexto::_('Portada')); ?>:</label>
                <img src="<?php echo $imagen; ?>" alt="foto" class="cargaimagen<?php echo $idgui ?> img-responsive thumbnail centrado" data-url=".cargaimagen<?php echo $idgui ?>"  data-tipo="image" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>">
                <input type="hidden" name="imagen" id="imagen<?php echo $idgui ?>" value="<?php echo @$imagen ?>">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
               <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Unit')); ?></label></div>
                  <div class="numberinfo"><span id="nunidad<?php echo $idgui ?>"><?php echo  (!empty($frm["nunidad"])?$frm["nunidad"]:'0').'</span> '.ucfirst(JrTexto::_('Unit')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Activities')); ?></label></div>
                  <div class="numberinfo"><span id="nactividad<?php echo $idgui ?>"><?php echo (!empty($frm["nactividad"])?$frm["nactividad"]:'0').'</span> '.ucfirst(JrTexto::_('Activities')); ?></div>
               </div>
                <div class="col-md-12 text-center infototal">
                  <div class="titulo"><label><?php echo ucfirst(JrTexto::_('Total').' '.JrTexto::_('Assessment')); ?></label></div>
                  <div class="numberinfo"><span id="nexamen<?php echo $idgui; ?>"><?php echo (!empty($frm["nexamenes"])?$frm["nexamenes"]:'0').'</span> '.ucfirst(JrTexto::_('Assessment')); ?></div>
               </div>
            </div>
          </div>         
          <div class="clearfix"><br></div>
          <div class="row">            
              <div class="col-md-12 text-center">
                <a id="btn-savenivel" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </a>
                <a class="btn btn-warning btn-close" href="<?php echo $this->documento->getUrlBase();?>/academico/nivel" data-dismiss="modal">
                  <i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>