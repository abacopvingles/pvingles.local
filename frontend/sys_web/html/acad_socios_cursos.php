<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style>
#cursosAsignados .bg-success:after{
    content: '\f00c';
   font-family: FontAwesome;
   font-weight: normal;
   font-style: normal;
    position:absolute;
    top:1ex;
    right:1ex;
    font-size:2em;
}
</style>
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Courses")." ".JrTexto::_("of")." ".JrTexto::_("Partner"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
              <label><?php echo  ucfirst(JrTexto::_("Business"))?></label>
                <div class="form-group">
                	<div class="cajaselect">            
                    <select id="idempresa<?php echo $idgui; ?>" name="idempresa" class="form-control" >  
                      <option value="0" ><?php echo  ucfirst(JrTexto::_("Select a business"))?></option>
                      <?php 
                      if(!empty($this->empresas))
                        foreach($this->empresas as $emp){?>
                          <option value="<?php echo $emp["idempresa"]; ?>" <?php echo $emp["idempresa"]==$this->idempresa?'selected="selected"':'';?> ><?php echo  ucfirst($emp["nombre"]);?></option>
                        <?php }
                      ?>
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
              <label><?php echo  ucfirst(JrTexto::_("text to Search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <!--div class="col-xs-6 col-sm-3 col-md-3 text-center"><br>
               <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("bolsa_empresas", "agregarsocio"));?>" data-titulo="<?php echo JrTexto::_("Partner").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div-->
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">
         <div class="panel-body" id="cursosAsignados">
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    var _idproyecto=0;
    var msjatencion='<?php echo JrTexto::_('Attention');?>';
    var idgui='<?php echo !empty($idgui)?$idgui:now();?>';
    var mostrarcursos<?php echo $idgui; ?>=function(){
        var idempresa=$('#idempresa<?php echo $idgui; ?>').val()||'';
        console.log(idempresa);
        if(idempresa==''||idempresa=='0') {
            $('#cursosAsignados').html('<h3>Seleccione una empresa</h3>');
         return false;
        }
       var formData = new FormData();       
        formData.append('idempresa', idempresa);
         //  formData.append('sql2', true);
           sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/proyecto_cursos/buscarmiscursosjson',
            msjatencion:msjatencion,
            type:'json',
            callback:function(rs){
                dt=rs.data||[];
                if(dt.length>0){
                    html='';
                    _idproyecto=rs.idproyecto;
                    $.each(dt,function(i,v){
                        var asignado=v.asignado==true?'bg-success':'bg-warning';
                        html+='<div class="col-md-4 col-md-6 col-sm-12 "><div class="img-thumbnail '+asignado+' btn-asignarcurso" data-idcurso="'+v.idcurso+'"  style="margin-top:2ex; width:100%; cursor:pointer;">';
                        html+='<div class="text-center" style="min-height:180px;"><img class="img img-responsive" src="'+_sysUrlBase_+(v.imagen||'/static/media/nofoto.jpg')+'" style=" max-height: 180px; display: inline;"></div>';
                        html+='<div class="titulo text-center" style="height: 50px; overflow: hidden; color:#000; flex-wrap: wrap-reverse;"><h3>'+v.nombre+'</h3></div>';
                        html+='<div class="subtitulo text-center" style="height: 45px; overflow: hidden; color: #555; flex-wrap: wrap-reverse;"><small>'+v.descripcion+'</small></div>';
                        html+='</div></div>';
                    })
                    $('#cursosAsignados').html(html);
                }
            }
           });
    }
    $('.btnbuscar').click(function(ev){
        mostrarcursos<?php echo $idgui; ?>();
    });
    $('#idempresa<?php echo $idgui; ?>').change(function(ev){
        mostrarcursos<?php echo $idgui; ?>();
    });
    mostrarcursos<?php echo $idgui; ?>();
   
    $('#cursosAsignados').on('click','.btn-asignarcurso',function(){
        var panel=$(this);
        var idcurso=$(this).attr('data-idcurso');
        var idproyecto=_idproyecto;
        var isasignado=$(this).hasClass('bg-success')?0:1;
        var formData = new FormData();       
        formData.append('idcurso', idcurso);
        formData.append('idproyecto', idproyecto);
        formData.append('isasignado', isasignado);
           sysajax({
            fromdata:formData,
            url:_sysUrlBase_+'/proyecto_cursos/cambiarestado',
            msjatencion:msjatencion,
            type:'json',
            callback:function(rs){
                if(rs.asignado==0){
                    panel.removeClass('bg-success').addClass('bg-warning');
                }else 
                panel.removeClass('bg-warning').addClass('bg-success');
            }
           });

        
    })
  
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'acad_licencias', 'eliminar', id);
        if(res) tabledatos5c11d0f36849f.ajax.reload();
      }
    }); 
  });
});
</script>