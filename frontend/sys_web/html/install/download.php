<div class="container">
   <div style="padding:15px;"></div>
   <div class="panel panel-primary">
      <div class="panel-heading">
         <h4><?php echo JrTexto::_('Descargar Sincronizacion');?></h4>
      </div>
      <div class="panel-body">
         <div class="panel text-center">
            <h1>Descargar Sincronizacion</h1>
            <div class="alert alert-info text-left">
               <p style="font-weight:bold;">Guía de pasos para sincronizar su archivo e instalarlo en dicha plataforma offline</p>
               <ol>
                  <li>Accionar el botón de descarga ubicado en la parte inferior del recuadro, después de la lista de pasos, para descargar un archivo comprimido de toda la información de la plataforma online.</li>
                  <li>Al presionar el botón, ubicar el directorio en donde se colocará dicho archivo comprimido.</li>
                  <li>Una vez ubicado el directorio, presionamos el botón de “Guardar” para descargar el archivo comprimido.</li>
                  <li>Listo. Seguidamente ir a la maquina donde va sincronizar la plataforma offline y cuando le requiera el archivo comprimido, inserte su archivo descargado con anterioridad.</li>
               </ol>
            </div>
            <h1><i class="fa fa-download" aria-hidden="true"></i></h1>
            <div class="alert alert-danger form-check">
               <label class="form-check-label">Descarga para una sincronización forzada&nbsp;<input class="form-check-input" type="checkbox" id="forzado" /><label>
            </div>
            <button type="button" id="btn_download" class="btn btn-primary"><?php echo JrTexto::_('Descargar');?></button>
         </div>
      </div>
   </div>
   <div class="panel panel-danger">
      <div class="panel-heading"><h4><?php echo JrTexto::_('Descargar Audios');?></h4></div>
      <div class="panel-body">
         <div class="panel text-center">
            <h1>Descargar Audios</h1>
            <div class="alert alert-warning text-left">
               <p style="font-weight:bold;">Guía de pasos para sincronizar los audios e instalarlo en dicha plataforma offline</p>
               <ol>
                  <li>Accionar el botón de descarga ubicado en la parte inferior del recuadro.</li>
                  <li>Al presionar el botón, ubicar el directorio en donde se colocará dicho archivo comprimido.</li>
                  <li>Una vez ubicado el directorio, presionamos el botón de “Guardar” para descargar el archivo comprimido.</li>
                  <li>Listo. Seguidamente ir a la maquina donde va sincronizar la plataforma offline y cuando le requiera el archivo comprimido, inserte su archivo descargado con anterioridad.</li>
               </ol>
            </div>
            <h1><i class="fa fa-download" aria-hidden="true"></i></h1>
            <button type="button" id="btn_download2" class="btn btn-danger"><?php echo JrTexto::_('Descargar');?></button>
         </div>
      </div>
   </div>
</div>
<?php
//TESTING
?>
<script type="text/javascript">
//deletemanualdownAction
function deleteFile(force){
   $.ajax({
      url: _sysUrlBase_.replace('/web','') +"/sync/manual/deletemanualdown/"+force,
      type: 'GET',
      data: {}
   }).done(function(resp){
      console.log('delete success');
   }).fail(function(xhr,textStatus,errorThrow){
      alert(xhr.responseText);
   });
}
$(document).ready(function(){
   $('#btn_download').on('click',function(){
      var isForzado = $('#forzado').is(':checked');
      $.ajax({
         url: _sysUrlBase_.replace('/web','') +"/sync/index/manualdown/"+isForzado,
         type: 'GET',
         data: {}
      }).done(function(resp){
         var _empty = (!resp.trim() || resp.length== 0 || !resp);
         if(!_empty){
            window.location = _sysUrlBase_.replace('/web','')+resp;
         }else{
            alert("no hay archivos disponibles");
         }
         deleteFile(isForzado);
      }).fail(function(xhr,textStatus,errorThrow){
         alert(xhr.responseText);
      });
   });
   $('#btn_download2').on('click',function(){
      $.ajax({
         url: _sysUrlBase_.replace('/web','') +"/sync/index/obtenermanualaudio",
         type: 'GET',
         data: {}
      }).done(function(resp){
         if(resp != 1 || resp != -1 || resp.length != 0){
            window.location = _sysUrlBase_.replace('/web','')+resp;
         }
      }).fail(function(xhr,textStatus,errorThrow){
         alert(xhr.responseText);
      });
   });
});
</script>