<style css>
    body{
        background: #e9f9ff; 
    }
    .smartenglish-p1{
        color:green;
    }
    .smartenglish-p2{
        color:#0066ff;
    }
    .smartenglish-p1, .smartenglish-p2{
        text-shadow: 2px 1px 2px #2d2d2d;
    }
    .separator{
        padding:10px;
    }
    #listConfig .lista{
        background:#ffffe9;
        border:1px solid black;
        border-radius:0.5em;
        padding:5px 10px 5px 20px;

    }
    #listConfig .lista h4 i {
        background:white;
        border:1px solid gray;
        margin: 0 5px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <h1 class="text-center">Bienvenido a la configuracion inicial de la plataforma virtual <span class="smartenglish-p1">Smart</span><span class="smartenglish-p2">English</span></h1>
            <div class="separator"></div>
            <div class="panel panel-primary">
                <div class="panel-heading"><h4>Información</h4></div>
                <div class="panel-body" id="container_html">
                    <!--Contenido dinamico-->
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 col-xs-4">
                            <button type="button" id="btn_prevForm" class="btn btn-primary">Anterior</button>
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <div style="font-size:18px; font-weight:bold; text-align:center;"><span id="pag_actual">1</span> / <span id="pag_max">1</span></div>
                        </div>
                        <div class="col-sm-4 col-xs-4">
                            <button type="button" id="btn_nextForm" class="btn btn-success" style="float:right;">Siguiente</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //$('.selectpicker').selectpicker('refresh');
    const URLWEB = '<?php echo $this->documento->getUrlBase();?>';
    var pagina = 1;
    var maximo = 4;
    var ListConfig = [false,false,false];
    var configuration = { "target" : null,"idproyecto" : 3, "local" : null };
    var syncTotal = false;
    var readyOrNot = ['<i class="fa fa-times" aria-hidden="true" style="color:red;"></i>','<i class="fa fa-check" aria-hidden="true" style="color:green;"></i>'];
    var contenido = [
        {"html" : '<form id="frmcontainer1" onsubmit="javascript:return false"><h2 class="text-center">Identificar el colegio</h2><div class="row"><div class="form-group col-xs-6"><label>Seleccionar Departamento</label><select id="departamento" class="form-control"><option value="01">Lima</option><option value="14">Lambayeque</option></select></div><div class="form-group col-xs-6"><label>Seleccionar Provincia</label><select id="provincia" class="form-control"><option value="0">Seleccionar provincia</option></select></div><div class="form-group col-xs-12"><label>Seleccionar Colegio</label><select id="colegio" class="form-control selectpicker"><option value="0">seleccionar</option></select></div></div></form>'},
        {"html" :'<form id="frmcontainer2" onsubmit="javascript:return false"><h2 class="text-center">Configuración del sincronizador</h2><div class="form-group"><p class="alert alert-warning">Insertar la dirección de la plataforma online, se requiere la url con el siguiente formato : "http://midominio.com/" ó "https://midominio.com/"</p><label>Ingresar URL del Host principal</label><input type="url" id="txt_url" class="form-control" placeholder="ingresar dirección (URL)" /></div><div class="form-group text-center"><!--<button type="button" class="btn btn-info" id="btn_verificarURL">Verificar Host</button>--></div></form>'},
        {"html" :'<form id="frmcontainer3" action="install/save" method="post" enctype="multipart/form-data"><h2 class="text-center">Actualizar la plataforma virtual</h2><p class="alert alert-info">Es obligatorio actualizar la "plataforma offline" con el archivo generado desde la plataforma online alojado en la nube.</p><div id="msj_ajax"></div><div id="bar_progress1" class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div></div><div id="bar_progress2" class="progress"><div class="progress-bar bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div></div><div class="form-group"><label>Subir archivo</label><input type="file" accept=".zip,.php" class="form-control"  id="archivo" name="archivo"></div><div class="form-group text-center"><button type="submit" class="btn btn-primary">Cargar Archivo</button></div></form>'},
        {"html" : '<div id="listConfig"><div class="lista"><h4><i class="fa fa-times" aria-hidden="true" style="color:red;"></i>Identificación del colegio</h4>   <h4><i class="fa fa-times" aria-hidden="true" style="color:red;"></i>Configuración del sincronizador</h4>   <h4><i class="fa fa-times" aria-hidden="true" style="color:red;"></i>Actualizar la plataforma virtual</h4></div></div>'}
    ];
    var resetBtnNext = function(){
        $('#btn_nextForm').text("Siguiente");
        $('#btn_nextForm').attr('disabled',false);
    }
    function listaConfiguracion(){
        $('#btn_nextForm').text("Guardar y Salir");
        if(ListConfig.indexOf(false) > -1){
            $('#btn_nextForm').attr('disabled',true);
        }
        $('#listConfig .lista').find('h4').each(function(k,v){
            var texto = $(this).text();
            var boolToInteger = (ListConfig[k]) ? 1 : 0;
            $(this).html(readyOrNot[boolToInteger]+texto);
        });
    }
    function saveConfiguration(){
        if(ListConfig.indexOf(true) == -1){
            alert("Falta alguna de las configuraciones");
            return false;
        }
        //hacer el archivo config.php en la sincronizacion
        $.ajax({
            url: URLWEB+'/install/crearconfiguracion',
            type: 'POST',
            dataType: 'json',
            data: {'target': configuration.target, 'local' : configuration.local,'idproyecto': configuration.idproyecto}
        }).done(function(resp){
            if(resp.code == 'ok'){
                window.location.href = URLWEB;
            }else{
                alert(resp.message);
                return false;
            }
        }).fail(function(xhr,textStatus,errorThrown){
            alert(xhr.responseText);
        });
    }
    function savelocal(){
        var local = $('#frmcontainer1').find('#colegio').val();
        if(local != 0){
            configuration.local = local;
            ListConfig[0] = true;
        }
    }
    function savetarget(){
        var url = $('#frmcontainer2').find('#txt_url').val();
        var isEmpty = (!url.trim() || url.length === 0 );
        if(!isEmpty){
            configuration.target = url;
            ListConfig[1] = true;
        }
    }
    function verificarSincronizacion(){
        if(syncTotal === true){
            ListConfig[2] = true;
        }
    }
    function calltosync(){
        $.ajax({
            url: URLWEB+'/install/sync',
            type: 'POST',
            dataType: 'json',
            data: {}
        }).done(function(resp){
            if(resp.code == 'ok'){
                $('#bar_progress2').find('.progress-bar').animate({ width: "100%" },'slow');
                syncTotal = true;
            }else{
                $('#bar_progress1').find('.progress-bar').animate({ width: "0%" },'slow');
                $('#bar_progress2').find('.progress-bar').animate({ width: "0%" },'slow');
                alert(resp.message);
            }
        }).fail(function(xhr, textStatus, errorThrow){
            alert(xhr.responseText);
        });
    }
    function uploadinit(){
        $('#frmcontainer3').ajaxForm({
            beforeSend: function(){
                $('#bar_progress1').find('.progress-bar').css('width','0%');
                console.log("preparando para enviar");
            },
            uploadProgress: function(event, position, total, percentComplete){
                var percentVal = percentComplete + '%';
                $('#bar_progress1').find('.progress-bar').css('width',percentVal);                
            },
            complete: function(xhr){
                var res = JSON.parse(xhr.responseText);
                if(res.code == 'ok'){
                    $('#bar_progress2').find('.progress-bar').animate({ width: "50%" },'slow');
                    calltosync();
                }else{
                    $('#bar_progress1').find('.progress-bar').animate({ width: "0%" },'slow');
                    alert(resp.message);
                }
            }
        }); 
    }
    function loadubigeo(t){
        if(t == 1){
            $.ajax({
                url: URLWEB+'/install/getinformation',
                type:'POST',
                dataType:'json',
                data:{"typeInfo" : t}
            }).done(function(resp){
                if(resp.code == 'ok'){
                    var options = '<option value="0">Seleccionar departamento</option>';
                    var obj = resp.data;
                    obj.forEach(function(elemento,indice){
                        options = options.concat('<option value="'+elemento.id_ubigeo+'">'+elemento.ciudad+'</option>');
                    });
                    $('#departamento').html(options);
                }else{
                    alert(resp.message);
                }
            }).fail(function(xhr,textStatus,errorThrow){
                alert(xhr.responseText);
            });

        }else if(t == 2){
            $.ajax({
                url: URLWEB+'/install/getinformation',
                type:'POST',
                dataType:'json',
                data:{"typeInfo" : t,"departamento": $('#departamento').val()}
            }).done(function(resp){
                if(resp.code == 'ok'){
                    var options = '<option value="0">Seleccionar provincia</option>';
                    var obj = resp.data;
                    obj.forEach(function(elemento,indice){
                        options = options.concat('<option value="'+elemento.id_ubigeo+'">'+elemento.ciudad+'</option>');
                    });
                    $('#provincia').html(options);
                    loadubigeo(3);
                }else{
                    alert(resp.message);
                }
            }).fail(function(xhr,textStatus,errorThrow){
                alert(xhr.responseText);
            });
        }else if(t == 3){
            var _departamento = null;
            var _provincia = null;
            if($('#departamento').val() != 0){
                _departamento = $('#departamento').val();
            }
            if($('#provincia').val() != 0){
                _provincia = $('#provincia').val();
            }
            if(_departamento == null ||_provincia == null){
                return false;
            }
            $.ajax({
                url: URLWEB+'/install/getinformation',
                type:'POST',
                dataType:'json',
                data:{"typeInfo" : t,"departamento" : _departamento, "provincia" : _provincia}
            }).done(function(resp){
                if(resp.code == 'ok'){
                    var options = '<option value="0">Seleccionar colegio</option>';
                    var obj = resp.data;
                    obj.forEach(function(elemento,indice){
                        options = options.concat('<option value="'+elemento.idlocal+'">'+elemento.nombre+'</option>');
                    });
                    $('#colegio').html(options);
                    $('.selectpicker').selectpicker('refresh');
                }else{
                    alert(resp.message);
                }
            }).fail(function(xhr,textStatus,errorThrow){
                alert(xhr.responseText);
            });
        }
        
    }
    $('document').ready(function(){
        $('#container_html').html(contenido[0].html);
        loadubigeo(1);
        $('#pag_max').text(maximo);
        $('#pag_actual').text(pagina);

        $('#btn_nextForm').on('click',function(){
            switch(pagina){
                case 1 : savelocal();
                break;
                case 2 : savetarget();
                break;
                case 3 : verificarSincronizacion();
                break;
                case maximo : saveConfiguration();
                break;
            }

            if((pagina + 1) <= maximo){
                ++pagina;
                var indice = pagina == 0 ? pagina : (pagina - 1);
                $('#pag_actual').text(pagina);
                $('#container_html').html(contenido[indice].html);
                if(pagina == 3){
                    if(syncTotal === true){
                        $('#frmcontainer3').html('<h1 class="alert alert-success">Ya se sincronizo la plataforma</h1>');
                    }else{
                        uploadinit();
                    }
                }
                if(pagina == maximo){
                    listaConfiguracion();
                }
            }
        });
        $('#btn_prevForm').on('click',function(){
            if((pagina - 1) >= 1){
                --pagina;
                var indice = pagina == 0 ? pagina : (pagina - 1);
                $('#pag_actual').text(pagina);
                $('#container_html').html(contenido[indice].html);
                if(indice == 0){
                    $('.selectpicker').selectpicker('refresh');
                    loadubigeo(1);
                }else if(indice == 2){
                    if(syncTotal === true){
                        $('#frmcontainer3').html('<h1 class="alert alert-success">Ya se sincronizo la plataforma</h1>');
                    }else{
                        uploadinit();
                    }
                }
                resetBtnNext();
            }
        });
        
        $('#departamento').on('change',function(){
            if($(this).val() != 0){
                loadubigeo(2);
            }
        });
        $('#provincia').on('change',function(){
            if($(this).val() != 0){
                loadubigeo(3);
            }
        });
        
    });
</script>