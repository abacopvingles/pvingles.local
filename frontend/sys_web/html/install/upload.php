<div class="container">
    <div style="padding:15px;"></div>
    <div class="panel panel-primary">
        <div class="panel-heading"><h4><?php echo JrTexto::_('Sincronizar');?></h4></div>
        <div class="panel-body">
            
            <form id="frmcontainer3" action="<?php echo str_replace('/web','',$this->documento->getUrlBase()) ; ?>/sync/manual/manualupfile" method="post" enctype="multipart/form-data">
                <h2 class="text-center">Actualizar la plataforma virtual</h2>
                <p class="alert alert-info">Ubique y suba el archivo generado por la plataforma previamente para la sincronización.</p>
                <div id="msj_ajax"></div>
                <div id="bar_progress1" class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <div id="bar_progress2" class="progress">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <div class="form-group">
                    <label>Subir archivo</label>
                    <input type="file" accept=".zip,.php" class="form-control"  id="archivo" name="archivo">
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary">Cargar Archivo</button>
                </div>
            </form>
        </div>
    </div>
    <div style="padding:15px;"></div>
    <div class="panel panel-danger">
        <div class="panel-heading"><h4><?php echo JrTexto::_('Sincronizar Audio');?></h4></div>
        <div class="panel-body">
            
            <form id="frmcontainer4" action="<?php echo str_replace('/web','',$this->documento->getUrlBase()) ; ?>/sync/manual/manualupfileaudio" method="post" enctype="multipart/form-data">
                <h2 class="text-center">Actualizar el audio de la plataforma virtual</h2>
                <p class="alert alert-info">Ubique y suba el archivo generado por la plataforma previamente para la sincronización.</p>
                <div id="msj_ajax"></div>
                <div id="bar_progress12" class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <div id="bar_progress22" class="progress">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
                </div>
                <div class="form-group">
                    <label>Subir archivo</label>
                    <input type="file" accept=".zip,.php" class="form-control"  id="archivo2" name="archivo">
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary">Cargar Archivo</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
function calltosync(){
    $.ajax({
        url: _sysUrlBase_.replace('/web','')+'/sync/index/manualup',
        type: 'POST',
        dataType: 'json',
        data: {}
    }).done(function(resp){
        if(resp.code == 'ok'){
            $('#bar_progress2').find('.progress-bar').animate({ width: "100%" },'slow');
        }else{
            $('#bar_progress1').find('.progress-bar').animate({ width: "0%" },'slow');
            $('#bar_progress2').find('.progress-bar').animate({ width: "0%" },'slow');
            alert(resp.message);
        }
    }).fail(function(xhr, textStatus, errorThrow){
        alert(xhr.responseText);
    });
}
function calltosyncAudio(){
    $.ajax({
        url: _sysUrlBase_.replace('/web','')+'/sync/index/manualupaudio',
        type: 'POST',
        dataType: 'json',
        data: {}
    }).done(function(resp){
        if(resp.code == 'ok'){
            $('#bar_progress22').find('.progress-bar').animate({ width: "100%" },'slow');
        }else{
            $('#bar_progress12').find('.progress-bar').animate({ width: "0%" },'slow');
            $('#bar_progress22').find('.progress-bar').animate({ width: "0%" },'slow');
            alert(resp.message);
        }
    }).fail(function(xhr, textStatus, errorThrow){
        alert(xhr.responseText);
    });
}
$(document).ready(function(){
    $('#frmcontainer3').ajaxForm({
        beforeSend: function(){
            $('#bar_progress1').find('.progress-bar').css('width','0%');
            console.log("preparando para enviar");
        },
        uploadProgress: function(event, position, total, percentComplete){
            var percentVal = percentComplete + '%';
            $('#bar_progress1').find('.progress-bar').css('width',percentVal);                
        },
        complete: function(xhr){
            var res = JSON.parse(xhr.responseText);
            if(res.code == 'ok'){
                $('#bar_progress2').find('.progress-bar').animate({ width: "50%" },'slow');
                calltosync();
            }else{
                $('#bar_progress1').find('.progress-bar').animate({ width: "0%" },'slow');
                alert(res.message);
            }
        }
    });

    $('#frmcontainer4').ajaxForm({
        beforeSend: function(){
            $('#bar_progress12').find('.progress-bar').css('width','0%');
            console.log("preparando para enviar");
        },
        uploadProgress: function(event, position, total, percentComplete){
            var percentVal2 = percentComplete + '%';
            $('#bar_progress12').find('.progress-bar').css('width',percentVal2);                
        },
        complete: function(xhr){
            var res = JSON.parse(xhr.responseText);
            if(res.code == 'ok'){
                $('#bar_progress22').find('.progress-bar').animate({ width: "50%" },'slow');
                calltosyncAudio();
            }else{
                $('#bar_progress12').find('.progress-bar').animate({ width: "0%" },'slow');
                alert(res.message);
            }
        }
    });
});
</script>