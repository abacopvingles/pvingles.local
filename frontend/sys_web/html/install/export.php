<div class="container">
   <div style="padding:15px;"></div>
   <div class="panel panel-primary">
      <div class="panel-heading">
         <h4>Exportador</h4>
      </div>
      <div class="panel-body">
         <div class="panel text-center">
            <h1>Descargar Archivos</h1>
            <div class="alert alert-info text-left">
               <p style="font-weight:bold;">Guía de pasos para exportar su archivo e instalarlo en dicha plataforma offline u/o sincronización manual</p>
               <ol>
                  <li>Accionar el botón de descarga ubicado en la parte inferior del recuadro, después de la lista de pasos, para descargar un archivo comprimido de toda la información de la plataforma online.</li>
                  <li>Al presionar el botón, ubicar el directorio en donde se colocará dicho archivo comprimido.</li>
                  <li>Una vez ubicado el directorio, presionamos el botón de “Guardar” para descargar el archivo comprimido.</li>
                  <li>Listo. Seguidamente ir a la maquina donde va instalar o sincronizar la plataforma offline y cuando le requiera el archivo comprimido, inserte su archivo descargado con anterioridad.</li>
               </ol>
            </div>
            <h1><i class="fa fa-download" aria-hidden="true"></i></h1>
            <button type="button" id="btn_download" class="btn btn-primary"><?php echo JrTexto::_('Descargar');?></button>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
var stringDescargando = '<?php echo JrTexto::_('Descargar'); ?>';
var stringCargando = '<?php echo JrTexto::_('Loading'); ?>';
$(document).ready(function(){
    $('#btn_download').on('click',function(){
        $(this).attr('disabled',true);
        $(this).text(stringCargando);
        $.ajax({
            url: _sysUrlBase_.replace('/web','')+"/sync/index/obtenermanual",
            type: 'POST',
            contentType: 'application/zip',
            data: {}
        }).done(function(resp){
            //window.open(_sysUrlBase_.replace('/web','')+resp,'download');
            $('#btn_download').attr('disabled',false);
            $('#btn_download').text(stringDescargando);
            window.location = _sysUrlBase_.replace('/web','')+resp;
        }).fail(function(xhr,textStatus,errorThrow){
            alert(xhr.responseText);
        });
    });
});
</script>