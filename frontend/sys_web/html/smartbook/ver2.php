<?php defined('RUTA_BASE') or die(); 
    $idgui = uniqid(); 
    $rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/css/tool_vocabulary.css">
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema() ?>/smartbook/ver.css">
<style>
#div_voca .pnlvocabulario div:nth-child(2n) > div:nth-child(2n) {
    display: none;
    background: red;
}
#div_voca .pnlvocabulario .textoarea , #div_voca .pnlvocabulario .texto{
    border:0px;
}
.content-smartic { overflow:initial !important; }
#div_voca .pnlvocabulario .textoarea { word-break: break-word; }
</style>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= "<b>".ucfirst(JrTexto::_($b['texto']))."</b>"; }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> 
</div>

<a href="anclaUp" class="ancla" id="anclaUp"></a>
<div id="smartbook-ver" class="editando row" data-idautor="<?php echo @$voc['idpersonal']; ?>">
    <input type="hidden" id="rolUsuario" value="<?php echo @$this->idrol; ?>">
    <input type="hidden" id="idCurso" value="<?php echo @$this->idcurso; ?>">
    <input type="hidden" id="idSesion" value="<?php echo @$this->idactividad; ?>">
    <input type="hidden" id="idBitacoraAlumnoSmbook" value="<?php echo !empty($this->bitac_alum_smbook)? @$this->bitac_alum_smbook['idbitacora_smartbook']:'';?>">
    <input type="hidden" id="idLogro" value="<?php echo @$this->idLogro; ?>">
    <div class="col-xs-12 col-sm-6">
        <!-- <a class="btn btn-default" href="<?php //echo $rutabase.$this->urlBtnRetroceder ?>">
            <i class="fa fa-arrow-left"></i> <?php //echo JrTexto::_("Go back") ?>
        </a>-->
        <a class="btn btn-info preview" href="#">
            <i class="fa fa-eye"></i> <?php echo JrTexto::_("Preview") ?>
        </a>
        <a class="btn btn-info back"  href="#" style="display: none;">
            <i class="fa fa-eye-slash"></i> <?php echo ucfirst(JrTexto::_("Close").' '.JrTexto::_("preview")) ?>
        </a>
        <a href="#" class="btn btn-lilac btn-pronunciar slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/pronunciacion">
            <i class="fa fa-bullhorn"></i> <?php echo ucfirst(JrTexto::_("Pronunciation")) ?>
        </a>
        
        <button aria-controls="bs-navbar" id="botonMenuCollapse" aria-expanded="false" class="btn btn-primary collapsed navbar-toggle" data-target="#navTabsLeft" data-toggle="collapse" type="button" style="margin: 0; bottom: -3px; z-index: 2; background: gray; border-radius: 24% 24% 0 0;"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
        </button>
    </div>
    <div class="col-xs-12 col-sm-6" style="padding-right: 70px;">
        <ul id="navTabsTop" class="nav nav-tabs pull-right">
            <li class="hvr-bounce-in <?php echo @$this->pdf['bitacora']['ultimo_visto'].' '.(empty(@$this->pdfs["caratula"])?'hidden':''); ?>"><a class="" href="#div_pdfs" data-toggle="tab" style="display: none;"><?php echo ucfirst(JrTexto::_("PDF")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->audios['bitacora']['ultimo_visto'].' '.(empty(@$this->audios["caratula"])?'hidden':''); ?>"><a class="" href="#div_audios" data-toggle="tab" style="display: none;"><?php echo ucfirst(JrTexto::_("Audios")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->imagenes['bitacora']['ultimo_visto'].' '.(empty(@$this->imagenes["caratula"])?'hidden':''); ?>"><a class="" href="#div_imagen" data-toggle="tab" style="display: none;"><?php echo ucfirst(JrTexto::_("Images")); ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->videos['bitacora']['ultimo_visto'].' '.(empty(@$this->videos["caratula"])?'hidden':''); ?>"><a class="" href="#div_videos" data-toggle="tab" style="display: none;"><?php echo ucfirst(JrTexto::_("Videos")); ?></a></li>
        </ul>
    </div>

    <a href="#anclaUp" class="scroller up" id="scroll-up" style="display: none;"><i class="fa fa-angle-double-up fa-2x"></i></a>

    <div id="cuaderno" class="tabbable tabs-right col-xs-12 padding-0"> <div id="anillado"></div>
        
        <ul id="navTabsLeft" class="nav nav-tabs collapse metodologias">
            <li class="active hvr-bounce-in"><a class="vertical" href="#div_presentacion" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li class="hvr-bounce-in <?php echo @$this->look['bitacora']['ultimo_visto'].' '.(empty(@$this->look['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_look" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Look")) ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->practice['bitacora']['ultimo_visto'].' '.(empty(@$this->practice['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_practice" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Practice")) ?></a></li>
            <li class="hvr-bounce-in <?php echo @$this->games['bitacora']['ultimo_visto'].' '.(empty(@$this->games)?'hidden':''); ?>"><a class="vertical" href="#div_games" data-toggle="tab"><?php echo JrTexto::_("Game") ?>s</a></li>
            <li class="hvr-bounce-in <?php echo @$this->vocabulario['bitacora']['ultimo_visto'].' '.(empty($this->vocabulario)?'hidden':''); ?>"><a class="vertical" href="#div_voca" data-toggle="tab"><?php echo JrTexto::_("Vocabulary") ?></a></li>
            <!--li class="hvr-bounce-in <?php echo @$this->workbook['bitacora']['ultimo_visto'].' '.(empty($this->workbook['texto'])?'hidden':''); ?>"><a class="vertical" href="#div_workbook" data-toggle="tab"><?php echo JrTexto::_("Workbook") ?></a></li-->
            <li class="hvr-bounce-in <?php echo @$this->dby['bitacora']['ultimo_visto'].' '.(empty(@$this->dby['act']['det'])?'hidden':''); ?>"><a class="vertical" href="#div_autoevaluar" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Do it by yourself")) ?></a></li> 
            <li ><a class="vertical btn btn-default" id="exit" href="<?php echo $rutabase.$this->urlBtnRetroceder ?>"><i class="fa fa-sign-out vertical"></i> <?php echo JrTexto::_("Exit") ?></a></li>
            <span class="hvr-bounce-in hidden"><a class="vertical" href="javascript:history.back();"><?php echo JrTexto::_("Exit") ?></a></span>
        </ul>

        <div id="tabContent" class="tab-content">
            <div id="contenedor-paneles" class="hidden">
                <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            <span>0</span>%
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12 infouserejericiopanel">                   
                    <div class="col-md-3 col-sm-3 col-xs-4 text-left" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-back_ " style="display: inline-block;  padding-top: 0.9em;  height: 3.1em;"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back'); ?></a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-4 text-center">
                        <span class="infouserejericio ejeinfo-numero numero_ejercicio">
                            <div class="titulo">Exercise</div>
                            <span class="actual">1</span>
                            <span class="total">16</span>
                        </span>
                        <span id="panel-intentos-dby"  class="infouserejericio ejeinfo-intento">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('attempt')); ?></div>
                            <span class="actual">1</span>
                            <span class="total"><?php echo $this->intentos ?></span>
                        </span>
                        <!--span id="panel-tiempo" class="infouserejericio ejeinfo-tiempo">
                            <div class="titulo">< ?php //echo ucfirst(JrTexto::_('time')); ?></div>
                            <span class="actual"></span>
                            <input type="text" class="form-control nopreview setpanelvalue" data-actualiza="time" placeholder="< ?php //echo JrTexto::_('e.g.'); ?>: 03:50" style="display: none;">
                            <span class="info-show">00:00</span>
                        </span-->                         
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-4 text-right" >
                        <a class="btn btn-inline btn-success btn-lg btnejercicio-next_" style="display: inline-block; padding-top: 0.9em;  height: 3.1em;"><?php echo JrTexto::_('Next'); ?> <i class="fa fa-arrow-right"></i></a>
                    </div>                            
                </div>
               
            </div>
           
            <!-- tab-pane for #navTabsLeft -->
            <div class="tab-pane aquihtml active" id="div_presentacion" data-tabgroup="navTabsLeft" style="background-image: url('<?php echo @str_replace('__xRUTABASEx__', $rutabase, $this->sesion["imagen"]) ;?>'); ">
                <div class="col-xs-12 portada-info" style="display: block;">
                    <h2 class="col-xs-12 col-sm-6 div_titulo hidden"><div class="div_fondo"><?php echo @$this->nivel["nombre"];?></div></h2>
                    <h2 class="col-xs-12 col-sm-6 div_titulo hidden"><div class="div_fondo"><?php echo @$this->unidad["nombre"];?></div></h2>
                    <h1 class="col-xs-12 col-sm-12 div_titulo"><div class="div_fondo"><?php echo @$this->sesion["nombre"];?></div></h1>
                    <?php if (!empty(@$this->bitacora)) { ?>
                    <div class=" div_acciones" style="display: none;">
                        <button class="btn btn-red btn-lg btn-continuar">
                            <span><?php echo ucfirst(JrTexto::_("Continue where I left")); ?> </span> &nbsp;
                            <i class="fa fa-hand-o-right"></i> 
                        </button>
                    </div>
                    <?php } ?>
                    <div class="col-xs-12 div_descripcion hidden div_fondo"><?php echo @$voc["texto"] ;?></div>
                    <div class="creado-por div_fondo hidden">
                        <?php echo JrTexto::_("Created by") ?>: 
                        <span><?php echo @$this->usuarioAct['nombre_full'] ;?></span>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_look" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->look['bitacora'])?'data-idbitacora='.$this->look['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->look["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->look["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12 Ejercicio">
                    <?php foreach (@$this->look['act']['det'] as $i => $det_html) {
                        echo str_replace('__xRUTABASEx__',$rutabase,$det_html['texto_edit']);
                    } ?>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_practice" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->practice['bitacora'])?'data-idbitacora='.$this->practice['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->practice["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->practice["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->practice["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            $encontro=false;
                            $c = !empty($this->practice['act']['det'])?count($this->practice['act']['det']):1;
                            foreach ($this->practice['act']['det'] as $i=>$ejerc) { $num++;
                               // if($this->rolActivo=='Alumno'){
                                if($encontro==false){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                        $encontro=true;
                                    }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                                        $aquiSeQuedo = true;
                                        $encontro=true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else $aquiSeQuedo = false;
                                /*}else{
                                    $aquiSeQuedo = ($num==1);
                                }*/
                                ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- para que cuente correctamente la barra progreso --></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio" style="display: none">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]) ) ?>-main-content">
                        <?php
                        if(!empty(@$this->practice['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $encontro=false;
                            $c = !empty($this->practice['act']['det'])?count($this->practice['act']['det']):1;
                            foreach (@$this->practice['act']['det'] as $i=>$ejerc) { 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                //if($this->rolActivo=='Alumno'){
                                    if($encontro==false){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                        $encontro=true;
                                    }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                                        $aquiSeQuedo = true;
                                        $encontro=true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                }else $aquiSeQuedo = false;
                               /* }else{
                                    $aquiSeQuedo = ($x==1);
                                } */
                                ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->practice["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>"  data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>" >
                            <?php $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div> 
            </div>
            <div class="tab-pane" id="div_games" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->games['bitacora'])?'data-idbitacora='.$this->games['bitacora']['idbitacora']:'' ?>>
                <div class="col-xs-12">
                    <ul class="nav nav-tabs list-games items-select-list">
                        <?php
                        if(!empty($this->games)){
                            foreach ($this->games as $i=>$g) {
                                echo '<li'.(($i==0)?' class="active"':'').'><a href="#game_'.$g['idtool'].'" data-idgame="'.$g['idtool'].'" data-title="'.$g['titulo'].'" class="item-select" data-toggle="tab">'.($i+1).'</a></li>';
                                $i++;
                            }
                        } ?>
                        <li style="float:right;"><button type="button" class="btn btn-success continue-game"><?php echo ucfirst(JrTexto::_('next')) ?></button></li>
                    </ul>
                </div>
                <div class="tab-content col-xs-12">
                    
                    <div class="mascara_visor">
                        <!--Dynamic content-->
                    </div>
                </div>
                <div class="col-xs-12" id="btns_control_alumnogame">
                    <div class="col-sm-12 botones-control" style="margin: 2.3em auto;">
                        <a class="btn btn-inline btn-success btn-lg continue-game" style="display: inline;"><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="div_voca" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->vocabulario['bitacora'])?'data-idbitacora='.$this->vocabulario['bitacora']['idbitacora']:'' ?>>
                <h1 class="col-xs-12 title-resrc"><?php echo JrTexto::_("Vocabulary") ?></h1>
                <div class="col-xs-12">
                    <?php if(!empty($this->vocabulario)){
                        foreach ($this->vocabulario as $i => $v) {
                            echo str_replace('__xRUTABASEx__', $rutabase, @$v['texto']);
                        } 
                    }else{ ?>
                    <div class="text-center">
                        <h1 style="color: rgb(148, 148, 148);"><i class="fa fa-minus-circle fa-3x"></i></h1>
                        <h4><?php echo JrTexto::_("No vocabulary to display"); ?></h4>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!--div class="tab-pane" id="div_workbook" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->workbook['bitacora'])?'data-idbitacora='.$this->workbook['bitacora']['idbitacora']:'' ?>>
                <h1 class="title-resrc"><?php echo JrTexto::_("Workbook") ?></h1>
                <div class=" mascara_visor">                    
                    <iframe src="<?php echo @str_replace('__xRUTABASEx__', $rutabase, @$this->workbook['texto']) ;?>" frameborder="0" height="100"></iframe>
                </div>
            </div-->
            <div class="tab-pane" id="div_autoevaluar" data-tabgroup="navTabsLeft" <?php echo !empty(@$this->dby['bitacora'])?'data-idbitacora='.$this->dby['bitacora']['idbitacora']:'' ?>>
                <div class="content-smartic metod" id="met-<?php echo @$this->dby["met"]["idmetodologia"]; ?>" data-idmet="<?php echo @$this->dby["met"]["idmetodologia"]; ?>" style="overflow: hidden;">
                    <div class="col-xs-12">
                        <ul class="nav nav-tabs ejercicios" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>-tabs" data-metod="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"])) ?>" data-id-metod="<?php echo @$this->dby["met"]["idmetodologia"]; ?>">
                            <?php if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = false;
                            $flag  = false;
                            $num=0;
                            $encontro=false;
                            $c = !empty($this->dby['act']['det'])?count($this->dby['act']['det']):-1;
                            //var_dump($c);
                            foreach ($this->dby['act']['det'] as $i=>$ejerc) { $num++;
                                //if($this->rolActivo=='Alumno'){
                                    if($encontro==false){
                                        if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                            $aquiSeQuedo  = $flag = true;
                                            $encontro=true;
                                        }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                                            $aquiSeQuedo = true;
                                            $encontro=true;
                                        }else{
                                            $aquiSeQuedo = false;
                                        }
                                    } else $aquiSeQuedo=false;
                                    //var_dump($c,$i);
                                    
                                /*}else{
                                    $aquiSeQuedo = ($num==1);
                                } */ ?>
                                <li class="<?php echo ($aquiSeQuedo)?'active':''; ?>"><a href="#tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ).'-'.$num; ?>" data-toggle="tab"><?php echo $num; ?></a></li>
                            <?php } } ?>
                            <li class="hidden comodin"><!-- tab obligado para calcular barra progreso--></li>
                        </ul>

                        <div class="col-xs-12 text-center numero_ejercicio" style="display: none;">
                            <div class="titulo"><?php echo ucfirst(JrTexto::_('Exercise')); ?></div>
                            <span class="actual"></span>
                            <span class="total"></span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="tab-content" id="<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]) ) ?>-main-content">
                        <?php 
                        if(!empty(@$this->dby['act']['det'])){
                            $aquiSeQuedo  = true;
                            $flag  = false;
                            $encontro=false;
                            $c = !empty($this->dby['act']['det'])?count($this->dby['act']['det']):-1;
                            foreach (@$this->dby['act']['det'] as $i=>$ejerc){ 
                                $x=$i+1; 
                                $iddetalle=$ejerc["iddetalle"]; 
                                //if($this->rolActivo=='Alumno'){
                                //    echo "antes".$encontro;
                                if($encontro==false){
                                    if((empty($ejerc['desarrollo']) || @$ejerc['desarrollo']['estado']=='P') && $flag===false){
                                        $aquiSeQuedo = $flag = true;
                                        $encontro=true;
                                    }elseif(@$ejerc['desarrollo']['estado']=='T' && $c== $i+1){
                                        $aquiSeQuedo = true;
                                        $encontro=true;
                                    }else{
                                        $aquiSeQuedo = false;
                                    }
                                } else $aquiSeQuedo=false;
                                // echo  "despues".$encontro;
                                //var_dump($c,$i);
                                /*}else{
                                    $aquiSeQuedo = ($x==1);
                                }*/
                                ?>
                            <div id="tab-<?php echo str_replace(' ', '_', strtolower(@$this->dby["met"]["nombre"]))."-".$x; ?>" class="tabhijo tab-pane fade <?php echo ($aquiSeQuedo)?'active in':''; ?>" data-iddetalle="<?php echo  @$ejerc["iddetalle"]; ?>">
                            <?php  $html = $ejerc["texto_edit"];
                            if($this->rolActivo=='Alumno' && !empty($ejerc['desarrollo']) && !empty(@$ejerc['desarrollo']['html_solucion'])){
                                $html  = $ejerc['desarrollo']['html_solucion'];
                            }
                            echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$html);?>
                            </div>
                         <?php } } ?>
                        </div>
                    </div>
                    <div class="col-xs-3 hidden">
                        <div class="widget-box">
                            <div class="widget-header bg-red">
                                <h4 class="widget-title">
                                    <i class="fa fa-paint-brush"></i>
                                    <span><?php echo JrTexto::_('Skills'); ?></span>
                                </h4>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="row habilidades">
                                        <div class="col-xs-12 ">
                                        <?php 
                                        $ihab=0;
                                        if(!empty($this->habilidades))
                                        foreach ($this->habilidades as $hab) { $ihab++;?>
                                            <div class="col-xs-12 btn-skill vertical-center" title="Click to activate" data-id-skill="<?php echo $hab['idmetodologia']; ?>"><?php echo ucfirst(JrTexto::_($hab['nombre'])); ?></div>
                                        <?php } ?>                                  
                                        </div>
                                    </div> 
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>    
    <a href="#anclaDown" class="scroller down" id="scroll-down"><i class="fa fa-angle-double-down fa-2x"></i></a>
</div>
<a href="anclaDown" class="ancla" id="anclaDown"></a>


<div id="btns_progreso_intentos" style="display: none;">
    <div class="row" id="btns_control_alumno">
        <div class="col-sm-12 botones-control">
            <a class="btn btn-inline btn-blue btn-lg try-again"><i class="fa fa-undo"></i> <?php echo ucfirst(JrTexto::_('try again')); ?></a>
            <a class="btn btn-inline btn-success btn-lg save-progreso" ><?php echo ucfirst(JrTexto::_('continue')) ?> <i class="fa fa-arrow-right"></i></a>
        </div>
    </div>
</div>

<section class="hidden" id="msjes_idioma" style="display: none;">
    <input type="hidden" id="attention" value="<?php echo JrTexto::_('Attention');?>">
    <input type="hidden" id="no_puedes_arrastrar" value='<?php echo JrTexto::_('You can not drag another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="no_puedes_seleccionar" value='<?php echo JrTexto::_('You can not select another option').'. '.JrTexto::_('You must click on "try again"');?>'>
    <input type="hidden" id="you_must_finish" value='<?php echo ucfirst(JrTexto::_('you must do the exercise before trying again')); ?>'>
    <input type="hidden" id="bien_hecho" value='<?php echo ucfirst(JrTexto::_('well done!')); ?>'>
    <input type="hidden" id="cargando" value='<?php echo ucfirst(JrTexto::_('Loading')); ?>'>
    <input type="hidden" id="ejerc_exito" value='<?php echo ucfirst(JrTexto::_('you have complete the exercise succesfully')); ?>.'>
    <input type="hidden" id="ir_sgte_ejerc" value='<?php echo ucfirst(JrTexto::_('continue to the next exercise')); ?>.'>
    <input type="hidden" id="finalizar" value='<?php echo ucfirst(JrTexto::_('you have finished the activity')); ?>.'>
    <input type="hidden" id="puedes_continuar" value='<?php echo ucfirst(JrTexto::_('you can continue next exercise')); ?>.'>
    <input type="hidden" id="tiempo_acabo" value='<?php echo ucfirst(JrTexto::_('time is over')); ?>.'>
    <input type="hidden" id="intentalo_otra_vez" value='<?php echo ucfirst(JrTexto::_('you can try again')); ?>.'>
    <input type="hidden" id="select_activity" value='<?php echo ucfirst(JrTexto::_('Select Activity')); ?>.'>
    <input type="hidden" id="select_upload" value='<?php echo JrTexto::_('Select or upload'); ?>'>
    <input type="hidden" id="click_add_description" value='<?php echo JrTexto::_('Click here to add description') ?>'>
    <input type="hidden" id="click_add_title" value='<?php echo JrTexto::_('Click here to add title') ?>'>
    <input type="hidden" id="remove_tab" value='<?php echo JrTexto::_('remove tab') ?>'>
    <input type="hidden" id="debes_completar_fichas" value='<?php echo JrTexto::_('you must complete sheets correctly');?>'>
    <input type="hidden" id="no_hay_audio" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback')); ?>'>
    <input type="hidden" id="no_hay_audio_en_escena" value='<?php echo ucfirst(JrTexto::_('no audio was detected for playback in scene')); ?>'>
    <input type="hidden" id="no_hay_orden_en_escena" value='<?php echo ucfirst(JrTexto::_('you have not set an order in the dialogues in scene')); ?>'>
    <input type="hidden" id="debes_establecer_orden" value='<?php echo ucfirst(JrTexto::_('you must set an order for the dialog bubbles')); ?>'>
    <input type="hidden" id="write_tag" value='<?php echo ucfirst(JrTexto::_('write tag')); ?>'>
    <input type="hidden" id="ejerc_no_completado" value='<?php echo ucfirst(JrTexto::_('you have not finished editing this exercise')); ?>'>
    <input type="hidden" id="grabar_audio" value='<?php echo ucfirst(JrTexto::_('Record')); ?>'>
    <input type="hidden" id="detener_audio" value='<?php echo ucfirst(JrTexto::_('Stop')); ?>'>
    <input type="hidden" id="escribir_texto" value='<?php echo ucfirst(JrTexto::_('Write')); ?>'>
    <input type="hidden" id="reproducir_audio" value='<?php echo ucfirst(JrTexto::_('Play')); ?>'>
    <input type="hidden" id="end" value='<?php echo ucfirst(JrTexto::_('End')); ?>'>
</section>

<script type="text/javascript">
//document.getElementById('div_imagen').style.display="none";
var _IDHistorialSesion=0;
//variables para las id's de los hitoriales
var oIdHistorial = {'games': 0,'practice': 0}

var optSlike={
    infinite: false,
    navigation: false,
    slidesToScroll: 1,
    centerPadding: '10px',
    slidesToShow: 7,
    responsive:[
    { breakpoint: 1200, settings: {slidesToShow: 6} },
    { breakpoint: 992, settings: {slidesToShow: 5 } },
    { breakpoint: 880, settings: {slidesToShow: 4 } },
    { breakpoint: 720, settings: {slidesToShow: 3 } },
    { breakpoint: 320, settings: {slidesToShow: 2} }
    ]
};

var pos=-1;
var posv=-1;
var showdiv=[];

function fileExists(url) {
    var rspta = false;
    if(url){
        var req = new XMLHttpRequest();
        req.open('GET', url, false);
        req.send();
        rspta = (req.status==200);
    }
    return rspta;
}

function existeEnSlider(url, $panel){
    var rspta = true;
    if(url){
        var archivo = $panel.find('.myslider *[src="'+url+'"]');
        rspta = (archivo.length>0);
    }
    return rspta;
}

function insertarFileLista(){
    
}

var guardarTResource = function($tipo,$file,$sw){
    
};

var showPrimerElemento = function ($contenedor) {
    if($contenedor.length==0){ return false; }
    $contenedor.find('.items-select-list .item-select').first().trigger('click');
    $contenedor.find('.items-select-list').removeClass('items-select-list');
}

var calcularProgresoMultimedia = function($tabPane){
    var porcentaje = 0.0;
    var numDivs=$tabPane.find('.myslider .miniatura').length;
    var numDivsVistos=$tabPane.find('.myslider .miniatura.visto').length;
    if(numDivs>0){
        porcentaje=(numDivsVistos/numDivs)*100;
    }
    return porcentaje;
};  

var totalPestanias = function(){
    /* Solo las pestañas (top o left) visibles en el smartbook */
    var cant = 0;
    $("#navTabsTop, #navTabsLeft").find('li>a').not('a[href="#div_presentacion"],a#exit').each(function(i, elem) {
        var isVisible = $(elem).hasClass('hidden') || $(elem).is(':visible')
        if(isVisible){ cant++; }
    });

    cant--; /* la pestaña div_games aun no se guarda, por loq  no se está considerando. 
                Esta linea deberá borrarse una vez implementado el guardar de juegos */

    return cant;
};

var guardarBitacora = function(dataSave, $tabPane){
    //if($('#rolUsuario').val()!='Alumno'){ console.log('No es Alumno - No guardar'); return false; }
    $.ajax({
        url: _sysUrlBase_+'/bitacora_smartbook/ajax_agregar',
        type: 'POST',
        dataType: 'json',
        data: dataSave,
        async : false,
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idLogro = $('#idLogro').val();
            var idSesion = $('#idSesion').val();
            var tieneLogro = idLogro>0;
            $tabPane.attr('data-idbitacora', resp.newid);
            if(resp.progreso>=100 && tieneLogro && resp.bandera==0){
                mostrarLogroObtenido({'id_logro':idLogro, 'idrecurso': idSesion  });
            }
        }
    }).fail(function(e) {
        console.log("!Error", e);
    });
};

var show_hide_Scroller = function($tab=null) {
    if($tab===null) return false;
    var containerHeight = $tab.closest('div.tab-pane[data-tabgroup="navTabsLeft"]').outerHeight();
    var contentHeight = 0;
    $tab.children().each(function(index, el) {
        contentHeight += $(el).outerHeight();
    });
    if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()+25; }
    if(contentHeight > containerHeight){
        $('#scroll-down').show();
    }else{
        $('#scroll-down').hide();
    }
};

var contarNumeroEjercicio = function($tabPane) {
    var $pnlNroEjercicio = $tabPane.find('.numero_ejercicio');
    if($pnlNroEjercicio.length===0){ return false; }
    
    var total_txt = $tabPane.find('ul.nav.ejercicios li').not('.comodin').length;
    var $actualTab = $tabPane.find('ul.nav.ejercicios li.active');
    var actual_txt = $actualTab.find('a[data-toggle="tab"]').text();
    if($actualTab.length==0){ actual_txt = MSJES_PHP.end; total_txt=''; }
    $pnlNroEjercicio.find('.actual').text(actual_txt);
    $pnlNroEjercicio.find('.total').text(total_txt);
    $('.infouserejericio.ejeinfo-numero .actual').text(actual_txt);
    $('.infouserejericio.ejeinfo-numero .total').text(total_txt);
};

var initBitac_Alum_Smbook = function () {
    if( $('#idBitacoraAlumnoSmbook').val()!=='' ) { return false; }
    $.ajax({
        url: _sysUrlBase_ + '/bitacora_alumno_smartbook/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: {
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : $('#idSesion').val(),
            'txtEstado' : 'P',
        },
    }).done(function(resp) {
        if(resp.code=='ok'){
            $('#idBitacoraAlumnoSmbook').val(resp.newid);
        }
    }).fail(function(err) {
        console.log("!Error", err);
    }).always(function() {});
};

var resetearEjercicio_=function(aquiplantilla,tipo){
   var idmet=aquiplantilla.closest('.metod').attr('data-idmet');
   var plantilla=aquiplantilla.find('.plantilla'); 
    if(idmet==3){
        //$('.infouserejericio.ejeinfo-tiempo').show().css({'opacity':1}).removeClass('hidden');
        $('.infouserejericio.ejeinfo-intento').show().css({'opacity':1}).removeClass('hidden');
        if(!plantilla.hasClass('yalointento') && !plantilla.hasClass('tiempo-pausado')){   
            resetAllplantilla(plantilla);
        }else{
            $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
        }
       var intento_actual=parseInt(plantilla.attr('data-intento')||1);
        $('#panel-intentos-dby').find('.actual').text(intento_actual);
    }else{
        //$('.infouserejericio.ejeinfo-tiempo').hide();
        $('.infouserejericio.ejeinfo-intento').hide();       
        $('.btnejercicio-next_').removeAttr('disabled').removeClass('disabled');
    }
   var pnlternativas=plantilla.find('.panelAlternativas');
    if(pnlternativas.length>0){
        txtlternativas=pnlternativas.text().replace(/\s||\n||\r/g,'');
        if(txtlternativas=='') pnlternativas.parent().hide();
        else pnlternativas.css({'margin-bottom':'0px'});
    }       
    $('#btns_control_alumno .botones-control').css({'margin-top':'1.3em'});
}

var registrarHistorialSesion = function(idTabPane = null){
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    var type = function(t){
        switch(t){
            case '#div_games': { return 'G'; } break;
            case '#div_practice' : { return 'A';} break;
        }
        return 'TR';
    };

    var lugar = idTabPane == null ? 'TR' : type(idTabPane);
    // alert(lugar);
    $.ajax({
        url: _sysUrlBase_+'/historial_sesion/agregar',
        type: 'POST',
        dataType: 'json',
        data: {'lugar': lugar, 'fechaentrada': fechahora, 'idcurso': $('#idCurso').val() },
    })
    .done(function(resp) {
        if(resp.code=='ok'){
            if(idTabPane === null){
                _IDHistorialSesion = resp.data.idhistorialsesion;
            }else{
                if(lugar === 'G'){
                    oIdHistorial.games = resp.data.idhistorialsesion; 
                }else if(lugar === 'A'){
                    oIdHistorial.practice = resp.data.idhistorialsesion;
                }else{
                    oIdHistorial.games = resp.data.idhistorialsesion;
                }
            }
        }else if(resp.code==201){
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.Error, 'error');
            redir('');

        }else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
    });
    return 0;
};

var editarHistoriaSesion = function(id = null,idTabPane = null){
    
    var _id = id != null ? id : 0 ;
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    var type = function(t){
        switch(t){
            case '#div_games': { return 'G'; } break;
            case '#div_practice' : { return 'A';} break;
        }
        return 'TR';
    };
    var lugar = idTabPane == null ? 'TR' : type(idTabPane);
    $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _id, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                if(id === null){
                    _IDHistorialSesion = resp.data.idhistorialsesion;
                }else{
                    if(lugar === 'G'){
                        oIdHistorial.games = resp.data.idhistorialsesion; 
                    }else if(lugar === 'A'){
                        oIdHistorial.practice = resp.data.idhistorialsesion;
                    }else{
                        oIdHistorial.games = resp.data.idhistorialsesion;
                    } 
                }
            }else if(resp.code==201){
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.Error, 'error');
                redir('');

            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
};
var activarbtnnext=function(activotmp,back){  
    if(!activotmp){
        $('.btnejercicio-back_').removeClass("disabled inidisabled").children('i.fa-spin').remove();
        $('.btnejercicio-next_').removeClass("disabled inidisabled").children('i.fa-spin').remove();
        $('#btns_control_alumno .save-progreso').removeClass("disabled inidisabled").children('i.fa-spin').remove();
    }else{
        $('.btnejercicio-back_').addClass("disabled inidisabled");
        if(back)$('.btnejercicio-back_').prepend('<i class="fa fa-gear fa-spin"></i>');
        $('.btnejercicio-next_').addClass("disabled inidisabled");
        $('#btns_control_alumno .save-progreso').addClass("disabled inidisabled");
        if(!back){
            $('.btnejercicio-next_').append('<i class="fa fa-gear fa-spin"></i>');
            $('#btns_control_alumno .save-progreso').append('<i class="fa fa-gear fa-spin"></i>');
        }
     
    }

}
function funciontest(obj = null){
    $(".nubeclick").remove();
    // console.log("llamado");
    var contenedor = $(".tab-content .tabhijo.active").find(".plantilla-completar");
    var inputs = contenedor.find(".mce-object-input");
    
    contenedor.find(".nubeclick").remove();
    
    inputs.each(function(){
        if($(this).hasClass("iswrite") || $(this).hasClass("ischoice")){
            if($(this).val() == "" || $(this).val().length == 0 ){
                    var idDinamico = $(this).attr("id")+"nube";
                    var padre = $(this).parent();
                   /* if(padre.hasClass("input-flotantes")){
                        padre = padre.parent();
                    }*/
                    var input = $(this);
                    $(this).data("nube",idDinamico);
                    padre.append("<span class='nubeclick' id='"+idDinamico+"'><?php echo JrTexto::_('Click aquí');?> </span>");
                    padre.find("#"+idDinamico).css({ top:(input.position().top + 23.5).toString() + "px", "left": input.position().left.toString() + "px" });
                    
            }
        }
    });
}
function showPanels(){
    var element = $('#navTabsLeft');
    if($(window).width() <= 425){
        $('#botonMenuCollapse').show();
        
        if(element.hasClass('in') && $('#botonMenuCollapse').hasClass('collapsed')){
            element.removeClass('in');
            // element.css('height','0px');
        }
    }else{
        element.addClass('in');
        element.css('height','initial');
        $('#botonMenuCollapse').addClass('collapsed');
        $('#botonMenuCollapse').hide();
    }
}
function modResponsive(){
    if($('#panel-intentos-dby').css('display') != 'none'){
        $('.btnejercicio-next_').closest('div').removeClass('col-xs-4');
        $('.btnejercicio-next_').closest('div').addClass('col-xs-3');
        $('.btnejercicio-back_').closest('div').removeClass('col-xs-4');
        $('.btnejercicio-back_').closest('div').addClass('col-xs-3');
        $('.ejeinfo-numero').closest('div').removeClass('col-xs-4');
        $('.ejeinfo-numero').closest('div').addClass('col-xs-6');
        if($(this).width()<= 458 && $(this).width() > 400 ){
            $('#panel-intentos-dby').css({padding:'0px',fontSize:'12.5px'});
            $('.ejeinfo-numero').css({padding:'0px',fontSize:'12.5px'});
        }else if($(this).width() <= 400){
            $('#panel-intentos-dby').css({padding:'0px',fontSize:'10px'});
            $('.ejeinfo-numero').css({padding:'0px',fontSize:'10px'});
            $('.btnejercicio-next_').closest('div').removeClass('col-xs-3');
            $('.btnejercicio-next_').closest('div').addClass('col-xs-4');
            $('.btnejercicio-back_').closest('div').removeClass('col-xs-3');
            $('.btnejercicio-back_').closest('div').addClass('col-xs-4');
            $('.ejeinfo-numero').closest('div').removeClass('col-xs-6');
            $('.ejeinfo-numero').closest('div').addClass('col-xs-4');
        }else{
            $('#panel-intentos-dby').css({padding:'',fontSize:''});
            $('.ejeinfo-numero').css({padding:'',fontSize:''});
        }
    }else{
        $('#panel-intentos-dby').css({padding:'0px',fontSize:''});
        $('.ejeinfo-numero').css({padding:'0px',fontSize:''});
    }
    
}
$(document).ready(function(){
    showPanels();
    modResponsive();
    $('.tab-pane[data-tabgroup="navTabsLeft"]').find('.nopreview').remove(); /*ejercicios y demás: quitar .nopreview*/
    $('#div_voca').find('.btnremovevoca').remove(); /*vocabulario: quitar .btnremovevoca*/
    setTimeout(function() {
        $( "#div_presentacion .div_acciones" ).fadeIn( "fast" );
    }, 2100);
    initBitac_Alum_Smbook();
    
    $('.btnejercicio-back_').on('click',function(ev){
        $this=$(this);
        $this.children('i.fa-spin').remove();
        if($this.hasClass('inidisabled')) return false;
        activarbtnnext(true,true);
        var tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0){ tabPaneActivo=$('#div_autoevaluar.active');}
        var tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
        var indexhijo=tabejercicioactivo.index();
        if(indexhijo==0){
            idtab=tabPaneActivo.attr('id');
            if(idtab=='div_practice') $('a[href="#div_look"]').trigger('click');
            else if(idtab=='div_autoevaluar') $('a[href="#div_voca"]').trigger('click');
        }else{            
            tabejercicioactivo.prev('li').children('a').trigger('click');
        }
        setTimeout(function(){
            activarbtnnext(false,true); 
            funciontest();
        },1450);
    })

    $('.btnejercicio-next_').on('click',function(ev){
        $this=$(this);
        $this.children('i.fa-spin').remove();
        if($this.hasClass('inidisabled')) return false;
        $this.addClass('inidisabled');
        activarbtnnext(true,false);
        guardarProgreso();
        var tabPaneActivo=$('#div_practice.active');
        if(tabPaneActivo.length==0){           
            tabPaneActivo=$('#div_autoevaluar.active');
        }
        var plantilla=tabPaneActivo.find('.tabhijo.active').find('.plantilla');
        try{
            if(plantilla.length){
                var pltaulumno=plantilla.find('.pnl-speach.alumno');
                var btn=pltalumno.find('.btnGrabarAudio');
                if(btn.length){
                    if(btn.children('i.fa-microphone').length){
                        if(!btn.hasClass('btn-danger')){
                            if(btn.hasClass('hide')) btn.trigger('click');
                        }
                    }
                }
            }
            var btn=plantilla.find('.');
        }catch(ex){}
        var progreso = parseFloat( tabPaneActivo.find('.metod').attr('data-progreso'));

        guardarBitacora({
            'Idbitacora' : tabPaneActivo.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : '<?php echo $this->idcursoDetalle;?>',
            'txtIdsesionB': $('#idSesion').val(),
            'txtPestania' : tabPaneActivo.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, tabPaneActivo); 
       
       var tabactivo=tabPaneActivo.find('.tabhijo.active');
       var tabejercicioactivo=tabPaneActivo.find('ul.ejercicios').children('li.active');
       var indexhijo=tabejercicioactivo.index();
       var totalhijos=tabPaneActivo.find('ul.ejercicios').find('li').length-2;
       idtab=tabPaneActivo.attr('id');
        if(indexhijo==totalhijos){ 
            if(idtab=='div_practice') $('a[href="#div_games"]').trigger('click');
            else if(idtab=='div_autoevaluar'){
               var href=$('a#exit').attr('href'); console.log(' ir a cargar siguiente sesion',href);
               window.location.href=href;
            }
        }else{
            if(idtab=='div_autoevaluar') tabactivo.find('.plantilla').addClass('avanzoejercicio');
            tabejercicioactivo.next('li').children('a').trigger('click');
        } 
        setTimeout(function(){
            activarbtnnext(false,true);
            funciontest(tabejercicioactivo);
        },1450);

    })

    $('#div_games').on('click','.continue-game',function(){
        /**
         * Boton de siguiente en la seccion de juegos....
         */
        var listaSmartbook = $('#navTabsLeft');
        var listaSmartbookLi = listaSmartbook.find("li");
        var listaSmartbookLiActive = listaSmartbook.find("li.active");
        var listaSmartbookLiCount = listaSmartbookLi.length;
        var listaJuegos = $('.list-games');
        var listaJuegosLi = listaJuegos.find("li");
        var listaJuegosLiCount = listaJuegosLi.length - 1;
        var indice = listaJuegos.find("li.active").index() + 1;
        
        if((indice % listaJuegosLiCount) == 0){
            var indiceSmartbook =  listaSmartbookLiActive.index() + 1;
            if((indiceSmartbook  % listaSmartbookLiCount)){
                listaSmartbookLi.eq(indiceSmartbook).find("a").click();
            }
        }else{
            listaJuegosLi.eq(indice).find("a").click();
        }
    });
    
    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        var $actualTab = $(this);
        var idTabPane = $actualTab.attr('href');
        var $tabpane = $(idTabPane);
        var $slider = $tabpane.find('.myslider');
        var idNavTab = $actualTab.closest('.nav-tabs').attr('id');
        setTimeout(function() { show_hide_Scroller($tabpane); }, 500);
        contarNumeroEjercicio($tabpane);
        if( $slider.length>0 && !$slider.hasClass('slick-initialized')){
            $slider.slick(optSlike);
        }
        showPrimerElemento($tabpane);
        $( "#div_presentacion .div_acciones" ).hide();
        $('audio').each(function(){ this.pause() });
        $('video').each(function(){ this.pause() });
        if( idNavTab==='navTabsTop' || idNavTab==='navTabsLeft' ){            
            $('#smartbook-ver #navTabsTop a[data-toggle="tab"], #smartbook-ver #navTabsLeft a[data-toggle="tab"]').not($actualTab).closest('li.active').removeClass('active');            
            if(idTabPane==="#div_look" || idTabPane==="#div_practice" || idTabPane==="#div_autoevaluar"){ 
                $('.tab-pane .content-smartic.metod.active').removeClass('active');
                $tabpane.find('.content-smartic.metod').addClass('active');
                if(!$tabpane.hasClass('smartic-initialized')){
                    $tabpane.tplcompletar({editando:false});
                }
                rinittemplatealshow();
                modResponsive();
                
                if(idTabPane==="#div_look"){
                   $('#contenedor-paneles').addClass('hidden');  // oculta el progreso;
                }else if(idTabPane==="#div_practice"){
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                   var panelActivo=$('#div_practice').find('.tabhijo.active');
                    resetearEjercicio_(panelActivo);
                    registrarHistorialSesion(idTabPane);
                    setTimeout(function(){  rinittemplate_nlsw(true); funciontest(); },550); 
                }else{                    
                    $('#contenedor-paneles').removeClass('hidden'); // muestra progreso
                    var panelActivo=$('#div_autoevaluar').find('.tabhijo.active');
                    revisarejericio(panelActivo.find('.plantilla')); 
                    setTimeout(function(){  rinittemplate_nlsw(true); funciontest(); },550);                   
                }
                activarbtnnext(false,false);
            }else{
                $('#contenedor-paneles').addClass('hidden'); // muestra el progreso
                activarbtnnext(false,false);
            }

            if(idNavTab==='navTabsLeft' && ( idTabPane==="#div_look" || idTabPane==="#div_voca" || idTabPane==="#div_workbook")){ // agregar cuand guardar games idTabPane==="#div_games" ||
                var $tabPane = $(idTabPane);
                var progreso = 100.00;
                guardarBitacora({
                    'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
                    'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val()||null,
                    'txtIdcurso' : $('#idCurso').val(),
                    'txtIdsesion' : '<?php echo $this->idcursoDetalle;?>',
                    'txtIdsesionB': $('#idSesion').val(),
                    'txtPestania' : $tabPane.attr('id'),
                    'txtTotal_Pestanias' : totalPestanias(),
                    'txtProgreso' : progreso.toFixed(2),
                    'txtOtros_datos' : null,
                    'txtIdlogro' : $('#idLogro').val(),
                }, $tabPane);
            }
            if(idNavTab==="navTabsLeft" && idTabPane === "#div_games"){
                registrarHistorialSesion(idTabPane);
            }
        }
    });

    $('#smartbook-ver ul.nav.ejercicios').on('shown.bs.tab', 'a[data-toggle="tab"]', function(ev) {
        ev.preventDefault();
        //$('#panel-tiempo .info-show').trigger('oncropausar'); 
        var $idpane=$(this).closest('.tab-pane').closest('.tab-pane').attr('id');
        var idTab = $(this).attr('href');
        var $tabPane = $(idTab).closest('div[data-tabgroup="navTabsLeft"]');
        contarNumeroEjercicio($tabPane);
        var $tabhijoActive=$tabPane.find('.tabhijo.active');        
        var totalpuntaje=100/$tabPane.find('.tabhijo').length; // total puntaje por ejericio.         
        if($idpane=='div_autoevaluar'){
            revisarejericio($tabhijoActive.find('.plantilla'));           
        }
        activarbtnnext(false,false);
    })

    //Obtener el evento cuando no se selecciona el boton nav-tab con direccion especifica
    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[href!="#div_games"]', function (e){
        editarHistoriaSesion(oIdHistorial.games,"#div_games");
    });
    //Obtener el evento cuando no se selecciona el boton nav-tab con direccion especifica
    $('#smartbook-ver .nav-tabs').on('shown.bs.tab', 'a[href!="#div_practice"]', function (e){  
        editarHistoriaSesion(oIdHistorial.practice,"#div_practice");
        activarbtnnext(false,false);
    });  

    $('#div_practice, #div_autoevaluar').on('click', '#btns_control_alumno .save-progreso', function(e) {

        $this=$(this);
        $this.children('i.fa-spin').remove();
        if($this.hasClass('inidisabled')) return false;
        $('.btnejercicio-next_').trigger('click');
       // activarbtnnext(true,false);
       /* e.preventDefault();
        var $tabPane = $(this).closest('[data-tabgroup]');
        var progreso = parseFloat( $tabPane.find('.metod').attr('data-progreso') );
        guardarBitacora({
            'Idbitacora' : $tabPane.attr('data-idbitacora')||null,
            'Idbitacora_smartbook' : $('#idBitacoraAlumnoSmbook').val(),
            'txtIdcurso' : $('#idCurso').val(),
            'txtIdsesion' : '<?php //echo $this->idcursoDetalle;?>',
            'txtPestania' : $tabPane.attr('id'),
            'txtTotal_Pestanias' : totalPestanias(),
            'txtProgreso' : progreso.toFixed(2),
            'txtOtros_datos' : null,
            'txtIdlogro' : $('#idLogro').val(),
        }, $tabPane);
        setTimeout(function(){activarbtnnext(false,false);},1450);*/
    });
    
    $('#smartbook-ver .preview').click(function(){
        $(this).hide();
        $(this).siblings('.back').show();
        $('#navTabsLeft li a[data-toggle="tab"]').first().trigger('click');
        $('.controles-edicion').hide();
        $('#smartbook-ver .descripcion-media').attr('readonly','readonly');  
        $('#smartbook-ver .nopreview').hide();
        $('#smartbook-ver').removeClass('editando');
    });

    $('#smartbook-ver .back').click(function(){
        $(this).hide();
        $(this).siblings('.preview').show();
        $('.controles-edicion').show();
        $('#smartbook-ver .descripcion-media').removeAttr('readonly');
        $('#smartbook-ver .nopreview').show();
        $('#smartbook-ver').addClass('editando');
    });

    var iScrollPos = 0;
    $('#smartbook-ver #tabContent > .tab-pane').scroll(function(e) {
        var iCurScrollPos = $(this).scrollTop();
        var tabPaneHeight = $(this).outerHeight();
        var contentHeight = 0;

        $(this).children().each(function(index, el) {
            contentHeight += $(el).outerHeight(); //+71;
        });

        if( !$('#contenedor-paneles').hasClass('hidden') ){ contentHeight += $('#contenedor-paneles').outerHeight()-3; }

        if (iCurScrollPos > iScrollPos) {
            $('#scroll-up').show();
            /*console.log(iCurScrollPos+' + '+tabPaneHeight+' +10= ', iCurScrollPos + tabPaneHeight+10);
            console.log('contentHeight = ', contentHeight);*/
            if(iCurScrollPos + tabPaneHeight-10 >= contentHeight) { /* LLego al final?*/
                $('#scroll-down').hide();
            }else {
                $('#scroll-down').show();
            }
        } else {
            $('#scroll-down').show();
            if(iCurScrollPos==0){ /* LLego al inicio?*/
                $('#scroll-up').hide();
            }else {
                $('#scroll-up').show();
            }
        }
        iScrollPos = iCurScrollPos;
    });

    $('#scroll-down').click(function(e) {  $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: 750}, 1200, 'linear'); });
    $('#scroll-up').click(function(e) { $('#smartbook-ver #tabContent > .tab-pane.active').animate({ scrollTop: -750}, 1200, 'linear'); });

/*** #Fn generales(Pdf,Aud,Img,Vid) ***/
    $('#smartbook-ver')
        .on('click', '.live-edit', function(e){
            if(!$('#smartbook-ver').hasClass('editando')){  return false;  }
            $(this).css({"border": "0px"}); 
            addtext1(e,this);
        }).on('blur','.live-edit>input',function(e){
            e.preventDefault();
            $(this).closest('.live-edit').removeAttr('Style'); 
            addtext1blur(e,this);
        }).on('keypress','.live-edit>input', function(e){
            if(e.which == 13){ 
                e.preventDefault();          
                $(this).trigger('blur');
            } 
        }).on('keyup','input', function(e){
            if(e.which == 27){ 
                $(this).attr('data-esc',"1");
                $(this).trigger('blur');
            }
        });

    $('#div_presentacion').on('click', '.btn-continuar', function(e) {
        e.preventDefault();
        $('.nav-tabs li.ultimo_visto a').trigger('click');
    });
   
    $('#div_games').on('click', '.nav-tabs a[data-toggle="tab"]', function(e) {
        e.preventDefault();
        var idGame = $(this).data('idgame');
        var titulo = $(this).data('title');
        var $container = $(this).closest('.tab-pane').find('.mascara_visor');
        if($(window).width() <= 425){
            $container.html('<div style="padding:2em;"><h1>'+titulo+'</h1><div><a href="'+_sysUrlBase_+'/game/ver/'+idGame+'" target="_blank" class="btn btn-primary"><?php echo JrTexto::_('Play') ?></a></div></div>');
        }else{
            $container.html('<iframe src="'+_sysUrlBase_+'/game/ver/'+idGame+'" frameborder="0"></iframe>');
        }
    });

    $('#smartbook-ver .preview').trigger('click');
    $('#smartbook-ver .preview').hide();
    $('#smartbook-ver .back').hide();

    $('#smartbook-ver .plantilla-speach').each(function(index, el) {
        initspeach( $(el) );
        if($(el).closest('.tabhijo').hasClass('active')){
            rinittemplatealshow();
        }
    });
    
    $(window).on('resize',function(){
        showPanels();
        modResponsive();
    });
/******************************************************/
    registrarHistorialSesion();
    $(window).on('beforeunload', function(){        
        editarHistoriaSesion(_IDHistorialSesion);
        editarHistoriaSesion(oIdHistorial.games,"#div_games");
        editarHistoriaSesion(oIdHistorial.practice,"#div_practice");
    });
});

</script>