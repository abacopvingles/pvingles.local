<div class="row pnlman">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo JrTexto::_("DBY exercises"); ?></h3>
						<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					</div>
					<div class="panel-body">			
						<table class="table table-responsive table-striped">
							<tr><th><?php echo JrTexto::_('Course') ?></th><th><?php echo JrTexto::_('Number of attemps') ?></th></tr>
							<?php 
							if($this->datosintentos)
							foreach ($this->datosintentos as $k){?>
								<tr>
								<td><?php echo $k["cursonombre"]; ?></td>
								<td >
								 <input type="number" idgeneral="<?php echo $k["idgeneral"];?>"
								 idnombre="<?php echo $k["nombre"];?>"
								 tipo_tabla="<?php echo $k["tipo_tabla"];?>"
								 mostrar="<?php echo $k["mostrar"];?>"
								 id="nintentos" name="nintentos" required="required" class="nintentos form-control" value="<?php echo @$k["codigo"];?>">
								</td>
							</tr>							

							<?php } ?>
							
						</table>
					</div>
				</div> 
			</div>
			<div class="col-sm-6 col-xs-12">
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo JrTexto::_("Platform Language"); ?></h3>
						<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					</div>
					<div class="panel-body">
						<?php $idioma=$this->documento->getIdioma(); ?>
						<table class="table table-responsive table-striped tableidioma">
							<tr><td><?php echo JrTexto::_("Spanish"); ?></td>
							<td><a href="javascript:;"  class="changeidioma2" idioma="ES"> <i class="idioma fa fa<?php echo $idioma=='ES'?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $idioma=='ES'?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a></td></tr>
							<tr><td><?php echo JrTexto::_("English"); ?></td>
							<td><a href="javascript:;"  class="changeidioma2" idioma="EN"> <i class="idioma fa fa<?php echo $idioma=='EN'?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $idioma=='EN'?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a></td></tr>
						</table>
					</div>
				</div> 
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.changeidioma2').click(function(){
		idi=$(this).attr('idioma');
		localStorage.setItem("sysidioma", idi);
		xajax__('', 'idioma', 'cambiaridioma', idi);
		var iobj=$(this).closest('tableidioma').find('.idioma');
		$(iobj).removeClass('fa-check-circle-o').addClass('fa-circle-o');
		$('i.idioma',this).removeClass('fa-circle-o').addClass('fa-check-circle-o');
	});
	$('.nintentos').blur(function(){
		var nintentos=$(this).val();		
		if(nintentos<=0){
			mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','El numero de intentos debe ser mayor a 1','danger');
		 	return	$(this).focus();
		}
		var _this=$(this);
		var formData = new FormData();      
      	formData.append('idgeneral',_this.attr('idgeneral'));
      	formData.append('nombre',_this.attr('idnombre'));
      	formData.append('tipo_tabla',_this.attr('tipo_tabla'));
      	formData.append('codigo',nintentos);
      	formData.append('mostrar',_this.attr('mostrar'));
	    var data={
	        fromdata:formData,
	        url:_sysUrlBase_+'/general/guardarPersonalizado',
	        msjatencion:'<?php echo JrTexto::_('Attention');?>',
	        type:'json',
	        callback:function(dt){mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',dt.msj,'success');}
	    }
		sysajax(data);     
	});
});	
</script>