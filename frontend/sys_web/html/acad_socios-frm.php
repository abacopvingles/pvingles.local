<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$return=!empty($_REQUEST["return"])?$_REQUEST["return"]:'false';
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
</style>
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Acad_licencias'">&nbsp;<?php echo JrTexto::_('Licencias'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="ventfrm<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
          <input type="hidden" id="datareturn<?php echo $idgui ?>" value="<?php echo $return;?>">
          <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"]; ?>">
          <input type="hidden" name="idproyecto" id="idpersona<?php echo $idgui; ?>" value="">
          <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion; ?>">
          <input type="hidden" name="rol" id="rol<?php echo $idgui; ?>" value="<?php echo !empty($rol)?$rol:10; ?>">
          <div class="col-xs-12 col-sm-12 col-md-6">
            <label><?php echo  ucfirst(JrTexto::_("Business"))?></label>
                <div class="form-group">
                	<div class="cajaselect">            
                    <select id="idempresa<?php echo $idgui; ?>" name="idempresa" class="form-control" >  
                      <!--option value="0" ><?php echo  ucfirst(JrTexto::_("Select a business"))?></option-->
                      <?php 
                      if(!empty($this->empresas))
                        foreach($this->empresas as $emp){?>
                          <option value="<?php echo $emp["idempresa"]; ?>" ><?php echo  ucfirst($emp["nombre"]);?></option>
                        <?php }
                      ?>
                    </select>
                  </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-12 col-md-6"><hr><h3>Datos del partner</h3></div>

        
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Document type")); ?></label>
            <div class="cajaselect">
              <select name="tipodoc" id="tipodoc<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
              <input type="text" class="form-control" name="dni" id="tmpdni<?php echo $idgui; ?>" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4"><br>
          <a class="btn btn-info btnsearch<?php echo $idgui ?>" href="javascript:void(0)"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></a>          
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="nombre" id="nombre<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_paterno" id="ape_paterno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_paterno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_materno" id="ape_materno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_materno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
            <div class="cajaselect">
              <select name="sexo" id="sexo<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
            <div class="cajaselect">
              <select name="estado_civil" id="estado_civil<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                  <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                    <?php echo $fkestado_civil["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div> 

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="telefono" id="telefono<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["telefono"]); ?>" > 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
              <input type="text" class="form-control" name="celular" id="celular<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["celular"]); ?>" > 
          </div> 
        </div> 
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center">        
          <div class="col-xs-12 col-sm-12 col-md-12 text-center contool">
            <div><label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Photo')); ?></label></div>
            <div style="position: relative;">
              <div class="toolbarmouse"><span class="btn"><i class="fa fa-pencil"></i></span></div>
              <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="foto_alumno img-responsive center-block thumbnail" id="foto<?php echo $idgui; ?>">
              <input type="file" class="input-file-invisible" name="foto" id="fileFoto<?php echo $idgui; ?>" accept="image/*">
            </div>      
          </div>                   
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <label><?php echo ucfirst(JrTexto::_("Birthday")); ?></label>
              <div class="form-group">
                <div class="input-group date datetimepicker">
                  <input type="text" class="form-control" required="required" name="fechanac" id="fechanac<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechanac"]); ?>"> 
                  <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
              </div>
            </div>
          </div>
      </div>
        <div class="clearfix"></div><br>
        <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12"><hr></div>
            <div class="col-md-12 text-center">
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_licencias'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
              <button id="btn-saveAcad_licencias" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            </div>
          </div>
        </div>
  </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var changefoto=false;
    var msjatencion='<?php echo JrTexto::_('Attention');?>';
    var idgui='<?php echo !empty($idgui)?$idgui:now();?>';
    var buscaralumno<?php echo $idgui; ?>=function(){
      var dniobj=$('#tmpdni'+idgui);
      var dni=dniobj.val();
      if(dni=='') {
         mostrar_notificacion(msjatencion,'<?php echo JrTexto::_('empty information');?>','warning');
        dniobj.focus();
        return false;
      }
      $('#idpersona'+idgui).val(0);
       var formData = new FormData();       
      formData.append('dni', dni); 
     var data={
        fromdata:formData,
        url:_sysUrlBase_+'/personal/buscarjson',
        msjatencion:msjatencion,
        type:'json',
        //showmsjok : true,
        callback:function(rs){
          var dt=rs.data;
          if(dt[0]!=undefined){
            var rw=dt[0];
            $('#idpersona'+idgui).val(rw.idpersona);
            $('#nombre'+idgui).val(rw.nombre);
            $('#ape_paterno'+idgui).val(rw.ape_paterno);
            $('#ape_materno'+idgui).val(rw.ape_materno);
            $('#sexo'+idgui).val(rw.sexo);
            $('#telefono'+idgui).val(rw.telefono);
            $('#celular'+idgui).val(rw.celular);
            $('#fechanac'+idgui).val(rw.fechanac);
            $('#estado_civil'+idgui).val(rw.estado_civil||'S');
            if(rw.foto!=''){$('#foto'+idgui).attr('src',_sysUrlStatic_+'/media/usuarios/'+rw.foto);}
           if($('#datareturn').val()!='false'){$('.btnseleccionar<?php echo $idgui ?>').removeClass('hide'); }
          }else{          
            $('.btnseleccionar<?php echo $idgui ?>').addClass('hide');
          }        
        }
      }
      sysajax(data);
    }
    $('.btnsearch'+idgui).click(function(){
      buscaralumno<?php echo $idgui; ?>();
    });

    $('#idempresa<?php echo $idgui; ?>').change(function(ev){
        idempresa=$(this).val();
        var formData = new FormData();
        formData.append('idempresa', idempresa); 
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/bolsa_empresas/getproyecto',
          msjatencion:msjatencion,
          type:'json',
          callback:function(rs){
            var dt=rs.data;
            $('idproyecto<?php echo $idgui; ?>').val(dt);
          }
        }
      sysajax(data);

    })



    $('#fechanac<?php echo $idgui; ?>').datetimepicker({ //lang:'es',  //timepicker:false,
      format:'YYYY/MM/DD'
    });
    var inputFile = document.getElementById('fileFoto'+idgui);
    inputFile.addEventListener('change', mostrarImagen, false);
    function mostrarImagen(event) {
      var file=event.target.files[0];
      changefoto=true;
      var reader=new FileReader();
      reader.onload=function(event){
        var img=document.getElementById('foto'+idgui);
        img.src=event.target.result;
      }
      reader.readAsDataURL(file);
    };

    $('#frm<?php echo $idgui;?>').bind({
     submit: function(event){
        event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm<?php echo $idgui;?>');
        console.log(myForm);
        var formData = new FormData(myForm);
        if(changefoto==true) formData.append('fotouser', $('.input-file-invisible')[0].files[0]);
        formData.append('idlocal', 1);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/guardarDatos',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
            $('input.idpersona').val(data.newid);
            redir(location.href);
          }
        }
        sysajax(data);
        return false;
      }
    });


    $('.btnretornar<?php echo $idgui; ?>').click(function(){
      if($(this).closest('.modal').length)
        $(this).closest('.modal').modal('hide');
      else{
          var dreturn=$('#datareturn<?php echo $idgui ?>').val()||-1;
          if(dreturn ==-1)window.history.back();
          else $('#'+dreturn).trigger('click');
      }
    })
});
</script>