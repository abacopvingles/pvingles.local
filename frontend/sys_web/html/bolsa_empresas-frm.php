<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"eeeexzx-1";
if(!empty($this->datos)) $frm=$this->datos;
$logo=$this->documento->getUrlBase().(!empty($frm["logo"])?$frm["logo"]:'/static/media/nofoto.jpg');
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Bolsa_empresas'">&nbsp;<?php echo JrTexto::_('Bolsa_empresas'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <form method="post" id="frm<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="accion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="row">
            <div class="col-md-7 col-sm-12">
              <div class="col-md-12 form-group">
                  <label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
                  <input type="text"  id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Nombre de la empresa') ?>">
              </div>
              <div class="col-md-12 form-group">
                <label><?php echo JrTexto::_('Razon social');?> <span class="required"> (*) </span></label>              
                  <input type="text"  id="rason_social<?php echo $idgui; ?>" name="rason_social" required="required" class="form-control" value="<?php echo @$frm["rason_social"];?>"  placeholder="<?php echo JrTexto::_('Razon social de la empresa') ?>">
              </div>
              <div class="col-md-12 form-group">
                <label><?php echo JrTexto::_('Dirección');?> <span class="required"> (*) </span></label>              
                  <input type="text"  id="direccion<?php echo $idgui; ?>" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>"  placeholder="<?php echo JrTexto::_('Dirección de la empresa') ?>">
              </div>
            </div>
            <div class="col-md-5 col-sm-12">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center contool">
            <div><label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Logo')); ?></label></div>
            <div style="position: relative;" class="sufirimgcontent">
              <div class="toolbarmouse text-center" ><span class="btn btnremoveimage" style="position:absolute"><i class="fa fa-pencil"></i></span></div>
              <img src="<?php echo $logo; ?>" alt="logo" name="logo" class="changeimage img-responsive center-block thumbnail" id="logo<?php echo $idgui; ?>" style="max-width: 150px;  max-height: 150px;">
            </div>
          </div>
        </div>
              
            <div  class="col-md-6 col-sm-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Ruc');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="ruc<?php echo $idgui; ?>" name="ruc"  class="form-control" value="<?php echo @$frm["ruc"];?>" placeholder="<?php echo JrTexto::_('RUC Ej:12345678901') ?>">
              </div>
            </div>
            <div  class="col-md-6 col-sm-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Telefono');?> <span class="required"> (*) </span></label>              
                  <input type="text"  id="telefono<?php echo $idgui; ?>" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>" placeholder="<?php echo JrTexto::_('Telefono de la empresa Ej: 2014 215 1215') ?>">
              </div>
            </div>
            <div  class="col-md-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Representante');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="representante<?php echo $idgui; ?>" name="representante" class="form-control" value="<?php echo @$frm["representante"];?>" placeholder="<?php echo JrTexto::_('Representante de  la empresa Ej. Abel chingo') ?>">
              </div>
            </div>
            <div  class="col-md-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Correo');?> <span class="required"> (*) </span></label>              
                <input type="email" id="correo<?php echo $idgui; ?>" name="correo"  class="form-control" value="<?php echo @$frm["correo"];?>"    placeholder="<?php echo JrTexto::_('Correo de la empresa o representante Ej. soyusuario@miempresa.com') ?>">
              </div>
            </div>
            <input type="hidden" name="usuario" id="usuario" value="<?php echo @$frm["usuario"];?>">
            <input type="hidden" name="clave" id="clave" value="<?php echo @$frm["clave"];?>">
            <!--div  class="col-md-6 col-sm-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Usuario');?> <span class="required"> (*) </span></label>              
                  <input type="text"  id="usuario<?php echo $idgui; ?>" name="usuario" required="required" class="form-control" value="<?php echo @$frm["usuario"];?>"   placeholder="<?php echo JrTexto::_('Usuario o alias Ej: miempresa17') ?>">
                </div>
            </div>
            <div  class="col-md-6 col-sm-12">
              <div class="col-md-12  form-group">
                <label><?php echo JrTexto::_('Clave');?> <span class="required"> (*) </span></label>              
                <input type="password" autocomplete="false" id="clave<?php echo $idgui; ?>" <?php echo empty($frm["idempresa"])?'required="required"':''?> name="clave" class="form-control" placeholder="<?php echo JrTexto::_('clave de usuario Ej: miclave123') ?>"/> 
              </div>
            </div-->
            <div  class="col-md-12 col-sm-12">
              <div class="col-md-12 form-group text-center"><hr>
                  <button type="button" class="btn btn-warning btn-close" style="position:relative" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_empresas'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button> 
                  <button type="submit" class="btn btn-success"  style="position:relative"><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              </div><br>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var idgui='<?php echo !empty($idgui)?$idgui:now();?>';
  var fcall=<?php echo !empty($fcall)?($fcall):'';?>;
/*$('#frm-<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true);
      var res = xajax__('', 'bolsa_empresas', 'saveBolsa_empresas', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_empresas"))?>');
      }
     }
  });*/
  
  $('img.changeimage').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      var $tmpmedia=$(this)
      var file=document.createElement('input');
      file.id='file_'+Date.now();
      file.type='file';
      file.accept='image/x-png, image/gif, image/jpeg, image/*';
      file.name=$(this).attr('alt');
      file.className="input-file-invisible";
      file.addEventListener('change',function(ev){
          var rd = new FileReader();
          rd.onload = function(filetmp){
            var filelocal = filetmp.target.result;
            $tmpmedia.attr('src',filelocal);
          }
          rd.readAsDataURL(this.files[0]);
          var hayfile=$tmpmedia.parent().children('input[type="file"]');
          if(hayfile) hayfile.remove();
          $tmpmedia.parent().append(file);
      })
    file.click();
  })
  
  
  $('#frm<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm'+idgui); 
        var formData = new FormData(myForm); 
        //if(changefoto==true) formData.append('logo', $('.input-file-invisible')[0].files[0]);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/bolsa_empresas/guardarBolsa_empresas',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
            if(_isFunction(fcall)){
             fcall();
             redir(_sysUrlBase_+'/bolsa_empresas');
            }
          }
        }
        sysajax(data);
        return false;
      }
    });
});
</script>