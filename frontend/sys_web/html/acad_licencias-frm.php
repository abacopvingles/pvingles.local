<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Acad_licencias'">&nbsp;<?php echo JrTexto::_('Licencias'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="frm<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="idlicencia" id="idlicencia<?php echo $idgui;?>" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion"   id="accion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <input type="hidden"  id="costoxrol<?php echo $idgui;?>" name="costoxrol" value="<?php echo @$frm["costoxrol"];?>">
          <input type="hidden"  id="costoxdescarga<?php echo $idgui;?>" name="costoxdescarga" value="<?php echo @$frm["costoxdescarga"];?>">
          
          <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="col-xs-12 col-sm-6 col-md-6">
            <label><?php echo  ucfirst(JrTexto::_("Business"))?></label>
                <div class="form-group">
                	<div class="cajaselect">            
                    <select id="idempresa<?php echo $idgui; ?>" name="idempresa" name="rol" class="form-control" >  
                      <!--option value="0" ><?php echo  ucfirst(JrTexto::_("Select a business"))?></option-->
                      <?php 
                      if(!empty($this->empresas))
                        foreach($this->empresas as $emp){?>
                          <option value="<?php echo $emp["idempresa"]; ?>" ><?php echo  ucfirst($emp["nombre"]);?></option>
                        <?php }
                      ?>
                    </select>
                  </div>
                </div>
            </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12"><br></div>
            <div class="col-xs-12 col-sm-6 col-md-6 table-resonsive">
                <label><?php echo  ucfirst(JrTexto::_("Licencias por Rol"))?></label>
                <table  class="table table-resonsive"> 
                <tr><th><?php echo ucfirst(JrTexto::_("role"));?></th><th><?php echo ucfirst("N° de licencias");?></th><th><?php echo ucfirst("Costo unitario");?></th></tr>
                <?php 
                  if(!empty($frm["costoxrol"])){
                    $costoxrol=json_decode($frm["costoxrol"],true);
                  }
                  if(!empty($this->roles))
                    foreach($this->roles as $r){ if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10) continue;?>
                    <tr>
                      <td><?php echo ucfirst($r["rol"]);?></td>
                      <td><input type="number"  name="idrol<?php echo @$r["idrol"];?>" class="costoroles form-control" value="<?php echo @$costoxrol["idrol".$r["idrol"]];?>"></td>
                      <td><input type="number"  name="crol<?php echo @$r["idrol"];?>" class="costoroles form-control" value="<?php echo @$costoxrol["crol".$r["idrol"]];?>"></td>
                    </tr>
                  <?php }?>
                  </table>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 table-resonsive">
                <label><?php echo  ucfirst(JrTexto::_("Licencias por Fecha"))?></label>
                <table  class="table table-resonsive"> 
                <tr><th><?php echo ucfirst(JrTexto::_("Date Start"));?></th><th><?php echo ucfirst("Date Finish");?></th></tr>
                    <tr>
                      <td><input type="date" id="fechainicio<?php echo $idgui;?>" name="fecha_inicio" class="form-control" value="<?php echo @$frm["fecha_inicio"];?>"></td>
                      <td><input type="date" id="fechafinal<?php echo $idgui;?>"  name="fecha_final" class="form-control" value="<?php echo @$frm["fecha_final"];?>"></td>
                    </tr>
                 
                  </table>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 table-resonsive">
                <label><?php echo  ucfirst(JrTexto::_("Licencias por Download"))?></label>
                <table  class="table table-resonsive"> 
                  <tr><th><?php echo ucfirst(JrTexto::_("Document"));?></th><th><?php echo ucfirst("N° de licencias");?></th><th><?php echo ucfirst("Costo unitario");?></th></tr>
                  <tr>
                    <td><?php echo ucfirst(JrTexto::_("Workbooks "));?></td>
                    <?php 
                      if(!empty($frm["costoxdescarga"])){
                        $costoxdescarga=json_decode($frm["costoxdescarga"],true);
                      }
                    ?>
                    <td><input type="number"  name="nworkbook" class="download form-control" value="<?php echo @$costoxdescarga["nworkbook"];?>"></td>
                    <td><input type="number"  name="cworkbook" class="download form-control" value="<?php echo @$costoxdescarga["cworkbook"];?>"></td>
                  </tr>
                  <tr>
                    <td><?php echo ucfirst(JrTexto::_("M.O.P "));?></td>
                    <td><input type="number"  name="nmop" class="download form-control " value="<?php echo @$costoxdescarga["nmop"];?>"></td>
                    <td><input type="number"  name="cmop" class="download form-control" value="<?php echo @$costoxdescarga["cmop"];?>"></td>
                  </tr>
                  </table>
            </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12"><hr></div>
            <div class="col-md-12 text-center">
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('acad_licencias'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
              <button id="btn-saveAcad_licencias" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#frm-<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm-<?php echo $idgui;?>'); 
        var formData = new FormData(myForm);
        var downloadjson={}; 
        var costoroles={};
        $('input.download').each(function(i,v){
          downloadjson[$(v).attr('name')]=$(v).val()||0;
        })
        formData.append('costoxdescarga', JSON.stringify(downloadjson));
        $('input.costoroles').each(function(i,v){
          costoroles[$(v).attr('name')]=$(v).val()||0;
        })
        formData.append('costoxrol', JSON.stringify(costoroles));
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_licencias/guardarAcad_licencias',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
             redir(_sysUrlBase_+'/acad_licencias');
          }
        }
        sysajax(data);
        return false;
      }
    });  
  
    $('#frm-<?php echo $idgui;?>').on('change','#idempresa<?php echo $idgui;?>',function(event){
      event.preventDefault();
        var idgui='<?php echo $idgui; ?>';
        var formData = new FormData(); 
       formData.append('idempresa', $('#idempresa<?php echo $idgui;?>').val());
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_licencias/buscarjson',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : false,
          callback:function(data){
            if(data.length>0){
              var downloadjson={}; 
              var costoroles={};
              $('#fechainicio'+idgui).val(data.fecha_inicio);
              $('#fechafinal'+idgui).val(data.fecha_final);
              downloadjson=JSON.parse(data.costoxdescarga);              
              $('input.download').each(function(i,v){
                $(v).val(downloadjson[$(v).attr('name')]||0);
              })
              formData.append('costoxdescarga', JSON.stringify(downloadjson));
              costoroles=JSON.parse(data.costoxrol);
              $('input.costoroles').each(function(i,v){
                $(v).val(costoroles[$(v).attr('name')]||0);
              })
            }
          }
        }
        sysajax(data);
        return false;
    })
});
</script>