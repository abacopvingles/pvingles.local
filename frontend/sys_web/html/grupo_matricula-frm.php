<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Grupo_matricula'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdgrupo_matricula" id="pkidgrupo_matricula" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdalumno">
              <?php echo JrTexto::_('Idalumno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtidalumno" name="txtIdalumno" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidalumno))
                foreach ($this->fkidalumno as $fkidalumno) { ?><option value="<?php echo $fkidalumno["dni"]?>"><?php echo $fkidalumno["dni"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdgrupo">
              <?php echo JrTexto::_('Idgrupo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtidgrupo" name="txtIdgrupo" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidgrupo))
                foreach ($this->fkidgrupo as $fkidgrupo) { ?><option value="<?php echo $fkidgrupo["idgrupo"]?>"><?php echo $fkidgrupo["idgrupo"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechamatricula">
              <?php echo JrTexto::_('Fechamatricula');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechamatricula" name="txtFechamatricula" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechamatricula"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado" name="txtEstado" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegusuario">
              <?php echo JrTexto::_('Regusuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegusuario" name="txtRegusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegfecha">
              <?php echo JrTexto::_('Regfecha');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegfecha" name="txtRegfecha" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSubgrupos">
              <?php echo JrTexto::_('Subgrupos');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSubgrupos" name="txtSubgrupos" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["subgrupos"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveGrupo_matricula" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('grupo_matricula'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Grupo_matricula', 'saveGrupo_matricula', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Grupo_matricula"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Grupo_matricula"))?>');
        <?php endif;?>       }
     }
  });







  
  
});


</script>

