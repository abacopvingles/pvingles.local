<style type="text/css">
	#cursos_matriculados .elem-curso:hover {
		box-shadow: 0px 0px 8px 3px grey;
	}
</style>


<div class="row" id="cursos_matriculados">
    <h2 class="text-center"><?php echo JrTexto::_("Choose a course to see your academic progress tracking"); ?></h2>
	<?php foreach ($this->cursosMatric as $c) {
        //verifica si se ha especificado en el request la plantilla
		$url = (isset($_REQUEST["plt"])) ? $this->documento->getUrlBase().'/docente/panelcontrol/?idcurso='.$c["idcurso"]."&plt=".$_REQUEST["plt"] : $this->documento->getUrlBase().'/docente/panelcontrol/?idcurso='.$c["idcurso"];
	?>
	<div class="col-xs-3"><a href="<?php echo $url; ?>">
        <div class="thumbnail elem-curso">
            <img src="<?php echo $this->documento->getUrlBase().(@str_replace('__xRUTABASEx__', '', $c["imagen"])); ?>" class="img-responsive center-block" alt="cover">
            <div class="caption">
                <h3 class="text-center"><?php echo $c["nombre"]; ?></h3>
                <!--p class="text-center">
                    <a href="<?php echo $this->documento->getUrlBase().'/docente/´panelcontrol/?idcurso='.$c["idcurso"]; ?>" class="btn btn-info" title="<?php echo ucfirst(JrTexto::_("View")); ?>"><i class="fa fa-eye"></i> <span class=""><?php echo ucfirst(JrTexto::_("View")); ?></span></a> 
                </p-->
            </div>
        </div>
   	</a></div>
	<?php } ?>
</div>