<div class="row" id="breadcrumb"> 
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div>

<div class="row" id="calificaciones-listar">
	<input type="hidden" id="hIdDocente" value="<?php echo $this->usuarioAct["dni"]; ?>">
	<div class="row hidden" id="filtros">
		<div class="form-group col-xs-12 col-sm-4 select-ctrl-wrapper select-azul">
			<select name="" id="" class="form-control select-ctrl">
				<option value="0">- <?php echo JrTexto::_("Select"); ?> -</option>
			</select>
		</div>
	</div>

	<div class="row" id="resultado_busqueda">
		<?php if(!empty($this->cursosMatric)) {
		foreach ($this->cursosMatric as $m) { ?>
		<div class="col-sm-6 col-md-3"><div class="thumbnail">
			<img src="<?php echo $this->documento->getUrlBase().$m["imagen"]; ?>" alt="imagen" class="img-responsive" style="height:170px;">
			<div class="caption">
				<h3  style="max-height:52px; overflow:hidden;" title="<?php echo $m["nombre"]; ?>"><?php echo $m["nombre"]; ?></h3>
				<p><b><?php echo ucfirst(JrTexto::_("Teacher")); ?>:</b> <span><?php echo $m["docente_nombre"]; ?></span></p>
				<p class="text-center"><a href="<?php echo $this->documento->getUrlBase(); ?>/notas/notas_alumno/listar/?identificador=<?php echo $m["idgrupoauladetalle"]; ?>&iddocente=<?php echo $m["iddocente"]; ?>" class="btn btn-info" role="button"><?php echo ucfirst(JrTexto::_("view")); ?></a></p>
			</div>
		</div></div>
		<?php }} ?>
	</div>
</div>

<section id="mensajes_idioma" class="hidden">
	<input type="hidden" id="a_word_or_phrase" value="<?php echo ucfirst(JrTexto::_("A word or phrase")); ?>">
</section>