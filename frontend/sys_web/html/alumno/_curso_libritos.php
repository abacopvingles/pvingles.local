
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
<div class="row" id="curso-inicio">
    <input type="hidden" name="hIdCurso" id="hIdCurso" value="<?php echo $this->idCurso; ?>">
    <?php foreach ($this->sylabus as $tipo => $arrNiveles) { ?> 
    <div class="col-xs-12"> 
        <div class="slider" id="slider-<?php echo $tipo; ?>">
            <?php if(!empty($arrNiveles)){ 
            foreach ($arrNiveles as $i=>$sy) { $t = $sy['tiporecurso'];
            $isLocked = ($i==0 || $this->tieneRolAdmin)? false : $isNextLocked;
            $tipo_elem='';
            if($t=='N' || $t=='U' || $t=='L'){
                $tipo_elem='t_nivel';
                $idCursoDet_idRecurso = $sy['idcursodetalle'];
            } else if($t=='E'){
                $tipo_elem='t_examen';
                $idCursoDet_idRecurso = $sy['idrecurso'];
            } ?>
            <div class="col-xs-12 col-md-6">
                <div class="card-nivel <?php echo (@$isLocked)?'locked':''.' '.$tipo_elem; ?> thumbnail color-black" title="<?php echo $sy['nombre']; ?>" data-idcursodetallerecurso="<?php echo (@$isLocked)?'-1':$idCursoDet_idRecurso; ?>">
                    <div class="col-xs-12 padding-0 image">
                        <img src="<?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $sy['imagen']) ; ?>" alt="course_cover" class="">
                        <!--div class="text-center botones">
                            <a class="btn"><i class="fa <?php echo (@$isLocked)?'fa-lock':'fa-play-circle-o'; ?> fa-4x color-white"></i></a>
                        </div-->
                    </div>

                    <div class="col-xs-12 col-md-12 padding-0 barra-progreso">
                        <div class="col-xs-offset-4 col-xs-8 padding-0">
                            <div class="progress">
                                <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>%;">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2 sr-only">
                            <span class="porcentaje"><?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>%</span>
                        </div>
                    </div>

                    <div class="col-xs-12 padding-0">
                        <div class="col-xs-12 col-md-12 padding-0 nombre">
                            <h5 class="col-xs-12 text-center padding-0"><?php echo $sy['orden'].'. '.$sy['nombre']; ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                if($t=='N' || $t=='U' || $t=='L'){
                    $isNextLocked = ($sy['progreso']<100);
                } else if($t=='E'){
                    $isNextLocked = empty(@$sy['resultado']);
                }
            }/*end foreach*/ }/*end if*/ ?>
        </div> 
    </div>
    <?php } ?>
</div>

<section class="hidden">
    <div class="col-xs-12 col-md-6" id="card-to-clone">
            <div class="card-nivel thumbnail color-black" title="---" data-idcursodetallerecurso="-1">
                <div class="col-xs-12 padding-0 image">
                    <img src="#" alt="course_cover" class="">
                    <!--div class="text-center botones">
                        <a class="btn"><i class="fa fa-4x color-white"></i></a>
                    </div-->
                </div>

                <div class="col-xs-12 col-md-12 padding-0 barra-progreso">
                    <div class="col-xs-offset-4 col-xs-8 padding-0">
                        <div class="progress">
                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 sr-only">
                        <span class="porcentaje">0%</span>
                    </div>
                </div>

                <div class="col-xs-12 padding-0 info-texto">
                    <div class="col-xs-12 col-md-12 padding-0 nombre">
                        <h5 class="col-xs-12 text-center padding-0">---</h5>
                    </div>
                </div>
            </div>
        </div>
</section>

<script type="text/javascript">
var esRolAdmin = <?php echo ($this->tieneRolAdmin)?'true':'false'; ?>;

var iniciarSlider = function(elemSlider, slideInicio=0){
    if(!elemSlider){ return false; }
    var cantSlides = 6;
    if( $(elemSlider).find('.card-nivel').length<=cantSlides ){ slideInicio = 0; }
    var sliderOpts = {
        /*centerMode: true,*/
        focusOnSelect: true,
        centerPadding: '0px',
        slidesToShow: cantSlides,
        initialSlide: slideInicio,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    /*centerMode: true,
                    arrows: false,*/
                    initialSlide: slideInicio,
                    centerPadding: '20px',
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 880,
                settings: {
                    /*centerMode: true,
                    arrows: false,*/
                    initialSlide: slideInicio,
                    centerPadding: '20px',
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 720,
                settings: {
                    /*centerMode: true,
                    arrows: false,*/
                    initialSlide: slideInicio,
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    /*centerMode: true,
                    arrows: false,*/
                    initialSlide: slideInicio,
                    centerPadding: '20px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    /*centerMode: true,
                    arrows: false,*/
                    initialSlide: slideInicio,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]
    };

    $(elemSlider).slick(sliderOpts);
};

var rellenarSgteSlider = function(){
    var idpadre = -1;
    var $slider = null;
    $('#curso-inicio .slider').each(function(index, el) {
        if($(el).find('.card-nivel').length>0){
            idpadre = $(el).find('.slick-active.slick-current .card-nivel').attr('data-idcursodetallerecurso');
            $slider = $(el);
        }
    });
    var indice =  $('#curso-inicio .slider').index($slider);
    $slider = $('#curso-inicio .slider').eq(indice+1); //obtener el sgte .slider (vacío).
    if($slider.length!=0){ getNivelesHijos(idpadre, $slider); }
};

var getNivelesHijos = function(idPadre, $contenedor) {
    if(!idPadre){ return false; }
    var idCurso = $('#hIdCurso').val();
    $.ajax({
        url: _sysUrlBase_+'/curso/getNivelesxIdpadre',
        type: 'GET',
        dataType: 'json',
        data: {idpadre:idPadre, idcurso:idCurso},
        beforeSend: function () {
            if($contenedor.hasClass('slick-initialized')){ $contenedor.slick('unslick'); }
            $contenedor.html('<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i> <?php echo JrTexto::_("Loading"); ?>...</div>');
        },
    }).done(function(resp) {
        if(resp.code=="ok"){
            if(resp.redir){ redir(resp.redir); }
            $contenedor.html('');
            var isNextLocked=null;
            var num = 0;
            var indiceInicio = 0;
            $.each(resp.data, function(i, val) {
                var tipo = val.tiporecurso;
                var isLocked = (i==0 || esRolAdmin)? false : isNextLocked;
                var clsCard = 'locked';
                var tipoCard = '';
                var idCursoDet_idRecurso = -1;
                var clsIconBoton = 'fa-lock';
                if(tipo=='N' || tipo=='U' || tipo=='L'){
                    ++num;
                    var nombre = num+'. '+val.nombre;
                    tipoCard = 't_nivel';
                }else if(tipo=='E'){
                    var nombre = val.nombre;
                    tipoCard = 't_examen';
                }
                if(!isLocked){
                    clsCard= '';
                    idCursoDet_idRecurso = (tipo=='E')?val.idrecurso:val.idcursodetalle;
                    clsIconBoton= 'fa-play-circle-o';
                    indiceInicio=i;
                }
                    
                var $cloned = $('#card-to-clone').clone();
                $cloned.removeAttr('id');
                $cloned.find('.card-nivel').addClass(clsCard+' '+tipoCard).attr({ "data-idcursodetallerecurso": idCursoDet_idRecurso, "title": val.nombre });
                $cloned.find('.image img').attr('src', val.imagen.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
                $cloned.find('.botones a i').addClass(clsIconBoton);
                $cloned.find('.barra-progreso .progress-bar').attr('aria-valuenow', val.progreso).css('width', val.progreso+'%');
                $cloned.find('.barra-progreso .porcentaje').text(val.progreso+'%');
                $cloned.find('.info-texto h4,.info-texto h5').text(nombre);

                $contenedor.append($cloned);

                if(tipo=='N' || tipo=='U' || tipo=='L'){
                    isNextLocked = (val.progreso<100);
                }else if(tipo=='E'){
                    isNextLocked = $.isEmptyObject(val.resultado);
                }
            });
            iniciarSlider($contenedor, indiceInicio);
            rellenarSgteSlider();
        }
    }).fail(function(err) {
        console.log("!error: ", err);
    });  
};

$(document).ready(function() {
    /*================= Al iniciar =================*/
    var $ultimoActivo = $('#curso-inicio .card-nivel').not('.locked').last();
    var indexOfActivo = 0;
    if(!esRolAdmin){ indexOfActivo = $('#curso-inicio .card-nivel').index($ultimoActivo); }
    iniciarSlider($ultimoActivo.closest('.slider'), indexOfActivo);

    rellenarSgteSlider();

    /*================== Eventos ===================*/
    $('.slider').on('click', '.card-nivel', function(e) {
        if($(this).hasClass('locked')){ return false; }
        var id = $(this).attr('data-idcursodetallerecurso');
        if($(this).hasClass('t_nivel')){
            var $slider = $(this).closest('.slider');
            var indice =  $('#curso-inicio .slider').index($slider);
            $slider = $('#curso-inicio .slider').eq(indice+1); //obtener el sgte .slider (vacío).
            getNivelesHijos(id, $slider);
        }else if($(this).hasClass('t_examen')){
            redir(_sysUrlBase_+'/examenes/resolver/?idexamen='+id);
        }
    });
});
</script>