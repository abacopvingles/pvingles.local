<style type="text/css">
    @media (min-width: 1250px) and (max-height: 800px){ 
        .widget-main{
            margin: 1px;
            padding: 1px;
        }
        .btn-container{
            padding: 1ex;
        }
        .btn-rectangle{
            min-height: 77px;
        }
    }
</style>
<div class="container">
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-3">
                <div class="widget-box">
                    <div class="widget-body widget-none-header widget-corner">
                        <div class="widget-main">
                            <div class="test-advice">
                                <h2 class="test-header"><?php echo JrTexto::_('test day');?></h2>
                                <picture>
                                    <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/teacher.png" class="img-responsive" alt="Teacher">
                                    <!--
                                    <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/reloj.png" class="img-responsive col-xs-6" alt="Clock">
                                    -->
                                </picture>
                                <div class="test-body">
                                    <h4 class="test-title">Riddles</h4>
                                    <p class="test-descrip">
                                        In this section you guess which object the teacher describes in class...
                                    </p>
                                </div>
                                <div class="test-button">
                                    <a href="#">
                                        <i class="fa fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <div class="col-xs-12 col-sm-6 col-lg-5">
                <div class="widget-box">
                    <div class="widget-header bg-green">
                        <h4 class="widget-title">
                            <i class="fa fa-pencil-square-o"></i>
                            <span>
                                A1 - 
                                <?php echo JrTexto::_('unit'); ?> 2: 
                                At the doctor
                            </span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 widget-banner">
                                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/children.png" class="img-responsive" alt="Children">
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 levels-btns">

                                <?php if(!empty($this->niveles)){
                                    $cnivel=count($this->niveles);
                                    $inivel=0;
                                    foreach ($this->niveles as $nivel){ $inivel++; ?>
                                        <div class="col-xs-3 col-sm-3 col-md-<?php echo ($cnivel==$inivel&&$inivel%2!=0)?'12':'6'; ?>">
                                        <a href="<?php echo $this->documento->getUrlSitio().'/recursos/listar/'.$nivel["idnivel"]; ?>" class="btn btn-circle btn-lilac"><?php echo $nivel["nombre"]; ?></a>
                                        </div> 
                                 <?php }} ?>
                            </div>
                        </div>
                        <div class="widget-main">
                            <div class="row">
                                <p class="col-xs-12">
                                    Breaking the Ice; Background; Achievement; News; Virtual World; On the Move; Planning; Predictions;...
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-blue col-xs-12 capitalize"><?php echo JrTexto::_('continue'); ?></a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-blue col-xs-12 capitalize"><?php echo JrTexto::_('my notes'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="widget-box">
                    <div class="widget-header bg-red">
                        <h4 class="widget-title">
                            <i class="fa fa-star"></i>
                            <span><?php echo JrTexto::_('my progress'); ?></span>
                        </h4>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="capitalize"><?php echo JrTexto::_('listening'); ?></span>
                                            </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="capitalize"><?php echo JrTexto::_('writing'); ?></span>
                                            </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                                <span class="capitalize"><?php echo JrTexto::_('reading'); ?></span>
                                            </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="capitalize"><?php echo JrTexto::_('speaking');?></span>
                                            </div>
                                        </div>
                                    </div>                                                   

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6" style="padding: 0px 5px">
                                            <a href="#" class="btn btn-blue btn-square">
                                                <div class="btn-label"><?php echo JrTexto::_('advance<br>sessions');?></div>
                                                <span class="btn-information">45</span>
                                                <i class="btn-icon fa fa-play-circle-o"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6" style="padding: 0px 5px">
                                            <a href="#" class="btn btn-green btn-square">
                                                <span class="btn-label"><?php echo JrTexto::_('time<br>sessions');?></span>
                                                <span class="btn-information">04:15</span>
                                                <i class="btn-icon glyphicon glyphicon-time"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="widget-box">
                    <div class="widget-body" style="min-height: auto !important; height: auto !important;">
                        <div class="widget-main">
                            <div class="row">
                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="#" class="btn btn-blue btn-rectangle slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/diccionario" data-ventana="vocabulary" >
                                        <span class="btn-label"><?php echo JrTexto::_('Vocabulary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-file-text-o"></i>
                                    </a>
                                </div>
                           

                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="<?php echo $this->documento->getUrlBase() ?>/actividad/listado/1/6/18" class="btn btn-yellow btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('Activities');?></span>
                                        <span class="btn-information">5</span>
                                        <i class="btn-icon fa fa-pencil"></i>
                                    </a>
                                </div>

                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="<?php echo $this->documento->getUrlBase() ?>/examenes/" class="btn btn-lilac btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('Assessment');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-comments"></i>
                                    </a>
                                </div>

                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="#" class="btn btn-green btn-rectangle  slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/diccionario" data-ventana="vocabulary">
                                        <span class="btn-label"><?php echo JrTexto::_('Dictionary');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-book"></i>
                                    </a>
                                </div>

                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="#" class="btn btn-green2 btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('Workbook');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-pencil-square"></i>
                                    </a>
                                </div>

                                <div class="hvr-shrink col-md-4 col-xs-4 btn-container">
                                    <a href="#" class="btn  btn-red btn-rectangle">
                                        <span class="btn-label"><?php echo JrTexto::_('Interest<br>links');?></span>
                                        <span class="btn-information"></span>
                                        <i class="btn-icon fa fa-at"></i>
                                    </a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).on('ready', function(){
    <?php if(!empty($this->usuarioAct["mostrar_guia"])){?>
    loadModal();
    <?php }?> 
    $('header').show('fast').addClass('static');
});
</script>