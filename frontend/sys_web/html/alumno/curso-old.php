<style type="text/css">
#curso-inicio .slick-slide {
    transition: all 300ms ease;
    opacity: 0.8 !important;
 }
</style>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>
<div class="row" id="curso-inicio">
    <input type="hidden" name="hIdCurso" id="hIdCurso" value="<?php echo $this->idCurso; ?>">
    <div class="col-xs-12 hidden" id="unidad_activa_sesiones">
    	<div class="col-xs-12 col-sm-6 col-md-3" id="unidad_activa">
    		<div class="card-nivel thumbnail color-black" title="" data-idcursodetallerecurso="">
    			<div class="col-xs-12 padding-0 image">
    				<img src="" alt="course_cover" class="center-block">
                    <!--div class="text-center botones">
                        <a class="btn"><i class="fa <?php //echo (@$isLocked)?'fa-lock':'fa-play-circle-o'; ?> fa-4x color-white"></i></a>
                    </div-->
                </div>

                <div class="col-xs-12 col-md-12 barra-progreso">
                	<div class="col-xs-12">
                		<div class="progress">
                			<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                			</div>
                		</div>
                	</div>
                	<div class="sr-only">
                		<div class="porcentaje">0%</div>
                	</div>
                </div>

                <div class="col-xs-12 nombre-contenedor">
                	<div class="col-xs-12 col-md-12  nombre">
                		<h5 class="col-xs-12 text-center">---</h5>
                	</div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-9" id="sesiones_activas">
        	<div class="panel border-red" id="pnl-cursos">
        		<div class="panel-heading bg-red">
        			<h3 class="panel-titulo">
        				<i class="fa fa-bookmark"></i>&nbsp;&nbsp;<?php echo JrTexto::_("Activities"); ?></h3>
        				<span href="#" class="pull-right cerrar" title="<?php echo JrTexto::_("Close"); ?>"><i class="fa fa-times fa-2x"></i></span>
        		</div>
        		<div class="panel-body">
        			<div class="slider" id="slider-sesion"> </div>
    			</div>
    		</div>
    	</div>
    </div>

    <?php $x=0; 
     $idrol=(int)$this->usuarioAct["idrol"];
    foreach ($this->sylabus as $tipo => $arrNiveles) { $x++; ?> 
    <div class="col-xs-12"> 
    	<h3 class="col-xs-12 bg-red titulo-tipo-nivel <?php echo $x==1?'':'hidden'; ?>"><?php 
	    	$titulo=""; 
	    	if($tipo=='nivel'){ $titulo=JrTexto::_("Levels"); }else if($tipo=='unidad'){ $titulo=JrTexto::_("Units"); }else if($tipo=='sesion'){ $titulo=JrTexto::_("Activities"); }
	    	echo $titulo;
    	?></h3>
        <div class="slider" id="slider-<?php echo $tipo; ?>">
            <?php if(!empty($arrNiveles)){ 
            foreach ($arrNiveles as $i=>$sy) { $t = $sy['tiporecurso'];

            $isLocked = $idrol==3?((isset($sy['bloqueado']))? $sy['bloqueado'] : $isNextLocked):false;
            $tipo_elem='';
            if($t=='N' || $t=='U' || $t=='L'){
                $tipo_elem='t_nivel';
                $idCursoDet_idRecurso = $sy['idcursodetalle'];
            } else if($t=='E'){
                $tipo_elem='t_examen';
                $idCursoDet_idRecurso = $sy['idrecurso'];
            } ?>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="card-nivel <?php echo (@$isLocked)?'locked':''.' '.$tipo_elem; ?> thumbnail color-black" title="<?php echo $sy['nombre']; ?>" data-idcursodetallerecurso="<?php echo (@$isLocked)?'-1':$idCursoDet_idRecurso; ?>">
                    <div class="col-xs-12 padding-0 image">
                        <img src="<?php echo !empty(@$sy['imagen'])?str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $sy['imagen']):$this->documento->getUrlStatic().'/media/imagenes/level_default.png'; ?>" alt="course_cover" class="center-block">
                        <!--div class="text-center botones">
                            <a class="btn"><i class="fa <?php echo (@$isLocked)?'fa-lock':'fa-play-circle-o'; ?> fa-4x color-white"></i></a>
                        </div-->
                    </div>

                    <div class="col-xs-12 col-md-12 barra-progreso">
                        <div class="col-xs-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>%;">
                                </div>
                            </div>
                        </div>
                        <div class="sr-only">
                            <div class="porcentaje"><?php echo !empty(@$sy['progreso'])?@$sy['progreso']:'0'; ?>%</div>
                        </div>
                    </div>

                    <div class="col-xs-12 nombre-contenedor">
                        <div class="col-xs-12 col-md-12  nombre">
                            <h5 class="col-xs-12 text-center"><?php echo $sy['orden'].'. '.$sy['nombre']; ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                if($t=='N' || $t=='U' || $t=='L'){
                    $isNextLocked = ($sy['progreso']<100);
                } else if($t=='E'){
                    $isNextLocked = empty(@$sy['resultado']);
                }
            }/*end foreach*/ }/*end if*/ ?>
        </div> 
    </div>
    <?php } ?>
</div>

<section class="hidden">
    <div class="col-xs-12 col-sm-6 col-md-3" id="card-to-clone">
        <div class="card-nivel thumbnail color-black" title="" data-idcursodetallerecurso="">
            <div class="col-xs-12 padding-0 image">
                <img src="" alt="course_cover" class="center-block">
            </div>

            <div class="col-xs-12 col-md-12 barra-progreso">
                <div class="col-xs-12">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                        </div>
                    </div>
                </div>
                <div class="sr-only">
                    <div class="porcentaje">0%</div>
                </div>
            </div>

            <div class="col-xs-12 nombre-contenedor">
                <div class="col-xs-12 col-md-12  nombre">
                    <h5 class="col-xs-12 text-center"></h5>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="mdl_examen_ubicacion" class="hidden">
    <input type="hidden" name="hIdExam_Ubicacion" id="hIdExam_Ubicacion" value="<?php echo @$this->examen_ubicacion['idexamen_ubicacion']; ?>">
    <h4 class="text-center"><?php echo JrTexto::_("You can take a placement test to move quickly to more advanced levels"); ?>.</h4>
    <div class="col-xs-offset-1 col-xs-10 col-sm-offset-3 col-sm-6 text-center">
        <a href="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.@$this->examen_ubicacion['idrecurso'] ?>" class="thumbnail">
            <img src="<?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(),@$this->examen_ubicacion['imagen']); ?>" alt="quiz_cover" class="img-responsive">
        </a>
        <a href="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.@$this->examen_ubicacion['idrecurso']; ?>" class="btn btn-lg btn-primary"><?php echo JrTexto::_("Take quiz"); ?></a>
    </div>
</section>

<section class="hidden">
    <textarea name="txtResultExamUbic" id="txtResultExamUbic"><?php echo !empty(@$this->examen_ubicacion['resultado'])?@json_encode(@$this->examen_ubicacion['resultado']):''; ?></textarea>
</section>

<script type="text/javascript">
const MINIMO_PARA_PASAR = 52;
var esRolAdmin = <?php echo ($this->tieneRolAdmin)?'true':'false'; ?>;
var tieneExamUbicacion = <?php echo !empty($this->examen_ubicacion)?'true':'false'; ?>;
var tieneExamUbicacion_resultado = <?php echo !empty(@$this->examen_ubicacion['resultado'])?'true':'false'; ?>;
var idrol=parseInt(<?php echo $this->usuarioAct["idrol"];?>);
var iniciarSlider = function(elemSlider, slideInicio=0){
    if(!elemSlider){ return false; }
    var cantSlides = 4;
    if( $(elemSlider).find('.card-nivel').length<=cantSlides ){ slideInicio = 0; }
    var sliderOpts = {
        /*centerMode: true,
        initialSlide: slideInicio,*/
        infinite: false,
        focusOnSelect: true,
        centerPadding: '0px',
        slidesToShow: cantSlides,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 880,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 720,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]
    };

    $(elemSlider).slick(sliderOpts);
};

var rellenarSgteSlider = function(){
    var idpadre = -1;
    var $slider = null;
    $('#curso-inicio .slider').each(function(index, el) {
        if($(el).find('.card-nivel').length>0){
            idpadre = $(el).find('.slick-active.slick-current .card-nivel').attr('data-idcursodetallerecurso');
            $slider = $(el);
        }
    });
    var indice =  $('#curso-inicio .slider').index($slider);
    $slider = $('#curso-inicio .slider').eq(indice+1); //obtener el sgte .slider (vacío).
    if($slider.length!=0){ getNivelesHijos(idpadre, $slider); }
};

var getNivelesHijos = function(idPadre, $contenedor) {
    if(!idPadre){ return false; }
    var idCurso = $('#hIdCurso').val();
    var esSesion = ($contenedor.attr('id')=="slider-sesion");
    $.ajax({
        url: _sysUrlBase_+'/curso/getNivelesxIdpadre',
        type: 'GET',
        dataType: 'json',
        data: {idpadre:idPadre, idcurso:idCurso},
        beforeSend: function () {
            if($contenedor.hasClass('slick-initialized')){ $contenedor.slick('unslick'); }
            if(esSesion) {
                $contenedor.html('<div class="text-center"><i class="fa fa-cog fa-spin fa-3x fa-fw"></i> <?php echo JrTexto::_("Loading"); ?>...</div>');
            }
        },
    }).done(function(resp) {
        if(resp.code=="ok"){
            if(resp.redir){ redir(resp.redir); }
            $contenedor.html('');
           /* var isNextLocked=null;*/
            var num = 0;
            var indiceInicio = 0;
            $.each(resp.data, function(i, val) {

                var tipo = val.tiporecurso;
               
                var isLocked = idrol==3?((typeof val.bloqueado != "undefined")? val.bloqueado : true):false;
                var clsCard = 'locked';
                var tipoCard = '';
                var idCursoDet_idRecurso = -1;
                var clsIconBoton = 'fa-lock';
                if(tipo=='N' || tipo=='U' || tipo=='L'){
                    ++num;
                    var nombre = num+'. '+val.nombre;
                    tipoCard = 't_nivel';
                }else if(tipo=='E'){
                    var nombre = val.nombre;
                    tipoCard = 't_examen';
                }
                if(!isLocked){
                    clsCard= '';
                    idCursoDet_idRecurso = (tipo=='E')?val.idrecurso:val.idcursodetalle;
                    clsIconBoton= 'fa-play-circle-o';
                    indiceInicio=i;
                }
                    
                var $cloned = $('#card-to-clone').clone();
                var porcent_progreso = val.progreso || 0.0;
                $cloned.removeAttr('id');
                $cloned.find('.card-nivel').addClass(clsCard+' '+tipoCard).attr({ "data-idcursodetallerecurso": idCursoDet_idRecurso, "title": val.nombre });
                if(tipoCard=='t_examen') {
                    var tipoExamen = val.tipo_examen;
                    $cloned.find('.card-nivel').attr('data-tipoexamen', tipoExamen);
                }

                if(val.imagen==null) {
                    var srcImagen = _sysUrlStatic_+'/media/imagenes/level_default.png';
                } else {
                    var srcImagen = val.imagen.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
                }
                $cloned.find('.image img').attr('src', srcImagen);
                $cloned.find('.botones a i').addClass(clsIconBoton);
                $cloned.find('.barra-progreso .progress-bar').attr('aria-valuenow', porcent_progreso).css('width', porcent_progreso+'%');
                $cloned.find('.barra-progreso .porcentaje').text(porcent_progreso+'%');
                $cloned.find('.nombre h4,.nombre h5').text(nombre);
                if(esSesion) {
                    $cloned.find('.nombre-contenedor').addClass('hidden');
                    $cloned.find('.barra-progreso .sr-only').removeClass('sr-only');
                }

                $contenedor.append($cloned);
                $contenedor.siblings('.titulo-tipo-nivel').removeClass('hidden');

                /*console.log('tipo =', tipo);
                console.log('val.progreso =', val.progreso);
                if(tipo=='N' || tipo=='U' || tipo=='L'){
                    isNextLocked = (val.progreso<MINIMO_PARA_PASAR);
                }else if(tipo=='E'){
                    isNextLocked = $.isEmptyObject(val.resultado);
                }
                console.log('isNextLocked =', isNextLocked);*/
            });

            if(esSesion) {
                $contenedor.find('.card-nivel').each(function(i, el) { $(el).addClass('hvr-sink'); });
                iniciarSlider($contenedor, indiceInicio);

            }
            rellenarSgteSlider();
        }
    }).fail(function(err) {
        console.log("!error: ", err);
    });  
};

var mostrarPnlSesionActiva = function($cardNivel){
    if(!$cardNivel){ return false; }
    $cardNivel.addClass('card-selected');
    $cardNivel.parent().addClass('hidden');
    $cardNivel.closest('.slider').addClass('desabilitado');

    $('#unidad_activa_sesiones').removeClass('hidden');
    var $card_activada = $('#unidad_activa_sesiones #unidad_activa .card-nivel');
    var idCursoDetRecurso = $cardNivel.attr('data-idcursodetallerecurso');
    var nombre = $cardNivel.find('.nombre h5').text();
    var imgSrc = $cardNivel.find('.image img').attr('src');
    var progreso = $cardNivel.find('.barra-progreso .progress-bar').attr('aria-valuenow');

    $card_activada.attr('title', nombre);
    $card_activada.attr('data-idcursodetallerecurso', idCursoDetRecurso);
    $card_activada.find('.image img').attr('src', imgSrc);
    $card_activada.find('.nombre h5').text(nombre);
    $card_activada.find('.barra-progreso .progress-bar')
        .css('width', progreso+'%')
        .attr('aria-valuenow', progreso);
    $card_activada.find('.barra-progreso .sr-only .porcentaje').text(progreso+'%');
};

var examenUbicacion = function() {
    if(!tieneExamUbicacion || tieneExamUbicacion_resultado) { return false; }
    $modal = sysmodal({
        html:'#mdl_examen_ubicacion',
        tam: 'md',
        borrar: true,
        titulo: '<?php echo JrTexto::_("Placement quiz"); ?>',
        ventanaid: 'modal_placement_quiz',
        cerrarconesc: false,
        showfooter: true
    });
    $modal.find('.modal-header').addClass('bg-primary');
};

$(document).ready(function() {
    /*================= Al iniciar =================*/
    var $ultimoActivo = $('#curso-inicio .card-nivel').not('.locked').last();
    var indexOfActivo = 0;
    if(!esRolAdmin){ indexOfActivo = $('#curso-inicio .card-nivel').index($ultimoActivo); }
    //iniciarSlider($ultimoActivo.closest('.slider'), indexOfActivo);

    rellenarSgteSlider();
    examenUbicacion();

    /*================== Eventos ===================*/
    $('.slider').on('click', '.card-nivel', function(e) {
        if($(this).hasClass('locked')){ return false; }
        var id = $(this).attr('data-idcursodetallerecurso');
        if(parseInt(id)<=0){ return false; }
        
        if($(this).hasClass('t_nivel')){
            var $slider = $(this).closest('.slider');
            if($slider.attr('id')=="slider-unidad") {
                $slider = $('#unidad_activa_sesiones #sesiones_activas').find('.slider');
                mostrarPnlSesionActiva($(this));
            }else{
                var indice =  $('#curso-inicio .slider').index($slider);
                $slider = $('#curso-inicio .slider').eq(indice+1); //obtener el sgte .slider (vacío).
            }
            getNivelesHijos(id, $slider);
        }else if($(this).hasClass('t_examen')){
            var tipoExamen = $(this).attr('data-tipoexamen')
            redir(_sysUrlBase_+'/examenes/resolver/?idexamen='+id+'&tipoexamen='+tipoExamen);
        }
    });

    $('#slider-sesion').on('mouseenter', '.card-nivel', function(e) {
        /*var name = $(this).attr('title');*/
        var name = $(this).find('.nombre').text().trim();
        $(this).parent().prepend('<div class="col-xs-12 text-center bolder name-on-hover">'+ name +'</div>');
    }).on('mouseleave', '.card-nivel', function(e) {
        var name = $(this).attr('title');
        $(this).parent().find('.name-on-hover').remove();
    });

    $('#unidad_activa_sesiones .cerrar').click(function(e) {
        e.preventDefault();
        $(this).closest('#unidad_activa_sesiones').addClass('hidden');
        /* Aparecer de nuevo la unidad en su lugar */
        
        $sliderDesabilitado = $('.desabilitado');
        $sliderDesabilitado.find('.card-selected').parent().removeClass('hidden');
        $sliderDesabilitado.find('.card-selected').removeClass('card-selected');
        $sliderDesabilitado.removeClass('desabilitado');
    });

    var contLlamado = 0;
    $('body').on('hide.bs.modal', '.modal.modal_placement_quiz', function(e) {
        if(contLlamado>0) { contLlamado=0; return false;  }
        var $modal = $(this);
        if( !$modal.hasClass('confirm_close') ) {
            e.preventDefault(); /* previene cierre del .modal */
            $.confirm({
                title: '<?php echo JrTexto::_("Skip") ?>',
                content: '<?php echo JrTexto::_("If you close this window, you will not be able to retake the placement quiz anymore"); ?>. <br> <b><?php echo JrTexto::_("Are you sure you want to skip the placement quiz?") ?></b>',
                confirmButtonClass: 'btn-danger',
                cancelButtonClass: 'btn-primary',
                confirmButton: '<?php echo JrTexto::_("Yes, skip") ?>',
                cancelButton: '<?php echo JrTexto::_("I will take quiz") ?>',
                confirm: function(){
                    $.ajax({
                        url: _sysUrlBase_+'/examen_ubicacion_alumno/xGuardar',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'txtIdexamen' : $modal.find('#hIdExam_Ubicacion').val(),
                            'txtEstado' : 'O',
                            'txtResultado' : $('#txtResultExamUbic').val(),
                        },
                    }).done(function(resp) {
                        if(resp.code=='ok') {
                            /*console.log(resp.msj, ' . NewID : ', resp.newid);*/
                        } else {
                            mostrar_notificacion('Error', resp.msj, 'error');
                        }
                    }).fail(function(err) {
                        console.log("error", err);
                    }).always(function() {
                        $modal.addClass('confirm_close');
                        $modal.modal('hide');
                    });
                },
            });
        }
        ++contLlamado;
    });
});
</script>