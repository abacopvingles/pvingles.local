<?php $usuarioAct = NegSesion::getUsuario(); ?>
<style type="text/css">
.slick-slide{
        position: relative;
    }
  /* sagregar css noavance*/  
</style>
<div class="main-container"> 
<div class="page-content">
    <div class="row" id="todas-unidades"> 
    <div class="col-xs-12">
        <div class="row" id="levels">
            <h3 class="col-xs-12 level-title"><?php echo JrTexto::_('level'); ?> <span id="namelevel"></span></h3>
            <div class="col-xs-12 level-wrapper">
                <div class="level-list">
                   <?php if(!empty($this->niveles))
                     foreach ($this->niveles as $nivel){?>
                    <div class="item-tools rname"  data-iname="namelevel" title="<?php echo $nivel["nombre"]?>" data-idnivel="<?php echo $nivel["idnivel"]; ?>" >
                      <span>
                        <a href="javascript:void(0) " class="hvr-float level-item <?php echo $nivel["idnivel"]==$this->idnivel?'active':'';?>" >
                           <div class="level-name"   ><?php echo $nivel["nombre"]; ?></div>
                        </a>                        
                      </span>
                    </div>
                     <?php } ?>
                </div>
            </div>
        </div>
        <div class="row" id="units">
            <h3 class="col-xs-12 unit-title"><?php echo JrTexto::_('Unit'); ?><span id="nameunit"></span></h3>
            <div class="col-xs-12 unit-wrapper">
                <div class="unit-list">
                    <?php 
                    $i=0;
                    if(!empty($this->unidades))
                     foreach ($this->unidades as $unidad){ $i++;?>
                    <div class="item-tools rname" data-iname="nameunit" title="<?php echo $unidad["nombre"]; ?>" data-idnivel="<?php echo $unidad["idnivel"]; ?>">
                      <span>
                        <a href="javascript:void(0)" class="hvr-float unit-item <?php echo $unidad["idnivel"]==$this->idunidad?'active':'';?> ">
                            <div class="unit-name"><?php echo JrTexto::_('Unit').''; ?></div>
                            <div class="unit-name"><?php echo str_pad($i,2,"0",STR_PAD_LEFT); ?></div>
                        </a>                        
                      </span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row" id="lessons">
            <h3 class="col-xs-12 lesson-title"><?php echo ucfirst(JrTexto::_('Activity')); ?><span id="namelesson"></span></h3>
            <div class="col-xs-12 lesson-wrapper">
                <div class="lesson-list">
                    <?php $j=0;
                    if(!empty($this->sesiones))
                     foreach ($this->sesiones as $sesion){ $j++?>
                      <div class="item-tools rname " data-iname="namelesson" title="<?php echo $sesion["nombre"]; ?>" data-idnivel="<?php echo $sesion["idnivel"]; ?>">
                          <span>
                            <a href="javascript:void(0) " class="lesson-item hvr-float">
                             <div class="lesson-name" ><?php echo JrTexto::_('Activity'); ?></div>
                                <div class="lesson-name"><?php echo str_pad($j,2,"0",STR_PAD_LEFT); ?></div>
                            </a>                         
                          </span>
                      </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div> 
    </div>
</div> 
</div>
<?php ?>
<script>
$(document).on('ready', function(ev){
    var niveles=function(data){
        try{
            var res = xajax__('', 'niveles', 'GetxPadre', data);
            if(res){ return res; }
            return false;
        }catch(error){
            console.log(error);
            return false;
        }       
     }
    var initseleccion=function(){
        $('a.active').each(function(){
            var dt=$(this).closest('.item-tools').data('iname');
            var name=$(this).closest('.item-tools').attr('title');
            $('#'+dt).text(' : '+name);
        });
    };

    var cargaractividaddes=function(data,txt){
        if(data!=false){
            var i=0;
            $('.'+txt+'-list').slick('unslick');
            $('.'+txt+'-list').html('').hide('fast');
            var html='';
            var idselected='';
            var txt_=(txt=='unit'?'<?php echo JrTexto::_('Unit'); ?>':'<?php echo JrTexto::_('Activity'); ?>');
            var dni='<?php echo $usuarioAct["dni"]; ?>';
            $.each(data,function(oi,obj){
                i++;
                idselected=idselected||obj.idnivel; 
                html+='<div class="item-tools rname" data-iname="name'+txt+'" title="'+obj.nombre+'"  data-idnivel="'+obj.idnivel+'"><span >'
                        +'<a href="javascript:void(0) " class=" hvr-float '+txt+'-item '+(i==1?'active':'')+'">'
                            +'<div class="'+txt+'-name">'+txt_+'</div>'
                            +'<div class="'+txt+'-name">'+(String("00" + i).slice(-2))+'</div>'
                        +'</a></div>';
            });           
            $('.'+txt+'-list').html(html).show('fast');
            $('.'+txt+'-list').slick(optionslike);
            return idselected;          
      }
    }

    $('#todas-unidades')
    .on("mouseover",".item-tools.rname",function(){
        var dt=$(this).data('iname');
        var name=$(this).attr('title');
        $('#'+dt).text(' : '+name);
        $(this).find('.toolbottom').removeClass('hide');
    }).on("mouseout",".item-tools.rname",function(){       
        var dt=$(this).data('iname');
        $obj=$('a.active').closest('[data-iname='+dt+']');
        var name=$obj.attr('title')||' ';
        $('#'+dt).text(' : '+name);          
        $(this).find('.toolbottom').addClass('hide');             
    }).on("click",'.item-tools[data-iname="namelevel"]',function(ev){
        var aobj=$(this).closest('.level-list').find('a');
        $(aobj).removeClass('active');
        $(this).find('a').addClass('active');
        var idnivel=$(this).data('idnivel');
        var data={tipo:'U','idpadre':idnivel}
        var unidades=niveles(data);
        if(unidades!=false){
            var idunidad=cargaractividaddes(unidades,'unit');   
            var data={tipo:'L','idpadre':idunidad}
            cargaractividaddes(niveles(data),'lesson');
            initseleccion();
        }
    }).on("click",'.item-tools[data-iname="nameunit"]',function(ev){
        var aobj=$(this).closest('.unit-list').find('a');
        $(aobj).removeClass('active');
        $(this).find('a').addClass('active');
        var idnivel=$(this).data('idnivel');
        var data={tipo:'L','idpadre':idnivel}
        cargaractividaddes(niveles(data),'lesson');  
        initseleccion();    
    }).on("click",'.item-tools[data-iname="namelesson"]',function(ev){
      var aobj=$(this).closest('.lesson-list').find('a');
      $(aobj).removeClass('active');
      $(this).find('a').addClass('active');
      var idnivel=$('.level-list a.active').closest('.item-tools').data('idnivel');
      var idunidad=$('.unit-list a.active').closest('.item-tools').data('idnivel');
      var idlesson=$(this).data('idnivel');
      redir(_sysUrlBase_+'/actividad/listado/'+idnivel+'/'+idunidad+'/'+idlesson);
    });
     initseleccion();
    var optionslike={
        //dots: true,
        infinite: false,
        //speed: 300,
        //adaptiveHeight: true
        navigation: false,
        slidesToScroll: 1,
        centerPadding: '60px',
        slidesToShow: 6,
       responsive:[
        { breakpoint: 1200, settings: {slidesToShow: 5} },
        { breakpoint: 992, settings: {slidesToShow: 4 } },
        { breakpoint: 880, settings: {slidesToShow: 3 } },
        { breakpoint: 720, settings: {slidesToShow: 2 } },
        { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }     
      ]
    };
    var slikelevel=$('.level-list').slick(optionslike);
    var slikunidad=$('.unit-list').slick(optionslike);
    var slickactividad=$('.lesson-list').slick(optionslike);
    $('.otooltip').tooltip();
    $('header').show('fast').addClass('static');
});
</script>