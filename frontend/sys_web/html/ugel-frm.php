<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Ugel'">&nbsp;<?php echo JrTexto::_('Ugel'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdugel" id="pkidugel" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Nombre de Ugel');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDescripcion" name="txtDescripcion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtAbrev">
              <?php echo JrTexto::_('Abrev');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtAbrev" name="txtAbrev" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["abrev"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIddepartamento">
              <?php echo JrTexto::_('DRE');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                <select id="txtIddepartamento" class="form-control select-ctrl " name="txtIddepartamento"></select>
                </div>             
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdprovincia">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                  <select id="txtIdprovincia" class="form-control select-ctrl " name="txtIdprovincia"></select>
                </div>
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveUgel" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning cerramodal" href="<?php echo JrAplicacion::getJrUrl(array('ugel'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var iddepartamento='<?php echo !empty($frm["iddepartamento"])?$frm["iddepartamento"]:"010000" ?>';
  var idprovincia='<?php echo !empty($frm["iddepartamento"])?$frm["idprovincia"]:"" ?>';

  var _cargarDRE=function(){
    sysajax({
      url:_sysUrlBase_+'/min_dre/buscarjson',
      callback:function(rs){
        $sel=$('#txtIddepartamento');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.ubigeo+'" '+(v.ubigeo==iddepartamento?'selected="selected"':'')+ '>'+v.descripcion+'</option>');
          })
          $sel.trigger('change');
      }})
    }
    _cargarDRE();

    $('#frm-<?php echo $idgui;?>').on('change','select#txtIddepartamento',function(ev){
      ev.preventDefault();
       var iddep=($(this).val()||'00').substr(0,2);
      var fd = new FormData();
        fd.append("departamento", iddep);
        fd.append("provincia", 'all');
        fd.append("distrito", "00");
        fd.append("pais", "PE");
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/ubigeo/buscarjson',
      callback:function(rs){
        $sel=$('#txtIdprovincia');
         $sel.html('');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.id_ubigeo+'" '+(v.id_ubigeo==idprovincia?'selected="selected"':'')+ '>'+v.ciudad+'</option>');
          })
      }})
    })
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'ugel', 'saveUgel', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Ugel"))?>');
      }
     }
  });
});
</script>

