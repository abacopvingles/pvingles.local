<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();
$cbonivel=$this->cbonivel;
$cbounidad=$this->cbounidad;


?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 1px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

</style>
<script type="text/javascript">
  function recargar(){
    form1.submit();
  }
</script>

<div class="container">
  <div class="row " id="levels" style="padding-top: 1ex; ">
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Home")?></a></li>                  
                  <li class="active"><?php echo JrTexto::_("Reporte de progreso del Estudiante por Habilidades")?></li>
                </ol>
            </div>
             
               

    </div>

  

<div class="row " id="levels" style="padding-top: 1ex; ">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Reporte")?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
      <form name="form1" id="form1" method="POST" action="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_progresohab">
         

         <div class="col-md-2 " style="text-align: left" >
          Nivel:
           <select class="form-control" name="cbo_nivel" id="cbo_nivel" onchange="cargar_unidad();">
              <option value="">Todos los Niveles</option>
             <?php
              if(!empty($this->lista_niveles)){
              foreach ($this->lista_niveles as $lista_nivel){
                $nombre=$lista_nivel['nombre'];
                $idnivel=$lista_nivel['idnivel'];
              ?>
              <option value="<?php echo $idnivel?>" <?php if ($cbonivel==$idnivel) echo "selected"; ?> ><?php echo $nombre?></option>
              <?php
              }//for 
              }//if
             ?>
           </select>
         </div>

         <div class="col-md-3 " style="text-align: left" >
          Unidad:
           <select class="form-control" name="cbo_unidad" id="cbo_unidad" onchange="">
            <option value="">Todas las Unidades</option>
            <?php
              if(!empty($this->lista_unidades)){
              foreach ($this->lista_unidades as $lista_unidad){
                $nombreuni=$lista_unidad['nombre'];
                $idunidad=$lista_unidad['idnivel'];
              ?>
              <option value="<?php echo $idunidad?>" <?php if ($cbounidad==$idunidad) echo "selected"; ?> ><?php echo $nombreuni?></option>
              <?php
              }//for 
              }//if
             ?>
           </select>
         </div>

         <div class="col-md-2" style="text-align: center;padding-top: 15px" >
          <a class="btn btn-primary" onclick="recargar();" ><i class="fa fa-search"></i>&nbsp;Filtrar</a>
         </div>


       </form>


       
        <?php
        $reporte='var data = google.visualization.arrayToDataTable([
          ["Element", "Porcentaje de Avance", { role: "style" } ],';

        $fondocolor="0099FF,99CC33,FF6600,FFCC00,FF6699,";
        $colores=explode(",",$fondocolor);
        //var_dump($this->lista_progreso);
        $x=0;
        if(!empty($this->lista_habilidades)){
        foreach ($this->lista_habilidades as $lista1){
          $_idhab = intval($lista1['idmetodologia']);
          if($_idhab >= 4 && $_idhab <= 7){
            //$totalpv=($lista1['porcentaje']/$lista1['cantidad']);
            $nombre=$lista1['nombre'];
            $idhabil=$lista1['idmetodologia'];
            $totalh = $this->habil[$idhabil];
            $totalcount = $this->habiltotal[$idhabil];
            //echo $totalh."<br>";
            
            if ($totalh!=0){
              $totalh=number_format(($totalh/$totalcount), 2, '.', '');          
            }
            
  
            $reporte.='
            ["'.$nombre.'", '.$totalh.', "#'.$colores[$x].'"],';
            $x++;
            if ($x==4) $x=0;
          }
        }//for

        

        /*for ($i=1;$i<=50;$i++){
          $ale = rand (1,100);
          $reporte.='
          ["'.$curso.'", '.$ale.', "#'.$colores[$x].'"],';
          $x++;
          if ($x==4) $x=0;
        }*/


          $reporte.=']);';
        }
        ?>

        <div id="columnchart_values" class="col-md-12" style="overflow: auto" ></div>

    </div>
</div>



</div>
</div>



<script type="text/javascript" src="<?php echo $this->documento->getUrlStatic();?>/libs/demochart/loader.js"></script>
    
    <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      <?=$reporte?>

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "Progreso del estudiante por Habilidades",
        width: 1200,
        height: 500,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>

<script type="text/javascript">

function cargar_unidad(){
  
  var idnivel=$("#cbo_nivel").val();
  if (!idnivel){
    $('#cbo_unidad').html("");
    $('#cbo_unidad').append('<option value="">Todas las Unidades</option>');
    return;

  }
  
  $('#cbo_unidad').html("");

  var data={'idpadre':idnivel,'tipo':"lista_unidades"};      
    res = xajax__('', 'Reportealumno', 'getxPadre',data);
    
    if(res){
      
      
      $('#cbo_unidad').append('<option value="">Todas las Unidades</option>');
      $.each(res,function(){
        x=this;       
        //alert(z);
         $('#cbo_unidad').append('<option value="'+x["idnivel"]+'" >'+x["nombre"]+'</option>');
        
      });//each

    }//if res
    

}


</script>