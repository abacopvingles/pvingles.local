<?php
require_once("../class/c_sesion.php");
$obj = new C_cursos();

$s = $_GET['s'];

$id_emp=$_SESSION['id_emp'];
if (!$id_emp) $id_emp="LIMA0000001";

$idprograma=$_GET['idprograma'];



    if ($idprograma==9){
        $cursoint="Programa de Especialización en Informática Educativa para Docentes";
        $txtcursos='0094,0112,0095,0109,0096,0110,0011,0047,';
        $txtpreciototal=360;
        $link_retorno="http&#x3a;&#x2f;&#x2f;www.eduktvirtual.com";
    }else{
        $link_retorno="http&#x3a;&#x2f;&#x2f;www.eduktvirtual.com&#x2f;payu";
    }

//echo $idprograma."asas";
?>

<!DOCTYPE html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107953619-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107953619-1');
</script>

<!-- Global site tag (gtag.js) - Google AdWords: 831142887 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-831142887"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-831142887');
</script>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cursos Virtuales, Word, Excel, power point, tics, office,Cursos Corporativos, Cursos en vivo">
    <meta name="author" content="">
    <title>EduktVirtual</title>
    
    <!-- core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css?2" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/prettyPhoto.css" rel="stylesheet">
    <link href="../css/main.css?1" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    
    
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="../icono.png">
    <link rel="stylesheet" href="../css/slider.css?11">

<!-- Event tag for Registro Web conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
<script>
function gtag_report_conversion(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
      'send_to': 'AW-831142887/DbC2CNal1nYQ5_eojAM',
      'event_callback': callback
  });
  return false;
}
</script>

</head><!--/head-->


<style type="text/css">
    .border{
        border: solid 1px #f00;
    }
    .div_header{
        position: fixed;
        z-index: 1000;
        width: 100%;
        background-color: #fff;

    }
</style>

<body class="homepage">

    <header id="header" class="div_header" >
        
        <?
        include("../menu_header2.php");
        ?>
        
    </header><!--/header-->
<style type="text/css">
    .fuente1{
        color: #fff;
    }
    .efecto1{
                position:relative;      
                -webkit-box-shadow:0 0px 4px #777, 0 0 20px #CCC inset;
                -moz-box-shadow:0 0px 4px #777, 0 0 20px #CCC inset;
                box-shadow:0 0px 4px #777, 0 0 20px #CCC inset;
    }
    .efecto2{
        -webkit-box-shadow: 0px 10px 6px -6px #777;
        -moz-box-shadow: 0px 10px 6px -6px #777;
        box-shadow: 0px 10px 6px -6px #777;
    }
    



.div_banner{
    height: 400px;    
}

@media (min-width: 240px) and (max-width: 319px){ 
    
    
    
}




@media (min-width: 320px) and (max-width: 479px) {

   
    .div_titulo1{
        margin-top: 220px;
    }
    
    .div_banner{
        height: 700px;    
    }

}

@media screen and (min-width: 480px) and (max-width: 567px) {
   
}

@media (min-width: 568px) and (max-width: 599px) {
    
   

}




@media screen and (min-width: 600px) and (max-width: 767px) {
    
    .div_titulo1{
        margin-top: 220px;
    }
    
    
    .div_banner{
        height: 750px;    
    }
    
}
@media screen and (min-width: 768px) and (max-width: 799px) {
    
    .div_form{
        margin-top: 0px;
    }
    
    
    .div_banner{
        height: 500px;    
    }

}
@media screen and (min-width: 800px) and (max-width: 830px) {
   
    .div_form{
        margin-top: 0px;
    }
    
    
    .div_banner{
        height: 500px;    
    }
    
}
@media all
    and (min-width: 831px)
    and (max-width: 1024px){
    
   .div_form{
        margin-top: 0px;
    }
    
    
    .div_banner{
        height: 400px;    
    }

}


@media all
    and (min-width: 1025px)
    and (max-width: 1279px){
    

}

@media all
    and (min-width: 1281px)
    and (max-width: 1366px){
    
}


@media all
    and (min-width: 1367px)
    and (max-width: 2560px){
    
    
   


}

input[type="checkbox"],
input[type="radio"] {
    box-sizing: border-box;
    padding: 0; 
    height: 25px;
    width: 25px;
}

</style>

<script defer src="https://unpkg.com/imask"></script>
    <script type="text/javascript">
      document.addEventListener("DOMContentLoaded", function () {
        var phoneMask = new IMask(document.getElementById('txtfono'), {
          mask: '{(+51)}000-000-000'        
        });
        var phoneMask = new IMask(document.getElementById('txtfono2'), {
          mask: '{(+51)}000-0000'        
        });

     

        var regExpMask = new IMask(document.getElementById('regexp-mask'), {
          mask: /^[1-6]\d{0,5}$/
        });

        var numberMask = new IMask(document.getElementById('number-mask'), {
          mask: Number,
          min: -10000,
          max: 10000,
          thousandsSeparator: ' '
        }).on('accept', function() {
          document.getElementById('number-value').innerHTML = numberMask.masked.number;
        });

        var dateMask = new IMask(
          document.getElementById('date-mask'),
          {
            mask: Date,
            min: new Date(2000, 0, 1),
            max: new Date(2020, 0, 1),
            lazy: false
          }
        ).on('accept', function() {
          document.getElementById('date-value').innerHTML = dateMask.masked.date || '-';
        });

        var dynamicMask = new IMask(
          document.getElementById('dynamic-mask'),
          {
            mask: [
              {
                mask: '+{7}(000)000-00-00'
              },
              {
                mask: /^\S*@?\S*$/
              }
            ]
          }
        ).on('accept', function() {
          document.getElementById('dynamic-value').innerHTML = dynamicMask.masked.unmaskedValue || '-';
        });
      });

      function ver_fono(sw){
        if (sw==1){
            $("#txtfono").css("display","");
            $("#txtfono2").css("display","none");
            $("#tiposw").val("1");
            $("#txtfono").focus();
            
        }else{
            $("#txtfono").css("display","none");
            $("#txtfono2").css("display","");
            $("#tiposw").val("2");
            $("#txtfono2").focus();
        }
        $("#div_tipot").css("display","none");
      }
    </script>

    <div id="div_espacio" style="width: 100%; height: 75px; background-color: #000"></div>

    <section id="main-slider" class="no-margin div_banner" >
        <div class="carousel slide">
            
            <div class="carousel-inner div_banner">

                <div class="item active" style="background-image: url(bg1.jpg?2)">
                    <div class="container"  >
                        <div class="row slide-margin " style="margin-top: 30px">
                            
                            <div class="col-sm-6 col-md-8 div_titulo1 "  >
                                
                                    <!--h1 class="animation animated-item-1 fuente1"  >Medios de Pago:</h1>
                                    <p style="font-size: 18px; text-align: left;"  class="animation animated-item-2 fuente1 ">
                                        <img src="../images/visa.png" width="15%">
                                        <img src="../images/mastercard.png" width="15%">
                                        <img src="../images/americanexpress.png" width="15%">
                                        <img src="../images/diners.png" width="15%"><br>
                                        <img src="../images/bcp.png" width="15%">
                                        <img src="../images/pagoefectivo.png" width="15%">
                                    </p-->
                                    <h1 class="animation animated-item-1 fuente1"  >Más de 25 Cursos</h1>
                                    <p style="font-size: 20px;" class="animation animated-item-1 fuente1" >Contamos con más de 25 cursos de Microsoft Office versiones 2013, 2010 y otros en sus niveles de inicial, intermedio y avanzado.</p>
                                
                            </div>

                            <div class="col-sm-6 col-md-4 animation animated-item-4 div_form ">
                            <form name="form_contac" id="form_contac" >
                            <input type="hidden" name="s" id="s" value="<?=$s?>">
                            <input type="hidden" name="idempresa" id="idempresa" value="<?=$id_emp?>">
                                <div class="row efecto1" style="padding: 10px; background-color: #fff;" >
                                
                                    <div class="col-sm-12" style="padding: 2px; text-align: center;color: #2E7DA4; font-size: 20px; ">
                                    Estas a punto de empezar tu curso!!!
                                    </div>

                                    <div class="col-sm-6" style="padding: 2px;">
                                    <input type="text" name="txtnom" id="txtnom" class="form-control" placeholder="Nombres *" >
                                    </div>

                                    <div class="col-sm-6" style="padding: 2px;">
                                    <input type="text" name="txtape" id="txtape" class="form-control" placeholder="Apellidos *" >
                                    </div>

                                    <div class="col-sm-6" style="padding: 2px;">
                                    <select class="form-control" name="dpto" id="dpto" onchange="ver_ciudad()" >
                                    <option value="" >Departamento</option>
                                    <?
                                    $lista_dpto = $obj->autogenerado("SELECT * from departamento order by 1");
                                    //var_dump($lista_cursos);
                                    for($i=0;$i<sizeof($lista_dpto);$i++){
                                        $departamento = $lista_dpto[$i]['nombre'];
                                        $iddep = $lista_dpto[$i]['iddepartamento'];
                                    ?>
                                    <option value="<?=$iddep?>"><?=$departamento?></option>
                                    <?
                                    }//for
                                    ?>
                                    </select>
                                    
                                    </div>

                                    <div class="col-sm-6" style="padding: 2px;">
                                        <div id="div_tipot">
                                        <input type="radio" name="tipot" id="tipot1" onclick="ver_fono(1)" value="1" ><font style="position: relative; top: -8px ">Móvil</font>&nbsp;&nbsp;&nbsp;
                                        <input type="radio" name="tipot" id="tipot2" onclick="ver_fono(2)" value="2"><font style="position: relative; top: -8px ">Teléfono</font>
                                        <input type="text" name="tiposw" id="tiposw" value="0" style="display: none">
                                        </div>

                                    <input type="tel" name="txtfono" id="txtfono" class="form-control" placeholder="xxx-xxx-xxx" style="display: none" >

                                    <input type="tel" name="txtfono2" id="txtfono2" class="form-control" placeholder="xxx-xxxx" style="display: none">

                                    </div>

                                    <div class="col-sm-12" style="padding: 2px;">
                                    <input type="email" name="txtcorreo" id="txtcorreo" class="form-control" placeholder="Email *" >
                                    </div>
                                    
                                    


                                    <div class="col-sm-12" style="padding: 2px;">

                                    <select class="form-control" name="programa" id="programa" onchange="ver_producto(1)">
                                        <option value="">Curso de Interés *</option>
                                        <option value="1">Microsoft Excel 2016 - Experto  Modalidad Presencial</option>
                                        <option value="2">Microsoft Excel 2016 - Experto  Modalidad  Virtual</option>
                                        <option value="3">Microsoft Office 2013 Básico</option>
                                        <option value="4">Microsoft Office 2013 Intermedio</option>
                                        <option value="5">Microsoft Office 2010 Básico</option>
                                        <option value="6">Microsoft Office 2010 Intermedio</option>
                                        <option value="7">Microsoft Office 2010 Avanzado</option>
                                        <option value="8">Herramienta Educativas</option>
                                        <option value="9" <?php if ($idprograma==9) echo "selected" ?>>Programa de Especialización en Informática Educativa para Docentes</option>
                                        <?
                                        $lista_cursos = $obj->autogenerado("SELECT idcurso,nombre,regfecha from curso where idcurso='0114' or idcurso='0115' or idcurso='0116' or idcurso='0117' or idcurso='0118' or idcurso='0119' or idcurso='0120' or idcurso='0121' or idcurso='0122' or idcurso='0043' or idcurso='0089' or idcurso='0048' or idcurso='0044' or idcurso='0090' or idcurso='0049' or idcurso='0045' or idcurso='0091' or idcurso='0065' or idcurso='0071' or idcurso='0046' or idcurso='0068' or idcurso='0092' or idcurso='0069' or idcurso='0093' or idcurso='0010' or idcurso='0070' or idcurso='0104' or idcurso='0103' or idcurso='0102' or idcurso='0094' or idcurso='0095' or idcurso='0109' or idcurso='0096' or idcurso='0110' or idcurso='0011' or idcurso='0016' or idcurso='0047' or idcurso='0050' or idcurso='0051' or idcurso='0052' or idcurso='0053' or idcurso='0054' or idcurso='0055' or idcurso='0072' or idcurso='0073' or idcurso='0105' or idcurso='0111' or idcurso='0112' or idcurso='0113' order by 3 desc");

                                        
                                        for($i=0;$i<sizeof($lista_cursos);$i++){
                                            $idcurso = $lista_cursos[$i]['idcurso'];
                                            $curso = $lista_cursos[$i]['nombre'];
                                        ?>
                                        <option value="<?=$idcurso?>"><?=$curso?></option>
                                        <?
                                        }
                                        ?>
                                    </select>

                                        <div class="col-sm-12" id="div_compra" style="text-align: right; display: none; padding: 0px">
                                            <input type="hidden" name="txtnomcurso" id="txtnomcurso">
                                            <input type="hidden" name="txtcursos" id="txtcursos" value="<?php echo $txtcursos;?>">
                                            <input type="hidden" name="txtpreciototal" id="txtpreciototal" value="<?php echo $txtpreciototal;?>">
                                            <div class="col-sm-8 " id="t_compra" style="height: 38px; padding-top: 8px" ></div>
                                            <div class="col-sm-4 " >
                                                <a class="btn btn-success" onclick="ver_producto(2)"><i class="fa fa-cart-plus" style="font-size: 20px"></i>&nbsp;&nbsp;Editar</a>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-12" style="padding: 2px;">
                                    <select class="form-control" name="referido" id="referido">
                                        <option value="">¿Cómo te enteraste?</option>
                                        <option value="1">Google</option>
                                        
                                    </select>
                                    </div>

                                    <div class="col-sm-6" style="padding: 8px;text-align: right;">
                                        ¿Cuándo iniciarías?
                                    </div>
                                    <?
                                    $fecini=date('Y-m-d');
                                    ?>
                                    <div class="col-sm-6" style="padding: 2px;">
                                        <input type="date" name="fecini" id="fecini" class="form-control" value="<?=$fecini?>">
                                    </div>

                                    

                                    <div class="col-sm-12" style="padding: 2px;">
                                        <button style="height: 50px; font-size: 25px" type="button" class="col-sm-12 btn btn-primary btn-contac btn-comienza">Comienza Ahora!!!</button>
                                    </div>
                                    
                                    <div class = "col-sm-12" style="">                    
                                        <label class = "msj_aviso " style="color:#093; font-weight: bold;font-size: 14px; text-align: center">                   
                                        </label>
                                    </div>
                                
                                
                                </div>

                            </form>
                            </div>






                        </div>
                    </div>
                </div><!--/.item-->

                
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        
    </section><!--/#main-slider-->

    
    <!--section id="about-us" style="padding-top: 20px; display: <?=$ver_testi?>">
        <div class="container">
            
            
                <div class="center wow fadeInDown " style="padding:0px; ">
                    <h2>En 3 Pasos puedes realizar tu compra en Linea:</h2>
                    <img src="../aviso_card.gif" class="img-responsive">
                </div>
            
        </div>
    </section-->

    <?
        include("../beneficios.php");
    ?>

    <?
        include("../porque.php");
    ?>

    <?
        include("../comofunciona.php");
    ?>

    <?
        include("../clientes.php");
    ?>

    <?
        include("../pie2.php");
    ?>

    

    


    



    

   

    <script src="../js/jquery.js"></script>
    
                                                                                            

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.prettyPhoto.js"></script>
    <script src="../js/jquery.isotope.min.js"></script>
    <script src="../js/main.js"></script>
    <!--script src="../js/wow.min.js"></script-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

<!--PRIMER MODAL-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="text-align: justify;">
            <div class="modal-content">
                <div id = 'vista_curso'>
                <?
                include("aviso_card.php");
                ?>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div id = 'vista_curso2'>
                </div>
            </div>
        </div>
    </div>

<script>

$(document).ready(function () {
    //$('#myModal').modal('show')
});
    

   
function ver_producto(sw){
    var idcur = $("#programa").val();
    var idempresa = $("#idempresa").val();
    var cursos = $("#txtcursos").val();
    console.log(idcur);

    /*if (idcur=='1' || idcur=='2' || idcur=='3' || idcur=='4' || idcur=='5' || idcur=='6'){
        return false;
    }*/

    if (idcur=='1' || idcur=='2' || idcur=='3' || idcur=='4' || idcur=='5' || idcur=='6' || idcur=='7' || idcur=='8' || idcur=='9'){
        if (sw=='1'){
            console.log('entre aqui');
            if (idcur=='1'){
                cursos='0123,0126,0127,0128,0129,';
                $('#txtnomcurso').val("Microsoft Excel 2016 - Experto  Modalidad Presencial");
                $("#txtcurso_int").val("Microsoft Excel 2016 - Experto  Modalidad Presencial");

            }else if (idcur=='2'){                    
                cursos='0130,0131,0132,0133,0134,';
                $('#txtnomcurso').val("Microsoft Excel 2016 - Experto  Modalidad  Virtual");
                $("#txtcurso_int").val("Microsoft Excel 2016 - Experto  Modalidad  Virtual");

            }else if (idcur=='3'){  
                cursos='0094,0095,0096,0102,0103,0111,0113,';
                $('#txtnomcurso').val("Microsoft Office 2013 Básico");
                $("#txtcurso_int").val("Microsoft Office 2013 Básico");  

                

            }else if (idcur=='4'){    
                cursos='0109,0110,0112,';
                $('#txtnomcurso').val("Microsoft Office 2013 Intermedio");
                $("#txtcurso_int").val("Microsoft Office 2013 Intermedio");

                

            }else if (idcur=='5'){
                cursos='0042,0043,0044,0045,0046,0068,0071,0069,';
                $('#txtnomcurso').val("Microsoft Office 2010 Básico");
                $("#txtcurso_int").val("Microsoft Office 2010 Básico");

                

            }else if (idcur=='6'){          
                cursos='0089,0090,0091,0092,0093,';
                $('#txtnomcurso').val("Microsoft Office 2010 Intermedio");
                $("#txtcurso_int").val("Microsoft Office 2010 Intermedio");

                
            }else if (idcur=='7'){            
                cursos='0048,0049,0065,';
                $('#txtnomcurso').val("Microsoft Office 2010 Avanzado");
                $("#txtcurso_int").val("Microsoft Office 2010 Avanzado");

                
            }else if (idcur =='8'){
                cursos='0010,0011,0016,0047,0050,0051,0052,0053,0054,0055,0070,0072,0073,0104,0105,';
                $('#txtnomcurso').val("Herramienta Educativas");
                $("#txtcurso_int").val("Herramienta Educativas"); 
            }else if (idcur =='9'){    
                cursos='0094,0112,0095,0109,0096,0110,0011,0047,';
                $('#txtnomcurso').val("Programa de Especialización en Informática Educativa para Docentes");
                $("#txtcurso_int").val("Programa de Especialización en Informática Educativa para Docentes");
                $("#txtpreciototal").val("360");
            }
            $("#txtcursos").val(cursos);
        }


    }else{
        

        if (sw=='1') $("#txtcursos").val(idcur+",");

        
        var formData = new FormData();        
        formData.append("idcurso",idcur);
        formData.append("sw","ver_curso");
        
        $.ajax({
            url: "grabar_ajax.php",
            type: "POST",                
            dataType: "json",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false                
        })
        .done(function(res){
            //alert(res);
            //alert(res[0]["nombre"]);
            $('#txtnomcurso').val(res[0]["nombre"]);
            $("#txtcurso_int").val(res[0]["nombre"]);
            
                
            
        }).fail(function() {

            //$('#t_detalle_tarea3').html("<b style='color:#f00'>No Hay Alumnos Seleccionados</b>");
               
        })

    }

    
    if (idcur!='9'){
           
        $("#returnURL").val("http://www.eduktvirtual.com/payu");
        
    $.ajax({
    url: "ver_curso.php?idcur="+idcur+"&sw="+sw+"&cursos="+cursos,
    type: "POST",
    //data: { dato_usuario: $(this).attr("id_usuario")},
        success: function(result){
            // console.log(result)
            $("#vista_curso2").html(result);
            $('#myModal2').modal('show')
        }
    });

    }else{
        $("#returnURL").val("http://www.eduktvirtual.com");
    }//if



    


}
         
   
$(".btn-contac").click(function(){
//function agregar_contac(){
    
    
    
    var txtnom = $("#txtnom").val();
    var txtape = $("#txtape").val();
    var txtfono = $("#txtfono").val();    
    var txtcorreo = $("#txtcorreo").val();
    var dpto = $("#dpto").val();
    var programa = $("#programa").val();
    var referido = $("#referido").val();
    var fecini = $("#fecini").val();

    var tiposw = $("#tiposw").val();
    var txtfono2 = $("#txtfono2").val();

    var txtcursos = $("#txtcursos").val();
    var txtpreciototal = $("#txtpreciototal").val();
    
    
   

    //alert(tipot);

    if(txtnom.length==0){
        $("#txtnom").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#txtnom").css("border", "");
        }, 5000);
        return(false);
    }
    if(txtape.length==0){
        $("#txtape").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#txtape").css("border", "");
        }, 5000);
        return(false);
    }
    if(dpto.length==0){
        $("#dpto").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#dpto").css("border", "");
        }, 5000);
        return(false);
    }
    

    if (tiposw=='0'){

        $("#div_tipot").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#div_tipot").css("border", "");
        }, 5000);
        return(false);

    }else if (tiposw=='1'){

        if(txtfono.length==0){
            $("#txtfono").css("border", "1px solid #ff0000");
            setTimeout (function(){
                $("#txtfono").css("border", "");
            }, 5000);
            return(false);
        }

    }else{

        
        if(txtfono2.length==0){
            $("#txtfono2").css("border", "1px solid #ff0000");
            setTimeout (function(){
                $("#txtfono2").css("border", "");
            }, 5000);
            return(false);
        }
        txtfono=txtfono2;

    }

    if(txtcorreo.length==0){
        $("#txtcorreo").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#txtcorreo").css("border", "");
        }, 5000);
        return(false);
    }
    if(programa.length==0){
        $("#programa").css("border", "1px solid #ff0000");
        setTimeout (function(){
            $("#programa").css("border", "");
        }, 5000);
        return(false);
    }

    var emailCompare = /^([a-z0-9_.-]+)@([da-z.-]+).([a-z.]{2,6})$/; // Syntax to compare against input
    var email = $('input#txtcorreo').val().toLowerCase(); // get the value of the input field
    if (email == "" || email == " " || !emailCompare.test(email)) {

        $(".msj_aviso").show();
        $(".msj_aviso").css("color","#f00");
        $(".msj_aviso").html("Escriba correctamente su Email");

        setTimeout (function(){
            $(".msj_aviso").hide();
        }, 3000);
        return(false);
    }

    

    $(".btn-contac").attr('disabled', 'true');
    
        /*if (programa=='1'){
            var pagina="../office2013/";
        }else if (programa=='2'){
            var pagina="../office2010/";
        }else if (programa=='3'){
            var pagina="../otros/";
        }else{
            var pagina="../office2013/";
        }*/
        
        var formData = new FormData();
        formData.append("txtnom",txtnom);
        formData.append("txtape",txtape);
        formData.append("txtfono",txtfono);
        formData.append("txtcorreo",txtcorreo);
        formData.append("dpto",dpto);
        formData.append("programa",programa);
        formData.append("referido",referido);
        formData.append("fecini",fecini);
        formData.append("txtpreciototal",txtpreciototal);
        formData.append("txtcursos",txtcursos);
        formData.append("sw","agregar_contac");
        

        $.ajax({
            url: "grabar_ajax.php",
            type: "POST",                
            dataType: "json",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false                
        })
        .done(function(res){
            //alert(res);
            if(res.codigo=="OK"){                   
                
               

               
                $(".msj_aviso").show();
                $(".msj_aviso").css("color","#093");
                $(".msj_aviso").html(res.mensaje);

                //$('#myModal').modal('show');

                //procesar();
                
                $("#txtapellidos").val(txtape);
                $("#txtnombres").val(txtnom);
                $("#txtcorreos").val(txtcorreo);                
                $("#txtphone").val(txtfono);

                
                /*Agregar session storage*/
                
                localStorage.setItem("_SESSION",JSON.stringify(res._SESSION));
                /*___________________________*/
                
                
                setTimeout (function(){
                    //$(".msj_aviso").css("display","none");
                    //form_contac.action=pagina;
                    //frm_eve.submit();
                    WebToContacts2844568000000605010.submit()
                }, 3000);
                
              

            }
            
        })

        .fail(function(error){
            console.log("error");
        })
   

});

function ver_ciudad(){
    //alert("asas");
    var iddep = $('#dpto').val();

    
    var formData = new FormData();
    formData.append("iddep",iddep);            
    formData.append("sw","ver_ciudad"); 

    $.ajax({
        url: "grabar_ajax.php",
        type: "POST",                
        dataType: "json",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false                
    })
    .done(function(res){
        console.log(res);
                            
            
        $('#txt_ciudad').val(res[0]["nombre"]);
            
        
    }).fail(function() {

        //$('#t_detalle_tarea3').html("<b style='color:#f00'>No Hay Alumnos Seleccionados</b>");
           
    })

    //$("#txt_ciudad").val("as");

}

   
        
function procesar (){
    
    var txtcorreo = $("#txtcorreo").val();
    var txtcursos = $("#txtcursos").val();

    var cant_cursos = txtcursos.split(",");
    var total = cant_cursos.length-1;
    if (total==1){
        //alert($("#txtnomcurso").val());
        var nombrecurso= $("#txtnomcurso").val()+" - Perú";
    }else{
        var nombrecurso= "Cursos de EduktVirtual - Perú";
    }


    var formData = new FormData();    
    formData.append("sw","obtener_datos");

    $.ajax({
        url: "grabar_ajax.php",
        type: "POST",                
        dataType: "json",
        data:  formData,
        contentType: false,
        cache: false,
        processData:false                
    })
    .done(function(res){            
            
            //var str = res[0]['payu'];
            var precio = res[0]['precio'];
            var codigo = res[0]['codigo'];
            var prueba = res[0]['prueba'];
            var merchantId = res[0]['merchantId'];
            var idcuenta = res[0]['idcuenta'];
            var api = res[0]['api'];
            var str = '<form method="post" action="https://checkout.payulatam.com/ppp-web-gateway-payu/"><input name="merchantId" type="text" value="'+merchantId+'" ><input name="accountId" type="text"  value="'+idcuenta+'" ><input name="ApiKey" type="text" value="'+api+'" ><input name="description" type="text"  value="'+nombrecurso+'"><input name="referenceCode" type="text" value="'+codigo+'" ><input name="amount" type="text" value="'+precio+'"><input name="tax" type="text" value="0" ><input name="taxReturnBase" type="text" value="0" ><input name="currency" type="text" value="PEN"><input name="signature" type="text" value="'+prueba+'"><input name="test" type="text"  value="0" ><input name="buyerEmail" type="text" value="'+txtcorreo+'" ><input name="responseUrl" type="text"  value="http://www.eduktvirtual.com/enbuenahora" ><input name="confirmationUrl" type="text" value="http://www.eduktvirtual.com/enbuenahora" ><input name="Submit" type="submit"  value="Enviar"></form>';

            var btn = str.replace('method="post"', 'name="frm_eve" id="frm_eve" method="post"');
            $("#div_frm").html(btn);
        
    })

    .fail(function(error){
        console.log("error");
    })
}

        

</script>




    <script src="../js/slider.min.js"></script>
    
    <script>
    $(function(){
        $('#slider2').rbtSlider({
            height: '20vh', 
            'dots': false,
            'arrows': false,
            'auto': 5
        });
    });
    </script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<?
        include("../pie.php");
    ?>



<script>
window.addEventListener('load',function(){
var timerUp = setInterval(function() {
if(jQuery('.msj_aviso:contains("En buena hora, Tus datos fueron")').is(':visible')){
gtag('event', 'formsubmit', {
  'event_category': 'form',
  'event_action': 'submit',
  'event_label':'success'
  
});
    clearInterval(timerUp);
}
  }, 1000)
});
</script>



<div id="div_frm" style="display:none ">
    
<div id='crmWebToEntityForm' style='width:600px;margin:auto;'>
   <META HTTP-EQUIV ='content-type' CONTENT='text/html;charset=UTF-8'>
   <form action='https://crm.zoho.com/crm/WebToContactForm'  name="WebToContacts2844568000000605010" method='POST' onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

   <!-- Do not remove this code. -->
  <input type='text' style='display:none;' name='xnQsjsdp' value='1aa8fe29e3f821a6fae5bf37956c1f4ac1358fe23d78087f9105fdc7a003d082'/>
  <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
  <input type='text' style='display:none;' name='xmIwtLD' value='5df9d27dae5bc39dbff561f0e96fd5aaaaf01592b1b94a23fa254e8d20a9a8f2'/>
  <input type='text' style='display:none;'  name='actionType' value='Q29udGFjdHM='/>
  

  <input type='text' style='display:;' name='returnURL' id='returnURL' value='<?php echo $link_retorno;?>' /> 
   <!-- Do not remove this code. -->
  <style>
    tr , td { 
      padding:6px;
      border-spacing:0px;
      border-width:0px;
      }
  </style>
  <table style='width:600px;background-color:white;color:black'>

  <tr><td colspan='2' style='text-align:left;color:black;font-family:Arial;font-size:14px;'><strong>Inscripcion EduktVirtual</strong></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Apellidos<span style='color:red;'>*</span></td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='80' name='Last Name' id="txtapellidos" /></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Nombre</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='40' name='First Name' id="txtnombres" /></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Correo electr&oacute;nico</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='100' name='Email' id="txtcorreos" /></td></tr>

  
  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Cursos de Interes</td><td style='width:250px;'>
    <input type='text'  name='CONTACTCF1' id="txtcurso_int" value="<?php echo $cursoint;?>" />
    <!--select style='width:250px;' name='CONTACTCF1' id="txtcurso_int">
      <option value='-None-'>-None-</option>
      <option value='Microsoft&#x20;Office&#x20;2013'>Microsoft Office 2013</option>
      <option value='Microsoft&#x20;Office&#x20;2010'>Microsoft Office 2010</option>
      <option value='Microsoft&#x20;Word&#x20;2013&#x20;Basico'>Microsoft Word 2013 Basico</option>
      <option value='Microsoft&#x20;Word&#x20;2013&#x20;Intermedio'>Microsoft Word 2013 Intermedio</option>
      <option value='Microsoft&#x20;Excel&#x20;2013&#x20;Basico'>Microsoft Excel 2013 Basico</option>
      <option value='Microsoft&#x20;Excel&#x20;2013&#x20;Intermedio-Avanzado'>Microsoft Excel 2013 Intermedio-Avanzado</option>
      <option value='Microsoft&#x20;Excel&#x20;2013&#x20;Avanzado'>Microsoft Excel 2013 Avanzado</option>
      <option value='Microsoft&#x20;Power&#x20;Point&#x20;2013&#x20;Basico'>Microsoft Power Point 2013 Basico</option>
      <option value='Microsoft&#x20;Power&#x20;Point&#x20;2013&#x20;Avanzado'>Microsoft Power Point 2013 Avanzado</option>
      <option value='Microsoft&#x20;Windows&#x20;8'>Microsoft Windows 8</option>
      <option value='Microsoft&#x20;Word&#x20;2010&#x20;Basico'>Microsoft Word 2010 Basico</option>
      <option value='Microsoft&#x20;Word&#x20;2010&#x20;Intermedio'>Microsoft Word 2010 Intermedio</option>
      <option value='Microsoft&#x20;Word&#x20;2010&#x20;Avanzado'>Microsoft Word 2010 Avanzado</option>
      <option value='Microsoft&#x20;Excel&#x20;2010&#x20;Basico'>Microsoft Excel 2010 Basico</option>
      <option value='Microsoft&#x20;Excel&#x20;2010&#x20;Intermedio'>Microsoft Excel 2010 Intermedio</option>
      <option value='Microsoft&#x20;Excel&#x20;2010&#x20;Avanzado'>Microsoft Excel 2010 Avanzado</option>
      <option value='Microsoft&#x20;Power&#x20;Point&#x20;2010&#x20;Basico'>Microsoft Power Point 2010 Basico</option>
      <option value='Microsoft&#x20;Power&#x20;Point&#x20;2010&#x20;Intermedio'>Microsoft Power Point 2010 Intermedio</option>
      <option value='Microsoft&#x20;Power&#x20;Point&#x20;2010&#x20;Avanzado'>Microsoft Power Point 2010 Avanzado</option>
      <option value='Microsoft&#x20;Outlook&#x20;2010'>Microsoft Outlook 2010</option>
      <option value='Microsoft&#x20;Access&#x20;2010&#x20;Basico'>Microsoft Access 2010 Basico</option>
      <option value='Microsoft&#x20;Access&#x20;2010&#x20;Intermedio'>Microsoft Access 2010 Intermedio</option>
      <option value='Enlace&#x20;de&#x20;Datos'>Enlace de Datos</option>
      <option value='Integraci&oacute;n&#x20;de&#x20;Datos&#x20;Intermedio'>Integraci&oacute;n de Datos Intermedio</option>
      <option value='Informatica&#x20;Basica'>Informatica Basica</option>
      <option value='Microsoft&#x20;Windows&#x20;Seven'>Microsoft Windows Seven</option>
      <option value='Tablas&#x20;Din&aacute;micas&#x20;en&#x20;Excel'>Tablas Din&aacute;micas en Excel</option>
      <option value='Excel&#x20;Financiero'>Excel Financiero</option>
      <option value='Herramientas&#x20;Educativas'>Herramientas Educativas</option>
      <option value='Prezi'>Prezi</option>
      <option value='Xmind'>Xmind</option>
      <option value='Freemind'>Freemind</option>
      <option value='Robomind'>Robomind</option>
    </select--></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Tel&eacute;fono</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='50' name='Phone' id="txtphone" /></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Pa&iacute;s para correspondencia</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='30' name='Mailing Country' value="Perú" /></td></tr>

  <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Estado para correspondencia</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='30' name='Mailing State' id="txt_estado"  /></td></tr>

    <tr><td  style='nowrap:nowrap;text-align:left;font-size:12px;font-family:Arial;width:200px;'>Ciudad para correspondencia</td><td style='width:250px;' ><input type='text' style='width:250px;'  maxlength='30' name='Mailing City' id="txt_ciudad" /></td></tr>

  <tr><td colspan='2' style='text-align:center; padding-top:15px;'>
    <input style='font-size:12px;color:#131307' type='submit' value='Enviar' />
    <input type='reset' style='font-size:12px;color:#131307' value='Restablecer' />
      </td>
  </tr>
   </table>
  <script>
    var mndFileds=new Array('Last Name');
    var fldLangVal=new Array('Apellidos');
    var name='';
    var email='';

    function checkMandatory() {
    for(i=0;i<mndFileds.length;i++) {
      var fieldObj=document.forms['WebToContacts2844568000000605010'][mndFileds[i]];
      if(fieldObj) {
      if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
       if(fieldObj.type =='file')
        { 
         alert('Seleccione un archivo para cargar.'); 
         fieldObj.focus(); 
         return false;
        } 
      alert(fldLangVal[i] +' no puede estar vacío.'); 
              fieldObj.focus();
              return false;
      }  else if(fieldObj.nodeName=='SELECT') {
             if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
        alert(fldLangVal[i] +' no puede ser nulo.'); 
        fieldObj.focus();
        return false;
         }
      } else if(fieldObj.type =='checkbox'){
       if(fieldObj.checked == false){
        alert('Please accept  '+fldLangVal[i]);
        fieldObj.focus();
        return false;
         } 
       } 
       try {
           if(fieldObj.name == 'Last Name') {
        name = fieldObj.value;
          }
      } catch (e) {}
        }
    }
       }
     
</script>

</div>


</body>
</html>