<?php
defined('RUTA_BASE') or die(); 
$idgui = uniqid();
$url = $this->documento->getUrlBase();

//variables to json
$json_progresos = json_encode($this->progresos);
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }

</style>

<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportealumno" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
			<li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>                  
      <li class="active">
        <?php echo JrTexto::_("Student Progress"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START CONTAINER-->
<div class="">
	<!--START PANEL-->
    <div class = 'panel panel-primary' >
    	<div class="panel-heading" style="text-align:left;">
    		<?php echo JrTexto::_("Report");?>
    	</div>
    	<div class = 'panel-body' style="text-align: center;  " >
            <h3 style="font-weight:bold;"><?php echo JrTexto::_("Student Progress"); ?></h3>
            <div class="row" style="margin:15px auto;">
                <div class="col-md-4" style="/*height:90px;*/">
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                    <h2><?php echo $this->fullname ?></h2>
                </div>
                <div class="col-md-8">
                  <div class="col-sm-12">
                    <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                          <?php
                            if(!empty($this->cursos)){               
                              foreach ($this->cursos as $key => $lista_nivel){
                                $nombre=$lista_nivel['nombre'];
                                $idnivel=$lista_nivel['idcurso'];
                                echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                              }
                            }else{
                              echo '<option value="0">Sin cursos registrado</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                	
                  <div id="progress_total" class="col-sm-12" style="text-align:center;">
                    <h4 style="padding-top:15px;"><?php echo JrTexto::_("Total Progress in The Course"); ?> <span class="titleCursoName">A1</span></h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                        aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width:35%">
                            <!-- 40% Complete (success) -->
                        </div>
                    </div>
                    <div class="countPercentage" data-value="35"><span>0</span>%</div>
                  </div>
                </div>
                <!--end top view -->
                <?php if(!empty($this->progresos)): ?>
                  <div class="col-md-12">
                    <!--start caja-->
                    
                    <!--start graficos-->
                    <div class="col-sm-6" style="border:1px solid gray; border-radius:1em; padding:5px 0; box-shadow: -3px 3px 14px;">
                      <div style="height:50px;"></div>
                      <h3><?php echo JrTexto::_("Units"); ?></h3>
                      <div>
                        <canvas id="chart1" width="800" height="700" class="chartjs-render-monitor" style="display: block;"></canvas>
                      </div>
                    </div>
                    <div class="col-sm-6" style="border:1px solid gray; border-radius:1em; padding:5px 0; box-shadow: 3px 3px 14px;">
                      <div style="height:50px; display:inline-block;">
                        <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Unit"); ?></p>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="select_viewActivity" id="select_viewActivity" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                              <option value="0"><?php echo JrTexto::_("No Data"); ?></option>
                            </select>
                        </div>
                      </div>
                      <h3><?php echo JrTexto::_("Sessions"); ?></h3>
                      <div>
                        <canvas id="chart2" width="800" height="700" class="chartjs-render-monitor" style="display: block;"></canvas>                      
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
                
            </div>
            <!--END row-->
</div>
<!--END CONTAINER-->

<script type="text/javascript">
	var progresos = <?php echo $json_progresos ?>;
  
  function nameSelect(select,value){
      var text = null;
      select.find('option').each(function(k,v){
      if($(this).attr('value') == value){
          text = $(this).text();
      }
      });
      return text;
  }
	function drawBarHorizontal(ctx,data){
	    var myPieChart = new Chart(ctx,{
	        type: 'horizontalBar',
	        data: data,
	        options: {
            legend: false,
		        scales: {
		            xAxes: [{
                    scaleLabel: {
                      display: true,
                      labelString: 'Percentage'
                    },
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
	    });
	  }
  function countText(obj,valueText, ToValue){
    var currentValue = parseInt(valueText);
    var nextVal = ToValue;
    var diff = nextVal - currentValue;
    var step = ( 0 < diff ? 1 : -1 );
    for (var i = 0; i < Math.abs(diff); ++i) {
        setTimeout(function() {
            currentValue += step
            obj.text(currentValue);
        }, 50 * i)   
    }
  }

  function drawprogresoxactividad(id,index){
    if(id != 0){
      var primero = true;
      var obj = $('#chart2');
      var labels = new Array();
      var data_progreso = new Array();

      for(var j in progresos[id][index]['hijo']){
        if(progresos[id][index]['hijo'][j] && parseInt(j) > -1){
          if(progresos[id][index]['hijo'][j]['tiporecurso'] == 'L'){
            labels.push(progresos[id][index]['hijo'][j]['nombre']);
            data_progreso.push(progresos[id][index]['hijo'][j]['progreso']);
          }//end if filtro
        }
      }
      
      chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]
      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
      drawBarHorizontal(obj,chartData);
    }//endif id
  }

  function drawprogresos(id){
    if(id != 0){
      
      //draw total progress
      var progressbar = $('#progress_total').find('.progress-bar');
      var total = 0;
      for(var i in progresos[id]){
        if(progresos[id][i] && parseInt(i) > -1 ){
          total += progresos[id][i]['progreso'];
          // total +=100; 
        }
      }

      total = (total * 100) / (100 * progresos[id].length) ;
      progressbar.css('width',total.toString()+"%");
      countTextObject = $('#progress_total').find('.countPercentage');
      countTextObject.data('value',total);
      countText(countTextObject.find('span'), countTextObject.find('span').text(), total );
      $('#progress_total').find('.titleCursoName').html(nameSelect($('#select_viewTable'),$('#select_viewTable').val()));
      //draw progress unidad
      var obj = $('#chart1');
      var labels = new Array();
      var data_progreso = new Array();
      for(var i in progresos[id]){
        if(progresos[id][i] && parseInt(i) > -1 ){
          labels.push(progresos[id][i]['nombre']);
          data_progreso.push(progresos[id][i]['progreso']);
        }
      }

      var chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]

      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
  		drawBarHorizontal(obj,chartData);
      //draw parte actividad...
      obj = $('#chart2');
      labels = new Array();
      data_progreso = new Array();
      var primero = true;
      var select_unidad = $('#select_viewActivity');
      select_unidad.html('');
      /* recorrido de objeto */
      for(var i in progresos[id]){
        if(primero === true){
          //code here
          for(var j in progresos[id][i]['hijo']){
            if(progresos[id][i]['hijo'][j] && parseInt(j) > -1){
              if(progresos[id][i]['hijo'][j]['tiporecurso'] == 'L'){
                labels.push(progresos[id][i]['hijo'][j]['nombre']);
                data_progreso.push(progresos[id][i]['hijo'][j]['progreso']);
              }//end if filtro
            }
          }
          primero = false;
        }
        if(progresos[id][i]['nombre']){          
          if(progresos[id][i]['nombre']){
            select_unidad.append('<option value="'+i+'">'+progresos[id][i]['nombre']+'</option>');
          }
        }//endif
      }
      
      
      chartData = {
        labels: labels,
        datasets: [{
          backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(255,30,131,0.5)','rgba(0,100,131,0.5)'],
          data: data_progreso,
          borderColor: 'white',
          borderWidth: 2
        }]
      };
      var _ide = obj.attr('id');
      obj.parent().html("").html('<canvas id="'+_ide+'" width="800" height="700" class="chartjs-render-monitor"></canvas>'); //No es la mejor opcion investigar update de la libreria
      obj = document.getElementById(_ide).getContext('2d');
      drawBarHorizontal(obj,chartData);
    }//endif
  }

	console.log(progresos);
	$('document').ready(function(){
		//..
    var id = $('#select_viewTable').val();
    drawprogresos(id);

    $('#select_viewTable').on('change',function(){
      drawprogresos($('#select_viewTable').val());
    });
    $('#select_viewActivity').on('change',function(){
      // drawprogresos($('#select_viewTable').val());
      drawprogresoxactividad($('#select_viewTable').val(),$('#select_viewActivity').val());
    });
	});
</script>