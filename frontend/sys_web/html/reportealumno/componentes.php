<?php defined('RUTA_BASE') or die(); 
$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$idgui = uniqid();


$RUTA_BASE = $this->documento->getUrlBase();
//if(empty($this->datos)) $frm=$this->datos;
$frm=$this->datos;

//echo $this->frmpestanas;
//echo $this->frmnivel;
//
//echo $this->frmaccion."as";

if ($frm['pestanas']){
	$pestanas=explode(",",$frm['pestanas']);
	$cant_pestanas=count($pestanas)-1;
	
}else{
	$cant_pestanas="";
}



?>



<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/componentes/general.css"
>



<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">


<style type="text/css">
  
.border{
  border: solid 0px #f00;
}





</style>




<div class="col-md-12 container">
<div class="row border" id="contenido" style="padding-top: 1ex; ">

	
	
	
					

<form class="form-horizontal" id="frm1" name="frm1" >
<input type="hidden" name="accion" id="accion" value="<?=$this->frmaccion?>">
<input type="text" name="pkIdcom" id="pkIdcom" value="<?=$frm['idnivel']?>">
<input type="text" name="txtpestanas" id="txtpestanas" value="<?=$frm['pestanas']?>">

		
		<div class = 'col-xs-12 col-sm-12 col-md-10 col-lg-10 border'  >
		<div class="form-group">

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			
			<label for="titulo"><?php echo JrTexto::_("Level")?> 1</label>
			<div class="cajaselect " >
				<select name="idnivel" id="idnivel" style="width: 100%; height: 35px" required >
					<option value="" ><?php echo JrTexto::_("Select")?></option>
					<?php
					if(!empty($this->cur_nivel))
					foreach ($this->cur_nivel as $cur_niv){
					?>
					<option value="<?php echo $cur_niv["idnivel"]; ?>" <?php echo $cur_niv["idnivel"]==$frm["id"]?'Selected':'';?>><?php echo $cur_niv["singular"]?></option>
					<?php					
					}//for
					?>
				</select>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

			<label for="titulo"><?php echo JrTexto::_("Status")?></label>
			<div class="cajaselect " >
				<select name="txtest" class="" id="txtest" style="width: 100%; height: 35px" required >						
					<option value="1" <?php echo $frm["estado"]=='1'?'Selected':'';?>>Activo</option>
					<option value="0" <?php echo $frm["estado"]=='2'?'Selected':'';?>>Inactico</option>
									
				</select>
			</div>

		</div>

		


		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<label for="titulo"><?php echo JrTexto::_("Order")?></label>
			<div class="cajaselect " >
				<select name="txtorden" id="txtorden" style="width: 100%; height: 35px" required>
					<option value="" ><?php echo JrTexto::_("Select")?></option>
					<?
					for ($i=1; $i<=10; $i++){
					?>
					<option value="<?=$i?>" <?php echo $frm["orden"]==$i?'Selected':'';?>><?=$i?></option>
					<?
					}//for
					?>
									
				</select>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<label for="titulo"><?php echo JrTexto::_("Quantity")?> <?php echo JrTexto::_("eyelashes")?></label>
			<div class="cajaselect " >
				<select name="txtcant" id="txtcant" style="width: 100%; height: 35px" onchange="cargar_cbo_pestanas()" >
					<option value="" ><?php echo JrTexto::_("Select")?></option>
					<?
					for ($i=1; $i<=10; $i++){
					?>
					<option value="<?=$i?>" <?php echo $cant_pestanas==$i?'Selected':'';?>><?=$i?></option>
					<?
					}//for
					?>
									
				</select>
			</div>
		</div>

		<div id="div_pestanas" class="col-md-12 border">asas
			
		</div>


		<div class="col-md-12">
		<label for="titulo"><?php echo JrTexto::_("Title")?></label>
			<input type="text" name="txttit" id="txttit" class="form-control gris" placeholder="<?php echo JrTexto::_("Add")?> <?php echo JrTexto::_("Title")?>" required value="<?php echo @$frm['nombre']; ?>" style="height: 35px">
		</div>


		</div>
		</div>

		<div class = 'col-xs-12 col-sm-12 col-md-2 col-lg-2 border'  >
			<div class="form-group">


			<a href="#" class="thumbnail btnportada istooltip" data-tipo="image" data-url=".img-portada" title="<?php echo ucfirst(JrTexto::_('Select an image')); ?>">
                <?php
                 $imgSrc=($this->frmaccion=='Nuevo')?$this->documento->getUrlStatic().'/media/web/nofoto.jpg':str_replace('__xRUTABASEx__', $RUTA_BASE, @$frm['imagen']);
                ?>
                <img src="<?php echo $imgSrc; ?>" alt="cover" class="img-responsive img-portada img-thumbnails istooltip">
                <input type="hidden" id="txtFoto" name="txtFoto" value="<?php echo $imgSrc; ?>">
            </a>


			</div>
		</div>

		

		<div class = 'col-xs-12 col-sm-12 col-md-12 col-lg-12 border' style="text-align: center;"  >
		<div class="form-group">
		<a id="btn-saveHoja"  class="btn btn-success" onclick="guardarComp()" ><i class=" fa fa-save"></i>Guardar</a>
		  <a type="button" class="btn btn-warning btn-close" href="<?php echo $this->documento->getUrlBase();?>/componentes"   ><i class=" fa fa-repeat"></i> Cancelar</a>
		</div>
		</div>


		
           
</form>


				


</div>
</div>

<script type="text/javascript">

//$('.istooltip').tooltip();

$(document).ready(function() {

	$('.btnportada').click(function(e){ 
        var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
        selectedfile(e,this,txt);
    });

    $('.btnportada img').load(function() {
        var src=$(this).attr('src');
        $('#txtFoto').val(src);
    });

});

function cargar_cbo_pestanas(){
	
	var cant=$("#txtcant").val();
	var accion =$("#accion").val();
	var pestanas =$("#txtpestanas").val();
	var cant_part = pestanas.split(",");
	//alert(accion+'-'+pestanas);
	var i =0;
	var y =0;
	var z =0;    
   

    var data={'tipo':'2'};    	
    res = xajax__('', 'Componentes', 'getxPadre2',data);
    
    if(res){
    	var z=0;
    	var singular=[];
    	var idnivel=[];
    	$.each(res,function(){
    		x=this;    		
    		z++;
    		singular[z] = x["singular"];
    		idnivel[z] = x["idnivel"];
    		//alert(z);
    		
    	});//each

    }//if res

    $('#div_pestanas').html("");
    
    //alert(cant);
    for (i=1;i<=cant;i++){
    	
    	

    	$('#div_pestanas').append('<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><label for="titulo">Level '+i+'</label><div class="cajaselect " ><select name="txtn['+i+']" id="txtn'+i+'" style="width: 100%; height: 35px" ><option value="" ><?php echo JrTexto::_("Any")?></option>');


    	for (y=1;y<=z;y++){
	    		
	    		if (accion=='Editar'){
	    			var val_check=[];
	    			//alert(y);
		    		
	                    //alert(cant_part[i-1]+'=='+idnivel[y]);
	                    if (cant_part[i-1]==idnivel[y]){
	                        val_check[y]="selected";	                        
	                        //alert(val_check[y]);
	                    }

	            	$('#txtn'+i).append('<option value="'+idnivel[y]+'" '+val_check[y]+'>'+singular[y]+'</option>');

	            }else{

	            	$('#txtn'+i).append('<option value="'+idnivel[y]+'" >'+singular[y]+'</option>');

	            }//


	    		
	    		//alert(x["singular"]);
	   
		}//for

		$('#div_pestanas').append('</select></div></div>');
    	
	}//for
    

}


	//var guardarComp = function() {
	function guardarComp() {
		//console.log("as");

        var val_count=0; 
        $('#frm1 *[required]').each(function(i, elem) {
            var value = $(elem).val();
            if(value=='' || value.trim()==''){
                $(elem).css("border","solid 1px #f00");
                setTimeout (function(){
	            	$(elem).css("border","");	            
	        	}, 3000);
	        	val_count++;
            }
        });

        if (val_count>0){
        	return false;
        }
        
        //alert(_sysUrlBase_+"/frontend/componentes/guardar");

        $.ajax({
            url: _sysUrlBase_+'/componentes/xSaveNivel1',
            type: 'POST',
            dataType: 'json',
            data: $('#frm1').serialize(),
        }).done(function(resp) {
        	
        	//console.log("hola");
            if(resp.code=='ok'){
                var id = resp.data;
                //alert(id);

                mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Data updated') ?>','success');

                setTimeout (function(){

	                //if($('#frm1 #accion').val()=="Nuevo"){
	                    return redir(_sysUrlBase_+'/componentes/');
	                //}
		            
		        }, 1000);

                
                
                
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
            }
        });
        
    }

<?
if ($this->frmaccion=="Editar"){
?>
	cargar_cbo_pestanas();
<?
}
?>



</script>