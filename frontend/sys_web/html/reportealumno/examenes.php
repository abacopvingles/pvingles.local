<?php 
defined("RUTA_BASE") or die(); 
?>
<input type="hidden" />
<style>

</style>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>
<div class="page-title">
    <div class="title_left"><h3></h3>
        <?php echo JrTexto::_('Reporte Notas');?>        
    </div>
</div>

<table class="table table-striped table-responsive">
        <thead>
        <tr class="headings">
            <th>#</th>
            <th><?php echo JrTexto::_("Nombre") ;?></th>
            <?php 
                if(!empty($this->alumnos[0]['notas'])){ $n=1;
              foreach ($this->alumnos[0]['notas'] as $not){ ?>
                <th>N <?php echo $n; $n++; ?></th>
              <?php } }?>
        </tr>
        </thead>
        <tbody>
        <?php $i=0; 
        if(!empty($this->alumnos))
        foreach ($this->alumnos as $reg){ $i++; ?>
        <tr>
            <td><?php echo $i;?></td>
            <td><?php echo @$reg["idalumno"]." - ".$reg["nombre"] ;?></td>
            <?php 
                if(!empty($reg['notas'])){ $n=1;
              foreach ($reg['notas'] as $not){ ?>
            <td><?php echo $not; ?></td>
            <?php } }?>                   
    </tr>
    <?php } ?>
            </tbody>
</table>

<script type="text/javascript">

$('document').ready(function(){
    console.log(_sysIdioma_);
    $('.table').DataTable( {
    "language": {
            "url": _sysUrlStatic_+'/libs/datatable1.10/idiomas/'+_sysIdioma_.toUpperCase() + ".json"
    }
  });

});
</script>