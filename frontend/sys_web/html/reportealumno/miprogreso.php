<style type="text/css">
/*#curso-inicio .slick-slide {
    transition: all 300ms ease;
    opacity: 0.8 !important;
 }
 #curso-inicio .slider.desabilitado > div {
    opacity: 0.5;
    pointer-events: none;
 }
 #curso-inicio .slider.desabilitado .card-nivel{ opacity: 1 !important;}
 #curso-inicio #sesiones_activas .card-nivel{
    margin:0 auto !important;
 }
 #curso-inicio .card-nivel .image img{
    margin-left:-15px;
 }
 /* #curso-inicio #sesiones_activas .card-nivel {
    border-radius: 50%;
    box-shadow: 0px 0px 10px -2px #777;
    width: 183px;
    height: 140px;
} */

.slider{
    margin-top: 1em;
    margin-bottom: 0px;
}
.card-nivel{
    max-width: 240px;
    max-height: 110px;
    border: 1px solid #ccc;
    position: relative;
    display: inline-flex; 
    overflow: hidden;
    background: #f3f3f3;
    z-index: 99;
}

.card-curso{
    max-width: 240px;
    max-height: 110px;
    border: 1px solid #ccc;
    position: relative;
    display: inline-flex; 
    overflow: hidden;
    background: #f3f3f3;
    z-index: 99;
}


.card-nivel2{
    width: 150px;
    position: relative;
}
.card-nivel2 img{
    max-width: 100%
    max-height:150px;
}

.card-nivel2 .nombre{
max-height: 101px;
    overflow: hidden;
    position: absolute;
    bottom: 0px;
    color: black;
    background: #faebd7d6;
    width: 100%;
    left: 0;

}

.card-nivel .imagen,.card-nivel .info{width: 50%; overflow: hidden;}
.lineright{ border-right: 2px solid #aaa; float: left; margin: 0px; padding: 0px; margin-right: -1px;     width: calc(50% + 1px);  }
.lineright::after{
    content: "";
    border-top:2px solid #ccc;
    width: 50%;
    position: absolute;
    top: 60px;
    height: 2px;
    left: 50%;
}
.lineleft{ border-left: 2px solid #aaa; margin-left: -2px;  float: right;  margin: 0px; padding: 0px; margin-left: -1px;     width: calc(50% + 1px);}
.lineleft::before{
    content: "";
    border-top:2px solid #ccc;
    width: 50%;
    position: absolute;
    top: 60px;
    height: 2px;
    right: 50%;

}
.pnlline{ min-height: 110px;}
.card-nivel::after{
    content: "\f118";
    font-family: FontAwesome;
    color: #0F0;
    font-weight: bold;
    font-size: 2.5em;
    padding-right: 0.5ex;
    position: absolute;
    right: 0px;
    bottom: 0px;
}
.card-nivel.locked::after{
    content: "\f119";
    font-family: FontAwesome;
    color: #F00;
    font-size: 2.5em;
    font-weight: bold;
    padding-right: 0.5ex;
    position: absolute;
    right: 0px;
    bottom: 0px;
}
.card-nivel.aquiuser::after{
    content: "\f1ae";
    font-family: FontAwesome;
    color: #00F;
    font-size: 4em;
    font-weight: bold;    
    position: absolute;
    right: 35%;
    bottom: -12px;
}
.faltahoras{
    position: absolute;
    bottom: 0px;
    right: 0px;
    width: 45%;
    text-align: center;
    background: #000;
    color: #fff;
    border-radius: 1ex;
    margin: 1ex;
}
.horasesion{
    position: absolute;
    top: 1ex;
    left: 1ex;
    z-index: auto;
    color: #000;
    font-weight: bold;
    padding: 0.25ex 0.5ex;
    background-color: #f7efef87;
    border-radius: 1ex;
}
.proresosesion{
     position: absolute;
    top: 1ex;
    right: 1ex;
    z-index: auto;
    color: #000;
    font-weight: bold;
    padding: 0.25ex 0.5ex;
    background-color: #f7efef87;
    border-radius: 1ex;
}
.iconok, .iconbad, .iconuser{
    position: absolute;
    z-index: 10057;
    right: 0px;
    bottom: 0px;
    font-size: 2em;
}

.iconok{ color:#0f0; }
.iconbad{ color:#f00; }
.iconuser{ color:#00f; }



 
</style>
<div class="row" id="breadcrumb">
    <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
            <?php foreach ($this->breadcrumb as $b) {
            $enlace = '<li>';
            if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
            else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
            $enlace .= '</li>';
            echo $enlace;
            } ?>
            <li id="showcursobimestre"><?php echo JrTexto::_('Bimestre');?></li>
            <!--li style="float:right" ><a href="#" class="btn btn-xs btn-primary print btnimprimircurso" style="color:#fff;" ><i class="fa fa-print"></i> <?php //echo JrTexto::_('Print Malla');?></a></li-->
            <li style="float:right" ><a href="#" class="showcurriculacomo Bimestre" data-tv="<?php echo JrTexto::_('Show in');?>" data-B="<?php echo JrTexto::_('Bimonthly');?>" data-T="<?php echo JrTexto::_('Quarterly');?>" ><?php echo JrTexto::_('Show in Quarterly');?></a></li>
        </ol>
    </div>
</div>

<?php 
$idcurso=$this->idCurso;
$curso=$this->cursoSel;
$imgcurso=!empty($curso["imagen"])?$curso["imagen"]:$this->documento->getUrlStatic().'/media/imagenes/level_default.png';
$imgcurso=str_replace($this->documento->getUrlBase(),'',$imgcurso);
$imgcurso=$this->documento->getUrlBase().$imgcurso;

function mashijos($hijo,$rb){ 
    $j=0;
    $hayprogreso=true; 
    if(!empty($hijo))
    foreach($hijo as $h){
        $img=$h["imagen"];
        $img=(!empty(@$h['imagen'])?str_replace('__xRUTABASEx__',$rb , $h['imagen']):$rb.'/static/media/imagenes/level_default.png');
        $t=$h["tiporecurso"];
        $txttipoexamen='';
        $tienehijos=array();
        $nhoras=!empty($h["nhoras"])?$h["nhoras"]:'00:45';   
        if($t!='E'){
            $j++;
            $tienehijos=!empty($h["hijo"])?$h["hijo"]:array();
        }else{
            if(!empty($h["txtjson"])){
                $tipoexa=json_decode($h["txtjson"]);
                $txttipoexamen='data-tipoexamen="'.$tipoexa->tipo.'"';
            }
        }
        $progreso=ceil(!empty(@$h['progreso'])?@$h['progreso']:'0').'%';
        $icon='';
        if($progreso>=51){
            $icon='iconok fa fa-smile-o';
            $hayprogreso=false;
        }else{
            if($hayprogreso==false){
                $icon='iconuser fa fa-child';
            }else {               
                $icon='iconbad fa fa-frown-o';
            }
             $hayprogreso=true;
        }


      $htmlhijo='<div class="card-nivel2 thumbnail locked color-black" title="'.$h["nombre"].'" data-idcursodetallerecurso="'.$h["idcursodetalle"].'" data-link="'.@$h["link"].'" data-progreso="'.(!empty(@$h['progreso'])?@$h['progreso']:'0').'" data-tiporecurso="'.$h["tiporecurso"].'" data-horas="'.$nhoras.'" style="width:150px">
    <div class="" style="margin:0px; padding:0px; height:110px;"><img src="'.$img.'" alt="imagen" class="img-resonsive" style="max-height: 100px; max-width:100%;     display: inline;" data-horas="'.(@!empty($h["nhoras"])?$h["nhoras"]:'').'"></div>
    <div class="horasesion">'.'('.$nhoras.')'.'</div>
    <div class="proresosesion">'.$progreso.'</div>
    <div class="col-xs-12 nombre-contenedor" style="bottom:0px; Width:100%;">
        <div class="nombre" style="max-height:50px; overflow:hidden; ">
            <h5 class="col-xs-12 text-center" style="padding:0.5ex;">'.((@$t!='E'?@$j.'. ':'<i class="btn-icon fa fa-list" style="float:left;"></i> ').$h['nombre']).'</h5>
            <i class="'.$icon.'"></i>
        </div>
    </div>'.(!empty($tienehijos)?('<div class="hijos" style="display:none;">'.mashijos($tienehijos,$rb,$idrol).'</div>'):'').'</div>';
    
    echo $htmlhijo;
    }
}
?>
<div class="row">
    <div class="col-md-12 text-center">
        <div data-link="" class="card-curso" title="A1 Beginning Test " data-idcursodetallerecurso="" data-tiporecurso="" data-horas=""> 
            <div class="imagen text-center" style="width: 100%">
                <img src="<?php echo $imgcurso; ?>" alt="imagen"  style="max-width: 100%; display:inline-flex" >
            </div>                 
            <div class="info text-center" style="width: 100%">
                <span class="nombre"><h3><?php echo $curso["nombre"] ?></h3></span><br>                             
                <small class="thoras"></small>
            </div>
        </div>
    </div>
    <?php 
        $i=0;
        $isLocked=false;
        if(!empty($this->newsylabus)){
            $num_b = 1;
            $num_t = 1;
    $j=0;
        foreach($this->newsylabus as $idet){ 
            $img=$idet["imagen"];
            $t=$idet["tiporecurso"];
            $txttipoexamen='';
            $tienehijos=array();
            if($t!='E'){
                $i++;
                if(!empty($idet["hijo"]))
                $tienehijos=$idet["hijo"];
                $nhoras='00:00';
                        $hh1=0;
                        $mm1=0;
                        $nses=0;
                if($idet["tiporecurso"]=='U'){
                    foreach ($idet["hijo"] as $h){                              
                        $h2=explode(":", $h["nhoras"]);
                        $hh2=intval(!empty($h2[0])?$h2[0]:'00');
                        $mm2=intval(!empty($h2[1])?$h2[1]:'00');
                        $hh1=$hh1+$hh2;
                        $mm1=$mm1+$mm2;
                        if($h["tiporecurso"]=='L')$nses++;
                    }
                    if($mm1>=60){
                        $hh1=$hh1+floor($mm1/60);
                        $mm1=$mm1%60;
                    }
                    $idet["nsesiones"]=$nses;
                    $idet["nhoras"]=str_pad($hh1, 2, "0", STR_PAD_LEFT).":".str_pad($mm1, 2, "0", STR_PAD_LEFT);
                }


            }else{
                $idet["nhoras"]=!empty($idet["nhoras"])?$idet["nhoras"]:'00:45';
                //$idet["nhoras"]='';
                if(!empty($idet["txtjson"])){
                    $tipoexa=json_decode($idet["txtjson"]);
                    $txttipoexamen='data-tipoexamen="'.$tipoexa->tipo.'"';
                    $idet['link'] = "{$idet['link']}&tipoexamen={$tipoexa->tipo}&idcurso={$this->idCurso}";
                    if(strcasecmp($tipoexa->tipo,'B') == 0){
                        $idet['link'] = "{$idet['link']}&numexamen={$num_b}";
                        $num_b++;
                    }elseif(strcasecmp($tipoexa->tipo,'T') == 0){
                        $idet['link'] = "{$idet['link']}&numexamen={$num_t}";
                        $num_t++;
                    }
                }
            }
            $j++;
            ?>

    <div class="col-md-12 text-center verislocked <?php echo $t=="E"?'showestosexamen':''; ?> viendolo" <?php echo $txttipoexamen; ?> ><div class="row">
        <div class="col-md-6 text-center">
            <div data-link="" class="card-nivel locked" title="<?php echo $idet["nombre"] ?>" data-idcursodetallerecurso="" data-tiporecurso="" 
            data-horas="<?php echo !empty($idet["nhoras"])?('('.$idet["nhoras"].')'):''; ?>" 
            data-progreso="<?php echo !empty($idet['progreso'])?$idet['progreso']:'0'; ?>" > 
                <div class="imagen text-center">
                    <img src="<?php echo !empty(@$idet['imagen'])?str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $idet['imagen']):$this->documento->getUrlStatic().'/media/imagenes/level_default.png'; ?>" alt="imagen" class="img-resonsive" style="max-height: 100%; "  >
                </div>                 
                <div class="info text-center">
                    <span class="nombre"><b><?php echo $idet["nombre"] ?></b></span><br>                             
                    <small class="thoras"><?php echo !empty($idet["nhoras"])?('('.$idet["nhoras"].')'):''; ?></small>
                </div>
            </div>
                <?php if(!empty($tienehijos)){?>
                <div class="slider hijos">
                    <?php  mashijos($tienehijos,$this->documento->getUrlBase()); ?>
                    </div>
                <?php }?>
        </div>

    </div></div>
<?php }} ?>
</div>
<br><br>
<div class="panel"><br><br>
    <table class="table table-responsive">
        <tr><th><i class="fa fa-smile-o" style="color:#0f0; font-size: 2em"></i></th><td><?php echo JrTexto::_('Finished') ?></td></tr>
        <tr><th><i class="fa fa-child" style="color:#00f; font-size: 2em"></i></th><td><?php echo JrTexto::_('Where I am') ?></td></tr>
        <tr><th><i class="fa fa-frown-o" style="color:#f00; font-size: 2em"></i></th><td><?php echo JrTexto::_('I have not finished') ?></td></tr>
    </table>
</div>

<script type="text/javascript">
const MINIMO_PARA_PASAR = 52;
var idcurso = <?php echo @$idcurso ?>;
var esRolAdmin = <?php echo ($this->tieneRolAdmin)?'true':'false'; ?>;
var tieneExamUbicacion = <?php echo !empty($this->examen_ubicacion)?'true':'false'; ?>;
var tieneExamUbicacion_resultado = <?php echo !empty(@$this->examen_ubicacion['resultado'])?'true':'false'; ?>;
var idrol=parseInt(<?php echo $this->usuarioAct["idrol"];?>);
var test=<?php echo !empty($this->Test)?json_encode(($this->Test)):'[]'?>;
var iniciarSlider = function(elemSlider, slideInicio=0){    
};

$(document).ready(function() {
    
    var valorprogreso=MINIMO_PARA_PASAR;

    if(test.length>0){
        $.each(test,function(i,v){          
            var div=$('.card-nivel[data-idcursodetallerecurso="'+v.idrecurso+'"]');
            var eshijo=div.parent().hasClass('hijos');
            var tienehijos=div.children('.hijos');
            //console.log(div,v,eshijo,tienehijos);
            if(tienehijos.length){// alfinal del hijo

                 html='<div class="card-nivel thumbnail  color-black istest" title="'+v.titulo+'" data-idcursodetallerecurso="'+v.idtest+'" data-link="'+_sysUrlBase_+'/test/resolver?idtest='+v.idtest+'&idasignacion='+v.idasignacion+'" data-progreso="0" data-tipo="t" data-tiporecurso="T">';
                 html+='  <div class="col-xs-12 image"><img src="'+_sysUrlBase_+v.imagen+'" alt="imagen" class="center-block"></div>';

                 html+='<div class="col-xs-12 col-md-12 barra-progreso" style="bottom:0px; Width:100%;" >';
                 html+='  <div class="col-xs-12"><div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">';
                 html+='</div></div></div> <div class=""> <div class="porcentaje">0%</div></div></div>';
                 html+='<div class="col-xs-12 nombre-contenedor" style="bottom:0px; Width:100%;"><div class="col-xs-12 col-md-12  nombre"><h5 class="col-xs-12 text-center" style="padding:0.5ex;"><i class="btn-icon fa fa-list" style="float:left;"></i> '+v.titulo+'</h5></div></div></div></div>';
                 tienehijos.append(html);
            }else{
                
             html='<div class="col-xs-12 col-sm-6 col-md-3" data-progreso="0"><div data-link="'+_sysUrlBase_+'/test/resolver?idtest='+v.idtest+'&idasignacion='+v.idasignacion+'" class="card-nivel istest" title="'+v.titulo+'" data-idcursodetallerecurso="'+v.idtest+'" data-tipo="t" data-tiporecurso="T"><div class="col-xs-12 image" style="margin:1ex 0px;"><img src="'+_sysUrlBase_+v.imagen+'" alt="imagen" class="center-block"></div> <div class="col-xs-12 col-md-12 barra-progreso" style="bottom:0px; Width:100%;"><div class=""> <div class="progress" style="background: #af9f887d;"> <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div></div><div class="sr-only"><div class="porcentaje">0%</div></div></div><div class="nombre-contenedor" style="bottom:0px; Width:100%;"><div class="col-xs-12 col-md-12  nombre"><h5 class="col-xs-12 text-center" style="padding:0.5ex;"><i class="btn-icon fa fa-file-text-o" style="float:left;"></i>'+v.titulo+'</h5></div></div></div></div>';
                 //siguinte hermano
                div.parent().after(html);

            }
        })
    }

    var activarcards=function(){
        var $islocked=$('.viendolo');
        var desbloquear=true;
        $islocked.each(function(i,t){
            var _card=$(t).children('div').children('div').children('.card-nivel');           
            if(desbloquear==true){
                _card.removeClass('locked');
                var p=parseInt(_card.attr('data-progreso'));
                console.log(p,valorprogreso,desbloquear);
                if(p<valorprogreso){
                    _card.addClass('aquiuser');
                   _card.append('<div class="faltahoras">Falta horas<br><span></span></div>');
                    desbloquear=false;
                }
                if(idrol==1) desbloquear=true;
            }else {_card.addClass('locked'); }            
            //console.log(_card,p,valorprogreso,desbloquear,idrol);
        })
    }
    $('.showcurriculacomo').click(function(ev){
        $(this).toggleClass('Bimestre');
        if($(this).hasClass('Bimestre')){            
            $(this).addClass('Bimestre').text($(this).attr('data-Tv')+' '+$(this).attr('data-T'));
            $('#showcursobimestre').text($(this).attr('data-B'));
            $('.showestosexamen[data-tipoexamen="T"]').hide().removeClass('viendolo');
            $('.showestosexamen[data-tipoexamen="B"]').show().addClass('viendolo');                   
        }else{
            $(this).removeClass('Bimestre').text($(this).attr('data-Tv')+' '+$(this).attr('data-B'));
            $('#showcursobimestre').text($(this).attr('data-T'));
            $('.showestosexamen[data-tipoexamen="T"]').show().addClass('viendolo');
            $('.showestosexamen[data-tipoexamen="B"]').hide().removeClass('viendolo');           
        }
        diseniarcards();
       
    })

    var diseniarcards=function(){
        var crads=$('.viendolo');
        $.each(crads,function(i,v){
            var _card=$(this).children('.row').children('div');
            var j=i%2;
            if(j==0) _card.removeClass('lineleft').addClass('lineright');
            else _card.removeClass('lineright').addClass('lineleft');
        })
        activarcards();
    }
    

    var iniciar=function(){
        var esbimestre=$('.showcurriculacomo').hasClass('Bimestre');
        if(esbimestre){        
            $('.showestosexamen[data-tipoexamen="T"]').hide().removeClass('viendolo');
            $('.showestosexamen[data-tipoexamen="B"]').show().addClass('viendolo');
        }else{
            $('.showestosexamen[data-tipoexamen="T"]').show().addClass('viendolo');
            $('.showestosexamen[data-tipoexamen="B"]').hide().removeClass('viendolo');
        }
        diseniarcards();        
    }

    iniciar();




    /*================= Al iniciar =================*/
    /*function mostrarcurso(_card){
        let link=_card.attr('data-link')||'';
        if(_card.hasClass('istest')){
            var fcall='refreshdatos'+Date.now();
            var url=link
            if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
            else url+='?fcall='+fcall;
              var ventana='<?php echo JrTexto::_('Test') ?>';
              var claseid=ventana+'_'+Date.now();
              var titulo=ventana;
              url+='&plt=modal';
              $idtmp='pnl2_'+Date.now();              
              openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true});
        }else redir(link);
    }*/

    /*================== Eventos ===================*/
   /* $('.slider').on('click', '.card-nivel', function(e) {
        if($(this).hasClass('locked')){ return false; }
        var id = $(this).attr('data-idcursodetallerecurso');
        var _card=$(this);
        var tienehijos=$(this).children('.hijos');
        if(tienehijos.length>0){
            _card.addClass('card-selected');
            _card.parent().addClass('hidden');
            _card.closest('.slider').addClass('desabilitado');
            $('#unidad_activa_sesiones').removeClass('hidden');
            _mostrarunidadActiva=$('#unidad_activa_sesiones #unidad_activa');
            _mostrarunidadActiva.html('').append(_card.clone());
            _mostrarsesionesActiva=$('#unidad_activa_sesiones #sesiones_activas #slider-sesion');
            _hijostmp=tienehijos.children('.card-nivel').clone();
            if(_mostrarsesionesActiva.hasClass('slick-initialized')){ 
                _mostrarsesionesActiva.slick('unslick'); 
            }
            _mostrarsesionesActiva.html('');
            let desbloquear=true;
            $.each(_hijostmp,function(i,v){
                el=$(v);
                el.addClass('hvr-sink');
                el.find('.nombre-contenedor').addClass('hidden');
                el.find('.barra-progreso .sr-only').removeClass('sr-only');
                if(desbloquear==true||idrol!=3)el.removeClass('locked');
                let p=parseInt(el.attr('data-progreso'));
                if(p<valorprogreso)desbloquear=false;               
                _mostrarsesionesActiva.append($('<div class="slider-item" style="min-height: 199px; position:relative;"></div>').append(el));
            })
            //_mostrarsesionesActiva.find('.card-nivel')
            indiceInicio=0;//empieza el bloqueo; 
            iniciarSlider(_mostrarsesionesActiva, indiceInicio);
        }else{
            mostrarcurso(_card);
        }
    });*/

   /* $('#slider-sesion').on('mouseenter', '.card-nivel', function(e) {
        var name = $(this).find('.nombre').text().trim();
        $(this).parent().prepend('<div class="col-xs-12 text-center bolder name-on-hover" style="width: 100%;font-size:1.3em;  z-index: 99;">'+ name +'</div>').fadeIn();
        $(this).parent().append('<div class="col-xs-12 text-center bolder name-on-hover" style="top:auto; bottom:1ex; width: 100%;font-size:1em;  z-index: 99;">'+($(this).attr('data-horas')||'')+'</div>').fadeIn();
    }).on('mouseleave', '.card-nivel', function(e) {
        var name = $(this).attr('title');
        $(this).parent().find('.name-on-hover').fadeOut().remove();
    });*/

    /*$('#unidad_activa_sesiones .cerrar').click(function(e) {
        e.preventDefault();
        $(this).closest('#unidad_activa_sesiones').addClass('hidden');
        $sliderDesabilitado = $('.desabilitado');
        $sliderDesabilitado.find('.card-selected').parent().removeClass('hidden');
        $sliderDesabilitado.find('.card-selected').removeClass('card-selected');
        $sliderDesabilitado.removeClass('desabilitado');
    });*/
   /* $('.btnimprimircurso').on('click',function(ev){
        var htmlmp=$('#curso-inicio').children('.slider').children('.verislocked.viendolo');
        var html='<ul style="list-style-type: none;">';
        var color={0:'#e2ae33',1:'#101010',2:'#efeeeed1',3:'#e9e7f3',4:'#087508c9'};
        $.each(htmlmp,function(i,v){
            $nv=$(v).children('.card-nivel');
            var nom=$nv.children('.nombre-contenedor');            
            var nhorasu=$nv.attr('data-horas')||'';
            var tiporecurso=$nv.attr('data-tiporecurso')||'L';
            var nomtxt=nom.children('.nombre').children('h5').clone();          
            nomtxt.children('.nhoras').remove();           
            var tmpcolor=tiporecurso=='E'?('color:'+color[0]+';'):'';
            tmpcolor=tiporecurso=='U'?('color:'+color[1]+';font-weight: bold;'):tmpcolor;
            tmpcolor=tiporecurso=='T'?('color:'+color[4]+';font-weight: bold;'):tmpcolor;

            html+='<li style="'+tmpcolor+' padding:0.5ex;">'+nomtxt.text()+'<span class="" style="float:right"><b>'+(nhorasu!=''?(nhorasu):'')+'</b></span></li>';
            if(nom.siblings('.hijos').length){
                html+='<ul style="list-style-type: none;">';
                nom.siblings('.hijos').children('.card-nivel').each(function(ii,vv){
                    var nom2=$(vv).children('.nombre-contenedor').find('h5').clone();
                    var nhoras=$(vv).attr('data-horas')||'';
                    var tiporecurso=$(vv).attr('data-tiporecurso')||'L';
                    var tmpcolor=tiporecurso=='E'?('background:'+color[((ii)%2)+2]+'; color:'+color[0]+';'):('background:'+color[((ii)%2)+2]+';');
                    tmpcolor=tiporecurso=='T'?(tmpcolor+'color:'+color[4]+';font-weight: bold;'):tmpcolor;
                    html+='<li style="padding:0.3ex; '+tmpcolor+'">'+nom2.text()+' <span class="" style="float:right">'+(nhoras!=''?('( '+nhoras+' )'):'')+'</span></li>';
                })
                html+='</ul>';
            }
        })
        html+='</ul>';
        html+='<div class="text-center hidden-print no-print "><a class="imprimir btn btn-sm btn-primary ">Imprimir</a></div>';
        var idmodal=sysmodal({titulo:"Malla Curricular",htmltxt:html})
        idmodal.on('click','.imprimir',function(){
            idmodal.find('.modal-body').imprimir();
        });
    })*/
    var sliderOpts = {
        /*centerMode: true,
        initialSlide: slideInicio,*/
        infinite: false,
        focusOnSelect: true,
        centerPadding: '0px',
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 880,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 720,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    /*centerMode: true,
                    arrows: false,
                    initialSlide: slideInicio,*/
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]
    };
    $('.slider').slick(sliderOpts);
});
</script>