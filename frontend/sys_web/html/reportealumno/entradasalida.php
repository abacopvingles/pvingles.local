<?php 

defined('RUTA_BASE') or die(); 
$idgui = uniqid();

$url = $this->documento->getUrlBase();

$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$entrada = ($this->entrada) ? 'true' : 'false';
$totalCursos = count($this->cursos);

$json_infoEntrada = json_encode($this->infoEntrada);
$json_infoSalida = json_encode($this->infoSalida);
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }
</style>
<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportealumno" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>
<!--START CONTAINER-->
<div class="container">


<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <!-- <a class="btn btn-primary" href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/entrada">Ver examen de Entrada </a>
  <a class="btn btn-danger" href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/salida">Ver examen de Salida </a> -->
  <div style="margin:5px 0;"></div>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>
      <li class="active">
        <?php echo ($this->entrada == true) ? JrTexto::_("Beginning and Final Test Comparison") : JrTexto::_("Reporte de Exámenes de Salida"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Report");?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >

      <h3 style="font-weight:bold;"><?php echo ($this->entrada == true) ? JrTexto::_("Beginning and final test comparison report") : JrTexto::_("Notas de Salida del Estudiante"); 
      ?></h3>
      <div class="container">
        <div class="row" style="margin:15px auto;">
          <div class="col-md-4" style="/*height:90px;*/">
            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
            <h2><?php echo $this->fullName ?></h2>
          </div>
          <div class="col-md-8">
            <div class="" style="text-align:left;">
              <div class="chart-container" style="position: relative; margin:0 auto; /*height:1%;*/ width:50%;">
                <canvas id="chart-area"  class="chartjs-render-monitor" ></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="table-responsive">
        <table id="tablaExamenesE" class="table table-bordered table-hover">
          <input type="hidden" id="urlBase" value="<?php echo $url?>" />
          <thead >
            <th class="text-center"><?php echo JrTexto::_("Course"); ?></th>
            <th class="text-center"><?php echo ($this->entrada == true) ? JrTexto::_("Beginning test score") : JrTexto::_("Final test score"); ?></th>
            <th class="text-center"><?php echo JrTexto::_("Listening"); ?></th>
            <th class="text-center"><?php echo JrTexto::_("Reading"); ?></th>
            <th class="text-center"><?php echo JrTexto::_("Writing"); ?></th>
            <th class="text-center"><?php echo JrTexto::_("Speaking"); ?></th>
          </thead>
          <tbody style="font-weight:bold;">
            <?php 
              if(!empty($this->cursos)){
                $keyExamen = $this->entrada == true ? 'E' : 'S';
                $first = true;          
                foreach ($this->cursos as $key => $lista_nivel){
                  $nombre=$lista_nivel['nombre'];
                  $idnivel=$lista_nivel['idcurso'];
                  $_nota = ($this->entrada == true) ? intval($this->Total[$idnivel][$keyExamen][0]) : intval($this->Total[$idnivel][$keyExamen][count($this->Total[$idnivel][$keyExamen]) - 1]);
                  $nota =  $_nota > 100 ? ( ( $_nota / ((ceil($_nota / 100) ) * 100) ) * 100 ) *0.20 : $_nota * 0.20;
                  $nota = intval(floor($nota));//round($nota,2);
                  $_nota2 = intval($this->Total[$idnivel]['S'][count($this->Total[$idnivel]['S'])-1]);
                  $nota2 =  $_nota2 > 100 ?  20 : $_nota2 * 0.20;
                  $nota2 = intval(floor($nota2));//round($nota2,2);
                  //Imprimir tabla
                  echo "<tr data-id='".$idnivel."'>";
                  if($first == true){
                    echo "<td rowspan='3'>{$nombre}</td><td>{$nota} pts / 20 pts</td>";
                    $first = false;
                  }else{
                    echo "<td rowspan='3'>{$nombre}</td><td>".JrTexto::_("Beginning test score")."<br> {$nota} pts / 20 pts</td>";
                  }
                  echo "<td><div class='countPercentage' data-id='4' data-value='".$this->TotalHab[$idnivel]['4']."'><span>0</span>%</div><input type='hidden' data-id='4' value='".$this->TotalHabContrario[$idnivel]['4']."' /></td>";
                  echo "<td><div class='countPercentage' data-id='5' data-value='".$this->TotalHab[$idnivel]['5']."'><span>0</span>%</div><input type='hidden' data-id='5' value='".$this->TotalHabContrario[$idnivel]['5']."' /></td>";
                  echo "<td><div class='countPercentage' data-id='6' data-value='".$this->TotalHab[$idnivel]['6']."'><span>0</span>%</div><input type='hidden' data-id='6' value='".$this->TotalHabContrario[$idnivel]['6']."' /></td>";
                  echo "<td><div class='countPercentage' data-id='7' data-value='".$this->TotalHab[$idnivel]['7']."'><span>0</span>%</div><input type='hidden' data-id='7' value='".$this->TotalHabContrario[$idnivel]['7']."' /></td>";
                  echo "<tr><td>".JrTexto::_('Final Test Score')."</td><td>Listening</td><td>Reading</td><td>Writing</td><td>Speaking</td></tr>";
                  echo "<tr><td>{$nota2} pts/ 20 pts</td><td>{$this->TotalHabContrario[$idnivel]['4']}%</td><td>{$this->TotalHabContrario[$idnivel]['5']}%</td><td>{$this->TotalHabContrario[$idnivel]['6']}%</td><td>{$this->TotalHabContrario[$idnivel]['7']}%</td></tr></tr>";
                }
              }
            ?>
          </tbody>
        </table>
      </div>

    </div>
    <div class="panel panel-warning" style="margin: 8px;">
      <div class="panel-heading" style="text-align: left;">
        <?php echo JrTexto::_("Comparison");?>
      </div>
      <div class="panel-body">
        <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('Courses'); ?></p>
        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
            <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
            <option value=''><?php echo JrTexto::_('Select'); ?></option>
              <?php
                if(!empty($this->cursos)){               
                  foreach ($this->cursos as $key => $lista_nivel){
                    $nombre=$lista_nivel['nombre'];
                    $idnivel=$lista_nivel['idcurso'];
                    echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                  }
                }
              ?>
            </select>
        </div>
        <div class="row" style="padding-top:10px;">
          <div class="col-md-12">
            <div class="chart-container" id="comparativo-container" style="position: relative; margin:0 auto; height:100%; width:100%">
              <canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>
            </div>
          </div>
          <?php if($this->entrada == true): ?>
          <?php else : ?>
          <?php endif; ?>
          <div class="col-md-6 chart-container" style="position: relative; margin:0 auto; ">
            <div style="border:1px solid black; border-radius:1em; text-align:center;">
                <?php echo ($this->entrada == true) ? "<h4>".JrTexto::_('Beginning Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_entrada'></span></p>" : "<h4>".JrTexto::_('Final Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_salida'></span>"; ?>
                <?php echo ($this->entrada == true) ? '<canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas>' : '<canvas id="radar2" style="margin-bottom:5px;"></canvas>'; ?>
                <div class="row" style="padding:0 20px;">
                  <div class="col-sm-3 bg bg-primary text-center" style="border-radius:0.3em;"><h4 class="badge badge-primary" style="width:100%;">Listening</h4> <h2 class="VL1">0 %</h2></div>
                  <div class="col-sm-3 bg bg-info text-center" style="border-radius:0.3em;"><h4 class="badge badge-secondary" style="width:100%;">Reading</h4> <h2 class="VR1">0 %</h2></div>
                  <div class="col-sm-3 bg bg-warning text-center" style="border-radius:0.3em;"><h4 class="badge badge-warning" style="width:100%;">Writing</h4> <h2 class="VW1">0 %</h2></div>
                  <div class="col-sm-3 bg bg-danger text-center" style="border-radius:0.3em;"><h4 class="badge badge-danger" style="width:100%;">Speaking</h4> <h2 class="VS1">0 %</h2></div>

                </div>
            </div>
          </div>
          <div class="col-md-6 chart-container" style="position: relative; margin:0 auto; ">
            <div style="border:1px solid black; border-radius:1em; text-align:center;">
                <?php echo ($this->entrada == false) ? "<h4>".JrTexto::_('Beginning Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_entrada'></span></p>" : "<h4>".JrTexto::_('Final Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_salida'></span>"; ?>
                <?php echo ($this->entrada == false) ? '<canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas>' : '<canvas id="radar2" style="margin-bottom:5px;"></canvas>'; ?>
                <div class="row" style="padding:0 20px;">
                  <div class="col-sm-3 bg bg-primary text-center" style="border-radius:0.3em;"><h4 class="badge badge-primary" style="width:100%;">Listening</h4> <h2 class="VL2">0 %</h2></div>
                  <div class="col-sm-3 bg bg-info text-center" style="border-radius:0.3em;"><h4 class="badge badge-secondary" style="width:100%;">Reading</h4> <h2 class="VR2">0 %</h2></div>
                  <div class="col-sm-3 bg bg-warning text-center" style="border-radius:0.3em;"><h4 class="badge badge-warning" style="width:100%;">Writing</h4> <h2 class="VW2">0 %</h2></div>
                  <div class="col-sm-3 bg bg-danger text-center" style="border-radius:0.3em;"><h4 class="badge badge-danger" style="width:100%;">Speaking</h4> <h2 class="VS2">0 %</h2></div>
                </div>
                <!-- <h4>Nota de Salida</h4>
                <canvas id="radar2" style="margin-bottom:5px;"></canvas> -->
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!--END CONTAINER-->

<script type="text/javascript">
var entrada = <?php echo $entrada ?>;
var infoEntrada = <?php echo !empty($json_infoEntrada) ? $json_infoEntrada : 'null' ?>;
var infoSalida = <?php echo !empty($json_infoSalida) ?  $json_infoSalida : 'null' ?>;

function countText(obj,valueText, ToValue){
  var currentValue = parseInt(valueText);
  var nextVal = ToValue;
  var diff = nextVal - currentValue;
  var step = ( 0 < diff ? 1 : -1 );
  for (var i = 0; i < Math.abs(diff); ++i) {
      setTimeout(function() {
          currentValue += step
          obj.text(currentValue);
      }, 100 * i)   
  }
}

function drawArea(obj, _data){
  new Chart(obj, {
      type: 'polarArea',
      data: {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
          label: "Habilidades obtenidas por el Examen",
          data: _data,
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          label: 'My dataset' // for legend
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Skills statistics obtained by the exam'
        },
        responsive: true,
				legend: {
					position: 'right',
				},
				scale: {
					ticks: {
						beginAtZero: true
					},
					reverse: false
				},
				animation: {
					animateRotate: false,
					animateScale: true
				}
      }
  });
}

function drawChart(obj,dat,texto  = 'Beginning and Final test comparison' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: false,
      title: {
        display: true,
        text: texto
      },
      tooltips: {
        // mode: 'index',
        // intersect: true
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            // console.log(tooltipModel.body[0].lines[0]);
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Difference');
            }
          }
        }
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
        legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          var meta = ci.getDatasetMeta(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }

          ci.update();

        }
      }//end legend
    }
  });
}

function drawRadar(obj, _data, color = "#3e95cd" ,isEntrada = true,_title = "Comparison of exams"){
  isEntrada = isEntrada == false ? "Final" : "Beginning";
  new Chart(obj, {
      type: 'radar',
      data: {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
          label: "skills obtained in the course",
          data: _data,
          backgroundColor: color + "7a",
          pointBackgroundColor: color,
          borderColor: color,
          label: isEntrada // for legend
        }]
      },
      options: {
        title: {
          display: true,
          text: _title
        },
        responsive: true,
				legend: {
					position: 'top',
				},
				scale: {
					ticks: {
						beginAtZero: true
					}
				},
				animation: {
					animateRotate: false,
					animateScale: true
				}
      }
  });
}

function pintarComparativa(id = null){
  
  var _info1 = (infoEntrada[id] != null) ? infoEntrada[id] : infoEntrada[Object.keys(infoEntrada)[0]];
  var _info2 = (infoSalida[id] != null) ? infoSalida[id] : infoSalida[Object.keys(infoSalida)[0]];
  $('#fecha_entrada').text(_info1);
  $('#fecha_salida').text(_info2);
  var datosRadar = new Array();
  datosRadar[0] = 0;
  datosRadar[1] = 0;
  datosRadar[2] = 0;
  datosRadar[3] = 0;

  var datosRadar2 = new Array();
  datosRadar2[0] = 0;
  datosRadar2[1] = 0;
  datosRadar2[2] = 0;
  datosRadar2[3] = 0;

  //Sumar habilidades en la comparativa
  $('#tablaExamenesE').find('tr').each(function(key,value){
    var comparativa = id == null ? (key == 1) : ($(this).data('id') == id);
    if(comparativa == true){
      $.each($(this).find('.countPercentage'),function(k,v){
        var idHab = parseInt($(this).data('id'));
        switch(idHab){
          case 4: datosRadar[0] += parseFloat($(this).data('value'));
          break;
          case 5: datosRadar[1] += parseFloat($(this).data('value'));
          break;
          case 6: datosRadar[2] += parseFloat($(this).data('value'));
          break;
          case 7: datosRadar[3] += parseFloat($(this).data('value'));
          break;
        }
      });
    }
  });
  //Sumar habilidades contraria en la comparativa
  $('#tablaExamenesE').find('tr').eq(0).find('td input').each(function(key, value){
    var idHab = parseInt($(this).data('id')); 
    switch(idHab){
      case 4: datosRadar2[0] += parseFloat($(this).val());
      break;
      case 5: datosRadar2[1] += parseFloat($(this).val());
      break;
      case 6: datosRadar2[2] += parseFloat($(this).val());
      break;
      case 7: datosRadar2[3] += parseFloat($(this).val());
      break;
    }
  });
  $('#tablaExamenesE').find('tr').each(function(key,value){
    var comparativa = id == null ? (key == 1) : ($(this).data('id') == id);

    if(comparativa == true){

      $.each($(this).find('input[type=hidden]'),function(k,v){
        var idHab = parseInt($(this).data('id'));
        switch(idHab){
          case 4: datosRadar2[0] += parseFloat($(this).val());
          break;
          case 5: datosRadar2[1] += parseFloat($(this).val());
          break;
          case 6: datosRadar2[2] += parseFloat($(this).val());
          break;
          case 7: datosRadar2[3] += parseFloat($(this).val());
          break;
        }
      });
    }
  });

  //Dibujar charts Radar
  if(entrada == true){
    $('.VL1').text(datosRadar[0].toString()+' %');
    $('.VR1').text(datosRadar[1].toString()+' %');
    $('.VW1').text(datosRadar[2].toString()+' %');
    $('.VS1').text(datosRadar[3].toString()+' %');
    drawRadar(document.getElementById("radar1"),datosRadar);
    $('.VL2').text(datosRadar2[0].toString()+' %');
    $('.VR2').text(datosRadar2[1].toString()+' %');
    $('.VW2').text(datosRadar2[2].toString()+' %');
    $('.VS2').text(datosRadar2[3].toString()+' %');
    drawRadar(document.getElementById("radar2"),datosRadar2,"#d85050",false);
  }else{
    drawRadar(document.getElementById("radar1"),datosRadar2);
    drawRadar(document.getElementById("radar2"),datosRadar,"#d85050",false);
  }
  //dibujar comparativa2
  var datosRadar3 = new Array();
  datosRadar3[0] = 0;
  datosRadar3[1] = 0;
  datosRadar3[2] = 0;
  datosRadar3[3] = 0;
  var datosRadar4 = new Array();
  datosRadar4[0] = 0;
  datosRadar4[1] = 0;
  datosRadar4[2] = 0;
  datosRadar4[3] = 0;
  //datosRadar = entrada . datosRadar2 = salida
  for(var i = 0; i < 4; i++){
    if(datosRadar[i] > datosRadar2[i]){
      datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
    }else if(datosRadar[i] < datosRadar2[i]){
      datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
    }
  }
  
  var stackdifference = 'Stack 1';
  var barChartData = {
    labels: ["Listening", "Reading", "Writing", "Speaking"],
    datasets: [{
        label: 'Beginning Test',
        backgroundColor: '#36a2eb',
        stack:'Stack 0',
        data: [
          datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
        ]
    }, {
        label: 'Final Test',
        backgroundColor: '#ff6384',
        stack:'Stack 1',
        data: [
          datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
        ]
    },{
        label: 'Difference',
        backgroundColor: '#d4b02f',
        stack: 'Stack 0',
        data: [
          Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
        ]
    },{
        label: 'hide',
        backgroundColor: '#d4b02f',
        stack: 'Stack 1',
        
        data: [
            Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
        ]
    }]

  };
  $('#comparativo-container').html("").html('<canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>'); //No es la mejor opcion investigar update de la
  drawChart("comparativo",barChartData);
}

$('document').ready(function(){
  //Dibujar contador de porcentaje de habilidades
  $('.countPercentage').each(function(key, value){
      countText($(this).find('span'), $(this).find('span').text(),parseInt($(this).data('value')) );
  });
  var datos = new Array();
  datos[0] = 0;
  datos[1] = 0;
  datos[2] = 0;
  datos[3] = 0;
  
  
  $('.countPercentage').each(function(key, value){
    var idHab = parseInt($(this).data('id')); 
    switch(idHab){
      case 4: datos[0] += parseFloat($(this).data('value'));
      break;
      case 5: datos[1] += parseFloat($(this).data('value'));
      break;
      case 6: datos[2] += parseFloat($(this).data('value'));
      break;
      case 7: datos[3] += parseFloat($(this).data('value'));
      break;
    }
  });
  //limitar al 100%
  var totalCursos = <?php echo $totalCursos ?>;
  if(totalCursos > 1){
    for(var i = 0; i < datos.length; i++){
      datos[i] = (datos[i] / (100 * totalCursos)) * 100;
    }
  }
  //Dibujar charts Area
  drawArea(document.getElementById("chart-area"),datos);

  //Dibujar comparativa
  pintarComparativa();
  
  

  //Evento al cambiar el select del filtro entrada/salida
  $('#select_viewGeneral').change(function(){
    //Dibujar comparativa
    pintarComparativa($(this).val());
  });
});
</script>