<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$tiemposArray = array('tiempoPV' => 0, 'tiempoG' => 0, 'tiempoA' => 0, 'tiempoE' => 0, 'tiempoSB' => 0, 'tiempoT' => 0);

function conversorSegundosHoras($tiempo_en_segundos) {
    $horas = floor($tiempo_en_segundos / 3600);
    $minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
    $segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);

    return $horas . ':' . $minutos . ":" . $segundos;
}
//echo conversorSegundosHoras(16064);
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

.card{
    display: block;
    width: 100%;
    border: 1px solid #ccc;
    padding: 0;
}
.card .card-title{
    font-size: 15px;
    font-weight: bold;
    border-bottom: 1px solid #ccc;
    padding-left: 15px;
    line-height: 2.2;
    height:80px;
}
.card .card-info{
    font-size: 2.5em;
    text-shadow: 2px 1px 3px #aaa;
    font-weight: bolder;
    text-align: center;
    border-bottom: 1px solid #efefef;
}
.card .card-info:last-child{ border-bottom:none; }

.card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }

.card.card-time .card-info .anio:after,
.card.card-time .card-info .mes:after,
.card.card-time .card-info .dia:after,
.card.card-time .card-info .hora:after,
.card.card-time .card-info .min:after,
.card.card-time .card-info .seg:after{
    font-size: 0.7em;
}
.card.card-time .card-info .anio:after{ content: "y"; }
.card.card-time .card-info .mes:after { content: "m"; }
.card.card-time .card-info .dia:after { content: "d"; }
.card.card-time .card-info .hora:after{ content: "h"; }
.card.card-time .card-info .min:after { content: "m"; }
.card.card-time .card-info .seg:after { content: "s"; }

.card .card-info .promedio{
    line-height: 75px;
}
.card .card-info .comentario span{
    display: block;
}
.card .card-info .comentario span.letras{
    font-size: .5em;
}
/*
* CUSTOM
*/
.time-container{
  padding:10px 5px;
  float:none;
  display:inline-block;
  vertical-align:top;
}
@media only screen and (max-width: 990px) {
  .card .card-info{
    font-size: 1.5em;
  }
  .card .card-title{
    font-size:13px;
  }
}
</style>
<div class="container">
<?php if(!$this->isIframe): ?>
  <div class="row " id="levels" style="padding-top: 1ex; ">
            <div class="col-md-12">
                <ol class="breadcrumb">
                  <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
                  <li><a href="<?php echo $this->documento->getUrlBase();?>/reportealumno"><?php echo JrTexto::_("Reports")?></a></li>
                  <li class="active"><?php echo JrTexto::_("Study time in the virtual platform")?></li>
                </ol>
            </div>
             
               

    </div>
<?php endif; ?>
  

<div class="row " id="levels" style="padding-top: 1ex; ">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Report")?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
               
        <?php

        $horasCount = null;

        if(!empty($this->lista_tiempo)){
          foreach ($this->lista_tiempo as $lista1){
            $tiemposArray['tiempoPV']=abs($lista1['tiempo']);
            $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
            //echo $totalpv."<br>";
            
            $horas=explode(":",$totalpv);
            
            $hora=$horas[0];          
            if (!$hora) $hora=0;          
            $min=$horas[1];
            if (!$min) $min=0;
            $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
            
            $horasCount = $horas;

          }//for
        }
        if(!empty($this->games)){
          foreach ($this->games as $lista1){
            $tiemposArray['tiempoG']=abs($lista1['tiempo']);
            $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
            //echo $totalpv."<br>";
            $horas=explode(":",$totalpv);
            
            $hora=$horas[0];          
            if (!$hora) $hora=0;          
            $min=$horas[1];
            if (!$min) $min=0;
            $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
            
            $horasCount_games = $horas;

          }//for
        }
        if(!empty($this->actividades)){
            foreach ($this->actividades as $lista1){
              $tiemposArray['tiempoA']=abs($lista1['tiempo']);
              $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
              //echo $totalpv."<br>";
              $horas=explode(":",$totalpv);
              
              $hora=$horas[0];          
              if (!$hora) $hora=0;          
              $min=$horas[1];
              if (!$min) $min=0;
              $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
              
              $horasCount_actvidad = $horas;
  
            }//for
          }

          if(!empty($this->examenes)){
            foreach ($this->examenes as $lista1){
              $tiemposArray['tiempoE']=abs($lista1['tiempo']);
              $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
              //echo $totalpv."<br>";
              $horas=explode(":",$totalpv);
              
              $hora=$horas[0];          
              if (!$hora) $hora=0;          
              $min=$horas[1];
              if (!$min) $min=0;
              $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
              
              $horasCount_examenes = $horas;
  
            }//for
          }
          if(!empty($this->smartbook)){
            foreach ($this->smartbook as $lista1){
              $tiemposArray['tiempoSB']=abs($lista1['tiempo']);
              $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
              //echo $totalpv."<br>";
              $horas=explode(":",$totalpv);
              
              $hora=$horas[0];          
              if (!$hora) $hora=0;          
              $min=$horas[1];
              if (!$min) $min=0;
              $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
              
              $horasCount_smartbook = $horas;
  
            }//for
          }
          if(!empty($this->tareas)){
            foreach ($this->tareas as $lista1){
              $tiemposArray['tiempoT']=abs($lista1['tiempo']);
              $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
              //echo $totalpv."<br>";
              $horas=explode(":",$totalpv);
              
              $hora=$horas[0];          
              if (!$hora) $hora=0;          
              $min=$horas[1];
              if (!$min) $min=0;
              $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
              
              $horasCount_tareas = $horas;
  
            }//for
          }
          

        /*for ($i=1;$i<=50;$i++){
          $ale = rand (1,100);
          $reporte.='
          ["'.$curso.'", '.$ale.', "#'.$colores[$x].'"],';
          $x++;
          if ($x==4) $x=0;
        }*/
        ?>

      <!--EMMY HERE-->
      <div class="row" style="text-align:center;">
        <!-- en PV-->
        <div class="col-xs-4 time-container" id="tiempoPV"  >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount[0])) ? $horasCount[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount[1])) ? $horasCount[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount[2])) ? $horasCount[2] : '0'; ?></span>
                      </div>
                      <!-- <div class="info-tiempo tiempo-optimo" title="<?php echo JrTexto::_('Optimal time'); ?>">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora">00</span>
                          <span class="min">00</span>
                          <span class="seg">00</span>
                      </div> -->
                  </div>
                  
                  <!-- <div class="col-xs-6 col-sm-3 porcentaje_tiempos color-success">
                      <p class="porcentaje">100%</p>
                      <p class="mensaje" style="font-size:12px"><?php echo JrTexto::_('Missing time to reach optimum time'); ?>: <span>00:00:00</span></p>
                  </div>
                  <div class="col-xs-6 col-sm-3 comentario color-warning">
                      <span><i class="fa fa-thumbs-up"></i></span>
                      <span class="letras">BUENO</span>
                  </div> -->
              </div>
          </div>
        </div>
        <!--en practica de HomeWork-->
        <div class="col-xs-4 time-container" id="tiempoT"  >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Time Spent on Activities'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                      </div>
                  </div>

              </div>
          </div>
        </div>

        <!--en actividades-->
        <!-- <div class="col-xs-3 time-container" id="tiempo"  >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Tiempo empleado en actividades'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> 
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount_actvidad[0])) ? $horasCount_actvidad[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount_actvidad[1])) ? $horasCount_actvidad[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount_actvidad[2])) ? $horasCount_actvidad[2] : '0'; ?></span>
                      </div>
                  </div>

              </div>
          </div>
        </div> -->

        <!-- en smartbook-->
        <div class="col-xs-4 time-container" id="tiempoSB"  >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Time Spent on the SmartBook'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount_smartbook[0])) ? $horasCount_smartbook[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount_smartbook[1])) ? $horasCount_smartbook[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount_smartbook[2])) ? $horasCount_smartbook[2] : '0'; ?></span>
                      </div>
                  </div>

              </div>
          </div>
        </div>
        <!--en juegos del smartbook-->
         <!--<div class="col-xs-3 time-container" id="tiempoG"  >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Tiempo empleado en juegos del SmartBook'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> 
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount_games[0])) ? $horasCount_games[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount_games[1])) ? $horasCount_games[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount_games[2])) ? $horasCount_games[2] : '0'; ?></span>
                      </div>
                  </div>

              </div>
          </div>
        </div> -->
        <!--en examenes en smartquiz-->
        <div class="col-xs-4 time-container" id="tiempoE" >
          <div class="card card-time">
              <div class="card-title"><?php echo JrTexto::_('Time Spent on Exams'); ?></div>
              <div class="card-info">
                  <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                      <div class="info-tiempo tiempo-obtenido">
                          <span class="anio" style="display: none;">00</span>
                          <span class="mes" style="display: none;">00</span>
                          <span class="dia" style="display: none;">00</span>
                          <span class="hora"><?php echo (!empty($horasCount_examenes[0])) ? $horasCount_examenes[0] : '0'; ?></span>
                          <span class="min"><?php echo (!empty($horasCount_examenes[1])) ? $horasCount_examenes[1] : '0'; ?></span>
                          <span class="seg"><?php echo (!empty($horasCount_examenes[2])) ? $horasCount_examenes[2] : '0'; ?></span>
                      </div>
                  </div>

              </div>
          </div>
        </div>
      </div>
      
      <!--End Emmy-->
        <!-- <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:80vw">
          <canvas id="columnchart_values2"></canvas>
        </div> -->
      
    </div>
</div>



</div>
</div>


<script type="text/javascript">
var tiemposJSON = <?php echo json_encode($tiemposArray) ?>;
// console.log(tiemposJSON);

function drawChart(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Time spent on the platform'
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },legend: {
            display: false
         }
    }
  });
}

$('document').ready(function(){
  var calcular = function(val, isline=null){
    var resultado = 0;
    // console.log('valor: ' + val);
    // console.log('totalpv: '+tiemposJSON.tiempoPV);
    // console.log('total: '+((val * 100) / tiemposJSON.tiempoPV));
    if(val > 0){
      var result_tmp = ((val * 100) / tiemposJSON.tiempoPV);
      var result_tmp_string = (result_tmp).toString().split('.');
      var convertINT = parseInt(result_tmp_string[0]);
      var convertINT_decimal = parseInt(result_tmp_string[1].substring(0,2));
      if(convertINT == 0){
        if(convertINT_decimal == 0){
          resultado = Math.ceil(result_tmp);
        }else{
          resultado = result_tmp;
        }
      }else{
        resultado = result_tmp;
      }
      // console.log(resultado);
      if(isline === true){
        resultado = resultado * ((Math.floor((Math.random()*3)+1) * 0.1) + 1)
      }

    }
    return resultado.toFixed(2);
  };
  var chartData = {
    labels: ['SmartBook', 'Tests', 'Activities'],
    datasets: [ {
      type: 'bar',
      label: ['SmartBook','SmartQuiz','Activity'],
      backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
      data: [
        calcular(tiemposJSON.tiempoSB),calcular(tiemposJSON.tiempoE),calcular(tiemposJSON.tiempoT)
        // 0,0,0,0
      ],
      borderColor: 'white',
      borderWidth: 2
    }]

  };
  // drawChart('columnchart_values2',chartData);
});

</script>

<script type="text/javascript">

// var tiempoXFiltro = function(idBuscar=null, tipo=''){
//     // mostrarCargando('#tiempos');
//     var str_url = '';
//     if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
//     if(tipo=='A') str_url = _sysUrlBase_+'/historial_sesion/tiempoxalumno/';
//     else return false;
//     // else if(tipo=='G') str_url = _sysUrlBase_+'/historial_sesion/tiempoxgrupo/';
        
//     if($('#pnl-datosfiltro .ultima-actividad').length>0){
//         var orden=0;
//         var ultima_actividad={};
//         $('#pnl-datosfiltro .ultima-actividad').each(function(index, el) {
//             var $this=$(el);
//             if(parseInt($this.data('orden')) > orden){
//                 ultima_actividad={
//                     'idnivel':$this.data('idnivel'),
//                     'idunidad':$this.data('idunidad'),
//                     'idactividad':$this.data('idactividad'),
//                 };
//             }
//         });
//     }else{
//         $('#tiempos .inicial h3').html('<p><?php echo JrTexto::_('The students or students have not worked any activity'); ?>.</p><p><?php echo JrTexto::_('You can not verify times for this students or students'); ?>.</p>')
//         return false;
//     }

//     if(str_url=='') return false;
//     $.ajax({
//         url: str_url,
//         type: 'POST',
//         async:false,
//         dataType: 'json',
//         data: {'id': idBuscar, 'tipo': tipo, 'ultima_actividad':JSON.stringify(ultima_actividad), 'idcurso':FILTRO.idcurso},
//     }).done(function(resp) {
//         console.log(resp);
//         if(resp.code=='ok'){
//             var tiempo = resp.data;
//             var configProf = resp.config_docente;
//             var tiempoJson = {};
//             /*if($('#tiempos .contenido').hasClass('slick-initialized')){
//                  $('#tiempos .contenido').slick('unslick');
//             }*/
//             $.each(tiempo, function(key, val) {
//                 if(key=='total'){ var $pnl_tiempo=$('#tiempo_total'); var optimo=configProf.tiempototal; }
//                 else if(key=='actividades'){ var $pnl_tiempo=$('#tiempo_actividades'); var optimo=configProf.tiempoactividades; }
//                 else if(key=='juegos'){ var $pnl_tiempo=$('#tiempo_games'); var optimo=configProf.tiempogames; }
//                 else if(key=='teacherresrc'){ var $pnl_tiempo=$('#tiempo_teacher_resrc'); var optimo=configProf.tiempoteacherresrc; }
//                 else if(key=='examenes'){ var $pnl_tiempo=$('#tiempo_examenes'); var optimo=configProf.tiempoexamenes; }
                
//                 if( isJSON(configProf.escalas) ) {
//                     var arr = JSON.parse(configProf.escalas);
//                     if(arr.length>0){ ESCALAS = arr; }
//                 }
                
//                 if($pnl_tiempo!==undefined){
//                     $cardObtenido = $pnl_tiempo.find('.tiempo-obtenido');
//                     $cardOptimo = $pnl_tiempo.find('.tiempo-optimo');
//                     mostrarInfoTiempo($cardObtenido, val);
//                     mostrarInfoTiempo($cardOptimo, optimo);
//                     analizarTiempos(val, optimo, $pnl_tiempo);
//                 }
//             });

//             $('#tiempos .inicial').hide();
//             $('#tiempos .contenido').show();
//             //$('#tiempos .contenido').slick();
//         }else{
//             mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
//         }
//     }).fail(fnAjaxFail);
// };

 </script>

    
  
   
