<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$ejericios=$this->datos["ejericios"];
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$met=$this->metodologia;
?>
<style type="text/css">
.shorteractivity{
	list-style: none;
}
.shorteractivity li{
	background: #fffae3;
    border: 1px dashed;  
    margin-bottom: 1ex;
    padding: 0.5ex;
    position: relative;
    font-size: 1.1em;
}
.shorteractivity li span.numberorder{
	font-weight: bold;
	font-size: 1.3em;
}
.shorteractivity li span.tipoorden{
	float: right;
}
.shorteractivity .enmovimiento{
	background: #f7ba7a;
	color:#131515;
}
</style>
<div class="row">
	<div class="col-md-12" >
		<?php if(!empty($ejericios)){ ?>
		<ol  class='shorteractivity' id="ordenar<?php echo $idgui ?>"> 
			<?php 
			$i=0;			
			foreach ($ejericios as $ejer){ $i++; 
			$detalle=$ejer["texto_edit"];
			$patron = '/\<h3(.*?)\"\>(.*?)<\/h3>/is'; 
            $titulo = '';
           // var_dump($detalle);
            if (preg_match($patron, $detalle, $extracto1)){ 
              $titulo = $extracto1[2];
			} 
			$hab=array(4=>'L',5=>'R',6=>'W',7=>'S');
			$habhay=explode('|',$ejer["idhabilidad"]);
            ?>
		  <li data-iddetalle="<?php echo  $ejer["iddetalle"]; ?>"><span class='numberorder'> <?php echo $i.".- </span>".$titulo." <span class='tipoorden'>".$ejer["tipo_desarrollo"]; ?><span style="color:green">
		  &nbsp|&nbsp 
		  <?php echo in_array(4,$habhay)?'<b>'.$hab[4].'</b>':''; ?>
		  <?php echo in_array(5,$habhay)?'<b>'.$hab[5].'</b>':''; ?>
		  <?php echo in_array(6,$habhay)?'<b>'.$hab[6].'</b>':''; ?>
		  <?php echo in_array(7,$habhay)?'<b>'.$hab[7].'</b>':''; ?></span>
		  <i title="trasnferir a <?php echo $this->metodologia==2?'DBY':'PRACTICE'; ?>" class="fa fa-share-square-o btn btn-warning btn-transferir"
		     data-iddetalle="<?php echo  $ejer["iddetalle"]; ?>" data-transferir="<?php echo $this->metodologia==2?3:2; ?>" style="color:blue;"></i>
		     <i title="Borrar" class="fa fa-trash btn btn-danger btn-eliminarDetalle" data-iddetalle="<?php echo  $ejer["iddetalle"]; ?>" ></i>
		  </span></li>
		 <?php } ?>
		</ol>
		<?php }else{ ?>
		<h2 class="text-center"><i class="fa fa-exclamation-circle fa-4x color-red"></i><br>
			<?php echo JrTexto::_('Empty data') ?>
		</h2>
		<?php } ?>
	</div>
	<?php if($this->documento->plantilla=='modal'){?>
	<div class="col-md-12 text-center">
		<button class="btn btn-primary " id="btncerrarmodalorder"> <i class="fa fa-close"></i> <?php echo JrTexto::_('Close') ?></button>
	</div>
	<?php }?>
</div>
<script type="text/javascript">
	var txtupdateorder='<?php echo JrTexto::_('Update') ?>';
	var txtcloseorder='<?php echo JrTexto::_('close') ?>';
	var changecambios=false;
	$('#btncerrarmodalorder').click(function(){
		if('norefresh'=='<?php echo $ventanapadre; ?>'){
			$(this).closest('.modal').find('.cerrarmodal').trigger('click');
		}else{
			if(changecambios)location.reload(true);
			else $(this).closest('.modal').find('.cerrarmodal').trigger('click');
		}
	});
	$("#ordenar<?php echo $idgui ?>").sortable({
		cursor: "move",
		opacity: 0.9,
		placeholder: "sortable-placeholder",
		start: function(event,ui) {
			$(ui.item).addClass('enmovimiento').attr('data-oldindex',$(ui.item).index()+1);
		},
		stop: function(event,ui){
			changecambios=true;
			var li=$(ui.item)
			li.removeClass('enmovimiento');
			var lis=$("#ordenar<?php echo $idgui ?>").find('li');
			var pos=parseInt(li.index()+1);
			var oldindex=parseInt($(ui.item).attr('data-oldindex'));
			var id=li.attr('data-iddetalle');
			$.each(lis,function(i,v){
				$(this).find('span.numberorder').text((i+1)+'.- ');
				$(this).removeAttr('data-oldindex');
			});			
			var formData = new FormData();
      		formData.append("iddetalle",id);
      		formData.append("neworder",pos);
			$.ajax({
		        url: _sysUrlBase_+'/actividad/ordenardetallejson',
		        type: "POST",
		        data:  formData,
		        contentType: false,
		        processData: false,
		        dataType:'json',
		        cache: false,
		        processData:false,     
		        beforeSend: function(XMLHttpRequest){
		        	$('#btncerrarmodalorder').text('<i class="fa fa-refresh"></i> '+txtupdateorder).addClass('disabled').attr('disabled','disabled');
		        },success: function(data)
		        {
		          if(data.code==='ok'){
		            //location.reload(true);
		          }else{
		            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','Error','warning');
		          }
		          $('#btncerrarmodalorder').html('<i class="fa fa-close"></i> '+txtcloseorder).removeClass('disabled').removeAttr('disabled');
		        },
		        error: function(e){ console.log(e); },
		        complete: function(xhr){ $('#btncerrarmodalorder').html('<i class="fa fa-close"></i> '+txtcloseorder).removeClass('disabled').removeAttr('disabled');}
		      });
		}
	});
	$("#ordenar<?php echo $idgui ?>").on('click','.btn-transferir',function(ev){
		ev.preventDefault();
		var obj=$(this);
		var iddet=obj.attr('data-iddetalle');
		var newmet=obj.attr('data-transferir');
		var formData = new FormData();
      		formData.append("iddetalle",iddet);
      		formData.append("newmet",newmet);
		$.ajax({
		        url: _sysUrlBase_+'/actividad/transferir',
		        type: "POST",
		        data:  formData,
		        contentType: false,
		        processData: false,
		        dataType:'json',
		        cache: false,
		        processData:false,     
		        beforeSend: function(XMLHttpRequest){
					//$obj.remove
		        	//$('#btncerrarmodalorder').text('<i class="fa fa-refresh"></i> '+txtupdateorder).addClass('disabled').attr('disabled','disabled');
		        },success: function(data){
		          if(data.code==='ok'){
		            obj.closest('li').remove();
		          }else{
		            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','Error','warning');
		          }// $('#btncerrarmodalorder').html('<i class="fa fa-close"></i> '+txtcloseorder).removeClass('disabled').removeAttr('disabled');
		        },
		        error: function(e){ console.log(e); },
		        complete: function(xhr){ //$('#btncerrarmodalorder').html('<i class="fa fa-close"></i> '+txtcloseorder).removeClass('disabled').removeAttr('disabled');
				}
		      });
	}).on('click','.btn-eliminarDetalle',function(ev){
		ev.preventDefault();
		var obj=$(this);
		var iddet=obj.attr('data-iddetalle');		
		var formData = new FormData();
      		formData.append("iddetalle",iddet);
      		$.ajax({
		        url: _sysUrlBase_+'/actividad/eliminarDetalle',
		        type: "POST",
		        data:  formData,
		        contentType: false,
		        processData: false,
		        dataType:'json',
		        cache: false,
		        processData:false,     
		        beforeSend: function(XMLHttpRequest){
		        },success: function(data){
		          if(data.code==='ok'){ 
		          	obj.closest('li').remove();
		          	//window.location=window.location;
		          }else{ mostrar_notificacion('<?php echo JrTexto::_('Attention');?>','Error','warning');
		          }
		        },
		        error: function(e){ console.log(e); },
		        complete: function(xhr){ 
				}
		      });

	})
</script>