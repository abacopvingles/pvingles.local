<style type="text/css">
    .div_menu{
        height: 170px; font-size: 20px; text-align: center;
        padding: 40px;
    }
    .color1{
        background-color: #22b14c;
    }
    .color2{
        background-color: #9B59B6;
    }
    .color3{
        background-color: #E74C3C;
    }
    .color4{
        background-color: #00a2e8;
    }
    .color5{
        background-color: #F39C12;
    }
    .color6{ background-color: #558bdd; }
    .color7{ background-color: #2ECC71; }
    .color8{ background-color: #F1C40F; }
    .color9{ background-color: #3910de; }
    .icon-menu{ width: 100%; font-size: 2em; }
    
</style>
<?php defined("RUTA_BASE") or die(); ?>

<div id="loadingPage_for_castro" style="
    display:none;
    width: 100%;
    height: 100%;
    position: fixed;
    background: #000000a3;
    left: 0;
    z-index: 99999; top: 0;
">
        <figure style="
    position: absolute;
    width: 100%;
    /* margin: 0 auto; */
    text-align: center;
    top: 35%;
    /* background: #0000004a; */
"><div style="
    display: inline-block;
    /* padding: 39px; */
    border-radius: 50%;
    overflow: hidden;
    /* height: 135px; */
    /* width: 52%; */
"><img src="<?php echo $this->documento->getUrlBase();?>/static/tema/css/images/cargando.gif" style="
    /* border-radius: 51%; */
    padding: 2.8em 2em;
    /* text-shadow: 2px 1px black; */
    background: black;
    min-width: 80%;
    /* height: 189px; */
    width: auto;
    max-width: 100%;
    "></div></figure>
    </div>

<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li class="active">
          <?php 
              echo JrTexto::_("Reports");
          ?>
        </li>
	  </ol>	
	</div>
</div>

<div class="form-view" >
  <!-- <div class="page-title">
    <div class="title_left"><h3></h3>
        <?php echo JrTexto::_('Report');?>        
    </div>
  </div> -->
  <div class="clearfix"></div>
  <div class="div_linea"></div>
  <div class="panel panel-primary">
    <div class="panel-heading" style="text-align:left;">
        <?php echo JrTexto::_("Report") ?>
    </div>
    <div class="panel-body row">

        <?php if($this->user['idproyecto'] !== '3'): ?>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_tiempoe/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color1 div_menu x_content ">
                    <?php echo JrTexto::_("Tiempo de Estudio en la PV"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/rptexamenes/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color5 div_menu x_content" >
                    <?php echo JrTexto::_("Notas de examenes"); ?>
                </div>
                </a>
            </div>

        <?php else : ?>
        <!-- <div style="background-color:white; border-radius:1em; box-shadow:1px 1px 5px; display:inline-block;"> -->
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/ubicacion/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color1 div_menu x_content ">
                    <i class="icon-menu fa fa-flag" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Placement Test"); ?> 
                </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/reporte_entrada" style="color: #fff;">
                <div class="color2 div_menu x_content">
                    <i class="icon-menu fa fa-sign-in" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Beginning Test"); ?> 
                </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/reporte_salida" style="color: #fff;">
                <div class="color3 div_menu x_content" >
                    <i class="icon-menu fa fa-sign-out" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Final Test"); ?> 
                </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/entrada" style="color: #fff;">
                <div class="color4 div_menu x_content" >
                    <i class="icon-menu fa fa-bar-chart" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Beginning and Final Test Comparison"); ?>
                </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_examene/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color5 div_menu x_content" >
                    <i class="icon-menu fa fa-pencil-square-o" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Bimonthly and Quarterly Tests"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/repor_tiempoe/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color6 div_menu x_content ">
                    <i class="icon-menu fa fa-clock-o" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Study Time in The Virtual Platform"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/usodominio/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color7 div_menu x_content ">
                    <i class="icon-menu fa fa-laptop" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Use and Management of the Virtual Platform"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/progreso/?plt=<?php echo $this->documento->plantilla ?>" style="color: #fff;">
                <div class="color8 div_menu x_content" >
                    <i class="icon-menu fa fa-tasks" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Student Progress"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/progreso_habilidad" style="color: #fff;">
                <div class="color9 div_menu x_content" >
                    <i class="icon-menu fa fa-pie-chart" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Progress by Skills"); ?>
                </div>
                </a>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportealumno/progreso_competencia" style="color: #fff;">
                <div class="color3 div_menu x_content" >
                    <i class="icon-menu fa fa-sitemap" aria-hidden="true"></i>
                    <?php echo JrTexto::_("Progress by Competition"); ?>
                </div>
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 hvr-bounce-in">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/miprogreso" style="color: #fff;">
                <div class="color3 div_menu x_content" style="background: #d367ef">
                    <i class="icon-menu fa fa-sitemap" aria-hidden="true"></i>
                    <?php echo JrTexto::_("You Progress"); ?>
                </div>
                </a>
            </div>

        <!-- </div> -->
            
        <?php endif; ?>

        <!--div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/Unidad"><?php echo JrTexto::_("Unit"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/leccion"><?php echo JrTexto::_("Activities"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/metodologia"><?php echo JrTexto::_("Methodology"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/habilidad"><?php echo JrTexto::_("Ability"); ?></a>
            </div>       
        </div> 
    
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/actividad/agregar"><?php echo JrTexto::_("Add Activities"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/usuario"><?php echo JrTexto::_("My profile"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/roles"><?php echo JrTexto::_("Roles"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/menu"><?php echo JrTexto::_("Menus"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/permisos"><?php echo JrTexto::_("Permisos"); ?></a>
            </div>       
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/ugel"><?php echo JrTexto::_("Ugel"); ?></a>
            </div>       
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/local"><?php echo JrTexto::_("Local"); ?></a>
            </div>       
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/ambiente"><?php echo JrTexto::_("Ambiente"); ?></a>
            </div>       
        </div>
        
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/generador"><?php echo JrTexto::_("Generator"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/bib_autor"><?php echo JrTexto::_("Author"); ?></a>
            </div>       
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">               
            <div class="x_content">
                <a href="<?php echo $this->documento->getUrlSitio(); ?>/bib_libro"><?php echo JrTexto::_("Book"); ?></a>
            </div>       
        </div-->
        
        
        
    </div>
  </div>
</div>
<script type="text/javascript">
$('document').ready(function(){
    $('.panel a').on('click',function(){
        $('#loadingPage_for_castro').fadeIn('fast');
    });
});

</script>