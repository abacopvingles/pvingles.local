<?php
$isExam = $this->isExam == true ? 'true' : 'false';
echo ($this->isExam == true) ? "<input type='hidden' id='idcurso' value='{$this->idcurso}'>" : '';
?>
<a href="<?php echo $this->urlGoBack; ?>" class="btn btn-primary" style="position: absolute; top: 10px;">
	<i class="fa fa-home"></i> <?php echo ucfirst(JrTexto::_('Go back')); ?>
</a>

<iframe src="<?php echo $this->url; ?>" frameborder="0" id="frameExam" style="width: 100%;"></iframe>

<script type="text/javascript">

var oIdHistorial = {'examen': 0}
var examen = <?php echo $isExam ?>;

var redimension = function(){
	var ventana = $(window).height();
	var header = $('header').height();
	$('#frameExam').css('height', (ventana-header-10)+'px');
};

var registrarHistorialSesion = function(idTabPane = null){
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    // var type = function(t){
    //     switch(t){
    //         case '#div_games': { return 'G'; } break;
    //         case '#div_practice' : { return 'A';} break;
    //     }
    //     return 'TR';
    // };

    var lugar = idTabPane == null ? 'E' : type(idTabPane);
    // alert(lugar);
    $.ajax({
        url: _sysUrlBase_+'/historial_sesion/agregar',
        type: 'POST',
        dataType: 'json',
        data: {'lugar': lugar, 'fechaentrada': fechahora, 'idcurso': $('#idcurso').val()},  
    })
    .done(function(resp) {
        if(resp.code=='ok'){
            
			oIdHistorial.examen = resp.data.idhistorialsesion;
            // console.log(oIdHistorial.examen);

        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
    });
    return 0;
};

var editarHistoriaSesion = function(id = null){

    var _id = id != null ? id : 0 ;
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _id, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
					oIdHistorial.examen = resp.data.idhistorialsesion;
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
};

$(document).ready(function() {
	if(examen == true){
        registrarHistorialSesion();
     }
	redimension();
    $(window).on('beforeunload', function(){
        if(examen == true){
            editarHistoriaSesion(oIdHistorial.examen);
        }
    });
	
});
$(window).resize(function(e) {
	redimension();
});
</script>