<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">

<?php if(!$ismodal){?>
<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        
        
        <?php if(!empty($this->datos["examenes"])){?>
          <li><a href="<?php echo $this->documento->getUrlBase();?>/examenes/examenesxcurso"><i class="fa fa-graduation-cap"></i>  &nbsp;<?php echo JrTexto::_('Courses'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_('Exam'); ?></li>
          <?php }else{?>
          <li class="active">&nbsp;<?php echo JrTexto::_('Courses'); ?></a></li>
          <?php } ?> 
    </ol>
</div> </div>
<?php } ?>

<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
    <div class="row">
      <?php if(!empty($this->idcurso)){ ?>
      <div class="col-md-12">
        <div class="panel" >      
           <div class="panel-body">      
            <div class="row"> 
              <div class="col-xs-12 col-sm-6 col-md-6">
                <label>Cursos </label>
                <div class="select-ctrl-wrapper select-azul" >
                  <select id="fkcbugel" name="fkcbugel" class="form-control select-ctrl" onchange="window.location=('?idcurso='+this.value)">
                      <?php 
                        if(!empty($this->datos["cursos"]))
                          foreach ($this->datos["cursos"] as $cur) { ?><option value="<?php echo $cur["idcurso"]; ?>" <?php echo $cur["idcurso"]==$this->idcurso?'selected="selected"':''?>> <?php echo $cur["nombre"]?> </option><?php } ?>                        
                  </select>
                </div>
              </div> 
              <?php if($this->usuario["idrol"]==1){?>
                <div class="col-xs-12 col-sm-6 col-md-6  text-center"><br>
                  <a href="<?php echo $this->documento->getUrlBase()."/examenes" ?>" class="btn btn-primary"><?php echo JrTexto::_('See All'); ?></a>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
	    <div class="col-md-12 col-sm-12 col-xs-12">
	      <div class="panel">         
          <div class="panel-body">
            <?php 
                  if(empty($this->idcurso)){
                    if(!empty($this->datos["cursos"]))
                      foreach ($this->datos["cursos"] as $cur){ ?>
                  <div class="col-md-2 col-sm-6 col-xs-12 btn-panel-container panelcont-xs text-center ">
                      <a href="?idcurso=<?php echo $cur["idcurso"]; ?>" class="btn-block btn-green btn-panel hvr-float-shadow" style="border: 1px solid #f00; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);">
                        <div><img src="<?php echo $this->documento->getUrlBase().$cur["imagen"]; ?>" class="img img-responsive" width="100%"></div>
                        <div class="namelevel text-center" ><h1 ><?php echo $cur["nombre"]; ?></h1></div>
                          <h6 style="color:#000"><strong>Total:</strong><span> <?php echo @$cur["numexa"]; ?> </span><?php echo JrTexto::_("Exam"); ?></h6>
                      </a>
                  </div>
              <?php } 
                }else{
                  if(!empty($this->datos["examenes"]))
                      foreach ($this->datos["examenes"] as $exa){
                      //var_dump($exa);            ?>
                  <div class="col-md-2 col-sm-6 col-xs-12 text-center" style="margin-top: 1ex;">
                      <a href="<?php echo $exa["urlver"]; ?>" class="btn-block btn-panel hvr-float-shadow" style="border: 1px solid #f5b32e; box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);">
                        <div class="namelevel text-center" style="height: 45px; overflow: hidden;"><h5 style="color:#000"><strong><?php echo $exa["nombre"]; ?></h5></strong></div>
                        <div><img src="<?php echo $exa["imagen"]; ?>" class="img img-responsive" width="100%"></div>
                        <i data-link="<?php echo @$exa["urledit"]; ?>" class="editarexamen btn btn-warning fa fa-pencil"></i>
                          <!--h6 style="color:#000">Total:<span> 8 </span><?php //echo JrTexto::_("Question"); ?></h6-->
                      </a>
                      <!--a href="?idexamen=<?php //echo $exa["idrecurso"]; ?>" class="btn-block btn-yellow btn-panel hvr-float-shadow">                   
                          
                      </a-->
                  </div>
              <?php }} ?>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.editarexamen').on('click',function(ev){
      ev.preventDefault();
      var link=$(this).attr('data-link');
      window.location.href=link;
    })
  })
</script>