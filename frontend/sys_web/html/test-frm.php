<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$imgcursodefecto='/static/media/nofoto.jpg';
if(!empty($frm['imagen'])) $imagen=$frm['imagen'];
else $imagen=$imgcursodefecto;
$urlmedia=$this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .tabstep li a{
    color: #fff;
    background-color: #ec9c58;
  }
  .tabstep li a:hover{
    color: #fff;
    background-color: #5ba1de;
  }
  .badgestep{
  position: absolute;
  left: 1ex;
  font-size: 1.5em;
  }
  .tr_clone{display: none};  
</style>
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Test">&nbsp;<?php echo JrTexto::_('Self-autoevaluation'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>

<ul class="nav nav-pills nav-justified thumbnail tabstep">
  <li class="active"><a data-toggle="tab" href="#tabdatos"><span class="badgestep">1.</span><?php echo JrTexto::_('Setting') ?></a></li>
  <li><a data-toggle="tab" href="#tabcriterios"><span class="badgestep">2.</span><?php echo JrTexto::_('Criterion') ?></a></li>
  <li><a data-toggle="tab" href="#tabasignacion"><span class="badgestep">3.</span><?php echo JrTexto::_('Assignment') ?></a></li>
  <!--li><a href="#"><span class="badgestep">4.</span><?php echo JrTexto::_('Setting') ?></a></li-->
</ul>
<div class="tab-content">
  <input type="hidden" name="idTest" value="<?php echo !empty($frm["idtest"])?$frm["idtest"]:0; ?>">
  <div id="tabdatos" class="tab-pane fade in active">
    <!--h3>1.- <?php //echo JrTexto::_('Setting');?></h3-->
    <div class="col-md-6 col-sm-12">
      <div class="form-group">
      <label for="titulo"><?php echo JrTexto::_('Titulo');?> <span class="required"> * </span> </label>
      <input type="text" class="form-control" id="titulo" name="txtTitulo" value="<?php echo @$frm["titulo"] ?>" placeholder="Test ejemplo">
      </div>      
      <div class="form-group">
        <label class="" for="txtPuntaje"><?php echo JrTexto::_('Puntaje');?> <span class="required"> * </span></label>
        <input type="hidden"  id="txtPuntaje" name="txtPuntaje" required="required" class="form-control" value="<?php echo @$frm["puntaje"];?>">
        <table id="tblEscalas" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>
              <th>Code</th>
              <th>Nombre</th>
              <th>Descripcion</th>
              <th>(*)</th>             
            </tr>
          </thead>
          <tbody>
            <tr class="tr_clone" >
              <td style="width: 70px;">
                <input type="text" class="form-control gris txtcode" placeholder="1">
              </td>
              <td style="width: 150px;">               
                <input type="text" class="form-control gris txtnombre" placeholder="e.g.: A,*,enproceso">
              </td>
              <td >
                <input type="text" class="form-control gris txtdescripcion" >
              </td>
              <td style="width: 25px;"><i class="btn btn-removefila fa fa-trash color-red"></i></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td class="text-right" colspan="3">
                <span class="btn btn-primary  btn-xs btn-addfila"><i class="fa fa-plus"></i><?php echo JrTexto::_('Add row');?></span>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="col-md-6 col-sm-12 text-center" id="frmdatos11">
      <div class="form-group">
        <label><?php echo JrTexto::_("Image") ?> </label> 
        <div style="position: relative;" class="sufirimgcontent">
          <div class="toolbarmouse text-center" ><span class="btn btnremoveimage" style="position:absolute;top: 0px;padding: 0px;"><i class="btn btn-danger btn-xs fa fa-trash"></i></span></div>
          <img src="<?php echo $urlmedia.$imagen; ?>" alt="imagen" name="imagen" class="nomimagen changeimage img-responsive center-block thumbnail" id="imagen" style="max-width: 200px;  max-height: 200px;">
        </div>
      </div>
      <div class="form-group text-center"><hr>
        <label class="" for="txtMostrar"> <?php echo JrTexto::_('Status');?> :        
          <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["mostrar"]==1?"fa-check-circle":"fa-circle-o";?>" 
          data-value="<?php echo @$frm["mostrar"];?>"  data-valueno="0" data-value2="<?php echo @$frm["mostrar"]==1?1:0;?>">
           <span> <?php echo JrTexto::_(@$frm["mostrar"]==1?"Activo":"Inactivo");?></span>
           <input type="hidden" name="txtMostrar"  value="<?php echo !empty($frm["mostrar"])?$frm["mostrar"]:0;?>" > 
           </a> 
        </label>
      </div>      
    </div>
    <div class="col-md-12 text-center">
      <hr>
       <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('test'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
       <a type="button" class="btn btn-info btnsavePaso1" href="#"><?php echo JrTexto::_('Next');?> <i class=" fa fa-arrow-right"></i> </a>
    </div>  
  </div>
  <div id="tabcriterios" class="tab-pane fade">
     <div class="col-md-6 text-center">
        <h3 class="_nomtitulo"><?php echo JrTexto::_('Title'); ?></h3>
        <img src="<?php echo $urlmedia.$imagen; ?>" alt="imagen" name="imagen" class="_nomimagen img-responsive center-block thumbnail" id="imagen" style="max-width: 200px;  max-height: 200px;">
        <img src="">
     </div>    
     <div class="col-md-6 text-center">
      <table id="tblEscalas2" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>           
              <th>Nombre</th>
              <th>Descripcion</th>   
            </tr>
          </thead>
          <tbody>           
          </tbody>         
        </table>
     </div>
      <div class="col-md-12">
        <table id="tblEscalas3" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>
              <th >#</th>
              <th >Criterio</th>
              <th >Valor</th>
              <th >(*)</th>             
            </tr>
          </thead>
          <tbody>              
          </tbody>
          <tfoot>
            <tr>
              <td class="text-right" colspan="4">
                <span class="btn btn-primary  btn-xs btn-addfila"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add row');?></span>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="col-md-12 text-center">
        <hr>
         <a type="button" class="btn btn-info btnbackPaso1" href="#"><i class=" fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
         <a type="button" class="btn btn-info btnsavePaso2" href="#"><?php echo JrTexto::_('Next');?> <i class=" fa fa-arrow-right"></i> </a>
      </div>
  </div>
  <div id="tabasignacion" class="tab-pane fade">
    <div class="col-md-12 text-center">
      <h3><?php echo JrTexto::_('Asignaciones'); ?></h3>  
    </div>
    <div class="col-md-12 text-center">
      <table id="tblEscalas4" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>
              <th><?php echo JrTexto::_('Course'); ?></th>
              <th><?php echo JrTexto::_('Resourse'); ?></th>   
              <th>(*)</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            if(!empty($this->Asignaciones)){
              foreach ($this->Asignaciones as $asi){
                ?>
                <tr data-idtestasigancion="<?php echo $asi["idtestasigancion"]; ?>" data-idrecurso="<?php echo $asi["idrecurso"]; ?>" >
                  <td>
                    <div class="select-ctrl-wrapper select-azul">
                      <select class="selcurso select-ctrl form-control">
                        <?php 
                          if(!empty($this->fkcursos))
                          foreach ($this->fkcursos as $i => $v){
                            echo '<option value="'.$v["idcurso"].'" '.($v["idcurso"]==$asi["idcurso"]?'selected="selected"':'').'>'.$v["nombre"].'</option>';
                          }                    
                        ?>
                      </select>
                    </div>
                  </td>
                  <td>
                    <div class="select-ctrl-wrapper select-azul">
                      <select class="aquirecursos select-ctrl form-control">
                      </select>
                    </div>
                  </td>
                  <td>
                    <i class="btn btn-success  btn-savefila fa fa-save "></i><i class="btn btn-danger  btn-removefila fa fa-trash"></i>
                  </td>
              </tr>
            <?php } } ?>
            <tr class="tr_clone">
              <td>
                <div class="select-ctrl-wrapper select-azul">
                  <select class="selcurso select-ctrl form-control">
                    <?php 
                      if(!empty($this->fkcursos))
                      foreach ($this->fkcursos as $i => $v){
                        echo '<option value="'.$v["idcurso"].'">'.$v["nombre"].'</option>';
                      }                    
                    ?>
                  </select>
                </div>
              </td>
              <td>
                <div class="select-ctrl-wrapper select-azul">
                  <select class="aquirecursos select-ctrl form-control">
                  </select>
                </div>
              </td>
              <td>
                <i class="btn btn-success  btn-savefila fa fa-save "></i><i class="btn btn-danger  btn-removefila fa fa-trash"></i>
              </td>
            </tr>
          </tbody> 
          <tfoot>
            <tr>
              <td class="text-right" colspan="4">
                <span class="btn btn-primary  btn-xs btn-addfila"><i class="fa fa-plus"></i> <?php echo JrTexto::_('Add row');?></span>
              </td>
            </tr>
          </tfoot>
        </table>
     </div>    
    <div class="col-md-12 text-center">
        <hr>
         <a type="button" class="btn btn-info btnbackPaso2" href="#"><i class=" fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>         
      </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
var datosiniciofrm=<?php echo !empty($frm)?json_encode($frm):'{}'; ?>; 
var agregarfilatblEscalas=function(dt){
  var tb=$('table#tblEscalas');
  var trclone=tb.find('.tr_clone').clone();
  trclone.find('.txtcode').val(dt.code||'');
  trclone.find('.txtnombre').val(dt.nombre||'');
  trclone.find('.txtdescripcion').val(dt.descripcion||'');
  trclone.removeClass('tr_clone');  
  tb.find('tbody').append(trclone);
}

$('table#tblEscalas').on('click','.btn-removefila',function(ev){
  $(this).closest('tr').remove();
}).on('click','.btn-addfila',function(ev){
  agregarfilatblEscalas({});
})


$('table#tblEscalas3').on('click','.btn-removefila',function(ev){
  var tr=$(this).closest('tr');
  var idtestcriterio=tr.attr('data-idtestcriterio')||0;
  tr.remove();
  if(idtestcriterio>0){
    var fd = new FormData(); 
    fd.append('idtestcriterio', idtestcriterio);   
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test_criterios/eliminarTest_criterios',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){}
    })
  }
  var tbr=$('table#tblEscalas3').children('tbody').children('tr');
  var itr=0;
    $.each(tbr,function(i,v){ itr++;  $(v).children('td').first().text(itr); })    
}).on('click','.btn-addfila',function(ev){
  agregarcriterios({});
}).on('click','.btn-savefila',function(ev){
  var tr=$(this).closest('tr');
  var fd = new FormData();
  var txtcriterio=tr.find('input[name="txtcriterio"]').val()||'';
  var idtestcriterio=tr.attr('data-idtestcriterio')||0;
    fd.append('txtIdtest', $('input[name="idTest"]').val());
    fd.append('txtCriterio', txtcriterio);
    fd.append('txtMostrar', 1);
    fd.append('idtestcriterio', idtestcriterio);    
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test_criterios/guardarTest_criterios',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){
        //if(_isFunction(obj.callback)){
          tr.attr('data-idtestcriterio',data.newid);
        // datosiniciofrm.idtest=data.newid;
        // obj.callback(data);
        //}*/
      }
    })
    var tb3=$('table#tblEscalas3');
})

$('table#tblEscalas4').on('click','.btn-removefila',function(ev){
  var tr=$(this).closest('tr');
  var idtestasigancion=tr.attr('data-idtestasigancion')||0;
  tr.remove();
  if(idtestasigancion>0){
    var fd = new FormData(); 
    fd.append('idtestasigancion', idtestasigancion);   
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test_asignacion/eliminarTest_asignacion',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){}
    })
  }
}).on('click','.btn-addfila',function(ev){
  var tbody4=$(this).closest('table').children('tbody');
  var trclone=tbody4.children('tr').first().clone();
   trclone.removeClass('tr_clone').attr('data-idtestasigancion',0).attr('data-idrecurso',0);
   tbody4.append(trclone);
}).on('change','.selcurso',function(ev){
      var tr=$(this).closest('tr');
      var idsel=tr.attr('data-idrecurso')||0;
      var idcurso=$(this).val();
      var donde=tr.find('.aquirecursos');
      donde.html('');
      sesiones({idcurso:idcurso,idsel:idsel,donde:donde});
}).on('click','.btn-savefila',function(ev){
  var tr=$(this).closest('tr');
   var idtestasigancion=tr.attr('data-idtestasigancion')||0;
    var fd = new FormData(); 
    fd.append('idtestasigancion', idtestasigancion);
    fd.append('txtIdtest', $('input[name="idTest"]').val());
    fd.append('txtIdcurso', tr.find('select.selcurso').val());
    fd.append('txtIdrecurso', tr.find('select.aquirecursos').val());
    fd.append('txtSituacion', 1);    
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test_asignacion/guardarTest_asignacion',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){       
          tr.attr('data-idtestasigancion',data.newid);         
        }
    })       // var tb3=$('table#tblEscalas3');
})


  $('img.changeimage').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      var $tmpmedia=$(this)
      var file=document.createElement('input');
      var nameimagen=$(this).attr('alt');
      file.id='file_'+Date.now();
      file.type='file';
      file.accept='image/x-png, image/gif, image/jpeg, image/*';
      file.name='file'+nameimagen;
      file.className="input-file-invisible file"+nameimagen;
      file.style='height:0px; width:0px;';
      file.addEventListener('change',function(ev){
          var rd = new FileReader();
          rd.onload = function(filetmp){
            var filelocal = filetmp.target.result;
            $tmpmedia.attr('src',filelocal);
          }
          rd.readAsDataURL(this.files[0]);
          var hayfile=$tmpmedia.parent().children('input[type="file"]');
          if(hayfile) hayfile.remove();
          $tmpmedia.parent().append(file);
      })
    file.click();
  })

  $('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }
    }
  });


  var initSetting=function(){
    if(datosiniciofrm.idtest!=0&&datosiniciofrm.idtest!=undefined){
      var puntaje=JSON.parse(datosiniciofrm.puntaje||'[]');
      $.each(puntaje,function(i,v){
        agregarfilatblEscalas({code:v.code,nombre:v.nombre,descripcion:v.description});
      })
    }
  }

  initSetting();


  var saveSetting=function(obj){
    var txtpuntaje=[];
    $('.table#tblEscalas').find('tbody tr').each(function(i,v){
      var tr=$(v);
      if(!tr.hasClass('tr_clone'))txtpuntaje.push({code:tr.find('.txtcode').val(),nombre:tr.find('.txtnombre').val(),description:tr.find('.txtdescripcion').val()});
    })

    var fd = new FormData();
    fd.append('txtTitulo', $('input[name="txtTitulo"]').val()||'');
    fd.append('txtPuntaje', JSON.stringify(txtpuntaje));
    fd.append('txtMostrar', $('input[name="txtMostrar"]').val()||'');
    fd.append('idTest', $('input[name="idTest"]').val());
    $('#frmdatos11').find('img.changeimage').each(function(i,v){
      var nomimg=$('img.changeimage').attr('alt');
      if($('input.file'+nomimg).length){
        fd.append(nomimg, $('input.file'+nomimg)[0].files[0]);
      }
    })
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test/guardarTest',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : true,
      callback:function(data){
        if(_isFunction(obj.callback)){
          $('input[name="idTest"]').val(data.newid);       
          datosiniciofrm.idtest=data.newid;
         obj.callback(data);
        }
      }
    })
  }



  var agregarcriterios=function(obj){
    var tb3=$('table#tblEscalas3');
    var n=tb3.find('tr').length-1;    
    var acc='<i class="btn btn-success  btn-savefila fa fa-save "></i><i class="btn btn-danger  btn-removefila fa fa-trash"></i>';
    var html='<tr data-idtestcriterio="'+(obj.idtestcriterio||0)+'"><td class="criterionum">'+n+'</td><td><input type="text" name="txtcriterio" class="form-control gris " placeholder="e.g.: A,*,enproceso" value="'+(obj.criterio||'')+'"></td><td class="filavalores"></td><td class="">'+acc+'</td></tr>';
    tb3.find('tbody').append(html);
  }
  var agregarvaloreacriterios=function(){
    var trtb1=$('table#tblEscalas').children('tbody').children('tr');
    var html='<div class="select-ctrl-wrapper select-azul"><select class="select-ctrl form-control">';
    $.each(trtb1,function(i,v){
      if(!$(v).hasClass('tr_clone'))
      html+='<option value="'+($(v).find('input.txtcode').val()||'')+'">'+($(v).find('input.txtnombre').val()||'')+'</option>';
    })
    html+='</select></div>';
    $('#tblEscalas3').find('.filavalores').html(html);
  }

  $('.btnsavePaso1').click(function(ev){
      btn=$(this);    
      saveSetting({callback:function(data){
        if(data.code=='ok'){
          $('a[href="#tabcriterios"]').trigger('click');          
        }
      }});
  })
  $('.btnsavePaso2').click(function(ev){
    $('a[href="#tabasignacion"]').trigger('click');
  });
  $('.btnbackPaso1').click(function(ev){
    $('a[href="#tabdatos"]').trigger('click');
  })
  $('.btnbackPaso2').click(function(ev){
    $('a[href="#tabcriterios"]').trigger('click');
  })


  $('a[href="#tabcriterios"]').on('click',function(ev){
    ev.preventDefault();
     var idTest=parseInt($('input[name="idTest"]').val()||'0');
     if(idTest==0){
        ev.stopPropagation();
        $('a[href="#tabdatos"]').trigger('click');
        return ;
     }
     $('._nomtitulo').text($('input[name="txtTitulo"]').val());
     $('._nomimagen').attr('src',$('img.nomimagen').attr('src'));
     var tbtbr2=$('#tblEscalas2').find('tbody');
     var trs=$('#tblEscalas').find('tbody').children('tr');
     tbtbr2.find('tr').remove();
     $.each(trs,function(i,v){
      var trtmp=$(v);
      if(!trtmp.hasClass('tr_clone')){
     // var td1=trtmp.children('td:eq(0)').children('input').val();
      var td2=trtmp.children('td:eq(1)').children('input').val();
      var td3=trtmp.children('td:eq(2)').children('input').val();
      var htmltr='<tr><td class="text-center">'+td2+'</td><td class="text-justify">'+td3+'</td></tr>';
      tbtbr2.append(htmltr);
      }      
     })
    $('#tblEscalas3').children('tbody').html('');
      var fd = new FormData();
      fd.append('idtest', $('input[name="idTest"]').val());   
      sysajax({
        fromdata:fd,
        url:_sysUrlBase_+'/test_criterios/buscarjson',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok : false,
        callback:function(data){      
          $.each(data.data,function(ii,vv){
            agregarcriterios(vv);
            agregarvaloreacriterios();
          })
        }
      })
  })

  $('a[href="#tabasignacion"]').on('click',function(ev){
    ev.preventDefault();
     var idTest=parseInt($('input[name="idTest"]').val()||'0');
     if(idTest==0){
        ev.stopPropagation();
        $('a[href="#tabdatos"]').trigger('click');
        return ;
     }
     var trs=$('table#tblEscalas4').children('tbody').children('tr');
     $.each(trs,function(i,v){
      var tr=$(v);
      var idsel=tr.attr('data-idrecurso')||0;
      var idcurso=tr.attr('data-idcurso')||tr.find('.selcurso').val();
      var donde=tr.find('.aquirecursos');
      donde.html('');
      sesiones({idcurso:idcurso,idsel:idsel,donde:donde});
     })
  })

  

  var tiporecurso={'U':'<?php echo JrTexto::_('Unity') ?>','L':'<?php echo JrTexto::_('Activity') ?>','E':'<?php echo JrTexto::_('Quiz') ?>'}
 
  var sesiones=function(obj){
    var haysesion=getsesionescurso(obj.idcurso,obj.idsel||0);
    if(haysesion!=false){
       obj.donde.html(haysesion);
       return;
    }
    var fd = new FormData();
    fd.append('idcurso', obj.idcurso);   
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/acad_cursodetalle/sesionesjson',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : false,
      callback:function(data){        
        var html=convertirdatos(data.sesiones,obj.idsel||0)
        sessionStorage.setItem('curso'+obj.idcurso,JSON.stringify({datos:data.sesiones,tiempo:Date.now()}));
        obj.donde.html(html);
      }
    })
  }
  var convertirdatos=function(datos,sel){
    html=''
    $.each(datos,function(ii,vv){ //hijos=vv["hijo"]||false;
      var tipo=vv["tiporecurso"];
      html+='<option value="'+vv["idcursodetalle"]+'" '+(sel==vv["idcursodetalle"]?'selected="selected"':'')+'>'+tiporecurso[tipo]+': '+vv["nombre"]+'</option>';
    })
    return html;
  }

  var getsesionescurso=function(idcurso,idsel){
    var haydatos=sessionStorage.getItem('curso'+idcurso)||false;
    if(!haydatos)return false;
    var dt=JSON.parse(haydatos);
    var tiempotranscurrido=(Date.now()-dt.tiempo)/60000;
    if(tiempotranscurrido>15) return false;
    return convertirdatos(dt.datos,idsel);
  }
});
</script>

