<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$_rol_tmp = array();
$id_roles = array();
if(!empty($this->roles)){
  foreach($this->roles as $r){ 
    if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10) 
      continue;
    $_rol_tmp[] = $r['rol'];
    $id_roles[] = $r['idrol'];
  }
}

$json_id_roles = json_encode($id_roles);
$json_roles = json_encode($_rol_tmp);
unset($_rol_tmp);
unset($id_roles);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Acad_licencias'">&nbsp;<?php echo JrTexto::_('Licencias'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="frm<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <div class="panel-body">
        <div id="msj-interno"></div>
          <div class="col-xs-12 col-sm-12 col-md-12">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#home"><?php echo JrTexto::_('All Companies'); ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#menu1"><?php echo  ucfirst(JrTexto::_("Company"))?></a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="home">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <label><?php echo JrTexto::_('All Companies'); ?></label>
                <!--<div class="pnlimprimir">-->
                <div class="">
                  <div class="col-xs-12 col-sm-12 col-md-12" style="padding:0;">
                      <div class="chart-container" id="Container_LicenciaComparativaAll" style="position: relative; margin:0 auto; height:100%; width:100%">
                          <canvas id="chartLicenciaComparativaAll"></canvas>
                      </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12" style="padding:0;">
                    <div class="table-responsive">
                      <label class="text-center alert alert-danger"><?php echo JrTexto::_("Licenses Assigned");?> / <?php echo JrTexto::_('Used Licenses'); ?></label>
                      <table class="table table-resonsive table-striped" id="textComparissonLincenseAll" style="border-left:1px solid gray; border-right:2px solid gray;">
                        <thead>
                          <th><?php echo JrTexto::_('Company'); ?></th>
                          <th><?php echo JrTexto::_("Docente");?></th>
                          <th><?php echo JrTexto::_('Alumno'); ?></th>
                          <th><?php echo JrTexto::_('Supervisor'); ?></th>
                          <th><?php echo JrTexto::_('Director-IEE'); ?></th>
                          <th><?php echo JrTexto::_('Director-UGEL'); ?></th>
                          <th><?php echo JrTexto::_('Director-Dre'); ?></th>
                          <th><?php echo JrTexto::_('Director-Nacional'); ?></th>
                          <th><?php echo JrTexto::_('Workbooks'); ?></th>
                          <th><?php echo JrTexto::_('M.O.P'); ?></th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>TestCompany</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                            <td>0 / 0</td>
                          </tr>
                          <!-- <?php 
                            if(!empty($this->roles)){
                              foreach($this->roles as $r){
                                if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10)
                                  continue;
                                echo '<tr id="idrol_lincenseAll'.@$r["idrol"].'">';
                                echo '<td class="nom">'.$r["rol"].'</td>';
                                echo '<td class="asign">0</td>';
                                echo '<td class="used">0</td>';
                                echo '</tr>';
                              }
                            }
                            ?> -->
                            <!-- <tr id="workbook_lincenseAll">
                              <td ><?php echo JrTexto::_('Workbooks');?></td>
                              <td class="asign">0</td>
                              <td class="used">0</td>
                            </tr>
                            <tr id="MOP_lincenseAll">
                              <td ><?php echo JrTexto::_('M.O.P');?></td>
                              <td class="asign">0</td>
                              <td class="used">0</td>
                            </tr> -->
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!--END LICENCE-->
                </div>
                <!--END IMPRIMIR-->
              </div>
            </div>
            <!--END HOME-->
            <div class="tab-pane fade" id="menu1">
              <div class="col-xs-12 col-sm-6 col-md-6">
            
                <label><?php echo  ucfirst(JrTexto::_("Company"))?></label>
                    <div class="form-group">
                        <div class="cajaselect">
                            <select id="idempresa<?php echo $idgui; ?>" name="idempresa" name="rol" class="form-control" >  
                                <!--option value="0" ><?php echo  ucfirst(JrTexto::_("Select a business"))?></option-->
                                <?php 
                                if(!empty($this->empresas))
                                foreach($this->empresas as $emp){?>
                                    <option value="<?php echo $emp["idempresa"]; ?>" ><?php echo  ucfirst($emp["nombre"]);?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12"><br></div>
                <div class="pnlimprimir">
                    
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                      <h3 class="nomempresa"></h3>
                      <span><b><?php echo ucfirst(JrTexto::_("Date Start")) ?></b> : <span class="fechainicio<?php echo $idgui; ?>"><?php echo @$frm["fecha_inicio"];?></span></span>
                      <span><b><?php echo ucfirst(JrTexto::_("Date Finish")) ?></b> : <span class="fechafinal<?php echo $idgui; ?>"><?php echo @$frm["fecha_final"];?></span></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9" style="padding:0;">
                        <div class="chart-container" id="Container_LicenciaComparativa" style="position: relative; margin:0 auto; height:100%; width:100%">
                            <canvas id="chartLicenciaComparativa"></canvas>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3" style="padding:0;">
                      <div class="table-responsive">
                        <table class="table table-resonsive table-striped" id="textComparissonLincense" style="border-left:1px solid gray; border-right:2px solid gray;">
                          <thead>
                            <th><?php echo JrTexto::_('Role'); ?></th>
                            <th><?php echo JrTexto::_("Licenses Assigned");?></th>
                            <th><?php echo JrTexto::_('Used Licenses'); ?></th>
                          </thead>
                          <tbody>
                            <?php 
                              if(!empty($this->roles)){
                                foreach($this->roles as $r){
                                  if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10)
                                    continue;
                                  echo '<tr id="idrol_lincense'.@$r["idrol"].'">';
                                  echo '<td class="nom">'.$r["rol"].'</td>';
                                  echo '<td class="asign">0</td>';
                                  echo '<td class="used">0</td>';
                                  echo '</tr>';
                                }
                              }
                              ?>
                              <tr id="workbook_lincense">
                                <td ><?php echo JrTexto::_('Workbooks');?></td>
                                <td class="asign">0</td>
                                <td class="used">0</td>
                              </tr>
                              <tr id="MOP_lincense">
                                <td ><?php echo JrTexto::_('M.O.P');?></td>
                                <td class="asign">0</td>
                                <td class="used">0</td>
                              </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <!--END LICENCE-->
                    <div class="col-xs-12 col-sm-12 col-md-12 table-resonsive">
                        <label><?php echo  ucfirst(JrTexto::_("License"))?></label>
                        <table  class="table table-resonsive table-striped"> 
                          <tr><th><?php echo ucfirst(JrTexto::_("role"));?></th>
                          <th><?php echo ucfirst("N° de licencias");?></th>
                          <th><?php echo ucfirst("Costo unitario");?> $</th>
                          <th><?php echo ucfirst("Subtotal");?> $ </th>
                          </tr>
                          <?php
                          if(!empty($frm["costoxrol"])){
                            $costoxrol=json_decode($frm["costoxrol"],true);
                          }
                          if(!empty($this->roles))
                              foreach($this->roles as $r){ if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10) continue;?>
                              <tr class="costoroles" idrol="<?php echo @$r["idrol"];?>">
                              <td><?php echo ucfirst(JrTexto::_("Role"))." ".ucfirst($r["rol"]);?></td>
                              <td class="idrol<?php echo @$r["idrol"];?>"><?php echo @$costoxrol["idrol".$r["idrol"]];?></td>
                              <td class="crol<?php echo @$r["idrol"];?>"><?php echo @$costoxrol["crol".$r["idrol"]];?></td>
                              <td class="srol<?php echo @$r["idrol"];?> text-right"><?php echo  (!empty($costoxrol["idrol".$r["idrol"]])?$costoxrol["idrol".$r["idrol"]]:0)*(!empty($costoxrol["crol".$r["idrol"]])?$costoxrol["crol".$r["idrol"]]:0);?></td>
                              </tr>
                          <?php }?>
                              <tr class="downloadworkbook">
                              <td><?php echo ucfirst(JrTexto::_("Download")." ".JrTexto::_("Workbooks"));?></td>
                              <td class="nworkbook"><?php echo @$costoxdescarga["nworkbook"];?></td>
                              <td class="cworkbook"><?php echo @$costoxdescarga["cworkbook"];?></td>
                              <td class="sworkbook  text-right"><?php echo  (!empty($costoxdescarga["nworkbook"])?$costoxdescarga["nworkbook"]:0)*(!empty($costoxdescarga["cworkbook"])?$costoxdescarga["cworkbook"]:0);?></td>
                              </tr>
                              <tr class="downloadmop">
                              <td><?php echo ucfirst(JrTexto::_("Download")." ".JrTexto::_("M.O.P"));?></td>
                              <td class="nmop"><?php echo @$costoxdescarga["nmop"];?></td>
                              <td class="cmop"><?php echo @$costoxdescarga["cmop"];?></td>
                              <td class="smop text-right"><?php echo  (!empty($costoxdescarga["nmop"])?$costoxdescarga["nmop"]:0)*(!empty($costoxdescarga["cmop"])?$costoxdescarga["cmop"]:0);?></td>
                              </tr>
                              <tr><th colspan="4"><hr></th></tr>
                              <tr>
                              <th colspan="3" class="text-right">Total</th>
                              <td class="total text-right">$ 1000</td>
                              </tr>
                        </table>
                    </div>
                    <!--END TABLE-->
                    <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-12 text-center">
                      <a type="button" class="btn btn-warning btn-imprimir"><i class=" fa fa-print"></i> <?php echo JrTexto::_('Print');?></a>
                    </div>
                  </div>
                  <!--START USADOS-->
                    <!--
                    <div class="col-xs-12 col-sm-12 col-md-12 table-resonsive">
                        <label><?php echo  ucfirst(JrTexto::_("Used Licenses"))?></label>
                        <table  class="table table-resonsive table-striped"> 
                          <tr><th><?php echo ucfirst(JrTexto::_("role"));?></th>
                          <th><?php echo ucfirst("N° de licencias");?></th>
                          </tr>
                          <?php 
                          if(!empty($frm["costoxrol"])){
                              $costoxrol=json_decode($frm["costoxrol"],true);
                          }
                          if(!empty($this->roles))
                              foreach($this->roles as $r){ if($r["idrol"]==1||$r["idrol"]==5||$r["idrol"]==10) continue;?>
                              <tr class="costoroles" idrol="<?php echo @$r["idrol"];?>">
                              <td><?php echo ucfirst(JrTexto::_("Role"))." ".ucfirst($r["rol"]);?></td>
                              <td class="idrolc<?php echo @$r["idrol"];?>"><?php echo @$costoxrol["idrol".$r["idrol"]];?></td>
                              </tr>
                          <?php }?>
                              <tr class="downloadworkbook">
                              <td><?php echo ucfirst(JrTexto::_("Download")." ".JrTexto::_("Workbooks"));?></td>
                              <td class="nworkbook1"><?php echo @$costoxdescarga["nworkbook"];?></td>
                              </tr>
                              <tr class="downloadmop">
                              <td><?php echo ucfirst(JrTexto::_("Download")." ".JrTexto::_("M.O.P"));?></td>
                              <td class="nmop1"><?php echo @$costoxdescarga["nmop"];?></td>
                              </tr>
                        </table>
                    </div>
                    -->
                    <!--END USADOS-->
                </div>
                <!--END IMRPIMIR-->
                <div class="nolicencias">
                  <h3 class="text-center">Esta empresa no tiene licencias asignadas</h3>
                </div>
            </div>
            <!--END MENU CONTAINER--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

var costoroles={};
var cont = [];
var roles = <?php echo (!empty($json_roles)) ? $json_roles : 'null' ?>;
var id_roles = <?php echo (!empty($json_id_roles)) ? $json_id_roles : 'null' ?>;

function drawChart2(obj,dat,texto  = 'empty' ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: false,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Difference');
            }
          }
        }
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !item.text.includes('hide');
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }
          

          ci.update();
        }
      }//end legend
    }
  });
}

var drawComparativa = function(){
    
    var datosRadar = new Array(roles.length);
    var datosRadar2 = new Array(roles.length);
    var datosRadar3 = new Array(roles.length);
    var datosRadar4 = new Array(roles.length);
    
    datosRadar = datosRadar.fill(0);
    datosRadar2 = datosRadar2.fill(0);
    datosRadar3 = datosRadar3.fill(0);
    datosRadar4 = datosRadar4.fill(0);

    if(cont.length > 0 && typeof cont != 'undefined' && id_roles.length > 0 && typeof id_roles != 'undefined'){
        if(costoroles != null){
          for(var i = 0;i < id_roles.length; i++ ){
            datosRadar[i] = costoroles['idrol'+id_roles[i]];
          }//end for datos
          datosRadar[datosRadar.length - 2] = parseInt(costoroles['idrol3']) + parseInt(costoroles['idrol2']);
          datosRadar[datosRadar.length - 1] = costoroles['idrol2'];
        }
        for(var i = 0;i < id_roles.length; i++ ){
          datosRadar2[i] = cont[id_roles[i]];
        }//end for datos
        datosRadar2[datosRadar2.length - 2] = cont[3] + cont[2];
        datosRadar2[datosRadar2.length - 1] = cont[2];
    }

    for(var i = 0; i < datosRadar.length; i++){
        if(datosRadar[i] > datosRadar2[i]){
        datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }else if(datosRadar[i] < datosRadar2[i]){
        datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
        }
    }
    var barChartData = {
        labels: roles,
        datasets: [{
            label: 'Licenses Assigned',
            backgroundColor: '#36a2eb',
            stack:'Stack 0',
            data: datosRadar
        }, {
            label: 'Used Licenses',
            backgroundColor: '#ff6384',
            stack:'Stack 1',
            data: datosRadar2
        },{
            label: 'Difference',
            backgroundColor: '#d4b02f',
            stack: 'Stack 0',
            data: datosRadar3
        },{
            label: 'hide',
            backgroundColor: '#d4b02f',
            stack: 'Stack 1',
            data: datosRadar4
        }]

    };
    $("#Container_LicenciaComparativa").html(" ").html('<canvas id="chartLicenciaComparativa"></canvas>');
    drawChart2("chartLicenciaComparativa",barChartData);
};

var setAsignlincense = function(){
  for(var i = 0;i < id_roles.length; i++ ){
    $('#textComparissonLincense').find('#idrol_lincense'+id_roles[i]).find('td.asign').text(costoroles['idrol'+id_roles[i]]);
  }
  $('#textComparissonLincense').find('#workbook_lincense').find('td.asign').text(parseInt(costoroles['idrol3']) + parseInt(costoroles['idrol2']));
  $('#textComparissonLincense').find('#MOP_lincense').find('td.asign').text(costoroles['idrol2']);
}
var setUsedlincense = function(){
  for(var i = 0;i < id_roles.length; i++){
    $('#textComparissonLincense').find('#idrol_lincense'+id_roles[i]).find('td.used').text(cont[id_roles[i]]);
  }
    $('#textComparissonLincense').find('#workbook_lincense').find('td.used').text(cont[3] + cont[2] );
    $('#textComparissonLincense').find('#MOP_lincense').find('td.used').text( cont[2]);
}

// function drawBarHorizontal(obj,dat,texto  = 'empty' ){
//   var ctx = document.getElementById(obj).getContext('2d');
//   var myMixedChart = new Chart(ctx, {
//     type: 'horizontalBar',
//     data: dat,
//     options: {
//       responsive: true,
//       title: {
//         display: false,
//         text: texto
//       },
//       tooltips: {
//         custom: function(tooltipModel) {
//           if(tooltipModel.body){
//             var strline = tooltipModel.body[0].lines[0];
//             var search = strline.search('hide');
//             if(search != -1){
//               tooltipModel.width = 125;
//               tooltipModel.body[0].lines[0] = strline.replace('hide','Difference');
//             }
//           }
//         }
//       },
//       scales: {
//             xAxes: [{
//                 stacked: true,
//             }],
//             yAxes: [{
//                 stacked: true
//             }]
//         },
//       legend: {
//         labels: {
//           filter: function(item, chart) {
//             // Logic to remove a particular legend item goes here
//             return !item.text.includes('hide');
//           }
//         },
//         onHover: function(event, legendItem) {
//           document.getElementById(obj).style.cursor = 'pointer';
//         },
//         onClick: function(e,legendItem){
//           var index = legendItem.datasetIndex;
//           var ci = this.chart;
//           // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
//           var meta = ci.getDatasetMeta(index);
//           // alert(index);
//           if(index == 2){
//             var meta2 = ci.getDatasetMeta(3);
//             meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
//             meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

//           }else{
//             meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
//           }
          

//           ci.update();
//         }
//       }//end legend
//     }
//   });
// }
function initresumen(){
  $.ajax({
    url: _sysUrlBase_+'/acad_licencias/getresumen',
    type: 'POST',
    dataType: 'json',
    data: {},
  }).done(function(resp){
    if(resp.code == 'ok'){
      console.log(resp.data);
      var count_respdata = Object.keys(resp.data).length;
      if(count_respdata>0){
        var empresas = []; var totalLicencias = [];
        for(var i = 0; i < count_respdata;i++){
          var row = '<tr><td>'+resp.data[i].empresa+'</td><td>'+resp.data[i].usado[2]+' / '+resp.data[i].assignado.idrol2+'</td><td>'+resp.data[i].usado[3]+' / '+resp.data[i].assignado.idrol3+'</td><td>'+resp.data[i].usado[4]+' / '+resp.data[i].assignado.idrol4+'</td><td>'+resp.data[i].usado[6]+' / '+resp.data[i].assignado.idrol6+'</td><td>'+resp.data[i].usado[7]+' / '+resp.data[i].assignado.idrol7+'</td><td>'+resp.data[i].usado[8]+' / '+resp.data[i].assignado.idrol8+'</td><td>'+resp.data[i].usado[9]+' / '+resp.data[i].assignado.idrol9+'</td><td>'+(parseInt(resp.data[i].usado[2]) + parseInt(resp.data[i].usado[3]))+' / '+(parseInt(resp.data[i].assignado.idrol2) + parseInt(resp.data[i].assignado.idrol3))+'</td><td>'+resp.data[i].usado[2]+' / '+resp.data[i].assignado.idrol2+'</td></tr>';
          $('#textComparissonLincenseAll').find('tbody').append(row);
          empresas.push(resp.data[i].empresa);
          var total_u = parseInt(resp.data[i].usado[2]) + parseInt(resp.data[i].usado[3]) + parseInt(resp.data[i].usado[4]) + parseInt(resp.data[i].usado[6]) + parseInt(resp.data[i].usado[7]) + parseInt(resp.data[i].usado[8]) + parseInt(resp.data[i].usado[9]) + (parseInt(resp.data[i].usado[2]) + parseInt(resp.data[i].usado[3])) + parseInt(resp.data[i].usado[2]); 
          var total_a = parseInt(resp.data[i].assignado.idrol2) + parseInt(resp.data[i].assignado.idrol3) + parseInt(resp.data[i].assignado.idrol4) + parseInt(resp.data[i].assignado.idrol6) + parseInt(resp.data[i].assignado.idrol7) + parseInt(resp.data[i].assignado.idrol8) + parseInt(resp.data[i].assignado.idrol9) + (parseInt(resp.data[i].assignado.idrol2) + parseInt(resp.data[i].assignado.idrol3)) + parseInt(resp.data[i].assignado.idrol2);
          totalLicencias.push({'asignado': total_a,'usado':total_u});
        }//end foreach
        //dibujar comparativa
        var datosRadar = new Array(count_respdata);
        var datosRadar2 = new Array(count_respdata);
        var datosRadar3 = new Array(count_respdata);
        var datosRadar4 = new Array(count_respdata);
        
        datosRadar = datosRadar.fill(0);
        datosRadar2 = datosRadar2.fill(0);
        datosRadar3 = datosRadar3.fill(0);
        datosRadar4 = datosRadar4.fill(0);
        for(var i = 0; i < count_respdata;i++){
          datosRadar[i] = totalLicencias[i].asignado;
        }
        for(var i = 0; i < count_respdata;i++){
          datosRadar2[i] = totalLicencias[i].usado;
        }
        for(var i = 0; i < count_respdata; i++){
          if(datosRadar[i] > datosRadar2[i]){
            datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
          }else if(datosRadar[i] < datosRadar2[i]){
            datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
          }
        }
        var barChartData = {
          labels: empresas,
          datasets: [{
              label: 'Licenses Assigned',
              backgroundColor: '#36a2eb',
              stack:'Stack 0',
              data: datosRadar
          }, {
              label: 'Used Licenses',
              backgroundColor: '#ff6384',
              stack:'Stack 1',
              data: datosRadar2
          },{
              label: 'Difference',
              backgroundColor: '#d4b02f',
              stack: 'Stack 0',
              data: datosRadar3
          },{
              label: 'hide',
              backgroundColor: '#d4b02f',
              stack: 'Stack 1',
              data: datosRadar4
          }]
        };
        $("#Container_LicenciaComparativaAll").html(" ").html('<canvas id="chartLicenciaComparativaAll"></canvas>');
        drawChart2("chartLicenciaComparativaAll",barChartData);
      }
    }else{
      console.log(resp.msj);
      return false;
    }
  }).fail(function(xhr,textStatus,errorThrown){
    return false;
  });
}

$(document).ready(function(){
    /*$('#frm-<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm-<?php echo $idgui;?>'); 
        var formData = new FormData(myForm);
        var downloadjson={}; 
        var costoroles={};
        $('input.download').each(function(i,v){
          downloadjson[$(v).attr('name')]=$(v).val()||0;
        })
        formData.append('costoxdescarga', JSON.stringify(downloadjson));
        $('input.costoroles').each(function(i,v){
          costoroles[$(v).attr('name')]=$(v).val()||0;
        })
        formData.append('costoxrol', JSON.stringify(costoroles));
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_licencias/guardarAcad_licencias',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
             redir(_sysUrlBase_+'/acad_licencias');
          }
        }
        sysajax(data);
        return false;
      }
    });*/
    roles.push('Workbooks');
    roles.push('M.O.P');
    initresumen();
    $('.btn-imprimir').on('click',function(ev){
        $('.pnlimprimir').imprimir();
    })
    var idgui='<?php echo $idgui; ?>';
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })
    $('#idempresa'+idgui).on('change',function(event){
      event.preventDefault();
      costoroles = null;
      cont = [];
        var formData = new FormData(); 
       formData.append('idempresa', $('#idempresa'+idgui).val());
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_licencias/buscarjson',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : false,
          callback:function(data){
              data=data.data;
            if(data.length>0){

                data=data[0];
                var downloadjson={};
                costoroles={};
              $('h3.nomempresa').text(data.empresa);
              $('span.fechainicio'+idgui).text(data.fecha_inicio);
              $('span.fechafinal'+idgui).text(data.fecha_final);
              downloadjson=JSON.parse(data.costoxdescarga);
              console.log(downloadjson);
              $('input.download').each(function(i,v){
                $(v).val(downloadjson[$(v).attr('name')]||0);
              })
              formData.append('costoxdescarga', JSON.stringify(downloadjson));
              costoroles=JSON.parse(data.costoxrol);
              var total=0.00;
              $('tr.costoroles').each(function(i,v){
                var id=$(v).attr('idrol');
                $(v).children('td.idrol'+id).text(costoroles["idrol"+id]||0);
                $(v).children('td.crol'+id).text(costoroles["crol"+id]||0);
                var stotal=parseFloat(costoroles["idrol"+id]||0)*parseFloat(costoroles["crol"+id]||0);
                total=total+stotal;
                $(v).children('td.srol'+id).text(formatter.format(stotal)+' ');
              })
                $('tr.downloadworkbook').children('td.nworkbook').text(downloadjson["nworkbook"]||0);
                $('tr.downloadworkbook').children('td.cworkbook').text(downloadjson["cworkbook"]||0);
                stotal=parseFloat(downloadjson["nworkbook"]||0)*parseFloat(downloadjson["cworkbook"]||0);
                total=total+stotal;
                $('tr.downloadworkbook').children('td.sworkbook').text('$ '+ formatter.format(stotal));

                $('tr.downloadmop').children('td.nmop').text(downloadjson["nmop"]||0);
                $('tr.downloadmop').children('td.cmop').text(downloadjson["cmop"]||0);
                stotal=parseFloat(downloadjson["nmop"]||0)+parseFloat(downloadjson["cmop"]||0);
                total=total+stotal;
                $('tr.downloadmop').children('td.smop').text(formatter.format(stotal));
                $('td.total').html('<b>'+ formatter.format(total)+'</b>');
                $('.pnlimprimir').show();
                $('.nolicencias').hide();
                //draw comparativa
                drawComparativa();
                setAsignlincense();
            }else{
              $('.nolicencias').show();
              $('.pnlimprimir').hide();
            }
          }
        }
        sysajax(data);
        var data2={
          fromdata:formData,
          url:_sysUrlBase_+'/acad_licencias/buscarjson1',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : false,
          callback:function(data){
              data=data.data;
              console.log(data);
              cont = [];
              for (var i = 0; i < 11; i++) {
                cont[i]=0;
              }
              for (var i = 0; i < data.length; i++) {
                // console.log(data[i].idrol)
                switch(data[i].idrol){
                  case "1":
                    cont[1]++;
                    break;
                  case "2":
                    cont[2]++;
                    break;
                  case "3":
                    cont[3]++;
                    break;
                  case "4":
                    cont[4]++;
                    break;
                  case "5":
                    cont[5]++;
                    break;
                  case "6":
                    cont[6]++;
                    break;
                  case "7":
                    cont[7]++;
                    break;
                  case "8":
                    cont[8]++;
                    break;
                  case "9":
                    cont[9]++;
                    break;
                  case "10":
                    cont[10]++;
                    break;
                }
              }
              // console.log(cont)
               $('input.download').each(function(i,v){
                $(v).val(downloadjson[$(v).attr('name')]||0);
              })
               var totalmop = 0;
               totalmop = cont[2];
               var totalwork = 0;
               totalwork = cont[2]+cont[3];
              $('tr.costoroles').each(function(i,v){
                var id=$(v).attr('idrol');
                $(v).children('td.idrolc'+id).text(cont[id]||0);
              })
                $('tr.downloadworkbook').children('td.nworkbook1').text(totalwork||0);

                $('tr.downloadmop').children('td.nmop1').text(totalmop||0);
                drawComparativa();
                setUsedlincense();
                

          }
        }
        sysajax(data2);
        return false;
    })
    $('#idempresa'+idgui).trigger('change');
});
</script>