<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Partner"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 ">
              <label><?php echo  ucfirst(JrTexto::_("Business"))?></label>
                <div class="form-group">
                	<div class="cajaselect">            
                    <select id="idempresa<?php echo $idgui; ?>" name="idempresa" class="form-control" >  
                      <option value="0" ><?php echo  ucfirst(JrTexto::_("Select a business"))?></option>
                      <?php 
                      if(!empty($this->empresas))
                        foreach($this->empresas as $emp){?>
                          <option value="<?php echo $emp["idempresa"]; ?>" <?php echo $emp["idempresa"]==$this->idempresa?'selected="selected"':'';?> ><?php echo  ucfirst($emp["nombre"]);?></option>
                        <?php }
                      ?>
                    </select>
                  </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-12 col-md-4">
              <div class="form-group">
              <label><?php echo  ucfirst(JrTexto::_("text to Search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center"><br>
               <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("bolsa_empresas", "agregarsocio"));?>" data-titulo="<?php echo JrTexto::_("Partner").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                    <th>#</th>
                    <th><?php echo JrTexto::_("Partner") ;?></th>
                    <th><?php echo JrTexto::_("Phone") ;?></th>
                    <th><?php echo JrTexto::_("Email") ;?></th>
                    <th><?php echo JrTexto::_("Documento"); ?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5c11d0f36849f='';
function refreshdatos5c11d0f36849f(){
    tabledatos5c11d0f36849f.ajax.reload();
}
$(document).ready(function(){  
  var estados5c11d0f36849f={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5c11d0f36849f='<?php echo ucfirst(JrTexto::_("acad_licencias"))." - ".JrTexto::_("edit"); ?>';
  var draw5c11d0f36849f=0;

  $('.btnbuscar').click(function(ev){
    refreshdatos5c11d0f36849f();
  });
$('#idempresa<?php echo $idgui; ?>').change(function(){
  refreshdatos5c11d0f36849f();
})

  tabledatos5c11d0f36849f=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data':'#'},        
        {'data':'<?php echo JrTexto::_("Partner"); ?>'},
        {'data':'<?php echo JrTexto::_("Phone"); ?>'},
        {'data':'<?php echo JrTexto::_("Email"); ?>'},
        {'data':'<?php echo JrTexto::_("Documento"); ?>'},
        //{'data': '<?php echo JrTexto::_("Fecha_final") ;?>'},
        //{'data': '<?php echo JrTexto::_("Estado") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/personal/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true
            d.idempresa=$('#idempresa<?php echo $idgui; ?>').val();
            d.idrol=10;
            d.texto=$('#texto').val(),
            draw5c11d0f36849f=d.draw;
        },
        "dataSrc":function(json){
          var data=json.data;
          json.draw = draw5c11d0f36849f;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){
            var fullname=data[i].ape_paterno +' '+data[i].ape_materno+', '+data[i].nombre;
            var acc='<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/perfil/?idpersona='+data[i].idpersona+'&rol=10" data-titulo="<?php echo JrTexto::_('Ficha'); ?> '+fullname +'"><i class="fa fa-eye"></i></a>';
            acc+='<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/formulario/?idpersona='+data[i].idpersona+'&rol=10&return=bolsa_empresas/socios" data-titulo="<?php echo ucfirst(JrTexto::_('Personal'))." ".JrTexto::_('Edit'); ?>"><i class="fa fa-pencil"></i></a>';
            acc+='<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/cambiarclave/?idpersona='+data[i].idpersona+'&rol=10" data-titulo="<?php echo JrTexto::_('Change')." ".JrTexto::_('Password'); ?>"><i class="fa fa-key"></i></a>';
           
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Partner");?>': fullname,
              '<?php echo JrTexto::_("Phone");?>': data[i].telefono+' <br> '+data[i].celular,
              '<?php echo JrTexto::_("Email");?>': data[i].email,
              '<?php echo JrTexto::_("Documento");?>': data[i].strtipodoc+'<br>'+data[i].dni,
              '<?php echo JrTexto::_("Actions") ;?>' :acc
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_licencias', 'setCampo', id,campo,data);
          if(res) tabledatos5c11d0f36849f.ajax.reload();
        }
      });
  })

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5c11d0f36849f';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_licencias';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:true});
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'acad_licencias', 'eliminar', id);
        if(res) tabledatos5c11d0f36849f.ajax.reload();
      }
    }); 
  });
});
</script>