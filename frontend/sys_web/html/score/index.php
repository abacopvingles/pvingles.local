<?php

defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';

$progresos = json_encode($this->progresos);
$cursos = json_encode($this->cursoScore);
$texto01 = JrTexto::_('Skills obtained in the course');
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css">

<style type="text/css">
.row >.item-recurso{ text-align: center; padding: 1ex;
}
.row >.item-recurso .panel{ text-align: center; padding: 0.2ex; margin:0.05ex; 0.1ex;
}
.row >.item-recurso .panel-body{ padding: 0.1ex;
}
.slick-slide{ position: relative;
}

.cajaselect {  overflow: hidden;width: 230px;position:relative;font-size: 1.8em;
}
select#level-item,select#unidad-item ,select#actividad-item { background: transparent; border: 2px solid #4683af;    padding: 5px; width: 250px; padding: 0.3ex 2ex; 
}
select:focus{ outline: none;}

.cajaselect::after{ font-family: FontAwesome; content: "\f0dd"; display: inline-block; text-align: center; width: 30px; height: 100%; background-color: #4683af; position: absolute; top: 0; right: 0px; pointer-events: none; color: antiquewhite; bottom: 0px; }
.titulo{ border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000; width: 85%; background-color: rgba(255, 255, 255, 0.53); color: #000; }
.autor{ border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000; width: 85%; text-align: right;  background-color: rgba(255, 255, 255, 0.53); color: #000;
}
.caratula{ border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%   }
.border{ border: solid 0px #f00; }

.border2{ border: solid 0px #337AB7; border-radius: 5px; }

.div_mante { opacity: 0; -webkit-transition: opacity 500ms; -moz-transition: opacity 500ms; -o-transition: opacity 500ms; -ms-transition: opacity 500ms; transition: opacity 500ms; }

.card{ display: block; width: 100%; border: 1px solid #ccc; padding: 0; }
.card .card-title{ font-size: 15px; font-weight: bold; border-bottom: 1px solid #ccc; padding-left: 15px; line-height: 2.2; height:80px; }
.card .card-info{ font-size: 2.0em; text-shadow: 2px 1px 3px #aaa; font-weight: bolder; text-align: center; border-bottom: 1px solid #efefef; }
.card .card-info:last-child{ border-bottom:none; }
.card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }
.card.card-time .card-info .anio:after,
.card.card-time .card-info .mes:after,
.card.card-time .card-info .dia:after,
.card.card-time .card-info .hora:after,
.card.card-time .card-info .min:after,
.card.card-time .card-info .seg:after{
    font-size: 0.7em;
}
.card.card-time .card-info .anio:after{ content: "y"; }
.card.card-time .card-info .mes:after { content: "m"; }
.card.card-time .card-info .dia:after { content: "d"; }
.card.card-time .card-info .hora:after{ content: "h"; }
.card.card-time .card-info .min:after { content: "m"; }
.card.card-time .card-info .seg:after { content: "s"; }

.card .card-info .promedio{ line-height: 75px; }
.card .card-info .comentario span{ display: block; }
.card .card-info .comentario span.letras{ font-size: .5em; }
/*
* CUSTOM
*/
.time-container{ padding:10px 5px; float:none; display:inline-block; vertical-align:top; }
.div_menu{ height: 120px; font-size: 20px; text-align: center; padding: 40px; }
.color1{ background-color: #EB6B56; }
.color2{ background-color: #2C82C9; }
.color3{ background-color: #9365B8; }
.color4{ background-color: #FAC51C; }

.color5{ background-color: #61BD6D; }

.select-ctrl-wrapper:after{ right:0!important; height:30px; }
.progress{
    margin: 8px 5px 5px 5px;
}
.disabled{
    background-color:#e1e1e1;
}
.hvr-radial-out img{
    opacity:1;
}
.hvr-radial-out:hover img{
    opacity:0;
}

.nivelCircle{
    cursor:pointer;
}
.selected{
    background-color:red!important;
}
.foto{ cursor:pointer; }
.circleProgressBar1 div:nth-child(1){ margin:0 auto; }
.countPercentage{ font-size:1.5em; }
.item-activity{ cursor:pointer; }
.item-activity:hover div{ border:0px solid red; }
.item-activity:hover div{
    border:2px solid red;
}
.item-activity span{
    position: absolute;
    top: 39%;
    font-size: 2em;
    font-weight: bold;
    width: 100%;
    text-align: center;
    opacity:0;
    color:white;
    z-index:1;
    color: white;
    text-shadow: 1px 1px 2px black;
}
.item-activity:hover span{
    opacity:1;
}
.item-activity .tela{
    background-color: rgb(128, 128, 128);
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 0;
    opacity:0;
}
.item-activity .content{position:relative; width:88%; border-radius:50%; overflow: hidden; margin: 0 auto; height: 150px; border: 1px solid gray;}
.item-activity:hover .tela{ opacity:0.35; }
.nivelContent {display: inline-block; padding: 5px; width: 33%;text-align: center;}
.lockedcourse {display: inline-block;border-radius: 50%;/* margin: 0 auto; */position: relative;}
.courseactive {display: inline-block;border-radius: 50%;overflow: hidden; margin: 0 auto; box-shadow: 0px 1px 10px black;}
.card-unidad { border-radius:1em;border-color: #E67E22;border-width: 2px;box-shadow:0px 1px 2px;margin: 0;max-height: 600px;overflow-y: scroll; }
.unityContent { padding-top: 5px; padding-bottom: 5px;}
#child{
    box-shadow: 0 0 0 rgba(204,44,44, 0.5);
    animation: pulse 2s infinite;
    border-radius:50%;
}
.imgr{
    -webkit-animation: 3s rotate linear infinite;
    animation: 3s rotate linear infinite;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

@-webkit-keyframes pulse {
  0% {
    -webkit-box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
  }
  70% {
      -webkit-box-shadow: 0 0 0 10px rgba(204,44,44, 0);
  }
  100% {
      -webkit-box-shadow: 0 0 0 0 rgba(204,44,44, 0);
  }
}
@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
    box-shadow: 0 0 0 0 rgba(204,44,44, 0.5);
  }
  70% {
      -moz-box-shadow: 0 0 0 10px rgba(204,44,44, 0);
      box-shadow: 0 0 0 10px rgba(204,44,44, 0);
  }
  100% {
      -moz-box-shadow: 0 0 0 0 rgba(204,44,44, 0);
      box-shadow: 0 0 0 0 rgba(204,44,44, 0);
  }
}
/* .slick-initialized { visibility: visible!important; } */
</style>
<!--
    Units of Course
    Skills Obtainend in the Course
-->
<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li class="active">
          <?php 
              echo JrTexto::_("Scores");
          ?>
        </li>
	  </ol>	
	</div>
</div>

<div class="container">
    <!-- <div style="position:relative;  margin:10px 0;">
        <a href="<?php echo $this->documento->getUrlBase();?>" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
    </div> -->
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:left; background-color:#E67E22; font-weight:bold;">
            <h4><i class="btn-icon fa fa-pie-chart"></i>&nbsp;<?php echo JrTexto::_("Scores") ?></h4>
        </div>
        <div class="panel-body">
            <div class="row" style="margin:0 -7px;">
                <div class="col-md-12"><h4 style="margin:5px; padding:10px; border-bottom:3px solid #dadada;"><i class="fa fa-info"></i> <?php echo JrTexto::_('Student Information'); ?></h4></div>
                <div class="row" style="margin:0;">
                    <div class="col-md-3">
                        <div style="height:90px; margin-top:10px;">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                        </div>
                        <div class="" style="text-align:center;">
                            <h4><?php echo $this->user["nombre_full"]; ?></h4>
                        </div>
                    </div>
                    <!--end avatar photo-->
                    <!--START BARRA PROGRESO CURSO -->
                    <div class="col-md-9">
                        <!--start design-->
                        <div class='progresbarra' style="width:100%;height: 97px;border:1px solid gainsboro;border-radius: 1em; margin:2px auto;">
                            <p style=" display: inline-block; font-size: 11px; position: relative; left: 2%; font-weight: bold;"><?php echo JrTexto::_('Start');?></p>
                            <p class='progrsfinish' style=" display: inline-block; font-size: 11px; position: absolute; right: 2%; font-weight: bold;"><?php echo JrTexto::_('Finish'); ?></p>
            
                            <div style=" font-size: 2em; position: relative; width: 100%; height: 30px;" class="progresimg">
                                <i class="fa fa-flag" aria-hidden="true" style="position: absolute;left: 3%;opacity: 0.8;"></i>
                                <i class="fa fa-flag-checkered" aria-hidden="true" style="position: absolute;left: 96%;opacity: 0.8;"></i>
                                <div id="barraprogresototal" style=" height: 25px; width: 59%; margin-left: 29px; max-width: 93%; background-color: #daf6da; border-radius: 1em;">
                                    <i class="fa fa-child hvr-pulse" aria-hidden="true" style="position: absolute;left: 62%;color: #fd454a;" id="child"></i>
                                </div>
                            </div>
                            <div class="base" style="position:relative;width: 100%;top: 0px;height: 35px;">
                                <p style=" display: inline-block; position: absolute; bottom: 0; left: 3%; margin: 0; font-weight: bold;">A1</p>
                                <p style=" display: inline-block; position: absolute; bottom: 0; left: 50%; margin: 0; font-weight: bold;">A2</p>
                                <p style=" display: inline-block; position: absolute; bottom: 0; left: 96%; margin: 0; font-weight: bold;">B1</p>
                                <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 2%;/* position: relative; */position: absolute;z-index: 1;"></div>
                                <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 49%;position: absolute;z-index: 1;"></div>
                                <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 95%;position: absolute;z-index: 1;"></div>
                                <div class="line" style="position: absolute;width: 100%;height:3px;background:lightgrey;top:5px;z-index: 0;"></div>
                            </div>
                        </div>
                        <!--end design-->
                        <!--START CARD-->

                        <div class="card" id="cursosGeneral" style="border-radius:1em; border-color:#E67E22;; border-width:2px; box-shadow:0px 1px 2px;">
                            <div class="card-body" style=" /* padding: 5px; */ ">
                                <div style="padding: 5px;">
                                    <?php for($i=1;$i<=3;$i++): ?>
                                    <div class="nivelContent" style="display: inline-block;/* padding: 5px; */width: 33%;text-align: center;">
                                        <div class="nivelCircle disabled lockedcourse">
                                            <div class="foto hvr-hang" style=" width: 167px; height: 128px; position: relative;"></div>
                                            <div class="porcentaje" style="position: absolute;top: 20%;/* left: 30%; *//* margin: 0 auto; */width: 100%;font-size: 2em;font-weight: bold;/* opacity: 0; */">
                                                <span style="/* display: inline-block; */font-size: 2em;"><i class="fa fa-lock" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                                0%
                                            </div>                                                
                                        </div>

                                        <div class="select" style=" height: 7px; background-color: #e1e1e1; border: 1px solid #a4a4a4; border-bottom-color: gray;"></div>
                                    </div>
                                    <?php endfor; ?>
                                </div>
                            </div>
                        </div>
                        <!--END CARD-->
                    </div>
                    <!--END COLUMN-->
                </div>
            </div>
            <div class="col-md-12" id="contenedor-details"></div>
            <!--Start contenedor unidad 1-->
            <!-- <div class="row" id="A1-Detail">
                <div class="col-md-12">
                    <h4 class="title-detail" style="margin:5px; padding:10px; border-bottom:3px solid #dadada;"><i class="fa fa-info"></i>&nbsp;Unidades del Curso A1</h4>
                </div>
                <div class="col-md-12"> -->
                    <!--Start card-->
                    <!-- <div class="card row card-unidad">
                        <div class="unityContent col-md-12">
                            <div data-toggle="modal" data-target="#unityCompleteModal" class="unity col-md-4 hvr-outline-in" style="text-align: center;">
                                <div class="nameText" style="text-align: center;font-size: 1.5em;">Introductions</div>
                                <div  class="foto hvr-hang"  style="width: 100%;max-width: 265px;margin: 0  auto;height: 150px;border-radius: 1em;background-color: gray;overflow: hidden;">
                                    <img src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20170221083851.jpg" style="width: 100%;" />
                                </div>
                                    
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                        100%
                                    </div>                                                
                                </div>
                                <div class="textProgress" style="text-align: center;font-size: 2.0em;">
                                    <span class="textProgress-now" style="font-weight: bold;font-size: 1.5em;color: #5cb85c;">04</span>
                                    <span style="font-weight: bold;font-size: 1.4em;padding: 0 5px;">/</span>
                                    <span class="textProgress-total">04</span>
                                </div>
                            </div>
                            <div class="unity col-md-4 hvr-outline-in" style="text-align: center;">
                                <div class="nameText" style="text-align: center;font-size: 1.5em;">Number &amp; Locations</div>
                                <div class="foto hvr-hang" style="width: 100%;max-width: 265px;margin: 0  auto;height: 150px;border-radius: 1em;background-color: gray;overflow: hidden;">
                                    <img src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20170313033457.jpg" style="width:100%;">
                                </div>                                
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                        100%
                                    </div>                                                
                                </div>
                                <div class="textProgress" style="text-align: center;font-size: 2.0em;">
                                    <span class="textProgress-now" style="font-weight: bold;font-size: 1.5em;color: #5cb85c;">06</span>
                                    <span style="font-weight: bold;font-size: 1.4em;padding: 0 5px;">/</span>
                                    <span class="textProgress-total">06</span>
                                </div>
                            </div>
                        
                        </div>
                    </div> -->
                    <!--endcard-->
                <!-- </div>    
            </div> -->
            <!--End A1 Detail Content-->

            <!--start chart total x curso-->
            <div class="col-md-12" id="chartGeneral">
                <h1 class="loading-chart" style="font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading Skill</span></h1>
                <div id="renderchart" style="display: none;">
                    <div class="col-md-12">
                        <h4 style="margin:5px; padding:10px; border-bottom:3px solid #dadada;" id="chartTitle"><i class="fa fa-info"></i> <?php echo JrTexto::_('Skills progress for the course') ?> A1</h4>
                    </div>
                    <div class="col-md-12">
                        <div class="card row" style="border-radius:1em;border-color: #E67E22;border-width: 2px;box-shadow:0px 1px 2px;margin: 0;max-height: 600px;overflow-y: scroll;">
                            <div class="col-md-6">
                                <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
                                    <canvas id="pieChart"  width="948" height="674"></canvas>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-sm-6" style="text-align:center;">
                                    <h4>Listening</h4>
                                    <div id="barListening" class="circleProgressBar1" style="font-size:1em;"></div>
                                </div>
                                <div class="col-sm-6" style="text-align:center;">
                                    <h4>Reading</h4>
                                    <div id="barReading" class="circleProgressBar1" style="font-size:1em;"></div>
                                </div>
                                <div class="col-sm-6" style="text-align:center;">
                                    <h4>Writing</h4>
                                    <div id="barWriting" class="circleProgressBar1" style="font-size:1em;"></div>
                                </div>
                                <div class="col-sm-6" style="text-align:center;">
                                    <h4>Speaking</h4>
                                    <div id="barSpeaking" class="circleProgressBar1" style="font-size:1em;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End chart-->
                </div>
                <!--end renderchar-->
            </div>
            
        </div>
    </div>
</div>

<!-- Modal -->
<div id="unityCompleteModal" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <div id="ModalListening" class="col-sm-3" style="text-align:center;">
                <h4>Listening</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        <!-- 40% Complete (success) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="0"><span>0</span>%</div>
            </div>
            <div id="ModalReading" class="col-sm-3" style="text-align:center;">
                <h4>Reading</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    <!-- 50% Complete (info) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="0"><span>0</span>%</div>
            </div>
            <div id="ModalWriting" class="col-sm-3" style="text-align:center;">
                <h4>Writing</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        <!-- 60% Complete (warning) -->
                    </div>
                </div>            
                <div class="countPercentage" data-value="0"><span>0</span>%</div>
            </div>
            <div id="ModalSpeaking" class="col-sm-3" style="text-align:center;">
                <h4>Speaking</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        <!-- 70% Complete (danger) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="0"><span>0</span>%</div>
            </div>
        </div>
        <div id="felicidadesModal" style="display: none;" class="col-md-12">
            <div class="alert alert-success">
                <strong><?php echo JrTexto::_('Congratulations'); ?>!</strong><?php echo JrTexto::_('You have completed the unit'); ?>.<strong></strong><a href="#" class="alert-link"><?php echo JrTexto::_('Click to Continue another Unit') ?></a>.
            </div>
        </div>
        <div class="col-md-12">
            <!--start design-->
            <div id="barraActividades" style="width:100%;height: 97px;border:1px solid gainsboro;border-radius: 1em; margin:2px auto;"><p style="display: inline-block;font-size: 11px;position: relative;left: 2%;font-weight: bold;"><?php echo JrTexto::_('Start'); ?></p>
                <p style="display: inline-block;font-size: 11px;position: absolute;right: 2%;font-weight: bold;"><?php echo JrTexto::_('Finish'); ?></p>
        
                <div style="font-size: 2em;position: relative;width: 100%;height: 30px;"><i class="fa fa-flag" aria-hidden="true" style="position: absolute;left: 3%;opacity: 0.8;"></i>
                    <i class="fa fa-flag-checkered" aria-hidden="true" style="position: absolute;left: 96%;opacity: 0.8;"></i>
                    <div id="greenbarmodal" style="height: 25px;width: 0%;margin-left: 29px;max-width: 93%;background-color: #daf6da;border-radius: 1em;">
                    <i class="fa fa-child hvr-pulse" aria-hidden="true" style="position: absolute;left: 3%;color: #fd454a;" id="child"></i>
                    </div>
                </div>
                <div class="base" style="position:relative;width: 100%;top: 0px;height: 35px;">
                    
                </div>
            </div>
        </div>
        <!--end design-->
        <div class="col-md-12" style="margin-top: 10px; border: 2px solid #b3b3b3; border-radius: 1em; padding-top: 20px;">
            <div class="content-activity">
            <!--start-->
            <!-- <div class=""> -->
                    <!-- <div class="" style="width:100px; height:200px;"></div> -->
                    
                    <!-- </div> -->

                    <!--end-->
            </div>
        </div>
        <div id="continuarBoton" style="display: none;" class="col-md-12">
            <div style="border:1px solid gray;box-shadow:1px 2px 2px;border-radius:1em;padding: 10px;margin-top: 5px;text-align: center;">
                    <a class="btn btn-primary" href="#"><?php echo JrTexto::_('Continue the Unit'); ?></a>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default close2" data-dismiss="modal"><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="unityCompleteModal2" class="modal fade in" role="dialog">
  <div class="modal-dialog modal-lg">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title"><?php echo JrTexto::_('Unit Detail'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <div class="col-sm-3" style="text-align:center;">
                <h4>Listening</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:35%">
                        <!-- 40% Complete (success) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="35"><span>35</span>%</div>
            </div>
            <div class="col-sm-3" style="text-align:center;">
                <h4>Reading</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                    <!-- 50% Complete (info) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="60"><span>60</span>%</div>
            </div>
            <div class="col-sm-3" style="text-align:center;">
                <h4>Writing</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                        <!-- 60% Complete (warning) -->
                    </div>
                </div>            
                <div class="countPercentage" data-value="50"><span>50</span>%</div>
            </div>
            <div class="col-sm-3" style="text-align:center;">
                <h4>Speaking</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        <!-- 70% Complete (danger) -->
                    </div>
                </div>
                <div class="countPercentage" data-value="70"><span>70</span>%</div>
            </div>
        </div>
        
        <div class="col-md-12">
            <!--start design-->
            <div style="width:100%;height: 97px;border:1px solid gainsboro;border-radius: 1em; margin:2px auto;"><p style="
    display: inline-block;
    font-size: 11px;
    position: relative;
    left: 2%;
    font-weight: bold;
"><?php echo JrTexto::_('Start'); ?></p>
    <p style="
    display: inline-block;
    font-size: 11px;
    position: absolute;
    right: 2%;
    font-weight: bold;
"><?php echo JrTexto::_('Finish'); ?></p>
    
    <div style="
    font-size: 2em;
    position: relative;
    width: 100%;
    height: 30px;
"><i class="fa fa-flag" aria-hidden="true" style="
    position: absolute;
    left: 3%;
    opacity: 0.8;
"></i><i class="fa fa-flag-checkered" aria-hidden="true" style="
    position: absolute;
    left: 96%;
    opacity: 0.8;
"></i>
   	<div style="
    height: 25px;
    width: 68%;
    margin-left: 29px;
    max-width: 93%;
    background-color: #daf6da;
    border-radius: 1em;
"><i class="fa fa-child hvr-pulse" aria-hidden="true" style="
    position: absolute;
    left: 71%;
    color: #fd454a;
    " id="child"></i></div></div>
                        <div class="base" style="position:relative;width: 100%;top: 0px;height: 35px;">
    <p style="
    display: inline-block;
    position: absolute;
    bottom: 0;
    left: 3%;
    margin: 0;
    font-weight: bold;
">S1</p>
                            <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 2%;/* position: relative; */position: absolute;z-index: 1;"></div>
                            <p style="
    display: inline-block;
    position: absolute;
    bottom: 0;
    left: 96%;
    margin: 0;
    font-weight: bold;
">S3</p>
                            <p style="
    display: inline-block;
    position: absolute;
    bottom: 0;
    left: 50%;
    margin: 0;
    font-weight: bold;
">S2</p><div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 95%;position: absolute;z-index: 1;"></div>
                        <div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 49%;position: absolute;z-index: 1;"></div><div class="line" style="position: absolute;width: 100%;height:3px;background:lightgrey;top:5px;z-index: 0;"></div></div>
                    </div>
                    <!--end design-->
        </div>
        <div class="col-md-12" style="margin-top: 10px; border: 2px solid #b3b3b3; border-radius: 1em; padding-top: 20px;">
            <div class="content-activity">
            <!--start-->
            <!-- <div class=""> -->
                    <!-- <div class="" style="width:100px; height:200px;"></div> -->
                    <div class="item-activity" style="">
                        <div class="content" style="">
                            <span>100%</span>
                            <div class="tela"></div>
                            <img style="width:100%;" src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20171107054303.jpg">
                        </div>
                    </div>
                    <div class="item-activity" style="">
                        <div class="content">
                            <span>50%</span>
                            <div class="tela"></div>
                            <img style="width:100%;" src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20171129050548.jpg">
                        </div>
                    </div>
                    <div class="item-activity" style="">
                        <div class="content">
                            <span>0%</span>
                            <div class="tela"></div>
                            <img style="width:100%;" src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20171129050936.jpg">
                        </div>
                    </div>
                    
                    <!-- </div> -->

                    <!--end-->
            </div>
        </div>
        <div class="col-md-12">
            <div style="border:1px solid gray;box-shadow:1px 2px 2px;border-radius:1em;padding: 10px;margin-top: 5px;text-align: center;">
                    <a class="btn btn-primary" href="#">Continuar la unidad</a>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>




<!-- <script src="<?php echo $this->documento->getUrlBase();?>/static/libs/sliders/slick/slick.min.js"></script> -->
<!-- C:\wampserver\www\antonio\pvingles.local\static\libs\sliders\slick\slick.js -->
<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">

var progresos = <?php echo $progresos ?>;
var cursos = <?php echo $cursos ?>;

function countText(obj,valueText, ToValue){
  let currentValue = parseInt(valueText);
  let nextVal = ToValue;
  let diff = nextVal - currentValue;
  let step = ( 0 < diff ? 1 : -1 );
  for (var i = 0; i < Math.abs(diff); ++i) {
      setTimeout(function() {
          currentValue += step
          obj.text(currentValue);
      }, 50 * i)   
  }
}

function drawPie(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: {
            // responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Skill'
            }
        }
    });
}

function drawHabilidad(obj , option = null, value = 0, speed = null){
    var _anim = speed === null ? 1 : speed;
    var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
    option.animationSpeed = _anim;
    var bar=new RadialProgress(obj,option);
    bar.setValue(value);
    bar.draw(true);
  }

function drawChart(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Chart.js Bar Chart'
        }
    }
  });
}

/**

<div class="nivelContent" style="display: inline-block; padding: 5px; width: 33%;text-align: center;margin-right: 4px;">
<div class="nivelCircle hvr-radial-out" data-content="#A2-Detail" data-namecourse="A2" style="display: inline-block;border-radius: 50%;overflow: hidden; margin: 0 auto; box-shadow: 0px 1px 10px black;">
                <img src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20180924053911.png" style="position: absolute; left: 0; height: 100%;">
                <div class="foto" style=" width: 167px; height: 128px; position: relative;"></div>
                <div class="porcentaje" style="position: absolute;top: 17%; left: 30%; margin: 0 auto; width: 100%;font-size: 4em;font-weight: bold;text-shadow: 1px 2px 1px black;color: #ffffff;">
                    A2
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    40%
                </div>                                                
            </div>

            <div class="select" style=" height: 7px; background-color: #e1e1e1; border: 1px solid #a4a4a4; border-bottom-color: gray; margin: 0px 4px; "></div>
        </div>

        <div class="nivelContent" style="display: inline-block; padding: 5px; width: 33%;text-align: center;">
            <div class="nivelCircle disabled" style="display: inline-block;border-radius: 50%; margin: 0 auto; position: relative;">
                <div class="foto hvr-hang" style=" width: 167px; height: 128px; position: relative;"></div>
                <div class="porcentaje" style="position: absolute;top: 20%; left: 30%; margin: 0 auto; width: 100%;font-size: 2em;font-weight: bold; opacity: 0; ">
                    <span style="display: inline-block; font-size: 2em;"><i class="fa fa-lock" aria-hidden="true"></i></span>
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    0%
                </div>                                                
            </div>
            <div class="select" style=" height: 7px; background-color: #e1e1e1; border: 1px solid #a4a4a4; border-bottom-color: gray;"></div>
        </div>

<div class="select" style="
    height: 7px;
    background-color: #e1e1e1;
    border: 1px solid #a4a4a4;
    border-bottom-color: gray;
"></div>
 */
// var _sysUrlBase_2 = 'https://abacoeducacion.org/web';
var _sysUrlBase_2 = _sysUrlBase_;

var contarHijosCompletado = function(obj){
    var resultado = 0;
    for(var i in obj){
        if(parseInt(obj[i]['progreso']) >= 50){
            resultado += 1;
        }
    }
    return resultado;
}

var renderHabilidadactividad = function(_idcurso, _idrecurso){
    if(_idcurso != 0){
        $.ajax({
            url: _sysUrlBase_+'/score/progresohabilidadunidad',
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: {'idcurso': _idcurso,'idrecurso' : _idrecurso},
        }).done(function(resp) {
            //code
            if(resp.code == 'ok'){
                $('#ModalListening').find('.progress-bar').css('width',resp.data[4]+'%')
                $('#ModalReading').find('.progress-bar').css('width',resp.data[5]+'%')
                $('#ModalWriting').find('.progress-bar').css('width',resp.data[6]+'%')
                $('#ModalSpeaking').find('.progress-bar').css('width',resp.data[7]+'%')

                $('#ModalListening').find('.countPercentage').data('value',resp.data[4]);
                $('#ModalReading').find('.countPercentage').data('value',resp.data[5]);
                $('#ModalWriting').find('.countPercentage').data('value',resp.data[6]);
                $('#ModalSpeaking').find('.countPercentage').data('value',resp.data[7]);

                $('#ModalListening').find('.countPercentage').find('span').html(resp.data[4]);
                $('#ModalReading').find('.countPercentage').find('span').html(resp.data[5]);
                $('#ModalWriting').find('.countPercentage').find('span').html(resp.data[6]);
                $('#ModalSpeaking').find('.countPercentage').find('span').html(resp.data[7]);

                $('#unityCompleteModal').find('.countPercentage').each(function(key, value){
                    countText($(this).find('span'), $(this).find('span').text(),parseInt($(this).data('value')) );
                });     
            }else{
                return false;
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    }//end if check idcurso
};

var renderHabilidad = function(_idcurso){
    if(_idcurso != 0){
        $('#chartTitle').html('<i class="fa fa-info"></i>&nbsp;\<?php echo JrTexto::_("Skills Obtained in the Course"); ?> '+ cursos[_idcurso]['nombre']);
        $.ajax({
            url: _sysUrlBase_+'/score/progresohabilidad',
            type: 'POST',
            dataType: 'json',
            data: {'idcurso': _idcurso},
        }).done(function(resp) {
            if(resp.code=='ok'){
                // console.log(resp);
                var calcular  = function(value){
                    return (value == 0) ? value : (value/100).toFixed(2);
                };
                $('#chartGeneral').find('.loading-chart').hide();
                $('#renderchart').show();
                // console.log(calcular(resp.data[4]));
                    var option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#ffbb04" };
                    drawHabilidad(document.getElementById("barListening"),option, calcular(resp.data[4]),0.1);
                    option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#c42d1b" };
                    drawHabilidad(document.getElementById("barReading"),option, calcular(resp.data[5]),0.1);
                    option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#d9b679" };
                    drawHabilidad(document.getElementById("barWriting"),option, calcular(resp.data[6]),0.1);
                    option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#2196f3" };
                    drawHabilidad(document.getElementById("barSpeaking"),option, calcular(resp.data[7]),0.1);

                    var _data = {
                        datasets: [{
                            data: [ (resp.data[4]), (resp.data[5]), (resp.data[6]),(resp.data[7])],
                            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
                        }],

                        // These labels appear in the legend and in the tooltips when hovering different arcs
                        labels: [
                            'Listening',
                            'Reading',
                            'Writing',
                            'Speaking'
                        ]
                    };
                    $('.chart-container').html(" ").html('<canvas id="pieChart" width="948" height="674"></canvas>');
                    drawPie(document.getElementById('pieChart'),_data);
                
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });

    }
}

var renderunidad = function(){
    // cursos[i]['nombre']+'-Detail'
    if(Object.keys(cursos).length > 0){
        //dibujar cuadros
        for(var i in cursos){
            var MainContent = $('<div></div>');
            var titleContent = $('<div></div>');
            var col = $('<div></div>');
            var cardUnidad = $('<div></div>');
            var unityContent = $('<div></div>');
            
            MainContent.attr('id',cursos[i]['nombre']+'-Detail');
            if(i == 31){
                MainContent.css('display','initial');
            }else{
                MainContent.css('display','none');
            }
            titleContent.addClass('col-md-12');
            titleContent.append('<h4 class="title-detail" style="margin:5px; padding:10px; border-bottom:3px solid #dadada;"><i class="fa fa-info"></i>&nbsp;\<?php echo JrTexto::_("Course Units"); ?> '+cursos[i]['nombre']+'</h4>');
            //setcontent to maincontent
            MainContent.append(titleContent);
            //Preparar<div class="col-md-12"> <!--Start card-->
            col.addClass('col-md-12'); //falta
            cardUnidad.addClass('card row card-unidad');
            unityContent.addClass('unityContent col-md-12');
        
            if(progresos[i] && parseInt(i) > -1 ){
                for(var j in progresos[i]){
                    if(parseInt(j) > -1 && (typeof progresos[i][j]['hijo'] !== 'undefined')){

                        var element = '';
                        element=element.concat('<div data-toggle="modal" data-target="#unityCompleteModal" class="unity col-md-4 hvr-outline-in" style="text-align: center;">');
                        element=element.concat('<div class="nameText" style="text-align: center;font-size: 1.5em; text-overflow: ellipsis;white-space: nowrap;">'+progresos[i][j]['nombre']+'</div>');
                        var color = '#b85c5c';
                        var complete = contarHijosCompletado(progresos[i][j]['hijo']);
                        if(complete > 0){
                            color = '#5cb85c';
                        }

                        // console.log(progresos[i][j]);
                        // console.log(Object.keys(progresos[i][j]['hijo']).length);
                        element=element.concat('<div class="foto hvr-hang" data-idcourse="'+i+'" data-idunidad="'+j+'" data-idrecurso="'+progresos[i][j]['idrecurso']+'" style="width: 100%;max-width: 265px;margin: 0  auto;height: 150px;border-radius: 1em;background-color: gray;overflow: hidden;">');
                        element= (progresos[i][j]['imagen'].indexOf('__xRUTABASEx__') == -1) ? element.concat('<img src="'+progresos[i][j]['imagen']+'" style="width: 100%;" />') : element.concat('<img src="'+progresos[i][j]['imagen'].replace('__xRUTABASEx__',_sysUrlBase_2)+'" style="width: 100%;" />');
                        element=element.concat('</div>');
                        element=element.concat('<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="'+progresos[i][j]['progreso']+'" aria-valuemin="0" aria-valuemax="100" style="width: '+(progresos[i][j]['progreso'].toFixed(2))+'%;">'+(progresos[i][j]['progreso']).toFixed(2)+'%');
                        element=element.concat('</div></div>');
                        element=element.concat('<div class="textProgress" style="text-align: center;font-size: 2.0em;">');
                        element=element.concat('<span class="textProgress-now" style="font-weight: bold;font-size: 1.5em;color: '+color+';">'+complete+'</span>');
                        element=element.concat('<span style="font-weight: bold;font-size: 1.4em;padding: 0 5px;">/</span>');
                        element=element.concat('<span class="textProgress-total">'+Object.keys(progresos[i][j]['hijo']).length+'</span>');
                        element=element.concat('</div>');
                        element=element.concat('</div>');

                        unityContent.append(element);

                    }
                }//endfor
            }
            cardUnidad.append(unityContent);
            col.append(cardUnidad);
            MainContent.append(col);
            $('#contenedor-details').append(MainContent);
        }//end for
    }else{
        //dibujar que no posee cursos matriculados
        $('body').append('<div class="col-md-12 alert alert-danger"><h1>El Estudiante esta registrado a ningun curso</h1></div>');
    }
}

var renderbarra = function(id,idunidad){
    if(id != 0){
        // console.log("holas");
        // console.log(progresos);
        // console.log(id);
        // console.log(idunidad);
        var obj_unidad = progresos[id][idunidad]['hijo'];
        var actividades_count = Object.keys(progresos[id][idunidad]['hijo']).length;
        //dibujar resultado de las barras y contador
        if(actividades_count > 0){
            $('#greenbarmodal').css('width','0%');
            $('#greenbarmodal').find('#child').css('left','3%');
            $('#barraActividades').find('.base').html(" ");
            var puntos = (actividades_count - 1);
            var coordenada = 0;
            // if(puntos > 1){
                coordenada = parseInt(93/puntos);
            // }
            var letrapoint = '<p style="display: inline-block;position: absolute;bottom: 0;left: 2%;margin: 0;font-weight: bold;">S1</p>';
            var point = '<div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: 2%;position: absolute;z-index: 1;"></div>';
            console.log(point);
            $('#barraActividades').find('.base').append('<div class="line" style="position: absolute;width: 100%;height:3px;background:lightgrey;top:5px;z-index: 0;"></div>');
            $('#barraActividades').find('.base').append(point);
            $('#barraActividades').find('.base').append(letrapoint);
            var sumcoordenada = 2;
            for(var i = 1; i <= actividades_count-1; i++){
                sumcoordenada += coordenada;
                point = '<div style="display:inline-block;width:30px;height:15px;background:#4e4d4d;border-radius: 50%;left: '+sumcoordenada+'%;position: absolute;z-index: 1;"></div>';
                letrapoint = '<p style="display: inline-block;position: absolute;bottom: 0;left: '+(sumcoordenada + 1)+'%;margin: 0;font-weight: bold;">S'+(i+1)+'</p>';
                $('#barraActividades').find('.base').append(point);
                $('#barraActividades').find('.base').append(letrapoint);
            }
             var cal1 = 100/(actividades_count - 1);
             if(cal1 > 1){
                //chequear si la primera actividad esta completada...
                 if(obj_unidad[0]["progreso"] >= 50){
                    var countporciento = 0;
                    var porcentajeactual = 0;
                    for (var i = 1; i < actividades_count; i++) {
                        if(obj_unidad[i]["progreso"] >= 50){
                            countporciento++;
                        }else{
                            porcentajeactual = parseInt(obj_unidad[i]["progreso"]);
                            break;
                        }
                        
                    }//end for
                    var sumwidth = parseInt($('#greenbarmodal').css('width'));
                    if(countporciento > 0){
                        var cal2 = cal1 * countporciento;
                        sumwidth = (cal2-2);
                    }
                    if(porcentajeactual > 0){
                        var cal3 = parseInt((porcentajeactual * cal1) / 100);
                        sumwidth += (cal3-2);
                    }
                    $('#greenbarmodal').css('width',sumwidth+'%');
                    $('#greenbarmodal').find('#child').css('left',(sumwidth +2)+'%');
                 }//end if chequeador
             }else{
                var sumwidth = (parseInt(obj_unidad[0]["progreso"]))-2;
                $('#greenbarmodal').css('width',sumwidth+'%');
                $('#greenbarmodal').find('#child').css('left',(sumwidth +2)+'%');
             }
             
        }
        for(var i in progresos[id][idunidad]){
            $('#ModalListening').find('.progress-bar').css('width',);
        }
    }//end if id <> 0
};
// var renderunidad = function(id,idunidad){
//     if(id != 0){
//         //dibujar resultado de las barras y contador
//         for(var i in progresos[id][idunidad]){
//             $('#ModalListening').find('.progress-bar').css('width',);
//         }
//         $('#unityCompleteModal').find('.countPercentage').each(function(key, value){
//             countText($(this).find('span'), $(this).find('span').text(),parseInt($(this).data('value')) );
//         });
//     }//end if id <> 0
// };

var barraprogresototal = function(){
    var total = 0;
    if(Object.keys(cursos).length > 0){
        // Object.keys(progresos[i]).length
        for(var i in cursos){
            if(cursos[i]){
                total += parseInt(cursos[i]['progreso']) * 100 / (100 * parseInt( Object.keys(progresos[i]).length)) ;
            }
        }//endforeach
        // console.log("hola");
        // console.log(cursos);
        // console.log(total);
        total = (total > 0) ? ((total * 100) / 150).toFixed(2) : 0;
        total = (total > 93) ? 93 : total; 
        $('#barraprogresototal').css('width',total+'%');
        $('#barraprogresototal').find('#child').css('left',(total + 3)+'%');
    }
}

var cursosGeneral = function(){
    if(cursos){
        for(var i in cursos){
            if(i == 31){
                var nivelContent = $('.nivelContent').eq(0);
                var nivelCircle = nivelContent.find('.nivelCircle');
                if(nivelCircle.hasClass('disabled')) { 
                    nivelCircle.removeClass('disabled'); 
                    nivelCircle.removeClass('lockedcourse'); 
                    nivelCircle.addClass('hvr-radial-out');
                    nivelCircle.addClass('courseactive');
                    nivelCircle.data('content','#'+cursos[i]['nombre']+'-Detail');
                    nivelCircle.data('namecourse',cursos[i]['nombre']);
                    nivelCircle.data('idcourse',i);
                    nivelCircle.prepend('<img src="'+_sysUrlBase_2+cursos[i]['imagen']+'" style="position: absolute; left: 0; height: 100%;" />');
                    nivelCircle.find('.foto').removeClass('hvr-hang');
                    nivelCircle.find('.porcentaje').css('top','17%');
                    nivelCircle.find('.porcentaje').css('font-size','4em');
                    nivelCircle.find('.porcentaje').css('color','#ffffff');
                    nivelCircle.find('.porcentaje').css('text-shadow','1px 2px 1px black');
                    nivelCircle.find('.porcentaje').html(cursos[i]['nombre']);

                    //dibujar progreso
                    nivelContent.find('.progress').find('.progress-bar').attr('aria-valuenow', cursos[i]['progreso']);
                    var progress = cursos[i]['progreso'] > 0 ? (cursos[i]['progreso'] * 100)/(parseInt( Object.keys(progresos[i]).length * 100)) : 0;                    nivelContent.find('.progress').find('.progress-bar').css('width', progress+'%');
                    nivelContent.find('.progress').find('.progress-bar').text((progress).toFixed(2)+'%');
                    // <img src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20180924053911.png" style="position: absolute; left: 0; height: 100%;">
                }
            }else{
                // if(Object.keys(cursos).length  > 1){
                //     // for(var j in cursos[i]){
                //         console.log("hola");
                //     // }
                // }
                if(parseInt(cursos[i]['progreso']) > 0 ){
                    var nivelContent = null;
                    switch(parseInt(i)){
                        case 35: { nivelContent = $('.nivelContent').eq(1); } break;
                        case 3: { nivelContent = $('.nivelContent').eq(2); } break;
                    }
                    if(nivelContent != null){
                        var nivelCircle = nivelContent.find('.nivelCircle');
                        if(nivelCircle.hasClass('disabled')) { 
                            nivelCircle.removeClass('disabled'); 
                            nivelCircle.removeClass('lockedcourse'); 
                            nivelCircle.addClass('hvr-radial-out');
                            nivelCircle.addClass('courseactive');
                            nivelCircle.data('content','#'+cursos[i]['nombre']+'-Detail');
                            nivelCircle.data('namecourse',cursos[i]['nombre']);
                            nivelCircle.data('idcourse',i);
                            nivelCircle.prepend('<img src="'+_sysUrlBase_2+cursos[i]['imagen']+'" style="position: absolute; left: 0; height: 100%;" />');
                            nivelCircle.find('.foto').removeClass('hvr-hang');
                            nivelCircle.find('.porcentaje').css('top','17%');
                            nivelCircle.find('.porcentaje').css('font-size','4em');
                            nivelCircle.find('.porcentaje').css('color','#ffffff');
                            nivelCircle.find('.porcentaje').css('text-shadow','1px 2px 1px black');
                            nivelCircle.find('.porcentaje').html(cursos[i]['nombre']);

                            //dibujar progreso
                            // parseInt(cursos[i]['progreso']) * 100 / (100 * parseInt( Object.keys(progresos[i]).length)) 
                            var progress = cursos[i]['progreso'] > 0 ? (cursos[i]['progreso'] * 100)/(parseInt( Object.keys(progresos[i]).length * 100)) : 0;
                            nivelContent.find('.progress').find('.progress-bar').attr('aria-valuenow', cursos[i]['progreso']);
                            nivelContent.find('.progress').find('.progress-bar').css('width',progress+'%');
                            nivelContent.find('.progress').find('.progress-bar').text((progress).toFixed(2)+'%');
                            // <img src="https://abacoeducacion.org/web/static/media/image/N1-U1-A1-20180924053911.png" style="position: absolute; left: 0; height: 100%;">
                        }
                    }//endif 
                }//end if
            } //end else
        }//endfor
    }
}//end cursosGeneral
/**///////////////////////////////////////////////////////////////////////////////////// */
/**///////////////////////////////////////////////////////////////////////////////////// */
/**/////////////////////-----------------MAIN--------------///////////////////////////// */
/**///////////////////////////////////////////////////////////////////////////////////// */
/**///////////////////////////////////////////////////////////////////////////////////// */
$('document').ready(function(){
    $.fn.modal.Constructor.DEFAULTS.backdrop = 'static'
    
    //Inicializar cursosGeneral
    barraprogresototal();
    //Inicializar cursosGeneral
    cursosGeneral();

    //renderizar unidades
    renderunidad();
    //renderizar total de habilidad
    renderHabilidad(31);

    $('.nivelCircle').eq(0).parent().find('.select').addClass('selected');
    // $('.nivelCircle').eq(0)
    $('.nivelCircle').on('click',function(){
        if($(this).hasClass('disabled') == false){
            $('.nivelCircle').each(function(k,v){
                $($(this).data('content')).hide();
            });
            $($(this).data('content')).show();
            $('.nivelContent').each(function(key,value){
                $(this).find('.select').removeClass('selected');
            });
            $(this).parent().find('.select').addClass('selected');
            $('#chartGeneral').find('.loading-chart').show();
            $('#renderchart').hide();
            renderHabilidad($(this).data('idcourse'));
        }
    });
    $('.foto').on('click',function(){
        
        //     var chartData = {
        //     labels: ['Listening', 'Reading', 'Writing','Speaking'],
        //     datasets: [{
		// 		label: 'Dataset 1',
		// 		backgroundColor: "#ffbb04",
		// 		borderColor: "#ffbb04",
		// 		borderWidth: 1,
		// 		data: [ 100,50,20,30]
        //     }]

        //     };
        // drawChart('unityCompleteModalChart',chartData);
        
        
        // $('#test').on('init', function () {
        //     visibility: visible;
        // });
        var target = $(this).parent().data('target');
        // console.log($(this).parent().attr('class'));
        var idcourse = $(this).data('idcourse');
        var idunidad = $(this).data('idunidad');
        var idrecurso = $(this).data('idrecurso');
        //var idactividad = $(this).data('idactividad');
        renderHabilidadactividad(idcourse,idrecurso);
        renderbarra(idcourse,idunidad);
        var variable = $(target).find('.modal-body').find('.content-activity');
        variable.html("");
        if(variable.hasClass('slick-initialized') == true){
            variable.removeClass('slick-initialized');
            variable.removeClass('slick-slider');
        }
        //dibujar las actividades
        var element = '';
        var contador = 0;
        var disparador = false;
        var obj_activity = progresos[idcourse][idunidad]['hijo'];
        for(var i in obj_activity){
            if(parseInt(i) > -1){
                if(obj_activity[i]['progreso']>=50){
                    contador++;
                }else{
                    if(disparador == false){
                        index = i;
                        disparador = true;
                    }
                }
                console.log(obj_activity);
                element = '';
                element = element.concat('<div class="item-activity" style="">');
                element = element.concat('<div class="content" style="">');
                element = element.concat('<span>'+obj_activity[i]['progreso']+'%</span>');
                element = element.concat('<div class="tela"></div>');
                element = (obj_activity[i]['imagen'].indexOf('__xRUTABASEx__') == -1) ? element.concat('<img style="width:100%;" src="'+obj_activity[i]['imagen']+'" />') : element.concat('<img style="width:100%;" src="'+obj_activity[i]['imagen'].replace('__xRUTABASEx__',_sysUrlBase_2)+'" />');
                element = element.concat('</div>');
                element = element.concat('</div>');
                variable.append(element);
                
            }
        }
        if(contador == Object.keys(obj_activity).length){
            $('#felicidadesModal').show();
            $('#felicidadesModal').find('a').attr('href',_sysUrlBase_+'/curso?id='+idunidad);
        }else{
            $('#continuarBoton').show();
            $('#continuarBoton').find('a').attr('href',_sysUrlBase_+'/smartbook/ver2/?idcursodetalle='+obj_activity[index]['idcursodetalle']+'&idrecurso='+obj_activity[index]['idrecurso']);
        }
        setTimeout(function(){
            variable.slick({
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1
            });
        }, 500);
    });
    $('#unityCompleteModal').on('click','.close,.close2',function(){
        $('#felicidadesModal').hide();
        $('#continuarBoton').hide();
    });
    // $('#myModal').modal({backdrop: 'static', keyboard: false})
});
</script>