<?php 
if(!empty($this->datos)){
    $rutabase=$this->documento->getUrlBase();
    $game=@str_replace('__xRUTABASEx__',$rutabase,$this->datos[0]["texto"]);
    $titulo=$this->datos[0]["titulo"];
    $descripcion=$this->datos[0]["descripcion"];
}
?>
<input type="hidden" name="hRol" id="hRol" value="Alumno">
<input type="hidden" name="idNivel" id="idNivel" value="<?php echo $this->idnivel; ?>">
<input type="hidden" name="idUnidad" id="idUnidad" value="<?php echo $this->idunidad; ?>">
<input type="hidden" name="idActividad" id="idActividad" value="<?php echo $this->idactividad; ?>">
<input type="hidden" name="hIdGame" id="hIdGame" value="<?php echo @$this->datos[0]['idtool']; ?>">
<input type="hidden" name="hRolUsuarioAct" id="hRolUsuarioAct" value="<?php echo @$this->rolUsuarioAct; ?>">

<?php /*Datos de Tarea_Archivo, si es que hubiera*/ ?>
<input type="hidden" name="hIdTareaArchivo" id="hIdTareaArchivo" value="<?php echo @$this->idTareaArchivo; ?>">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->tarea_archivo['tablapadre']=='R')?@$this->tarea_archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hIdTareaRespuesta" id="hIdTareaRespuesta" value="<?php echo @$this->tarea_respuesta['idtarea_respuesta']; ?>">

<style type="text/css"> #games-tool .addtext{ margin: 0; } </style>
<div class="container-fluid" id="games-tool">
    <div class="row games-container">
        <div class="col-xs-12 games-titulo-descripcion" style="display: none;">
            <div class="col-md-12 titulo text-center">
                <h3 ><?php echo $titulo; ?></h3>
            </div>
            <div class="col-md-12 descripcion text-center">
                <?php echo $descripcion; ?>                
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 games-main">
            <?php if( (empty(@$this->tarea_archivo) && !empty($game)) || 
                (@$this->tarea_archivo['tablapadre']=='T' || @$this->tarea_archivo['tablapadre']==null && !empty($game)) ){
                echo $game;
            }elseif( @$this->tarea_archivo['tablapadre']=='R' ){
                $texto = json_decode(@$this->tarea_archivo['texto'],true);
                echo str_replace('__xRUTABASEx__', $rutabase, $texto[0]['html']);
            } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
var msjes = {
    'confirm_action' : '<?php echo JrTexto::_('Confirm action');?>',
    'are_you_sure' : '<?php echo JrTexto::_('are you sure you want to delete this').' '.JrTexto::_('game'); ?>?',
    'accept' : '<?php echo JrTexto::_('Accept');?>',
    'cancel' : '<?php echo JrTexto::_('Cancel');?>',
    'guardar' : '<?php echo JrTexto::_('Save');?>',
};
var fnPreviewGame = function(){
    $('.discard-game').hide();
    $('.save-crossword').hide();
    $('.plantilla-crucigrama .nopreview').hide();
    $('.plantilla-sopaletras .nopreview').hide();
    $('.plantilla-rompecabezas .nopreview').hide();
    $('.plantilla-recursoexterno .nopreview').hide();
    $('.plantilla-recursoexterno .save-ganecontentdinamico').hide();
    $('.plantilla-recursoexterno').css('height','95vh');
    $('.btn.preview-game').hide();
    $('.btn.backedit-game').show();        
    $('.titulo-game').closest('.input-group').hide().closest('.titulo').find('p').html($('.titulo-game').val()).show();
    $('.descripcion-game').closest('.input-group').hide().siblings('p').html($('.descripcion-game').val()).show();
};
function imgLoaded(imgElement) {
    return imgElement.complete && imgElement.naturalHeight !== 0;
}
function estilosopa(){
    if($(window).width() <= 768){
        $('#findword-game #puzzle').css('padding','0px');
        $('#findword-game #puzzle .puzzleSquare').css('width','7%');
        $('#findword-game #words ul').css({textAlign: 'center',padding: '0px'});
        $('#findword-game #words li').css('width','33%');
    }
}
function estilorompecabeza(){
    $('.plantilla-rompecabezas').css('min-width','400px');

    let element = $('#puzzle-containment').find('div').eq(0);
    let _slot = null;

    var limpiarclase = function(e){
        e.removeClass('col-xs-6');
        e.removeClass('col-xs-7');
        e.removeClass('col-xs-8');
        e.removeClass('col-xs-9');
        e.removeClass('col-xs-10');
        e.removeClass('col-xs-11');
        e.removeClass('col-xs-12');
    }
    $('#puzzle-containment').find('.col-xs-6').each(function(index,element){
        if(index != 0){
            $(this).removeClass('col-xs-6');
            $(this).addClass('col-sm-6 col-xs-12');
        }
    });
    if($(window).width() <= 768){
        let slots = $('#puzzle-containment').find('.snappuzzle-wrap').find('.snappuzzle-slot');
        let contador = 0,contadorfila = 0,max = 3;
        let width = 112, height = 77;
        slots.each(function(){
            contador = contador % max;
            if(contador < 3){
              $(this).css({width: width+'px',height: height+'px', left: (width * contador)+'px',top: (height * contadorfila) +'px'});
            }
            contador++;
            if(contador == 3){
                contadorfila++;
            }
        });
    }
    //set slot
    _slot = $('#puzzle-containment .snappuzzle-wrap .snappuzzle-slot').eq(0);

    $('#puzzle-containment').find('.snappuzzle-wrap img').css({width:( parseFloat(_slot.css('width')) * 3) + 'px',height: (parseFloat(_slot.css('height')) * 3)+'px'});

    //710
    if($(window).width() <= 710 && $(window).width() > 622){
        limpiarclase(element);

        if(!element.hasClass('col-xs-7')){
            element.addClass('col-xs-7');
        }
    }else if($(window).width() <= 622 && $(window).width() > 564){
        limpiarclase(element);

        if(!element.hasClass('col-xs-8')){
            element.addClass('col-xs-8');
        }
    }else if($(window).width() <= 564 && $(window).width() > 508){
        limpiarclase(element);
        
        if(!element.hasClass('col-xs-9')){
            element.addClass('col-xs-9');
        }
    }else if($(window).width() <= 508 && $(window).width() > 464){
        limpiarclase(element);
        
        if(!element.hasClass('col-xs-10')){
            element.addClass('col-xs-10');
        }
    }else if($(window).width() <= 464 && $(window).width() > 425){
        limpiarclase(element);
        
        if(!element.hasClass('col-xs-11')){
            element.addClass('col-xs-11');
        }
    }else if($(window).width() <= 425){
        limpiarclase(element);
        
        if(!element.hasClass('col-xs-12')){
            element.addClass('col-xs-12');
        }
    }else{
        limpiarclase(element);

        if(!element.hasClass('col-xs-6')){
            element.addClass('col-xs-6');
        }
    }
}
function checkTipoJuego(){
    if($('body').find('.plantilla-sopaletras').length > 0){
        estilosopa();
    }else if($('body').find('.plantilla-rompecabezas').length > 0){
        estilorompecabeza();
    }
}
$(document).ready(function(){
    
    $('.games-titulo-descripcion').show();
    //if( $('#hRol').val()=='Alumno' ){ 
     fnPreviewGame(); 
    //}
    $('.games-main').iniciarVisor({
        fnAlIniciar: function(){
            var $tmpl = $('.games-main').find('.plantilla');
            if($tmpl.hasClass('plantilla-sopaletras')){iniciarSopaLetras($tmpl);}
            else if($tmpl.hasClass('plantilla-crucigrama')){iniciarCrucigrama($tmpl);}
            else if($tmpl.hasClass('plantilla-rompecabezas')){
                var $img = $tmpl.find('#puzzle-containment img.valvideo');
                var _img_ = document.getElementsByClassName("valvideo")[0];
                if(imgLoaded(_img_)){
                    iniciarRompecabezas($tmpl);
                }else{
                    $img.load(function() { iniciarRompecabezas($tmpl); });
                }
            }
        },
        texto:msjes,
        btnGuardar: ($('#hRol').val()=='Alumno')?'.btn_guardar_juego':false,
    });
    setTimeout(() => {
        checkTipoJuego();
    }, 500);
    $(window).resize(function(){
        checkTipoJuego();
    });
    mostrarGame(true);
});
</script>