<?php 
$RUTA_BASE = $this->documento->getUrlBase();
if(!empty($this->datos)) $frm=$this->datos;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="grabacion_add">
    <!--<div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('Record')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>-->

            <!-- contenido de moda-body para tipo_adjunto="GrabacionVoz" -->
    <div id="adjuntar_grabacionvoz">
        <form class="form-horizontal" id="frm-adjuntar_grabacionvoz" name="frm-adjuntar_grabacionvoz">
            <input type="hidden" name="accion" id="accion" value="<?php echo $this->frmaccion; ?>">
            <input type="hidden" name="pkId_grabacion" id="pkId_grabacion" value="<?php echo @$frm['id_grabacion']; ?>">
            <div class="form-group ">
                <label for="txtNombre" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-8">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control txtNombre" value="<?php echo JrTexto::_('recording');?>" data-uniqid="<?php echo uniqid();?>">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger grabarme" data-estado="stopped"><i class="fa fa-circle"></i> <span><?php echo JrTexto::_('Rec'); ?></span></button>
                <a class="btn btn-primary reproducir"  data-estado="paused" disabled="disabled"><i class="fa fa-play"></i> <span><?php echo JrTexto::_('Play'); ?></span></a>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <div class="thumbnail onda_voz" style="min-height: 140px; padding-left: 0; padding-right: 0;"></div>
                </div>
            </div>
            <div class="form-group">
                 <div class="col-xs-12">
                 <hr>
                    <a href="<?php echo $this->documento->getUrlBase();?>/modbiblioteca/#/grabacion/listar" class="btn btn-primary irlistado  pull-left">Ir a listado</a>
                    <button class="btn btn-success guardar_adjunto pull-right">Guardar</button>
                </div>
            </div>
        </form>
    </div>
    
</div>


<script>
$('.istooltip').tooltip();
var rutaslib = '<?php echo $this->documento->getUrlStatic();?>/libs/audiorecord/';
var recorder;
$(document).ready(function() {
    var fnAjaxFail = function(xhr, textStatus, errorThrown) {
        //console.log("Error");
        //console.log(xhr);
        //console.log(textStatus);
        throw errorThrown;
        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    };
    
    var subirmedia=function(tipo, file, otrosDatos={}){
        var formData = new FormData();
        formData.append("tipo", tipo);
        if(tipo=='G'){
            formData.append("filearchivo", file);/* "file" es un Blob */
            formData.append("nombre_file", otrosDatos.nombrearchivo);
        }else{
            formData.append("filearchivo", file[0].files[0]);
        }

        if($('#accion').val()=='Editar'){
            formData.append("tablapadre", 'T');
            formData.append("idpadre", $('#pkIdtarea').val());
        }
        $.ajax({
            url: _sysUrlBase_+'/bib_grabacion/subirarchivo',
            type: "POST",
            data:  formData,
            contentType: false,
            processData: false,
            dataType :'json',
            cache: false,
            xhr:function(){
                var xhr = new window.XMLHttpRequest();
                /*Upload progress*/
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = Math.floor((evt.loaded*100) / evt.total);
                        $('#barra-progreso .progress-bar').width(percentComplete+'%');
                        $('#barra-progreso .progress-bar span').text(percentComplete+'%');
                    }
                }, false);
                /*Download progress*/
                xhr.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                    }
                }, false);
                return xhr;
            },
            beforeSend: function(XMLHttpRequest){
                div=$('#barra-progreso');
                $('#barra-progreso').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                $('#barra-progreso .progress-bar').width('0%');
                $('#barra-progreso .progress-bar span').text('0%'); 
                $('#barra-progreso').fadeIn('fast'); 
                $('#btn-saveBib_libro').attr('disabled','disabled');
            },      
            success: function(data){
                if(data.code==='ok'){
                    $('#barra-progreso .progress-bar').width('100%');
                    $('#barra-progreso .progress-bar').html('Complete <span>100%</span>');
                    $('#barra-progreso').addClass('progress-bar-success').removeClass('progress-bar-animated');
                    
                    agregarTblAdjuntos({'idtarea_archivos': data.idtarea_archivos, 'nombre': data.nombre, 'ruta': data.ruta, 'tipo': data.tipo});
                    $('#inp_CargadorArchivos').val('');

                    mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                }else{
                    $('#barra-progreso').addClass('progress-bar-warning progress-bar-animated');
                    mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
                    return false;
                }
            },
            error: function(e) {
                $('#barra-progreso').addClass('progress-bar-danger progress-bar-animated');
                $('#barra-progreso .progress-bar').html('Error <span>-1%</span>'); 
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
                return false;
            },
            complete: function(xhr){
                $('#barra-progreso .progress-bar').html('Complete <span>100%</span>'); 
                $('#barra-progreso').addClass('progress-bar-success progress-bar-animated').fadeOut('fast');
            }
        });
    };

    var crearModal = function(param){
        if(param.deDonde==undefined || param.deDonde==''){ return false; }
        if(param.nombreContenedor==undefined || param.nombreContenedor=='') {
            param.nombreContenedor=param.deDonde;
        }
        var $modal = $('#modalclone').clone();
        $modal.attr('id','mdl-'+param.nombreContenedor);
        if(param.small){$modal.find('.modal-dialog').removeClass('modal-lg');}
        else{$modal.find('.modal-dialog').addClass('modal-lg');}
        $modal.find('.modal-header #modaltitle').html('<?php echo JrTexto::_('Attach'); ?> '+param.titulo);
        $modal.find('#modalfooter .btn.cerrarmodal').addClass('pull-left');
        $modal.find('#modalfooter').append('<button class="btn btn-success guardar_adjunto"><?php echo JrTexto::_('Save'); ?></button>');
        $('body').append($modal);
        $('#mdl-'+param.nombreContenedor).modal({keyboard:false, backdrop:'static'});
        $modal.find('#modalcontent').html($('#'+param.deDonde).html());
        $modal.find('#modalcontent').find('form').attr({'id':'frm-'+param.nombreContenedor,'name':'frm-'+param.nombreContenedor});
    };

    var preparar_datos = function(idcontenedor, tipo) {
        var datos = {};
        if(tipo=='G'){
            grabacion_ConvertirMP3_Subir(idcontenedor, true);
        }
        return datos;
    };

    var initGrabarVoz = function(){
        audioRecorder.requestDevice(function(recorderObject){
            recorder = recorderObject;
        }, {recordAsOGG: false});
    };
    
    var grabacion_ConvertirMP3_Subir = function(classModal, subir=false){
        var fnCallback = function(blob){
            var a = Date.now();
            var length = ((blob.size*8)/128000);
            var url = URL.createObjectURL(blob);
            if(subir){
                subirmedia('G', blob, {'nombrearchivo': $(classModal).find('input.txtNombre').val()+'_'+$(classModal).find('input.txtNombre').attr('data-uniqid') } );
                $(classModal).closest('.modal').modal('hide');
            }else{
                var wavesurfer = Object.create(WaveSurfer);
                wavesurfer.init({
                    container: document.querySelector(classModal+' .onda_voz'),
                    waveColor: '#85BCEA',
                    progressColor: '#337AB7',
                    backend: 'MediaElement'
                });
                wavesurfer.load(url);
                $(classModal+' .btn.reproducir').show();
                document.querySelector(classModal+' .btn.reproducir').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
            }
        };
        recorder.exportMP3(fnCallback);
    };

    $('body').on('click', '#frm-adjuntar_grabacionvoz .btn.grabarme', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var $modal = $(this).closest('.modal.in');
        if($btn.attr('data-estado')=='stopped'){
            $btn.attr('data-estado','recording');
            recorder.clear();
            recorder && recorder.record();
            $btn.siblings('.btn.reproducir').attr('disabled', 'disabled');
            $modal.find('.onda_voz').html('<h1 class="text-center animated infinite pulse"> <i class="fa fa-microphone"></i> Recoding...</h1>');
            $btn.removeClass('btn-danger').addClass('btn-default');
            $btn.find('i.fa').removeClass('fa-circle').addClass('fa-stop');
            $btn.find('span').text('<?php echo JrTexto::_('Stop'); ?>');
        }else{
            $btn.attr('data-estado','stopped');
            recorder && recorder.stop();
            $btn.siblings('.btn.reproducir').removeAttr('disabled');
            $modal.find('.onda_voz').html('');
            $btn.removeClass('btn-default').addClass('btn-danger');
            $btn.find('i.fa').removeClass('fa-stop').addClass('fa-circle');
            $btn.find('span').text('<?php echo JrTexto::_('Rec'); ?>');
            grabacion_ConvertirMP3_Subir('.modal.in');
        }
    }).on('click', '.modal.in #frm-adjuntar_grabacionvoz .btn.reproducir', function(e) {
        e.preventDefault();
        var $btn = $(this);
        if($btn.attr('data-estado')=='paused'){
            $btn.attr('data-estado','playing');
            $btn.siblings('.btn.grabarme').attr('disabled', 'disabled');
            $btn.find('i.fa').removeClass('fa-play').addClass('fa-pause');
            $btn.find('span').text('<?php echo JrTexto::_('Pause'); ?>');
        }else{
            $btn.attr('data-estado','paused');
            $btn.siblings('.btn.grabarme').removeAttr('disabled');
            $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
            $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
        }
    }).on('ended', '.modal.in #frm-adjuntar_grabacionvoz .onda_voz audio', function(e) {
        console.log('audio Pause');
        $(this).currentTime = 0;
        var $btn = $(this).closest('.modal.in').find('.btn.reproducir');
        $btn.attr('data-estado','paused');
        $btn.siblings('.btn.grabarme').removeAttr('disabled');
        $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
        $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
    });

    $('body').on('click', '.modal.in .guardar_adjunto', function(e) {
        e.preventDefault();
        //var tipo = $(this).data('guardar');
        var tipo = 'G';
        var datos = preparar_datos('#grabacion_add', tipo);
        if($.isEmptyObject(datos)){ return false; }
       /* $.ajax({
            url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos',
            type: 'POST',
            dataType: 'json',
            data:  datos,
        }).done(function(resp) {
            if(resp.code=='ok'){
                agregarTblAdjuntos({'id_grabacion': resp.idtarea_archivos, 'nombre': resp.nombre, 'ruta': resp.ruta, 'tipo': resp.tipo});
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        }).fail(fnAjaxFail).always(function() {
            $('.modal.in').modal('hide');
        });*/
    });
    
    $('body').on('hidden.bs.modal', '.modal', function(event) { 
        //$(this).remove(); 
    }).on('keyup', '.form-group *[required]', function(e) {
        if(!$(this).val() || $(this).val().trim()==''){
            $(this).closest('.form-group').addClass('has-error');
        }else{
            $(this).closest('.form-group').removeClass('has-error');
        }
    }).on('change', '#grabacion_add *[required]', function(e) {
        var valor = $(this).val();
        if(valor!='' && valor.trim()!=''){
            $(this).closest('.form-group').removeClass('has-error');
        }
    });
    $('.irlistado').click(function(e) {
        console.log('listado');
         $(this).closest('.modal').modal('hide');
    });

    $('.btn.guardartarea').click(function(e) {
        e.preventDefault();
        guardarTarea();
    });

    initGrabarVoz();

});
</script>