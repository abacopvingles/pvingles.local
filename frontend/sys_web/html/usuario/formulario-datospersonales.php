<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:'false';
$return=!empty($_REQUEST["return"])?$_REQUEST["return"]:'false';
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:RUTA_BASE.'static/media/usuarios/user_avatar.jpg';
$rol=!empty($_REQUEST["rol"])?$_REQUEST["rol"]:"";
$buscarper=!empty($_REQUEST["buscarper"])?$_REQUEST["buscarper"]:"si";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .toolbarmouse{
    position: absolute;
    right: 0px;
    top: 0px;
    background: rgba(255, 255, 253, 0.72);
  }
  .input-file-invisible{
    position: absolute;
    top: 0px;
  }
</style>
<div class="col-md-12">
<div class="panel panel-body">
<input type="hidden" id="datareturn" value="<?php echo $datareturn;?>">
  <form class="" id="frm<?php echo $idgui; ?>"  target="" enctype="multipart/form-data">
    <input type="hidden" name="idpersona" id="idpersona<?php echo $idgui; ?>" value="<?php echo @$frm["idpersona"]; ?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo @$this->accion; ?>">
    <input type="hidden" name="rol" id="rol<?php echo $idgui; ?>" value="<?php echo @$rol; ?>">
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-9">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Document type")); ?></label>
            <div class="cajaselect">
              <select name="tipodoc" id="tipodoc<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fktipodoc)) foreach ($this->fktipodoc as $fk) { ?>
                  <option value="<?php echo $fk["codigo"]?>"  >
                    <?php echo $fk["nombre"] ?>
                    </option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5">
          <div class="form-group">
            <label for="titulo" class="text-left">N° <?php echo ucfirst(JrTexto::_('Id card')); ?></label>                    
              <input type="text" class="form-control" name="dni" id="tmpdni<?php echo $idgui; ?>" required="required" value="<?php echo @$frm["dni"]; ?>" placeholder=""> 
          </div>  
        </div> 
         <?php 
         if($buscarper!='no'){?>    
        <div class="col-xs-12 col-sm-6 col-md-3"><br>
          <a class="btn btn-info btnsearch<?php echo $idgui ?>" href="javascript:void(0)"><i class="fa fa-search"></i> <?php echo JrTexto::_('Search'); ?></a>          
        </div> 
         <?php } ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Names')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="nombre" id="nombre<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["nombre"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Father's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_paterno" id="ape_paterno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_paterno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("Mother's last name")); ?></label>                    
              <input type="text" class="form-control"  required="required" name="ape_materno" id="ape_materno<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["ape_materno"]); ?>" placeholder=""> 
          </div>  
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Gender")); ?></label>
            <div class="cajaselect">
              <select name="sexo" id="sexo<?php echo $idgui;?>" class="form-control">                     
                <?php if(!empty($this->fksexo)) foreach ($this->fksexo as $fksexo) { ?>
                  <option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> >
                    <?php echo $fksexo["nombre"] ?>
                    </option>
                <?php } ?>                            
              </select>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label><?php echo ucfirst(JrTexto::_("Marital Status")); ?></label>
            <div class="cajaselect">
              <select name="estado_civil" id="estado_civil<?php echo $idgui;?>" class="form-control">
                <?php if(!empty($this->fkestado_civil)) foreach ($this->fkestado_civil as $fkestado_civil) { ?>
                  <option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> >
                    <?php echo $fkestado_civil["nombre"] ?>
                    </option>
                <?php } ?>                        
              </select>
            </div>
          </div>
        </div> 

        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Telephone')); ?></label>                    
              <input type="text" class="form-control"  required="required" name="telefono" id="telefono<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["telefono"]); ?>" > 
          </div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group">
            <label for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_('Mobile number')); ?></label>                    
              <input type="text" class="form-control" name="celular" id="celular<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["celular"]); ?>" > 
          </div> 
        </div> 
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3 text-center">
        <div class="col-md-12 text-center form-group contool">												
					<label><?php echo ucfirst(JrTexto::_('Photo')); ?></label>                
					<div style="position: relative;" class="frmchangeimage text-center">
						<div class="toolbarmouse text-center"><span class="btn btn-xs btn-danger btnremoveimage"><i class="fa fa-trash"></i></span></div>
					  <div><img src="<?php echo $this->documento->getUrlBase().$fotouser; ?>" alt="fotouser" class="foto_alumno img-responsive center-block thumbnail __subirfile img-fluid center-block" data-nombre="foto" data-type="imagen"  id="foto<?php echo $idgui;?>" style="min-height:100px; max-width: 200px; max-height: 150px;"></div>
					</div>													
				</div>                
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
          <label><?php echo ucfirst(JrTexto::_("Birthday")); ?></label>
            <div class="form-group">
              <div class="input-group date datetimepicker">
                <input type="text" class="form-control" required="required" name="fechanac" id="fechanac<?php echo $idgui; ?>" value="<?php echo ucfirst(@$frm["fechanac"]); ?>"> 
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              </div>
            </div>
            </div>
          </div>
      </div>
      <div class="clearfix"></div><br>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <a class="btn btn-default btnretornar<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_('Back');?></a>
        <button type="submit" class="btn btn-primary cerrarmodal"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save');?></button>
        <?php if($buscarper!='false'){ ?><a class="btn btn-warning btnseleccionar<?php echo $idgui ?> hide" href="javascript:void(0)"><i class="fa fa-hand-o-down"></i> <?php echo JrTexto::_('Selected'); ?></a><?php } ?>
      </div>
      <div class="clearfix"></div><br>
    </div>            
  </form>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    var changefoto=false;
    var msjatencion='<?php echo JrTexto::_('Attention');?>';
    var idgui='<?php echo !empty($idgui)?$idgui:now();?>';
    var buscaralumno<?php echo $idgui; ?>=function(){
      var dniobj=$('#tmpdni'+idgui);
      var dni=dniobj.val();
      if(dni=='') {
         mostrar_notificacion(msjatencion,'<?php echo JrTexto::_('empty information');?>','warning');
        dniobj.focus();
        return false;
      }
      $('#idpersona'+idgui).val(0);
      var formData = new FormData();       
      formData.append('dni', dni); 
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/personal/buscarjson',
        msjatencion:msjatencion,
        type:'json',
        //showmsjok : true,
        callback:function(rs){
          var dt=rs.data;
          if(dt[0]!=undefined){
            var rw=dt[0];
            $('#idpersona'+idgui).val(rw.idpersona);
            $('#nombre'+idgui).val(rw.nombre);
            $('#ape_paterno'+idgui).val(rw.ape_paterno);
            $('#ape_materno'+idgui).val(rw.ape_materno);
            $('#sexo'+idgui).val(rw.sexo);
            $('#telefono'+idgui).val(rw.telefono);
            $('#celular'+idgui).val(rw.celular);
            $('#fechanac'+idgui).val(rw.fechanac);
            $('#estado_civil'+idgui).val(rw.estado_civil||'S');
            if(rw.foto!=''){
              $('#foto'+idgui).attr('src',_sysUrlStatic_+'/media/usuarios/'+rw.foto);              
            }
           if($('#datareturn').val()!='false'){
             $('.btnseleccionar<?php echo $idgui ?>').removeClass('hide');
           }else{

           }
          }else{          
            $('.btnseleccionar<?php echo $idgui ?>').addClass('hide');
          }        
        }
      }
      sysajax(data);
    }
    $('.btnsearch'+idgui).click(function(){
      buscaralumno<?php echo $idgui; ?>();
    });

var fcall=<?php echo !empty($fcall)?($fcall):'undefined';?>;

    $('#frm<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
        btn=$(this);
        var myForm = document.getElementById('frm'+idgui); 
        var formData = new FormData(myForm);
        var guardarfile=($('input[name="idpersona"]').val()||'')==''?false:true;
        $('#frm'+idgui).find('img.__subirfile').each(function(i,v){  
            var nomimg=$(v).attr('alt');
            if($('input.file'+nomimg).length)formData.append(nomimg, $('input.file'+nomimg)[0].files[0]);
            else{
              var img=($(v).attr('src')||'').replace(_sysUrlBase_,'');
              var nomimg=$(v).attr('data-nombre')||$(v).attr('alt');
              formData.append(nomimg,img);
              
            }
        })
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/guardarDatos',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'json',
          showmsjok : true,
          callback:function(data){
            $('input.idpersona').val(data.newid);
            $('img.fotouserlogin').attr('src',sysUrlBase_+data.fotouser)
            if(_isFunction(fcall)){
              fcall();
              redir(_sysUrlBase_+'/personal');
            }//else $('.btnseleccionar'+idgui).removeClass('hide');
          }
        }
        sysajax(data);
        return false;
      }
    });


    $('#fechanac<?php echo $idgui; ?>').datetimepicker({ //lang:'es',  //timepicker:false,
      format:'YYYY/MM/DD'
    }); 

    <?php if(!empty($fcall)){ ?>
    $('.btnseleccionar<?php echo $idgui ?>').click(function(){      
        var tmpobj=$('.tmp<?php echo $fcall ?>');        
        if(tmpobj.length){
            var dniobj=$('#tmpdni'+idgui);
            tmpobj.attr('data-returndni',$('#tmpdni'+idgui).val());
            tmpobj.attr('data-returnidpersona',$('#idpersona'+idgui).val());
            tmpobj.attr('data-returnnombre',$('#ape_paterno'+idgui).val()+' '+$('#ape_materno'+idgui).val()+', '+$('#nombre'+idgui).val());
            tmpobj.attr('data-return',dniobj.val());
            tmpobj.on('returndata').trigger('returndata');
        } 
        <?php if($this->documento->plantilla=='modal'){ ?>
        $(this).closest('.modal').modal('hide');
        <?php } ?>
    });
    <?php } ?>
    $('.btnretornar<?php echo $idgui; ?>').click(function(){      
      if($(this).closest('.modal').length)
        $(this).closest('.modal').modal('hide');
      else      
        window.history.back();
    })
    $('.__subirfile').on('click',function(){
        var $img=$(this);
        var guardar=($('input[name="idpersona"]').val()||'')==''?false:true;
        var nombre=guardar==true?('user_'+$('input[name="idpersona"]').val()):'';
        __subirfileok({file:$img,typefile:'imagen',nombre:nombre,uploadtmp:true,guardar:false,dirmedia:'usuarios/'},function(rs){
            //var f=rs.media.replace(_sysUrlBase_, '');
            //$('input#'+$img.attr('id')).val(f);
        });
	});
  });
</script>