<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=($this->documento->plantilla!="modal")?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$frm=!empty($this->datos)?$this->datos:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'static/media/usuarios/user_avatar.jpg';
$experiencias=$this->experiencias;
$metas=$this->metas;
$referencias=$this->referencias;
$educacion=$this->educacion;
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .table,.panel{
    font-size: 1em;
    font-family: Arial;
  }
  .table tr th:first-child{
    width: 200px;
  }
  .thpt{ width: 10px; }
  .tabletitle{ font-size: 1.5em; }
</style>
<div class="col-md-12">
  <div class="panel panel-body" style="display: block" id="pnla_<?php echo $idgui; ?>">
      <div class="col-xs-12 col-sm-12 col-md-12 text-center ">
          <img src="<?php echo $this->documento->getUrlBase().$fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; min-width: 100px; ">  
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center"> <div class="tabletitle"><br>
        <strong><?php echo ucfirst(@$frm["ape_paterno"]).' '.ucfirst(@$frm["ape_materno"]).', '.ucfirst(@$frm["nombre"]); ?></strong></div>
        <?php
        if(!empty($frm["fechanac"])){
          list($Y,$m,$d) = explode("-",$frm["fechanac"]);
          echo ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y )." ".JrTexto::_('Years');
        } 
        ?>
      </div>      
      <div class="col-xs-12 col-sm-12 col-md-12">        
        <hr>
        <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("General Information")); ?></strong></div>
        <table class="table table-responsive table-striped">
          <tr><th>N° <?php echo ucfirst(JrTexto::_('Id card')); ?></th>     <th class="thpt">:</th><td><?php echo @$frm["dni"]; ?></td></tr>
          <tr><th><?php echo ucfirst(JrTexto::_("Gender")); ?></th>         <th class="thpt">:</th><td><?php echo @$frm["strsexo"]; ?></td></tr>
          <tr><th><?php echo ucfirst(JrTexto::_("Marital Status")); ?></th> <th class="thpt">:</th><td><?php echo @$frm["strestadocivil"]; ?></td></tr>          
        </table>
      </div>
  </div>
</div>
<div class="col-md-12">
  <div class="panel panel-body">
    <div class="col-md-12">
    <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("Contact information")); ?></strong></div>
    <table class="table table-responsive table-striped">     
      <tr><th><?php echo ucfirst(JrTexto::_("Email")); ?></th>          <th class="thpt">:</th><td><?php echo @$frm["email"]; ?></td></tr>
      <tr><th><?php echo ucfirst(JrTexto::_("Telephone")); ?></th>      <th class="thpt">:</th><td><?php echo @$frm["telefono"]." / ".$frm["celular"]; ?></td></tr>
      <tr><th><?php echo ucfirst(JrTexto::_("Ubigeo")); ?></th>         <th class="thpt">:</th><td><?php echo @$frm["ubigeo"]; ?></td></tr>
      <tr><th><?php echo ucfirst(JrTexto::_("Address")); ?></th>        <th class="thpt">:</th><td><?php echo @$frm["urbanizacion"].", ".@$frm["direccion"]; ?></td></tr>
    </table>
  </div></div>
</div>
<?php if(!empty($metas[0])){ $meta_=$metas[0];?>
<div class="col-md-12">
  <div class="panel panel-body">
    <?php if(!empty($meta_["objetivo"])){?><div class="col-md-12">
    <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("My Objetive")); ?></strong></div>
    <table class="table table-responsive table-striped">      
      <tr><td><?php echo @$meta_["objetivo"] ?></td></tr>      
    </table></div><?php }?>

    <?php if(!empty($meta_["meta"])){?><div class="col-md-12">
    <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("My Goal")); ?></strong></div>
    <table class="table table-responsive table-striped">     
      <tr><td><?php echo @$meta_["meta"] ?></td></tr>      
    </table></div><?php }?>
  </div>
</div>
<?php } ?>
<?php if(!empty($educacion)){ ?>
<div class="col-md-12">
  <div class="panel panel-body">
    <div class="col-md-12">
    <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("Educational Instruction")); ?></strong></div>
    <?php foreach ($educacion as $edu){ ?>
      <table class="table table-responsive table-striped">
        <tr><td><strong><?php 
          if(!empty($edu["titulo"])&&!empty($edu["institucion"])) echo ucfirst($edu["titulo"]." - ".$edu["institucion"]);
          elseif (empty($edu["titulo"])&&!empty($edu["institucion"])) echo ucfirst($edu["institucion"]);
          elseif (!empty($edu["titulo"])&&empty($edu["institucion"])) echo ucfirst($edu["titulo"]);
          ?></strong><br>
          <?php echo JrTexto::_('From the'); ?>: </span><?php echo $edu["fechade"] ." - ".(!empty($edu["actualmente"])?JrTexto::_('to the present'):$edu["fechahasta"]); ?><br>
          <?php 
                if(!empty($edu["strtipoestudio"])&&!empty($edu["strareaestudio"])) echo $edu["strtipoestudio"]." - ".$edu["strareaestudio"]; 
                elseif (empty($edu["strtipoestudio"])&&!empty($edu["strareaestudio"])) echo $edu["institucion"];
                elseif (!empty($edu["strtipoestudio"])&&empty($edu["strareaestudio"])) echo $edu["strtipoestudio"];
              ?><?php echo ' - <strong>'.$edu["strsituacion"]."</strong>";?>
        </td></tr> 
      </table>           
    <?php } ?>
  </div>
  </div>
</div>
<?php } ?>
<?php if(!empty($referencias)){?>
<div class="col-md-12">
  <div class="panel panel-body">
    <div class="col-md-12">
    <div class="tabletitle"><strong> <?php echo ucfirst(JrTexto::_("Work experience")); ?></strong></div>
    <?php foreach ($referencias as $ref){ ?>
      <table class="table table-responsive table-striped">
        <tr><td><strong><?php echo ucfirst($ref["nombre"].' - '.$ref["cargo"]); ?></strong><br>
         <strong><?php echo JrTexto::_('Relationship'); ?> :</strong><?php echo $ref["relacion"]; ?><br>
         <strong><?php echo JrTexto::_('Contact'); ?> :</strong><br>
         <i class="fa fa-at"></i> <?php echo $ref["correo"] ?><br>
         <i class="fa fa-phone"></i> <?php echo $ref["telefono"] ?>          
        </td></tr> 
      </table>           
    <?php } ?>
    </div>
  </div>
</div>
<?php } ?>
<div class="text-center hidden-print">
  <a class="btn btn-default btncerrar<?php echo $idgui; ?>" href="javascript:javascript:void(0);"> <i class="fa fa-undo"></i> <?php echo JrTexto::_('Back'); ?></a>        
  <a class="btn btn-warning" href="javascript:window.print();"> <i class="fa fa-print"></i> <?php echo JrTexto::_('Print'); ?></a>
  <?php if($this->usuarioAct["idpersona"]===$frm["idpersona"]||$this->usuarioAct["dni"]===$frm["dni"]){ ?>
  <a class="btn btn-success" href="<?php echo $this->documento->getUrlBase().'/personal/formulario/?idpersona='.@$frm["idpersona"].'&rol='.@$frm["rol"].'&datareturn=false&plt=mantenimientos-out&buscarper=no';?>"> <i class="fa fa-pencil"></i> <?php echo JrTexto::_('Edit'); ?></a>
  <?php } ?>
  <br><br>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.btncerrar<?php echo $idgui; ?>').click(function(){
      <?php if($ismodal==true){?>
        $(this).closest('.modal').modal('hide');
      <?php }else{?>
          window.history.back();
      <?php } ?>  
    })
  });
</script>