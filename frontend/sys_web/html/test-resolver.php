<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$imgcursodefecto='/static/media/nofoto.jpg';
$test=!empty($this->test[0])?$this->test[0]:array();
if(!empty($test['imagen'])) $imagen=$test['imagen'];
else $imagen=$imgcursodefecto;
$urlmedia=$this->documento->getUrlBase();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<style type="text/css">
  .tabstep li a{
    color: #fff;
    background-color: #ec9c58;
  }
  .tabstep li a:hover{
    color: #fff;
    background-color: #5ba1de;
  }
  .badgestep{
  position: absolute;
  left: 1ex;
  font-size: 1.5em;
  }
  .tr_clone{display: none};  
</style>
<?php if(!$ismodal){ ?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Test">&nbsp;<?php echo JrTexto::_('Self-autoevaluation'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>

  <input type="hidden" name="idTest" value="<?php echo !empty($frm["idtest"])?$frm["idtest"]:0; ?>">
  <div id="tabcriterios" class="tab-pane fade in active">
     <div class="col-md-6 text-center">
        <h3 class="_nomtitulo"><?php echo @$test["titulo"]; ?></h3>
        <img src="<?php echo $urlmedia.$imagen; ?>" alt="imagen" name="imagen" class="_nomimagen img-responsive center-block thumbnail" id="imagen" style="max-width: 200px;  max-height: 200px;">
     </div>    
     <div class="col-md-6 text-center">
      <table id="tblEscalas2" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>           
              <th>Nombre</th>
              <th>Descripcion</th>   
            </tr>
          </thead>
          <tbody> 
          <?php 
         
          $puntaje=!empty($test["puntaje"])?json_decode($test["puntaje"]):array();
          if(!empty($puntaje)){ 
            foreach ($puntaje as $k => $v){             
              echo '<tr><td>'.@$v->nombre.'</td><td>'.@$v->description.'</td></tr>';
            }
          }  ?>         
          </tbody>         
        </table>
     </div>
      <div class="col-md-12">
        <table id="tblEscalas3" class="table table-reponsive table-striped ">
          <thead class="bg-blue">
            <tr>
              <th >#</th>
              <th >Criterio</th>
              <th >Valor</th>
            </tr>
          </thead>
          <tbody> 
            <?php          
          if(!empty($this->criterios)){ 
            foreach ($this->criterios as $k => $v){
              $idtestasigancion=(!empty($v["idtestasigancion"])?$v["idtestasigancion"]:$this->idtestasigancion);
              $puntajemarcado=(!empty($v["puntaje"])?$v["puntaje"]:'');
               $optionpuntaje='';
              if(!empty($puntaje)){ 
                 $optionpuntaje='<option value="0">'.JrTexto::_('Selected').'</option>';
                foreach ($puntaje as $kp => $vp){
                  $optionpuntaje.='<option value="'.@$vp->nombre.'" '.($vp->nombre==$puntajemarcado?'selected="selected"':'').' >'.@$vp->nombre.'</option>';
                }
              }

              echo '<tr data-idtestalumno="'.(!empty($v["idtestalumno"])?$v["idtestalumno"]:0).'" data-idtestasigancion="'.$idtestasigancion.'" data-idtestcriterio="'.@$v["idtestcriterio"].'" data-idtest="'.@$v["idtest"].'" ><td>'.($k+1).'</td><td>'.@$v["criterio"].'</td><td style="max-width: 150px"><div class="select-ctrl-wrapper select-azul">
                      <select class="selcriteriovalor select-ctrl form-control"></div>'.$optionpuntaje.'</select></div></td></tr>';
            }
          } ?>        
          </tbody>
          
        </table>
      </div>
      <div class="col-md-12 text-center"><hr>
         <a type="button" class="btn btn-info btnsaveTest close cerrarmodal" href="#"><i class=" fa fa-close"></i><?php echo JrTexto::_('Exit');?>  </a>
      </div>
  </div>

<script type="text/javascript">
$(document).ready(function(){

$('table#tblEscalas3').on('change','select.selcriteriovalor',function(ev){
  var tr=$(this).closest('tr');
  var fd = new FormData();
    fd.append('idtest', tr.attr('data-idtest')||0);   
    fd.append('idtestcriterio', tr.attr('data-idtestcriterio')||0);
    fd.append('idtestalumno', tr.attr('data-idtestalumno')||0);
    fd.append('idtestasigancion', tr.attr('data-idtestasigancion')||1);
    fd.append('puntaje',$(this).val()||0);  
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/test_alumno/guardarTest_alumno',
      msjatencion:'<?php echo JrTexto::_('Attention');?>',
      type:'json',
      showmsjok : false,
      callback:function(data){
          tr.attr('data-idtestalumno',data.newid);
      }
    })
})
});
</script>

