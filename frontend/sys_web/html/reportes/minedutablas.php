<?php
$json_tablamacro = $this->_tablamacro;
$json_tablamicro = $this->_tablamicro;

//@extract($_GET);
if(isset($_GET['iiee']) && isset($_GET['ubigeo'])){
    echo '<input type="hidden" id="iiee" value="'.$_GET['iiee'].'"/>';
    echo '<input type="hidden" id="ubigeo" value="'.$_GET['ubigeo'].'"/>';
    // var_dump($_GET['iiee']);
    // var_dump($_GET['ubigeo']);
}

?>
<style>

.select-ctrl-wrapper:after{
  right:0!important;
}
.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<?php if(!empty($this->title)): ?>
<h1><?php echo JrTexto::_($this->title)?></h1>
<?php endif; ?>


<div id="container-report" style="margin:0!important">
<!-- START SECTION 1 UBICACION-->
<!--START GRAPHICS-->
<div class="row">
    <div class="col-md-4 section-u-dre">
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio"></canvas>
        </div>
    </div>
    <div class="col-md-2 section-u-dre">
        <div style="max-height:190px; overflow-y:scroll;">
            <div style="word-wrap: break-word;"><p><b>DREasdsdaasdasdsadsaddasd1:</b><span>10pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE2:</b><span>15pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE3:</b><span>18pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE4:</b><span>10pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE2:</b><span>15pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE3:</b><span>18pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE4:</b><span>10pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE2:</b><span>15pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE3:</b><span>18pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>DRE4:</b><span>10pts</span></p></div>
        </div>
    </div>
    <div class="col-md-4 section-u-dre">
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio02"></canvas>
        </div>
    </div>
    <div class="col-md-2 section-u-dre">
        <div style="max-height: 190px; overflow-y:scroll;">
            <div style="word-wrap: break-word;"><p><b>UGEL1:</b><span>10pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>UGEL2:</b><span>15pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>UGEL3:</b><span>18pts</span></p></div>
            <div style="word-wrap: break-word;"><p><b>UGEL4:</b><span>10pts</span></p></div>
        </div>
    </div>
    <div class="col-md-12 section-u-cole"><hr class="linedivisor" /></div>
    <div class="col-md-6 section-u-cole">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <th>#</th>
                <th>Colegio</th>
                <th>Nota promedio</th>
                </thead>
                <tbody>
                <tr>
                <td>0</td>
                <td>Colegio nombre prueba</td>
                <td>17pts</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 section-u-cole">
        <div id="idcolegio-container" style="display: inline-block;">
                        
            <h4 style="display:inline-block;"><?php echo JrTexto::_("School"); ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="idcolegio" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_("Select").'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                    <option value="1">seleccionar</option>
                </select>
            </div>
        </div>
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio03"></canvas>
        </div>
    </div>
    <div class="col-md-12 section-u-grado"><hr class="linedivisor" /></div>
    <div class="col-md-12 section-u-grado">
        <div class="col-sm-6">
            <label><?php echo JrTexto::_("Grade"); ?></label>        
            <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
                <canvas id="chartUbicacion_promedio04"></canvas>
            </div>
        </div>
        <div class="col-sm-6 section-u-grado">
            <label><?php echo JrTexto::_("Section"); ?></label>
            <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
                <canvas id="chartUbicacion_promedio05"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-12"><hr class="linedivisor" /></div>
    <div class="col-md-6 section-u-dre">
        <label><?php echo JrTexto::_("Skill"). ": DRE"; ?></label>
        <div id="iddre-skill-container" style="display: inline-block;">                        
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="iddre-skill" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_("Select").'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                    <option value="1">seleccionar</option>
                </select>
            </div>
        </div>
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio06"></canvas>
        </div>
    </div>
    <div class="col-md-6 section-u-dre">
        <label><?php echo JrTexto::_("Skill"). ": UGEL"; ?></label>
        <div id="idugel-skill-container" style="display: inline-block;">                        
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="idugel-skill" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_("Select").'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                    <option value="1">seleccionar</option>
                </select>
            </div>
        </div>
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio07"></canvas>
        </div>
    </div>
    <div class="col-md-6 section-u-cole">
        <label><?php echo JrTexto::_("Skill"). ": ".JrTexto::_("School"); ?></label>
        <div id="idcolegio-skill-container" style="display: inline-block;">                        
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="idcolegio-skill" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_("Select").'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                    <option value="1">seleccionar</option>
                </select>
            </div>
        </div>
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio08"></canvas>
        </div>
    </div>
    <div class="col-md-6 section-u-grado">
        <label><?php echo JrTexto::_("Skill"). ": ".JrTexto::_("Grade"); ?></label>
        <div id="idgrado-skill-container" style="display: inline-block;">                        
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="idgrado-skill" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_("Select").'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                    <option value="1">seleccionar</option>
                    <option value="1">SADSADSADSADSA ASD SADASDASDSADADSASDASDSADAS</option>
                </select>
            </div>
        </div>
        <div class="chart-container" style="position: relative; margin:0 auto; height:100%; width:100%">
          <canvas id="chartUbicacion_promedio09"></canvas>
        </div>
    </div>
</div>
<!--END GRAPHICS-->
<!-- END SECTION 1 UBICACION-->
    <?php if($this->isExamenes == true):?>
        <div style="display:block; text-align:center; ">
            <select id="select_bimestre_trimestre" style="padding: 10px; width: 250px; font-size: large; margin: 10px auto;">
                <option value="1">Bimestre</option>
                <option value="2">Trimestre</option>
            </select>
        </div>
        <div style="text-align:center;">
            <button id="changebitri" type="button" class="btn btn-default">Generar</button>
        </div>
    <?php endif; ?>
    <div id="tabla01-container">
        <div class="table-responsive">
            <table class="table" id="tabla01">
                <thead>
                    <th>columna1</th>
                    <th>columna2</th>
                    <th>columna3</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    
    </div>
    <div id="tabla02-container">
        <h3>Reporte por IIEE</h3>
        <div class="table-responsive">
            <table class="table" id="tabla02">
                <thead>
                    <th>columna1</th>
                    <th>columna2</th>
                    <th>columna3</th>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
</div>


<script type="text/javascript">
var _tablamacro = <?php echo $json_tablamacro ?>;
var _tablamicro = <?php echo $json_tablamicro ?>;

var drawChart01 = function(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Promedio de Notas'
      },
      scales:{"yAxes":[{"ticks":{"beginAtZero":true}}]},
      tooltips: {
        mode: 'point',
        intersect: true
      },legend: {
            display: false
         }
    }
  });
};
var drawChart02 = function(obj,data, title = 'Ubicación'){
    var ctx = document.getElementById(obj).getContext('2d');
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: {
            // responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: title
            }
        }
    });
}

$('document').ready(function(){
    var chartData = {
        labels: ['DRE4','DRE3','DRE2','DRE1'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
        data: [
            10,15,20,13
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart01('chartUbicacion_promedio',chartData);
    var chartData = {
        labels: ['UGEL4','UGEL3','UGEL2','UGEL1'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
        data: [
            10,15,20,13
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart01('chartUbicacion_promedio02',chartData);
    var _datatopie = {
        datasets: [{
            data: [10,5,10,3,0],
            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'A1',
            'A2',
            'B1',
            'B2',
            'C1'
        ]
    };
    drawChart02('chartUbicacion_promedio03',_datatopie);
    var chartData = {
        labels: ['Primero','Segundo','Tercero','Cuarto','Quinto'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
        data: [
            10,15,20,13,15
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart01('chartUbicacion_promedio04',chartData);
    var chartData = {
        labels: ['A','B','C','D','F','G','H','I','J'],
        datasets: [ {
        type: 'bar',
        backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
        data: [
            10,15,20,13,15,12,18,15,14
        ],
        borderColor: 'white',
        borderWidth: 2
        }]

    };
    drawChart01('chartUbicacion_promedio05',chartData,'Skill');
    var chartData = {
        datasets: [{
            data: [10,15,10,13],
            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Listening', 'Reading','Writing','Speaking'
        ]

    };
    drawChart02('chartUbicacion_promedio06',chartData,'Skill');
    var chartData = {
        datasets: [{
            data: [10,15,10,13],
            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Listening', 'Reading','Writing','Speaking'
        ]

    };
    drawChart02('chartUbicacion_promedio07',chartData,'Skill');
    var chartData = {
        datasets: [{
            data: [10,15,10,13],
            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Listening', 'Reading','Writing','Speaking'
        ]

    };
    drawChart02('chartUbicacion_promedio08',chartData,'Skill');
    var chartData = {
        datasets: [{
            data: [10,15,10,13],
            backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Listening', 'Reading','Writing','Speaking'
        ]

    };
    drawChart02('chartUbicacion_promedio09',chartData,'Skill');
    // console.table(tablaGeneral);
    $('#container-report').parent().css('margin','0px');//.css('margin','0px!important');
    $('#container-report').parent().css('width','100%');//.css('margin','0px!important');
    // console.log(_tablamacro);
    // console.log(_tablamicro);

    if(_tablamacro.header != null && _tablamacro.body != null){
        $('#tabla01').find('thead').html('<tr></tr>');
        $('#tabla01').find('tbody').html(' ');
        var cabecera = '';
        var row = '';
        for(var i in _tablamacro.header){
            if(typeof _tablamacro.header[i] == 'string'){
                // console.log('%c pasa', 'color:blue')
                // console.log(_tablamacro.header[i]);
                cabecera = '<th>'+_tablamacro.header[i]+'</th>';
                $('#tabla01').find('thead tr').append(cabecera);
            }
        }
        for(var i in _tablamacro.body){
            row = null;
            if(typeof _tablamacro.body[i] != 'undefined' && _tablamacro.body[i] && typeof _tablamacro.body[i] != 'function'){
                row = $('<tr></tr>');
                for(var j in _tablamacro.body[i]){
                    row.append('<td>'+_tablamacro.body[i][j]+'</td>');
                }
                $('#tabla01').find('tbody').append(row);
            }
        }
        
    }//end if
    else{
        $('#tabla01-container').hide();
    }
    if(_tablamicro.header != null){
        $('#tabla02').find('thead').html('<tr></tr>');
        $('#tabla02').find('tbody').html(' ');
        var cabecera = '';
        var row = '';
        for(var i in _tablamicro.header){
            if(typeof _tablamicro.header[i] == 'string'){
                // console.log('%c pasa', 'color:blue')
                // console.log(_tablamacro.header[i]);
                cabecera = '<th>'+_tablamicro.header[i]+'</th>';
                $('#tabla02').find('thead tr').append(cabecera);
            }
        }//endfor _tablamicro
        for(var i in _tablamicro.body){
            row = null;
            if(typeof _tablamicro.body[i] != 'undefined' && _tablamicro.body[i] && typeof _tablamicro.body[i] != 'function'){
                row = $('<tr></tr>');
                for(var j in _tablamicro.body[i]){
                    row.append('<td>'+_tablamicro.body[i][j]+'</td>');
                }
                $('#tabla02').find('tbody').append(row);
            }
        }//endfor _tablamicro
    }else{

        $('#tabla02-container').hide();
    }

    $("#tabla01").DataTable({dom: 'Bfrtip', buttons: ['excel', 'pdf', 'print' ]});
    $("#tabla02").DataTable({dom: 'Bfrtip', buttons: ['excel', 'pdf', 'print' ]});

    /*Events*/
    $("#changebitri").on('click',function(){
        
        if($("#select_bimestre_trimestre").val() != 0 ){
            var _data = new Object;
            var esIIEE = false;
            if($('#iiee').length > 0 && $('#ubigeo').length > 0){
                _data.iiee = $('#iiee').val();
                _data.ubigeo = $('#ubigeo').val();
                esIIEE = true;
            }
            if($("#select_bimestre_trimestre").val() == 2){
                _data.istrimestre = true;
                if($('#trimestre-content').length > 0){
                        $('#tabla02-container').hide();
                        $('#tabla01-container').hide();
                    if(esIIEE == true){
                    }else{
                    }
                    $('#trimestre-content').show();
                    _data = null;
                }
            }else{
                _data = null;
            }
            
            if($("#select_bimestre_trimestre").val() == 1){
                if($('#iiee').length > 0 && $('#ubigeo').length > 0){
                    $('#tabla02-container').show();
                }else{
                    $('#tabla01-container').show();
                }
                $('#trimestre-content').hide();
            }
            if(_data != null){
                $('#changebitri').html('<p class="loading-chart" style="font-size:24px; width:100%; text-align:center;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:2em;"></i><span style="width:100%; display:block;">Cargando...</span></p>');
                $('#changebitri').css('pointer-events','none');
                $.ajax({
                    url: _sysUrlBase_+'/reportes/minedu_examenes',
                    type: 'POST',
                    dataType: 'json',
                    data: _data,
                }).done(function(resp) {
                    if(resp.code=='ok'){
                        if($('#trimestre-content').length == 0){
                                var contenedor = $('<div></div>');
                                contenedor.attr('id','trimestre-content');
                                $('#container-report').append(contenedor);
                        }
                        var t1 = JSON.parse(resp.data[0]);
                        var t2 = JSON.parse(resp.data[1]);
                        
                        if(t1.header != null && t1.body != null){
                            
                            if($('#table-trimestre01-content').length == 0){
                                var contenedor2 = $('<div></div>');
                                contenedor2.attr('id','table-trimestre01-content');
                                contenedor2.addClass('table-responsive');
                                var table = $('<table></table>');
                                
                                table.addClass('table');
                                table.append('<thead><tr></tr></thead>');
                                var cabecera = '';
                                var row = '';
                                for(var i in t1.header){
                                    if(typeof t1.header[i] == 'string'){
                                        // console.log('%c pasa', 'color:blue')
                                        // console.log(_tablamacro.header[i]);
                                        cabecera = '<th>'+t1.header[i]+'</th>';
                                        table.find('thead tr').append(cabecera);
                                    }
                                }
                                table.append('<tbody></tbody>');
                                for(var i in t1.body){
                                    row = null;
                                    if(typeof t1.body[i] != 'undefined' && t1.body[i] && typeof t1.body[i] != 'function'){
                                        row = $('<tr></tr>');
                                        for(var j in t1.body[i]){
                                            row.append('<td>'+t1.body[i][j]+'</td>');
                                        }
                                        table.find('tbody').append(row);
                                    }
                                }
                                contenedor2.append(table);
                                $('#trimestre-content').append(contenedor2);
                                $('#table-trimestre01-content').find('table').DataTable({dom: 'Bfrtip', buttons: ['excel', 'pdf', 'print' ]});
                                $('#tabla01-container').hide();
                                $('#tabla02-container').hide();
                            }
                        }
                        if(t2.header != null && t2.body != null){
                            if($('#table-trimestre02-content').length == 0){
                                var contenedor2 = $('<div></div>');
                                contenedor2.attr('id','table-trimestre02-content');
                                contenedor2.addClass('table-responsive');
                                var table = $('<table></table>');
                                table.addClass('table');
                                table.append('<thead><tr></tr></thead>');
                                var cabecera = '';
                                var row = '';
                                for(var i in t2.header){
                                    if(typeof t2.header[i] == 'string'){
                                        // console.log('%c pasa', 'color:blue')
                                        // console.log(_tablamacro.header[i]);
                                        cabecera = '<th>'+t2.header[i]+'</th>';
                                        table.find('thead tr').append(cabecera);
                                    }
                                }
                                table.append('<tbody></tbody>');
                                for(var i in t2.body){
                                    row = null;
                                    if(typeof t2.body[i] != 'undefined' && t2.body[i] && typeof t2.body[i] != 'function'){
                                        row = $('<tr></tr>');
                                        for(var j in t2.body[i]){
                                            row.append('<td>'+t2.body[i][j]+'</td>');
                                        }
                                        table.find('tbody').append(row);
                                    }
                                }
                                contenedor2.append(table);
                                $('#trimestre-content').append(contenedor2);
                                $('#table-trimestre02-content').find('table').DataTable({dom: 'Bfrtip', buttons: ['excel', 'pdf', 'print' ]});
                                $('#tabla01-container').hide();
                                $('#tabla02-container').hide();
                            }
                        }
                    } else {
                        return false;
                    }
                    $('#changebitri').html('generar');
                    $('#changebitri').css('pointer-events','initial');
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    $('#changebitri').html('generar');
                    $('#changebitri').css('pointer-events','initial');
                    return false;
                });
            }else{
                console.log("error, falta definir la institucion o el ubigeo");
                return false;
            }
        }//end if select bimestre or trimestre val
    });
});
</script>