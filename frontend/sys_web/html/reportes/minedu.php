<?php
defined('RUTA_BASE') or die();

$idrol = $this->idrol;
$json_ubicaciones_filtros = json_encode($this->ubicaciones_filtros);
$director = "Usuario";
switch(intval($this->idrol)){
    case 9: $director = "Director Nacional";
    break;
    case 8: $director = "Director DRE";
    break;
    case 7: $director = "Director UGEL";
    break;
    case 6: $director = "Director IIEE";
    break;
}

?>

<style>

.bootstrap-datetimepicker-widget table td.active.today:before {
    border-bottom-color: #fff;
}

.bootstrap-datetimepicker-widget table td.today:before {
    content: '';
    display: inline-block;
    border: solid transparent;
    border-width: 0 0 7px 7px;
    border-bottom-color: #337ab7;
    border-top-color: rgba(0, 0, 0, 0.2);
    position: absolute;
    bottom: 4px;
    right: 4px;
}

.bootstrap-datetimepicker-widget table td.disabled{
    background-color:#ff00008f!important;
}


/*switcher*/
/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #337ab7;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #b73333;
}

input:focus + .slider {
  box-shadow: 0 0 1px #b73333;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.closefilter{
    
    /* right: 23px; */
    float: right;
    margin-bottom: 5px;
    border: 1px solid wheat;
    border-radius: 0.3em;
    padding: 1px 9px;
    background:white;
    font-size: 19px;
    color: black;
}
.closefilter:hover{ color: white; background: #ff5656;}
/*//////////////*/
.tabcontent-filter { border: 1px solid #d0d0d0; padding: 3px; border-radius: 0 0 0.5em 0.5em; border-top: none; }
.title-main{ border-bottom: 2px solid gray; border-radius:1em; font-weight:700; text-shadow:1px 1px 2px gray; text-align: center;}
.pills-content>li{ border: 1px solid #cacaca; border-radius: 0.5em; margin:2px;}
.pills-content{ border-bottom:1px solid #337ab7; padding-bottom:5px; border-radius:0.5em; }
.pills-ugel>li,.pills-ugel>li>a {  padding:5px!important;}
.title-c{ border-bottom: 2px solid red;}
.disabled { cursor:no-drop;}
.btn.disabled{pointer-events: none;}
.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<div class="container">
<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li class="active">
          <?php 
              echo "Reporte";
          ?>
        </li>
	  </ol>	
	</div>
</div>
    <!--start panel-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <?php echo JrTexto::_("Reporte");?>
        </div>
        <div class="panel-body">
            <!--start row-->
            <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle"  style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">
                    <h2 style="text-align:center;"><?php echo $this->fullname ?></h2>
                    <h4 class="alert alert-info" style="text-align:center;"><?php echo $director ?></h4>
                </div>
                <div class="col-md-9">
                    <div class="form-group" style="display:none;">
                        <label for="sel1">Reportes a nivel:</label>
                        <select class="form-control" id="tiporeporte">
                            <option value="0">sin datos</option>
                        </select>
                    </div>
                    <div class="col-sm-12">
                        <div style="text-align:center;">
                            <?php if($this->user['idrol'] == 9): ?>
                            <button id="btn_nacional" data-type="1" class="btn btn-danger btn_filter" style="padding:20px; margin:0 5px; background-color:darkred; font-weight:bold;" type="button">Nivel Nacional <i class="fa fa-search" style="display:block; font-size:20px;"></i></button>
                            <?php endif; ?>
                            <?php if($this->user['idrol'] == 8 | $this->user['idrol'] == 9): ?>
                            <button id="btn_dre"  data-type="2" data-display="#filter_display_dre" class="btn btn-primary btn_filter" style="padding:20px; margin:0 5px; font-weight:bold; min-width:100px;" type="button">DRE<i class="fa fa-search" style="display:block; font-size:20px;"></i></button>
                            <?php endif; ?>
                            <?php if($this->user['idrol'] == 7 | $this->user['idrol'] == 8 | $this->user['idrol'] == 9): ?>
                            <button id="btn_ugel" data-type="3" data-display="#filter_display_ugel" class="btn btn-info btn_filter" style="padding:20px; margin:0 5px; font-weight:bold; min-width:100px;" type="button">UGEL<i class="fa fa-search" style="display:block; font-size:20px;"></i></button>
                            <?php endif; ?>
                            <?php if($this->user['idrol'] == 6 || $this->user['idrol'] == 7 || $this->user['idrol'] == 8 || $this->user['idrol'] == 9): ?>
                            <button id="btn_iiee" data-type="4" data-display="#filter_display_iiee" class="btn btn-warning btn_filter" style="padding:20px; margin:0 5px; font-weight:bold; min-width:100px;" type="button">IIEE<i class="fa fa-search" style="display:block; font-size:20px;"></i></button>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!--display:none; border: 1px solid darkgray; border-radius:0.5em; margin-top: 5px; padding-top: 5px;-->
                    <div id="filter_display_dre" class="col-sm-12" style="display:none; border: 1px solid #337ab7; border-radius:0.5em; margin-top: 5px; padding-top: 5px;">
                        <div class="col-sm-8">
                            <div style="display:block;"></div>
                            <div class="col-sm-6" style="width:100%; padding-top:9px; border-radius: 0.5em; border: 1px solid #D0D0D0;">
                                <div class="form-group">
                                    <label for="sel1">DRE:</label>
                                    <select class="form-control departamentos">
                                        <?php if(!empty($this->ubicaciones_filtros['departamentos'])): ?>
                                        <option value="0">Seleccionar</option>
                                        <?php foreach($this->ubicaciones_filtros['departamentos'] as $value): ?>
                                            <option value="<?php echo $value['iddre'] ?>" data-departamento="<?php echo $value['ubigeo'] ?>"><?php echo $value['descripcion'] ?></option>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                            <option value="0">Sin datos</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">

                            <div class="form-group">
                                <label for="sel1">A&ntilde;o:</label>
                                <select class="form-control anio" disabled>
                                    <option value="2019">2019</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sel1">Personal:</label>
                                <select class="form-control personal">
                                    <option value="A">Alumno</option>
                                    <option value="D">Docente</option>
                                    <!-- <option value="3">Alumno/Docente</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="filter_display_ugel" class="col-sm-12" style="display:none; border: 1px solid #337ab7; border-radius:0.5em; margin-top: 5px; padding-top: 5px;">
                        <div class="col-sm-6">
                            <div class="col-sm-6" style="width:100%; padding-top:9px; border-radius: 0.5em; border: 1px solid #D0D0D0;">
                                <div class="form-group">
                                    <label for="sel1">DRE:</label>
                                    <select class="form-control departamentos" >
                                        <?php if(!empty($this->ubicaciones_filtros['departamentos'])): ?>
                                        <option value="0">Seleccionar</option>
                                        <?php foreach($this->ubicaciones_filtros['departamentos'] as $value): ?>
                                            <option value="<?php echo $value['iddre'] ?>"><?php echo $value['descripcion'] ?></option>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                            <option value="0">Sin datos</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-6" style="width:100%; padding-top:9px; border-radius: 0.5em; border: 1px solid #D0D0D0;">
                                <div class="form-group">
                                    <label>UGEL</label><br>
                                    <select class="form-control ugel">
                                        <?php if(!empty($this->ubicaciones_filtros['ugel'])): ?>
                                        <option value="0">Seleccionar</option>
                                        <?php foreach($this->ubicaciones_filtros['ugel'] as $value): ?>
                                            <option value="<?php echo $value['idugel'] ?>" data-departamento="<?php echo $value['iddepartamento'] ?>" ><?php echo $value['descripcion'] ?></option>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                            <option value="0">Sin datos</option>
                                        <?php endif; ?>
                                    </select>                                
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sel1">A&ntilde;o:</label>
                                <select class="form-control anio" disabled>
                                    <option value="2019">2019</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sel1">Personal:</label>
                                <select class="form-control personal">
                                    <option value="A">Alumno</option>
                                    <option value="D">Docente</option>
                                    <!-- <option value="3">Alumno/Docente</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="filter_display_iiee" class="col-sm-12" style="display:none; border: 1px solid #337ab7; border-radius:0.5em; margin-top: 5px; padding-top: 5px;">
                        <div class="col-sm-6">
                            <div class="col-sm-6" style="width:100%; padding-top:9px; border-radius: 0.5em; border: 1px solid #D0D0D0;">
                                <div class="form-group">
                                    <label for="sel1">DRE:</label>
                                    <select class="form-control departamentos" >
                                        <?php if(!empty($this->ubicaciones_filtros['departamentos'])): ?>
                                        <option value="0">Seleccionar</option>
                                        <?php foreach($this->ubicaciones_filtros['departamentos'] as $value): ?>
                                            <option value="<?php echo $value['iddre'] ?>"><?php echo $value['descripcion'] ?></option>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                            <option value="0">Sin datos</option>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-6" style="width:100%; padding-top:9px; border-radius: 0.5em; border: 1px solid #D0D0D0;">
                                <div class="form-group">
                                    <label>UGEL</label><br>
                                    <select class="form-control ugel">
                                        <?php if(!empty($this->ubicaciones_filtros['ugel'])): ?>
                                        <option value="0">Seleccionar</option>
                                        <?php foreach($this->ubicaciones_filtros['ugel'] as $value): ?>
                                            <option value="<?php echo $value['idugel'] ?>" data-departamento="<?php echo $value['iddepartamento'] ?>" ><?php echo $value['descripcion'] ?></option>
                                        <?php endforeach;?>
                                        <?php else: ?>
                                            <option value="0">Sin datos</option>
                                        <?php endif; ?>
                                    </select>                                
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group" style="text-align:center;" id="iiee-group">
                                <label for="sel1" >Intituci&oacute;n:</label>
                                <!-- <select class="form-control selectpicker" disabled id="iiee" data-show-subtext="true" data-live-search="true"> -->
                                <select class="form-control selectpicker iiee" id="iiee" data-live-search="true">
                                    <option value="0">seleccionar</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sel1">A&ntilde;o:</label>
                                <select class="form-control anio" disabled>
                                    <option value="2019">2019</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="sel1">Personal:</label>
                                <select class="form-control personal">
                                    <option value="A">Alumno</option>
                                    <option value="D">Docente</option>
                                    <!-- <option value="3">Alumno/Docente</option> -->
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div style="display:block; padding:5px;"></div>
                    </div>
                    
                </div>
                    <div class="col-sm-4" style="display:none;">
                        <div class="form-group">
                            <label for="sel1">Periodo:</label>
                            <label class="check-all"><input type="checkbox" id="check-periodo" value="" />Periodo Completo</label>
                            <select class="form-control" disabled id="periodo">
                                <option value="1">Bimestre</option>
                                <option value="2">Trimestre</option>
                                <option value="3">Bimestre/Trimestre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2" style="display:none;">
                        <div class="form-group">
                            <label for="sel1">Desde:</label>
                            <input type="text" disabled class="form-control" id="datepicker01" />
                        </div>
                    </div>
                    <div class="col-sm-2" style="display:none;">
                        <div class="form-group">
                            <label for="sel1">Hasta:</label>
                            <input type="text" disabled class="form-control" id="datepicker02" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary" style="display:none; width:100%;" id="generar" type="button">Generar Reportes</button>
                    </div>
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
    <!--end panel-->
    <div class="panel panel-primary" id="reportes-contenedor">
        <div class="panel-body">
            <h4>Tipos de Reportes</h4>
            <ul class="nav nav-pills pills-content">
                <li class="active"><a data-toggle="pill" href="#home">Examen de Ubicaci&oacute;n</a></li>
                <li><a data-toggle="pill" href="#menu1">Tiempos en la Plataforma Virtual</a></li>
                <li><a data-toggle="pill" href="#menu2">Uso y Dominio de la Plataforma Virtual </a></li>
                <li><a data-toggle="pill" href="#menu4">Examen de Entrada y Salida</a></li>
                <li><a data-toggle="pill" href="#menu5">Examen Bimestral y Trimestral </a></li>
                <!-- <li><a data-toggle="pill" href="#menu6">Comparativo de Entrada y Salida </a></li> -->
                <li><a data-toggle="pill" href="#menu7">Progreso en la Plataforma Virtual</a></li>
                <li><a data-toggle="pill" href="#menu8">Progreso por Habiilidad</a></li>
            </ul>
            <div class="tab-content">
                <div id="home" style="position:relative;" class="tab-pane fade in active">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;">
                        <img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive">
                    </div>
                </div>
                <div id="menu1" style="position:relative;" class="tab-pane fade">    
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
                <div id="menu2" style="position:relative;" class="tab-pane fade">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
                <div id="menu4" style="position:relative;" class="tab-pane fade">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
                <div id="menu5" style="position:relative;" class="tab-pane fade">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
                <!-- <div id="menu6" class="tab-pane fade">
                    <h1 class="loading-chart" style="font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">CARGANDO GRAFICOS DE: <span class="nameMenu"></span></span></h1>
                </div> -->
                <div id="menu7" style="position:relative;" class="tab-pane fade">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
                <div id="menu8" style="position:relative;" class="tab-pane fade">
                    <div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="<?php echo $this->documento->getUrlStatic(); ?>/tema/css/images/cargando.gif" class="img-responsive"></div>                
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
/**CONTANTES O GLOBALES */
var ubicaciones_filtros = <?php echo $json_ubicaciones_filtros ?>;
// console.log(ubicaciones_filtros);
var iiee = null;
var rol = <?php echo $idrol ?>;
var iddepartamento = null;
var datosPreparados = new Object;
var typeFilter = 1;

// console.log(ubicaciones_filtros);

function loadIIEE(idrol = null){
    var datos = new Object;
    datos.idproyecto = 3;
    if(idrol == 8){
        //codigo ubigeo
        if(Object.keys(ubicaciones_filtros.departamentos).length > 0 && ubicaciones_filtros.departamentos != null){
            datos.idubigeo = ubicaciones_filtros.departamentos[0].id_ubigeo;
        }
    }
    if(idrol == 7){
        //id ugel
        if(Object.keys(ubicaciones_filtros.ugel).length > 0 && ubicaciones_filtros.ugel != null){
            datos.idugel = ubicaciones_filtros.ugel[0].idugel;
        }
    }
    if(idrol == 6){
        //id local
    }

    $.ajax({
        url: _sysUrlBase_+'/reportes/listarinstituciones',
        type: 'POST',
        dataType: 'json',
        data: datos,
    }).done(function(resp) {
        if(resp.code=='ok'){
            if(Object.keys(resp.data).length > 0){
                iiee = resp.data;
            }else{
                $('.iiee').html('<option value="0">Sin datos</option>');
            }
            // console.log(iiee);
        }else{
            return false;
        }
    }).fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
}

function initFiltros(){


    //main
    var tipos = '';
    var iddre  = 0;
    var idugel  = 0;
    var idlocal  = 0;
    if(rol == 8 || rol == 7 || rol == 6 || rol == 5){
        iddre = $('#filter_display_dre').find('.departamentos option').eq(1).attr('value');
        idugel = $('#filter_display_ugel').find('.ugel option').eq(1).attr('value');
        idlocal = $('#filter_display_iiee').find('.iiee option').eq(1).attr('value');
    }
    switch(rol){
        //Minedu
        // case 2: {
        //     tipos = '<option value="1">Nacional</option><option value="2">Dirección Regional de Educación</option><option value="3">Unidad de Gestión Educativa Local</option><option value="4">Institucional</option>';
        //     $('.check-all').each(function(k,v){
        //         if(k != 0){
        //             $(this).addClass('disabled');
        //             $(this).find('input').prop('disabled', true);
        //         }
        //     });
        // } break;
        
        case 9:{
            //nacional
        } break;
        case 8:{
            //dre
            $('#filter_display_dre').find('.departamentos').val(iddre);
            $('#filter_display_ugel').find('.departamentos').val(iddre);            
            $('#filter_display_iiee').find('.departamentos').val(iddre);
        } break;
        case 7:{
            //ugel
            $('#filter_display_ugel').find('.departamentos').val(iddre);            
            $('#filter_display_ugel').find('.ugel').val(idugel);
            $('#filter_display_iiee').find('.departamentos').val(iddre);
            $('#filter_display_iiee').find('.ugel').val(idugel);
        } break;
        case 6:{
            //iiee
            $('#filter_display_iiee').find('.departamentos').val(iddre);
            $('#filter_display_iiee').find('.ugel').val(idugel);
            $('#filter_display_iiee').find('.iiee').val(idlocal);
            $('.selectpicker').selectpicker('refresh');
        } break;

    }
    $('#tiporeporte').html(tipos);
    // if($('#departamentos').val() != 0){
    //     $('#provincia').prop('disabled', false);
    // }
    // if($('#distrito').val() != 0){
    //     $('#provincia').prop('disabled', false);
    // }
    if($('#departamentos').val() != 0){
        $('#ugel').prop('disabled', false);
    }


}

function pad(number, length, right = false){
    var str = '' + number;
    while(str.length < length){
        str = (right == false) ? 0 + str : str + 0;
    }
    return str;
}

function disabledElemento(_ide,onlyInput = null, notdisabled = null){
    var input_ = (notdisabled == true) ? $('#'+_ide).prop('disabled',false) : $('#'+_ide).prop('disabled',true);
    if(onlyInput == null){ var addclassdisabled = (notdisabled == true) ? $('#'+_ide).parent().removeClass('disabled') : $('#'+_ide).parent().addClass('disabled'); }
}
function llenarinstitucionesxugel(idugel,todo = null){

    if(iiee != null){
        var iiee_select = null;
        $('.iiee').each(function(k,v){
            if($(this).is(":visible")){
                iiee_select = $(this).find('.iiee');
                return false;
            }
        });
        // console.log(iiee_select.html());
        if(iiee_select != null){
            // console.log("aqui");
            iiee_select.html(' ');
            iiee_select.append('<option value="0">Seleccionar</option>');
            if(iddepartamento !=null || todo == true){
                var _option_iiee = '';
                for(var i in iiee){
                    if(typeof iiee[i].idugel != 'undefined'){
                        if(iiee[i].idugel == idugel){
                            _option_iiee = '';
                            _option_iiee = '<option value="'+iiee[i].idlocal+'" data-ubigeo="'+iiee[i].id_ubigeo+'">'+iiee[i].nombre+'</option>';
                            iiee_select.append(_option_iiee);
                        }
                    }
                }// end foreach
            }//end if verificaicon
        }

    }//endif iee
}

function llenarinstituciones(codigo,tipo,todo = null){
    //tipo : 1 departamento, 2 provincia, 3 distrito
    if(iiee != null){
        $('#iiee').html(' ');
        $('#iiee').append('<option value="0">Seleccionar</option>');
        if(iddepartamento !=null || todo == true){
            var _option_iiee = '';
            for(var i in iiee){
                if(typeof iiee[i].id_ubigeo != 'undefined'){
                    var myReg = new RegExp((codigo.toString()) + ".*");
                    // console.log(myReg);
                    if ((iiee[i].id_ubigeo.toString()).substring(0,(tipo*2)).match(myReg) || todo == true) {
                        _option_iiee = '';
                        _option_iiee = '<option value="'+iiee[i].idlocal+'" data-ubigeo="'+iiee[i].id_ubigeo+'">'+iiee[i].nombre+'</option>';
                        $('#iiee').append(_option_iiee);
                    }
                }//endif typeof
            }
        }//endif diferente de null
    }//end if relleno de iiee
}

var verificar = function(obj,type=null){
    if(type != null){
        switch(type){
            case 1: {

            }
            break;
            case 2: {
                if(typeof obj.dre == 'undefined' || obj.dre == 'NaN000'){
                    return false;
                }
            }
            break;
            case 3: {
                if((typeof obj.dre == 'undefined' || obj.dre == 'NaN000') || typeof obj.ugel == 'undefined'){
                    return false;
                }
            }
            break;
            case 4: {
                if((typeof obj.dre == 'undefined' || obj.dre == 'NaN000') && typeof obj.ugel == 'undefined' && typeof obj.iiee == 'undefined' || obj.iiee == ''){
                    return false;
                }
            }
            break;

        }
    }else{
        if(typeof obj.departamentocompleto == 'undefined' && typeof obj.ubigeo == 'undefined'){
            return false;
        }
        if(typeof obj.iieecompleto == 'undefined' && typeof obj.iiee == 'undefined'){
            return false;
        }
        if(typeof obj.iieecompleto == 'undefined' && typeof obj.iiee == 'undefined'){
            return false;
        }
    }
    // if(typeof obj.obj == 'undefined'){
    //     if(obj.desde == '' && obj.hasta == ''){
    //         return false;
    //     }
    // }

    return true;
}

function loadIframe(pills = null){
    //do it any code
    $(pills).find('.loading-chart').hide();
}

function generariframe(params,type=1){
    var controllerAndFunction = '';
    var _url = '';
    var data = '';

    data = new Array();
    if(typeof params.idreporte != 'undefined'){
        data.push('idreporte='+params.idreporte);
    }
    if(typeof params.dre != 'undefined'){
        data.push('dre='+params.dre);
    }
    if(typeof params.ugel != 'undefined'){
        data.push('ugel='+params.ugel);
    }
    if(typeof params.iiee != 'undefined'){
        data.push('iiee='+params.iiee);
    }
    if(typeof params.persona != 'undefined'){
        data.push('persona='+params.persona);
    }
    if(typeof params.anio != 'undefined'){
        data.push('anio='+params.anio);
    }
    data.push('plt=sintop2');
    if(data.length > 0){
        data = data.join("&");
    }
    switch(type){
        case 1:{
            controllerAndFunction = '/reportes/minedu_ubicacion';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            //console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#home').append('<iframe onload="loadIframe(\'#home\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 2:{
            controllerAndFunction = '/reportes/minedu_tiempos';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            //console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu1').append('<iframe onload="loadIframe(\'#menu1\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 3:{
            controllerAndFunction = '/reportes/minedu_usodominios';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            // console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu2').append('<iframe onload="loadIframe(\'#menu2\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 4:{
            controllerAndFunction = '/reportes/minedu_entradasalida';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            // console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu4').append('<iframe onload="loadIframe(\'#menu4\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 5:{
            controllerAndFunction = '/reportes/minedu_examenes';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            // console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu5').append('<iframe onload="loadIframe(\'#menu5\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 6:{
            controllerAndFunction = '/reportes/minedu_progresos';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            // console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu7').append('<iframe onload="loadIframe(\'#menu7\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;
        case 7:{
            controllerAndFunction = '/reportes/minedu_progresos_habilidad';
            
            _url = _sysUrlBase_+controllerAndFunction+'?'+data;
            // console.log('%c '+_url, 'color:blue; font-size:20px;');
            $('#menu8').append('<iframe onload="loadIframe(\'#menu8\');" style="width: 100%; min-height: 600px; max-height: 800px;" src="'+_url+'"></iframe>');
        }
        break;

    }
}
var init_iieeFilter = function(obj){
    var ugelVal = obj.find('.ugel').val();
    var dreVal = obj.find('.departamentos').val();
    if(dreVal != 0 && ugelVal != 0){
        iddepartamento = dreVal;
        llenarinstitucionesxugel(ugelVal);
        $('.selectpicker').selectpicker('refresh');
    }
}
var displayFilter = function(t){
    $('.btn_filter').each(function(k,v){
        if(t == $(this).data('type')){
            $($(this).data('display')).show();
            if(t == 4){
                init_iieeFilter($($(this).data('display')));
            }
        }else{
            $($(this).data('display')).hide();
        }
    });
}
$('document').ready(function(){
    $('#reportes-contenedor').hide();
    loadIIEE(rol);
    $('#datepicker01').datetimepicker({ format:'YYYY-MM-DD' });
    $('#datepicker02').datetimepicker({useCurrent: false, format: 'YYYY-MM-DD'});
    $("#datepicker01").on("dp.change", function (e) {
        $('#datepicker02').data("DateTimePicker").minDate(e.date);
    });
    $("#datepicker02").on("dp.change", function (e) {
        $('#datepicker01').data("DateTimePicker").maxDate(e.date);
    });

    //init filtros

    initFiltros();

    //eventos filtros departamento
    $('.btn_filter').on('click',function(){
        typeFilter = parseInt($(this).data('type'));
        $('#generar').show();
        displayFilter(typeFilter);
    });
    $('body').on('change','.departamentos',function(){
        if(typeFilter > 2 && $(this).val() != 0){
            iddepartamento = $(this).val();
            var ubi = 0;
            for(var i = 0; i < Object.keys(ubicaciones_filtros.departamentos).length;i++){
                
                if(ubicaciones_filtros.departamentos[i].iddre == iddepartamento){
                    ubi = ubicaciones_filtros.departamentos[i].ubigeo;
                }
            }
            // console.log(ubi);
            var obj = null;
            if(typeof ubicaciones_filtros.ugel != 'undefined'){
                var optiones = '';
                var contenedores = $(this).parents("div");

                contenedores.each(function(k,v){
                    if($(this).attr('id') == 'filter_display_ugel' || $(this).attr('id') == 'filter_display_iiee' ){
                        obj = $(this).find('.ugel');
                        return false;
                    }
                });
                if(obj != null && typeof obj != 'undefined'){
                    obj.html(' ');
                    obj.append('<option value="0">Seleccionar</option>');
                    for(var i in ubicaciones_filtros.ugel){
                        optiones = '<option value="'+ubicaciones_filtros.ugel[i].idugel+'" data-departamento="'+ubicaciones_filtros.ugel[i].iddepartamento+'" >'+ubicaciones_filtros.ugel[i].descripcion+'</option>';
                        obj.append(optiones);
                    }
                }
                if(obj != null && typeof obj != 'undefined'){
                    obj.find('option').each(function(k,v){
                        //pad(iddepartamento,6,true)
                        if($(this).data('departamento') != ubi && $(this).val() != 0){
                            $(this).remove();
                        }
                    });
                }
            }
            
        }
    });
    $('#departamentos').change(function(){
        iddepartamento = $('#departamentos').val();
        /*Resetear filtro */
        // console.log(ubicaciones_filtros.provincias);
        if(typeof ubicaciones_filtros.ugel != 'undefined'){
            var optiones = '';
            $('#ugel').html(' ');
            $('#ugel').append('<option value="0">Seleccionar</option>');
            for(var i in ubicaciones_filtros.ugel){
                optiones = '<option value="'+ubicaciones_filtros.ugel[i].idugel+'" data-departamento="'+ubicaciones_filtros.ugel[i].iddepartamento+'" >'+ubicaciones_filtros.ugel[i].descripcion+'</option>';
                $('#ugel').append(optiones);
            }
        }
        if(typeof ubicaciones_filtros.provincias != 'undefined'){
            var optiones = '';
            $('#provincia').html(' ');
            $('#provincia').append('<option value="0">Seleccionar</option>');
            for(var i in ubicaciones_filtros.provincias){
                optiones = '<option value="'+ubicaciones_filtros.provincias[i].provincia+'" data-departamento="'+ubicaciones_filtros.provincias[i].departamento+'">'+ubicaciones_filtros.provincias[i].ciudad+'</option>';
                $('#provincia').append(optiones);
            }

            // $('#distrito').html(' ');
            // $('#distrito').append('<option value="0">Seleccionar</option>');
            // $('#distrito').prop('disabled',true);
        }
        if($(this).val() != 0){
            llenarinstituciones(iddepartamento,1);
        }
        if(iiee != null){
            $('#iiee').prop('disabled',false);
            disabledElemento('check-iiee',null,true);
        }
        disabledElemento('check-Provincia',null,true);

        $('#provincia').find('option').each(function(k,v){
            if($(this).data('departamento') != $('#departamentos').val() && $(this).val() != 0){
                $(this).remove();
            }
        });
        disabledElemento('check-UGEL',null,true);
        disabledElemento('ugel',true,true);

        $('#ugel').find('option').each(function(k,v){
            if($(this).data('departamento') != pad($('#departamentos').val(),6,true) && $(this).val() != 0){
                $(this).remove();
            }
        });


        $('#provincia').prop('disabled', false);

        $('.selectpicker').selectpicker('refresh');
    });
    //eventos filtros ugel
    $('#ugel').change(function(){
        if($(this).val() != 0){
            if(iddepartamento == null){
                iddepartamento = 0;
            }
            llenarinstitucionesxugel($(this).val());
            $('.selectpicker').selectpicker('refresh');
        }
    });
    $('#filter_display_iiee').on('change','.ugel',function(){
        if($(this).val() != 0){
            if(iddepartamento == null){
                iddepartamento = 0;
            }
            llenarinstitucionesxugel($(this).val());
            $('.selectpicker').selectpicker('refresh');
        }
    });
    //eventos filtros provincia
    $('#provincia').change(function(){
        if(typeof ubicaciones_filtros.distrito != 'undefined'){
            var optiones = '';
            // $('#distrito').html(' ');
            // $('#distrito').append('<option value="0">Seleccionar</option>');
            // for(var i in ubicaciones_filtros.distrito){
            //     optiones = '<option value="'+ubicaciones_filtros.distrito[i].distrito+'" data-departamento="'+ubicaciones_filtros.distrito[i].departamento+'" data-provincia="'+ubicaciones_filtros.distrito[i].provincia+'" >'+ubicaciones_filtros.distrito[i].ciudad+'</option>';
            //     $('#distrito').append(optiones);
            // }

            llenarinstituciones(iddepartamento+$('#provincia').val(),2);
        }
        disabledElemento('check-Distrito',null,true);

        // $('#distrito').find('option').each(function(k,v){
        //     // $(this).data('departamento') != iddepartamento

        //     if($('#departamentos').val() != $(this).data('departamento') && $(this).val() != 0 ){
        //         $(this).remove();
        //     }
        // });
        // $('#distrito').find('option').each(function(k,v){
        //     if($('#provincia').val() != $(this).data('provincia') && $(this).val() != 0 ){
        //         $(this).remove();
        //     }
        // });
        // $('#distrito').prop('disabled', false);
        $('.selectpicker').selectpicker('refresh');
    });

    //Evento del filtro distrito
    // $('#distrito').change(function(){
    //     if($('#distrito').val() != 0){
    //         llenarinstituciones(iddepartamento+$('#provincia').val()+$('#distrito').val(),3);
    //     }//end if diferente de cero
    // });

    $('#iiee').change(function(){
        disabledElemento('check-periodo',null,true);
        disabledElemento('personal',true,true);
        disabledElemento('anio',true,true);
        disabledElemento('periodo',true,true);
        disabledElemento('datepicker01',true,true);
        disabledElemento('datepicker02',true,true);
    });
    //eventos filtros check-all
    $('.check-all').on('click','input',function(){
        // console.log($(this).attr('id'));
        if($(this).attr('id') == 'check-Departamentos'){
            if(typeof ubicaciones_filtros.ugel != 'undefined'){
                var optiones = '';
                $('#ugel').html(' ');
                $('#ugel').append('<option value="0">Seleccionar</option>');
                for(var i in ubicaciones_filtros.ugel){
                    if(typeof ubicaciones_filtros.ugel[i] != 'function'){
                        optiones = '<option value="'+ubicaciones_filtros.ugel[i].idugel+'" data-departamento="'+ubicaciones_filtros.ugel[i].iddepartamento+'" >'+ubicaciones_filtros.ugel[i].descripcion+'</option>';
                        $('#ugel').append(optiones);

                    }//end if validacion
                }
            }
            if($(this).is(":checked") == true){

                $('#departamentos').prop('disabled',true);
                if($('#departamentos').val() != 0){
                    // disabledElemento('check-Provincia',null,null);
                    disabledElemento('check-UGEL',null,null);
                    // disabledElemento('check-Distrito',null,null);
                    disabledElemento('provincia',true,null);
                    disabledElemento('ugel',true,null);
                    // disabledElemento('distrito',true,null);
                    $('#provincia').val(0);
                    $('#ugel').val(0);
                    // $('#distrito').val(0);


                }else{
                    disabledElemento('check-UGEL',null,true);
                    disabledElemento('ugel',true,true);
                }
                /** procesos para el filtro de IIEE */
                llenarinstituciones(0,0,true); // todos
                if(iiee != null){
                    disabledElemento('check-iiee',null,true);
                    disabledElemento('iiee',true,true);
                    // $('#personal').prop('disabled',false);
                }
                //$("#ugel").prop('disabled',false);

            }else{
                $('#departamentos').prop('disabled',false);
                if($('#departamentos').val() != 0){
                    $('#ugel').find('option').each(function(k,v){
                        if($(this).data('departamento') != pad($('#departamentos').val(),6,true) && $(this).val() != 0){
                            $(this).remove();
                        }
                    });
                    disabledElemento('check-Provincia',null,true);
                    disabledElemento('check-UGEL',null,true);
                    disabledElemento('provincia',true,true);
                    disabledElemento('ugel',true,true);

                    // disabledElemento('check-Distrito',null,true);
                    // disabledElemento('check-Distrito',null,true);
                    // disabledElemento('distrito',true,true);
                }else{
                    disabledElemento('check-UGEL',null,false);
                    disabledElemento('ugel',true,false);
                }

                /** procesos para el filtro de IIEE */
                llenarinstituciones(iddepartamento,1);
                if(iiee != null){
                    if($('#departamentos').val() == 0){
                        disabledElemento('check-periodo');
                        disabledElemento('check-iiee');
                        disabledElemento('iiee',true);
                        disabledElemento('personal',true);
                        disabledElemento('anio',true);
                        disabledElemento('periodo',true);
                        disabledElemento('datepicker01',true);
                        disabledElemento('datepicker02',true);
                    }
                }
            }

        }
        if($(this).attr('id') == 'check-UGEL'){
            if($(this).is(":checked") == true){
                disabledElemento('ugel',true,null);
                if($('#departamentos').val() != 0){
                    llenarinstituciones(iddepartamento,1);
                }else{
                    llenarinstituciones(0,0,true); // todos
                }
            }else{
                disabledElemento('ugel',true,true);
                if($('#ugel').val() != 0){
                    llenarinstitucionesxugel($('#ugel').val());
                }
            }
            $('.selectpicker').selectpicker('refresh');
        }
        if($(this).attr('id') == 'check-Provincia'){
            if($(this).is(":checked") == true){
                $('#provincia').prop('disabled',true);
                if($('#provincia').val() != 0){
                    // disabledElemento('check-Distrito');
                    // disabledElemento('distrito',true);
                    // $('#distrito').val(0);
                }

                llenarinstituciones(iddepartamento,1);
            }else{
                $('#provincia').prop('disabled',false);
                if($('#provincia').val() != 0){
                    // disabledElemento('check-Distrito',null,true);
                    // disabledElemento('distrito',true,true);

                    llenarinstituciones(iddepartamento+$('#provincia').val(),2);
                }
            }
        }
        // if($(this).attr('id') == 'check-Distrito'){
        //     if($(this).is(":checked") == true){
        //         disabledElemento('distrito',true);
        //         llenarinstituciones(iddepartamento+$('#provincia').val(),2);
        //     }else{
        //         llenarinstituciones(iddepartamento+$('#provincia').val()+$('#distrito').val(),3);
        //         disabledElemento('distrito',true,true);
        //     }
        // }
        if($(this).attr('id') == 'check-iiee'){
            if($(this).is(":checked") == true){
                disabledElemento('personal',true,true);
                disabledElemento('anio',true,true);
                disabledElemento('periodo',true,true);
                disabledElemento('check-periodo',null,true);
                disabledElemento('datepicker01',true,true);
                disabledElemento('datepicker02',true,true);
                // disabledElemento('check-iiee',null,true);
                disabledElemento('iiee',true);
            }else{
                if($('#iiee').val() == 0){
                    $('#personal').prop('disabled',true);
                    $('#anio').prop('disabled',true);
                    disabledElemento('check-periodo');
                    $('#periodo').prop('disabled',true);
                    $('#datepicker01').prop('disabled',true);
                    $('#datepicker02').prop('disabled',true);
                    // disabledElemento('check-iiee');
                    disabledElemento('iiee',true,true);


                }
            }//end if checked
        }
        if($(this).attr('id') == 'check-periodo'){
            if($(this).is(":checked") == true){
                disabledElemento('datepicker01',true);
                disabledElemento('datepicker02',true);
            }else{
                disabledElemento('datepicker01',true,true);
                disabledElemento('datepicker02',true,true);
            }
        }
        $('.selectpicker').selectpicker('refresh');
    });

    //display cargadores....
    // $('#home').find('.nameMenu').text($('.pills-content .active').find('a').text());

    // $('.pills-content').on('click','li',function(){
    //     if(typeof $(this).find('a').attr('href') !== 'undefined' ){
    //         var ide = $(this).find('a').attr('href');
    //         var title = $(this).find('a').text();
    //         $(ide).find('.nameMenu').text(title);
    //     }
    // });
    $('#showiiee').on('click',function(){
        $('#iiee-group').show(1000);
        $('#showgrado').removeClass('disabled');
        $(this).hide();
    });
    $('#showgrado').on('click',function(){
        $('#grado-group').show(1000);
        $(this).hide();
    });
    $('.closefilter').on('click',function(){
        $(this).parent().hide(500);
        $($(this).data('buttonshow')).show(1000);
        if($(this).data('buttonshow') == '#showiiee'){
            $('.closefilter').each(function(k,v){
                if($(this).data('ide') > 0){
                    $(this).parent().hide(500);
                    $($(this).data('buttonshow')).show(1000);
                    $($(this).data('buttonshow')).addClass('disabled');
                    // $(this).show(1000);
                }
            });
        }
    });
    $('#generar').on('click',function(){
        //verificar.....
        var id_modoreporte = typeFilter;
        var id_dre = $('.departamentos:visible').val();
        var id_provincia = $('#provincia').val();
        var id_ugel = $('.ugel:visible').val();
        
        // var id_distrito = $('#distrito').val();
        var id_iiee = $('#iiee:visible').val();
        var id_personal = $('.personal:visible').val();
        var id_anio = $('.anio:visible').val();
        // var id_periodo = $('#periodo').val();
        // var data_datepicker01 = $('#datepicker01').val();
        // var data_datepicker02 = $('#datepicker02').val();
        // var check_departamentos = $('#check-Departamentos').is(':checked');
        // var check_provincia = $('#check-Provincia').is(':checked');
        // var check_ugel = $('#check-UGEL').is(':checked');
        // var check_distrito = $('#check-Distrito').is(':checked');
        // var check_iiee = $('#check-iiee').is(':checked');
        // var check_periodo = $('#check-periodo').is(':checked');
        datosPreparados = new Object;
        //idubigeo
        // if(check_departamentos == true){
        //     datosPreparados.departamentocompleto = true;
        // }else{
        //     //verificar departamento, provincia, distrito....
        //     if(id_departamentos != 0){
        //         var prepararubigeo = id_departamentos;
        //         if(check_provincia == true){
        //             prepararubigeo = pad(prepararubigeo,6,true);
        //         }else{
        //             if(id_provincia != 0){
        //                 prepararubigeo += id_provincia;
        //                 // if(check_distrito == true){
        //                 //     prepararubigeo = pad(prepararubigeo,2,true);
        //                 // }else{
        //                 //     if(id_distrito != 0){
        //                 //         prepararubigeo += id_distrito;
        //                 //     }
        //                 // }
        //             } // end if provincia
        //         }
        //         datosPreparados.ubigeo = pad(prepararubigeo,6,true);
        //     }
        //     if(id_ugel != 0 && check_ugel != true){
        //         datosPreparados.ugel = id_ugel;
        //     }//end if ugel
        // }//end if departamento todo...
        // if(check_iiee == true){
        //     datosPreparados.iieecompleto = true;
        //     datosPreparados.personal = id_personal;
        // }else{
        //     if(id_iiee != 0){
        //         datosPreparados.iiee = id_iiee;
        //         datosPreparados.personal = id_personal;
        //     }
        // }
        if(typeof id_modoreporte != 'undefined' || id_modoreporte != 0 || id_modoreporte != null){
            datosPreparados.idreporte = id_modoreporte;
        }
        if(typeof id_dre != 'undefined' && id_dre != 0 && id_dre != null && id_dre != "0"){
            datosPreparados.dre = id_dre;
        }
        if(typeof id_ugel != 'undefined' && id_ugel != 0 && id_ugel != null && id_ugel != "0"){
            datosPreparados.ugel = id_ugel;
        }
        if(typeof id_iiee != 'undefined' && id_iiee != 0 && id_iiee != null && id_iiee != '0'){
            datosPreparados.iiee = id_iiee;
        }
        
        if(typeof id_personal != 'undefined'){
            datosPreparados.persona = id_personal;
        }
        if(typeof id_anio != 'undefined'){
            datosPreparados.anio = id_anio;
        }
        // if(check_periodo == true){
        //     datosPreparados.periodocompleto = true;
        //     datosPreparados.anio = id_anio;
        //     datosPreparados.periodo = id_periodo;
        // }else{
        //     datosPreparados.anio = id_anio;
        //     datosPreparados.periodo = id_periodo;
        //     datosPreparados.desde = data_datepicker01;
        //     datosPreparados.hasta = data_datepicker02;
        // }

        // console.log(datosPreparados);
        //verificar que los filtros importante esten para poder generar el reporte...
        if(verificar(datosPreparados,typeFilter)){
            $('.loading-chart').each(function(k,v){ $(this).show() });
            $('#reportes-contenedor').find('iframe').each(function(k,v){ $(this).remove();  });
            $('#reportes-contenedor').show();
            $('#reportes-contenedor').find('.tab-pane').each(function(k,v){
                //if($(this).hassClass('active'))
                    $(this).removeClass('active');
                    $(this).removeClass('in');
            });
            $('#reportes-contenedor').find('.pills-content').find('li').each(function(k,v){
                //if($(this).hassClass('active'))
                    $(this).removeClass('active');
            });
            $('#reportes-contenedor').find('.pills-content').find('li').eq(0).addClass('active');
            $('#home').addClass('in');
            $('#home').addClass('active');

            generariframe(datosPreparados,1);

        }else{
            $.alert({
                icon: 'fa fa-warning',
                title: 'Error!',
                content: 'Faltan campos obligatorios (*)',
                confirmButton: 'Ok',
                // cancelButton: 'Cerrar',
                theme: 'light',
                typeAnimated: true,
                confirmButtonClass: 'btn-danger',
            });
        }
    });
    /**EVENTO AL PRESIONAR UNA PILLS O TAB */
    $('#reportes-contenedor').on('click','a',function(){
        var referencia = $(this).attr('href');
        if(referencia == '#menu1'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,2);
            }
        }
        if(referencia == '#menu2'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,3);
            }
        }
        if(referencia == '#menu4'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,4);
            }
        }
        if(referencia == '#menu5'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,5);
            }
        }
        if(referencia == '#menu7'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,6);
            }
        }
        if(referencia == '#menu8'){
            if($(referencia).find('iframe').length == 0){
                generariframe(datosPreparados,7);
            }
        }
    });
});
</script>