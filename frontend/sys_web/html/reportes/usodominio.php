<?php 
defined("RUTA_BASE") or die(); 
$idgui = uniqid();
// $urlbase = $this->documento->getUrlBase();
echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
//array to json
$jsonFechas = json_encode($this->fechas);
$jsonTiempos = json_encode($this->tiempos);
$jsonDominio = json_encode($this->dominio);
?>

<style type="text/css">
    .div_menu{
        height: 120px; font-size: 20px; text-align: center;
        padding: 40px;
    }
    .color1{
        background-color: #EB6B56;
    }
    .color2{
        background-color: #2C82C9;
    }
    .color3{
        background-color: #9365B8;
    }
    .color4{
        background-color: #FAC51C;
    }

    .color5{
        background-color: #61BD6D;
    }
    
    .select-ctrl-wrapper:after{
        right:0!important;
        height:30px;
    }
    .table-responsive{
        max-height:200px;
    }
    .circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
    .text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
    .text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
    #tabla_alumnos td{ cursor:pointer; }
    #tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
    #tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
    #tabla_alumnos_length { display: none; }

    .imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<div class="container">
    <!-- <div style="position:relative;  margin:10px 0;">
        <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
    </div> -->
    <div class="row " id="levels" style="padding-top: 1ex; ">
        <div class="col-md-12">
            <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
            <li class="active">
                <?php echo JrTexto::_("Use and Management of the Virtual Platform"); ?>
            </li>
            </ol>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:left;">
            <?php echo JrTexto::_("Report") ?>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12"><h3><?php echo JrTexto::_('Teacher Reports'); ?></h3></div>
                <div class="col-md-12" style="margin-bottom:5px; ">
                    <div class="row" style="margin:0; border-radius:0.4em; background-color:white; padding: 2px; border: 2px solid #337ab7;">
                        <div class="col-md-3" style="">
                            <div style="height:90px; margin-top:10px;">
                                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                            </div>
                            <div class="" style="text-align:center;">
                                <h4><?php echo $this->fullname; ?></h4>
                            </div>
                            <div style="padding-top:20px; text-align:center;">
                                <?php if(isset($this->ultimavez['P'])): ?>
                                    <h4>
                                        <?php echo JrTexto::_('Last Time on the Platform'); ?>
                                        <span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['P']['fecha']; ?></span>
                                    </h4>
                                    <h4>
                                        <?php echo JrTexto::_('Realized Hours'); ?>
                                        <span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['P']['tiempo']; ?></span>
                                    </h4>
                                <?php endif; ?>
                                <?php if(isset($this->ultimavez['TR'])): ?>
                                    <h4>
                                        <?php echo JrTexto::_('Last Time in the SmartBook'); ?>
                                        <span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['TR']['fecha']; ?></span>
                                    </h4>
                                    <h4>
                                        <?php echo JrTexto::_('Realized Hours'); ?>
                                        <span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['TR']['tiempo']; ?></span>
                                    </h4>
                                <?php endif; ?>
                                <?php if(isset($this->ultimavez['A'])): ?>
                                    <h4>
                                        <?php echo JrTexto::_('Last Time in the Exercises'); ?>
                                        <span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['A']['fecha']; ?></span>
                                    </h4>
                                    <h4>
                                        <?php echo JrTexto::_('Realized Hours'); ?>
                                        <span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['A']['tiempo']; ?></span>
                                    </h4>
                                <?php endif; ?>
                                <?php if(isset($this->ultimavez['E'])): ?>
                                    <h4>
                                        <?php echo JrTexto::_('Last Time on Tests'); ?>
                                        <span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['E']['fecha']; ?></span>
                                    </h4>
                                    <h4>
                                        <?php echo JrTexto::_('Realized Hours'); ?>
                                        <span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['E']['tiempo']; ?></span>
                                    </h4>
                                <?php endif; ?>
                                <?php if(isset($this->ultimavez['T'])): ?>
                                    <h4>
                                        <?php echo JrTexto::_('Last Time in Activities'); ?>
                                        <span style="display:block; color:green; font-weight: bold;"><?php echo $this->ultimavez['T']['fecha']; ?></span>
                                    </h4>
                                    <h4>
                                        <?php echo JrTexto::_('Realized Hours'); ?>
                                        <span style="color: green; font-weight: bold;"><?php echo $this->ultimavez['T']['tiempo']; ?></span>
                                    </h4>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-sm-12">
                                <h3><?php echo JrTexto::_('Platform Usage'); ?></h3>
                                <canvas id="chart1" width="1000" height="300" class="chartjs-render-monitor" style="display: block;"></canvas>
                            </div>
                            <div class="col-sm-12">
                                <h3><?php echo JrTexto::_('Management of The Virtual Platform'); ?></h3>
                                <div class="col-sm-12">
                                    <div class="dominio">
                                        <p class="text-custom1"><?php echo JrTexto::_('Command'); ?></p>
                                        <div id="barPlataforma" class="circleProgressBar1" style="font-size:0.8em;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="Listening-color">
                                        <p class="text-custom1">SmartBook</p>
                                        <div id="barSmartBook" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="Reading-color">
                                        <p class="text-custom1"><?php echo JrTexto::_('Activity'); ?></p>
                                        <div id="barHomeWork" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="Writing-color">
                                        <p class="text-custom1"><?php echo JrTexto::_('Sessions'); ?></p>
                                        <div id="barActivity" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="Speaking-color">
                                        <p class="text-custom1"><?php echo JrTexto::_('Test'); ?></p>
                                        <div id="barSmartQuiz" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-12" style=" padding:5px; border-radius:0.5em 0.5em 0 0; margin-top:10px; background-color:#337ab7;">
                                <h4 style="margin: 2px 0; color:white;"><?php echo JrTexto::_('Testing Use and Management of The Virtual Platform');?></h4>
                            </div>
                            <div class="col-sm-4" style="text-align:center;">
                                <h3><?php echo JrTexto::_('Score'); ?></h3>
                                <h1 style="border:3px solid #bbbbbb;font-weight:bold; border-radius:50%; display:inline-block; padding:25px 10px;"><?php echo (!empty($this->examen)) ? $this->examen['nota'] : 00 ?> %</h1>
                            </div>
                            <div class="col-sm-8" style="text-align:left;">
                                <h3 style="text-align:center;"><?php echo JrTexto::_('Improved Skills'); ?></h3>
                                <ol id="lista_habilidad">
                                    <?php 
                                        if(!empty($this->examen)){
                                            foreach($this->examen['habilidades'] as $key => $value){
                                                echo '<li>';
                                                echo '<h4>'.$value.'</h4>';
                                                echo '<div class="progress">';
                                                echo '<div id="habprogres1" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:'.$this->examen['habilidades_puntaje'][$key].'%">';
                                                echo $this->examen['habilidades_puntaje'][$key].'%';
                                                echo '</div>';
                                                echo '</div>';
                                                echo '</li>';
                                            }
                                        }else{
                                            echo "<div class='alert alert-danger'><p>".JrTexto::_('Without Skills')."</p></div>";
                                        }
                                    ?>
                                </ol>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-12"><h3><?php echo JrTexto::_('Students Reports'); ?></h3></div>
                        <div class="col-md-12">
                            <div id="info" style="display:block;">
                                <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                                <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
                            </div>
                          <div id="idcolegio-container" style="display: inline-block;">
                              
                              <h4 style="display:inline-block;"><?php echo JrTexto::_('School'); ?></h4>
                              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                  <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                                      <?php if(!empty($this->miscolegios)){
                                      echo '<option value="0">'.JrTexto::_('Select').'</option>';
                                      foreach ($this->miscolegios  as $c) {
                                          if(empty($cursos)) $cursos=$c["cursos"];
                                          echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                                      }} ?>
                                  </select>
                              </div>
                          </div>
                          <div id="idcurso-container" style="display: inline-block;">
                              <h4 style="display:inline-block;"><?php echo JrTexto::_('Course'); ?></h4>
                              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                  <select name="idcurso" id="idcurso" style="width:150px;" class="select-docente form-control select-ctrl">
                                    <option value="0"><?php JrTexto::_('Select'); ?></option>
                                  </select>
                              </div>
                          </div>
                          <div id="idgrados-container" style="display: inline-block;">
                              <h4 style="display: inline-block;"><?php echo JrTexto::_('Grade'); ?></h4>
                              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                  <select name="grados" id="idgrados" style="width:150px;" class="select-docente form-control select-ctrl">
                                    <option value="0"><?php JrTexto::_('Select'); ?></option>
                                  </select>
                              </div>
                          </div>
                          <div id="idseccion-container" style="display: inline-block;">
                              <h4 style="display: inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
                              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                  <select name="seccion" id="idseccion" style="width:150px;" class="select-docente form-control select-ctrl select-nivel">
                                    <option value="0"><?php JrTexto::_('Select'); ?></option>
                                  </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                                <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_("Students"); ?></h4>       
                                <div class="table-responsive">
                                    <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                                        <thead>
                                            <tr><td><?php echo JrTexto::_('Names'); ?></td></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9" style="border-radius:0.4em;">
                            <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                                <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                                <h4><?php echo JrTexto::_('Select Student'); ?></h4>
                            </div>
                        </div>
                </div>
                <div class="col-md-9" style="text-align:right;">
                    <div style="padding:5px 0;"></div>
                    <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_usodominio" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
                </div>
            </div>
        </div>
        <!--end panel body-->
    </div>
    <!--endpanel-->
</div>
<!--end container-->

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">

var jsonFechas = <?php echo $jsonFechas?>;
var jsonTiempos = <?php echo $jsonTiempos?>;
var jsonDominio = <?php echo $jsonDominio ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}

function drawHabilidad(obj , option = null, value = 0, speed = null){
var _anim = speed === null ? 1 : speed;
var option = option === null ? {colorFg:"#337ab7",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
option.animationSpeed = _anim;
var bar=new RadialProgress(obj,option);
bar.setValue(value);
bar.draw(true);
}

function drawTimeline(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'bar',
        data: data,
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'series',
                    ticks: {
                        source: 'labels'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Hours on the virtual platform (h.min)'
                    }
                }]
            }
        }
    });
}

function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 10,
        "pagingType": "simple"
    });
}
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("select");?></option>>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}
var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};
$('document').ready(function(){
    $('#info').hide();

    //$('#selectGrupo').hide();
    $('#idcurso-container').hide();
    $('#idseccion-container').hide();
    $('#idgrados-container').hide();

    dibujarDatatable();

    var obj = document.getElementById('chart1').getContext('2d');
    var labels = new Array();
    var _data = new Array();

    for(var i in jsonFechas){
        if(jsonFechas[i].fecha){
            labels.push(jsonFechas[i].fecha);
            if(jsonTiempos[jsonFechas[i].fecha]){
                _data.push(jsonTiempos[jsonFechas[i].fecha]);
            }
        }
    }//end for in

    var data = {
        labels: labels,
        datasets: [{
            label: 'Use of the platform',
            data: _data,
            type: 'line',
            borderColor:"rgb(75, 192, 192)",
            pointRadius: 0.1,
            fill: false,
            lineTension: 0,
            borderWidth: 2
        }]
    };

    drawTimeline(obj,data);

    //draw el dominio en la plataforma virtual
    var calcular = function(valor){
        return (valor == 0) ? 0 : (valor / 100);
    };

    // drawHabilidad(document.getElementById("barPlataforma"),null, 0.15,0.15);
    // drawHabilidad(document.getElementById("barSmartBook"),null, 0.50,0.15);
    // drawHabilidad(document.getElementById("barHomeWork"),null, 0.50,0.15);
    // drawHabilidad(document.getElementById("barActivity"),null, 0.50,0.15);
    // drawHabilidad(document.getElementById("barSmartQuiz"),null, 0.50,0.15);

    drawHabilidad(document.getElementById("barPlataforma"),null, calcular(jsonDominio.dominio),0.15);
    drawHabilidad(document.getElementById("barSmartBook"),null, calcular(jsonDominio.smartbook),0.15);
    drawHabilidad(document.getElementById("barHomeWork"),null, calcular(jsonDominio.homework),0.15);
    drawHabilidad(document.getElementById("barActivity"),null, calcular(jsonDominio.activity),0.15);
    drawHabilidad(document.getElementById("barSmartQuiz"),null, calcular(jsonDominio.smarquiz),0.15);

    $('#idcolegio').change(function(){
        //change width
        var texto = nombreselect('#idcolegio',$('#idcolegio').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

            //buscar informacion del colegio...
          $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
  $('#idseccion').change(function(){
      // alert($(this).val());
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var alumnos = $('<tr></tr>');
            var filas = '';
            var tabla_tmp = $('<table></table>');
            tabla_tmp.attr('id','tabla_alumnos');
            tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
            tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names"); ?></td></tr></thead><tbody></tbody>');
            console.log(resp.data.alumnos);
            for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                //data-dni="'+resp.data.alumnos[i].dni+'"
                filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
            }
            tabla_tmp.find('tbody').html(filas);

            $('#tabla_alumnos_wrapper').remove();
            $('#contenedorAlumnos').append(tabla_tmp);
            dibujarDatatable();
        } else {
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
  });

  $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
      if($(this).data('id')){
          var controllerAndFunction = '/reportealumno/usodominio';
          var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
          $('#viewContent').html('');
          $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
          $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
          $('#contenedorAlumnos table').css('pointer-events',"none");
          $('#contenedorAlumnos table').css('opacity',"0.5");
        //   $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
      }
  });

});

</script>
