  <?php 
defined("RUTA_BASE") or die(); 
$idgui = uniqid();
$url = $this->documento->getUrlBase();
// $urlbase = $this->documento->getUrlBase();
echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
$entrada = ($this->entrada) ? 'true' : 'false';
$totalCursos = count($this->cursos);

$json_infoEntrada = json_encode($this->infoEntrada);
$json_infoSalida = json_encode($this->infoSalida);
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }

.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<!--START CONTAINER-->
<div class="container">

<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->



<div class="row " id="levels" style="padding-top: 1ex; ">
  <!-- <a class="btn btn-primary" href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativo?entrada=1">Ver examen de Entrada </a>
  <a class="btn btn-danger" href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativo?entrada=0">Ver examen de Salida </a> -->
  <div style="margin:5px 0;"></div>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
    <div class="col-md-12">
        <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>     
        <li class="active">
            <?php echo JrTexto::_("Beginning and Final Test Comparison"); ?>
        </li>
        </ol>
    </div>
</div>
<?php endif; ?>

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Report");?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >

      <h3 style="font-weight:bold;"><?php echo JrTexto::_("Beginning and Final Test Comparison");?></h3>
      <div class="container">
        <div class="row" style="margin:15px auto;">
          <div class="col-md-4" style="/*height:90px;*/">
            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
            <h2><?php echo $this->fullname ?></h2>
            <!-- <div class="" style="text-align:left;">
              <div class="chart-container" style="position: relative; margin:0 auto; /*height:1%;*/ width:50%;">
                <canvas id="chart-area"  class="chartjs-render-monitor" ></canvas>
              </div>
            </div> -->
          </div>
          <div class="col-md-8">
            <!--start table-->
            <div class="table-responsive">
              <table id="tablaExamenesE" class="table table-bordered table-hover">
                <input type="hidden" id="urlBase" value="<?php echo $url?>" />
                <thead>
                  <th><?php echo JrTexto::_("Course"); ?></th>
                  <th><?php echo ($this->entrada == true) ? JrTexto::_("Beginning Test Score") : JrTexto::_("Final Test Score"); ?></th>
                  <th><?php echo JrTexto::_("Listening"); ?></th>
                  <th><?php echo JrTexto::_("Reading"); ?></th>
                  <th><?php echo JrTexto::_("Writing"); ?></th>
                  <th><?php echo JrTexto::_("Speaking"); ?></th>
                </thead>
                <tbody style="font-weight:bold;">
                  <?php 
                    if(!empty($this->cursos)){
                      $keyExamen = $this->entrada == true ? 'E' : 'S';
                      $first = true;
                      foreach ($this->cursos as $key => $lista_nivel){
                        $nombre=$lista_nivel['nombre'];
                        $idnivel=$lista_nivel['idcurso'];
                        $_nota = ($this->entrada == true) ? intval($this->Total[$idnivel][$keyExamen][0]) : intval($this->Total[$idnivel][$keyExamen][count($this->Total[$idnivel][$keyExamen]) - 1]);
                        $nota =  $_nota > 100 ? ( ( $_nota / ((ceil($_nota / 100) ) * 100) ) * 100 ) *0.20 : $_nota * 0.20;
                        $nota = intval(floor($nota));//round($nota,2);
                        $_nota2 = intval($this->Total[$idnivel]['S'][count($this->Total[$idnivel]['S'])-1]);
                        $nota2 =  $_nota2 > 100 ?  20 : $_nota2 * 0.20;
                        $nota2 = intval(floor($nota2));//round($nota2,2);
                        //Imprimir tabla
                        echo "<tr data-id='".$idnivel."'>";
                        if($first == true){
                          echo "<td rowspan='3'>{$nombre}</td><td>{$nota} pts / 20 pts</td>";
                          $first = false;
                        }else{
                          echo "<td rowspan='3'>{$nombre}</td><td>".JrTexto::_("Beginning Test Score")." <br> {$nota} pts / 20 pts</td>";
                        }
                        echo "<td><div class='countPercentage' data-id='4' data-value='".$this->TotalHab[$idnivel]['4']."'><span>0</span>%</div><input type='hidden' data-id='4' value='".$this->TotalHabContrario[$idnivel]['4']."' /></td>";
                        echo "<td><div class='countPercentage' data-id='5' data-value='".$this->TotalHab[$idnivel]['5']."'><span>0</span>%</div><input type='hidden' data-id='5' value='".$this->TotalHabContrario[$idnivel]['5']."' /></td>";
                        echo "<td><div class='countPercentage' data-id='6' data-value='".$this->TotalHab[$idnivel]['6']."'><span>0</span>%</div><input type='hidden' data-id='6' value='".$this->TotalHabContrario[$idnivel]['6']."' /></td>";
                        echo "<td><div class='countPercentage' data-id='7' data-value='".$this->TotalHab[$idnivel]['7']."'><span>0</span>%</div><input type='hidden' data-id='7' value='".$this->TotalHabContrario[$idnivel]['7']."' /></td>";
                        echo "<tr><td>".JrTexto::_('Final Test Score')."</td><td>Listening</td><td>Reading</td><td>Writing</td><td>Speaking</td></tr>";
                        echo "<tr><td>{$nota2} pts / 20 pts</td><td>{$this->TotalHabContrario[$idnivel]['4']}%</td><td>{$this->TotalHabContrario[$idnivel]['5']}%</td><td>{$this->TotalHabContrario[$idnivel]['6']}%</td><td>{$this->TotalHabContrario[$idnivel]['7']}%</td></tr></tr>";
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!--End table-->
            
          </div>
        </div>
      </div>

    </div>
    <div class="panel panel-warning" style="margin: 8px;">
      <div class="panel-heading" style="text-align: left;">
        <?php echo JrTexto::_("Comparison");?>
      </div>
      <div class="panel-body">
        <p style="font-size:large; display:inline-block; margin:0; padding-right:15px;"><?php echo JrTexto::_('Courses'); ?></p>
        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
            <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height: 34px;">
            <option value=''><?php echo JrTexto::_('Select'); ?></option>
              <?php
                if(!empty($this->cursos)){               
                  foreach ($this->cursos as $key => $lista_nivel){
                    $nombre=$lista_nivel['nombre'];
                    $idnivel=$lista_nivel['idcurso'];
                    echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                  }
                }
              ?>
            </select>
        </div>
        <div class="row" style="padding-top:10px;">
          <div class="col-md-12">
            <div class="chart-container" id="comparativo-container" style="position: relative; margin:0 auto; height:100%; width:100%">
              <canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>
            </div>
          </div>
          <?php if($this->entrada == true): ?>
          <?php else : ?>
          <?php endif; ?>
          <div class="col-md-6 chart-container" style="position: relative; margin:0 auto; ">
            <div style="border:1px solid black; border-radius:1em; text-align:center;">
                <?php echo ($this->entrada == true) ? "<h4>".JrTexto::_('Beginning Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_entrada'></span></p>" : "<h4>".JrTexto::_('Final Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_salida'></span>"; ?>
                <?php echo ($this->entrada == true) ? '<div id="radar1-content"><canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas></div>' : '<div id="radar2-content"><canvas id="radar2" style="margin-bottom:5px;"></canvas></div>'; ?>
            </div>
          </div>
          <div class="col-md-6 chart-container" style="position: relative; margin:0 auto; ">
            <div style="border:1px solid black; border-radius:1em; text-align:center;">
                <?php echo ($this->entrada == false) ? "<h4>".JrTexto::_('Beginning Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_entrada'></span></p>" : "<h4>".JrTexto::_('Final Test Score')."</h4><p>".JrTexto::_('Exam Taken on').": <span id='fecha_salida'></span>"; ?>
                <?php echo ($this->entrada == false) ? '<div id="radar1-content"><canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas></div>' : '<div id="radar2-content"><canvas id="radar2" style="margin-bottom:5px;"></canvas></div>'; ?>
                <!-- <h4>Nota de Salida</h4>
                <canvas id="radar2" style="margin-bottom:5px;"></canvas> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--end panel-->
</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3><?php echo JrTexto::_('Students Reports'); ?></h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
        <div id="info" style="display:block;">
            <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
            <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
        </div>
        <div id="idcolegio-container" style="display: inline-block;">
            
            <h4 style="display:inline-block;"><?php echo JrTexto::_('School'); ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select id="idcolegio" style="width:150px;" class="select-docente form-control select-ctrl ">
                    <?php if(!empty($this->miscolegios)){
                    echo '<option value="0">'.JrTexto::_('Select').'</option>';
                    foreach ($this->miscolegios  as $c) {
                        if(empty($cursos)) $cursos=$c["cursos"];
                        echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                    }} ?>
                </select>
            </div>
        </div>
        <div id="idcurso-container" style="display: inline-block;">
            <h4 style="display:inline-block;"><?php echo JrTexto::_('Course'); ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select name="idcurso" id="idcurso" style="width: 150px;" class="select-docente form-control select-ctrl">
                    <?php 
                    if(!empty($cursos)){
                    echo '<option value="0">Seleccionar</option>';

                    foreach ($cursos  as $c){
                        if(empty($grados)) $grados=$c["grados"];                           
                        echo '<option value="'.$c["idcurso"].'">'.$c["strcurso"].'</option>';
                    }} ?>
                </select>
            </div>
        </div>
        <div id="idgrados-container" style="display: inline-block;">
            <h4 style="display: inline-block;"><?php echo JrTexto::_('Grades'); ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select name="grados" id="idgrados" style="width: 150px;" class="select-docente form-control select-ctrl">
                    <?php if(!empty($grados)){
                    echo '<option value="0">Seleccionar</option>';
                    foreach ($grados as $c){
                        if(empty($seccion)) $seccion=$c["secciones"];
                        echo '<option value="'.$c["idgrado"].'">'.$c["grado"].'</option>';
                    }} ?>
                </select>
            </div>
        </div>
        <div id="idseccion-container" style="display: inline-block;">
            <h4 style="display: inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                <select name="seccion" id="idseccion" style="width: 150px;" class="select-docente form-control select-ctrl select-nivel">
                    <?php if(!empty($seccion)){
                    echo '<option value="0">Seleccionar</option>';
                    foreach ($seccion  as  $c) {
                        echo '<option value="'.$c["idgrupoauladetalle"].'">'.$c["seccion"].'</option>';
                    }} ?>
                </select>
            </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
          <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_('Students'); ?></h4>       
          <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
              <thead>
                  <tr><td><?php echo JrTexto::_('Names'); ?></td></tr>
              </thead>
              <!--<tbody>
                  <tr>
                      <td>Pepe</td>
                  </tr>
              </tbody>-->
          </table>
        </div>
      </div>
      <div class="col-md-9" style="border-radius:0.4em;">
          <!-- <a class="btn btn-primary disabled" id="verEntrada" href="javascript:void(0)">Ver examen de Entrada del Alumno </a>
          <a class="btn btn-danger disabled" id="verSalida" href="javascript:void(0)">Ver examen de Salida del Alumno </a> -->
          <div style="margin:5px 0;"></div>
          <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
              <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
              <h4><?php echo JrTexto::_('Select Student'); ?></h4>
          </div>
      </div>
      <div class="col-md-9" style="text-align:right;">
        <div style="padding:5px 0;"></div>
        <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_entradasalida" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
      </div>
    </div>
  </div>
</div>
<!--END CONTAINER-->

<script type="text/javascript">
var entrada = <?php echo $entrada ?>;
var infoEntrada = <?php echo !empty($json_infoEntrada) ? $json_infoEntrada : 'null' ?>;
var infoSalida = <?php echo !empty($json_infoSalida) ?  $json_infoSalida : 'null' ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function countText(obj,valueText, ToValue){
  var currentValue = parseInt(valueText);
  var nextVal = ToValue;
  var diff = nextVal - currentValue;
  var step = ( 0 < diff ? 1 : -1 );
  for (var i = 0; i < Math.abs(diff); ++i) {
      setTimeout(function() {
          currentValue += step
          obj.text(currentValue);
      }, 100 * i)   
  }
}
function calcular(val, isline ){
  var resultado = 0;
  if(val > 0){
    // console.log(resultado);
    if(isline === true){
      resultado = val * ((Math.floor((Math.random()*5)+1) * 0.1) + 1)
    }
  }
  return resultado.toFixed(2);
};
function drawChart(obj,_data,color,isEntrada,_title){
  var ctx = document.getElementById(obj).getContext('2d');
  // var _data2 = new Array();
  // for(var i in _data){
  //   _data2.push(calcular(_data[i],true));
  // }

  isEntrada = isEntrada == false ? "Salida" : "Entrada";
  var chartData = {
    labels: ["Listening", "Reading", "Writing", "Speaking"],
    datasets: [
    //   {
    //   type: 'line',
    //   borderColor: color + "7a",
    //   borderWidth: 2,
    //   fill: false,
    //   data: _data2
    // }, 
    {
      type: 'bar',
      labels: ["Listening", "Reading", "Writing", "Speaking"],
      backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)'],
      data: _data,
      borderColor: 'white',
      borderWidth: 2
    }]

  };
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: chartData,
    options: {
      responsive: true,
      title: {
        display: true,
        text: _title
      },
      tooltips: {
        mode: 'point',
        intersect: true
      },
      // legend: {
			// 		position: 'top',
			// 	}
      legend:false
    }
  });
}
function drawChart2(obj,dat,texto ){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: false,
      title: {
        display: true,
        text: texto
      },
      tooltips: {
        custom: function(tooltipModel) {
          if(tooltipModel.body){
            // console.log(tooltipModel.body[0].lines[0]);
            var strline = tooltipModel.body[0].lines[0];
            var search = strline.search('hide');
            if(search != -1){
              tooltipModel.width = 125;
              tooltipModel.body[0].lines[0] = strline.replace('hide','Difference');
            }
          }
        }
        // mode: 'index',
        // intersect: true,
        // filter: function (tooltipItem, data) {
        //     var label = data.labels[tooltipItem.index];
            
        //     // console.log(tooltipItem, data, label);
        //     console.log(label);
        //     // if (label != "hide") {
        //       return true;
        //     // } 
        // }  
      },
      scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true
            }]
        },
      legend: {
        labels: {
          filter: function(item, chart) {
            // Logic to remove a particular legend item goes here
            return !(item.text.indexOf('hide') !== -1);
          }
        },
        onHover: function(event, legendItem) {
          document.getElementById(obj).style.cursor = 'pointer';
        },
        onClick: function(e,legendItem){
          var index = legendItem.datasetIndex;
          var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          var meta = ci.getDatasetMeta(index);
          // alert(index);
          if(index == 2){
            var meta2 = ci.getDatasetMeta(3);
            meta2.hidden = meta2.hidden === null? !ci.data.datasets[3].hidden : null;
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;

          }else{
            meta.hidden = meta.hidden === null? !ci.data.datasets[index].hidden : null;
          }
          // ci.data.datasets.forEach(function(e, i) {
          //   var meta = ci.getDatasetMeta(i);

          //   if (i !== index) {
          //     if (!alreadyHidden) {
          //       meta.hidden = meta.hidden === null ? !meta.hidden : null;
          //     } else if (meta.hidden === null) {
          //       meta.hidden = true;
          //     }
          //   } else if (i === index) {
          //     meta.hidden = null;
          //   }
          // });

          ci.update();
          // var index = legendItem.datasetIndex;
          // var ci = this.chart;
          // var alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;
          
          

          // ci.update();
        }
      }//end legend
    }
  });
}

function drawArea(obj, _data){
  new Chart(obj, {
      type: 'polarArea',
      data: {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
          label: "Habilidades obtenidas por el Examen",
          data: _data,
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          label: 'My dataset' // for legend
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Statistics of the skills obtained by the test'
        },
        responsive: true,
				legend: {
					position: 'right',
				},
				scale: {
					ticks: {
						beginAtZero: true
					},
					reverse: false
				},
				animation: {
					animateRotate: false,
					animateScale: true
				}
      }
  });
}

function drawRadar(obj, _data, color,isEntrada,_title){
  isEntrada = isEntrada == false ? "Salida" : "Entrada";
  new Chart(obj, {
      type: 'radar',
      data: {
        labels: ["Listening", "Reading", "Writing", "Speaking"],
        datasets: [{
          label: "Habilidades obtenidas por el Examen",
          data: _data,
          backgroundColor: color + "7a",
          pointBackgroundColor: color,
          borderColor: color,
          label: isEntrada // for legend
        }]
      },
      options: {
        title: {
          display: true,
          text: _title
        },
        responsive: true,
				legend: {
					position: 'top',
				},
				scale: {
					ticks: {
						beginAtZero: true
					}
				},
				animation: {
					animateRotate: false,
					animateScale: true
				}
      }
  });
}

function pintarComparativa(id){
  var _info1 = (infoEntrada[id] != null) ? infoEntrada[id] : infoEntrada[Object.keys(infoEntrada)[0]];
  var _info2 = (infoSalida[id] != null) ? infoSalida[id] : infoSalida[Object.keys(infoSalida)[0]];

  $('#fecha_entrada').text(_info1);
  $('#fecha_salida').text(_info2);
  var datosRadar = new Array();
  datosRadar[0] = 0;
  datosRadar[1] = 0;
  datosRadar[2] = 0;
  datosRadar[3] = 0;

  var datosRadar2 = new Array();
  datosRadar2[0] = 0;
  datosRadar2[1] = 0;
  datosRadar2[2] = 0;
  datosRadar2[3] = 0;

  //Sumar habilidades en la comparativa
  $('#tablaExamenesE').find('tr').each(function(key,value){
    var comparativa = id == null ? (key == 1) : ($(this).data('id') == id);
    if(comparativa == true){
      $.each($(this).find('.countPercentage'),function(k,v){
        var idHab = parseInt($(this).data('id'));
        switch(idHab){
          case 4: datosRadar[0] += parseFloat($(this).data('value'));
          break;
          case 5: datosRadar[1] += parseFloat($(this).data('value'));
          break;
          case 6: datosRadar[2] += parseFloat($(this).data('value'));
          break;
          case 7: datosRadar[3] += parseFloat($(this).data('value'));
          break;
        }
      });
    }
  });
  //Sumar habilidades contraria en la comparativa
  $('#tablaExamenesE').find('tr').eq(0).find('td input').each(function(key, value){
    var idHab = parseInt($(this).data('id')); 
    switch(idHab){
      case 4: datosRadar2[0] += parseFloat($(this).val());
      break;
      case 5: datosRadar2[1] += parseFloat($(this).val());
      break;
      case 6: datosRadar2[2] += parseFloat($(this).val());
      break;
      case 7: datosRadar2[3] += parseFloat($(this).val());
      break;
    }
  });
  $('#tablaExamenesE').find('tr').each(function(key,value){
    var comparativa = id == null ? (key == 1) : ($(this).data('id') == id);

    if(comparativa == true){

      $.each($(this).find('input[type=hidden]'),function(k,v){
        var idHab = parseInt($(this).data('id'));
        switch(idHab){
          case 4: datosRadar2[0] += parseFloat($(this).val());
          break;
          case 5: datosRadar2[1] += parseFloat($(this).val());
          break;
          case 6: datosRadar2[2] += parseFloat($(this).val());
          break;
          case 7: datosRadar2[3] += parseFloat($(this).val());
          break;
        }
      });
    }
  });

  //Dibujar charts Radar

  if(entrada == true){
    // drawRadar(document.getElementById("radar1"),datosRadar, "#3e95cd",true,"Comparison of exams");
    $('#radar1-content').html("").html('<canvas id="radar1"  width="506" height="253" style="margin-bottom:5px;"></canvas>'); //No es la mejor opcion investigar update de la libreria
    $('#radar2-content').html("").html('<canvas id="radar2"  width="506" height="253" style="margin-bottom:5px;"></canvas>'); //No es la mejor opcion investigar update de la libreria
    drawChart("radar1",datosRadar,"#3e95cd",true,"Comparison of exams");
    drawChart("radar2",datosRadar2,"#d85050",false,"Comparison of exams");
    // drawRadar(document.getElementById("radar2"),datosRadar2,"#d85050",false,"Comparison of exams");
  }else{
    // drawRadar(document.getElementById("radar1"),datosRadar2);
    // drawRadar(document.getElementById("radar2"),datosRadar,"#d85050",false,"Comparison of exams");
    drawChart("radar1",datosRadar,"#3e95cd",true,"Comparison of exams");
    drawChart("radar2",datosRadar2,"#d85050",false,"Comparison of exams");
  }
  var datosRadar3 = new Array();
  datosRadar3[0] = 0;
  datosRadar3[1] = 0;
  datosRadar3[2] = 0;
  datosRadar3[3] = 0;
  var datosRadar4 = new Array();
  datosRadar4[0] = 0;
  datosRadar4[1] = 0;
  datosRadar4[2] = 0;
  datosRadar4[3] = 0;
  //datosRadar = entrada . datosRadar2 = salida
  for(var i = 0; i < 4; i++){
    if(datosRadar[i] > datosRadar2[i]){
      datosRadar4[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
    }else if(datosRadar[i] < datosRadar2[i]){
      datosRadar3[i] = Math.abs(datosRadar2[i] - datosRadar[i]);
    }
  }
  
  var stackdifference = 'Stack 1';
  var barChartData = {
    labels: ["Listening", "Reading", "Writing", "Speaking"],
    datasets: [{
        label: 'Beginning Test',
        backgroundColor: '#36a2eb',
        stack:'Stack 0',
        data: [
          datosRadar[0],datosRadar[1] ,datosRadar[2] ,datosRadar[3]
        ]
    }, {
        label: 'Final Test',
        backgroundColor: '#ff6384',
        stack:'Stack 1',
        data: [
          datosRadar2[0],datosRadar2[1] ,datosRadar2[2] ,datosRadar2[3]
        ]
    },{
        label: 'Difference',
        backgroundColor: '#d4b02f',
        stack: 'Stack 0',
        data: [
          Math.abs(datosRadar3[0]), Math.abs(datosRadar3[1]), Math.abs(datosRadar3[2]) , Math.abs(datosRadar3[3])
        ]
    },{
        label: 'hide',
        backgroundColor: '#d4b02f',
        stack: 'Stack 1',
        
        data: [
            Math.abs(datosRadar4[0]), Math.abs(datosRadar4[1]), Math.abs(datosRadar4[2]) , Math.abs(datosRadar4[3])
        ]
    }]

  };
  $('#comparativo-container').html("").html('<canvas id="comparativo" style="min-height:250px; margin: 0 auto"></canvas>'); //No es la mejor opcion investigar update de la
  drawChart2("comparativo",barChartData,'Comparison of beginning and final tests');
}
function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 10,
        "pagingType": "simple"
    });
}

function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}

var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}

var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};

$('document').ready(function(){
  $('#info').hide();

  var btnviewentrada = true;
  var _viewContent = false;
  var idAlumnoSeleccionado = null;

  $('#idcurso-container').hide();
  $('#idseccion-container').hide();
  $('#idgrados-container').hide();

  dibujarDatatable();
  //Dibujar contador de porcentaje de habilidades
  $('.countPercentage').each(function(key, value){
      countText($(this).find('span'), $(this).find('span').text(),parseInt($(this).data('value')) );
  });
  var datos = new Array();
  datos[0] = 0;
  datos[1] = 0;
  datos[2] = 0;
  datos[3] = 0;
  
  
  $('.countPercentage').each(function(key, value){
    var idHab = parseInt($(this).data('id')); 
    switch(idHab){
      case 4: datos[0] += parseFloat($(this).data('value'));
      break;
      case 5: datos[1] += parseFloat($(this).data('value'));
      break;
      case 6: datos[2] += parseFloat($(this).data('value'));
      break;
      case 7: datos[3] += parseFloat($(this).data('value'));
      break;
    }
  });
  //limitar al 100%
  var totalCursos = <?php echo $totalCursos ?>;
  if(totalCursos > 1){
    for(var i = 0; i < datos.length; i++){
      datos[i] = (datos[i] / (100 * totalCursos)) * 100;
    }
  }
  //Dibujar charts Area
  // drawArea(document.getElementById("chart-area"),datos);

  //Dibujar comparativa
  pintarComparativa(null);
  
  

  //Evento al cambiar el select del filtro entrada/salida
  $('#select_viewGeneral').change(function(){
    //Dibujar comparativa
    pintarComparativa($(this).val());
  });

  // $('#idcolegio').change(function(){
  //       if($('#idcolegio').val() != 0){
  //           $('#idcurso-container').show();
  //       }
  //     });
  //     $('#idcurso').change(function(){
  //       if($('#idcurso').val() != 0){
  //           $('#idgrados-container').show();
  //       }
  //     });
  //     $('#idgrados').change(function(){
  //       if($('#idgrados').val() != 0){
  //           $('#idseccion-container').show();
  //       }
  //     });

$('#idcolegio').change(function(){
  //change width
  var texto = nombreselect('#idcolegio',$('#idcolegio').val());
  // console.log(texto);
  $(this).css('width',(texto.length + 200)+'px');

  if($('#idcolegio').val() != 0){
      $('#idcurso-container').show();
      var idcolegio=$(this).val()||'';
      for (var i = 0; i < datoscurso.length; i++) {
        if(datoscurso[i].idlocal==idcolegio){              
              datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
            actualizarcbo($(this),datoscurso[i].cursos);
        }
      }

  //buscar informacion del colegio...
  $.ajax({
      url: _sysUrlBase_+'/local/infolocal',
      type: 'POST',
      dataType: 'json',
      data: {'idlocal': $('#idcolegio').val()},
  }).done(function(resp) {
      if(resp.code=='ok'){
          if(Object.keys(resp.data).length > 0){
              var dre = null;
              if(resp.data[0].dre == null || resp.data[0].dre == 0){
                  dre = resp.data[0].departamento;
              }else{
                  dre = resp.data[0].dre;
              }
              $('#info').find('#infoDre').find('span').text(dre);
              $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
          }
      } else {
          return false;
      }
  })
  .fail(function(xhr, textStatus, errorThrown) {
      return false;
  });
  $('#info').show();
}else{
  $('#info').hide();
}
});
      $('#idcurso').change(function(){
        //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });

  $('#idseccion').change(function(){
    //change width
    var texto = nombreselect('#idseccion',$('#idseccion').val());
    // console.log(texto);
    $(this).css('width',(texto.length + 200)+'px');
      // alert($(this).val());
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var alumnos = $('<tr></tr>');
            var filas = '';
            var tabla_tmp = $('<table></table>');
            tabla_tmp.attr('id','tabla_alumnos');
            tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
            tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names"); ?></td></tr></thead><tbody></tbody>');
            console.log(resp.data.alumnos);
            for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                //data-dni="'+resp.data.alumnos[i].dni+'"
                filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
            }
            tabla_tmp.find('tbody').html(filas);
            if($('#verEntrada').hasClass("disabled")){
              $('#verEntrada').removeClass('disabled');
            }
            if($('#verSalida').hasClass("disabled")){
              $('#verSalida').removeClass('disabled');
            }

            $('#tabla_alumnos_wrapper').remove();
            $('#contenedorAlumnos').append(tabla_tmp);
            dibujarDatatable();
        } else {
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
  });

  var loadPreview = function(obj,idealum){
    if(obj != null){
      idAlumnoSeleccionado = idealum != null ? idAlumnoSeleccionado : obj.data('id') ;
    }
    //console.log(idAlumnoSeleccionado);
    if(idAlumnoSeleccionado != null){
        // idAlumnoSeleccionado = $(this).data('id');
        _viewContent =(_viewContent === false) ? true : _viewContent;
        var controllerAndFunction = (btnviewentrada == true) ? '/reportealumno/entrada' : '/reportealumno/salida';
        var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+idAlumnoSeleccionado+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
        $('#viewContent').html('');
        $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
        $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
        $('#contenedorAlumnos table').css('pointer-events',"none");
        $('#contenedorAlumnos table').css('opacity',"0.5");
        // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
    }
  }

  $('#verEntrada').on('click',function(){
    btnviewentrada = true;
    if (_viewContent === true){
      loadPreview(null,null);
    }
  });
  $('#verSalida').on('click',function(){
    btnviewentrada = false;
    if (_viewContent === true){
      loadPreview(null,null);
    }
  });
  $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
    loadPreview($(this),null);
  });
});
</script>