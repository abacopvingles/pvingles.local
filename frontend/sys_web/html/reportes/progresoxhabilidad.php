<?php

defined('RUTA_BASE') or die();
$idgui = uniqid();

$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$url = $this->documento->getUrlBase();

//variables to json
$JSON_habilidad_bimestre_all = json_encode($this->habilidad_bimestre_all);
$JSON_habilidad_trimestre_all = json_encode($this->habilidad_trimestre_all);
$JSON_habilidad_total = json_encode($this->habilidad_total);

echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: white; font-size: large; }

/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {display:none;}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #337ab7;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #b73333;
}

input:focus + .slider {
  box-shadow: 0 0 1px #b73333;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.text-custom1{ font-weight: bold; color: black; font-size: large; }

.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }


.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<?php if(!$this->isIframe): ?>
<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->
<?php endif; ?>

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
      <li class="active">
        <?php echo JrTexto::_("Progress by Skills"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START CONTAINER-->
<div class="">
	<!--START PANEL-->
    <div class = 'panel panel-primary' >
    	<div class="panel-heading" style="text-align:left;">
    		<?php echo JrTexto::_("Report");?>
    	</div>
    	<div class = 'panel-body' style="text-align: center;  " >
            <h3 style="font-weight:bold;"><?php echo JrTexto::_("Teacher Progress"); ?></h3>
            <div class="row" style="margin:15px auto;">
                <div class="col-md-4" style="text-align:center;">
                    <div class="" style="/*height:90px;*/">
                        <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                        <h2><?php echo $this->fullname ?></h2>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="col-sm-12">
                        <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Courses"); ?></p>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                            <?php
                                if(!empty($this->cursos)){               
                                foreach ($this->cursos as $key => $lista_nivel){
                                    $nombre=$lista_nivel['nombre'];
                                    $idnivel=$lista_nivel['idcurso'];
                                    echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                                }
                                }else{
                                echo '<option value="0">Sin cursos registrado</option>';
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        
                        <!--start chart habilidad-->
                        <div class="chart-container" style="position: relative; margin:0 auto; ; width:40%">
                            <canvas id="pieChart"  width="348" height="374"></canvas>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-12" >
                    <div class="col-sm-3" style="text-align:center;">
                        <h4>Listening</h4>
                        <div id="barListening" class="circleProgressBar1" style="font-size:1em;"></div>
                    </div>
                    <div class="col-sm-3" style="text-align:center;">
                        <h4>Reading</h4>
                        <div id="barReading" class="circleProgressBar1" style="font-size:1em;"></div>
                    </div>
                    <div class="col-sm-3" style="text-align:center;">
                        <h4>Writing</h4>
                        <div id="barWriting" class="circleProgressBar1" style="font-size:1em;"></div>
                    </div>
                    <div class="col-sm-3" style="text-align:center;">
                        <h4>Speaking</h4>
                        <div id="barSpeaking" class="circleProgressBar1" style="font-size:1em;"></div>
                    </div>
                    
                </div> -->
                <div class="col-sm-12 col-md-12">
                    <?php if(!empty($this->cursos)): ?>
                        <div class = 'panel panel-danger' >
                            <div class="panel-heading" style="text-align:left;">
                                <?php echo JrTexto::_("Compare");?>
                            </div>
                            <div class = 'panel-body' style="text-align: center;  " >
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><?php echo JrTexto::_("Bimester"); ?></label>
                                        <!-- Rounded switch -->
                                        <label id="switch_tipo" class="switch">
                                            <input type="checkbox" class="switchera">
                                            <span class="slider round"></span>
                                        </label>
                                        <label><?php echo JrTexto::_("Quarterely"); ?></label>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="biselect">
                                            <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Bimester"); ?></p>
                                            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                                <select name="select_viewBimestre" id="select_viewBimestre" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                                                    <option value="1"><?php echo JrTexto::_("Bimester"); ?> 1</option>
                                                    <option value="2"><?php echo JrTexto::_("Bimester"); ?> 2</option>
                                                    <option value="3"><?php echo JrTexto::_("Bimester"); ?> 3</option>
                                                    <option value="4"><?php echo JrTexto::_("Bimester"); ?> 4</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div id="triselect" style="display:none;">
                                            <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Bimester"); ?></p>
                                            <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                                                <select name="select_viewTrimestre" id="select_viewTrimestre" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                                                    <option value="1"><?php echo JrTexto::_("Quarterely"); ?> 1</option>
                                                    <option value="2"><?php echo JrTexto::_("Quarterely"); ?> 2</option>
                                                    <option value="3"><?php echo JrTexto::_("Quarterely"); ?> 3</option>
                                                </select>
                                            </div>
                                        </div>
                                        

                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-sm-6" style="border-right: 1px solid gray;">
                                            <div class="col-sm-12">
                                                <div class="chart-container" style="position: relative; margin:0 auto; width:100%">
                                                    <canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Listening</h4>
                                                <div id="barListeningC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Reading</h4>
                                                <div id="barReadingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Writing</h4>
                                                <div id="barWritingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Speaking</h4>
                                                <div id="barSpeakingC1" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                        </div>
                                        <!--radar2-->
                                        <div class="col-sm-6" style="border-left: 1px solid gray;">
                                            <div class="col-sm-12">
                                                <div class="chart-container" style="position: relative; margin:0 auto;  width:100%">
                                                    <canvas id="radar2" width="506" height="253" style="margin-bottom:5px;"></canvas>
                                                </div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Listening</h4>
                                                <div id="barListeningC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Reading</h4>
                                                <div id="barReadingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Writing</h4>
                                                <div id="barWritingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                            <div class="col-sm-3" style="text-align:center;">
                                                <h4>Speaking</h4>
                                                <div id="barSpeakingC2" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End row-->
                            </div>
                        </div>
                    <?php endif;?>
                        <!--END panel-->
                </diV
            </div>
            <!--end row-->
        </div>
    </div>
    <!--END panel-->
    

</div>
<!--END CONTAINER-->
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3><?php echo JrTexto::_("Students Reports"); ?></h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div id="info" style="display:block;">
                <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
            </div>
            <div id="idcolegio-container" style="display: inline-block;">
                
                <h4 style="display:inline-block;"><?php echo JrTexto::_("School"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                        <?php if(!empty($this->miscolegios)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';
                        foreach ($this->miscolegios  as $c) {
                            if(empty($cursos)) $cursos=$c["cursos"];
                            echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
            <div id="idcurso-container" style="display: inline-block;">
                <h4 style="display:inline-block;"><?php echo JrTexto::_("Course"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="idcurso" id="idcurso" style="width:150px;" class="select-docente form-control select-ctrl">
                        <?php 
                        if(!empty($cursos)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';

                        foreach ($cursos  as $c){
                            if(empty($grados)) $grados=$c["grados"];                           
                            echo '<option value="'.$c["idcurso"].'">'.$c["strcurso"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
            <div id="idgrados-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Grade"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="grados" id="idgrados" style="width:150px;" class="select-docente form-control select-ctrl">
                        <?php if(!empty($grados)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';
                        foreach ($grados as $c){
                            if(empty($seccion)) $seccion=$c["secciones"];
                            echo '<option value="'.$c["idgrado"].'">'.$c["grado"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
            <div id="idseccion-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Section"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="seccion" id="idseccion" style="width:150px;" class="select-docente form-control select-ctrl select-nivel">
                        <?php if(!empty($seccion)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';
                        foreach ($seccion  as  $c) {
                            echo '<option value="'.$c["idgrupoauladetalle"].'">'.$c["seccion"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
              <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_("Students"); ?></h4>       
              <div class="table-responsive">
                <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                    <thead>
                        <tr><td><?php echo JrTexto::_("Names"); ?></td></tr>
                    </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-9" style="border-radius:0.4em;">
              <div style="margin:5px 0;"></div>
              <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                  <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                  <h4><?php echo JrTexto::_("Select Student"); ?></h4>
              </div>
          </div>
            <div class="col-md-9" style="text-align:right;">
                <div style="padding:5px 0;"></div>
                <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_habilidades" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_("View Results by Section"); ?></a>
            </div>
          
        </div>
      </div>
    </div>
    <!--end panel-->

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">

var habilidad_bimestre_all = <?php echo $JSON_habilidad_bimestre_all ?>;
var habilidad_trimestre_all = <?php echo $JSON_habilidad_trimestre_all ?>;
var habilidad_total = <?php echo $JSON_habilidad_total ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;
function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}

function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 25,
        "pagingType": "simple"
    });
}

function drawPie(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: {
            // responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Skills'
            }
        }
    });
}

function drawHabilidad(obj , option = null, value = 0, speed = null){
var _anim = speed === null ? 1 : speed;
var option = option === null ? {colorFg:"#FFFFFF",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
option.animationSpeed = _anim;
var bar=new RadialProgress(obj,option);
bar.setValue(value);
bar.draw(true);
}

function drawRadar(obj, _data, color = "#3e95cd" ,isGeneral = true,bimestre = "Bimester",_title = "Comparison of skills"){
    isGeneral = isGeneral == false ? bimestre+" "+$("#select_viewBimestre").val() : "Current";
    // var chartData = {
    //     labels: ["Listening", "Reading", "Writing", "Speaking"],
    //     datasets: [{
    //     type: 'line',
    //     borderColor: color + "7a",
    //     borderWidth: 2,
    //     fill: false,
    //     data: _data2
    //     }, {
    //     type: 'bar',
    //     labels: ["Listening", "Reading", "Writing", "Speaking"],
    //     backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)','rgba(55,30,131,0.5)'],
    //     data: _data,
    //     borderColor: 'white',
    //     borderWidth: 2
    //     }]

    // };
    // var myMixedChart = new Chart(ctx, {
    //     type: 'bar',
    //     data: chartData,
    //     options: {
    //     responsive: true,
    //     title: {
    //         display: true,
    //         text: _title
    //     },
    //     tooltips: {
    //         mode: 'point',
    //         intersect: true
    //     },
    //     // legend: {
    //             // 		position: 'top',
    //             // 	}
    //     legend:false
    //     }
    // });
    new Chart(obj, {
        type: 'radar',
        data: {
            labels: ["Listening", "Reading", "Writing", "Speaking"],
            datasets: [{
            label: "Habilidades obtenidas mediante el progreso",
            data: _data,
            backgroundColor: color + "7a",
            pointBackgroundColor: color,
            borderColor: color,
            label: isGeneral // for legend
            }]
        },
        options: {
            title: {
            display: true,
            text: _title
            },
            responsive: true,
                    legend: {
                        position: 'top',
                    },
                    scale: {
                        ticks: {
                            beginAtZero: true
                        }
                    },
                    animation: {
                        animateRotate: false,
                        animateScale: true
                    }
        }
    });
}
function drawcomparativa(idcurso,s = false){
    var idbimestre = s == false ? $("#select_viewBimestre").val() : $("#select_viewTrimestre").val();
    var obj = s == false ? habilidad_bimestre_all : habilidad_trimestre_all;
    //console.log(habilidad_bimestre_all[idcurso][idbimestre]);
    var datosRadar = new Array();
    datosRadar[0] = obj[idcurso][idbimestre][4];
    datosRadar[1] = obj[idcurso][idbimestre][5];
    datosRadar[2] = obj[idcurso][idbimestre][6];
    datosRadar[3] = obj[idcurso][idbimestre][7];
    $('#radar1').parent().html("").html('<canvas id="radar1" width="506" height="253" style="margin-bottom:5px;"></canvas');
    drawRadar(document.getElementById("radar1"),datosRadar,"#d85050",false);

    var option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#ffbb04" };
    drawHabilidad(document.getElementById("barListeningC1"),option, (obj[idcurso][idbimestre][4] / 100),0.2);
    option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#c42d1b" };
    drawHabilidad(document.getElementById("barReadingC1"),option, (obj[idcurso][idbimestre][5] / 100),0.2);
    option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#d9b679" };
    drawHabilidad(document.getElementById("barWritingC1"),option, (obj[idcurso][idbimestre][6] / 100),0.2);
    option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#2196f3" };
    drawHabilidad(document.getElementById("barSpeakingC1"),option, (obj[idcurso][idbimestre][7] / 100),0.2); 
}
function drawtotal1(idcurso){
//   var option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#ffbb04" };
//   drawHabilidad(document.getElementById("barListening"),option, (habilidad_total[idcurso][4] / 100),0.1);
//   option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#c42d1b" };
//   drawHabilidad(document.getElementById("barReading"),option, (habilidad_total[idcurso][5] / 100),0.1);
//   option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#d9b679" };
//   drawHabilidad(document.getElementById("barWriting"),option, (habilidad_total[idcurso][6] / 100),0.1);
//   option = {colorText:"#000000",thick:15,fixedTextSize:0.25,colorFg:"#2196f3" };
//   drawHabilidad(document.getElementById("barSpeaking"),option, (habilidad_total[idcurso][7] / 100),0.1);
  var datapie = new Array();
  for(var i in habilidad_total[idcurso]){
      if(habilidad_total[idcurso][i] > 0){
        datapie.push(habilidad_total[idcurso][i]);
      }
  }
  
  var _data = {
      datasets: [{
          data: datapie,
          backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
      }],

      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
          'Listening',
          'Reading',
          'Writing',
          'Speaking'
      ]
  };
  $('#pieChart').parent().html("").html('<canvas id="pieChart" width="348" height="374"></canvas');
  drawPie(document.getElementById('pieChart'),_data);
  //draw radar general
  $('#radar2').parent().html("").html('<canvas id="radar2" width="506" height="253" style="margin-bottom:5px;"></canvas');
  drawRadar(document.getElementById("radar2"),datapie);

  var option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#ffbb04" };
  drawHabilidad(document.getElementById("barListeningC2"),option, (Math.floor(habilidad_total[idcurso][4]) / 100),0.2);
  option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#c42d1b" };
  drawHabilidad(document.getElementById("barReadingC2"),option, (Math.floor(habilidad_total[idcurso][5]) / 100),0.2);
  option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#d9b679" };
  drawHabilidad(document.getElementById("barWritingC2"),option, (Math.floor(habilidad_total[idcurso][6]) / 100),0.2);
  option = {colorText:"#000000",thick:10,fixedTextSize:0.25,colorFg:"#2196f3" };
  drawHabilidad(document.getElementById("barSpeakingC2"),option, (Math.floor(habilidad_total[idcurso][7]) / 100),0.2);
}
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
  }
  var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};
$('document').ready(function(){
    $('#info').hide();

    $('#idcurso-container').hide();
    $('#idseccion-container').hide();
    $('#idgrados-container').hide();

    dibujarDatatable();

    var idcurso = $('#select_viewTable').val();
    
    if(idcurso != 0){
      drawtotal1(idcurso);
    }
    //Pintar comparativa de habilidad
    
    
    drawcomparativa(idcurso);
    
    /**EVENTOS */

    $('#select_viewTable').on('change',function(){
      if($('#select_viewTable').val() != 0){
        drawtotal1($('#select_viewTable').val());
        drawcomparativa($('#select_viewTable').val());
        drawcomparativa($('#select_viewTable').val(),true);
      }
    });
    $('#select_viewBimestre').on('change',function(){
      drawcomparativa($('#select_viewTable').val());
    });
    $('#select_viewTrimestre').on('change',function(){
      drawcomparativa($('#select_viewTable').val(),true);
    });
    $('#switch_tipo').on("change", function(){
        var switchera = $('.switchera').is(':checked');
        var idcurso = $('#select_viewTable').val();
        if(switchera){
            $('#triselect').show();
            $('#biselect').hide();

        }else{
            $('#biselect').show();
            $('#triselect').hide();
        }
        drawcomparativa(idcurso,switchera);
    });

    $('#idcolegio').change(function(){
        //change width
        var texto = nombreselect('#idcolegio',$('#idcolegio').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

            //buscar informacion del colegio...
            $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
          //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
          //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
    $('#idseccion').change(function(){
        //change width
        var texto = nombreselect('#idseccion',$('#idseccion').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        // alert($(this).val());
        $.ajax({
            url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
            type: 'POST',
            dataType: 'json',
            data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
        }).done(function(resp) {
            if(resp.code=='ok'){
                var alumnos = $('<tr></tr>');
                var filas = '';
                var tabla_tmp = $('<table></table>');
                tabla_tmp.attr('id','tabla_alumnos');
                tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
                tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names") ?></td></tr></thead><tbody></tbody>');
                console.log(resp.data.alumnos);
                for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                    //data-dni="'+resp.data.alumnos[i].dni+'"
                    filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
                }
                tabla_tmp.find('tbody').html(filas);

                $('#tabla_alumnos_wrapper').remove();
                $('#contenedorAlumnos').append(tabla_tmp);
                dibujarDatatable();
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    });

    $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
        if($(this).data('id')){
            var controllerAndFunction = '/reportealumno/progreso_habilidad';
            var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
            $('#viewContent').html('');
            $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
            
            $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
            // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
            $('#contenedorAlumnos table').css('pointer-events',"none");
            $('#contenedorAlumnos table').css('opacity',"0.5");
        }
    });
});

</script>