<?php 
defined("RUTA_BASE") or die(); 
$idgui = uniqid();
$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
// $urlbase = $this->documento->getUrlBase();
$entrada = ($this->entrada) ? 'true' : 'false';
echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
echo "<input type='hidden' id='dni' value='{$this->user['dni']}'/>";
?>

<style type="text/css">
    .div_menu{
        height: 120px; font-size: 20px; text-align: center;
        padding: 40px;
    }
    .color1{
        background-color: #EB6B56;
    }
    .color2{
        background-color: #2C82C9;
    }
    .color3{
        background-color: #9365B8;
    }
    .color4{
        background-color: #FAC51C;
    }

    .color5{
        background-color: #61BD6D;
    }
    
    .select-ctrl-wrapper:after{
        right:0!important;
        height:30px;
    }
    .table-responsive{
        max-height:200px;
    }
    #tabla_alumnos td{ cursor:pointer; }
    #tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
    #tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
    #tabla_alumnos_length { display: none; }
    
    .imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<div class="container">
    <!-- <div style="position:relative;  margin:10px 0;">
        <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
    </div> -->

    <div class="">
        <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
        <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
        <?php if($this->entrada == true): ?>
            <li class="active">
                <?php echo JrTexto::_("Beginning test"); ?>
            </li>                    
        <?php else: ?>
            <li class="active">
                <?php echo JrTexto::_("Final test"); ?>
            </li>    
        <?php endif; ?>
        
        </ol>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:left;">
            <?php echo JrTexto::_("Report") ?>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12"><h3><?php echo JrTexto::_('Teacher Reports'); ?></h3></div>
                <div class="col-md-12" style="margin-bottom:5px; ">
                    <div class="row" style="margin:0; border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                        <div class="col-md-3" style="">
                            <div style="height:90px; margin-top:10px;">
                                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                            </div>
                            <div class="" style="text-align:center;">
                                <h4><?php echo $this->user["nombre_full"]; ?></h4>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="table-responsive">
                                <table id="tablaExamenesE" class="table table-bordered table-hover">
                                <!-- <input type="hidden" id="urlBase" value="" /> -->
                                <thead>
                                    <th><?php echo JrTexto::_("Course"); ?></th>
                                    <th><?php echo ($this->entrada == true) ? JrTexto::_("Beginning Test Score") : JrTexto::_("Final Test Score"); ?></th>
                                    <th><?php echo JrTexto::_("Listening"); ?></th>
                                    <th><?php echo JrTexto::_("Reading"); ?></th>
                                    <th><?php echo JrTexto::_("Writing"); ?></th>
                                    <th><?php echo JrTexto::_("Speaking"); ?></th>
                                </thead>
                                <tbody style="font-weight:bold;">
                                    <?php 
                                    if(!empty($this->cursos)){
                                        $keyExamen = $this->entrada == true ? 'E' : 'S';                  
                                        foreach ($this->cursos as $key => $lista_nivel){
                                        $nombre=$lista_nivel['nombre'];
                                        $idnivel=$lista_nivel['idcurso'];
                                        $_nota = ($this->entrada == true) ? intval($this->Total[$idnivel][$keyExamen][0]) : intval($this->Total[$idnivel][$keyExamen][count($this->Total[$idnivel][$keyExamen]) - 1]);
                                        $nota =  $_nota > 100 ? ( ( $_nota / ((ceil($_nota / 100) ) * 100) ) * 100 ) *0.20 : $_nota * 0.20;
                                        $nota = intval(floor($nota));//round($nota,2);
                                        //Imprimir tabla
                                        echo "<tr data-id='".$idnivel."'>";
                                        echo "<td>{$nombre}</td><td>{$nota} pts / 20 pts</td>";
                                        echo "<td><div class='countPercentage' data-id='4' data-value='".$this->TotalHab[$idnivel]['4']."'><span>0</span>%</div><input type='hidden' data-id='4' value='".$this->TotalHabContrario[$idnivel]['4']."' /></td>";
                                        echo "<td><div class='countPercentage' data-id='5' data-value='".$this->TotalHab[$idnivel]['5']."'><span>0</span>%</div><input type='hidden' data-id='5' value='".$this->TotalHabContrario[$idnivel]['5']."' /></td>";
                                        echo "<td><div class='countPercentage' data-id='6' data-value='".$this->TotalHab[$idnivel]['6']."'><span>0</span>%</div><input type='hidden' data-id='6' value='".$this->TotalHabContrario[$idnivel]['6']."' /></td>";
                                        echo "<td><div class='countPercentage' data-id='7' data-value='".$this->TotalHab[$idnivel]['7']."'><span>0</span>%</div><input type='hidden' data-id='7' value='".$this->TotalHabContrario[$idnivel]['7']."' /></td>";
                                        echo "</tr>";
                                        }
                                    }
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"><h3><?php echo JrTexto::_('Students Reports'); ?></h3></div>
                <div class="col-md-12">
                    <div id="info" style="display:block;">
                        <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                        <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
                    </div>
                  <div id="idcolegio-container" style="display: inline-block;">
                      
                      <h4 style="display:inline-block;"><?php echo JrTexto::_('School'); ?></h4>
                      <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                          <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                              <?php if(!empty($this->miscolegios)){
                              echo '<option value="0">'.JrTexto::_('Select').'</option>';
                              foreach ($this->miscolegios  as $c) {
                                  if(empty($cursos)) $cursos=$c["cursos"];
                                  echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                              }} ?>
                          </select>
                      </div>
                  </div>
                  <div id="idcurso-container" style="display: inline-block;">
                      <h4 style="display:inline-block;"><?php echo JrTexto::_('Course'); ?></h4>
                      <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                          <select name="idcurso" id="idcurso" style="width: 150px;" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                          </select>
                      </div>
                  </div>
                  <div id="idgrados-container" style="display: inline-block;">
                      <h4 style="display: inline-block;"><?php echo JrTexto::_('Grades'); ?></h4>
                      <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                          <select name="grados" id="idgrados" style="width: 150px;" class="select-docente form-control select-ctrl">
                            <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                          </select>
                      </div>
                  </div>
                  <div id="idseccion-container" style="display: inline-block;">
                      <h4 style="display: inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
                      <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                          <select name="seccion" id="idseccion" style="width: 150px;" class="select-docente form-control select-ctrl select-nivel">
                            <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                          </select>
                      </div>
                  </div>
                </div>
                <div class="col-md-3">
                    <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                        <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_('Student'); ?></h4>       
                        <div class="table-responsive">
                            <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                                <thead>
                                    <tr><td><?php echo JrTexto::_('Names'); ?></td></tr>
                                </thead>
                            </table>                        
                        </div>
                    </div>
                </div>
                <div class="col-md-9" style="border-radius:0.4em;">
                    <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                        <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                        <h4><?php echo JrTexto::_('Select Student'); ?></h4>
                    </div>
                </div>
                <div class="col-md-9" style="text-align:right;">
                        <div style="padding:5px 0;"></div>
                <?php if($this->entrada == true): ?>
                    
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_examene" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
                <?php else: ?>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_examens" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var entrada = <?php echo $entrada ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}

function countText(obj,valueText, ToValue){
  let currentValue = parseInt(valueText);
  let nextVal = ToValue;
  let diff = nextVal - currentValue;
  let step = ( 0 < diff ? 1 : -1 );
  for (var i = 0; i < Math.abs(diff); ++i) {
      setTimeout(function() {
          currentValue += step
          obj.text(currentValue);
      }, 100 * i)   
  }
}

function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 10,
        "pagingType": "simple"
    });
}
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){  
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');              
        $.each(predatos,function(e,v){   
                  
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
  }
  var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};
$('document').ready(function(){
    $('#info').hide();

    //Dibujar contador de porcentaje de habilidades
    $('.countPercentage').each(function(key, value){
      countText($(this).find('span'), $(this).find('span').text(),parseInt($(this).data('value')) );
    });

    $('#idcurso-container').hide();
    $('#idseccion-container').hide();
    $('#idgrados-container').hide();
    
    dibujarDatatable();
    
    $('#idcolegio').change(function(){
        //change width
        var texto = nombreselect('#idcolegio',$('#idcolegio').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

           //buscar informacion del colegio...
          $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
    $('#idcurso').change(function(){
        //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });

    $('#idseccion').change(function(){
        //change width
        var texto = nombreselect('#idseccion',$('#idseccion').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        // alert($(this).val());
        $.ajax({
            url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
            type: 'POST',
            dataType: 'json',
            data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
        }).done(function(resp) {
            if(resp.code=='ok'){
                let alumnos = $('<tr></tr>');
                let filas = '';
                let tabla_tmp = $('<table></table>');
                tabla_tmp.attr('id','tabla_alumnos');
                tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
                tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names"); ?></td></tr></thead><tbody></tbody>');
                console.log(resp.data.alumnos);
                for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                    //data-dni="'+resp.data.alumnos[i].dni+'"
                    filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'"><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
                }
                tabla_tmp.find('tbody').html(filas);

                $('#tabla_alumnos_wrapper').remove();
                $('#contenedorAlumnos').append(tabla_tmp);
                dibujarDatatable();
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
    });

    $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
        if($(this).data('id')){
            let controllerAndFunction = (entrada == true) ? '/reportealumno/reporte_entrada' : '/reportealumno/reporte_salida';
            let url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&usuario='+$(this).data('user')+'&clave='+$(this).data('key')+'&plt=sintop2';
            $('#viewContent').html('');
            $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
            $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
            $('#contenedorAlumnos table').css('pointer-events',"none");
            $('#contenedorAlumnos table').css('opacity',"0.5");
            // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
        }
    });
});
</script>