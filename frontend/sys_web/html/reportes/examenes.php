<?php
defined('RUTA_BASE') or die();
$idgui = uniqid();

$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$url = $this->documento->getUrlBase();

echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";

$json_sumBimestre = json_encode($this->sumBimestre);
$json_sumTrimestre = json_encode($this->sumTrimestre);
$json_sumBimestre_habilidad = json_encode($this->sumBimestre_habilidad);
$json_sumTrimestre_habilidad = json_encode($this->sumTrimestre_habilidad);
$json_sumBimestre_habilidad_total = json_encode($this->sumBimestre_habilidad_total);
$json_sumTrimestre_habilidad_total = json_encode($this->sumTrimestre_habilidad_total);

?>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
  
}

/*Custom2*/
.circleProgressBar1{
  padding:5px 0;
}
.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){
  margin:0 auto;
}

.Listening-color {
  background-color:#beb3e2;
  border-radius: 1em;
}
.Reading-color {
  background-color:#d9534f;
  border-radius: 1em;
}
.Speaking-color {
  background-color:#5bc0de;
  border-radius: 1em;
}
.Writing-color {
  background-color:#5cb85c;
  border-radius: 1em;
}
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.text-custom1{ font-weight: bold; color: black; font-size: large; }

.circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
.text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
.text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>

<!--START CONTAINER-->
<div class="container">

<!-- <div style="position:relative;  margin:10px 0;">
    <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
</div> -->

<?php if(!$this->isIframe): ?>
<div class="row " id="levels" style="padding-top: 1ex; ">
  <div class="col-md-12">
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
      <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li> 
      <li class="active">
        <?php echo JrTexto::_("Bimonthly and Quarterly Tests"); ?>
      </li>
    </ol>
  </div>
</div>
<?php endif; ?>

<!--START PANEL-->
<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Reporte");?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
      <h3 style="font-weight:bold;"><?php echo JrTexto::_("Bimestral / Quarterly Exam Student Skills"); ?></h3>
          <div class="">
            <div class="row" style="margin:15px auto;">
              <div class="col-md-3" style="/*height:90px;*/">
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $this->foto ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                <div class="" style="text-align:left;">
                  <h4><?php echo $this->fullname; ?></h4>
                </div>
                <!-- <div class="col-sm-6">
                    <p class="text-custom1">Listening</p>
                    <div id="barListening" class="circleProgressBar1" style="font-size:0.5em;"></div>
                </div>
                <div class="col-sm-6 ">
                    <p class="text-custom1">Reading</p>
                    <div id="barReading" class="circleProgressBar1" style="font-size:0.5em;"></div>
                </div>
                <div class="col-sm-6 ">
                    <p class="text-custom1">Writing</p>
                    <div id="barWriting" class="circleProgressBar1" style="font-size:0.5em;"></div>
                </div>
                <div class="col-sm-6 ">
                    <p class="text-custom1">Speaking</p>
                    <div id="barSpeaking" class="circleProgressBar1" style="font-size:0.5em;"></div>
                </div> -->
              </div>
              <!--end col-->
              <div class="col-md-9">
                <!--!code-->
                <div class="col-sm-12">
                  <!--START tab for report-->
                  <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#home"><?php echo JrTexto::_('Bimonthly'); ?></a></li>
                    <li><a data-toggle="pill" href="#menu1"><?php echo JrTexto::_('Quarterly'); ?></a></li>
                    <li><a data-toggle="pill" href="#menu2"><?php echo JrTexto::_('Bimonthly').'/'.JrTexto::_('Quarterly'); ?></a></li>
                  </ul>

                  <!--start content-->
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                      <h3><?php echo JrTexto::_('Bimonthly Test Score');?></h3>
                      <div class="table-responsive">
                        <table id="" class="table table-bordered table-hover tablaExamenesE">
                          <input type="hidden" id="urlBase" value="<?php echo $url?>" />
                          <thead>
                            <th><?php echo JrTexto::_("Course"); ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 1"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 2"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 3"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 4"; ?></th>
                          </thead>
                          <tbody>
                            <?php 
                              if(!empty($this->cursos)){                  
                                foreach ($this->cursos as $key => $lista_nivel){
                                  $nombre=$lista_nivel['nombre'];
                                  $idnivel=$lista_nivel['idcurso'];
                                  //Imprimir tabla
                            ?>
                                  <tr data-id="<?php echo $idnivel; ?>">
                                    <td>
                                      <p> <?php echo $nombre; ?></p>
                                    </td>
                                    <?php

                                      if(array_key_exists($lista_nivel['idcurso'], $this->sumBimestre) == true){
                                        for($i=1;$i<=4;$i++){
                                    ?>
                                        <td> 
                                          <?php 
                                            //ceil;
                                            if($this->sumBimestre[$lista_nivel['idcurso']][$i] > 100){
                                              $calculo = ($this->sumBimestre[$lista_nivel['idcurso']][$i] / ( (ceil( $this->sumBimestre[$lista_nivel['idcurso']][$i] / 100 )) * 100 )) * 100;
                                            }else{
                                              $calculo = $this->sumBimestre[$lista_nivel['idcurso']][$i];
                                            }
                                            echo intval(floor( $calculo * 0.20  ))." pts / 20 pts";
                                          ?>
                                          <button class="btn viewDetail"  style="border-radius:100%; float:right;" data-tipo="B" data-curso="<?php echo $lista_nivel['idcurso']; ?>" data-bimestre="<?php echo $i; ?>" type="button" data-toggle="tooltip" title="Ver Habilidades Desarrolladas"><i class="fa fa-eye"></i></button>
                                        </td>
                                    <?php
                                        }
                                      }else{
                                        for($i=0;$i<4;$i++){
                                          echo "<td>0</td>";
                                        }
                                      }
                                    ?>
                                  </tr>
                            <?php 
                                } //endforeach que recorre cursos
                              } //endif verificar si esta vacio cursos
                            ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                      <h3><?php echo JrTexto::_('Quarterly Test Score'); ?></h3>
                      <div class="table-responsive">
                        <table id="" class="table table-bordered table-hover tablaExamenesE">
                          <input type="hidden" id="urlBase" value="<?php echo $url?>" />
                          <thead>
                            <th><?php echo JrTexto::_("Course"); ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 1"; ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 2"; ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 3"; ?></th>
                          </thead>
                          <tbody>
                            <?php 
                              if(!empty($this->cursos)){                  
                                foreach ($this->cursos as $key => $lista_nivel){
                                  $nombre=$lista_nivel['nombre'];
                                  $idnivel=$lista_nivel['idcurso'];
                                  //Imprimir tabla
                            ?>
                                  <tr data-id="<?php echo $idnivel; ?>">
                                    <td>
                                      <p> <?php echo $nombre; ?></p>
                                    </td>
                                    <?php

                                      if(array_key_exists($lista_nivel['idcurso'], $this->sumTrimestre) == true){
                                        for($i=1;$i<=3;$i++){
                                    ?>
                                        <td> 
                                          <?php 
                                            //ceil;
                                            if($this->sumTrimestre[$lista_nivel['idcurso']][$i] > 100){
                                              $calculo = $this->sumTrimestre[$lista_nivel['idcurso']][$i] / ( (ceil( $this->sumTrimestre[$lista_nivel['idcurso']][$i] / 100 )) * 100 );
                                            }else{
                                              $calculo = $this->sumTrimestre[$lista_nivel['idcurso']][$i];
                                            }
                                            echo intval(floor( $calculo * 0.20  ))." pts / 20 pts";
                                          ?>
                                          <button class="btn viewDetail"  style="border-radius:100%; float:right;" data-tipo="T" data-curso="<?php echo $lista_nivel['idcurso']; ?>" data-bimestre="<?php echo $i; ?>" type="button" data-toggle="tooltip" title="<?php echo JrTexto::_("View")." ".JrTexto::_("Developed Skills"); ?>"><i class="fa fa-eye"></i></button>
                                        </td>
                                    <?php
                                        }
                                      }else{
                                        for($i=0;$i<3;$i++){
                                          echo "<td>0</td>";
                                        }
                                      }
                                    ?>
                                  </tr>
                            <?php 
                                } //endforeach que recorre cursos
                              } //endif verificar si esta vacio cursos
                            ?>
                          </tbody>
                        </table>
                      </div>
                      <!--endtable-->
                    </div>
                    <div id="menu2" class="tab-pane fade">
                      <h3><?php echo JrTexto::_('Bimonthly and Quarterly Tests Score'); ?></h3>
                      <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_('Courses'); ?></p>
                      <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                          <select name="select_viewTable" id="select_viewTable" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                            <?php
                              if(!empty($this->cursos)){               
                                foreach ($this->cursos as $key => $lista_nivel){
                                  $nombre=$lista_nivel['nombre'];
                                  $idnivel=$lista_nivel['idcurso'];
                                  echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                                }
                              }else{
                                echo '<option value="0">'.JrTexto::_('no data').'</option>';
                              }
                            ?>
                          </select>
                      </div>
                      <div style="background:#4466bb; color:white;"><h3><?php echo JrTexto::_('Bimester'); ?></h3></div>
                      <!--START TABLES-->
                      <div class="table-responsive" style="margin:10px auto;">
                        <table id="tableBimestre" class="table table-bordered table-hover tablaExamenesE">
                          <thead>
                            <th><?php echo JrTexto::_("Course"); ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 1"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 2"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 3"; ?></th>
                            <th><?php echo JrTexto::_("Bimester")." 4"; ?></th>
                          </thead>
                          <tbody>
                          
                          </tbody>
                        </table>
                      </div>
                      <div style="background:#4466bb; color:white;"><h3><?php echo JrTexto::_('Trimester'); ?></h3></div>
                      <!--trimestre-->
                      <div class="table-responsive">
                        <table id="tableTrimestre" class="table table-bordered table-hover tablaExamenesE">
                          <thead>
                            <th><?php echo JrTexto::_("Course"); ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 1"; ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 2"; ?></th>
                            <th><?php echo JrTexto::_("Trimester")." 3"; ?></th>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>
                      <!--END TABLES-->
                    </div>
                  </div>
                  <!--end content tab-->
                </div>
              </div>
              <!--end col-->
              <!--Start vista general -->
              <div id="viewGeneral" class="col-md-12 panel panel-danger E3-animate-zoom" style="display:block">
                <div class="panel-body">
                  <!-- <div id="piechart_3d" style="width: 900px; height: 500px; margin:0 auto;"></div> -->
                  <!-- InformacionNotas -->
                  <h3 style="font-weight:bold;"><?php echo JrTexto::_("Skills by Courses"); ?></h3>

                  <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_('Courses'); ?></p>
                  <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                      <select name="select_viewGeneral" id="select_viewGeneral" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                        <?php
                          if(!empty($this->cursos)){               
                            foreach ($this->cursos as $key => $lista_nivel){
                              $nombre=$lista_nivel['nombre'];
                              $idnivel=$lista_nivel['idcurso'];
                              echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                            }
                          }else{
                            echo '<option value="0">'.JrTexto::_("No data").'</option>';
                          }
                        ?>
                      </select>
                  </div>

                  <p style="font-size:large; display:inline-block; margin:0 5px; padding-right:15px;"><?php echo JrTexto::_("Bimester")." / ".JrTexto::_("Quarterly"); ?></p>
                  <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                      <select name="select_viewGeneral_tipo" id="select_viewGeneral_tipo" class="select-ctrl select-nivel" style="min-width:220px; height:35px;">
                        <option value="B"><?php echo JrTexto::_("Bimester"); ?></option>
                        <option value="T"><?php echo JrTexto::_("Quarterly"); ?></option>
                      </select>
                  </div>
                  
                  <!-- <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                  <option value="0">test</option>
                  </select> -->

                  <!--START VIEW GENERAL HABILIDAD-->
                  <div id="container-viewgeneral">
                    <div class="col-md-12">
                      <h4 style="margin:5px; padding:10px; border-bottom:3px solid #dadada;" id="chartTitle"><i class="fa fa-info"></i>&nbsp;<?php echo JrTexto::_('Skills Obtained in the Course'); ?> A1</h4>
                    </div>
                    <div class="col-md-12">
                      <div class="card row" style="border-radius:1em;border-color: #E67E22;border-width: 2px;box-shadow:0px 1px 2px;margin: 0;max-height: 600px;overflow-y: scroll;">
                          <div class="col-md-6">
                              <div class="chart-container" id="pieChart_content" style="position: relative; margin:0 auto; width:100%">
                                  <canvas id="pieChart"  width="948" height="474"></canvas>
                              </div>
                          </div>
                          <div class="col-md-6" style="padding-top:15px;">
                            <div class="col-sm-6" id="view_Listening" style="text-align:center;">
                              <h4>Listening</h4>
                              <div class="progress">
                                  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                      <!-- 40% Complete (success) -->
                                  </div>
                              </div>
                              <div class="countPercentage" data-value="35"><span>0</span>%</div>
                            </div>
                            <div class="col-sm-6" id="view_Reading" style="text-align:center;">
                                <h4>Reading</h4>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                    <!-- 50% Complete (info) -->
                                    </div>
                                </div>
                                <div class="countPercentage" data-value="60"><span>0</span>%</div>
                            </div>
                            <div class="col-sm-6" id="view_Writing" style="text-align:center;">
                              <h4>Writing</h4>
                              <div class="progress">
                                  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                      <!-- 60% Complete (warning) -->
                                  </div>
                              </div>            
                              <div class="countPercentage" data-value="50"><span>0</span>%</div>
                            </div>
                            <div class="col-sm-6" id="view_Speaking" style="text-align:center;">
                              <h4>Speaking</h4>
                              <div class="progress">
                                  <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                      <!-- 70% Complete (danger) -->
                                  </div>
                              </div>
                              <div class="countPercentage" data-value="70"><span>0</span>%</div>
                            </div>
                          </div>
                      </div>
                    </div>
                    <!--end view general x curso -->
                  </div>
                  



            </div>
            <!--end row-->
          </div>
          <!--END VIEW GENERAL HABILIDAD-->
          <div id="details" class="col-md-12 panel panel-danger E3-animate-zoom" style="display: none;">
            <div class = 'panel-heading' style="text-align: left; font-weight: bold; font-size: large; min-height: 55px;">
              <?php echo JrTexto::_("Skill Detail")?>
              <button class="btn btn-danger" id="close_details" type="button" style="font-weight: bold; float:right;"><span>&times;</span></button>
            </div>
            <div class="panel-body">
              <!-- <div id="columnchart_values" class="col-md-12" style="overflow: auto" ></div> -->
              <div class="col-sm-12">
                <h4 style="margin:5px; padding:10px; border-bottom:3px solid #dadada;" id="chartTitle2"><i class="fa fa-info"></i>&nbsp;<?php echo JrTexto::_('Skills Obtained in the Course'); ?> A1</h4>
              </div>
              <div class="col-sm-12">
                <div class="card row" style="border-radius:1em;border-color: #E67E22;border-width: 2px;box-shadow:0px 1px 2px;margin: 0;max-height: 600px;overflow-y: scroll;">
                    <div class="col-md-6">
                        <div class="chart-container" id="pieChart2_content" style="position: relative; margin:0 auto; width:100%">
                            <canvas id="pieChart2"  width="948" height="474"></canvas>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-top:15px;">
                      <div id="detail_listening" class="col-sm-6" style="text-align:center;">
                        <h4>Listening</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
                            aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                <!-- 40% Complete (success) -->
                            </div>
                        </div>
                        <div class="countPercentage" data-value="35"><span>0</span>%</div>
                      </div>
                      <div id="detail_reading" class="col-sm-6" style="text-align:center;">
                          <h4>Reading</h4>
                          <div class="progress">
                              <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                              <!-- 50% Complete (info) -->
                              </div>
                          </div>
                          <div class="countPercentage" data-value="0"><span>0</span>%</div>
                      </div>
                      <div id="detail_writing" class="col-sm-6" style="text-align:center;">
                        <h4>Writing</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                <!-- 60% Complete (warning) -->
                            </div>
                        </div>            
                        <div class="countPercentage" data-value="0"><span>0</span>%</div>
                      </div>
                      <div id="detail_speaking" class="col-sm-6" style="text-align:center;">
                        <h4>Speaking</h4>
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                <!-- 70% Complete (danger) -->
                            </div>
                        </div>
                        <div class="countPercentage" data-value="0"><span>0</span>%</div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <!--end container-->
    </div>
    <!--end panelbody-->
    
</div>
</div>
</div>
<div class="panel panel-primary">
      <div class="panel-heading">
        <h3><?php echo JrTexto::_("Students Reports"); ?></h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <div id="info" style="display:block;">
                <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
            </div>
            <div id="idcolegio-container" style="display: inline-block;">
                
                <h4 style="display:inline-block;"><?php echo JrTexto::_("School"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select id="idcolegio" style="width: 200px;" class="select-docente form-control select-ctrl ">
                        <?php if(!empty($this->miscolegios)){
                        echo '<option value="0">'.JrTexto::_("Select").'</option>';
                        foreach ($this->miscolegios  as $c) {
                            if(empty($cursos)) $cursos=$c["cursos"];
                            echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
            <div id="idcurso-container" style="display: inline-block;">
                <h4 style="display:inline-block;"><?php echo JrTexto::_("Course"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="idcurso" id="idcurso" style="width: 150px;" class="select-docente form-control select-ctrl">
                      <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
            <div id="idgrados-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Grades"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="grados" id="idgrados" style="width: 150px;" class="select-docente form-control select-ctrl">
                      <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
            <div id="idseccion-container" style="display: inline-block;">
                <h4 style="display: inline-block;"><?php echo JrTexto::_("Section"); ?></h4>
                <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                    <select name="seccion" id="idseccion" style="width: 150px;" class="select-docente form-control select-ctrl select-nivel">
                      <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
              <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_("Student"); ?></h4>       
              <div class="table-responsive">
                <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                    <thead>
                        <tr><td><?php echo JrTexto::_("Names"); ?></td></tr>
                    </thead>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-9" style="border-radius:0.4em;">
              <div style="margin:5px 0;"></div>
              <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                  <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                  <h4><?php echo JrTexto::_("Select Student"); ?></h4>
              </div>
          </div>
          <div class="col-md-9" style="text-align:right;">
              <div style="padding:5px 0;"></div>
              <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_examenes" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
          </div>
        </div>
      </div>
    </div>
    <!--end panel-->
<!--END PANEL-->
<!--END CONTAINER-->

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">
  /**VARIABLES PHP TO JAVASCRIPT */
  var sumBimestre = <?php echo $json_sumBimestre  ?>;
  var sumTrimestre = <?php echo $json_sumTrimestre ?>;
  var sumBimestre_habilidad = <?php echo $json_sumBimestre_habilidad ?>;
  var sumTrimestre_habilidad = <?php echo $json_sumTrimestre_habilidad ?>;
  var sumBimestre_habilidad_total = <?php echo $json_sumBimestre_habilidad_total ?>;
  var sumTrimestre_habilidad_total = <?php echo $json_sumTrimestre_habilidad_total ?>;
  var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

  /**/////////////////////////////////////////// */
  function countText(obj,valueText, ToValue){
    var currentValue = parseInt(valueText);
    var nextVal = ToValue;
    var diff = nextVal - currentValue;
    var step = ( 0 < diff ? 1 : -1 );
    for (var i = 0; i < Math.abs(diff); ++i) {
        setTimeout(function() {
            currentValue += step
            obj.text(currentValue);
        }, 50 * i)   
    }
  }

  function frameload(){
    $('.loading-chart').remove();
    $('#contenedorAlumnos table').css('pointer-events',"initial");
    $('#contenedorAlumnos table').css('opacity',"1");
  }

  function dibujarDatatable(){
      if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
          $('#tabla_alumnos').DataTable().clear().destroy();
          // console.log($('#tabla_alumnos').find('tbody'));
      }
      $('#tabla_alumnos').DataTable({
          "paging":   true,
          "ordering": false,
          "info":     false,
          "displayLength": 10,
          "pagingType": "simple"
      });
  }
  function drawPie(ctx,data){
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: {
            // responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Skill'
            }
        }
    });
  }
  function drawCustom2(obj,barChartData){
    var _title = (entrada == true) ? "Notas por unidad del estudiante de Entrada" : "Notas por unidad del estudiante de Salida";
    var ctx = document.getElementById(obj).getContext('2d');
    var myBar = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        responsive: true,
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: _title
        }
      }
    });
  }
  function drawHabilidad(obj , option = null, value = 0, speed = null){
    var _anim = speed === null ? 1 : speed;
    var option = option === null ? {colorFg:"#2196f3",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
    option.animationSpeed = _anim;
    var bar=new RadialProgress(obj,option);
    bar.setValue(value);
    bar.draw(true);
  }
  var operacion = function(v){
    var value = 0;
    if(v != 0){
      value = v / 100;
    }
    return value
  };
  var operacion2 = function(v){
    var value = 0;
    if(v != 0){
      value = v * 0.20;
    }
    return value
  };
  var first = function (v) {
    var value = null;
    $t = true;
    for(var i in v ){
      if($t === true){
        value = i;
      }
      $t = false;
    }
    return value;
  };

  //draw tables notas
    
    function drawprogressbar(id,valor){
      $(id).find('.progress-bar').css('width', valor.toString() + '%');
      $(id).find('.progress-bar').attr('aria-valuenow', valor.toString());
    }
    function nameSelect(select,value){
      var text = null;
      select.find('option').each(function(k,v){
        if($(this).attr('value') == value){
          text = $(this).text();
        }
      });
      return text;
    }
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}

var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};

    $(document).ready(function(){
      $('#info').hide();

      $('#idcurso-container').hide();
      $('#idseccion-container').hide();
      $('#idgrados-container').hide();

      dibujarDatatable();
      // console.log(sumBimestre);
      // console.log(sumBimestre.hasOwnProperty($('#select_viewTable').val()));
      //draw tables notas
      var drawbimestretrimestre = function(){
        if($('#select_viewTable').val() != 0){
          var idcur = $('#select_viewTable').val();
          var prepare_html = $('<tr></tr>');
          prepare_html.attr('data-id',$('#select_viewTable').val());
          prepare_html.append('<td><p>'+nameSelect($('#select_viewTable'),$('#select_viewTable').val())+'</p></td>');
          var td_tmp = '';

          if(sumBimestre.hasOwnProperty(idcur)){
            for(var r = 1; r <= 4; r++){
              var calculo = 0;
              if(sumBimestre[idcur][r] > 100){
                calculo = (sumBimestre[idcur][r] / ((Math.ceil(sumBimestre[idcur][r] / 100)) * 100)) * 100;
              }else{
                calculo = sumBimestre[idcur][r];
              }
              td_tmp += '<td>'+Math.floor(calculo * 0.20)+' pts / 20 pts <button class="btn viewDetail"  style="border-radius:100%; float:right;" data-tipo="B" data-curso="'+idcur+'" data-bimestre="'+r+'" type="button" data-toggle="tooltip" title="\<?php echo JrTexto::_("View")." ".JrTexto::_("Developed Skills"); ?>"><i class="fa fa-eye"></i></button></td>';
            }
            
          }else{
            for(var r = 0; r < 4; r++){
              td_tmp += "<td>0 pts / 20 pts</td>"
            }
          } //end if
          
          prepare_html.append(td_tmp);
          $('#tableBimestre').find('tbody').html(" ");
          $('#tableBimestre').find('tbody').append(prepare_html);
          //trimestre
          prepare_html = $('<tr></tr>');
          prepare_html.attr('data-id',$('#select_viewTable').val());
          prepare_html.append('<td><p>'+nameSelect($('#select_viewTable'),$('#select_viewTable').val())+'</p></td>');
          var td_tmp = '';

          if(sumTrimestre.hasOwnProperty(idcur)){
            for(var r = 1; r <= 3; r++){
              var calculo = 0;
              if(sumTrimestre[idcur][r] > 100){
                calculo = (sumTrimestre[idcur][r] / ((Math.ceil(sumTrimestre[idcur][r] / 100)) * 100)) * 100;
              }else{
                calculo = sumTrimestre[idcur][r];
              }
              td_tmp += '<td>'+Math.floor(calculo * 0.20)+' pts / 20 pts <button class="btn viewDetail"  style="border-radius:100%; float:right;" data-tipo="T" data-curso="'+idcur+'" data-bimestre="'+r+'" type="button" data-toggle="tooltip" title="\<?php echo JrTexto::_("View")." ".JrTexto::_("Developed Skills");?>"><i class="fa fa-eye"></i></button></td>';
            }
            
          }else{
            for(var r = 0; r < 3; r++){
              td_tmp += "<td>0 pts / 20 pts</td>"
            }
          } //end iff
          prepare_html.append(td_tmp);
          
          $('#tableTrimestre').find('tbody').html(" ");
          $('#tableTrimestre').find('tbody').append(prepare_html);

        }
      };

      //draw total de habilidad de todos los cursos
      var val = 0;
      var nuevoTotal = {4:0, 5:0, 6:0, 7:0};
      for(var i in sumBimestre_habilidad_total){
        nuevoTotal[4] += sumBimestre_habilidad_total[i][4];
        nuevoTotal[5] += sumBimestre_habilidad_total[i][5];
        nuevoTotal[6] += sumBimestre_habilidad_total[i][6];
        nuevoTotal[7] += sumBimestre_habilidad_total[i][7];
      }
      for(var i in sumTrimestre_habilidad_total){
        nuevoTotal[4] += sumTrimestre_habilidad_total[i][4];
        nuevoTotal[5] += sumTrimestre_habilidad_total[i][5];
        nuevoTotal[6] += sumTrimestre_habilidad_total[i][6];
        nuevoTotal[7] += sumTrimestre_habilidad_total[i][7];
      }
      for(var i in nuevoTotal){
        if(nuevoTotal[i] > 100){
          nuevoTotal[i] = (nuevoTotal[i] * 100 ) / 200;
        }
      }
      // drawHabilidad(document.getElementById("barListening"),null, nuevoTotal[4] / 100,0.15);
      // drawHabilidad(document.getElementById("barReading"),null, nuevoTotal[5] / 100,0.15);
      // drawHabilidad(document.getElementById("barWriting"),null, nuevoTotal[6] / 100,0.15);
      // drawHabilidad(document.getElementById("barSpeaking"),null, nuevoTotal[7] / 100,0.15);

      //draw inicial del total de habilidad por cursos
      
      var drawViewGeneral = function(){
        if($('#select_viewGeneral').val() != 0){
          $('#container-viewgeneral').find('#chartTitle').html('<i class="fa fa-info"></i>&nbsp;\<?php echo JrTexto::_("Skills Obtained in the Course"); ?> '+ nameSelect($('#select_viewGeneral'),$('#select_viewGeneral').val()));
          var object = sumBimestre_habilidad_total;
          if($('#select_viewGeneral_tipo').val() == 'T'){
            object = sumTrimestre_habilidad_total;
          }
          var idcurso_total = $('#select_viewGeneral').val();

          //draw barras y texto

          $('#view_Listening').find('.progress-bar').css('width', object[idcurso_total][4].toString() + '%');
          $('#view_Listening').find('.progress-bar').attr('aria-valuenow', object[idcurso_total][4].toString());
          countTextObject = $('#view_Listening').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[idcurso_total][4]) );
          $('#view_Reading').find('.progress-bar').css('width', object[idcurso_total][5].toString() + '%');
          $('#view_Reading').find('.progress-bar').attr('aria-valuenow', object[idcurso_total][5].toString());
          countTextObject = $('#view_Reading').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[idcurso_total][5]) );
          $('#view_Writing').find('.progress-bar').css('width', object[idcurso_total][6].toString() + '%');
          $('#view_Writing').find('.progress-bar').attr('aria-valuenow', object[idcurso_total][6].toString());
          countTextObject = $('#view_Writing').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[idcurso_total][6]) );
          if(object[idcurso_total][7]){
            $('#view_Speaking').find('.progress-bar').css('width', object[idcurso_total][7].toString() + '%');
            $('#view_Speaking').find('.progress-bar').attr('aria-valuenow', object[idcurso_total][7].toString());
            countTextObject = $('#view_Speaking').find('.countPercentage');
            countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[idcurso_total][7]) );
          }else{
            $('#view_Speaking').find('.progress-bar').css('width','0%');
            $('#view_Speaking').find('.progress-bar').attr('aria-valuenow', 0);
            countTextObject = $('#view_Speaking').find('.countPercentage');
            countText(countTextObject.find('span'), countTextObject.find('span').text(),0 );
          }

          // draw pie
          var _datatopie = {
            datasets: [{
                data: [object[idcurso_total][4], object[idcurso_total][5], object[idcurso_total][6],object[idcurso_total][7]],
                backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Listening',
                'Reading',
                'Writing',
                'Speaking'
            ]
          };
          $("#pieChart_content").html("").html('<canvas id="pieChart"></canvas>'); //No es la mejor opcion investigar update de la libreria
          drawPie(document.getElementById('pieChart'),_datatopie);
        }else{
          $('#container-viewgeneral').hide();
        }
      };

      drawViewGeneral();

      /**//////////////////////////////////////////////////////////////// */
      drawbimestretrimestre();
      $('#select_viewGeneral').on('change',function(){
        drawViewGeneral();
      });
      $('#select_viewGeneral_tipo').on('change',function(){
        drawViewGeneral();
      });
      $('#select_viewTable').on('change',function(){
        drawbimestretrimestre();
      });

      $('[data-toggle="tooltip"]').tooltip(); 
      $('#close_details').on('click',function(){
        $(this).parents("#details").hide();
        $('#viewGeneral').show();
      });
      $('.tablaExamenesE').on('click','.viewDetail',function(){
        var row = $(this).parents('tr');
        $('#chartTitle2').html('<i class="fa fa-info"></i>&nbsp;\<?php echo JrTexto::_("Skills Obtained in the Course"); ?> ' + row.find('td').eq(0).text());
        var curso = $(this).data('curso');
        var numero = $(this).data('bimestre');
        var countTextObject = null;
        //ver tipo
        var object = sumBimestre_habilidad;
        if($(this).data('tipo') == 'T'){
          object = sumTrimestre_habilidad;
        }
        // draw progress bar 
        if(object[curso] && object[curso][numero] && object[curso][numero][4]){
          drawprogressbar('#detail_listening',object[curso][numero][4]);
          countTextObject = $('#detail_listening').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[curso][numero][4]) );
        }
        if(object[curso] && object[curso][numero] && object[curso][numero][5]){
          drawprogressbar('#detail_reading',object[curso][numero][5]);
          countTextObject = $('#detail_reading').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[curso][numero][5]) );
        }
        if(object[curso] && object[curso][numero] && object[curso][numero][6]){
          drawprogressbar('#detail_writing',object[curso][numero][6]);
          countTextObject = $('#detail_writing').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[curso][numero][6]) );
        }
        if(object[curso] && object[curso][numero] &&  object[curso][numero][7]){
          drawprogressbar('#detail_speaking',object[curso][numero][7]);
          countTextObject = $('#detail_speaking').find('.countPercentage');
          countText(countTextObject.find('span'), countTextObject.find('span').text(),parseInt(object[curso][numero][7]) );
        }

        // draw pie
        var _dat = [0,0,0,0];
        
        if(object[curso] && object[curso][numero]){
          _dat = [object[curso][numero][4], object[curso][numero][5], object[curso][numero][6],object[curso][numero][7]];
        }
        var _datatopie = {
          datasets: [{
              data: _dat,
              backgroundColor:["#ffbb04","#c42d1b","#d9b679","#2196f3"]
          }],

          // These labels appear in the legend and in the tooltips when hovering different arcs
          labels: [
              'Listening',
              'Reading',
              'Writing',
              'Speaking'
          ]
        };
        $("#pieChart2_content").html("").html('<canvas id="pieChart2"></canvas>'); //No es la mejor opcion investigar update de la libreria
        drawPie(document.getElementById('pieChart2'),_datatopie);
        

        $('#viewGeneral').hide();
        $('#details').show();
      });
      /** EVENTOS ALUMNOS */
      $('#idcolegio').change(function(){
        //change width
        var texto = nombreselect('#idcolegio',$('#idcolegio').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

           //buscar informacion del colegio...
          $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
        //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
    $('#idseccion').change(function(){
      //change width
      var texto = nombreselect('#idseccion',$('#idseccion').val());
      // console.log(texto);
      $(this).css('width',(texto.length + 200)+'px');
        // alert($(this).val());
      $.ajax({
          url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
          type: 'POST',
          dataType: 'json',
          data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
      }).done(function(resp) {
          if(resp.code=='ok'){
              var alumnos = $('<tr></tr>');
              var filas = '';
              var tabla_tmp = $('<table></table>');
              tabla_tmp.attr('id','tabla_alumnos');
              tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
              tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names"); ?></td></tr></thead><tbody></tbody>');
              console.log(resp.data.alumnos);
              for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                  //data-dni="'+resp.data.alumnos[i].dni+'"
                  filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
              }
              tabla_tmp.find('tbody').html(filas);

              $('#tabla_alumnos_wrapper').remove();
              $('#contenedorAlumnos').append(tabla_tmp);
              dibujarDatatable();
          } else {
              return false;
          }
      })
      .fail(function(xhr, textStatus, errorThrown) {
          return false;
      });
    });

    $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
        if($(this).data('id')){
            var controllerAndFunction = '/reportealumno/repor_examene';
            var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
            $('#viewContent').html('');
            $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
            $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
            $('#contenedorAlumnos table').css('pointer-events',"none");
            $('#contenedorAlumnos table').css('opacity',"0.5");
            // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
        }
    });
    });//end document


</script>
