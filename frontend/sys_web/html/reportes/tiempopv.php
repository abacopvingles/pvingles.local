<?php defined('RUTA_BASE') or die();
$idgui = uniqid();

$usuarioAct = NegSesion::getUsuario();
$urlbase = $this->documento->getUrlBase();
$RUTA_BASE = $this->documento->getUrlBase();

$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';

$tiemposArray = array('tiempoPV' => 0, 'tiempoG' => 0, 'tiempoA' => 0, 'tiempoE' => 0, 'tiempoSB' => 0, 'tiempoT' => 0);

echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";

function conversorSegundosHoras($tiempo_en_segundos) {
    $horas = floor($tiempo_en_segundos / 3600);
    $minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
    $segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);

    return $horas . ':' . $minutos . ":" . $segundos;
}
//echo conversorSegundosHoras(16064);
?>

<?php

$horasCount = null;

if(!empty($this->lista_tiempo)){
  foreach ($this->lista_tiempo as $lista1){
    $tiemposArray['tiempoPV']=abs($lista1['tiempo']);
    $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
    //echo $totalpv."<br>";
    
    $horas=explode(":",$totalpv);
    
    $hora=$horas[0];          
    if (!$hora) $hora=0;          
    $min=$horas[1];
    if (!$min) $min=0;
    $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
    
    $horasCount = $horas;

  }//for
}
if(!empty($this->games)){
  foreach ($this->games as $lista1){
    $tiemposArray['tiempoG']=abs($lista1['tiempo']);
    $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
    //echo $totalpv."<br>";
    $horas=explode(":",$totalpv);
    
    $hora=$horas[0];          
    if (!$hora) $hora=0;          
    $min=$horas[1];
    if (!$min) $min=0;
    $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
    
    $horasCount_games = $horas;

  }//for
}
if(!empty($this->actividades)){
    foreach ($this->actividades as $lista1){
      $tiemposArray['tiempoA']=abs($lista1['tiempo']);
      $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
      //echo $totalpv."<br>";
      $horas=explode(":",$totalpv);
      
      $hora=$horas[0];          
      if (!$hora) $hora=0;          
      $min=$horas[1];
      if (!$min) $min=0;
      $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
      
      $horasCount_actvidad = $horas;

    }//for
  }

  if(!empty($this->examenes)){
    foreach ($this->examenes as $lista1){
      $tiemposArray['tiempoE']=abs($lista1['tiempo']);
      $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
      //echo $totalpv."<br>";
      $horas=explode(":",$totalpv);
      
      $hora=$horas[0];          
      if (!$hora) $hora=0;          
      $min=$horas[1];
      if (!$min) $min=0;
      $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
      
      $horasCount_examenes = $horas;

    }//for
  }
  if(!empty($this->smartbook)){
    foreach ($this->smartbook as $lista1){
      $tiemposArray['tiempoSB']=abs($lista1['tiempo']);
      $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
      //echo $totalpv."<br>";
      $horas=explode(":",$totalpv);
      
      $hora=$horas[0];          
      if (!$hora) $hora=0;          
      $min=$horas[1];
      if (!$min) $min=0;
      $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
      
      $horasCount_smartbook = $horas;

    }//for
  }
  if(!empty($this->tareas)){
    foreach ($this->tareas as $lista1){
      $tiemposArray['tiempoT']=abs($lista1['tiempo']);
      $totalpv=conversorSegundosHoras(abs($lista1['tiempo']));
      //echo $totalpv."<br>";
      $horas=explode(":",$totalpv);
      
      $hora=$horas[0];          
      if (!$hora) $hora=0;          
      $min=$horas[1];
      if (!$min) $min=0;
      $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
      
      $horasCount_tareas = $horas;

    }//for
  }
  

/*for ($i=1;$i<=50;$i++){
  $ale = rand (1,100);
  $reporte.='
  ["'.$curso.'", '.$ale.', "#'.$colores[$x].'"],';
  $x++;
  if ($x==4) $x=0;
}*/

$json_tiemposArray = (!empty($tiemposArray)) ? json_encode($tiemposArray) : 'null'; 
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css">

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

.card{
    display: block;
    width: 100%;
    border: 1px solid #ccc;
    padding: 0;
}
.card .card-title{
    font-size: 15px;
    font-weight: bold;
    border-bottom: 1px solid #ccc;
    padding-left: 15px;
    line-height: 2.2;
    height:80px;
}
.card .card-info{
    font-size: 2.0em;
    text-shadow: 2px 1px 3px #aaa;
    font-weight: bolder;
    text-align: center;
    border-bottom: 1px solid #efefef;
}
.card .card-info:last-child{ border-bottom:none; }

.card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }

.card.card-time .card-info .anio:after,
.card.card-time .card-info .mes:after,
.card.card-time .card-info .dia:after,
.card.card-time .card-info .hora:after,
.card.card-time .card-info .min:after,
.card.card-time .card-info .seg:after{
    font-size: 0.7em;
}
.card.card-time .card-info .anio:after{ content: "y"; }
.card.card-time .card-info .mes:after { content: "m"; }
.card.card-time .card-info .dia:after { content: "d"; }
.card.card-time .card-info .hora:after{ content: "h"; }
.card.card-time .card-info .min:after { content: "m"; }
.card.card-time .card-info .seg:after { content: "s"; }

.card .card-info .promedio{
    line-height: 75px;
}
.card .card-info .comentario span{
    display: block;
}
.card .card-info .comentario span.letras{
    font-size: .5em;
}
/*
* CUSTOM
*/
.time-container{
  padding:10px 5px;
  float:none;
  display:inline-block;
  vertical-align:top;
}
.div_menu{
    height: 120px; font-size: 20px; text-align: center;
    padding: 40px;
}
.color1{
    background-color: #EB6B56;
}
.color2{
    background-color: #2C82C9;
}
.color3{
    background-color: #9365B8;
}
.color4{
    background-color: #FAC51C;
}

.color5{
    background-color: #61BD6D;
}

.select-ctrl-wrapper:after{
    right:0!important;
    height:30px;
}
#tabla_alumnos td{ cursor:pointer; }
#tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
#tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
#tabla_alumnos_length { display: none; }
.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="container">
    <!-- <div style="position:relative;  margin:10px 0;">
        <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
    </div> -->
    <div class="row " id="levels" style="padding-top: 1ex; ">
        <div class="col-md-12">
            <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>
            <li class="active">
                <?php echo JrTexto::_("Study Time in the Virtual Platform"); ?>
            </li>
            </ol>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:left;">
            <?php echo JrTexto::_("Report") ?>
        </div>
        <div class="panel-body">
            <!--START TIME-->
            <div class="row">
                <div class="col-md-12"><h3><?php echo JrTexto::_("Teacher's Report - Times"); ?></h3></div>
                <div class="col-md-12" style="margin-bottom:5px; ">
                    <div class="row" style="margin:0; border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                        <div class="col-md-3" style="">
                            <div style="height:90px; margin-top:10px;">
                                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                            </div>
                            <div class="" style="text-align:center;">
                                <h4><?php echo $this->user["nombre_full"]; ?></h4>
                            </div>
                            <!-- <div class="chart-container" style="position: relative; margin:0 auto; height:100%; ">
                                <canvas id="columnchart_values2" style="display: block; width: 320px; height: 313px;"></canvas>
                            </div> -->
                        </div>
                        <div class="col-md-9">
                        <div class="row" style="text-align:center; padding-top:15px;">
                            <!-- en PV-->
                            <div class="col-xs-4 time-container" id="tiempoPV"  >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Study Time in the Virtual Platform'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount[0])) ? $horasCount[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount[1])) ? $horasCount[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount[2])) ? $horasCount[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!--en practica de HomeWork-->
                            <div class="col-xs-4 time-container" id="tiempoT"  >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Time Spent on Activities'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_tareas[0])) ? $horasCount_tareas[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_tareas[1])) ? $horasCount_tareas[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_tareas[2])) ? $horasCount_tareas[2] : '0'; ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </div>

                            <!--en actividades-->
                            <!-- <div class="col-xs-4 time-container" id="tiempo"  >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Tiempo empleado en actividades'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos">
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_actvidad[0])) ? $horasCount_actvidad[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_actvidad[1])) ? $horasCount_actvidad[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_actvidad[2])) ? $horasCount_actvidad[2] : '0'; ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </div> -->

                            <!-- en smartbook-->
                            <div class="col-xs-4 time-container" id="tiempoSB"  >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Time Spent on the SmartBook'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_smartbook[0])) ? $horasCount_smartbook[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_smartbook[1])) ? $horasCount_smartbook[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_smartbook[2])) ? $horasCount_smartbook[2] : '0'; ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </div>
                            <!--en juegos del smartbook-->
                            <!-- <div class="col-xs-4 time-container" id="tiempoG"  >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Tiempo empleado en juegos del SmartBook'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos">
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_games[0])) ? $horasCount_games[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_games[1])) ? $horasCount_games[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_games[2])) ? $horasCount_games[2] : '0'; ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            </div> -->
                            <!--en examenes en smartquiz-->
                            <div class="col-xs-4 time-container" id="tiempoE" >
                            <div class="card card-time">
                                <div class="card-title"><?php echo JrTexto::_('Time Spent on Exams'); ?></div>
                                <div class="card-info">
                                    <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                                        <div class="info-tiempo tiempo-obtenido">
                                            <span class="anio" style="display: none;">00</span>
                                            <span class="mes" style="display: none;">00</span>
                                            <span class="dia" style="display: none;">00</span>
                                            <span class="hora"><?php echo (!empty($horasCount_examenes[0])) ? $horasCount_examenes[0] : '0'; ?></span>
                                            <span class="min"><?php echo (!empty($horasCount_examenes[1])) ? $horasCount_examenes[1] : '0'; ?></span>
                                            <span class="seg"><?php echo (!empty($horasCount_examenes[2])) ? $horasCount_examenes[2] : '0'; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END TIME-->
        </div>
        <div class="col-md-12"><h3><?php echo JrTexto::_("Students Reports"); ?></h3></div>
        <div class="col-md-12">
            <div id="info" style="display:block;">
                <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
            </div>
          <div id="idcolegio-container" style="display: inline-block;">
              
              <h4 style="display:inline-block;"><?php echo JrTexto::_("School"); ?></h4>
              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                  <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                      <?php if(!empty($this->miscolegios)){
                      echo '<option value="0">'.JrTexto::_("Select").'</option>';
                      foreach ($this->miscolegios  as $c) {
                          if(empty($cursos)) $cursos=$c["cursos"];
                          echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                      }} ?>
                  </select>
              </div>
          </div>
          <div id="idcurso-container" style="display: inline-block;">
              <h4 style="display:inline-block;"><?php echo JrTexto::_("Course"); ?></h4>
              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                  <select name="idcurso" style="width:150px;" id="idcurso" class="select-docente form-control select-ctrl">
                    <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                  </select>
              </div>
          </div>
          <div id="idgrados-container" style="display: inline-block;">
              <h4 style="display: inline-block;"><?php echo JrTexto::_("Grade"); ?></h4>
              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                  <select name="grados" style="width:150px;" id="idgrados" class="select-docente form-control select-ctrl">
                    <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                  </select>
              </div>
          </div>
          <div id="idseccion-container" style="display: inline-block;">
              <h4 style="display: inline-block;"><?php echo JrTexto::_("Section"); ?></h4>
              <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                  <select name="seccion" style="width:150px;" id="idseccion" class="select-docente form-control select-ctrl select-nivel">
                    <option value="0"><?php echo JrTexto::_("Select"); ?></option>
                  </select>
              </div>
          </div>
      </div>
        <div class="col-md-3">
            <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_("Students"); ?></h4>       
                <div class="table-responsive">
                    <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                        <thead>
                            <tr><td><?php echo JrTexto::_("Names"); ?></td></tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-9" style="border-radius:0.4em;">
            <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;">
                <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                <h4><?php echo JrTexto::_("Select Student"); ?></h4>
            </div>
        </div>
        <div class="col-md-9" style="text-align:right;">
            <div style="padding:5px 0;"></div>
            <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_tiempo" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_("View Results by Section"); ?></a>
        </div>
    </div>
</div>

<script type="text/javascript">
var tiemposArray = <?php echo $json_tiemposArray ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}
function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 10,
        "pagingType": "simple"
    });
}

function drawChart(obj,dat){
  var ctx = document.getElementById(obj).getContext('2d');
  var myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: dat,
    options: {
      responsive: true,
      title: {
        display: true,
        text: 'Time spent on the platform'
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      legend:false
    }
  });
}

function calcular(value,isline = false){
  if(value == 0 || !value || typeof value == 'undefined'){ return 0; }
  return (isline == true) ? parseInt(value * ((Math.floor((Math.random()*3)+1) * 0.1) + 1)) : parseInt(value);
}

var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("Select"); ?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}

var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};

$('document').ready(function(){
    $('#info').hide();

    //$('#selectGrupo').hide();
    $('#idcurso-container').hide();
    $('#idseccion-container').hide();
    $('#idgrados-container').hide();

    
    dibujarDatatable();
    //console.log(tiemposArray);
    //columnchart_values2
//     var chartData = {
//     labels: ['Smartbook', 'Test', 'Activity',''],
//     datasets: [{
//       type: 'bar',
//       backgroundColor: ['rgba(255,30,131,0.5)','rgba(0,100,131,0.5)','rgba(25,30,131,0.5)'],
//       data: [
//          calcular(tiemposArray.tiempoSB),calcular(tiemposArray.tiempoE),calcular(tiemposArray.tiempoT)
//         //10,20,10,5
//       ],
//       borderColor: 'white',
//       borderWidth: 2
//     }]

//   };
  //Accion al cambiar valor al input de seleccionar curso
  $('#idcolegio').change(function(){
    //change width
    var texto = nombreselect('#idcolegio',$('#idcolegio').val());
    // console.log(texto);
    $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }

          //buscar informacion del colegio...
          $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
        //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
  /*
  $('#select_Course').change(function(){
    $('#selectGrupo').show();
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupo',
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': $('#select_Course').val(), 'idproyecto': $('#proyecto').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            let options = '<option val="0">Seleccionar</option>';
            console.log(resp.data.grupos.length);
            for (var i = resp.data.grupos.length - 1; i >= 0; i--) {
                options += '<option value="'+resp.data.grupos[i].id+'" >'+resp.data.grupos[i].nombre+'</option>';
            }
            $('#select_Group').html(options);
        } else {
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
  });
*/
  $('#idseccion').change(function(){
      //change width
      var texto = nombreselect('#idseccion',$('#idseccion').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
    // alert($(this).val());
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
        }).done(function(resp) {
            if(resp.code=='ok'){
                let alumnos = $('<tr></tr>');
                let filas = '';
                let tabla_tmp = $('<table></table>');
                tabla_tmp.attr('id','tabla_alumnos');
                tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
                tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names") ?></td></tr></thead><tbody></tbody>');

                for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                    filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'"><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
                }
                tabla_tmp.find('tbody').html(filas);

                $('#tabla_alumnos_wrapper').remove();
                $('#contenedorAlumnos').append(tabla_tmp);
                dibujarDatatable();
            } else {
                return false;
            }
    }).fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
    
    });

//   drawChart('columnchart_values2',chartData);
  $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
        if($(this).data('id')){
            let controllerAndFunction = '/reportealumno/repor_tiempoe';
            let url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
            $('#viewContent').html('');
            $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:98%; margin:8px;"></iframe');
            $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
            // $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
            $('#contenedorAlumnos table').css('pointer-events',"none");
            $('#contenedorAlumnos table').css('opacity',"0.5");
        }
  });
});
</script>