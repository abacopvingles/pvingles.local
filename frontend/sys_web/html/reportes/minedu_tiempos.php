<?php

?>
<style>
.row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

select.select-ctrl{ padding-right: 35px; }
.select-ctrl-wrapper:after{
  right:0!important;
}
.E3-animate-zoom {animation:animatezoom 0.6s}@keyframes animatezoom{from{transform:scale(0)} to{transform:scale(1)}}

.box_custom{ background-color:white; border:1px solid #4683af; border-radius:0.5em; }

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}

</style>

<div class="row" id="search_container">
    <div class="col-md-12">
        <div class="table-responsive box_custom" style="padding: 5px 0;">
            <table class="table" id="tabla01">
                <thead style="background-color:#d3e5fb;">
                    <th><?php echo $this->Entidad;?></th>
                    <th>Tiempo de estudio en la plataforma virtual</th>
                    <th>Tiempo de examen</th>
                    <th>Tiempo del smartbook</th>
                    <th>Tiempo de actividades</th>
                </thead>
                <tbody>
                    <?php
                        if(!empty($this->RowTabla)){
                            foreach($this->RowTabla as $r){
                                echo "<tr>";
                                echo "<td>{$r['nombre']}</td>";
                                echo "<td>{$r['tiempopv']}</td>";
                                echo "<td>{$r['tiempo_exam']}</td>";
                                echo "<td>{$r['tiempo_smartbook']}</td>";
                                echo "<td>{$r['tiempo_task']}</td>";
                                echo "</tr>";
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">

$('document').ready(function(){
    $('#search_container').parent().css('margin','5px 0');
    $('#search_container').parent().css('width','100%');
    $('#tabla01').DataTable({
        "language":{
            "url" : "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        }
    });
});

</script>