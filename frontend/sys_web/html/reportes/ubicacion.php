<?php 
defined("RUTA_BASE") or die(); 
$idgui = uniqid();
$frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
$fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';
// $urlbase = $this->documento->getUrlBase();
$entrada = ($this->entrada) ? 'true' : 'false';
echo "<input type='hidden' id='proyecto' value='{$this->user['idproyecto']}'/>";
echo "<input type='hidden' id='idpersona' value='{$this->user['idpersona']}'/>";
echo "<input type='hidden' id='dni' value='{$this->user['dni']}'/>";
$examenExist = empty($this->examen) ? 'null' : json_encode($this->examen) ;
?>
<style type="text/css">
    .div_menu{
        height: 120px; font-size: 20px; text-align: center;
        padding: 40px;
    }
    .color1{
        background-color: #EB6B56;
    }
    .color2{
        background-color: #2C82C9;
    }
    .color3{
        background-color: #9365B8;
    }
    .color4{
        background-color: #FAC51C;
    }

    .color5{
        background-color: #61BD6D;
    }
    
    .select-ctrl-wrapper:after{
        right:0!important;
        height:30px;
    }
    .table-responsive{
        max-height:200px;
    }
    .circleProgressBar1 div:nth-child(1), #chart_div div:nth-child(1){ margin:0 auto; }
    .text-nota1{ font-size: x-large; font-weight: bold; color: #b73333; }
    .text-custom1 { font-size: large; border-bottom: 1px solid gray; border-radius: 10%; box-shadow: 0px 1px #e20b0b; }
    #tabla_alumnos td{ cursor:pointer; }
    #tabla_alumnos thead { background-color: #839dcc; color: white; font-weight: bold; }
    #tabla_alumnos_filter input { width: 50%; border-radius: 0.8em; font-size: small; padding: 1px 5px; }
    #tabla_alumnos_length { display: none; }
    .select-docente{max-width:300px; font-size:medium!important; font-weight:600; padding-right:35px!important; height:30px;}

.imgr{ -webkit-animation: 3s rotate linear infinite; animation: 3s rotate linear infinite; -webkit-transform-origin: 50% 50%; transform-origin: 50% 50%;
}
@keyframes rotate {from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}}
@-webkit-keyframes rotate {from {-webkit-transform: rotate(0deg);}
  to {-webkit-transform: rotate(360deg);}}
</style>


<div class="container">
    <!-- <div style="position:relative;  margin:10px 0;">
        <a href="<?php echo $this->documento->getUrlBase();?>/reportes" class="btn btn-primary" style="border-radius:0.3em;"><i class="fa fa-backward"></i>&nbsp;<?php echo JrTexto::_("Regresar") ?></a>
    </div> -->
    <div class="row " id="levels" style="padding-top: 1ex; ">
        <div class="col-md-12">
            <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><?php echo JrTexto::_("Home")?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/reportes"><?php echo JrTexto::_("Reports")?></a></li>                  
            <li class="active">
                <?php echo JrTexto::_("Placement Test"); ?>
            </li>
            </ol>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading" style="text-align:left;">
            <?php echo JrTexto::_("Report") ?>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12"><h3><?php echo JrTexto::_('Teacher Reports'); ?></h3></div>
                <div class="col-md-12" style="margin-bottom:5px; ">
                    <div class="row" style="margin:0; border-radius:0.4em; background-color:white; padding: 2px; border: 2px solid #337ab7;">
                        <div class="col-md-3" style="">
                            <div style="height:90px; margin-top:10px;">
                                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" id="foto<?php echo $idgui; ?>" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
                            </div>
                            <div class="" style="text-align:center;">
                                <h4><?php echo $this->user["nombre_full"]; ?></h4>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <?php if(empty($this->examen)): ?>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <?php echo JrTexto::_("Result")?>
                                </div>
                                <div class="panel-body" style="text-align: center;">
                                    <h1><?php echo JrTexto::_('There is not report'); ?></h1>
                                    <h4><?php echo JrTexto::_('Have not taken the placement test'); ?></h4>
                                </div>
                            </div>

                            <?php else: ?>
                            <div class="row" style="text-align:center;">
                                <div class="col-sm-4">
                                    <h4><?php echo JrTexto::_('Percentage Rating'); ?></h4>
                                    <div class="text-nota1">
                                        <?php echo $this->examen['nota']."%"; ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4><?php echo JrTexto::_('Final Score'); ?></h4>
                                    <div class="text-nota1">
                                        <?php echo $this->examen['notaFinal']." pts"; ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <h4><?php echo JrTexto::_('Level Located'); ?></h4>
                                    <div class="text-nota1">
                                        <?php echo $this->examen['nivelUbicado']; ?>
                                    </div>
                                </div>
                                <div class="col-sm-12" style="border-top:1px solid black; padding-bottom:10px; padding-top:10px; margin-top:10px;">
                                    <div class="col-sm-12">
                                        <h4><?php echo JrTexto::_('Achievement'); ?></h4>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="Listening-color">
                                            <p class="text-custom1">Listening</p>
                                            <div id="barListening" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 ">
                                        <div class="Reading-color">
                                            <p class="text-custom1">Reading</p>
                                            <div id="barReading" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 ">
                                        <div class="Writing-color">
                                            <p class="text-custom1">Writing</p>
                                            <div id="barWriting" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 ">
                                        <div class="Speaking-color">
                                            <p class="text-custom1">Speaking</p>
                                            <div id="barSpeaking" class="circleProgressBar1" style="font-size:0.5em;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12"><h3><?php echo JrTexto::_('Students Reports'); ?></h3></div>
                <div class="col-md-12">
                    <div id="info" style="display:block;">
                        <h4 id="infoDre" style="display:inline-block;padding: 0 10px;">DRE:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">Lambayeque</span></h4>
                        <h4 id="infoUgel" style="display:inline-block;padding: 0 10px;">UGEL:<span style="background-color: white;border: 2px solid #4683af;padding: 5px 10px;border-radius: 0.3em;margin-left: 5px;">UGEL Lambayeque</span></h4>
                    </div>
                    <div id="idcolegio-container" style="display: inline-block;">
                        
                        <h4 style="display:inline-block;"><?php echo JrTexto::_('School'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select id="idcolegio" style="width:200px;" class="select-docente form-control select-ctrl ">
                                <?php if(!empty($this->miscolegios)){
                                echo '<option value="0">'.JrTexto::_('Select').'</option>';
                                foreach ($this->miscolegios  as $c) {
                                    if(empty($cursos)) $cursos=$c["cursos"];
                                    echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                                }} ?>
                            </select>
                        </div>
                    </div>
                    <div id="idcurso-container" style="display: inline-block;">
                        <h4 style="display:inline-block;"><?php echo JrTexto::_('Course'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="idcurso" id="idcurso" style="width: 150px;" class="select-docente form-control select-ctrl">
                                <option value="0"><?php JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div id="idgrados-container" style="display: inline-block;">
                        <h4 style="display: inline-block;"><?php echo JrTexto::_('Grades'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="grados" id="idgrados" class="select-docente form-control select-ctrl">
                                <option value="0"><?php JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div id="idseccion-container" style="display: inline-block;">
                        <h4 style="display: inline-block;"><?php echo JrTexto::_('Section'); ?></h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block; ">
                            <select name="seccion" id="idseccion" style="width: 150px;" class="select-docente form-control select-ctrl select-nivel">
                                <option value="0"><?php JrTexto::_('Select'); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--
                <div class="col-md-12" style="margin:10px 0;">
                    <h4 style="display:inline-block; ">Curso</h4>
                    <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                        <select name="select_viewGeneral" id="select_Course" class="select-ctrl select-nivel" style="min-width:220px; height:30px;">
                            <option value="0">Seleccionar</option>
                        <?php
                            if(!empty($this->cursos)){               
                            foreach ($this->cursos as $key => $lista_nivel){
                                $nombre=$lista_nivel['nombre'];
                                $idnivel=$lista_nivel['idcurso'];
                                echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                            }
                            }
                        ?>
                        </select>
                    </div>
                    &nbsp;
                    <div id="selectGrupo" style="display:inline-block;">
                        <h4 style="display:inline-block; ">Grupo</h4>
                        <div class="select-ctrl-wrapper select-azul" style="display:inline-block;">
                            <select name="select_viewGeneral" id="select_Group" class="select-ctrl select-nivel" style="min-width:220px; height:30px;">
                            <?php
                                if(!empty($this->cursos)){               
                                foreach ($this->cursos as $key => $lista_nivel){
                                    $nombre=$lista_nivel['nombre'];
                                    $idnivel=$lista_nivel['idcurso'];
                                    echo '<option value="'.$idnivel.'">'.$nombre.'</option>';
                                }
                                }
                            ?>
                            
                            </select>
                        </div>
                    </div>
                    -->
                </div>
                <div class="col-md-3">
                    <div class="" id="contenedorAlumnos" style="border-radius:0.4em; background-color:#f7f7f7; padding: 2px; border: 2px solid #337ab7;">
                        <h4 style="font-weight: bold; padding: 10px; background-color: #4683af; color: white; margin-top: 0;"><?php echo JrTexto::_('Student'); ?></h4>       
                        <div class="table-responsive">
                            <table id="tabla_alumnos" class="table table-bordered table-hover tablaAlumnos">
                                <thead>
                                    <tr><td><?php echo JrTexto::_('Names'); ?></td></tr>
                                </thead>
                                <!--<tbody>
                                    <tr>
                                        <td>Pepe</td>
                                    </tr>
                                </tbody>-->
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" style="border-radius:0.4em;">
                    <div id="viewContent" style="border-radius:0.4em; background-color:#f7f7f7; border: 2px solid #337ab7; text-align:center;position: relative;">
                        <h1><i class="fa fa-minus-circle fa-3x"></i></h1>
                        <h4><?php echo JrTexto::_('Select Student'); ?></h4>
                    </div>
                </div>
                <div class="col-md-9" style="text-align:right;">
                    <div style="padding:5px 0;"></div>
                    <a href="<?php echo $this->documento->getUrlBase(); ?>/reportes/comparativa_alumno_examenu" class="btn btn-info" id="viewcomparativa" style="right:0;"><?php echo JrTexto::_('View Results by Section'); ?></a>
                </div>

                <!-- <div class="col-md-9" style="text-align:right;">
                    <div style="padding:5px 0;"></div>
                    <button type="button" class="btn btn-info" id="viewcomparativa" style="right:0;">Ver Comparativa de los Alumnos</button>
                </div> -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function RadialProgress(t,i){t.innerHTML="";var e=document.createElement("div");e.style.width="10em",e.style.height="10em",e.style.position="relative",t.appendChild(e),t=e,i||(i={}),this.colorBg=void 0==i.colorBg?"#404040":i.colorBg,this.colorFg=void 0==i.colorFg?"#007FFF":i.colorFg,this.colorText=void 0==i.colorText?"#FFFFFF":i.colorText,this.indeterminate=void 0!=i.indeterminate&&i.indeterminate,this.round=void 0!=i.round&&i.round,this.thick=void 0==i.thick?2:i.thick,this.progress=void 0==i.progress?0:i.progress,this.noAnimations=void 0==i.noAnimations?0:i.noAnimations,this.fixedTextSize=void 0!=i.fixedTextSize&&i.fixedTextSize,this.animationSpeed=void 0==i.animationSpeed?1:i.animationSpeed>0?i.animationSpeed:1,this.noPercentage=void 0!=i.noPercentage&&i.noPercentage,this.spin=void 0!=i.spin&&i.spin,i.noInitAnimation?this.aniP=this.progress:this.aniP=0;var s=document.createElement("canvas");s.style.position="absolute",s.style.top="0",s.style.left="0",s.style.width="100%",s.style.height="100%",s.className="rp_canvas",t.appendChild(s),this.canvas=s;var n=document.createElement("div");n.style.position="absolute",n.style.display="table",n.style.width="100%",n.style.height="100%";var h=document.createElement("div");h.style.display="table-cell",h.style.verticalAlign="middle";var o=document.createElement("div");o.style.color=this.colorText,o.style.textAlign="center",o.style.overflow="visible",o.style.whiteSpace="nowrap",o.className="rp_text",h.appendChild(o),n.appendChild(h),t.appendChild(n),this.text=o,this.prevW=0,this.prevH=0,this.prevP=0,this.indetA=0,this.indetB=.2,this.rot=0,this.draw=function(t){1!=t&&rp_requestAnimationFrame(this.draw);var i=this.canvas,e=window.devicePixelRatio||1;if(i.width=i.clientWidth*e,i.height=i.clientHeight*e,1==t||this.spin||this.indeterminate||!(Math.abs(this.prevP-this.progress)<1)||this.prevW!=i.width||this.prevH!=i.height){var s=i.width/2,n=i.height/2,h=i.clientWidth/100,o=i.height/2-this.thick*h*e/2;h=i.clientWidth/100;if(this.text.style.fontSize=(this.fixedTextSize?i.clientWidth*this.fixedTextSize:.26*i.clientWidth-this.thick)+"px",this.noAnimations)this.aniP=this.progress;else{var a=Math.pow(.93,this.animationSpeed);this.aniP=this.aniP*a+this.progress*(1-a)}(i=i.getContext("2d")).beginPath(),i.strokeStyle=this.colorBg,i.lineWidth=this.thick*h*e,i.arc(s,n,o,-Math.PI/2,2*Math.PI),i.stroke(),i.beginPath(),i.strokeStyle=this.colorFg,i.lineWidth=this.thick*h*e,this.round&&(i.lineCap="round"),this.indeterminate?(this.indetA=(this.indetA+.07*this.animationSpeed)%(2*Math.PI),this.indetB=(this.indetB+.14*this.animationSpeed)%(2*Math.PI),i.arc(s,n,o,this.indetA,this.indetB),this.noPercentage||(this.text.innerHTML="")):(this.spin&&!this.noAnimations&&(this.rot=(this.rot+.07*this.animationSpeed)%(2*Math.PI)),i.arc(s,n,o,this.rot-Math.PI/2,this.rot+this.aniP*(2*Math.PI)-Math.PI/2),this.noPercentage||(this.text.innerHTML=Math.round(100*this.aniP)+" %")),i.stroke(),this.prevW=i.width,this.prevH=i.height,this.prevP=this.aniP}}.bind(this),this.draw()}window.rp_requestAnimationFrame=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.msRequestAnimationFrame||function(t,i){setTimeout(t,1e3/60)},RadialProgress.prototype={constructor:RadialProgress,setValue:function(t){this.progress=t<0?0:t>1?1:t},setIndeterminate:function(t){this.indeterminate=t},setText:function(t){this.text.innerHTML=t}};
</script>

<script type="text/javascript">
var examenExist = <?php echo $examenExist ?>;
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;

function frameload(){
  $('.loading-chart').remove();
  $('#contenedorAlumnos table').css('pointer-events',"initial");
  $('#contenedorAlumnos table').css('opacity',"1");
}

function drawHabilidad(obj , option = null, value = 0, speed = null){
  var _anim = speed === null ? 1 : speed;
  var option = option === null ? {colorFg:"#f44336",thick:10,fixedTextSize:0.3,colorText:"#000000" } : option;
  option.animationSpeed = _anim;
  var bar=new RadialProgress(obj,option);
  bar.setValue(value);
  bar.draw(true);
}

function dibujarDatatable(){
    if ($.fn.DataTable.isDataTable("#tabla_alumnos")) {
        $('#tabla_alumnos').DataTable().clear().destroy();
        // console.log($('#tabla_alumnos').find('tbody'));
    }
    $('#tabla_alumnos').DataTable({
        "paging":   true,
        "ordering": false,
        "info":     false,
        "displayLength": 10,
        "pagingType": "simple"
    });
}
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idcurso').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');
        $('#idgrados').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');
        $('#idseccion').append('<option value="0">\<?php echo JrTexto::_("select");?></option>');              
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}
var nombreselect = function(obj,value){
  var resultado = '';
  $(obj).find('option').each(function(k,v){
    if($(this).attr('value') == value){
      resultado = $(this).text();
    }
  });
  return resultado;
};
$('document').ready(function(){
    $('#info').hide();
  //$('#selectGrupo').hide();
  $('#idcurso-container').hide();
  $('#idseccion-container').hide();
  $('#idgrados-container').hide();

  dibujarDatatable();

  var datos = new Array();
  datos[0] = 0;
  datos[1] = 0;
  datos[2] = 0;
  datos[3] = 0;

  if(examenExist != null){
    // console.log(examenExist);
    var habilidades = JSON.parse(examenExist.habilidad_puntaje);
    habilidades[4] = (typeof habilidades[4] != 'undefined' && habilidades[4] != 0) ? habilidades[4] / 100 : 0;
    habilidades[5] = (typeof habilidades[5] != 'undefined' && habilidades[5] != 0) ? habilidades[5] / 100 : 0;
    habilidades[6] = (typeof habilidades[6] != 'undefined' && habilidades[6] != 0) ? habilidades[6] / 100 : 0;
    habilidades[7] = (typeof habilidades[7] != 'undefined' && habilidades[7] != 0) ? habilidades[7] / 100 : 0;
    drawHabilidad(document.getElementById("barListening"),null, habilidades[4] ,0.1);
    drawHabilidad(document.getElementById("barReading"),null, habilidades[5],0.1);
    drawHabilidad(document.getElementById("barWriting"),null, habilidades[6],0.1);
    drawHabilidad(document.getElementById("barSpeaking"),null, habilidades[7],0.1);
  }

    $('#idcolegio').change(function(){
        //change width
        var texto = nombreselect('#idcolegio',$('#idcolegio').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcolegio').val() != 0){
            $('#idcurso-container').show();
            var idcolegio=$(this).val()||'';
            for (var i = 0; i < datoscurso.length; i++) {
              if(datoscurso[i].idlocal==idcolegio){              
                    datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
                  actualizarcbo($(this),datoscurso[i].cursos);
              }
            }
            //buscar informacion del colegio...
            $.ajax({
                url: _sysUrlBase_+'/local/infolocal',
                type: 'POST',
                dataType: 'json',
                data: {'idlocal': $('#idcolegio').val()},
            }).done(function(resp) {
                if(resp.code=='ok'){
                    if(Object.keys(resp.data).length > 0){
                        var dre = null;
                        if(resp.data[0].dre == null || resp.data[0].dre == 0){
                            dre = resp.data[0].departamento;
                        }else{
                            dre = resp.data[0].dre;
                        }
                        $('#info').find('#infoDre').find('span').text(dre);
                        $('#info').find('#infoUgel').find('span').text(resp.data[0].ugel);
                    }
                } else {
                    return false;
                }
            })
            .fail(function(xhr, textStatus, errorThrown) {
                return false;
            });
            $('#info').show();
        }else{
            $('#info').hide();
        }
      });
      $('#idcurso').change(function(){
        //change width
        var texto = nombreselect('#idcurso',$('#idcurso').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idcurso').val() != 0){
            $('#idgrados-container').show();
            var idcurso=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;                
                    actualizarcbo($(this),grados);
                }
        }
      });
      $('#idgrados').change(function(){
        //change width
        var texto = nombreselect('#idgrados',$('#idgrados').val());
        // console.log(texto);
        $(this).css('width',(texto.length + 200)+'px');
        if($('#idgrados').val() != 0){
            $('#idseccion-container').show();
            var idgrados=$(this).val()||'';
            var idcolegio=$('#idcolegio').val()||'';
            var idcurso=$('#idcurso').val()||'';
            for (var i = 0; i < datoscurso.length; i++)
            if(datoscurso[i].idlocal==idcolegio)
            for (var j = 0; j < datoscurso[i].cursos.length; j++)
                if(datoscurso[i].cursos[j].idcurso==idcurso){
                    var grados=datoscurso[i].cursos[j].grados;
                    for (var h = 0; h < grados.length; h++){
                        if(grados[h].idgrado==idgrados){
                            var secciones=grados[h].secciones;
                            secciones.sort(function(a, b){return a.seccion - b.seccion;});
                            actualizarcbo($(this),secciones);
                        }
                    }
                    
                }
        }
      });
  /*
  $('#select_Course').change(function(){
    $('#selectGrupo').show();
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupo',
        type: 'POST',
        dataType: 'json',
        data: {'idcurso': $('#select_Course').val(), 'idproyecto': $('#proyecto').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            let options = '<option val="0">Seleccionar</option>';
            console.log(resp.data.grupos.length);
            for (var i = resp.data.grupos.length - 1; i >= 0; i--) {
                options += '<option value="'+resp.data.grupos[i].id+'">'+resp.data.grupos[i].nombre+'</option>';
            }
            $('#select_Group').html(options);
        } else {
            $('#select_Group').html('<option value="0">No hay grupos asignados</option>');
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
  });
  */
  $('#idseccion').change(function(){
    //change width
    var texto = nombreselect('#idseccion',$('#idseccion').val());
    // console.log(texto);
    $(this).css('width',(texto.length + 200)+'px');
      // alert($(this).val());
    $.ajax({
        url: _sysUrlBase_+'/reportes/listaGrupoAlumnos',
        type: 'POST',
        dataType: 'json',
        data: {'idgrupoaula': $(this).val(), 'idproyecto': $('#proyecto').val()},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var alumnos = $('<tr></tr>');
            var filas = '';
            var tabla_tmp = $('<table></table>');
            tabla_tmp.attr('id','tabla_alumnos');
            tabla_tmp.addClass('table table-bordered table-hover tablaAlumnos');
            tabla_tmp.append('<thead><tr><td>\<?php echo JrTexto::_("Names"); ?></td></tr></thead><tbody></tbody>');
            console.log(resp.data.alumnos);
            for (var i = resp.data.alumnos.length - 1; i >= 0; i--) {
                //data-dni="'+resp.data.alumnos[i].dni+'"
                filas = filas.concat('<tr data-id="'+resp.data.alumnos[i].id+'" data-dni="'+resp.data.alumnos[i].dni+'" ><td>'+resp.data.alumnos[i].nombre+'</td></tr>');        
            }
            tabla_tmp.find('tbody').html(filas);

            $('#tabla_alumnos_wrapper').remove();
            $('#contenedorAlumnos').append(tabla_tmp);
            dibujarDatatable();
        } else {
            return false;
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
        return false;
    });
  });

  $('#contenedorAlumnos').on('click','.tablaAlumnos tbody tr',function(){
      if($(this).data('id')){
          var controllerAndFunction = '/reportealumno/ubicacion';
          var url = _sysUrlBase_ + controllerAndFunction +'?isIframe=1&idalumno='+$(this).data('id')+'&idproyecto='+$('#proyecto').val()+'&plt=sintop2';
          $('#viewContent').html('');
          $('#viewContent').append('<iframe onload="frameload();" src="'+url+'" style="min-height:600px; position:relative; width:95%; margin:8px;"></iframe');
          $('#viewContent').append('<div class="loading-chart" style="background:black;position:absolute; top:0; width:100%; height:100%;"><img style="margin:0 auto; position:relative; top:50%;" src="'+_sysUrlBase_+'/static/tema/css/images/cargando.gif" class="img-responsive"></div>');
          $('#contenedorAlumnos table').css('pointer-events',"none");
          $('#contenedorAlumnos table').css('opacity',"0.5");
        //   $('#viewContent').append('<h1 class="loading-chart" style="position:absolute;top:0;font-size:24px; width:100%; text-align:center; margin-top:25px;"><i class="fa fa-cog imgr" aria-hidden="true" style="font-size:3em;"></i><span style="width:100%; display:block;">Loading... <span class="nameMenu"></span></span></h1>');
      }
  });

});


</script>