<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) {
  $frm=$this->datos;
  $this->orden=@$frm["orden"];
}
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Niveles'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdnivel" id="pkidnivel" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <input type="hidden" name="txtTipo" value="N">
          <input type="hidden" name="txtIdpadre" value="0">
          <input type="hidden" name="txtOrden" value="<?php echo $this->orden; ?>">
          <div class="form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
            <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
            </label>
            <div class="col-md-8 col-sm-8 col-xs-12">
              <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
            </div>
          </div>          

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                 <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtEstado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
              </div>
            </div>
            
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtImagen">
              <?php echo ucfirst(JrTexto::_('Image'));
                $ruta=!empty($frm["imagen"])?$frm["imagen"]:$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
                $ruta=str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$ruta);
              ?><span class="required">* :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 text-left" style="max-height:180px; overflow: hidden;">
                 <a class="biblioteca-open btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a image from our multimedia library or upload your own image')) ?>" data-tipo="image" data-url=".imgtemporal" style="position: absolute;top: 0px;"><i class="fa fa-picture-o"></i> <?php echo ucfirst(JrTexto::_('Change image')); ?></a><img src="<?php echo $ruta; ?> " class="imgtemporal img-responsive" width="50%"  >
                 <input type="hidden" name="imagen" value="<?php echo $ruta; ?>" class="imagentemporalval">
              </div>
              
            </div>
            <div class="clearfix"></div>

          <div class="form-group">
            <br>
            <div class="col-md-12 text-center">
              <button id="btn-saveNiveles" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('niveles'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  var cambiarruta=function(){
    $('input.imagentemporalval').val($('img.imgtemporal').attr('src'));
  }
$(document).ready(function(){
  $('.biblioteca-open').click(function(e) {
    e.preventDefault();
    var txt= '<?php echo JrTexto::_('Select or upload'); ?>';
    selectedfile(e,this,txt,'cambiarruta');
  });
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'niveles', 'saveNiveles', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Niveles"))?>');
      }
     }
  });

});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });
</script>

