<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Personal"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                                                                                                                                                                                                                                                                                                                                                                                  
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Personal", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Tipodoc") ;?></th>
                    <th><?php echo JrTexto::_("Dni") ;?></th>
                    <th><?php echo JrTexto::_("Ape paterno") ;?></th>
                    <th><?php echo JrTexto::_("Ape materno") ;?></th>
                    <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Fechanac") ;?></th>
                    <th><?php echo JrTexto::_("Sexo") ;?></th>
                    <th><?php echo JrTexto::_("Estado civil") ;?></th>
                    <th><?php echo JrTexto::_("Ubigeo") ;?></th>
                    <th><?php echo JrTexto::_("Urbanizacion") ;?></th>
                    <th><?php echo JrTexto::_("Direccion") ;?></th>
                    <th><?php echo JrTexto::_("Telefono") ;?></th>
                    <th><?php echo JrTexto::_("Celular") ;?></th>
                    <th><?php echo JrTexto::_("Email") ;?></th>
                    <th><?php echo JrTexto::_("Idugel") ;?></th>
                    <th><?php echo JrTexto::_("Regusuario") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Usuario") ;?></th>
                    <th><?php echo JrTexto::_("Clave") ;?></th>
                    <th><?php echo JrTexto::_("Token") ;?></th>
                    <th><?php echo JrTexto::_("Rol") ;?></th>
                    <th><?php echo JrTexto::_("Foto") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Situacion") ;?></th>
                    <th><?php echo JrTexto::_("Idioma") ;?></th>
                    <th><?php echo JrTexto::_("Tipousuario") ;?></th>
                    <th><?php echo JrTexto::_("Idlocal") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5bd3e68a32d4a='';
function refreshdatos5bd3e68a32d4a(){
    tabledatos5bd3e68a32d4a.ajax.reload();
}
$(document).ready(function(){  
  var estados5bd3e68a32d4a={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5bd3e68a32d4a='<?php echo ucfirst(JrTexto::_("personal"))." - ".JrTexto::_("edit"); ?>';
  var draw5bd3e68a32d4a=0;

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5bd3e68a32d4a();
  });
  tabledatos5bd3e68a32d4a=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Tipodoc") ;?>'},
            {'data': '<?php echo JrTexto::_("Dni") ;?>'},
            {'data': '<?php echo JrTexto::_("Ape_paterno") ;?>'},
            {'data': '<?php echo JrTexto::_("Ape_materno") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Fechanac") ;?>'},
            {'data': '<?php echo JrTexto::_("Sexo") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado_civil") ;?>'},
            {'data': '<?php echo JrTexto::_("Ubigeo") ;?>'},
            {'data': '<?php echo JrTexto::_("Urbanizacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
            {'data': '<?php echo JrTexto::_("Telefono") ;?>'},
            {'data': '<?php echo JrTexto::_("Celular") ;?>'},
            {'data': '<?php echo JrTexto::_("Email") ;?>'},
            {'data': '<?php echo JrTexto::_("Idugel") ;?>'},
            {'data': '<?php echo JrTexto::_("Regusuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Regfecha") ;?>'},
            {'data': '<?php echo JrTexto::_("Usuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Clave") ;?>'},
            {'data': '<?php echo JrTexto::_("Token") ;?>'},
            {'data': '<?php echo JrTexto::_("Rol") ;?>'},
            {'data': '<?php echo JrTexto::_("Foto") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            {'data': '<?php echo JrTexto::_("Situacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Idioma") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipousuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Idlocal") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/personal/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5bd3e68a32d4a=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5bd3e68a32d4a;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Tipodoc") ;?>': data[i].tipodoc,
              '<?php echo JrTexto::_("Dni") ;?>': data[i].dni,
              '<?php echo JrTexto::_("Ape_paterno") ;?>': data[i].ape_paterno,
              '<?php echo JrTexto::_("Ape_materno") ;?>': data[i].ape_materno,
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Fechanac") ;?>': data[i].fechanac,
              '<?php echo JrTexto::_("Sexo") ;?>': data[i].sexo,
              '<?php echo JrTexto::_("Estado_civil") ;?>': data[i].estado_civil,
              '<?php echo JrTexto::_("Ubigeo") ;?>': data[i].ubigeo,
              '<?php echo JrTexto::_("Urbanizacion") ;?>': data[i].urbanizacion,
              '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
              '<?php echo JrTexto::_("Telefono") ;?>': data[i].telefono,
              '<?php echo JrTexto::_("Celular") ;?>': data[i].celular,
              '<?php echo JrTexto::_("Email") ;?>': data[i].email,
              '<?php echo JrTexto::_("Idugel") ;?>': data[i].idugel,
              '<?php echo JrTexto::_("Regusuario") ;?>': data[i].regusuario,
              '<?php echo JrTexto::_("Regfecha") ;?>': data[i].regfecha,
              '<?php echo JrTexto::_("Usuario") ;?>': data[i].usuario,
              '<?php echo JrTexto::_("Clave") ;?>': data[i].clave,
              '<?php echo JrTexto::_("Token") ;?>': data[i].token,
              '<?php echo JrTexto::_("Rol") ;?>': data[i].rol,
              '<?php echo JrTexto::_("Foto") ;?>': data[i].foto,
              '<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
              '<?php echo JrTexto::_("Situacion") ;?>': data[i].situacion,
              '<?php echo JrTexto::_("Idioma") ;?>': data[i].idioma,
              '<?php echo JrTexto::_("Tipousuario") ;?>': data[i].tipousuario,
              '<?php echo JrTexto::_("Idlocal") ;?>': data[i].idlocal,
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/editar/?id='+data[i].idpersona+'" data-titulo="'+tituloedit5bd3e68a32d4a+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idpersona+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'personal', 'setCampo', id,campo,data);
          if(res) tabledatos5bd3e68a32d4a.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd3e68a32d4a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'personal', 'eliminar', id);
        if(res) tabledatos5bd3e68a32d4a.ajax.reload();
      }
    }); 
  });
});
</script>