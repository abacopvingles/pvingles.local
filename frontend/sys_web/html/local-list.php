<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("IIEE"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
         <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="select-ctrl-wrapper select-azul">
                <select id="idDre" class="form-control select-ctrl " name="idDre">
                <?php if(!empty($this->dress))
                foreach($this->dress as $dre){
                  $txtdre=$this->iddress==$dre["ubigeo"]?'selected="selected"':'';?>
                  <option value="<?php echo $dre["ubigeo"]; ?>" <?php echo $txtdre;?> ><?php echo $dre["descripcion"]; ?></option>
               <?php  }   ?> 
                </select>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="select-ctrl-wrapper select-azul">
                <select id="idUgel" class="form-control select-ctrl " name="idUgel">
                  <?php if(!empty($this->ugeles))
                    foreach($this->ugeles as $ugel){
                      $txtidugel=$this->idugel==$ugel["idugel"]?'selected="selected"':'';
                      ?>
                    <option value="<?php echo $ugel["idugel"]; ?>" <?php echo $txtidugel; ?> ><?php echo $ugel["descripcion"]; ?></option>
                  <?php  }   ?> 
                </select>
            </div>
         </div>      
                                                                                                                                          
            <div class="col-xs-12 col-sm-6 col-md-3">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-center">
               <a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("local", "agregar"));?>" data-titulo="<?php echo JrTexto::_("IIEE").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("local", "importar"));?>" data-titulo="<?php echo JrTexto::_("Importar"); ?>"><i class="fa fa-upload"></i> <?php echo JrTexto::_('Importar')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Direccion") ;?></th>
                    <th><?php echo JrTexto::_("Id ubigeo") ;?></th>                 
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5bd29a8ad0d4e='';
function refreshdatos5bd29a8ad0d4e(){
    tabledatos5bd29a8ad0d4e.ajax.reload();
}
$(document).ready(function(){  
  var estados5bd29a8ad0d4e={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5bd29a8ad0d4e='<?php echo ucfirst(JrTexto::_("IIEE"))." - ".JrTexto::_("edit"); ?>';
  var draw5bd29a8ad0d4e=0;

  $('.btnbuscar').click(function(ev){
    refreshdatos5bd29a8ad0d4e();
  });
  $('#idUgel').change(function(ev){
    refreshdatos5bd29a8ad0d4e();
  })
  $('#idDre').change(function(ev){
    var iddre=$(this).val();
    var fd = new FormData();
        fd.append("iddepartamento", iddre);
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/ugel/buscarjson',
      callback:function(rs){
        console.log(rs);
        $sel=$('#idUgel');
          dt=rs.data;
          $sel.html('');
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.idugel+'" '+(v.ubigeo==iddre?'selected="selected"':'')+ '>'+v.descripcion+'</option>');
          })
          $sel.trigger('change');
      }})
  });
  tabledatos5bd29a8ad0d4e=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
        {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
        {'data': '<?php echo JrTexto::_("Id_ubigeo") ;?>'},
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/local/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.texto=$('#texto').val(),
            d.idugel=$('#idUgel').val(),
            draw5bd29a8ad0d4e=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5bd29a8ad0d4e;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
              '<?php echo JrTexto::_("Id_ubigeo") ;?>': data[i].id_ubigeo,
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/local/editar/?id='+data[i].idlocal+'" data-titulo="'+tituloedit5bd29a8ad0d4e+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idlocal+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'local', 'setCampo', id,campo,data);
          if(res) tabledatos5bd29a8ad0d4e.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd29a8ad0d4e';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'IIEE';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{footer:false,borrarmodal:true});
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'local', 'eliminar', id);
        if(res) tabledatos5bd29a8ad0d4e.ajax.reload();
      }
    }); 
  });
});
</script>