<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$ventanapadre=$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
$datareturn=!empty($_REQUEST["datareturn"])?$_REQUEST["datareturn"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <?php if($this->documento->plantilla!='mantenimientos-out'){?>
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>       
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php }else{?>
          <li><a href="javascript:history.back()"><i class="fa fa-reply"></i>&nbsp;<?php echo JrTexto::_("Atras"); ?></a></li>
          <li class="active"><?php echo JrTexto::_('Matriculas'); ?></li>
        <?php } ?>             
    </ol>
  </div>
</div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">
         <form id="frmEstudiantes<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">
          <input type="hidden" name="fkcbrol" value="3">
          <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3 form-group" >
              <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label>
              <div class="cajaselect">
                <select id="fkcbdre" name="dre" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                    <?php 
                    if(!empty($this->dress))
                    foreach ($this->dress as $fk) { ?><option value="<?php echo $fk["ubigeo"]?>" ><?php echo $fk["descripcion"] ?></option><?php } ?>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
              <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
              <div class="cajaselect">
                <select id="fkcbugel" name="ugel" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group"  >
              <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
              <div class="cajaselect">
                <select id="fkcbiiee" name="iiee" class="form-control" >
                  <option value=""><?php echo JrTexto::_('All'); ?></option>
                </select>
              </div>
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-3 form-group" >
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select id="fkcbcurso" name="curso" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                    <?php if(!empty($this->fkcursos))
                    foreach ($this->fkcursos as $fk) { ?><option value="<?php echo $fk["idcurso"]?>" ><?php echo $fk["nombre"] ?></option><?php } ?>
                </select>
              </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
            <label><?php echo JrTexto::_('Gender'); ?></label>
            <div class="cajaselect">
              <select id="fkcbsexo" name="sexo" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
            <label><?php echo JrTexto::_('Grade'); ?></label>
            <div class="cajaselect">
              <select id="fkcbsexo" name="sexo" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
            <label><?php echo JrTexto::_('Section'); ?></label>
            <div class="cajaselect">
              <select id="fkcbsexo" name="sexo" class="form-control" >
                <option value=""><?php echo JrTexto::_('All'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>
          
            
            <div class="col-xs-6 col-sm-4 col-md-3 form-group">
              <label><?php echo ucfirst(JrTexto::_('State'));?></label>
              <div class="cajaselect">
                <select name="estado" id="cbestado" class="form-control">
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
                </select>
              </div>
            </div>

            
            
            <div class="col-xs-12 col-sm-6 col-md-6 form-group">
              <label><?php echo  ucfirst(JrTexto::_("Text to search"))?></label>
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 text-center pnlnodocente<?php echo $idgui;?>"><br>
              <!--a class="btn btn-warning btnchangevista<?php echo $idgui; ?>" href="#" data-titulo="<?php echo JrTexto::_("Change").' - '.JrTexto::_("View"); ?>">
                <i class="fa fa-drivers-license-o"></i> </a-->
                <a class="btn btn-warning btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("personal", "formulario"))."?return=false&accion=add&plt=".$this->documento->plantilla;?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
                <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php echo JrTexto::_("Importar"); ?>"><i class="fa fa-upload"></i> <?php echo JrTexto::_('Importar')?></a>
                <!--a class="btn btn-primary btnvermodal" data-modal="si" href="<?php //echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php //echo JrTexto::_("Personal").' - '.JrTexto::_("import"); ?>"><i class="fa fa-cloud-upload"></i> <?php //echo JrTexto::_('Import')?></a-->
              </div>
            </div>
            </div>
          </form>
      </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 pnlvista" id="vista<?php echo $idgui; ?>">        
    </div>
    <script type="text/javascript">
      var vista=window.localStorage.vistapnl||1;
      var cargarvista<?php echo $idgui; ?>=function(view){
        var _vista=view||vista||1;
        var frmtmp=document.getElementById('frmpersonal<?php echo $idgui; ?>');
        var formData = new FormData(frmtmp);
        formData.append('vista',_vista);
        var data={
          fromdata:formData,
          url:_sysUrlBase_+'/personal/listado',
          msjatencion:'<?php echo JrTexto::_('Attention');?>',
          type:'html',
          callback:function(dt){$('#vista<?php echo $idgui; ?>').html(dt);}
        }
        sysajax(data);
      }
      cargarvista<?php echo $idgui; ?>(vista);
      $(document).ready(function(){
        $('.btnchangevista<?php echo $idgui; ?>').click(function(){
          var _vista=window.localStorage.vistapnl||1;
          vista=_vista==1?2:1;
          window.localStorage.setItem('vistapnl',vista);
          cargarvista<?php echo $idgui; ?>(vista);
        });
      })
    </script>
    </div>
</div>

























<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >
      <div class="panel-body">
         <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Acad_matricula", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Acad_matricula").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idgrupoauladetalle") ;?></th>
                    <th><?php echo JrTexto::_("Idalumno") ;?></th>
                    <th><?php echo JrTexto::_("Fecha registro") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Idusuario") ;?></th>
                    <th><?php echo JrTexto::_("Fecha matricula") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5bd4788e236d6='';
function refreshdatos5bd4788e236d6(){
    tabledatos5bd4788e236d6.ajax.reload();
}
$(document).ready(function(){  
  var estados5bd4788e236d6={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5bd4788e236d6='<?php echo ucfirst(JrTexto::_("acad_matricula"))." - ".JrTexto::_("edit"); ?>';
  var draw5bd4788e236d6=0;

  
  $('.btnbuscar').click(function(ev){
    refreshdatos5bd4788e236d6();
  });
  tabledatos5bd4788e236d6=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        {'data': '<?php echo JrTexto::_("Idgrupoauladetalle") ;?>'},
            {'data': '<?php echo JrTexto::_("Idalumno") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha_registro") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            {'data': '<?php echo JrTexto::_("Idusuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Fecha_matricula") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/acad_matricula/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             //d.texto=$('#texto').val(),
                        
            draw5bd4788e236d6=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5bd4788e236d6;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idgrupoauladetalle") ;?>': data[i].idgrupoauladetalle,
              '<?php echo JrTexto::_("Idalumno") ;?>': data[i].idalumno,
              '<?php echo JrTexto::_("Fecha_registro") ;?>': data[i].fecha_registro,
              '<?php echo JrTexto::_("Estado") ;?>': data[i].estado,
              '<?php echo JrTexto::_("Idusuario") ;?>': data[i].idusuario,
              '<?php echo JrTexto::_("Fecha_matricula") ;?>': data[i].fecha_matricula,
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/acad_matricula/editar/?id='+data[i].idmatricula+'" data-titulo="'+tituloedit5bd4788e236d6+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idmatricula+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'acad_matricula', 'setCampo', id,campo,data);
          if(res) tabledatos5bd4788e236d6.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5bd4788e236d6';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Acad_matricula';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'acad_matricula', 'eliminar', id);
        if(res) tabledatos5bd4788e236d6.ajax.reload();
      }
    }); 
  });
});
</script>