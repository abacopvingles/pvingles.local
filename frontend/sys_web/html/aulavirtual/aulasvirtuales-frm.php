<?php $idgui = uniqid();
if(!empty($this->datos)) {
  $aula=$this->datos;
  $portada=$aula["portada"];
  if(!empty($portada))
    $portada=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada);
  else
    $portada=$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
}
else {
  $aula=null;
  $portada=$this->documento->getUrlStatic().'/media/imagenes/levels/nofoto.jpg';
}
 ?>
 <style type="text/css">
  .border0{
    border: 0px;
  }
  .input-group-addon{
    border-radius: 0.2ex;
  }
  .border1{
    margin-top:0px; 
    border:2px solid #4683af;
    border-radius: 0.25ex;
  }
  hr{
        margin-top: 5px; 
    margin-bottom: 5px; 
  }

  img.img-portada{
    display: block;
    margin-left: auto;
    margin-right: auto;
    min-height: 100px;
  }
</style>
<form method="post" id="frm-<?php echo $idgui;?>" onsubmit="return false;">
<input type="hidden" name="idaula" id="idaula" value="<?php echo @$aula["aulaid"]; ?>">
<div class="row">
  <div class="panel panel-primary" style="margin-top: 1ex;">
    <div class="panel-heading" style="overflow: hidden;">
    <ol class="breadcrumb pull-left" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
      <li><a href="<?php echo $this->documento->getUrlBase() ?>/aulasvirtuales/" style="color:#fff"><?php echo ucfirst(JrTexto::_("Smartclass")); ?></a></li>                  
      <li class="active"  style="color:#ccc"><?php echo JrTexto::_("Setting"); ?></li>
        </ol>
    <div class="pull-right"><a href="#" class="btn btn-xs btn-default showdemo" data-videodemo="settings.mp4"><i class="fa fa-question-circle"></i> <?php echo JrTexto::_("Help"); ?></a></div>
      </div>
    <div class="panel-body">      
      <div class="col-xs-12 col-sm-6 col-md-6" style="padding: 0px;">
        <div class="panel-body">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="titulo"><?php echo ucfirst(JrTexto::_("Title")); ?>:</label>
                <input type="text" class="form-control gris" id="titulo" name="titulo" placeholder="<?php echo ucfirst(JrTexto::_("add title")); ?>" value="<?php echo @$aula["titulo"] ?>">
            </div>              
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="descripcion"><?php echo ucfirst(JrTexto::_("Description")); ?>:</label>
                <textarea name="descripcion" rows="6" class="form-control gris" name="descripcion" placeholder="<?php echo ucfirst(JrTexto::_("Add Description")); ?>"><?php echo @$aula["descripcion"] ?></textarea>
            </div>              
          </div>          
        </div>
      </div>
      <div class="panel col-xs-12 col-sm-6 col-md-6" style="padding: 0px;">
        <div class="panel-body">          
          <div class="col-xs-12 col-sm-12 col-md-12">
            <label for="titulo" class="text-center" style="display: block;"><?php echo ucfirst(JrTexto::_("cover")); ?>:</label>
            <div style="text-align: center; max-height: 135px; overflow: hidden;" >           
              <img class="img-portada img-responsive img-thumbnails istooltip" src="<?php echo $portada ?>" width="170px" data-tipo="image" data-url=".img-portada" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>" >
              <input type="hidden" name="portada" value="<?php echo $portada ?>">
              </div>
              <div class="clearfix"></div>             
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12"> 
            <hr>           
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group"><label for="titulo" class="text-left"><?php echo  ucfirst(JrTexto::_("date first"))?>:</label>  
                    <div class='input-group date datetimepicker border1' >
                      <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>  </span>
                        <input type='text' class="form-control border0" name="fecha_inicio" value="<?php echo @$aula["fecha_inicio"]; ?>" />           
                    </div>
                </div>  
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group"><label for="titulo" class="text-left"><?php echo  ucfirst(JrTexto::_("date finish"))?>:</label>
                      <div class='input-group date datetimepicker border1'>
                        <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>  </span>
                          <input type='text' class="form-control border0"  name="fecha_final" value="<?php echo @$aula["fecha_final"]; ?>" />
                      </div>
                  </div>
              </div>              
          </div>                    
        </div>
      </div>
      <div class="panel col-xs-12 col-sm-12 col-md-12">
      <div class="panel-body">
      <fieldset>
        <legend style="font-size: 12px;">
          <div class="col-xs-12 col-sm-4 col-md-4">          
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Directed to"))?> :</b></div>
                <div class="cajaselect "> 
                <select name="dirigidoa" id="dirigidoa" class="conestilo">
                    <option value="A" <?php echo @$aula["dirigidoa"]=='A'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("My students"))?></option>
                    <option value="I" <?php echo @$aula["dirigidoa"]=='I'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("My Guests"))?></option>
                    <option value="P" <?php echo @$aula["dirigidoa"]=='P'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("General public"))?></option>
                </select>
                </div>           
              </div>
            </div>          
          </div>
          <div class="col-xs-12 col-sm-5 col-md-4">          
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Maximum number of participants"))?> :</b></div>
               
                <input type="text" class="form-control" name="nparticipantes" id="nparticipantes" value="<?php echo !empty($aula["nparticipantes"])?$aula["nparticipantes"]:30; ?>">
                        
              </div>
            </div>          
          </div>
          <div class="col-xs-12 col-sm-3 col-md-4">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo  ucfirst(JrTexto::_("State"))?> :</b></div>
                <div class="cajaselect "> 
                <select name="estado" id="activity-item" class="conestilo">
                  <option value="0" <?php echo @$aula["estado"]=='0'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("Active"))?></option>        
                  <option value="AB" <?php echo @$aula["estado"]=='AB'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("Opened"))?></option>
                  <option value="CL" <?php echo @$aula["estado"]=='CL'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("Closed"))?></option>
                  <option value="BL" <?php echo @$aula["estado"]=='BL'?'Selected="selected"':''; ?> ><?php echo ucfirst(JrTexto::_("Locked"))?></option>
                </select>
                </div>           
              </div>
            </div>            
          </div>                   
          <div class="clearfix"></div></legend>
        <div class="pnlestudiantes">
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo ucfirst(JrTexto::_("Level"))?></label>
            <div class="cajaselect">
              <select name="nivel" id="level-item" class="conestilo">
                <option value="" ><?php echo ucfirst(JrTexto::_("All Levels"))?></option>
                <?php if(!empty($this->niveles))
                       foreach ($this->niveles as $nivel){?>
                       <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
                  <?php }?>
                </select>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo  ucfirst(JrTexto::_("Unit"))?></label>
            <div class="cajaselect"> 
            <select name="unidad" id="unit-item" class="conestilo">
              <option value="" ><?php echo ucfirst(JrTexto::_("All Unit"))?></option>
              <?php if(!empty($this->unidades))
                       foreach ($this->unidades as $unidad){?>
                       <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
                <?php }?>
            </select>
            </div>
          </div>
          <div class="col-xs-6 col-sm-4 col-md-3">
          <label><?php echo  ucfirst(JrTexto::_("Activity"))?></label>
          <div class="cajaselect"> 
            <select name="actividad" id="activity-item" class="conestilo">
                <option value="" ><?php echo ucfirst(JrTexto::_("All Activity"))?></option>
                 <?php if(!empty($this->actividades))
                         foreach ($this->actividades as $act){?>
                         <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
                  <?php }?>
            </select>
          </div>
          </div>          
          <div class="clearfix"></div><br>
            <div class="col-xs-6 col-sm-3 form-group select-ctrl-wrapper select-azul">
                <select name="opcColegio" id="opcColegio" class="form-control select-ctrl ">
                    <option value="-1">- <?php echo JrTexto::_('Select School'); ?>-</option>
                    <?php foreach ($this->locales as $l){                     
                      if(!empty($aula["filtroestudiantes"])){;
                      $filtroestudiantes= json_decode($aula["filtroestudiantes"]);
                      $idlocal= $filtroestudiantes->colegio;                    
                      }
                      ?>
                    <option value="<?php echo $l['idlocal']?>" <?php echo @$idlocal==$l['idlocal']?'Selected="selected"':''; ?>><?php echo $l['nombre']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-xs-6 col-sm-3 form-group select-ctrl-wrapper select-azul">
                <select name="opcAula" id="opcAula" class="form-control select-ctrl ">
                    <option value="-1">- <?php echo JrTexto::_('Select'); ?> Smartclass-</option>
                </select>
            </div>
            <div class="col-xs-6 col-sm-3 form-group select-ctrl-wrapper select-azul">
                <select name="opcGrupo" id="opcGrupo" class="form-control select-ctrl ">
                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                </select>
            </div>
            <div class="col-xs-6 col-sm-3 form-group select-ctrl-wrapper select-azul">
                <select name="opcAlumno" id="opcAlumno" class="form-control select-ctrl ">
                    <option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>
                </select>
            </div>
          </div>
        </div>
        <input type="hidden" name="filtroestudiantes" id="filtroestudiantes" value="" class="form-control">
      </fieldset>
      </div>
      </div>
      <hr>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center"><br>
          <button class="btn btn-danger btneliminaraula hidden" data-idaula="<?php echo @$aula["aulaid"]; ?>"><i class="fa fa-save"></i> <?php echo JrTexto::_('Remove'); ?> Smartclass</button>
          <button class="btn btn-primary btnsaveaula"><i class="fa fa-save"></i> <?php echo JrTexto::_('save and invite participants'); ?></button>          
        </div>      
    </div>  
  </div>
</div>
</form>
<script type="text/javascript">
  $(document).ready(function(){
    
    $('.istooltip').tooltip();
//recargar combos de niveles 
    var leerniveles=function(data){
          try{
              var res = xajax__('', 'niveles', 'getxPadre', data);
              if(res){ return res; }
              return false;
          }catch(error){
              return false;
          }       
      }
      var addniveles=function(data,obj){
        var objini=obj.find('option:first').clone();
        obj.find('option').remove();
        obj.append(objini);
        if(data!==false){
          var html='';
          $.each(data,function(i,v){
            html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
          });
          obj.append(html);
        }
        id=obj.attr('id');
        //if(id==='activity-item')  cargaraulaes();
      }

      $('#level-item').change(function(){
        var idnivel=$(this).val();
            var data={tipo:'U','idpadre':idnivel}
            var donde=$('#unit-item');
            if(idnivel!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
            donde.trigger('change');
      });
      $('#unit-item').change(function(){
        var idunidad=$(this).val();
            var data={tipo:'L','idpadre':idunidad}
            var donde=$('#activity-item');
            if(idunidad!=='') addniveles(leerniveles(data),donde);
            else addniveles(false,donde);
      });

      $('.img-portada').click(function(e){ 
          var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
          selectedfile(e,this,txt);
      });

      $('.img-portada').load(function() {
          var src = $(this).attr('src');
          if(src!=''){
              $(this).siblings('input').val(src);
          }
      });

      $('#dirigidoa').change(function(){
        console.log("aaa");
        if($('#dirigidoa').val()=='A') $('.pnlestudiantes').show();
        else $('.pnlestudiantes').hide();
      });
      $('#dirigidoa').trigger('change');

     
      $('.btnsaveaula').click(function(){
        guardaraula();
      });

      <?php if(!empty($aula)) { ?>
        $('.btneliminaraula').removeClass('hidden');
      <?php } ?>

      $('.btneliminaraula').click(function(){
        try{
          var idaula=$(this).attr('data-idaula')||-1;
              var res = xajax__('', 'aulasvirtuales', 'eliminar', parseInt(idaula));
              if(res)  redir('<?php echo $this->documento->getUrlBase() ?>/aulasvirtuales/');
          }catch(error){
              
          }
      });

      var guardaraula=function(){
              var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
              var IDGUI = '<?php echo $idgui; ?>';
              var formData = new FormData($("#frm-"+IDGUI)[0]);
              $.ajax({
                url: _sysUrlBase_+'/aulasvirtuales/guardar',
                type: "POST",
                data:  formData,
                contentType: false,
                dataType :'json',
                cache: false,
                processData:false,
                beforeSend: function(XMLHttpRequest){
                   $('#procesando').show('fast'); 
                },
                success: function(data)
                {
                  if(data.code==='ok'){
                    mostrar_notificacion(msjeAttention, data.msj, 'success');
                    $('#idaula').val(data.new);
                    setTimeout(redir(_sysUrlBase_+'/aulasvirtuales/invitar/?idaula='+data.new),2000);
                  }else{
                    mostrar_notificacion(msjeAttention, data.msj, 'warning');
                  }
                  $('#procesando').hide('fast');
                  return false;
                },
                error: function(xhr,status,error){
                  console.log(xhr,status,error);
                  mostrar_notificacion(msjeAttention, status, 'warning');
                  $('#procesando').hide('fast');
                  return false;
                }               
            });
      };

    var fnAjaxFail = function(xhr, textStatus, errorThrown) {
        console.log("error"); 
        console.log(xhr.responseText);
        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    };

    var filtroestudiantes=<?php echo !empty($aula["filtroestudiantes"])?$aula["filtroestudiantes"]:'{colegio:-1,aula:-1,grupo:0,alumnos:0}'; ?>;
    

    var inputfiltroestudiantes=function(){
       var data={
        colegio:$('#opcColegio').val(),
        aula:$('#opcAula').val(),
        grupo:$('#opcGrupo').val(),
        alumnos:$('#opcAlumno').val()
      }
      $('#filtroestudiantes').val(JSON.stringify(data));
    }
    
    $('.pnlestudiantes').on('change', '#opcColegio', function(e) {
        e.preventDefault();
        var $this = $(this);
        var idColegio = $this.val();       
        if(idColegio==null || idColegio==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
            $('#pnl-filtros #opcAula').html(optGrup);
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/ambiente/getxlocal/',
            type: 'POST',
            dataType: 'json',
            data: {'idlocal':idColegio},
            beforeSend: function(){ $('#pnl-filtros #opcAula').attr('disabled','disabled'); }
        }).done(function(resp) {
            if(resp.code=='ok'){
                var idaula_=filtroestudiantes.aula;
                var aulas = resp.data;
                var options='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
                $.each(aulas, function(key, val){                   
                   var sel=idaula_==val.idambiente?' Selected="selected"':' ';
                    options+='<option value="'+val.idambiente+'"'+sel+'>'+val.numero+'</option>';
                });
                $('.pnlestudiantes #opcAula').html(options);
                inputfiltroestudiantes();
                if(idaula_!="-1")$('.pnlestudiantes #opcAula').trigger('change');                
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function() {
            $('#pnl-filtros #opcAula').removeAttr('disabled');
        });   
    }).on('change', '#opcAula', function(e) {
        e.preventDefault();
        var $this = $(this);
        var idAula = $this.val();
        if(idAula==null || idAula==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
            $('.pnlestudiantes #opcGrupo').html(optGrup);

            var optAlum='<option value="-1">- <?php echo JrTexto::_('Select Student'); ?> -</option>';
            $('.pnlestudiantes #opcAlumno').html(optAlum);
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/grupo_matricula/getByAula/',
            type: 'POST',
            dataType: 'json',
            data: {'idambiente':idAula},
            beforeSend: function(){ 
                $('#pnl-filtros #opcGrupo').attr('disabled','disabled'); 
                $('#pnl-filtros #opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                /***************    Grupos    ***************/
                var idgrupo_=filtroestudiantes.grupo;                
                var grupos = resp.data.grupos;
                var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
                    optGrup+='<option value="0" '+(idgrupo_==0?' Selected="selected"':' ')+'>- <?php echo JrTexto::_('All Groups'); ?> -</option>';
                $.each(grupos, function(key, gr) {
                    var sel=idgrupo_==gr.idgrupo?' Selected="selected"':' ';
                    optGrup+='<option value="'+gr.idgrupo+'" '+sel+'>'+gr.dias+' - '+gr.horainicio+'-'+gr.horafin+'</option>';
                });
                $('.pnlestudiantes #opcGrupo').html(optGrup);

                /***************    Alumnos    ***************/
                var idlumno_=filtroestudiantes.alumnos;
                var alumnos = resp.data.alumnos;
                 optAlum+='<option value="0">- <?php echo JrTexto::_('All Students'); ?> -</option>';
                $.each(alumnos, function(key, al) {
                    var sel=idlumno_==al.dni?' Selected="selected"':' ';
                    optAlum+='<option value="'+al.dni+'" data-email="'+al.email+'" '+sel+'>'+al.ape_paterno+' '+al.ape_materno+' '+al.nombre+'</option>';
                });
                $('.pnlestudiantes #opcAlumno').html(optAlum);
                inputfiltroestudiantes();
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $('.pnlestudiantes #opcGrupo').removeAttr('disabled');
            $('.pnlestudiantes #opcAlumno').removeAttr('disabled');
        }); 
    }).on('change', '#opcGrupo', function(e) {
        e.preventDefault();
        inputfiltroestudiantes();
    }).on('change', '#opcAlumno', function(e) {
        e.preventDefault();
        inputfiltroestudiantes();
    })

    <?php if(!empty($filtroestudiantes)) echo "$('.pnlestudiantes #opcColegio').trigger('change');"; ?>
      //funciones de inicio  
      showexamen('setting');
      $('.datetimepicker').datetimepicker({
            defaultDate: "<?php echo date("m/d/Y h:i") ?>",
            format: 'YYYY/MM/DD HH:mm'           
      });
  });
</script>