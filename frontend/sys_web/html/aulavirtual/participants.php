<style type="text/css">
	.danger{
		border: 1px solid red; 
	}
	.btnsendmail,.btnborraruser{
		cursor: pointer;
	}
</style>
<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
$usuarios=!empty($_REQUEST["usuarios"])?$_REQUEST["usuarios"]:'';
$Participantes=!empty($this->participantes)?$this->participantes:'';
$aula=@$this->aula;
$portada=@$aula["portada"];
if(!empty($portada)){
	$pos = strpos($portada, 'nofoto');
	if($pos===false)
		$portada='<div style="text-align: center;"><img  src="'.@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada).'" width="60%" ></div>';
	else
		$portada='';
}else
$portada='';
$enlace=$this->documento->getUrlBase()."/aulavirtual/requisitos/?id=".$aula["aulaid"]."&".$idgui.$aula["aulaid"].date("Ymd")."&idioma=ES";
if($this->documento->plantilla!='modal'){
?>
<div class="row">
	<div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li class="active"><?php echo JrTexto::_('Smartclass'); ?></li>
        </ol>
	</div>
</div>
<?php }?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="panel">         
         	<div class="panel-body">
         	<button class="btn-addpartipant btn btn-success pull-right btn-xs" style="margin-bottom: 0.5ex;"><i class="fa fa-plus"></i> <?php echo JrTexto::_("Add New") ?></button>
	            <table class="table table-striped table-responsive">
		            <thead>
		                <tr class="headings">
		                  	<th>#</th>
		                  	<th ><?php echo JrTexto::_("Name") ;?></th> 
		                    <th ><?php echo JrTexto::_("Email"); ?></th> 
		                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
		                </tr>
		            </thead>
	              	<tbody>
	              	<?php 
	              	$i=1;
	              	if(!empty($Participantes)){	
	              	   $usuadd=array();              		            		
	              		foreach ($Participantes as $usu){ 
	              			array_push($usuadd,$usu["email"]);
	              			?>
	              			<tr>
			              		<td class="id"><?php echo $i; ?></td>
			              		<td class="istooltip name btnaddnombre" title="<?php echo JrTexto::_("Your Name"); ?>" data-txt='<?php echo JrTexto::_("Your Name"); ?>'><?php echo $usu["usuario"]; ?></td>
			              		<td class="istooltip email btnaddnombre" title="<?php echo JrTexto::_("Your Email"); ?>" data-txt='<?php echo JrTexto::_("Your Email"); ?>' data-tipo="email"><?php echo $usu["email"]; ?></td>
			              		<td><i class="istooltip btnsendmail fa fa-envelope-o"  title="<?php echo JrTexto::_("Send mail"); ?>"></i> <i  title="<?php echo JrTexto::_("Remove participant"); ?>"  class="istooltip btnborraruser fa fa-trash"></i></td>
			              	</tr>
	              		<?php $i++; }
	              	}
	              	if(!empty($usuarios)){
	              		$datos=json_decode($usuarios);	              		             		
	              		foreach ($datos as $usu){
	              			if(in_array($usu->email,$usuadd )) continue;
	              			?>
	              			<tr>
			              		<td class="id"><?php echo $i; ?></td>
			              		<td class="istooltip name btnaddnombre" title="<?php echo JrTexto::_("Your Name"); ?>" data-txt='<?php echo JrTexto::_("Your Name"); ?>'><?php echo $usu->name; ?></td>
			              		<td class="istooltip email btnaddnombre" title="<?php echo JrTexto::_("Your Email"); ?>" data-txt='<?php echo JrTexto::_("Your Email"); ?>' data-tipo="email"><?php echo $usu->email; ?></td>
			              		<td><i class="istooltip btnsendmail fa fa-envelope-o" title="<?php echo JrTexto::_("Send mail"); ?>"></i> <i title="<?php echo JrTexto::_("Remove participant"); ?>" class="istooltip btnborraruser fa fa-trash"></i></td>
			              	</tr>
	              		<?php $i++; }
	              	}
	              	
	              	?>
	              	<tr>
	              		<td class="id"><?php echo $i;?></td>
	              		<td class="istooltip name btnaddnombre" title="<?php echo JrTexto::_("Your Name"); ?>" data-txt='<?php echo JrTexto::_("Your Name"); ?>'><?php echo JrTexto::_("Your Name"); ?></td>
	              		<td class="istooltip email btnaddnombre" title="<?php echo JrTexto::_("Your Email"); ?>" data-txt='<?php echo JrTexto::_("Your Email"); ?>' data-tipo="email"><?php echo JrTexto::_("Your Email"); ?></td>
	              		<td><i class="istooltip btnsendmail fa fa-envelope-o" title="<?php echo JrTexto::_("Send mail"); ?>"></i> <i  title="<?php echo JrTexto::_("Remove participant"); ?>"  class="istooltip btnborraruser fa fa-trash"></i></td>
	              	</tr>
	                </tbody>
	        	</table>
	        	<div class="text-center"><hr>
	        	<button class="istooltip btn-seleccionar btn btn-primary  " style="margin-top: 0.5ex;"><i class="fa fa-save"></i> <?php echo JrTexto::_("Save Participants") ?></button>
	        	<button class="istooltip btn-addpartipant btn btn-success " style="margin-top: 0.5ex;"><i class="fa fa-plus"></i> <?php echo JrTexto::_("Add New") ?></button>
	        	</div>
        	</div>
        </div>
	</div>
</div>
</div>
<div id="tmpmensaje" style="display: none;"><div>
	 Ud. <b><span style="color:#b90e0e"> !!usuario!! </span> </b> 
	<br>Esta cordialmente invitado a la siguiente clase virtual denominada:<br>
	<h3 style="text-center:center; color:#031b47"><?php echo $aula["titulo"]; ?></h3><br><?php echo $portada; ?><br>
	<p style="text-align: justify;" data-mce-style="text-align: justify;"><?php echo $aula["descripcion"]; ?></p>
	<p style="text-align: center;" data-mce-style="text-align: center;">
	<h4 style="text-align: center;" data-mce-style="text-align: center;">Para conectarse </h4>
	Verifique la fecha y hora de la clase : <b><?php echo $aula["fecha_inicio"]; ?></b><br> luego
	click  en el siguiente enlace <b><a href="<?php echo $enlace ?>">Ingresar aqui</b></a><br>
	o copie la siguiente direccion url en su navegador<br>
	<a href="<?php echo $enlace ?>"><b><?php echo $enlace ?></b></a>
	</p>
	  </div>
</div>
<script type="text/javascript">
    var enviarmail<?php echo $idgui; ?>=function(data){
    	var mensaje=$('#tmpmensaje').html();
		var Arradjuntos=[];
		var imgs=[];
		$(mensaje).find('img').each(function(){
			imgs.push($(this).attr('src'));
		});
		var txtemails=JSON.stringify(data);
		var formData = new FormData();
		if(imgs!=='[]')formData.append("images", JSON.stringify(imgs));
		if(txtemails==='[]'){ 
			mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', '<?php echo JrTexto::_('Mail message without recipients'); ?>', 'error');
			return;
		}
		formData.append("paraemail", txtemails);
		formData.append("idaula", '<?php echo $aula["aulaid"]; ?>');
		$.ajax({
            url: _sysUrlBase_+'/Aulavirtualinvitados/json_guardar/',
            type: "POST",
            data:  formData,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,	          
            success: function(data)
            {	               	
                if(data.code==='ok'){
                	mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');	                   
                }else{
                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
                }
                $('#procesando').hide('fast');
                return false;
            },
            error: function(xhr,status,error){
            	console.log(xhr);
            	console.log(status);
            	console.log(error);
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
                $('#procesando').hide('fast');
                return false;
            }
        });
        formData.append("deemail", 'info@abacoeducacion.org');
        formData.append("mensaje", mensaje);
        formData.append("asunto", '<?php echo ucfirst(JrTexto::_("Invitation to participate in Smartclass"))?>');          
        console.log(txtemails); 
        $.ajax({
            url: _sysUrlBase_+'/Sendemail/enviarcorreoall',
            type: "POST",
            data:  formData,
            contentType: false,
            dataType :'json',
            cache: false,
            processData:false,
            beforeSend: function(XMLHttpRequest){
                $('#procesando').show('fast');                
            },
            success: function(data)
            {	               
                if(data.code==='ok'){
                	mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');	                  
                }else{
                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
                }
                $('#procesando').hide('fast');
                return false;
            },
	        error: function(xhr,status,error){
	            	console.log(xhr);
	            	console.log(status);
	            	console.log(error);
	                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
	                $('#procesando').hide('fast');
	                return false;
	            },
	        });
    }
	$(document).ready(function(ev){
		var numerartabla<?php echo $idgui; ?>=function(){
			var table=$('#ventana_<?php echo $idgui; ?> table');
			var tr=$('tbody',table).find('tr');
			var i=0;
			$.each(tr,function(){i++;
				$(this).find('td:first').text(i);
			});
		}
		$('#ventana_<?php echo $idgui; ?>').on('click','.btn-addpartipant',function(ev){
			ev.preventDefault();
			var table=$('#ventana_<?php echo $idgui; ?> table');
			var tr=table.find('tr:last').clone(true);
			$('td',tr).each(function(){
				$(this).text($(this).attr('data-txt'));
			});
			$('tbody',table).append(tr);
			numerartabla<?php echo $idgui; ?>();
		}).on('click','td.btnaddnombre',function(ev){
			ev.preventDefault();
			console.log('bbb');
		    if($('input',this).length>0)return;
			var tipo=$(this).attr('data-tipo')||'text';
			var txttipo=tipo=='email'?'<?php echo JrTexto::_("Your Email"); ?>':'<?php echo JrTexto::_("Your Name"); ?>';
			var txt=$(this).text()||'';
			if(txt==txttipo) txt='';
			$(this).removeClass('danger');
			$(this).html('<input type="'+tipo+'" class="form-control" placeholder="'+txttipo+'" value="'+txt+'">');
			$(this).find('input').focus();
		}).on('blur','td.btnaddnombre input',function(ev){
			var td=$(this).closest('td');
			td.removeClass('danger');
			var tipo=td.attr('data-tipo')||'text';
			var txtini=td.attr('data-txt');
			var txt=$(this).val();
			if(tipo=='email'){
				if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(txt)){
					td.addClass('danger'); 
				}
			}
			if(txt==''||txt==txtini){td.addClass('danger');}
			if(txt!='')td.html(txt);
		}).on('keydown','td.btnaddnombre input',function(ev){						
		    var code = ev.keyCode || ev.which;
		    var td=$(this).closest('td');
		    var tipo=td.attr('data-tipo')||'text';
			if(code===9||code===13){ 
				console.log(td.siblings('td'));
				td.trigger('blur');
		        if(tipo!='email')td.siblings('td.btnaddnombre').trigger('click');
		    }
		}).on('click','i.btnborraruser',function(ev){
			var table=$('#ventana_<?php echo $idgui; ?> table');
			if ($('tbody',table).find('tr').length>1){
				var tr=$(this).closest('tr');
				var _email=$("td.email",tr).text();
				var idaula= '<?php echo $aula["aulaid"]; ?>';
				console.log(tr);
				tr.remove();
				if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(_email)){
					return false;
				}
				var formData = new FormData();
				formData.append("email", _email);
				formData.append("idaula", '<?php echo $aula["aulaid"]; ?>');
				$.ajax({
	            url: _sysUrlBase_+'/aulavirtualinvitados/json_eliminar',
	            type: "POST",
	            data:  formData,
	            contentType: false,
	            dataType :'json',
	            cache: false,
	            processData:false,
	            beforeSend: function(XMLHttpRequest){
	                $('#procesando').show('fast');                
	            },
	            success: function(data)
	            {	               	
	                console.log(data);
	                if(data.code==='ok'){
	                	mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'success');	                  
	                }else{
	                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
	                }
	                $('#procesando').hide('fast');
	                return false;
	            },
		        error: function(xhr,status,error){
		            	console.log(xhr);
		            	console.log(status);
		            	console.log(error);
		                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
		                $('#procesando').hide('fast');
		                return false;
		            },
		        });

			}			
		}).on('click','i.btnsendmail',function(ev){
			var tr=$(this).closest('tr');
			var _name=$('.name',tr).text();
			var _txtname=$('.name',tr).attr('data-txt');
			var _email=$('.email',tr).text();
			var _txtemail=$('.email',tr).attr('data-txt');
			if(_name==_txtname&&_email==_txtemail)return true;
			if(_name==_txtname){$('.name',tr).addClass('danger'); _name='';} //error=true;}				
			if(_email==_txtemail){$('.email',tr).addClass('danger'); error=true;}
			
			if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(_email)){
				$('.email',tr).addClass('danger'); error=true;
			}
			var item=[{
				id:$('.id',tr).text(),
				email:$('.email',tr).text(),
				name:_name
			}]
			enviarmail<?php echo $idgui; ?>(item);
		}).on('click','.btn-seleccionar',function(ev){
			var table=$('#ventana_<?php echo $idgui; ?> table');
			var tr=$('tbody',table).find('tr');
			var data=[];
			var error=false;
			$.each(tr,function(){
				var _name=$('.name',this).text();
				var _txtname=$('.name',this).attr('data-txt');
				var _email=$('.email',this).text();
				var _txtemail=$('.email',this).attr('data-txt');
				if(_name==_txtname&&_email==_txtemail)return true;
				if(_name==_txtname){$('.name',this).addClass('danger'); _name='';} //error=true;}				
				if(_email==_txtemail){$('.email',this).addClass('danger'); error=true;}
				
				if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(_email)){
					$('.email',this).addClass('danger'); error=true;
				}
				var item={
					id:$('.id',this).text(),
					email:$('.email',this).text(),
					name:_name
				}
				data.push(item);
			});
			if(error==false)
				if(typeof <?php echo $ventanapadre?> == 'function'){
          			<?php echo $ventanapadre?>(data);
          			$(this).closest('.modal').find('.cerrarmodal').trigger('click');
      			}
		});
	});
</script>