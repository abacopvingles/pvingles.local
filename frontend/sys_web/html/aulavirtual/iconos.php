<div class="grupo-opciones">
<div class="dropup ">
<button class="btn btn-default dropdown-toggle btn-xs" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  <?php echo ucfirst(JrTexto::_("Icons"));?>
  <span class="caret"></span>
</button>
<ul class="dropdown-menu " id="showiconos" aria-labelledby="dropdownMenu2">
  <li >
      <i class="hvr-grow em em-angry" title="triste" data-audio="triste"></i>
      <i class="hvr-grow em em-anguished" title="angustiado" data-audio="angustiado"></i>
      <i class="hvr-grow em em-astonished" title="asombrado" data-audio="asombrado"></i>
      <i class="hvr-grow em em-blush" title="sonrojo" data-audio="Enrojecido"></i>
      <!--i class="hvr-grow em em-flushed" title="Enrojecido"></i-->
      <!--i class="hvr-grow em em-bowtie"></i-->
      <i class="hvr-grow em em-fearful" title="temereso" data-audio="temeroso"></i>
      <i class="hvr-grow em em-cold_sweat" title="sudor frio" data-audio="sudor"></i>
      <i class="hvr-grow em em-scream" title="Congelado" data-audio="conjelado"></i>
      <i class="hvr-grow em em-confused" title="confundido" data-audio="confundido"></i>
      <i class="hvr-grow em em-cry" title="llorar" data-audio="lloro"></i>
      <i class="hvr-grow em em-disappointed" title="decepcionado" data-audio="decepcionado"></i>
      <i class="hvr-grow em em-disappointed_relieved" title="decepcionado y aliviado"></i>
      <i class="hvr-grow em em-expressionless" title="inexpresivo" data-audio="inexpresivo"></i>                                              
      <i class="hvr-grow em em-frowning" title="enfadado preocupado" data-audio="enfadado"></i>

      <!--i class="hvr-grow em em-full_moon_with_face"></i-->
      <!--i class="hvr-grow em em-grimacing" title="haciendo muecas"></i-->
      <i class="hvr-grow em em-grin" title="mueca" data-audio="mueca"></i>
      <i class="hvr-grow em em-grinning" title="Sonriendo" data-audio="risas.mp3"></i>
      <i class="hvr-grow em em-hushed" title="Callado" data-audio="callado"></i>
      <i class="hvr-grow em em-innocent" title="inocente" data-audio="inocente"></i>
      <i class="hvr-grow em em-joy" title="Alegre" data-audio="feliz"></i>
      <i class="hvr-grow em em-kissing" title="Besando" data-audio="beso"></i>
      <i class="hvr-grow em em-kissing_closed_eyes" title="Besando con los ojos cerrados" data-audio="beso"></i>
      <i class="hvr-grow em em-kissing_face" title="cara de beso" data-audio="beso"></i>
      <i class="hvr-grow em em-kissing_heart" title="Besando el corazon" data-audio="beso"></i>
      <i class="hvr-grow em em-kissing_smiling_eyes" title=""></i>
      <i class="hvr-grow em em-heart_eyes" title=""></i>
      <i class="hvr-grow em em-laughing" title=""></i>
      <i class="hvr-grow em em-neutral_face" title=""></i>
      <i class="hvr-grow em em-pensive" title=""></i>
      <i class="hvr-grow em em-relaxed" title="relajado" data-audio="relajado"></i>
      <i class="hvr-grow em em-relieved" title=""></i>
      <i class="hvr-grow em em-weary" title=""></i>
      <i class="hvr-grow em em-worried" title=""></i>
      <i class="hvr-grow em em-yum" title=""></i>
      <i class="hvr-grow em em-wink" title=""></i>
      <i class="hvr-grow em em-woman" title=""></i>
      <i class="hvr-grow em em-unamused" title=""></i>
      <i class="hvr-grow em em-tired_face"></i>
      <i class="hvr-grow em em-triumph"></i>
      <i class="hvr-grow em em-stuck_out_tongue"></i>
      <i class="hvr-grow em em-stuck_out_tongue_closed_eyes"></i>
      <i class="hvr-grow em em-stuck_out_tongue_winking_eye"></i>
      <i class="hvr-grow em em-sunglasses"></i>
      <i class="hvr-grow em em-sweat"></i>
      <i class="hvr-grow em em-sweat_smile"></i>
      <i class="hvr-grow em em-sleeping" title="dormiendo" data-audio="durmiendo"></i>
      <i class="hvr-grow em em-sleepy" title="dormiendo" data-audio="dormiendo"></i>
      <i class="hvr-grow em em-smile"></i>
      <i class="hvr-grow em em-smiley"></i>
      <i class="hvr-grow em em-smirk"></i>
      <i class="hvr-grow em em-sob"></i>
      <i class="hvr-grow em em-satisfied" title="satisfecho" data-audio="satisfecho"></i>                     
      <i class="hvr-grow em em-persevere" title="perserverante" data-audio="perserverante"></i>
      <i class="hvr-grow em em-mask"></i>
      <i class="hvr-grow em em-new_moon_with_face"></i>
      <i class="hvr-grow em em-trollface"></i>
      <i class="hvr-grow em em-performing_arts"></i>
      <i class="hvr-grow em em-alien"></i>
      <i class="hvr-grow em em-angel"></i>
      <i class="hvr-grow em em-bow"></i>
      <i class="hvr-grow em em-baby"></i>
      <i class="hvr-grow em em-boy"></i>
      <i class="hvr-grow em em-cop"></i>
      <i class="hvr-grow em em-man"></i>
      <i class="hvr-grow em em-neckbeard"></i>
      <i class="hvr-grow em em-girl"></i>
      <i class="hvr-grow em em-guardsman"></i>
      <i class="hvr-grow em em-person_with_blond_hair"></i>
      <i class="hvr-grow em em-princess"></i>
      <i class="hvr-grow em em-information_desk_person"></i>
      <i class="hvr-grow em em-person_frowning"></i>
      <i class="hvr-grow em em-person_with_pouting_face" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-no_good" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-ok_woman" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-raising_hand" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-two_men_holding_hands" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-two_women_holding_hands" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-couple" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-dancers" title="bailando" data-audio="bailando"></i>
      <i class="hvr-grow em em-dancer" title="bailando" data-audio="bailando"></i>
      <i class="hvr-grow em em-running" title="corriendo" data-audio="corriendo"></i>
      <i class="hvr-grow em em-walking" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-kiss" title="beso" data-audio="beso"></i>
      <i class="hvr-grow em em-heartbeat" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-purple_heart" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-gift_heart" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-broken_heart" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-heart" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-facepunch" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-fist" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-muscle" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-hand" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-plus1" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-ok_hand"  title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-open_hands" title="ok" data-audio="aplauso.mp3"></i>
      <i class="hvr-grow em em-raised_hand" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-raised_hands" title="ok" data-audio="aplauso1.mp3"></i>
      <i class="hvr-grow em em-v" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-monkey_face" title="mono" data-audio="mono"></i>
      <i class="hvr-grow em em-mouse" title="ok" data-audio="ok"></i>
      <i class="hvr-grow em em-koala"  title="koala" data-audio="koala"></i>
      <i class="hvr-grow em em-panda_face" title="panda" data-audio="panda"></i>
      <i class="hvr-grow em em-penguin"  title="pinguino" data-audio="pinguino"></i>
      <i class="hvr-grow em em-pig"  title="cerdo" data-audio="cerdo"></i>
      <i class="hvr-grow em em-skull" title="calabera" data-audio="calabera"></i>
      <i class="hvr-grow em em-star" title="start" data-audio="start"></i>
      <i class="hvr-grow em em-star2"  title="start" data-audio="start"></i>
      <i class="hvr-grow em em-zzz"   title="durmiendo" data-audio="durmiendo"></i>
      <i class="hvr-grow em em-coffee"  title="beber" data-audio="beber"></i>
      <i class="hvr-grow em em-wine_glass"  title="beber" data-audio="beber"></i>
      <i class="hvr-grow em em-tropical_drink"  title="beber" data-audio="beber"></i>
      </li>                   
</ul>
</div>
</div>