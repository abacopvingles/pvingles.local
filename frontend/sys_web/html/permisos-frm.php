<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Permisos'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdpermiso" id="pkidpermiso" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRol">
              <?php echo JrTexto::_('Rol');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRol" name="txtRol" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["rol"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMenu">
              <?php echo JrTexto::_('Menu');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtMenu" name="txtMenu" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["menu"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txt_list">
              <?php echo JrTexto::_(' list');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["_list"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo !empty($frm["_list"])?$frm["_list"]:0;?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["_list"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["_list"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txt_list" value="<?php echo !empty($frm["_list"])?$frm["_list"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txt_add">
              <?php echo JrTexto::_(' add');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["_add"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo !empty($frm["_add"])?$frm["_add"]:0;?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["_add"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["_add"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txt_add" value="<?php echo !empty($frm["_add"])?$frm["_add"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txt_edit">
              <?php echo JrTexto::_(' edit');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["_edit"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo !empty($frm["_edit"])?$frm["_edit"]:0;?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["_edit"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["_edit"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txt_edit" value="<?php echo !empty($frm["_edit"])?$frm["_edit"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txt_delete">
              <?php echo JrTexto::_(' delete');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["_delete"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo !empty($frm["_delete"])?$frm["_delete"]:0;?>"
                data-valueno="0" 
                data-value2="<?php echo @$frm["_delete"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["_delete"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txt_delete" value="<?php echo !empty($frm["_delete"])?$frm["_delete"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-savePermisos" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('permisos'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){ 
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Permisos', 'savePermisos', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Permisos"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Permisos"))?>');
        <?php endif;?>       }
     }
  }); 
  
});


</script>

