<style type="text/css">
    .port{
        width: 100%;
        height: 300px;
    }
    .port>img{
        height: 300px;
        width: 100%;
    }
</style>
<?php   
    // var_dump($this->miscursos);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-list">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <!-- <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('Library')); ?></a></li> -->
            <li class="active"><?php echo JrTexto::_('List'); ?></li>
        </ol>
    </div> </div>

    <div class="row" id="filtros-actividad" style="padding-top: 10px;"> 
        <div class="col-xs-12 col-sm-4 padding-0">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul">
                    <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                        <option value="0" selected>- <?php echo ucfirst(JrTexto::_("Select a course")); ?> <span>*</span> -</option>
                        <?php 
                            $i=0;
                            if(!empty($this->cursos))
                                foreach($this->cursos as $t){
                                    if(!empty($this->miscursos))
                                        foreach($this->miscursos as $c){
                                            if ($t["idcurso"]==$c["idcurso"]) {
                                                // echo '<option value="'.$t["idcurso"].'"'.($this->idCurso==$t["idcurso"]?'selected="selected"':'').'>'.$t["nombre"].'</option>';
                                                // break ;
                                                if ($i==0) {
                                                    echo '<option value="'.$t["idcurso"].'" selected>'.$t["nombre"].'</option>';
                                                    $i++;
                                                    break ;
                                                }else{
                                                    echo '<option value="'.$t["idcurso"].'"'.($this->idCurso==$t["idcurso"]?'selected="selected"':'').'>'.$t["nombre"].'</option>';
                                                    break ;
                                                }
                                            }
                                        }}
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 padding-0">
            <div class="col-xs-12 form-group">
                    <label for="texto" class="col-xs-12 col-sm-3" style="padding-top: 8px;text-align: right;"><?php echo ucfirst(JrTexto::_('Search')).':'; ?></label>
                    <div class="col-xs-12 col-sm-9">
                        <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
                    </div>
            </div>
        </div>
    </div>
    <div class="row" id="listados">
        <div class="col-xs-12">
            <!-- <a href="<?php echo $this->documento->getUrlBase();?>/tarea/agregar" class="col-xs-12 col-sm-2 btn btn-success crear-tarea"> <?php echo JrTexto::_("Assign Materials")?></a> -->
            <div class="col-xs-12 col-sm-10">
                <ul class="nav nav-tabs">
                    <!-- <li class="tabtodos">
                        <a href="#" id='booksmostrar' data-toggle="tab"><?php echo ucfirst(JrTexto::_("Books"))?></a>
                    </li> -->
                    <!-- <li class="todosvideos">
                        <a href="#" id='videosmostrar' data-toggle="tab"><?php echo ucfirst(JrTexto::_("Videos"))?></a>
                    </li> -->
                    <!-- <li class="todosaudios">
                        <a href="#" id='audiosmostrar' data-toggle="tab"><?php echo ucfirst(JrTexto::_("Audios"))?></a>
                    </li> -->
                    <li id='tabmusica' class="todosmusica">
                        <a href="#" id='musicamostrar' data-toggle="tab"><?php echo ucfirst(JrTexto::_("Music"))?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 tab-content" style="padding-top: 15px;">
            <div id="pnl-pendientes" class="row tab-pane fade active in">
                <?php if(!empty($this->tareasPend)) {
                foreach ($this->tareasPend as $t) { ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                        <div class="panel-heading titulo">
                            <h3><?php echo $t['nombre']; ?></h3>
                            <!--<div class="opciones hidden">
                                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id='.$t['idtarea'];?>" class="color-blue editar"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-idtarea="<?php echo $t['idtarea'];?>" class="color-red eliminar"><i class="fa fa-times"></i></a>
                            </div>-->
                        </div>
                        <div class="panel-body contenido">
                            <div class="col-xs-4 portada">
                                <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                            </div>
                            <div class="col-xs-8 descripcion">
                                <?php echo $t['descripcion']; ?>
                            </div>
                            <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                <div class="col-xs-8 fecha-entrega">
                                    <div class="dato"><?php echo $t['fechaentrega'] .' '. date('h:i a', strtotime($t['horaentrega'])); ?></div>
                                    <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                </div>
                                <div class="col-xs-4 presentados border-left">
                                    <div class="dato"><?php echo $t['cant_presentados']; ?></div>
                                    <div class="info"><?php echo JrTexto::_("Submitted")?></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id='.$t['idtarea_asignacion'];?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                        </div>
                    </div>
                </div>
                <?php } }else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("No activity found").'</h4></div>'; } ?>
            </div>
            <div id="pnl-finalizados" class="row tab-pane fade">
                <?php if(!empty($this->tareasFin)) {
                foreach ($this->tareasFin as $t) { ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                        <div class="panel-heading titulo">
                            <h3><?php echo $t['nombre']; ?></h3>
                            <!--<div class="opciones hidden">
                                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id='.$t['idtarea'];?>" class="color-blue editar"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-idtarea="<?php echo $t['idtarea'];?>" class="color-red eliminar"><i class="fa fa-times"></i></a>
                            </div>-->
                        </div>
                        <div class="panel-body contenido">
                            <div class="col-xs-4 portada">
                                <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                            </div>
                            <div class="col-xs-8 descripcion">
                                <?php echo $t['descripcion']; ?>
                            </div>
                            <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                <div class="col-xs-8 fecha-entrega">
                                    <div class="dato"><?php echo $t['fechaentrega'] .' '. date('h:i a', strtotime($t['horaentrega'])); ?></div>
                                    <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                </div>
                                <div class="col-xs-4 presentados border-left">
                                    <div class="dato"><?php echo $t['cant_presentados']; ?></div>
                                    <div class="info"><?php echo JrTexto::_("Submitted")?></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id='.$t['idtarea_asignacion'];?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                        </div>
                    </div>
                </div>
                <?php } }else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("No activity found").'</h4></div>'; } ?>
            </div>
            <div id="pnl-todos" class="row tab-pane active in">
            </div>
            <div id="pnl-todosvideos" class="row tab-pane active in">
            </div>
            <div id="pnl-todosaudios" class="row tab-pane active in">
            </div>
            <div id="pnl-todosmusica" class="row tab-pane active in">
            </div>
        </div>
    </div>
</div>

<section class="hidden">
    <div class="col-xs-12 col-sm-3" id="clone-tarea_item_todo">
        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="">
            <div class="panel-heading titulo">
                <h3>title</h3>
                <!-- <div class="opciones">
                    <a href="#" data-idtarea="" class="color-red eliminar"><i class="fa fa-times"></i></a>
                </div> -->
            </div>
            <div class="panel-body contenido">
                <div class="col-xs-12  port">
                    <img src="<?php echo $this->documento->getUrlStatic();?>/media/web/nofoto.jpg" alt="img" class="img-responsive">
                </div>
                <!-- <div class="col-xs-8 descripcion">
                    descripcion
                </div>
                <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                    <div class="col-xs-8 fecha-entrega">
                        <div class="dato">dd-mm-yyyy hh:ii aa</div>
                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                    </div>
                    <div class="col-xs-4 asignados border-left">
                        <div class="dato">00</div>
                        <div class="info"><?php echo JrTexto::_("Students assigned")?></div>
                    </div>
                </div> -->
            </div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id=';?>" class="btn btn-blue editarlibro"><?php echo ucfirst(JrTexto::_("View"))?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>
</section>

<script>
var $contenedorFiltros = $('#filtros-actividad');
var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp){
        //console.log(resp);
        if(resp.code=='ok') {
            var options = '';            
            if(resp.data.length) {
                var tienesesion=esunidad=false;
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E"){
                        if(det.tiporecurso=="L"){
                            options += '<option value="'+det.idrecurso+'" data-recurso="L" >'+det.nombre+'</option>';
                            tienesesion=true;
                        }else if(det.tiporecurso=="U"){
                            options += '<option value="'+det.idcursodetalle+'" data-recurso="U" >'+det.nombre+'</option>';
                            esunidad=true;
                        }else options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.addClass('select-cursodetalle');
                $select.append(options);
                $select.closest('.form-group').parent().removeClass('hidden');
                if(tienesesion) $select.addClass('tienesesion');
                if(esunidad) $select.addClass('esunidad');
            }else{
                $select.attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().prev().find('select.sel-cursodet').attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.form-group').parent().remove();
        }
    }).fail(function(err) {
        $select.closest('.form-group').parent().remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var borrarSobrantes = function($select) {
    var $listSelect = $contenedorFiltros.find('select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).length>=1){
        $contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).closest('.form-group').parent().remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect,
        'required' : 'required'
    });
    $contenedorFiltros.append($divSelect_new);
    return $contenedorFiltros.find('#'+nuevoIdSelect);
};

var fnAjaxFail = function(xhr, textStatus, errorThrown){ throw errorThrown;
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
};

var nuevoItemTarea = function(tarea, $contenedor) {
    var $itemTarea = $('#clone-tarea_item').clone();
    $itemTarea.removeAttr('id');
    $itemTarea.find('.panel-heading.titulo>h3').html(tarea.nombre);
    $itemTarea.find('.tarea-item').attr('data-idtarea',tarea.idtarea);
    var img_src=(tarea.foto!='')?tarea.foto:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemTarea.find('.panel-body.contenido>.portada>img').attr('src', img_src.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
    $itemTarea.find('.panel-body.contenido>.descripcion').html(tarea.descripcion);
    $itemTarea.find('.panel-body.contenido .fecha-entrega>.dato').html(tarea.fechaentrega+' '+tarea.horaentrega);
    $itemTarea.find('.panel-body.contenido .presentados>.dato').html(tarea.cant_presentados);
    $itemTarea.find('.panel-footer a.vertarea').attr('href', _sysUrlBase_+'/tarea/ver/?id='+tarea.idtarea_asignacion);

    $contenedor.append($itemTarea);
};

var nuevoItemTodoLibros = function(libros,filtros) {
    var $itemLibro = $('#clone-tarea_item_todo').clone();
    // $itemLibro.removeAttr('id');
    $itemLibro.find('.panel-heading.titulo>h3').html(libros.nombre);
    $itemLibro.find('.tarea-item').attr('data-idtarea',libros.id_estudio);
    var link = _sysUrlBase_+'/static/libreria/image/'+libros.foto;
    var img_src=(libros.foto!='')?link:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemLibro.find('.panel-body.contenido>.port>img').attr('src', img_src);
    var idcurso=$('#opcIdCurso').val();
    // var idunidad=$('#filtros-actividad').find('select.esunidad option:selected').val()||-1;
    // var idrecurso=$('#filtros-actividad').find('select.tienesesion option:selected').val()||-1;
    
    $itemLibro.find('.panel-footer a.editarlibro').attr('href', _sysUrlBase_+'/library/ver/?id_estudio='+libros.id_estudio+'&idcurso='+filtros['idcurso']+'&tipo=L');
    $('#pnl-todos').append($itemLibro);
};
var nuevoItemTodoVideos = function(videos,filtros) {
    var $itemVideo = $('#clone-tarea_item_todo').clone();
    // $itemVideo.removeAttr('id');
    $itemVideo.find('.panel-heading.titulo>h3').html(videos.nombre);
    $itemVideo.find('.tarea-item').attr('data-idtarea',videos.id_estudio);
    var link = _sysUrlBase_+'/static/libreria/image/'+videos.foto;
    var img_src=(videos.foto!='')?link:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemVideo.find('.panel-body.contenido>.port>img').attr('src', img_src);
    var idcurso=$('#opcIdCurso').val();
    // $itemVideo.find('.panel-footer a.editarlibro').attr('href', _sysUrlBase_+'/library/editar/?id_estudio='+videos.id_estudio+'&idcurso='+filtros['idcurso']);
    // var idcurso=$('#opcIdCurso').val();
    // var idunidad=$('#filtros-actividad').find('select.esunidad option:selected').val()||-1;
    // var idrecurso=$('#filtros-actividad').find('select.tienesesion option:selected').val()||-1;
    $itemVideo.find('.panel-footer a.editarlibro').attr('href', _sysUrlBase_+'/library/ver/?id_estudio='+videos.id_estudio+'&idcurso='+filtros['idcurso']+'&tipo=V');
    // $itemLibro.find('.panel-footer a.editartarea').attr('href', _sysUrlBase_+'/tarea/editar/?id='+tarea.idtarea+'&idcurso='+idcurso+'&idunidad='+idunidad+'&idrecurso='+idrecurso);

    $('#pnl-todosvideos').append($itemVideo);
};
var nuevoItemTodoAudios = function(audios,filtros) {
    var $itemAudios = $('#clone-tarea_item_todo').clone();
    // $itemVideo.removeAttr('id');
    $itemAudios.find('.panel-heading.titulo>h3').html(audios.nombre);
    $itemAudios.find('.tarea-item').attr('data-idtarea',audios.id_estudio);
    var link = _sysUrlBase_+'/static/libreria/image/'+audios.foto;
    var img_src=(audios.foto!='')?link:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemAudios.find('.panel-body.contenido>.port>img').attr('src', img_src);
    var idcurso=$('#opcIdCurso').val();
    $itemAudios.find('.panel-footer a.editarlibro').attr('href', _sysUrlBase_+'/library/ver/?id_estudio='+audios.id_estudio+'&idcurso='+filtros['idcurso']+'&tipo=A');
    // var idcurso=$('#opcIdCurso').val();
    // var idunidad=$('#filtros-actividad').find('select.esunidad option:selected').val()||-1;
    // var idrecurso=$('#filtros-actividad').find('select.tienesesion option:selected').val()||-1;
    
    // $itemLibro.find('.panel-footer a.editartarea').attr('href', _sysUrlBase_+'/tarea/editar/?id='+tarea.idtarea+'&idcurso='+idcurso+'&idunidad='+idunidad+'&idrecurso='+idrecurso);

    $('#pnl-todosaudios').append($itemAudios);
};
var nuevoItemTodoMusica = function(musica,filtros) {
    var $itemMusica = $('#clone-tarea_item_todo').clone();
    // $itemVideo.removeAttr('id');
    $itemMusica.find('.panel-heading.titulo>h3').html(musica.nombre);
    $itemMusica.find('.tarea-item').attr('data-idtarea',musica.id_estudio);
    var link = _sysUrlBase_+'/static/libreria/image/'+musica.foto;
    var img_src=(musica.foto!='')?link:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemMusica.find('.panel-body.contenido>.port>img').attr('src', img_src);
    var idcurso=$('#opcIdCurso').val();
    $itemMusica.find('.panel-footer a.editarlibro').attr('href', _sysUrlBase_+'/library/ver/?id_estudio='+musica.id_estudio+'&idcurso='+filtros['idcurso']+'&tipo=M');
    // var idcurso=$('#opcIdCurso').val();
    // var idunidad=$('#filtros-actividad').find('select.esunidad option:selected').val()||-1;
    // var idrecurso=$('#filtros-actividad').find('select.tienesesion option:selected').val()||-1;
    
    // $itemLibro.find('.panel-footer a.editartarea').attr('href', _sysUrlBase_+'/tarea/editar/?id='+tarea.idtarea+'&idcurso='+idcurso+'&idunidad='+idunidad+'&idrecurso='+idrecurso);

    $('#pnl-todosmusica').append($itemMusica);
};
var currentRequest = null;
var cargarLibros = function(filtros=[]){
    currentRequest = $.ajax({
        url: _sysUrlBase_+'/library/listarMaterial/',
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }
            $('.tab-content .tab-pane').each(function(index, el) {
                $(el).html('<div class="cargando" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            });
        },
    }).done(function(resp) {
        var todas = resp.data.todo;
        var msjeVacio='<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>';
        if(todas.length>0){
            $.each(todas, function(i, libros) {
                nuevoItemTodoLibros(libros,filtros);
            });
        }else{
            $('#pnl-todos').append(msjeVacio);
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};

var cargarVideos = function(filtros=[]){
    currentRequest = $.ajax({
        url: _sysUrlBase_+'/library/listarVideos/',
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }
            $('.tab-content .tab-pane').each(function(index, el) {
                $(el).html('<div class="cargando" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            });
        },
    }).done(function(resp) {
        var todas = resp.data.todo;
        var msjeVacio='<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>';
        if(todas.length>0){
            $.each(todas, function(i, videos) {
                nuevoItemTodoVideos(videos,filtros);
            });
        }else{
            $('#pnl-todosvideos').append(msjeVacio);
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};   
var cargarAudios = function(filtros=[]){
    currentRequest = $.ajax({
        url: _sysUrlBase_+'/library/listarAudios/',
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }
            $('.tab-content .tab-pane').each(function(index, el) {
                $(el).html('<div class="cargando" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            });
        },
    }).done(function(resp) {
        var todas = resp.data.todo;
        var msjeVacio='<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>';
        if(todas.length>0){
            $.each(todas, function(i, audios) {
                nuevoItemTodoAudios(audios,filtros);
            });
        }else{
            $('#pnl-todosaudios').append(msjeVacio);
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};  
var cargarMusica = function(filtros=[]){
    currentRequest = $.ajax({
        url: _sysUrlBase_+'/library/listarMusica/',
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }
            $('.tab-content .tab-pane').each(function(index, el) {
                $('.tab-content .tab-pane .cargando').remove();
                $(el).html('<div class="cargando" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            });
        },
    }).done(function(resp) {
        var todas = resp.data.todo;
        var msjeVacio='<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>';
        if(todas.length>0){
            $.each(todas, function(i, musica) {
                nuevoItemTodoMusica(musica,filtros);
            });
        }else{
            $('#pnl-todosmusica').append(msjeVacio);
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};   
$(document).ready(function() {
    // $('#opcIdCurso').val(0);
    // cargarLibros($('#opcIdCurso').val());
    $('#booksmostrar').on('click', function(e) {

        var idCurso = $('#opcIdCurso').val() || 0;
        var texto = $('#texto').val() || 0;
             // console.log(idCurso);
        var filtros = {
            'idcurso': idCurso,
            'nombre':texto
        };
        cargarLibros(filtros);
        // borrarSobrantes($(this));
        if(idCurso==0){ return false; }
        // $select_new = nuevoSelectFiltro(idPadre);
        // getCursoDetalle({
        //     'idcurso': idCurso,
        //     'idpadre': idPadre,
        // }, $select_new);
    });
    $('#videosmostrar').on('click', function(e) {
        var idCurso = $('#opcIdCurso').val() || 0;
        var texto = $('#texto').val() || 0;
             // console.log(idCurso);
        var filtros = {
            'idcurso': idCurso,
            'nombre':texto
        };
        cargarVideos(filtros);
        // borrarSobrantes($(this));
        if(idCurso==0){ return false; }
        // $select_new = nuevoSelectFiltro(idPadre);
        // getCursoDetalle({
        //     'idcurso': idCurso,
        //     'idpadre': idPadre,
        // }, $select_new);
    });
    $('#audiosmostrar').on('click', function(e) {
        var idCurso = $('#opcIdCurso').val() || 0;
        var texto = $('#texto').val() || 0;
             // console.log(idCurso);
        var filtros = {
            'idcurso': idCurso,
            'nombre':texto
        };
        cargarAudios(filtros);
        // borrarSobrantes($(this));
        if(idCurso==0){ return false; }
        // $select_new = nuevoSelectFiltro(idPadre);
        // getCursoDetalle({
        //     'idcurso': idCurso,
        //     'idpadre': idPadre,
        // }, $select_new);
    });
    $('#musicamostrar').on('click', function(e) {
        var idCurso = $('#opcIdCurso').val() || 0;
        var texto = $('#texto').val() || 0;
             // console.log(idCurso);
        var filtros = {
            'idcurso': idCurso,
            'nombre':texto
        };
        cargarMusica(filtros);
        // borrarSobrantes($(this));
        if(idCurso==0){ return false; }
        // $select_new = nuevoSelectFiltro(idPadre);
        // getCursoDetalle({
        //     'idcurso': idCurso,
        //     'idpadre': idPadre,
        // }, $select_new);
    });
    $('#opcIdCurso').change(function(e){
        if ($('#tabmusica').hasClass( "active" ).toString() == 'true') {
            $("#musicamostrar").trigger( "click" );
        }
    });
    $("#musicamostrar").trigger( "click" );
    // $('#tarea-list').on('click', '.panel.tarea-item .opciones .eliminar', function(e) {
    //     e.preventDefault();
    //     var idtarea = $(this).attr('data-idtarea');
    //     $.confirm({
    //         title: '<?php echo JrTexto::_('Delete');?>',
    //         content: '<?php echo JrTexto::_('Are you sure to delete this record?'); ?>',
    //         confirmButton: '<?php echo JrTexto::_('Accept');?>',
    //         cancelButton: '<?php echo JrTexto::_('Cancel');?>',
    //         confirmButtonClass: 'btn-green2',
    //         cancelButtonClass: 'btn-red',
    //         closeIcon: true,
    //         confirm: function(){
    //             $.ajax({
    //                 url: _sysUrlBase_+'/tarea/xEliminar_logica',
    //                 type: 'POST',
    //                 dataType: 'json',
    //                 data: {'idtarea': idtarea},
    //             }).done(function(resp) {
    //                 if(resp.code=='ok'){
    //                     $('#tarea-list').find('.panel.tarea-item[data-idtarea="'+idtarea+'"]').each(function() { $(this).closest('[class^="col-"]').remove(); });
    //                 }else{
    //                     mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.msj, 'error');
    //                 }
    //             }).fail(fnAjaxFail);
    //         },
    //     });
    // });

    /***** escribir Tab en la URL *****/
    // var url = document.location.toString();
    // if (url.match('#')) {
    //     $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    // }
    // $('.nav-tabs a').on('shown.bs.tab', function (e) {
    //     window.location.hash = e.target.hash;
    // })
    /**** FIN escribir Tab en la URL ****/
});
</script>