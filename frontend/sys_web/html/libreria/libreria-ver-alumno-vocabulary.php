<?php 
$RUTA_BASE = $this->documento->getUrlBase();
$arrSkills = $frm = array();
if(!empty($this->datos)) $frm=$this->datos;
// print_r($frm);
if(!empty(@$frm["habilidades"])) $arrSkills=json_decode(@$frm["habilidades"], true);

?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-add">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <?php
                if ($this->idrol==1) {
                   ?>
                    <li><a href="<?php echo $this->documento->getUrlBase();?>/library/"><?php echo ucfirst(JrTexto::_('Library')); ?></a></li>
                   <?php
                }else{
                    ?>
                    <li><a href="<?php echo $this->documento->getUrlBase();?>/library/library"><?php echo ucfirst(JrTexto::_('Library')); ?></a></li>
                   <?php
                }
            ?>  
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>
    
    <form class="form-horizontal" id="frmAgregarLibrary" name="frmAgregarLibrary">
        <input type="hidden" name="accion" id="accion" value="<?php echo $this->frmaccion; ?>">
        <input type="hidden" name="pkIdestudio" id="pkIdestudio" value="<?php echo @$frm['idtarea']; ?>">
        <input type="hidden" name="opcIdactividad" id="idactividad2" value="" />
        <div class="panel pnl-contenedor <?php if($this->frmaccion=='Nuevo'){echo 'panel-default'; } ?>">
            <?php if($this->frmaccion=='Editar'){ ?>
            <div class="panel-heading bg-blue" style="">
                <h3 class="panel-title" style="font-size: 1.8em;">
                    <i class="fa fa-briefcase"></i> &nbsp;<?php echo ucfirst(@$frm['nombre']); ?>
                </h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <?php } ?>
            <div class="panel-body">
<!--                 <div class="row " id="filtros-actividad"> 
                    <div class="col-xs-12 col-sm-3">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel" required>
                                    <option value="0" >- <?php echo ucfirst(JrTexto::_("Select course")); ?> <span>*</span> -</option>
                                    <?php if(!empty($this->cursos)){
                                    foreach ($this->cursos  as $c) {
                                        // echo $this->idcurso.' '.$c["idcurso"];
                                        echo '<option value="'.$c["idcurso"].'" '.(($this->idcurso==$c["idcurso"])?'selected="selected"':''). '>'.$c["nombre"].'</option>';
                                    }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                   
                </div> -->
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <div class="form-group">
                            <label for="txtFoto" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Cover')); ?></label>
                            <div class="col-xs-12 col-sm-9" style="text-align: center;">
                            	<div class="col-xs-12 col-sm-12">
                            		<?php
                                         $imgSrc=($this->frmaccion=='Nuevo')?$this->documento->getUrlStatic().'/media/web/nofoto.jpg':$this->documento->getUrlStatic().'/media/web/noaudio.png'; 
                                    ?>
                                    <img src="<?php echo $imgSrc; ?>" alt="cover" class=" " style="border-width: 10px;border-style: double;width: 75%">
                            	</div>
                                <!-- <a href="<?php echo $this->documento->getUrlBase().'/biblioteca/verpdf/?link='.$this->documento->getUrlStatic().'/libreria/pdf/'.@$frm['archivo'];?>" class="aworkbook hvr-float-shadow"> -->
                                    
                                <!-- </a> -->
                                
                                <a href="#" id="activaraudios" class="btn btn-blue editarlibro" style='margin-top:5%;'><?php echo ucfirst(JrTexto::_("View"))?></a>
                            </div>
                            <div class="col-xs-12 col-sm-12">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <div class="form-group">
                            <label for="txtNombre" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Title')); ?></label>
                            <div class="col-xs-12 col-sm-9">
                                <input type="text" id="txtNombre" name="txtNombre" class="form-control" autocomplete="off" disabled value="<?php echo @$frm['nombre'];?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Objectives of activity')); ?></label>
                            <div class="col-xs-12 col-sm-9">
                                <textarea type="text" id="txtDescripcion" name="txtDescripcion" class="form-control" disabled rows="5"><?php echo @$frm['descripcion'];?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id='alum-palabras_table'style='display: none;'>
                	
                	<div class="col-xs-12 col-sm-offset-2 col-sm-8">
			        <table class="table table-striped table-hover" id="tblPalabras">
                        <thead>
                            <tr class="bg-orange">
                                <?php if(!empty($this->datos1)) {
                                foreach ($this->datos1 as $idagrupacion=>$arrPalabras) { 
                                    foreach ($arrPalabras as $j=>$p) {?>
                                <th><span class="pull-right">A-Z</span></th>
                                <?php  } break; } } ?>
                            </tr>
                        </thead>
                        <tbody>
                             <?php foreach ($this->datos1 as $idagrupacion=>$arrPalabras) { ?>
                             <tr data-idagrupacion="<?php echo $idagrupacion; ?>">
                                <?php foreach ($arrPalabras as $j=>$p) { ?>
                                <td>
                                    <button class="btn btn-xs btn-orange <?php echo empty($p['audio'])?'pronunciar':'btn-audio'; ?>" data-idaudio="audio_<?php echo $p['idpalabra']; ?>"><i class="fa fa-volume-down"></i></button>
                                    <span class="texto"><?php echo $p["palabra"]; ?></span>

                                    <?php if(!empty($p["audio"])){
                                        $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $p["audio"]);  ?>
                                    <audio src="<?php echo $src; ?>" id="audio_<?php echo $p['idpalabra']; ?>" class="hidden" onended="endAudio(this)"></audio>
                                    <?php } ?>

                                </td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
			    </div>
                </div>
            </div>
            <?php if($this->frmaccion=='Nuevo'){ ?>
            <div class="panel-footer text-right">
                <button class="btn btn-blue guardartarea"><?php echo JrTexto::_('Save and Continue'); ?></button>
            </div>
            <?php } ?>
        </div>
    </form>
    
    <?php if($this->frmaccion=='Editar') {
    $arrTarea_Asignaciones=@$frm['tarea_asignacion'];
    if(!empty($arrTarea_Asignaciones)){
    foreach ($arrTarea_Asignaciones as $asign) { ?>
    <form class="form-horizontal frmAsignacion" id="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" name="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" data-id="<?php echo $asign['idtarea_asignacion']; ?>">
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-success">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <small class="sr-only">
                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    <?php echo date('d-m-Y', strtotime($asign['fechaentrega'])).' '.date('h:i a', strtotime($asign['horaentrega'])); ?>
                </small>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcColegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php foreach ($this->locales as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcAula">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcGrupo">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Date/Time of presentation')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <div class="input-group datetimepicker fechaentrega">
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php }} ?>
    <div class="row" id="botones-accion"> <div class="col-xs-12 text-center" style="margin-bottom: 20px;">
        <a href="<?php echo $this->documento->getUrlBase();?>/library" class="btn btn-lg btn-default pull-left"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back to list")?></a>
        <!--button class="btn btn-lg btn-green agregar_asignacion"><i class="fa fa-users"></i> <?php //echo JrTexto::_("Assign to students")?></button-->
        <button class="btn btn-lg btn-blue pull-right guardar_edicion"><i class="fa fa-save"></i> <?php echo JrTexto::_("Save")?></button>
    </div> </div>
    <?php } ?>
</div>

<section class="hidden">
    <!-- select-box nivel del curso -->
    <div class="col-xs-12 col-sm-3 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>

    <!-- contenido de moda-body para tipo_adjunto="Enlace" -->
    <div id="adjuntar_link">
        <form class="form-horizontal" id="frm-adjuntar_link" name="frm-adjuntar_link">
            <input type="hidden" name="txtTipo" id="txtTipo" value="L">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group">
                <label for="txtRuta" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Link'); ?> (*)</label>
                <div class="col-xs-12 col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-link"></i></span>
                        <input type="text" name="txtRuta" id="txtRuta" class="form-control" placeholder="<?php echo JrTexto::_('e.g.'); ?>: http://www.webpage.com"  autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="txtNombre" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-6">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control">
                </div>
            </div>
        </form>
    </div>

    <!-- contenido de moda-body para tipo_adjunto="GrabacionVoz" -->
    <div id="adjuntar_grabacionvoz">
        <form class="form-horizontal" id="frm-adjuntar_grabacionvoz" name="frm-adjuntar_grabacionvoz">
            <input type="hidden" name="txtTipo" id="txtTipo" value="G">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group ">
                <label for="txtNombre" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-8">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control txtNombre" value="<?php echo JrTexto::_('recording');?>" data-uniqid="<?php echo uniqid();?>">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger grabarme" data-estado="stopped"><i class="fa fa-circle"></i> <span><?php echo JrTexto::_('Rec'); ?></span></button>
                <a class="btn btn-primary reproducir"  data-estado="paused" disabled="disabled"><i class="fa fa-play"></i> <span><?php echo JrTexto::_('Play'); ?></span></a>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <canvas class="thumbnail barras_voz" id="barras_voz" style="height: 40px; padding-left: 0; padding-right: 0; width: 100%;"></canvas>
                    <canvas class="thumbnail onda_voz" id="onda_voz" style="height: 100px; padding-left: 0; padding-right: 0; width: 100%;"></canvas>
                    <!--div class="thumbnail onda_voz" id="onda_voz" style="min-height: 100px; padding-left: 0; padding-right: 0;"></div-->
                </div>
            </div>
            <audio class="hidden" id="recording_player" onended="endedRecordingPlayer(this)"></audio>
        </form>
    </div>

    <!-- contenido de moda-body para tipo_adjunto="Actividad","Juego","Examen" -->
    <div id="adjuntar_act_gam_exa">
        <form class="form-horizontal" id="" name="">
            <input type="hidden" name="txtTipo" id="txtTipo" value="">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group ">
                <div class="col-sm-offset-3 col-xs-12 col-sm-6">
                    <div class="input-group">
                        <input type="text" id="txtBuscar" name="txtBuscar" class="form-control" placeholder="<?php echo JrTexto::_('Search'); ?>...">
                        <span class="input-group-addon btn btnbuscar"><i class="fa fa-search"></i></span>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12 divResultados">
            </div>
        </div>
        <div class="row divVistaTabs">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                </ul>
            </div>
            <div class="col-xs-12 tab-content">
            </div>
        </div>
    </div>

    <?php if($this->frmaccion=='Editar'){ ?>
    <!-- nuevo FORM para asignar la atrea a alumno(s) -->
    <form class="form-horizontal frmAsignacion" id="frmAsignacion" name="" data-id="">
        <?php if($this->frmaccion=='Editar'){ ?>
        <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
        <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
        <?php } ?>
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-green">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcColegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php 
                                    if(!empty($this->locales))
                                    foreach ($this->locales as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcAula">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcGrupo">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos" style="display: none;">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Date/Time of presentation')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <div class="input-group datetimepicker fechaentrega">
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php } ?>
</section>

<script>

$('.istooltip').tooltip();
var _sysUrlSmartquiz_ = '<?php echo URL_SMARTQUIZ; ?>';
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;

var fnAjaxFail = function(xhr, textStatus, errorThrown) {
    //console.log("Error");
    console.log(xhr);
    console.log(textStatus);
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    throw errorThrown;
};
function endAudio(audio) {
    $(audio).removeClass('playing');
    $(audio).removeClass('paused');
}
var guardarTarea = function() {
    $('#frmAgregarLibrary *[required]').each(function(i, elem) {
        var value = $(elem).val();
        console.log(value);
    });
    // if($('#frmAgregarTarea .has-error').length>=1){ return false; }
    // console.log($('select[name=opcIdcursodetalle]').val());
    // if($('select[name=opcIdcursodetalle]').val() != 0){
    //     $('#idactividad2').val($('select[name=opcIdcursodetalle]').val());
    // }
    $.ajax({
        url: _sysUrlBase_+'/library/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: $('#frmAgregarLibrary').serialize(),
    }).done(function(resp) {
        if(resp.code=='ok'){
            var id_estudio = resp.data;
            if($('#frmAgregarLibrary #accion').val()=="Nuevo"){
                var idcurso=$('#opcIdCurso').val();
                return redir(_sysUrlBase_+'/library/editar/?id_estudio='+id_estudio);
            }
            mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Data updated') ?>','success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');

        }
    }).fail(fnAjaxFail);
}
var initEdit = function(){
    var idtema=<?php echo !empty(@$frm['idtema'])?@$frm['idtema']:'null'; ?>;
    $('#pkIdestudio').val(idtema);
    $('#opcIdactividad').val(0); 
};

$(document).ready(function() {
	$('#activaraudios').on('click',function(){
		$('#alum-palabras_table').show();
	});
	$('#tblPalabras').DataTable({
        "paging":   false,
        "info":     false
    });

    $(".btn-audio").click(function(e) {
        var idAudio = $(this).attr('data-idaudio');
        var $audio = $('#'+idAudio);
        if( $audio.length ){
            if( !$audio.hasClass('playing') ){
                $('audio.playing').each(function(){
                    this.pause(); this.currentTime = 0;  /*stopping audio */
                    endAudio(this);
                });
                $audio.addClass('playing');
                $audio.removeClass('paused');
                $audio.trigger('play');
            } else {
                $audio.addClass('paused');
                $audio.removeClass('playing');
                $audio.trigger('pause');
            }
        }
        return false;
    });
    $('#filtros-actividad').on('change', '.select-nivel', function(e) {
        var idPadre = 0;
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        // borrarSobrantes($(this));
        if(idCurso==0){ 
            $('.contenedor-lista-habilidades table tbody').html('');
            return false; 
        }
    });

    $('.btnportada').click(function(e){ 
        var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
        selectedfile(e,this,txt);
    });

    $('.btnportada img').load(function() {
        var src=$(this).attr('src');
        $('#txtFoto').val(src);
    });

<?php if($this->frmaccion=='Editar'){ ?>
    /********************** Editar **********************/
    $('#botones-accion').on('click', '.agregar_asignacion', function(e) {
        e.preventDefault();
        var now = Date.now();
        var idFrm = $('section #frmAsignacion').attr('id');
        var $frm = $('section #frmAsignacion').clone();
        $frm.attr({ 'id': idFrm+'_'+now, 'name': idFrm+'_'+now, });
        $('#botones-accion').before($frm);
        initFechaYHora($('#'+idFrm+'_'+now).find('.datetimepicker.fechaentrega'), $('#'+idFrm+'_'+now).find('.datetimepicker.horaentrega'));
    }).on('click', '.guardar_edicion', function(e) {
        e.preventDefault();
        guardarTarea();
        // guardarAsignaciones();
    });

    initEdit();
<?php } ?>

});
</script>