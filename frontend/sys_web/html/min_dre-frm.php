<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Min_dre'">&nbsp;<?php echo JrTexto::_('DRE'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIddre" id="pkiddre" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDescripcion">
              <?php echo JrTexto::_('Descripcion');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDescripcion" name="txtDescripcion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["descripcion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 ">
                <div class="select-ctrl-wrapper select-azul">
                <select id="txtUbigeo" class="form-control select-ctrl " name="txtUbigeo">
                </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtOpcional">
              <?php echo JrTexto::_('Opcional');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtOpcional" name="txtOpcional" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["opcional"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"><hr></div>
          <div class="form-group">
            <div class="col-md-12 text-center">
              <button id="btn-saveMin_dre" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning cerramodal" href="<?php echo JrAplicacion::getJrUrl(array('min_dre'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var id_ubigeo='<?php echo !empty($frm["ubigeo"])?$frm["ubigeo"]:"010000" ?>';
  var _cargarcombos=function(){
    var fd = new FormData();
        fd.append("provincia", "00");
        fd.append("distrito", "00");
        fd.append("pais", "PE");       
    sysajax({
      fromdata:fd,
      url:_sysUrlBase_+'/ubigeo/buscarjson',
      callback:function(rs){
        $sel=$('#txtUbigeo');
          dt=rs.data;
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.id_ubigeo+'" '+(v.id_ubigeo==id_ubigeo?'selected="selected"':'')+ '>'+v.id_ubigeo +" : "+ v.ciudad+'</option>');
          })
      }})}
    _cargarcombos();

$('#frm-<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'min_dre', 'saveMin_dre', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Min_dre"))?>');
      }
     }
  });
});


</script>

