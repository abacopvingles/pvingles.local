<?php 

    defined("RUTA_BASE") or die(); 
    $urlbase = $this->documento->getUrlBase();
    $RUTA_BASE = $this->documento->getUrlBase();
    
    $frm=!empty($this->datos_Perfil)?$this->datos_Perfil:"";
    $fotouser=!empty($frm["foto"])?$frm["foto"]:'user_avatar.jpg';

    function conversorSegundosHoras($tiempo_en_segundos) {
        $horas = floor($tiempo_en_segundos / 3600);
        $minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
        $segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);
    
        return $horas . ':' . $minutos . ":" . $segundos;
    }  
?>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>
<style>
.back1{
    background-color:#bbe898;
}

</style>

<style type="text/css">
  .row >.item-recurso{
    text-align: center;
    padding: 1ex;
  }
  .row >.item-recurso .panel{
    text-align: center;
    padding: 0.2ex;
    margin:0.05ex; 0.1ex;
  }
  .row >.item-recurso .panel-body{
    padding: 0.1ex;
  }
    .slick-slide{
    position: relative;
  }

  .cajaselect {  
     overflow: hidden;
     width: 230px;
     position:relative;
     font-size: 1.8em;
  }
  select#level-item,select#unidad-item ,select#actividad-item {
     background: transparent;
     border: 2px solid #4683af;   
     padding: 5px;
     width: 250px;
     padding: 0.3ex 2ex; 
  }
  select:focus{ outline: none;}

  .cajaselect::after{
     font-family: FontAwesome;
     content: "\f0dd";
    display: inline-block;
    text-align: center;
    width: 30px;
    height: 100%;
    background-color: #4683af;
    position: absolute;
    top: 0;
    right: 0px;
    pointer-events: none;
    color: antiquewhite;
    bottom: 0px;
  }

  .titulo{
    border: solid 0px #f00;position: relative; top:30px; left: 15px; z-index: 1000;
    width: 85%;
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .autor{
    border: solid 0px #f00;position: relative; bottom: 28px; left: 12px; z-index: 1000;
    width: 85%; text-align: right; 
    background-color: rgba(255, 255, 255, 0.53);
      color: #000;
  }
  .caratula{
    border: solid 0px #f00;position: absolute; top:38px; left:25px;  width: 75%; height: 68%  
  }
.border{
  border: solid 0px #f00;
}

.border2{
  border: solid 0px #337AB7; border-radius: 5px;
}

.div_mante {
  opacity: 0;
  -webkit-transition: opacity 500ms;
  -moz-transition: opacity 500ms;
  -o-transition: opacity 500ms;
  -ms-transition: opacity 500ms;
  transition: opacity 500ms;
}

.card{
    display: block;
    width: 100%;
    border: 1px solid #ccc;
    padding: 0;
}
.card .card-title{
    font-size: 15px;
    font-weight: bold;
    border-bottom: 1px solid #ccc;
    padding-left: 15px;
    line-height: 2.2;
}
.card .card-info{
    font-size: 2.5em;
    text-shadow: 2px 1px 3px #aaa;
    font-weight: bolder;
    text-align: center;
    border-bottom: 1px solid #efefef;
}
.card .card-info:last-child{ border-bottom:none; }

.card .card-info .info-tiempo.tiempo-optimo{ font-size: .7em; }

.card.card-time .card-info .anio:after,
.card.card-time .card-info .mes:after,
.card.card-time .card-info .dia:after,
.card.card-time .card-info .hora:after,
.card.card-time .card-info .min:after,
.card.card-time .card-info .seg:after{
    font-size: 0.7em;
}
.card.card-time .card-info .anio:after{ content: "y"; }
.card.card-time .card-info .mes:after { content: "m"; }
.card.card-time .card-info .dia:after { content: "d"; }
.card.card-time .card-info .hora:after{ content: "h"; }
.card.card-time .card-info .min:after { content: "m"; }
.card.card-time .card-info .seg:after { content: "s"; }

.card .card-info .promedio{
    line-height: 75px;
}
.card .card-info .comentario span{
    display: block;
}
.card .card-info .comentario span.letras{
    font-size: .5em;
}

</style>


<div class="container">
  <div class="row " id="levels" style="padding-top: 1ex; ">
            
             
               

    </div>

  


<div class="row " id="levels" style="padding-top: 1ex; ">

<div class = 'panel panel-primary' >
    <div class = 'panel-heading' style="text-align: left;">
    <?php echo JrTexto::_("Reporte")?>
    </div>

    <div class = 'panel-body' style="text-align: center;  " >
<div class="container">
<div class="row" style="margin:15px auto;">
<div class="col-md-3" style="height:90px;">
<img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/<?php echo $fotouser; ?>" alt="foto" class="img-responsive center-block img-circle" style="max-height: 150px; max-width: 150px; width:100%; height:100%; ">  
</div>
<div class="col-md-9">
<div class="" style="text-align:left;">
<h4><?php echo $this->user["nombre_full"]; ?></h4>
</div>
</div>
</div>
</div>
      

       
        <?php
        $reporte='var data = google.visualization.arrayToDataTable([
          ["Element", "Tiempo", { role: "style" } ],';

        $fondocolor="0099FF,99CC33,FF6600,FFCC00,FF6699,";
        $colores=explode(",",$fondocolor);
        
        $horasCount = null;

        if(!empty($this->lista_tiempo)){
          foreach ($this->lista_tiempo as $lista1){
            $totalpv=conversorSegundosHoras($lista1['tiempo']);
            //echo $totalpv."<br>";
  
            
            $horas=explode(":",$totalpv);
            
            $hora=$horas[0];          
            if (!$hora) $hora=0;          
            $min=$horas[1];
            if (!$min) $min=0;
            $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');
            
            $horasCount = $horas;

            $reporte.='
            ["PV", '.$totalpv.', "#'.$colores[0].'"],';
          }//for
        }
        
        $x=1;
        if(!empty($this->lista_tiempoc))
        foreach ($this->lista_tiempoc as $lista2){
          $curso=$lista2['nombre'];
          $totalpv=conversorSegundosHoras($lista2['tiempo']);
          
          $horas=explode(":",$totalpv);
          $hora=$horas[0];          
          if (!$hora) $hora=0;          
          $min=$horas[1];
          if (!$min) $min=0;
          $totalpv=number_format($hora.'.'.$min, 2, '.', ' ');

          
          $reporte.='
          ["'.$curso.'", '.$totalpv.', "#'.$colores[$x].'"],';
          $x++;
          if ($x==4) $x=0;

        }//for

        /*for ($i=1;$i<=50;$i++){
          $ale = rand (1,100);
          $reporte.='
          ["'.$curso.'", '.$ale.', "#'.$colores[$x].'"],';
          $x++;
          if ($x==4) $x=0;
        }*/


        $reporte.=']);';
        ?>

      <!--EMMY HERE-->
      <div class="col-xs-12" id="tiempoPV" style="padding: 0;" >
        <div class="card card-time">
            <div class="card-title"><?php echo JrTexto::_('Tiempo de estudio en la Plataforma Virtual'); ?></div>
            <div class="card-info">
                <div class="col-xs-12  resultado_tiempos"> <!--col-sm-6-->
                    <div class="info-tiempo tiempo-obtenido">
                        <span class="anio" style="display: none;">00</span>
                        <span class="mes" style="display: none;">00</span>
                        <span class="dia" style="display: none;">00</span>
                        <span class="hora"><?php echo (!empty($horasCount[0])) ? $horasCount[0] : '0'; ?></span>
                        <span class="min"><?php echo (!empty($horasCount[1])) ? $horasCount[1] : '0'; ?></span>
                        <span class="seg"><?php echo (!empty($horasCount[2])) ? $horasCount[2] : '0'; ?></span>
                    </div>
                    <!-- <div class="info-tiempo tiempo-optimo" title="<?php echo JrTexto::_('Optimal time'); ?>">
                        <span class="anio" style="display: none;">00</span>
                        <span class="mes" style="display: none;">00</span>
                        <span class="dia" style="display: none;">00</span>
                        <span class="hora">00</span>
                        <span class="min">00</span>
                        <span class="seg">00</span>
                    </div> -->
                </div>
                <!-- <div class="col-xs-6 col-sm-3 porcentaje_tiempos color-success">
                    <p class="porcentaje">100%</p>
                    <p class="mensaje" style="font-size:12px"><?php echo JrTexto::_('Missing time to reach optimum time'); ?>: <span>00:00:00</span></p>
                </div>
                <div class="col-xs-6 col-sm-3 comentario color-warning">
                    <span><i class="fa fa-thumbs-up"></i></span>
                    <span class="letras">BUENO</span>
                </div> -->
            </div>
        </div>
      </div>
      <!--End Emmy-->
        <div id="columnchart_values" class="col-md-12" style="overflow: auto" ></div>
      
    </div>
</div>



</div>
</div>



<script type="text/javascript">

// var tiempoXFiltro = function(idBuscar=null, tipo=''){
//     // mostrarCargando('#tiempos');
//     var str_url = '';
//     if(idBuscar==null || idBuscar==-1 || tipo=='') return false;
//     if(tipo=='A') str_url = _sysUrlBase_+'/historial_sesion/tiempoxalumno/';
//     else return false;
//     // else if(tipo=='G') str_url = _sysUrlBase_+'/historial_sesion/tiempoxgrupo/';
        
//     if($('#pnl-datosfiltro .ultima-actividad').length>0){
//         var orden=0;
//         var ultima_actividad={};
//         $('#pnl-datosfiltro .ultima-actividad').each(function(index, el) {
//             var $this=$(el);
//             if(parseInt($this.data('orden')) > orden){
//                 ultima_actividad={
//                     'idnivel':$this.data('idnivel'),
//                     'idunidad':$this.data('idunidad'),
//                     'idactividad':$this.data('idactividad'),
//                 };
//             }
//         });
//     }else{
//         $('#tiempos .inicial h3').html('<p><?php echo JrTexto::_('The students or students have not worked any activity'); ?>.</p><p><?php echo JrTexto::_('You can not verify times for this students or students'); ?>.</p>')
//         return false;
//     }

//     if(str_url=='') return false;
//     $.ajax({
//         url: str_url,
//         type: 'POST',
//         async:false,
//         dataType: 'json',
//         data: {'id': idBuscar, 'tipo': tipo, 'ultima_actividad':JSON.stringify(ultima_actividad), 'idcurso':FILTRO.idcurso},
//     }).done(function(resp) {
//         console.log(resp);
//         if(resp.code=='ok'){
//             var tiempo = resp.data;
//             var configProf = resp.config_docente;
//             var tiempoJson = {};
//             /*if($('#tiempos .contenido').hasClass('slick-initialized')){
//                  $('#tiempos .contenido').slick('unslick');
//             }*/
//             $.each(tiempo, function(key, val) {
//                 if(key=='total'){ var $pnl_tiempo=$('#tiempo_total'); var optimo=configProf.tiempototal; }
//                 else if(key=='actividades'){ var $pnl_tiempo=$('#tiempo_actividades'); var optimo=configProf.tiempoactividades; }
//                 else if(key=='juegos'){ var $pnl_tiempo=$('#tiempo_games'); var optimo=configProf.tiempogames; }
//                 else if(key=='teacherresrc'){ var $pnl_tiempo=$('#tiempo_teacher_resrc'); var optimo=configProf.tiempoteacherresrc; }
//                 else if(key=='examenes'){ var $pnl_tiempo=$('#tiempo_examenes'); var optimo=configProf.tiempoexamenes; }
                
//                 if( isJSON(configProf.escalas) ) {
//                     var arr = JSON.parse(configProf.escalas);
//                     if(arr.length>0){ ESCALAS = arr; }
//                 }
                
//                 if($pnl_tiempo!==undefined){
//                     $cardObtenido = $pnl_tiempo.find('.tiempo-obtenido');
//                     $cardOptimo = $pnl_tiempo.find('.tiempo-optimo');
//                     mostrarInfoTiempo($cardObtenido, val);
//                     mostrarInfoTiempo($cardOptimo, optimo);
//                     analizarTiempos(val, optimo, $pnl_tiempo);
//                 }
//             });

//             $('#tiempos .inicial').hide();
//             $('#tiempos .contenido').show();
//             //$('#tiempos .contenido').slick();
//         }else{
//             mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
//         }
//     }).fail(fnAjaxFail);
// };

// </script>

    
  
   
