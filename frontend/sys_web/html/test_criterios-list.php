<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_("Test_criterios"); ?></li>       
    </ol>
</div> </div>
<?php } ?>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                                                  
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbidtest" id="cbidtest" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                                                        
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbmostrar" id="cbmostrar" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Test_criterios", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Test_criterios").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Idtest") ;?></th>
                    <th><?php echo JrTexto::_("Criterio") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5c50666e7ef1f='';
function refreshdatos5c50666e7ef1f(){
    tabledatos5c50666e7ef1f.ajax.reload();
}
$(document).ready(function(){  
  var estados5c50666e7ef1f={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5c50666e7ef1f='<?php echo ucfirst(JrTexto::_("test_criterios"))." - ".JrTexto::_("edit"); ?>';
  var draw5c50666e7ef1f=0;

  
  $('#cbidtest').change(function(ev){
    refreshdatos5c50666e7ef1f();
  });
  $('#cbmostrar').change(function(ev){
    refreshdatos5c50666e7ef1f();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5c50666e7ef1f();
  });
  tabledatos5c50666e7ef1f=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Idtest") ;?>'},
            {'data': '<?php echo JrTexto::_("Criterio") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/test_criterios/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idtest=$('#cbidtest').val(),
             d.mostrar=$('#cbmostrar').val(),
             //d.texto=$('#texto').val(),
                        
            draw5c50666e7ef1f=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5c50666e7ef1f;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Idtest") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="idtest"  data-id="'+data[i].idtestcriterio+'"> <i class="fa fa'+(data[i].idtest=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5c50666e7ef1f[data[i].idtest]+'</a>',
              '<?php echo JrTexto::_("Criterio") ;?>': data[i].criterio,
                '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idtestcriterio+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5c50666e7ef1f[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/test_criterios/editar/?id='+data[i].idtestcriterio+'" data-titulo="'+tituloedit5c50666e7ef1f+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idtestcriterio+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'test_criterios', 'setCampo', id,campo,data);
          if(res) tabledatos5c50666e7ef1f.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5c50666e7ef1f';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Test_criterios';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'test_criterios', 'eliminar', id);
        if(res) tabledatos5c50666e7ef1f.ajax.reload();
      }
    }); 
  });
});
</script>