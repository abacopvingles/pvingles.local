<?php 
$RUTA_BASE = $this->documento->getUrlBase();
$arrSkills = $frm = array();
if(!empty($this->datos)) $frm=$this->datos;
if(!empty(@$frm["habilidades"])) $arrSkills=json_decode(@$frm["habilidades"], true);

?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-add">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('Activity')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>
    
    <form class="form-horizontal" id="frmAgregarTarea" name="frmAgregarTarea">
        <input type="hidden" name="accion" id="accion" value="<?php echo $this->frmaccion; ?>">
        <input type="hidden" name="pkIdtarea" id="pkIdtarea" value="<?php echo @$frm['idtarea']; ?>">
        <input type="hidden" name="opcIdactividad" id="idactividad2" value="" />
        <div class="panel pnl-contenedor <?php if($this->frmaccion=='Nuevo'){echo 'panel-default'; } ?>">
            <?php if($this->frmaccion=='Editar'){ ?>
            <div class="panel-heading bg-blue" style="">
                <h3 class="panel-title" style="font-size: 1.8em;">
                    <i class="fa fa-briefcase"></i> &nbsp;<?php echo ucfirst(@$frm['nombre']); ?>
                </h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <?php } ?>
            <div class="panel-body">
                <div class="row " id="filtros-actividad"> 
                    <div class="col-xs-12 col-sm-3">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                                    <option value="0">- <?php echo ucfirst(JrTexto::_("Select course")); ?> <span>*</span> -</option>
                                    <?php if(!empty($this->cursos)){
                                    foreach ($this->cursos  as $c) {
                                        echo $this->idcurso.' '.$c["idcurso"];
                                        echo '<option value="'.$c["idcurso"].'" '.(($this->idcurso==$c["idcurso"])?'selected="selected"':''). '>'.$c["nombre"].'</option>';
                                    }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="txtNombre" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Title')); ?> <span>*</span></label>
                            <div class="col-xs-12 col-sm-9">
                                <input type="text" id="txtNombre" name="txtNombre" class="form-control"  autocomplete="off" required value="<?php echo @$frm['nombre'];?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Objectives of activity')); ?></label>
                            <div class="col-xs-12 col-sm-9">
                                <textarea type="text" id="txtDescripcion" name="txtDescripcion" class="form-control" rows="5"><?php echo @$frm['descripcion'];?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtFoto" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Cover')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <a href="#" class="thumbnail btnportada istooltip" data-tipo="image" data-url=".img-portada" title="<?php echo ucfirst(JrTexto::_('Select an image')); ?>">
                                    <?php
                                     $imgSrc=($this->frmaccion=='Nuevo')?$this->documento->getUrlStatic().'/media/web/nofoto.jpg':str_replace('__xRUTABASEx__', $RUTA_BASE, @$frm['foto']); 
                                    ?>
                                    <img src="<?php echo $imgSrc; ?>" alt="cover" class="img-responsive img-portada">
                                    <input type="hidden" id="txtFoto" name="txtFoto" value="<?php echo $imgSrc; ?>">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="txtHabilidades" class="col-xs--12 col-sm-3 control-label"><?php echo JrTexto::_("Skills"); ?><span>*</span></label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="contenedor-lista-habilidades">
                                    <table id="lista-habilidades">
                                        <thead>
                                            <tr> <th width="90%"></th> <th width="10%"></th> </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($this->habilidades)) {
                                            foreach ($this->habilidades as $h) { 
                                                $isActive = (in_array($h["idmetodologia"], $arrSkills));
                                                if($h["idmetodologia"]>7) continue;
                                            ?>
                                            <tr>
                                                <td><a href="#" class="list-group-item <?php echo ($isActive)?'active':''; ?>" data-idhabilidad="<?php echo $h["idmetodologia"] ?>"><?php echo $h["nombre"] ?></a></td>
                                                <td><input type="radio" name="opcHabilidad_destacada" class="radio-ctrl <?php echo (!$isActive)?'hidden':''; ?>" value="<?php echo $h["idmetodologia"] ?>" title="<?php echo ucfirst(JrTexto::_("Outstanding skill")); ?>" <?php echo (@$frm["habilidad_destacada"]==$h["idmetodologia"])?'checked="checked"':'' ?> ></td>
                                            </tr>
                                            <?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" id="txtHabilidades" name="txtHabilidades" value='<?php echo @$frm['habilidades']; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="opcPuntajemaximo" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Max. score")?> <span>*</span></label>
                            <div class="col-xs-12 col-sm-3 select-ctrl-wrapper select-azul">
                                <select name="opcPuntajemaximo" id="opcPuntajemaximo" class="form-control select-ctrl">
                                    <?php for ($i=100; $i >= 1; $i--) { 
                                    echo '<option value="'.$i.'" >'.(($i<10)?"0":"").$i.'</option>';
                                    } ?>
                                </select>
                            </div>
                            <label for="opcPuntajeminimo" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Min. score to pass")?> <span>*</span></label>
                            <div class="col-xs-12 col-sm-3 select-ctrl-wrapper select-azul">
                                <select name="opcPuntajeminimo" id="opcPuntajeminimo" class="form-control select-ctrl">
                                    <?php for ($i=99; $i >= 0; $i--) { 
                                    echo '<option value="'.$i.'" >'.(($i<10)?"0":"").$i.'</option>';
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Attachment')); ?></label>
                            <div class="col-xs-12 col-sm-9">
                                <input type="file" name="inp_CargadorArchivos" id="inp_CargadorArchivos" class="hidden">
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="documento" title="<?php echo JrTexto::_('Document'); ?>"><i class="fa fa-paperclip"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="video" title="<?php echo JrTexto::_('Video'); ?>"><i class="fa fa-video-camera"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="audio" title="<?php echo JrTexto::_('Audio'); ?>"><i class="fa fa-headphones"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="grabacionvoz" title="<?php echo JrTexto::_('Voice recording'); ?>"><i class="fa fa-microphone"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="enlace" title="<?php echo JrTexto::_('Link'); ?>"><i class="fa fa-link"></i></button>
                                <!--button class="btn btn-default btnadjunto istooltip" data-adjunto="actividad" title="<?php //echo JrTexto::_('Activity'); ?>"><i class="fa fa-font"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="juego" title="<?php //echo JrTexto::_('Game'); ?>"><i class="fa fa-puzzle-piece"></i></button-->
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="examen" title="<?php echo JrTexto::_('Exam'); ?>"><i class="fa fa-list"></i></button>
                            </div>
                            <div class="col-xs-12" style="height: 20px;">
                                <div id="barra-progreso" style="display: none;">
                                    <div class="progress" style="margin: 0;">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only">0%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-offset-3 col-xs-12 col-sm-9 lista-archivos">
                                <input type="hidden" name="txtIdTarea_archivos" id="txtIdTarea_archivos" value="">
                                <table class="table table-striped" id="tblAdjuntos">
                                    <tbody>
                                    <?php if($this->frmaccion=='Editar'){ 
                                    $arrAttach=@$frm['tarea_archivos'];
                                    $arrIcon=array('D'=> 'fa fa-paperclip', 'V'=> 'fa fa-video-camera', 'L'=> 'fa fa-link', 'G'=> 'fa fa-microphone', 'A'=> 'fa fa-font', 'J'=> 'fa fa-puzzle-piece', 'E'=> 'fa fa-list', 'U'=> 'fa fa-headphones' );
                                    if(!empty($arrAttach)){
                                        foreach ($arrAttach as $adj) { ?>
                                        <tr data-id="<?php echo $adj['idtarea_archivos'] ?>">
                                            <td style="width: 7%"><i class="<?php echo $arrIcon[$adj['tipo']] ?>"></i></td>
                                            <td><?php echo $adj['nombre'] ?></td>
                                            <td style="width: 18%">
                                                <a href="<?php $ruta=$adj['ruta'];
                                                if($adj['tipo']=='A' || $adj['tipo']=='J' || $adj['tipo']=='E'){
                                                    $ruta.=$adj['idtarea_archivos'];
                                                }  
                                                echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta); ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                <button title="<?php echo ucfirst(JrTexto::_('Delete')); ?>" class="btn btn-xs btn-default color-red eliminararchivo"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php } } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 text-right color-warning"><b>(*) <small><?php echo JrTexto::_('Required data'); ?>.</small></b></div>
                </div>
            </div>
            <?php if($this->frmaccion=='Nuevo'){ ?>
            <div class="panel-footer text-right">
                <button class="btn btn-blue guardartarea"><?php echo JrTexto::_('Save and Continue'); ?></button>
            </div>
            <?php } ?>
        </div>
    </form>
    
    <?php if($this->frmaccion=='Editar') {
    $arrTarea_Asignaciones=@$frm['tarea_asignacion'];
    if(!empty($arrTarea_Asignaciones)){
    foreach ($arrTarea_Asignaciones as $asign) { ?>
    <form class="form-horizontal frmAsignacion" id="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" name="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" data-id="<?php echo $asign['idtarea_asignacion']; ?>">
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-success">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <small class="sr-only">
                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    <?php echo date('d-m-Y', strtotime($asign['fechaentrega'])).' '.date('h:i a', strtotime($asign['horaentrega'])); ?>
                </small>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcColegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php foreach ($this->locales as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcAula">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcGrupo">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Date/Time of presentation')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <div class="input-group datetimepicker fechaentrega">
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php }} ?>
    <div class="row" id="botones-accion"> <div class="col-xs-12 text-center" style="margin-bottom: 20px;">
        <a href="<?php echo $this->documento->getUrlBase();?>/tarea#pnl-todos" class="btn btn-lg btn-default pull-left"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back to list")?></a>
        <!--button class="btn btn-lg btn-green agregar_asignacion"><i class="fa fa-users"></i> <?php //echo JrTexto::_("Assign to students")?></button-->
        <button class="btn btn-lg btn-blue pull-right guardar_edicion"><i class="fa fa-save"></i> <?php echo JrTexto::_("Save")?></button>
    </div> </div>
    <?php } ?>
</div>

<section class="hidden">
    <!-- select-box nivel del curso -->
    <div class="col-xs-12 col-sm-3 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>

    <!-- contenido de moda-body para tipo_adjunto="Enlace" -->
    <div id="adjuntar_link">
        <form class="form-horizontal" id="frm-adjuntar_link" name="frm-adjuntar_link">
            <input type="hidden" name="txtTipo" id="txtTipo" value="L">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group">
                <label for="txtRuta" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Link'); ?> (*)</label>
                <div class="col-xs-12 col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-link"></i></span>
                        <input type="text" name="txtRuta" id="txtRuta" class="form-control" placeholder="<?php echo JrTexto::_('e.g.'); ?>: http://www.webpage.com"  autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="txtNombre" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-6">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control">
                </div>
            </div>
        </form>
    </div>

    <!-- contenido de moda-body para tipo_adjunto="GrabacionVoz" -->
    <div id="adjuntar_grabacionvoz">
        <form class="form-horizontal" id="frm-adjuntar_grabacionvoz" name="frm-adjuntar_grabacionvoz">
            <input type="hidden" name="txtTipo" id="txtTipo" value="G">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group ">
                <label for="txtNombre" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-8">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control txtNombre" value="<?php echo JrTexto::_('recording');?>" data-uniqid="<?php echo uniqid();?>">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger grabarme" data-estado="stopped"><i class="fa fa-circle"></i> <span><?php echo JrTexto::_('Rec'); ?></span></button>
                <a class="btn btn-primary reproducir"  data-estado="paused" disabled="disabled"><i class="fa fa-play"></i> <span><?php echo JrTexto::_('Play'); ?></span></a>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <canvas class="thumbnail barras_voz" id="barras_voz" style="height: 40px; padding-left: 0; padding-right: 0; width: 100%;"></canvas>
                    <canvas class="thumbnail onda_voz" id="onda_voz" style="height: 100px; padding-left: 0; padding-right: 0; width: 100%;"></canvas>
                    <!--div class="thumbnail onda_voz" id="onda_voz" style="min-height: 100px; padding-left: 0; padding-right: 0;"></div-->
                </div>
            </div>
            <audio class="hidden" id="recording_player" onended="endedRecordingPlayer(this)"></audio>
        </form>
    </div>

    <!-- contenido de moda-body para tipo_adjunto="Actividad","Juego","Examen" -->
    <div id="adjuntar_act_gam_exa">
        <form class="form-horizontal" id="" name="">
            <input type="hidden" name="txtTipo" id="txtTipo" value="">
            <?php if($this->frmaccion=='Editar'){ ?>
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
            <?php } ?>
            <div class="form-group ">
                <div class="col-sm-offset-3 col-xs-12 col-sm-6">
                    <div class="input-group">
                        <input type="text" id="txtBuscar" name="txtBuscar" class="form-control" placeholder="<?php echo JrTexto::_('Search'); ?>...">
                        <span class="input-group-addon btn btnbuscar"><i class="fa fa-search"></i></span>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12 divResultados">
            </div>
        </div>
        <div class="row divVistaTabs">
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                </ul>
            </div>
            <div class="col-xs-12 tab-content">
            </div>
        </div>
    </div>

    <?php if($this->frmaccion=='Editar'){ ?>
    <!-- nuevo FORM para asignar la atrea a alumno(s) -->
    <form class="form-horizontal frmAsignacion" id="frmAsignacion" name="" data-id="">
        <?php if($this->frmaccion=='Editar'){ ?>
        <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
        <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
        <?php } ?>
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-green">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcColegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php 
                                    if(!empty($this->locales))
                                    foreach ($this->locales as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcAula">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl opcGrupo">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos" style="display: none;">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Date/Time of presentation')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <div class="input-group datetimepicker fechaentrega">
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php } ?>
</section>

<script>
$('.istooltip').tooltip();
var _sysUrlSmartquiz_ = '<?php echo URL_SMARTQUIZ; ?>';
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;

var fnAjaxFail = function(xhr, textStatus, errorThrown) {
    //console.log("Error");
    //console.log(xhr);
    //console.log(textStatus);
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    throw errorThrown;
};

var $contenedorFiltros = $('#filtros-actividad');
var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            var options = '';
            if(resp.data.length) {
                var tienesesion=esunidad=false;
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso=="L"){
                        options += '<option value="'+det.idrecurso+'" data-recurso="L" >'+det.nombre+'</option>';
                        tienesesion=true;
                    }else if(det.tiporecurso=="U"){
                        options += '<option value="'+det.idcursodetalle+'" data-recurso="U" >'+det.nombre+'</option>';
                        esunidad=true;
                    }else if(det.tiporecurso!='E') options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                });
                $select.addClass('select-cursodetalle');
                $select.append(options);
                $select.closest('.form-group').parent().removeClass('hidden');
                if(tienesesion) $select.addClass('tienesesion').attr('name','opcIdcursodetalle');
                if(esunidad) $select.addClass('esunidad');
            }else{
                $select.attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().prev().find('select.sel-cursodet').attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.form-group').parent().remove();
        }
    }).fail(function(err) {
        $select.closest('.form-group').parent().remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var arrIndicadores = [];
var getIndicadores = function ( dataSend ) {
    $.ajax({
        url: _sysUrlBase_+'/acad_cursohabilidad/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            arrIndicadores = resp.data;
            cargarIndicadores();
        } else {
        }
    }).fail(function(err, txtErr) {
        console.log("error : ", err);
        mostrar_notificacion('Error',  txtErr, 'error');
    }).always(function() {});
};

var arrHabilidades = JSON.parse('<?php echo !empty(@$frm["habilidades"])?@$frm["habilidades"]:'[]'; ?>');
var habilidadDestacada = '<?php echo @$frm["habilidad_destacada"]; ?>';
var cargarIndicadores = function(){
    if(arrIndicadores.length==0) return false;
    $tblIndicadores = $('#lista-habilidades');
    var strSkills = '';
    $.each(arrIndicadores, function(i, indic) {
        let isActive = $.inArray(indic.idcursohabilidad, arrHabilidades) >= 0;
        strSkills += '<tr>';
        strSkills += '<td><a href="#" class="list-group-item '+(isActive?'active':'')+'" data-idhabilidad="'+indic.idcursohabilidad+'">'+indic.texto+'</a></td>';
        strSkills += '<td><input type="radio" name="opcHabilidad_destacada" class="radio-ctrl '+(!isActive?'hidden':'')+'" value="'+indic.idcursohabilidad+'" title="<?php echo ucfirst(JrTexto::_("Outstanding skill")); ?>" '+(habilidadDestacada==indic.idcursohabilidad?'checked="checked"':'')+' ></td>';
        strSkills += '</tr>';
    });
    $tblIndicadores.html(strSkills);
};

var borrarSobrantes = function($select) {
    var $listSelect = $contenedorFiltros.find('select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).length>=1){
        $contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).closest('.form-group').parent().remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect,
        'required' : 'required'
    });
    $contenedorFiltros.append($divSelect_new);
    return $contenedorFiltros.find('#'+nuevoIdSelect);
};

var leerniveles=function(data){
    try{
        var res = xajax__('', 'niveles', 'getxPadre', data);
        if(res){ return res; }
        return false;
    }catch(error){
        return false;
    }       
};

var actualizarInputHabilidades = function() {
    var $input = $('#txtHabilidades');
    var arrHabilidades = [];
    $('#lista-habilidades .list-group-item.active').each(function(i, elem) {
        let idHab = $(elem).attr('data-idhabilidad').toString();
        arrHabilidades.push(idHab);
    });
    var strHabilidades = JSON.stringify(arrHabilidades);
    $input.val( strHabilidades );
};

var addniveles=function(data,obj){
    var objini=obj.find('option:first').clone();
    obj.find('option').remove();
    obj.append(objini);
    if(data!==false){
        var html='';
        $.each(data,function(i,v){
            html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
        });
        obj.append(html);
    }
    id=obj.attr('id');
    if(id==='activity-item') {
        var filtros = {
            'idnivel': $('#level-item').val(),
            'idunidad': $('#unit-item').val(),
            'idactividad': $('#activity-item').val(),
        };
    }
};

var agregarTblAdjuntos = function(objArchivo) {
    objArchivo = objArchivo||{};
    if(objArchivo.length==0) return false;
    var icon = '';
    if(objArchivo.tipo=='D'){ icon='fa-paperclip'; }
    else if(objArchivo.tipo=='V'){ icon='fa-video-camera'; }
    else if(objArchivo.tipo=='U'){ icon='fa-headphones'; }
    else if(objArchivo.tipo=='G'){ icon='fa-microphone'; }
    else if(objArchivo.tipo=='L'){ icon='fa-link'; }
    else if(objArchivo.tipo=='A'){ icon='fa-font'; }
    else if(objArchivo.tipo=='J'){ icon='fa-puzzle-piece'; }
    else if(objArchivo.tipo=='E'){ icon='fa-list'; }
    else{ icon='fa-file'; }

    if(objArchivo.nombre.trim()==''){ objArchivo.nombre = objArchivo.ruta; }
    if(objArchivo.ruta.indexOf('http://')==-1 && objArchivo.ruta.indexOf('https://')==-1){
        objArchivo.ruta = 'http://'+objArchivo.ruta;
    }
    var fila = '<tr data-id="'+objArchivo.idtarea_archivos+'">';
    fila+='<td style="width: 7%"> <i class="fa '+icon+'"></i> </td>';
    fila+='<td>'+objArchivo.nombre+'</td>'
    fila+='<td style="width: 18%">';
    fila+='<a href="'+objArchivo.ruta+'" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank" class="btn btn-xs btn-default color-info verarchivo"><i class="fa fa-eye"></i></a>';
    fila+='<button title="<?php echo ucfirst(JrTexto::_('Delete')); ?>" class="btn btn-xs btn-default color-red eliminararchivo"><i class="fa fa-trash"></i></button>';
    fila+='</td>';
    fila += '</tr>';
    $('#tblAdjuntos tbody').append(fila);

    actualizarInputIdArchivos();
};

var actualizarInputIdArchivos = function(){
    var arrIds=[];
    $('#tblAdjuntos tbody tr').each(function(i, elem) {
        var id = $(elem).data('id');
        arrIds.push(id);
    });
    $('#txtIdTarea_archivos').val(JSON.stringify(arrIds));
};

var subirmedia_tarea=function(tipo, $file, otrosDatos){
    otrosDatos = otrosDatos||{};
    var formData = new FormData();
    formData.append("tipo", tipo);
    if(tipo=='G'){
        formData.append("filearchivo", $file);/* "file" es un Blob */
        formData.append("nombre_file", otrosDatos.nombrearchivo);
    }else{
        formData.append("filearchivo", $file[0].files[0]);
    }

    if($('#accion').val()=='Editar'){
        formData.append("tablapadre", 'T');
        formData.append("idpadre", $('#pkIdtarea').val());
    }
    $.ajax({
        url: _sysUrlBase_+'/tarea_archivos/subirarchivo',
        type: "POST",
        data:  formData,
        contentType: false,
        processData: false,
        dataType :'json',
        cache: false,
        xhr:function(){
            var xhr = new window.XMLHttpRequest();
            /*Upload progress*/
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = Math.floor((evt.loaded*100) / evt.total);
                    $('#barra-progreso .progress-bar').width(percentComplete+'%');
                    $('#barra-progreso .progress-bar span').text(percentComplete+'%');
                }
            }, false);
            /*Download progress*/
            xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                }
            }, false);
            return xhr;
        },
        beforeSend: function(XMLHttpRequest){
            div=$('#barra-progreso');
            $('#barra-progreso').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
            $('#barra-progreso .progress-bar').width('0%');
            $('#barra-progreso .progress-bar span').text('0%'); 
            $('#barra-progreso').fadeIn('fast'); 
            $('#btn-saveBib_libro').attr('disabled','disabled');
        },      
        success: function(data){
            if(data.code==='ok'){
                $('#barra-progreso .progress-bar').width('100%');
                $('#barra-progreso .progress-bar').html('Complete <span>100%</span>');
                $('#barra-progreso').addClass('progress-bar-success').removeClass('progress-bar-animated');
                
                agregarTblAdjuntos({'idtarea_archivos': data.idtarea_archivos, 'nombre': data.nombre, 'ruta': data.ruta, 'tipo': data.tipo});
                $('#inp_CargadorArchivos').val('');

                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                $file.val('');
            }else{
                $('#barra-progreso').addClass('progress-bar-warning progress-bar-animated');
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
                return false;
            }
        },
        error: function(e) {
            $('#barra-progreso').addClass('progress-bar-danger progress-bar-animated');
            $('#barra-progreso .progress-bar').html('Error <span>-1%</span>'); 
            mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
            return false;
        },
        complete: function(xhr){
            $('#barra-progreso .progress-bar').html('Complete <span>100%</span>'); 
            $('#barra-progreso').addClass('progress-bar-success progress-bar-animated').fadeOut('fast');
        }
    });
};

var crearModal = function(param){
    if(param.deDonde==undefined || param.deDonde==''){ return false; }
    if(param.nombreContenedor==undefined || param.nombreContenedor=='') {
        param.nombreContenedor=param.deDonde;
    }
    var $modal = $('#modalclone').clone();
    $modal.attr('id','mdl-'+param.nombreContenedor);
    if(param.small){$modal.find('.modal-dialog').removeClass('modal-lg');}
    else{$modal.find('.modal-dialog').addClass('modal-lg');}
    $modal.find('.modal-header #modaltitle').html('<?php echo JrTexto::_('Attach'); ?> '+param.titulo);
    $modal.find('#modalfooter .btn.cerrarmodal').addClass('pull-left');
    $modal.find('#modalfooter').append('<button class="btn btn-success guardar_adjunto"><?php echo JrTexto::_('Save'); ?></button>');
    $('body').append($modal);
    $('#mdl-'+param.nombreContenedor).modal({keyboard:false, backdrop:'static'});
    $modal.find('#modalcontent').html($('#'+param.deDonde).html());
    $modal.find('#modalcontent').find('form').attr({'id':'frm-'+param.nombreContenedor,'name':'frm-'+param.nombreContenedor});
};

var listarResultados = function(arrElems, $divResults){
    arrElems = arrElems||{};
    if($.isEmptyObject(arrElems)) {return false;}
    var botones = '';
    var i = 0;
    $.each(arrElems, function(idelem, elem) {
        if($divResults.closest('.modal.in').find('.divVistaTabs .nav-tabs li[data-id="'+idelem+'"]').length>0){
            var color=' btn-success ';
            var activo=' active ';
        }else{
            var color=' btn-info ';
            var activo=' ';
        }
        botones+='<div class="col-xs-12 col-sm-3">'+
        '<button class="btn btn-xs btn-block  seleccionar'+color+activo+'" data-id="'+idelem+'" data-tipo="'+elem.tipo_adj+'">'+(i+1)+'. '+elem.titulo+'</button>'+
        '</div>';
        i++;
    });
    $divResults.html(botones);
};

var arrActividades={};
var listarActividades = function(detalle, tipo_adj, $modal){
    detalle = detalle||{};
    tipo_adj = tipo_adj||'';
    if($.isEmptyObject(detalle)) {return false;}
    arrActividades={};
    $.each(detalle, function(idActividad, arrDetalles_act) {
        $.each(arrDetalles_act, function(index, val) {
            arrActividades[val.iddetalle]={
                'titulo': val.titulo,
                'descripcion': val.descripcion,
                'html': val.texto_edit.replace(/__xRUTABASEx__/gi,_sysUrlBase_),
                'tipo_adj':tipo_adj,
            };
        });
    });
    listarResultados(arrActividades, $modal.find('.divResultados'));
};

var arrJuegos={};
var listarJuegos = function(detalle, tipo_adj, $modal){
    detalle = detalle||{};
    tipo_adj = tipo_adj||'';
    if($.isEmptyObject(detalle)) {return false;}
    $.each(detalle, function(index, juego) {
        arrJuegos[juego.idtool]={
            'titulo': juego.titulo,
            'descripcion': juego.descripcion,
            'html': juego.texto.replace(/__xRUTABASEx__/gi,_sysUrlBase_),
            'tipo_adj':tipo_adj,
        };
    });
    listarResultados(arrJuegos, $modal.find('.divResultados'));
};

var arrExamenes={};
var listarExamenes = function(detalle, tipo_adj, $modal){
    detalle = detalle||{};
    tipo_adj = tipo_adj||'';
    if($.isEmptyObject(detalle)) {return false;}
    $.each(detalle, function(index, exam) {
        arrExamenes[exam.idrecurso]={
            'titulo': exam.nombre,
            'descripcion': exam.imagen,
            /*'html': exam.texto.replace(/__xRUTABASEx__/gi,_sysUrlBase_),*/
            'tipo_adj':tipo_adj,
        };
    });
    listarResultados(arrExamenes, $modal.find('.divResultados'));
};

var reordenarTabs = function ($contenedor) {
    $contenedor.find('.nav-tabs li>a').each(function(index, el) {
        $(this).html(index+1);
    });
};

var estaAgregado = function(idElem, $contenedor){
    var resp= false;
    $contenedor.find('li>a').each(function(index, el) {
        var id = $(this).attr('data-id');
        if(id==idElem){
            resp=true;
            return false;
        }
    });
    return resp;
};

var setTarea_Archivos = function($modal, tipo) {
    var datos = {};
    if(tipo=='G'){
        /*grabacion_ConvertirMP3_Subir('.modal.in', true);*/
        grabacion_GuardarYSubir($modal, tipo);
    }else if(tipo=='L'){
        if(!$modal.find('form #txtRuta').val()){ return {}; }
        datos = $modal.find('form').serialize();
    }else if(tipo=='A' || tipo=='J' || tipo=='E'){
        var arrId = [];
        var nombre=(tipo=='A')?'Activity':((tipo=='J')?'Game':'Exam');
        $modal.find('.divVistaTabs .nav-tabs li').each(function(index, elem) {
            var id = $(elem).data('id');
            arrId.push(id);
        });
        if(arrId.length==0){ return {}; }
        datos={
            'txtNombre': nombre,
            'txtRuta' : _sysUrlBase_+'/tarea_archivos/visor/?idarchivo=',
            'txtTexto': JSON.stringify(arrId),
            'txtTipo': tipo,
        };
        if($('#accion').val()=='Editar'){
            datos['txtTablapadre'] = $modal.find('#txtTablapadre').val();
            datos['txtIdpadre'] = $modal.find('#txtIdpadre').val();
        }
    }
    return datos;
};

var guardarTarea = function() {
    $('#frmAgregarTarea *[required]').each(function(i, elem) {
        var value = $(elem).val();
        if(value=='' || value.trim()==''){
            $(elem).closest('.form-group').addClass('has-error');
        }
    });
    if($('#frmAgregarTarea .has-error').length>=1){ return false; }
    console.log($('select[name=opcIdcursodetalle]').val());
    if($('select[name=opcIdcursodetalle]').val() != 0){
        $('#idactividad2').val($('select[name=opcIdcursodetalle]').val());
    }
    $.ajax({
        url: _sysUrlBase_+'/tarea/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: $('#frmAgregarTarea').serialize(),
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idTarea = resp.data;
            if($('#frmAgregarTarea #accion').val()=="Nuevo"){
                var idcurso=$('#opcIdCurso').val();
                var idunidad=$('#filtros-actividad').find('select.esunidad option:selected').val()||-1;
                var idrecurso=$('#filtros-actividad').find('select.tienesesion option:selected').val()||-1;
                return redir(_sysUrlBase_+'/tarea/editar/?id='+idTarea+'&idcurso='+idcurso+'&idunidad='+idunidad+'&idrecurso='+idrecurso);
            }
            mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Data updated') ?>','success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
        }
    }).fail(fnAjaxFail);
}

var initGrabarVoz = function(){
    initAudio(); 
};

function endedRecordingPlayer(elem) {
    $(elem).currentTime = 0;
    var $btn = $(elem).closest('.modal.in').find('.btn.reproducir');
    $btn.attr('data-estado','paused');
    $btn.siblings('.btn.grabarme').removeAttr('disabled');
    $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
    $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
};

var grabacion_ConvertirMP3_Subir = function(classModal, subir=false){
    var fnCallback = function(blob){
        var a = Date.now();
        var length = ((blob.size*8)/128000);
        var url = URL.createObjectURL(blob);
        if(subir){
            subirmedia_tarea('G', blob, {'nombrearchivo': $(classModal).find('input.txtNombre').val()+'_'+$(classModal).find('input.txtNombre').attr('data-uniqid') } );
            $(classModal).modal('hide');
        }else{
            var wavesurfer = Object.create(WaveSurfer);
            wavesurfer.init({
                container: document.querySelector(classModal+' .onda_voz'),
                waveColor: '#85BCEA',
                progressColor: '#337AB7',
                backend: 'MediaElement'
            });
            wavesurfer.load(url);
            $(classModal+' .btn.reproducir').show();
            document.querySelector(classModal+' .btn.reproducir').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
        }
    };
    recorder.exportMP3(fnCallback);
};

var grabacion_GuardarYSubir = function($modal, tipo) {
    var nombre = $modal.find('input.txtNombre').val(),
        uniqID = $modal.find('input.txtNombre').attr('data-uniqid');
    var fd = new FormData();
    fd.append("tipo", 'G');
    fd.append('nombre_file', nombre+'_'+uniqID);
    if($('#accion').val()=='Editar'){
        fd.append("tablapadre", 'T');
        fd.append("idpadre", $('#pkIdtarea').val());
    }
    var fnAjax = {
        beforeSend : function() {
            $modal.find('.btn').attr('disabled', 'disabled');
            $modal.find('.btn.guardar_adjunto').prepend('<i class="fa fa-circle-o-notch fa-spin fa-fw" id="loading-icon"></i> ');
        },
        success : function(resp) {
            if(resp.code=='ok'){
                agregarTblAdjuntos({'idtarea_archivos': resp.idtarea_archivos, 'nombre': resp.nombre, 'ruta': resp.ruta, 'tipo': resp.tipo});
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
                $modal.modal('hide');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        },
        fail : function(xhr, txtStatus, errorThrown) {
            mostrar_notificacion('Error', errorThrown, 'error');
            throw errorThrown;
        }
    };

    Recorder.uploadRecording(_sysUrlBase_+'/tarea_archivos/subirarchivo', fd, fnAjax);
};

<?php if($this->frmaccion=='Editar'){ ?>
/********************** Editar **********************/
var initFechaYHora = function($selFecha, $selHora){
    var ahora = Date.now();
    var now = new Date();
    var fecha_hora = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+(now.getDate()+1)+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    fecha = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
    hora = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
    $selFecha.datetimepicker({
        defaultDate: fecha_hora,
        date: fecha,
        format: 'DD-MM-YYYY',
        locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
        minDate: "<?php echo date("Y-m-d H:i:s") ?>",
    });
    $selHora.datetimepicker({
        defaultDate: fecha_hora,
        date: hora,
        format: 'LT',
        locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
    });
}

var guardarAsignaciones = function(){
    var arrTarea_Asignacion=[];
    $('#tarea-add .frmAsignacion').each(function(i, frm){
        var $frm = $(frm);
        var asignacion={
            'idtarea_asignacion': $frm.data('id'),
            'idtarea': $('#pkIdtarea').val(),
            'idgrupo': $frm.find('.opcGrupo').val(),
            'fechaentrega': $frm.find('.dtpFecha').val(),
            'horaentrega': $frm.find('.dtpHora').val(),
            'alumnos':[],
        };
        $frm.find('.checkbox-ctrl.item-alumno').each(function(i, chkbox) {
            asig_alum ={
                'iddetalle': $(chkbox).data('iddetalle'),
                'idalumno': $(chkbox).val(),
                'estado': $(chkbox).data('estado'),
                'registrar': $(chkbox).is(':checked'),
            };
            asignacion.alumnos.push(asig_alum);
        });
        if(asignacion.alumnos.length<=0){ 
            mostrar_notificacion('<?php echo JrTexto::_('Warning') ?>', '<?php echo JrTexto::_('No students selected in assignment Assigning') ?>');
            return false; 
        }
        arrTarea_Asignacion.push(asignacion);
    });

    if(arrTarea_Asignacion.length<=0){ return false; }

    $.ajax({
        url: _sysUrlBase_+'/tarea_asignacion/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: {'arrTarea_asignacion':JSON.stringify(arrTarea_Asignacion)},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idTarea = resp.data;
            setTimeout(function() {
                window.location.reload(true);
            }, '1000');
            mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Assigning successful') ?>','success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
        }
    }).fail(fnAjaxFail);
};

var initEdit = function(){
    var idActividad=<?php echo !empty(@$frm['idactividad'])?@$frm['idactividad']:'null'; ?>;
    var curso_detalles = JSON.parse('<?php 
        $str=str_replace("'", "\'", json_encode(@$frm['curso_detalles'])); 
        echo str_replace('"', '\"', @$str); ?>');
    var idCurso = <?php echo !empty($this->idcurso)?$this->idcurso:'null'; ?>;
    var idUnidad = <?php echo !empty($_GET["idunidad"])?$_GET["idunidad"]:'null'; ?>;
    var idRecurso = <?php echo !empty($_GET["idrecurso"])?$_GET["idrecurso"]:'null'; ?>;
    var idCursoDetalle = <?php echo !empty(@$frm['idcursodetalle'])?@$frm['idcursodetalle']:'null'; ?>;
    var ptjeMax=<?php echo !empty(@$frm['puntajemaximo'])?@$frm['puntajemaximo']:'null'; ?>;
    var ptjeMin=<?php echo !empty(@$frm['puntajeminimo'])?@$frm['puntajeminimo']:'null'; ?>;

    $('#activity-item').val(idActividad);
    $('#opcIdCurso').val(idCurso);
    $('#opcIdCurso').trigger('change');

    var cur_det = { 'cursodetalle_hijo' : curso_detalles};
    do {
        cur_det = cur_det.cursodetalle_hijo;
        $('#opcIdCursoDet_'+cur_det.idpadre).val(cur_det.idcursodetalle);
        $('#opcIdCursoDet_'+cur_det.idpadre).trigger('change');
    } while( cur_det.idcursodetalle != idCursoDetalle);
    if(idUnidad!=null){       
       $('#filtros-actividad').find('select.esunidad').val(idUnidad).trigger('change');
    }
    if(idRecurso!=null){       
       $('#filtros-actividad').find('select.tienesesion').val(idRecurso);
    }

    $('#opcPuntajemaximo').val(ptjeMax);
    $('#opcPuntajemaximo').trigger('change');
    $('#opcPuntajeminimo').val(ptjeMin);

    actualizarInputIdArchivos();
    $('#tarea-add form.frmAsignacion').each(function(index, el) {
        initFechaYHora($(el).find('.datetimepicker.fechaentrega'), $(el).find('.datetimepicker.horaentrega'));
    });   
};
<?php } ?>

$(document).ready(function() {
    $('#filtros-actividad').on('change', '.select-nivel', function(e) {
        var idPadre = 0;
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        borrarSobrantes($(this));
        if(idCurso==0){ 
            $('.contenedor-lista-habilidades table tbody').html('');
            return false; 
        }

        $select_new = nuevoSelectFiltro(idPadre);
        getCursoDetalle({
            'idcurso': idCurso,
            'idpadre': idPadre,
        }, $select_new);

        var idCursoDetalle = null;
        if(idPadre>0) idCursoDetalle = idPadre;
        getIndicadores({'idcurso': idCurso, 'idcursodetalle': idCursoDetalle});
    });

    $('body').on('click', '#lista-habilidades .list-group-item', function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        var $radioCtrl = $(this).closest('tr').find('.radio-ctrl');
        var nameInputRadio = $radioCtrl.attr('name');
        if( $(this).hasClass('active') ) {
            $radioCtrl.removeClass('hidden');
            var $inputAChequear = $radioCtrl;
        } else {
            $radioCtrl.addClass('hidden');
            $radioCtrl.prop('checked', false);
            var $inputAChequear = $('*[name="'+nameInputRadio+'"]').not('.hidden').first();
        }
        if(typeof $('*[name="'+nameInputRadio+'"]:checked').val()=='undefined') { 
            $inputAChequear.prop('checked', true); 
        }
        actualizarInputHabilidades();
    });

    $('.btnportada').click(function(e){ 
        var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
        selectedfile(e,this,txt);
    });

    $('.btnportada img').load(function() {
        var src=$(this).attr('src');
        $('#txtFoto').val(src);
    });

    $('#opcPuntajemaximo').change(function(e) {
        var ptjeMax = $(this).val();
        var ptjeMin = $('#opcPuntajeminimo').val();
        var opciones ='';
        var selected ='';
        for(var x=ptjeMax-1; x>=0; x--){
            selected=(x==ptjeMin)?'selected':'';
            opciones+='<option class="'+x+'" '+selected+'>'+x+'</option>';
        }
        $('#opcPuntajeminimo').html(opciones);
    });

    $("#inp_CargadorArchivos").change(function(e) {
        var $file=$(this);
        if($file.val()=='') return false;
        var tipo = $file.attr('data-tipo');
        subirmedia_tarea(tipo, $file);
        e.preventDefault();
        e.stopPropagation();
    });

    $('.btnadjunto').click(function(e) {
        e.preventDefault();
        var tipo = $(this).data('adjunto');
        switch(tipo) {
            case 'documento':
                var arrAccept = [];
                arrAccept.push("application/msword");
                arrAccept.push("application/vnd.ms-excel");
                arrAccept.push("application/vnd.ms-powerpoint");
                arrAccept.push("application/vnd.ms-project");
                arrAccept.push("application/xhtml+xml");
                arrAccept.push("text/html");
                arrAccept.push("text/css");
                arrAccept.push("text/plain");
                arrAccept.push("application/pdf");
                arrAccept.push("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                arrAccept.push("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                arrAccept.push("application/vnd.openxmlformats-officedocument.presentationml.slideshow");
                arrAccept.push("application/vnd.openxmlformats-officedocument.presentationml.presentation");
                var accept = arrAccept.join(', ');
                $("#inp_CargadorArchivos").attr('accept', accept);
                $("#inp_CargadorArchivos").attr('data-tipo', 'D');
                $("#inp_CargadorArchivos").trigger('click');
                break;
            case 'video':
                var accept = "video/*";
                $("#inp_CargadorArchivos").attr('accept', accept);
                $("#inp_CargadorArchivos").attr('data-tipo', 'V');
                $("#inp_CargadorArchivos").trigger('click');
                break;
            case 'audio':
                var accept = "audio/*";
                $("#inp_CargadorArchivos").attr('accept', accept);
                $("#inp_CargadorArchivos").attr('data-tipo', 'U');
                $("#inp_CargadorArchivos").trigger('click');
                break;
            case 'enlace':
                crearModal({deDonde:'adjuntar_link', titulo:'<?php echo JrTexto::_('Link'); ?>', small:true});
                $('#mdl-adjuntar_link .guardar_adjunto').attr('data-guardar', 'L');
                break;
            case 'grabacionvoz':
                crearModal({deDonde:'adjuntar_grabacionvoz', titulo:'<?php echo JrTexto::_('Voice recording'); ?>', small:true});
                $('#mdl-adjuntar_grabacionvoz .guardar_adjunto').attr('data-guardar', 'G');
                initGrabarVoz();
                break;
            case 'actividad':
                crearModal({deDonde:'adjuntar_act_gam_exa', titulo:'<?php echo JrTexto::_('Activity'); ?>', nombreContenedor:'adjuntar_actividad'});
                $('#mdl-adjuntar_actividad .btnbuscar').attr('data-buscar', 'A');
                $('#mdl-adjuntar_actividad .guardar_adjunto').attr('data-guardar', 'A');
                $('#mdl-adjuntar_actividad .divVistaTabs .tab-content').tplcompletar({editando:false});
                break;
            case 'juego':
                crearModal({deDonde:'adjuntar_act_gam_exa', titulo:'<?php echo JrTexto::_('Game'); ?>', nombreContenedor:'adjuntar_juego'});
                $('#mdl-adjuntar_juego .btnbuscar').attr('data-buscar', 'J');
                $('#mdl-adjuntar_juego .guardar_adjunto').attr('data-guardar', 'J');
                $('#mdl-adjuntar_juego .divVistaTabs ul.nav-tabs').hide();
                $('#mdl-adjuntar_juego .divVistaTabs .tab-content').html('<iframe class="frame_juego" style="display:none; border:0; width: 100%; height: 500px;"></iframe>');
                break;
            case 'examen':
                crearModal({deDonde:'adjuntar_act_gam_exa', titulo:'<?php echo JrTexto::_('Exam'); ?>', nombreContenedor:'adjuntar_examen'});
                $('#mdl-adjuntar_examen .btnbuscar').attr('data-buscar', 'E');
                $('#mdl-adjuntar_examen .guardar_adjunto').attr('data-guardar', 'E');
                $('#mdl-adjuntar_examen .divVistaTabs ul.nav-tabs').hide();
                $('#mdl-adjuntar_examen .divVistaTabs .tab-content').html('<iframe class="frame_examen" style="display:none; border:0; width: 100%; height: 500px;"></iframe>');
                
                $('#mdl-adjuntar_examen .btnbuscar').closest('.form-group').hide();
                $('#mdl-adjuntar_examen .btnbuscar').trigger('click');
                break;
            default: break;
        }
    });

    $('#tblAdjuntos').on('click', '.btn.eliminararchivo', function(e) {
        e.preventDefault();
        var $tr= $(this).closest('tr');
        var idArch= $tr.attr('data-id');
        $.confirm({
            title: '<?php echo JrTexto::_('Delete');?>',
            content: '<?php echo JrTexto::_('Are you sure to delete this file?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                $.ajax({
                    url: _sysUrlBase_+'/tarea_archivos/xEliminar',
                    type: 'POST',
                    dataType: 'json',
                    data: {'idtarea_archivos': idArch},
                }).done(function(resp) {
                    if(resp.code="ok"){
                        $tr.remove();
                        actualizarInputIdArchivos();
                    }else{
                        mostrar_notificacion('<?php echo JrTexto::_('Error') ?>', resp.msj, 'error');
                    }
                }).fail(fnAjaxFail);
            },
        });
    });

    $('body').on('click', '.modal .btnbuscar', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $modal = $this.closest('.modal');
        var idmetodologia = 3;
        var url_ruta = _sysUrlBase_;
        var buscar = $this.attr('data-buscar'); /* A, J, E */
        var texto = $this.closest('.input-group').find('#txtBuscar').val();
        var idnivel = ($("#level-item").val()>0)? $('#level-item').val() : '';
        var idunidad = ($("#unit-item").val()>0)? $('#unit-item').val() : '';
        var idactividad = ($("#activity-item").val()>0)? $('#activity-item').val() : '';
        var idcursodetalle = ($('select[name="opcIdcursodetalle"]').val()>0)? $('select[name="opcIdcursodetalle"]').val() : '';
        var formData = new FormData();
        formData.append("nivel", idnivel);
        formData.append("unidad", idunidad);
        formData.append("actividad", idactividad);
        formData.append("idcursodetalle", idcursodetalle);
        if(buscar=='A'){ 
            url_ruta+='/actividad/buscarXCursoDet'; 
            formData.append("titulo", texto);
            formData.append("met", idmetodologia);
            var fnListarResult = listarActividades;
        }else if(buscar=='J'){ 
            url_ruta+='/tools/buscarGamesXCursoDet'; 
            formData.append("txtBuscar", texto);
            var fnListarResult = listarJuegos;
        }else if(buscar=='E'){ 
            url_ruta+='/examenes/buscarSmartquizXCursoDet'; 
            formData.append("txtBuscar", texto);
            var fnListarResult = listarExamenes;
        }else { return false; }
        $.ajax({
            url: url_ruta,
            type: 'POST',
            dataType: 'json',
            data: formData,
            contentType: false,
            processData: false,
            cache: false,
            beforeSend: function(){
                $this.closest('.modal').find('.divResultados').html('<div id="animacion-loading" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            },
        }).done(function(resp) {
            if(resp.code==='ok'){
                var detalles = [];
                if(!$.isEmptyObject(resp.data) || ($.isArray(resp.data) && resp.data.length>0)){
                    if(buscar=='A'){detalles = resp.data[idmetodologia].acts;}
                    else if(buscar=='J'){detalles = resp.data;}
                    else if(buscar=='E'){detalles = resp.data;}
                    fnListarResult(detalles, buscar, $modal);
                }else{
                    $this.closest('.modal').find('.divResultados').append('<h4 class="text-center"><?php echo JrTexto::_('No results for your search'); ?>.</h4>');
                }
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msje, 'error');
            }
        }).fail(fnAjaxFail).always(function() {
            $this.closest('.modal').find('.divResultados #animacion-loading').remove();
        });
    }).on('click', '.modal.in .divResultados .seleccionar', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var tipo = $(this).data('tipo');
        var $modal = $(this).closest('.modal.in');
        if(tipo=='A'){ var objResultados= arrActividades; /*var $modal=$('#mdl-adjuntar_actividad');*/ }
        else if(tipo='J'){ var objResultados= arrJuegos; /*var $modal=$('#mdl-adjuntar_juego');*/ }
        else if(tipo='E'){ var objResultados= arrExamenes; /*var $modal=$('#mdl-adjuntar_examen');*/ }
        else{ return false; }
        if(!$(this).hasClass('active')){
            $(this).addClass('active').addClass('btn-success').removeClass('btn-info');
            if(tipo=='A'){
                $modal.find('.divVistaTabs .nav-tabs').append('<li data-id="'+id+'"><a href="#adj_'+tipo+id+'" data-toggle="tab">Num</a></li>');
                $modal.find('.divVistaTabs .tab-content').append('<div id="adj_'+tipo+id+'" data-id="'+id+'" class="tab-pane fade">'+objResultados[id].html+'</div>');
            }else{
                /* para JUEGOS y EXAMENES */
                $modal.find('.divVistaTabs .nav-tabs').html('<li data-id="'+id+'"><a href="#adj_'+tipo+id+'" data-toggle="tab">Num</a></li>');
                $(this).closest('.divResultados').find('.btn.seleccionar.active').not($(this)).removeClass('active btn-success').addClass('btn-info');
            }
            reordenarTabs($modal);
            $modal.find('.nav-tabs li>a[href="#adj_'+tipo+id+'"]').click();
        } else {
            $(this).removeClass('active').removeClass('btn-success').addClass('btn-info');
            $modal.find('.divVistaTabs .nav-tabs li[data-id="'+id+'"]').remove();
            $modal.find('.divVistaTabs .tab-content #adj_'+tipo+id).remove();
            reordenarTabs($modal);
            if($modal.find('.divVistaTabs .tab-content iframe').length>0){
                $modal.find('.divVistaTabs .tab-content iframe').attr('src', '').hide();
            }
            $modal.find('.divVistaTabs .nav-tabs li>a').last().trigger('click');
        }
    }).on('keypress', '.modal #txtBuscar', function(e) {
        if(e.which==13){ 
            e.preventDefault(); 
            var $btn = $(this).closest('.form-group').find('.btnbuscar');
            $btn.trigger('click');
        }
    }).on('click', '.modal.in #frm-adjuntar_grabacionvoz .btn.grabarme', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var $modal = $(this).closest('.modal.in');
        if($btn.attr('disabled')=='disabled'){ return false; }
        if($btn.attr('data-estado')=='stopped'){
            $btn.attr('data-estado','recording');
            /*recorder.clear();
            recorder && recorder.record();*/
            $btn.siblings('.btn.reproducir').attr('disabled', 'disabled');
            /*$modal.find('.onda_voz').html('<h1 class="text-center animated infinite pulse"> <i class="fa fa-microphone"></i> Recoding...</h1>');*/
            $btn.removeClass('btn-danger').addClass('btn-default');
            $btn.find('i.fa').removeClass('fa-circle').addClass('fa-stop');
            $btn.find('span').text('<?php echo JrTexto::_('Stop'); ?>');
        }else{
            $btn.attr('data-estado','stopped');
            /*recorder && recorder.stop();*/
            $btn.siblings('.btn.reproducir').removeAttr('disabled');
            /*$modal.find('.onda_voz').html('');*/
            $btn.removeClass('btn-default').addClass('btn-danger');
            $btn.find('i.fa').removeClass('fa-stop').addClass('fa-circle');
            $btn.find('span').text('<?php echo JrTexto::_('Rec'); ?>');

            //grabacion_ConvertirMP3_Subir('.modal.in');
        }
        toggleRecording(this); /* main.js */
    }).on('click', '.modal.in #frm-adjuntar_grabacionvoz .btn.reproducir', function(e) {
        e.preventDefault();
        var $btn = $(this);
        if($btn.attr('disabled')=='disabled'){ return false; }
        if($btn.attr('data-estado')=='paused'){
            $btn.attr('data-estado','playing');
            $btn.siblings('.btn.grabarme').attr('disabled', 'disabled');
            $btn.find('i.fa').removeClass('fa-play').addClass('fa-pause');
            $btn.find('span').text('<?php echo JrTexto::_('Pause'); ?>');
            $('#mdl-adjuntar_grabacionvoz #recording_player').trigger('play');
        }else{
            $btn.attr('data-estado','paused');
            $btn.siblings('.btn.grabarme').removeAttr('disabled');
            $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
            $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
            $('#mdl-adjuntar_grabacionvoz #recording_player').trigger('pause');
        }
    });

    $('body').on('click', '.modal.in .guardar_adjunto', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var tipo = $(this).data('guardar');
        var datos = setTarea_Archivos($(this).closest('.modal'), tipo);
        if($.isEmptyObject(datos)){ return false; }
        $.ajax({
            url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos',
            type: 'POST',
            dataType: 'json',
            data:  datos,
            beforeSend : function() {
                $btn.attr('disabled', 'disabled');
                $btn.prepend('<i class="fa fa-circle-o-notch fa-spin fa-fw" id="loading-icon"></i> ');
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                agregarTblAdjuntos({'idtarea_archivos': resp.idtarea_archivos, 'nombre': resp.nombre, 'ruta': resp.ruta, 'tipo': resp.tipo});
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        }).fail(fnAjaxFail).always(function() {
            $('.modal.in').modal('hide');
        });
    });
    
    $('body').on('hidden.bs.modal', '.modal', function(event) { 
        if($(this).attr('id')=="mdl-adjuntar_grabacionvoz"){ destroyAudio(); }
        $(this).remove(); 
    }).on('keyup', '.form-group *[required]', function(e) {
        if(!$(this).val() || $(this).val().trim()==''){
            $(this).closest('.form-group').addClass('has-error');
        }else{
            $(this).closest('.form-group').removeClass('has-error');
        }
    }).on('change', '#frmAgregarTarea *[required]', function(e) {
        var valor = $(this).val()||'';
        if(valor!='' && valor.trim()!=''){
            $(this).closest('.form-group').removeClass('has-error');
        }
    }).on('click', '.modal.in .nav-tabs li>a', function(e) {
        var $modal = $(this).closest('.modal.in');
        var tipo = $modal.find('.modal-footer .btn.guardar_adjunto').data('guardar');
        if($modal.find('.divVistaTabs .tab-content iframe').length>0){
            var id = $(this).closest('li').data('id');
            var $iframe = $modal.find('.divVistaTabs .tab-content iframe');
            if(tipo=='J'){
                $iframe.attr('src', _sysUrlBase_+'/game/ver/'+id).show();
            }else if(tipo=='E'){
                $iframe.attr('src', _sysUrlBase_+'/examenes/resolver/?idexamen='+id).show();
            }

            $iframe.load(function() {
                var $iframe=$(this);
                var $insideFrame=$iframe.contents();
                var $selector=$iframe;
                var fnInit=null;
                if($iframe.hasClass('frame_juego')){ 
                    $selector=$insideFrame.find('#games-tool .games-main'); 
                    var $plantilla = $selector.find('.plantilla');
                    if($plantilla.hasClass('plantilla-sopaletras')){ 
                        fnInit=function(){ iniciarSopaLetras($plantilla); borrarNoPreview($plantilla, $insideFrame); };
                    }else if($plantilla.hasClass('plantilla-crucigrama')){ 
                        fnInit=function(){ iniciarCrucigrama($plantilla); borrarNoPreview($plantilla, $insideFrame); };
                    }else if($plantilla.hasClass('plantilla-rompecabezas')){ 
                        fnInit=function(){ iniciarRompecabezas($plantilla); borrarNoPreview($plantilla, $insideFrame); }; 
                    }
                }else if($iframe.hasClass('frame_examen')){
                    $selector=$insideFrame.find('#examen_preview');
                }

                $selector.iniciarVisor({
                    btnGuardar: '.no-boton-to-click',
                    fnAlIniciar: fnInit,
                });
            });
        }
    });

    $('.btn.guardartarea').click(function(e) {
        e.preventDefault();
        guardarTarea();
    });

<?php if($this->frmaccion=='Editar'){ ?>
    /********************** Editar **********************/
    $('#botones-accion').on('click', '.agregar_asignacion', function(e) {
        e.preventDefault();
        var now = Date.now();
        var idFrm = $('section #frmAsignacion').attr('id');
        var $frm = $('section #frmAsignacion').clone();
        $frm.attr({ 'id': idFrm+'_'+now, 'name': idFrm+'_'+now, });
        $('#botones-accion').before($frm);
        initFechaYHora($('#'+idFrm+'_'+now).find('.datetimepicker.fechaentrega'), $('#'+idFrm+'_'+now).find('.datetimepicker.horaentrega'));
    }).on('click', '.guardar_edicion', function(e) {
        e.preventDefault();
        guardarTarea();
        guardarAsignaciones();
    });

    var arrAlumnosXGrupo={};
    $('body').on('change', '.opcColegio', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $contenedor = $this.closest('form.frmAsignacion');
        var idColegio = $this.val();
        if(idColegio==null || idColegio==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
            $contenedor.find('.opcAula').html(optGrup);

            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
            $contenedor.find('.opcGrupo').html(optGrup);

            $contenedor.find('.listado-alumnos').hide();
            $contenedor.find('.listado-alumnos ul').html('');
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/ambiente/getMisLocales/',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idlocal':idColegio},
            beforeSend: function(){ $contenedor.find('.opcAula').attr('disabled','disabled'); }
        }).done(function(resp) {
            if(resp.code=='ok'){
                var aulas = resp.data;
                var options='<option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>';
                $.each(aulas, function(key, val) {
                    options+='<option value="'+val.idambiente+'">'+val.numero+'</option>';
                });
                $contenedor.find('.opcAula').html(options);
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function() {
            $contenedor.find('.opcAula').removeAttr('disabled');
        });   
    }).on('change', '.opcAula', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $contenedor = $this.closest('form.frmAsignacion');
        var idAula = $this.val();
        var idLocal = $('.opcColegio').val();
        if(idAula==null || idAula==-1) {
            var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
            $contenedor.find('.opcGrupo').html(optGrup);

            $contenedor.find('.listado-alumnos').hide();
            $contenedor.find('.listado-alumnos ul').html('');
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/acad_grupoauladetalle/buscarjson/',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {
                'idlocal':idLocal,
                'idambiente':idAula,
                'iddocente':'<?php echo $this->usuarioAct["dni"]; ?>',
            },
            beforeSend: function(){ 
                $contenedor.find('.opcGrupo').attr('disabled','disabled'); 
                $contenedor.find('.opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                /***************    Grupos    ***************/
                var grupos = resp.data;
                var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
                $.each(grupos, function(key, gr) {
                    optGrup+='<option value="'+gr.idgrupoauladetalle+'">'+gr.strgrupoaula+' '+gr.nombre+' '+' ('+gr.strtipogrupo+')</option>';
                    arrAlumnosXGrupo[gr.idgrupo] = gr.alumnos;
                });
                $contenedor.find('.opcGrupo').html(optGrup);
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $contenedor.find('.opcGrupo').removeAttr('disabled');
            $contenedor.find('.opcAlumno').removeAttr('disabled');
        }); 
    }).on('change', '.opcGrupo', function(e){
        var $this=$(this);
        var idGrupo = $this.val();
        var $contenedor = $this.closest('form.frmAsignacion');
        if(idGrupo<=0){
            $contenedor.find('.listado-alumnos').hide();
            $contenedor.find('.listado-alumnos ul').html('');
            return false;
        }
        var list = '';
        $.ajax({
            url: _sysUrlBase_+'/acad_matricula/buscarjson/',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {
                'idgrupoauladetalle':idGrupo,
            },
            beforeSend: function(){  
                $contenedor.find('.opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                /***************    Alumnos    ***************/
                var matriculas = resp.data;
                var optGrup='<option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>';
                if(matriculas.length) {
                    $.each(matriculas, function(i, alum) {
                        list += '<li class="col-xs-12 col-sm-6"> <label>';
                        list += '<input type="checkbox" class="checkbox-ctrl item-alumno" name="chkIdAlumno[]" value="'+alum.idalumno+'" data-estado="N" data-iddetalle="" > ';
                        list += alum.stralumno;
                        list += '</label> </li>';
                    });
                } else {
                    list += '<span><?php echo JrTexto::_('There are no students in this group') ?></span>';
                }
                $contenedor.find('ul').html(list);
                $contenedor.find('.listado-alumnos').show();
                $contenedor.find('.check-all').prop('checked',true);
                $contenedor.find('.check-all').trigger('change');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $contenedor.find('.opcAlumno').removeAttr('disabled');
        }); 
    }).on('change', '.check-all', function(e) {
        var isChecked = $(this).is(':checked');
        var $contenedor = $(this).closest('form.frmAsignacion');
        var $item = $contenedor.find('.listado-alumnos ul li input.item-alumno').prop('checked', isChecked);
    }).on('click', 'form .eliminar_asignacion', function(e) {
        e.preventDefault();
        var $frm = $(this).closest('.frmAsignacion');
        $.confirm({
            title: '<?php echo JrTexto::_('Delete');?>',
            content: '<?php echo JrTexto::_('Are you sure to delete this record?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                var idAsig = $frm.attr('data-id') || 0;
                if(idAsig<=0){ $frm.remove(); } 
                else {
                    $.ajax({
                        url: _sysUrlBase_+'/tarea_asignacion/xEliminarAsignacion',
                        type: 'POST',
                        dataType: 'json',
                        data: {'idtarea_asignacion': idAsig},
                    }).done(function(resp) {
                        if(resp.code='ok'){ $frm.remove(); }
                        else{ mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error'); }
                    }).fail(fnAjaxFail);
                }
            },
        });
    });

    initEdit();
<?php } ?>

});
</script>