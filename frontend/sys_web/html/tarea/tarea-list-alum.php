<?php 
$userInfo = NegSesion::getUsuario();
if($userInfo['idproyecto'] == 3){
    $setHistorial = 'true';
    // echo "<input type='hidden' id='idcurso' value='{$this->idcurso}'>";
}

$arrEstados=array(
    'N'=> array('nombre'=>JrTexto::_('New'), 'clase'=>'color-info'),
    'P'=> array('nombre'=>JrTexto::_('Submitted'), 'clase'=>'color-yellow'),
    'D'=> array('nombre'=>JrTexto::_('Returned'), 'clase'=>'color-danger'),
    'E'=> array('nombre'=>JrTexto::_('Evaluated'), 'clase'=>'color-success'),
); 
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-list">
    <input type="hidden" id="hIdCurso" name="hIdCurso" value="<?php echo(@$this->idCurso) ?>">
    <!--div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('Activity')); ?></a></li>
            <li class="active"><?php echo JrTexto::_('List'); ?></li>
        </ol>
    </div> 
    </div-->

    <div class="row <?php //echo empty(@$this->idCurso)?'':'hidden' ?>" id="filtros-actividad"> 
        <div class="col-xs-12 col-sm-4 padding-0">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul">
                    <select name="opcIdCurso" id="opcIdCurso" class="form-control select-ctrl select-nivel">
                        <option value="0">- <?php echo ucfirst(JrTexto::_("Select a course")); ?> -</option>
                        <?php 
                            if(!empty($this->cursos))
                                foreach($this->cursos as $t){
                                    if(!empty($this->miscursos))
                                        foreach($this->miscursos as $c){
                                            echo '<option value="'.$t["idcurso"].'"'.($this->idCurso==$t["idcurso"]?'selected="selected"':'').'>'.$t["nombre"].'</option>';
                                            break ;
                                        }}
                        ?>
                    </select>
                </div>
            </div>
        </div>

    </div>

    <div class="row" id="listados">
        <div class="col-xs-12">
            <ul class="nav nav-tabs">
                <li class="tabpendientes active">
                    <a href="#pnl-pendientes" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Pending")); 
                        if(!empty($this->tareasPend)){ echo ' <span class="badge">'.count($this->tareasPend).'</span>'; } ?></a>
                </li>
                <li class="tabfinalizados">
                    <a href="#pnl-finalizados" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Finished")); ?></a>
                </li>
                <li class="tabcargando hidden">
                    <a href="#pnl-cargando" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Cargando")); ?></a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 tab-content" style="padding-top: 15px;">
            <div id="pnl-pendientes" class="row tab-pane fade active in">
                <div class="hidden pnlcargando">
                    <div class="cargando" style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/loading.gif"><br><span style="color: #006E84;"><?php echo JrTexto::_("Loading"); ?></span></div>
                </div>
                <div class="hidden pnlvacio">
                    <div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>
                </div>

                <div class="listado">
                    <?php if(!empty($this->tareasPend)) {
                    foreach ($this->tareasPend as $t) { ?>
                    <div class="col-xs-12 col-sm-6">
                        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                            <div class="panel-heading titulo">
                                <h3><?php echo $t['nombre']; ?></h3>
                            </div>
                            <div class="panel-body contenido">
                                <div class="col-xs-4 portada">
                                    <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                    if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                    <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                                </div>
                                <div class="col-xs-8 descripcion">
                                    <?php echo $t['descripcion']; ?>
                                </div>
                                <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                    <div class="col-xs-8 fecha-entrega">
                                        <div class="dato"><?php echo empty(@$t['fechaentrega'])?'---':@$t['fechaentrega'] .' '. date('h:i a', strtotime(@$t['horaentrega'])); ?></div>
                                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                    </div>
                                    <div class="col-xs-4 estado_promedio border-left">
                                        <div class="dato <?php echo empty(@$t['asignacion_alumno']['estado'])?'':$arrEstados[@$t['asignacion_alumno']['estado']]['clase']; ?>"><?php echo empty(@$t['asignacion_alumno']['estado'])?'--':$arrEstados[@$t['asignacion_alumno']['estado']]['nombre']; ?></div>
                                        <div class="info"><?php echo JrTexto::_("State")?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-center">
                                <a href="<?php 
                                if(!empty(@$t['asignacion_alumno']['iddetalle'])){
                                    $ruta = '/tarea/ver/?id='.@$t['asignacion_alumno']['iddetalle'];
                                }else{
                                    $ruta = '/tarea/asignar_alummo/?idtarea='.@$t['idtarea'];
                                }
                                echo $this->documento->getUrlBase().@$ruta;?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                            </div>
                        </div>
                    </div>
                    <?php } 
                } else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("No activity found").'</h4></div>'; } ?>
                </div>
            </div>        
            <div id="pnl-finalizados" class="row tab-pane fade">
                    <div class="hidden pnlcargando">
                        <div class="cargando" style="text-align: center; margin-top: 2em;"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/loading.gif"><br>
                        <span style="color: #006E84;"><?php echo JrTexto::_("Loading"); ?></span></div>
                    </div>
                    <div class="hidden pnlvacio">
                        <div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>
                    </div>
                    <div class="listado">
                        <?php if(!empty($this->tareasFin)) {
                        foreach ($this->tareasFin as $t) { ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                                <div class="panel-heading titulo">
                                    <h3><?php echo $t['nombre']; ?></h3>
                                </div>
                                <div class="panel-body contenido">
                                    <div class="col-xs-4 portada">
                                        <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                        if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                        <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                                    </div>
                                    <div class="col-xs-8 descripcion">
                                        <?php echo $t['descripcion']; ?>
                                    </div>
                                    <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                        <div class="col-xs-8 fecha-entrega">
                                            <div class="dato"><?php echo empty(@$t['fechaentrega'])?'---':@$t['fechaentrega'] .' '. date('h:i a', strtotime(@$t['horaentrega'])); ?></div>
                                            <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                        </div>
                                        <div class="col-xs-4 estado_promedio border-left">
                                            <div class="dato">
                                                <i><?php echo ((@$t['asignacion_alumno']['notapromedio']!='')?$t['asignacion_alumno']['notapromedio']:'---'); ?></i>
                                                <small> / <?php echo round(@$t['puntajemaximo']).'</small>'; ?></small>
                                            </div>
                                            <div class="info"><?php echo JrTexto::_("Score")?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id='.$t['asignacion_alumno']['iddetalle'];?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                                </div>
                            </div>
                        </div>
                        <?php } } else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("No activity found").'</h4></div>'; } ?>
                    </div>
            </div>
        </div>
    </div>
</div>

<section class="hidden">
    <div class="col-xs-12 col-sm-6" id="clone-tarea_item">
        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="">
            <div class="panel-heading titulo">
                <h3>title</h3>
            </div>
            <div class="panel-body contenido">
                <div class="col-xs-4 portada">
                    <img src="<?php echo $this->documento->getUrlStatic();?>/media/web/nofoto.jpg" alt="img" class="img-responsive">
                </div>
                <div class="col-xs-8 descripcion">
                    descripcion
                </div>
                <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                    <div class="col-xs-8 fecha-entrega">
                        <div class="dato">dd-mm-yyyy hh:ii aa</div>
                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                    </div>
                    <div class="col-xs-4 estado_promedio border-left">
                        <div class="dato">--</div>
                        <div class="info"><?php echo JrTexto::_("State")?></div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id=';?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>
</section>

<script>
/**Emmy modificacion */
var setHistorial = <?php echo $setHistorial ?>;
var oIdHistorial = {'tarea': 0}
/*////////*/
var ESTADOS = JSON.parse('<?php echo json_encode($arrEstados); ?>');

var fnAjaxFail = function(xhr, textStatus, errorThrown) {
    //console.log("Error");
    //console.log(xhr);
    //console.log(textStatus);
    throw errorThrown;
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
};

var nuevoItemTarea = function(tarea, $contenedor) {
    var $itemTarea = $('#clone-tarea_item').clone();
    $itemTarea.removeAttr('id');
    $itemTarea.find('.panel-heading.titulo>h3').html(tarea.nombre);
    $itemTarea.find('.tarea-item').attr('data-idtarea',tarea.idtarea);
    var img_src=(tarea.foto!='')?tarea.foto:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemTarea.find('.panel-body.contenido>.portada>img').attr('src', img_src.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
    $itemTarea.find('.panel-body.contenido>.descripcion').html(tarea.descripcion);
    $itemTarea.find('.panel-body.contenido .fecha-entrega>.dato').html(($('#opcIdCurso').val()!=''?'---':tarea.fechaentrega)+' '+($('#opcIdCurso').val()!=''?'':tarea.horaentrega));
    if( $('#hIdCurso').val()=='' ) {
        if(tarea.asignacion_alumno.estado=='N' || tarea.asignacion_alumno.estado=='D'){
            $itemTarea.find('.panel-body.contenido .estado_promedio>.dato').html(ESTADOS[tarea.asignacion_alumno.estado]['nombre']).addClass(ESTADOS[tarea.asignacion_alumno.estado]['clase']);
            $itemTarea.find('.panel-body.contenido .estado_promedio>.info').html('<?php echo JrTexto::_('State'); ?>');
        }
        if(tarea.asignacion_alumno.estado=='P' || tarea.asignacion_alumno.estado=='E'){
            var calificacion = (tarea.asignacion_alumno.notapromedio!=null)?tarea.asignacion_alumno.notapromedio:'---';
            $itemTarea.find('.panel-body.contenido .estado_promedio>.dato').html('<i>'+calificacion+'</i><small> / '+parseFloat(tarea.puntajemaximo).toFixed(0)+'</small>');
            $itemTarea.find('.panel-body.contenido .estado_promedio>.info').html('<?php echo JrTexto::_('Score'); ?>');
        }
    }

    
    if( $('#opcIdCurso').val()!='' ) { var ruta = '/tarea/asignar_alummo/?idtarea='+tarea.idtarea; }
    else { var ruta = '/tarea/ver/?id='+tarea.asignacion_alumno.iddetalle; }
    $itemTarea.find('.panel-footer a.vertarea').attr('href', _sysUrlBase_+ruta);

    $contenedor.append($itemTarea);
};

var currentRequest = null;
var cargarTareas = function(filtros=[]){
    //console.log(filtros);
    var urlAjax = _sysUrlBase_+'/tarea/listarNuevasDevueltasYFinalizadas/';
    if( $('#opcIdCurso').val()!='' ) { urlAjax = _sysUrlBase_+'/curso/listarNuevasDevueltasYFinalizadas/'; }
    currentRequest = $.ajax({
        url: urlAjax,
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }

            $('.tab-content .tab-pane').each(function(index, el) {
                $(el).find('.pnlcargando').removeClass('hidden');
                $(el).find('.listado').addClass('hidden');
            });

        },
    }).done(function(resp) {
        if(resp.code=='Error') {
            return false;
        }
        var pendientes = resp.data.pendientes;
        var finalizadas = resp.data.finalizadas;
        var todas = resp.data.todo;

        $('#pnl-pendientes .pnlcargando').addClass('hidden');
        $('#pnl-finalizados .pnlcargando').addClass('hidden');

        if(pendientes.length){
            $('#pnl-pendientes .listado').html('');
            $.each(pendientes, function(i, tarea) {
                nuevoItemTarea(tarea, $('#pnl-pendientes .listado'));
            });
            $('.nav.nav-tabs .tabpendientes').trigger('click');
            $('#pnl-pendientes .pnlvacio').addClass('hidden');
            $('#pnl-pendientes .listado').removeClass('hidden');
            $('.tabpendientes .badge').text(pendientes.length).removeClass('hidden');
        }else{
            $('#pnl-pendientes .listado').removeClass('');
            $('#pnl-pendientes .pnlvacio').removeClass('hidden');
            $('.tabpendientes .badge').text('0').addClass('hidden');
        }
        if(finalizadas.length){
            $('#pnl-finalizados .listado').html('');
            $.each(finalizadas, function(i, tarea) {
                nuevoItemTarea(tarea, $('#pnl-finalizados .listado'));
            });
            $('#pnl-finalizados .pnlvacio').addClass('hidden');
            $('#pnl-finalizados .listado').removeClass('hidden');
            $('.tabfinalizados .badge').text(finalizadas.length).removeClass('hidden');
        }else{
            $('#pnl-finalizados .listado').html('');
            $('#pnl-finalizados .pnlvacio').removeClass('hidden');
            $('.tabfinalizados .badge').text('0').addClass('hidden');
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};

/*var leerniveles=function(data){
    try{
        var res = xajax__('', 'niveles', 'getxPadre', data);
        if(res){ return res; }
        return false;
    }catch(error){
        return false;
    }       
};*/

/*var addniveles=function(data,obj){
    var objini=obj.find('option:first').clone();
    obj.find('option').remove();
    obj.append(objini);
    if(data!==false){
        var html='';
        $.each(data,function(i,v){
            html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
        });
        obj.append(html);
    }
    id=obj.attr('id');
    if(id==='activity-item') {
        var filtros = {
            'idnivel': $('#level-item').val(),
            'idunidad': $('#unit-item').val(),
            'idactividad': $('#activity-item').val(),
            'idcurso':$('#hIdCurso').val(),
        };
        cargarTareas(filtros);
    }
};*/

var registrarHistorialSesion = function(idTabPane = null){
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();

    var lugar = idTabPane == null ? 'T' : type(idTabPane);

    $.ajax({
        url: _sysUrlBase_+'/historial_sesion/agregar',
        type: 'POST',
        dataType: 'json',
        data: {'lugar': lugar, 'fechaentrada': fechahora },
    })
    .done(function(resp) {
        if(resp.code=='ok'){
            oIdHistorial.tarea = resp.data.idhistorialsesion;
        } else {
            mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
        }
    })
    .fail(function(xhr, textStatus, errorThrown) {
    });
    return 0;
};

var editarHistoriaSesion = function(id = null){

    var _id = id != null ? id : 0 ;
    var now = new Date();
    var fechahora = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate() + " " +  now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
    $.ajax({
            url: _sysUrlBase_+'/historial_sesion/editar',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {'idhistorialsesion': _id, 'fechasalida': fechahora},
        })
        .done(function(resp) {
            if(resp.code=='ok'){
                if(id === null){
                    _IDHistorialSesion = resp.data.idhistorialsesion;
                }else{
                    oIdHistorial.tarea = resp.data.idhistorialsesion;
                }
            } else {
                return false;
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            return false;
        });
};

$(document).ready(function() {
    /**EmmyModificacion */
    if(setHistorial == true){
        registrarHistorialSesion();
    }

    $('#filtros-actividad').on('change', '.select-nivel', function(e) {
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') == "opcIdCurso" ) { $('#hIdCurso').val( idCurso ); }
     
        if(idCurso==0){ return false; }
        cargarTareas({'idcurso': idCurso});
    });

  

    /***** escribir Tab en la URL *****/
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    $('.nav-tabs a').on('shown.bs.tab', function (e) {     
        window.location.hash = e.target.hash;
    })
    /**** FIN escribir Tab en la URL ****/
});
$(window).on('beforeunload', function(){
    if(setHistorial == true){
        editarHistoriaSesion(oIdHistorial.tarea);
    }
});
</script>