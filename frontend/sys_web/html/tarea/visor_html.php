<?php
$usuarioAct = NegSesion::getUsuario();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<input type="hidden" name="hIdArchivoRespuesta" id="hIdArchivoRespuesta" value="<?php echo (@$this->archivo['tablapadre']=='R')?@$this->archivo['idtarea_archivos']:''; ?>">
<input type="hidden" name="hTipoAdjunto" id="hTipoAdjunto" value="<?php echo @$this->archivo['tipo']; ?>">
<input type="hidden" name="hIdArchivo" id="hIdArchivo" value="<?php echo @$this->idtarea_archivos; ?>">
<input type="hidden" name="hIdRespuesta" id="hIdRespuesta" value="<?php echo @$this->respuesta['idtarea_respuesta']; ?>">

<div class="" id="tarea-visor">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('Activity')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div></div>
    <div class="row"> <div class="col-xs-12">
        <div class="panel pnl-contenedor" id="visor-html" style="border:1px solid #5bc0de;">
            <div class="panel-heading bg-info">
                <div class="row">
                    <div class="col-xs-2"><img src="<?php echo str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), @$this->tarea['foto']) ?>" alt="cover" class="img-responsive"></div>
                    <div class="col-xs-10">
                        <h3><?php echo @$this->tarea['nombre']; ?></h3>
                        <p><?php echo @$this->tarea['descripcion']; ?></p>
                    </div>
                </div>
            </div>
            <div class="panel-body metod active" data-idmet="3" id="met-3">
                <div class="col-xs-12 hidden">
                    <ul class="nav nav-tabs ejercicios">
                    <?php foreach ($this->html as $i=>$elem) { ?>
                        <li data-id="<?php echo @$elem['id'] ?>" class="<?php echo ($i==0)?'active':'';?>"><a href="#adj_<?php echo @$elem['id'] ?>" data-toggle="tab"><?php echo $i+1; ?></a></li>
                    <?php } ?>
                        <li class="hidden"><?php /* Tab adicional obligatorio: en "calcularBarraProgreso();" del JS "static/js/new/completar.js" siempre se resta 1 por el btnAgregarTab que no se debia contar al momento de hacer el calculo */ ?></li>
                    </ul>
                </div>

                <div id="contenedor-paneles">
                    <div id="panel-barraprogreso" class="text-center eje-panelinfo" style="display: none;">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                <span>0</span>%
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-12 tab-content">
                    <?php if($this->archivo['tipo']=="A"){
                    foreach ($this->html as $i=>$elem) { ?>
                    <div id="adj_<?php echo $elem['id'] ?>" data-id="<?php echo $elem['id'] ?>" class="tabhijo tab-pane fade <?php echo ($i==0)?'active in':'';?>">
                        <?php echo str_replace('__xRUTABASEx__',$this->documento->getUrlBase(), $elem['texto']); ?>
                    </div>
                    <?php }
                    } elseif($this->archivo['tipo']=="J" || $this->archivo['tipo']=="E") {
                        $clase=($this->archivo['tipo']=="J")?'frame_juego':'frame_examen';
                        $ruta_iframe=$this->documento->getUrlBase();
                        $idtarea_archivo=($this->archivo['tablapadre']=='R')?$this->archivo['idtarea_archivos_padre']:$this->archivo['idtarea_archivos'];
                        if($this->archivo['tipo']=="J"){ $ruta_iframe.='/game/ver/'.$this->html[0]['id'].'?'; }
                        if($this->archivo['tipo']=="E"){ $ruta_iframe.='/examenes/teacherresrc_view?idexamen='.$this->html[0]['id'].'&'; }
                        $ruta_iframe.='idtarea_archivos='.$idtarea_archivo;
                        if(!empty($this->asignacion)){ $ruta_iframe.='&idtarea_asignacion='.$this->asignacion['idtarea_asignacion']; } 
                        if(!empty($this->idalumno)){ $ruta_iframe.='&idalumno='.$this->idalumno; }
                        ?>
                    <div class="alert alert-info alert-dismissible" role="alert"> 
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong><i class="fa fa-info-circle"></i> <?php echo ucfirst(JrTexto::_('Tip')); ?>: </strong>
                        <?php echo ucfirst(JrTexto::_('You must completely solve the following exercise')).'. '.ucfirst(JrTexto::_('This type of exercise can be solved only once')); ?>.
                    </div>
                    <iframe src="<?php echo $ruta_iframe; ?>" id="iframeVisor" class="tabhijo <?php echo $clase; ?>" style="border:none; height: 500px; width: 100%;"></iframe>
                    <?php } ?>
                </div>
            </div>
            <div class="panel-footer" style="overflow: hidden;">
                <div class="col-xs-3 text-left">
                    <button class="btn btn-danger cerrar_adjunto"><i class="fa fa-times"></i> <?php echo JrTexto::_('Close'); ?></button>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="btn-group btn-group-justified btns-navegacion">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default navegacion atras"><i class="fa fa-chevron-left"></i></button>
                        </div>
                        <div class="btn-group text-center actual-total">
                            <span class="actual">1</span>
                            <span class="total"> <?php echo count($this->html); ?></span>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default navegacion sgte"><i class="fa fa-chevron-right"></i></button>
                        </div>
                    </div>
                </div>
                <?php if(@$this->asignacion_alumno['estado']=='N' || @$this->asignacion_alumno['estado']=='D') { ?>
                <div class="col-xs-7 col-sm-3 text-right">
                    <a href="#" class="btn btn-success guardar_progreso"><?php echo ucfirst(JrTexto::_('save')); ?></a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div></div>
</div>

<script>
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;
$(document).ready(function() {
    const PUNTAJE_MAX=<?php echo @$this->tarea['puntajemaximo']; ?>;
    const PUNTAJE_MIN=<?php echo @$this->tarea['puntajeminimo']; ?>;

    var initVisorHTML = function (){
        var tipoAdj = $('#hTipoAdjunto').val();
        if(tipoAdj=='A'){
            $('#visor-html').tplcompletar({editando:false});
            $('#visor-html ul.ejercicios>li>a').first().trigger('click');
        }
        if(tipoAdj=='J' || tipoAdj=='E'){
            $('.btns-navegacion').hide();
        }
    };

    $('#visor-html .btns-navegacion .btn.navegacion').click(function(e) {
        var $liActivo = $('#visor-html ul.ejercicios li.active');
        var actual = $liActivo.text();
        if($(this).hasClass('atras')){
            var $tab = $liActivo.prev('li').find('a');
        }else if($(this).hasClass('sgte')){
            var $tab = $liActivo.next('li').find('a');
        }
        if(!$tab.length){ return false;}
        $tab.trigger('click');
        actual = $tab.text();

        $('#visor-html .btns-navegacion .actual-total .actual').text(actual);
    });

    $('#visor-html .panel-footer .btn.cerrar_adjunto').click(function(e) {
        $.confirm({
            title: '<?php echo JrTexto::_('Close');?>',
            content: '<?php echo JrTexto::_('Are you sure to close this window?');?>',
            confirmButton: '<?php echo JrTexto::_('Yes');?>',
            cancelButton: '<?php echo JrTexto::_('No');?>',
            confirmButtonClass: 'btn-danger',
            cancelButtonClass: 'btn-default',
            closeIcon: true,
            confirm: function(){
                window.close();
            },
        });
    });

    <?php if(@$this->asignacion_alumno['estado']=='N' || @$this->asignacion_alumno['estado']=='D') { ?>
    var calcularPuntaje= function(){
        var porcentAcumulado= parseFloat($('.metod.active').attr('data-progreso'))||0.0;
        var ptjeAcumulado= porcentAcumulado*PUNTAJE_MAX/100;
        return ptjeAcumulado;
    };

    var prepararHTML= function() {
        var arrHTML= [];
        $('.metod.active .tab-content .tabhijo').each(function(i, tab_pane) {
            var elem = {
                'id': $(tab_pane).attr('data-id'),
                'html': $(tab_pane).html(),
            };
            arrHTML.push(elem);
        });
        return arrHTML;
    };

    var guardarProgresoActiv= function(){
        var ptjeAcum = calcularPuntaje();
        var arrHtml = prepararHTML();
        var formData = new FormData();
        formData.append('txtIdtarea_archivos', $('#hIdArchivoRespuesta').val());
        formData.append('txtTablapadre', 'R');
        formData.append('txtIdpadre', $('#hIdRespuesta').val());
        formData.append('txtRuta', _sysUrlBase_+'/tarea_archivos/visor/?idarchivo=');
        formData.append('txtTipo', $('#hTipoAdjunto').val());
        formData.append('txtPuntaje', ptjeAcum);
        formData.append('txtTexto', JSON.stringify(arrHtml));
        formData.append('txtIdtarea_archivos_padre', $('#hIdArchivo').val());

        if($('#hTipoAdjunto').val()=='A'){ formData.append('txtNombre', 'Activity'); }
        else if ($('#hTipoAdjunto').val()=='J') { formData.append('txtNombre', 'Game'); }
        else if ($('#hTipoAdjunto').val()=='E') { formData.append('txtNombre', 'Exam'); }

        $.ajax({
            url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos/',
            type: 'POST',
            dataType: 'json',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() {
                $('#visor-html .guardar_progreso').prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
            },
        }).done(function(resp) {
            if(resp.code=='ok'){
                $('#hIdArchivoRespuesta').val(resp.idtarea_archivos);
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            throw errorThrown;
            mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
        }).always(function() {
            $('#visor-html .btn.guardar_progreso').removeAttr('disabled');
            $('#visor-html .btn.guardar_progreso i.fa').remove();
        });
    };

    $('#visor-html .guardar_progreso').click(function(e) {
        e.preventDefault();
        if($('#hTipoAdjunto').val()=='A'){
            guardarProgresoActiv();
        }
    });
    <?php } ?>

    $('iframe.frame_juego').load(function() {
        var guardarProgresoJuego = null;
        var $iframe=$(this);
        var $innerFrame = $iframe.contents();
        var $plantilla = $innerFrame.find('#games-tool .games-main .plantilla');
        var fnInit = function(){ borrarNoPreview($plantilla, $innerFrame); };
        guardarProgresoJuego=function() {
            var puntajeAcum = calcularPuntajeJuego(PUNTAJE_MAX, $plantilla);/* static/js/tools_games.js */
            var arrHtml = [];
            var elem = {
                'id': $innerFrame.find('#hIdGame').val(),
                'html': $innerFrame.find('#games-tool .games-main').html(),
            };
            arrHtml.push(elem);

            var formData = new FormData();
            formData.append('txtIdtarea_archivos', $innerFrame.find('#hIdArchivoRespuesta').val());
            formData.append('txtTablapadre', 'R');
            formData.append('txtIdpadre', $innerFrame.find('#hIdTareaRespuesta').val());
            formData.append('txtRuta', _sysUrlBase_+'/tarea_archivos/visor/?idarchivo=');
            formData.append('txtTipo', 'J');
            formData.append('txtPuntaje', puntajeAcum);
            formData.append('txtTexto', JSON.stringify(arrHtml));
            formData.append('txtIdtarea_archivos_padre', $innerFrame.find('#hIdTareaArchivo').val());
            formData.append('txtNombre', 'Game');
            $.ajax({
                url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos/',
                type: 'POST',
                dataType: 'JSON',
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function() {
                    $('#visor-html .guardar_progreso').prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
                },
            }).done(function(resp) {
                if(resp.code=='ok'){
                    $innerFrame.find('#hIdArchivoRespuesta').val(resp.idtarea_archivos);
                    mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
                }else{
                    mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                throw errorThrown;
                mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
            }).always(function() {
                $('#visor-html .btn.guardar_progreso').removeAttr('disabled');
                $('#visor-html .btn.guardar_progreso i.fa').remove();
            });
        };

        var idTareaRspta = $innerFrame.find('#hIdArchivoRespuesta').val();
        if(idTareaRspta>0){ 
            $('#visor-html .btn.guardar_progreso').hide(); 
        }else{
            if($plantilla.hasClass('plantilla-sopaletras')){ 
                fnInit=function(){ iniciarSopaLetras($plantilla); borrarNoPreview($plantilla, $innerFrame); };
            }else if($plantilla.hasClass('plantilla-crucigrama')){ 
                fnInit=function(){ iniciarCrucigrama($plantilla); borrarNoPreview($plantilla, $innerFrame); };
            }else if($plantilla.hasClass('plantilla-rompecabezas')){ 
                fnInit=function(){ iniciarRompecabezas($plantilla); borrarNoPreview($plantilla, $innerFrame); }; 
            }
        }
        
        var tipoAdj = $('#hTipoAdjunto').val();
        if(tipoAdj=='J'){
            $innerFrame.find('#games-tool .games-main').iniciarVisor({
                btnGuardar: '#visor-html .btn.guardar_progreso',
                fnGuardarProgreso: guardarProgresoJuego,
                fnAlIniciar: fnInit,
                texto: {
                    guardar: '<?php echo ucfirst(JrTexto::_('Save')); ?>',
                },
            });
        }
    });

    $('iframe.frame_examen').load(function() {
        var $iframe=$(this);
        var $innerFrame = $iframe.contents();
        var fnInit = null;
        var guardarProgresoExamen = function(){
            var arrHtml = [];
            var jsonPreguntas = {};
            $innerFrame.find('.panel-body .pnlpregunta .tmpl').each(function(i, elem) {
                var idEjerc = $(this).attr('data-idpregunta');
                var $divPadre = $(this).parent();
                var html = $(this).html().trim();
                jsonPreguntas[idEjerc]=html;
            });
            var elem={
                'id': $innerFrame.find('#hIdExamen').val(),
                'html': jsonPreguntas,
            };
            arrHtml.push(elem);

            var calif_max_exam=parseFloat($innerFrame.find('#calificacion_total').val())||100.00;
            var calif_obtenida=parseFloat($innerFrame.find('#infototalcalificacion').attr('data-puntaje'))||0.00;
            var puntajeAcum = (calif_obtenida*calif_max_exam)/PUNTAJE_MAX;

            var formData = new FormData();
            formData.append('txtIdtarea_archivos', $innerFrame.find('#hIdArchivoRespuesta').val());
            formData.append('txtTablapadre', 'R');
            formData.append('txtIdpadre', $innerFrame.find('#hIdTareaRespuesta').val());
            formData.append('txtRuta', _sysUrlBase_+'/tarea_archivos/visor/?idarchivo=');
            formData.append('txtTipo', 'E');
            formData.append('txtPuntaje', puntajeAcum);
            formData.append('txtTexto', JSON.stringify(arrHtml));
            formData.append('txtIdtarea_archivos_padre', $innerFrame.find('#hIdTareaArchivo').val());
            formData.append('txtNombre', 'Exam');
            $.ajax({
                url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos/',
                type: 'POST',
                dataType: 'JSON',
                data: formData,
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function() {
                    $('#visor-html .btn.guardar_progreso')
                        .prepend('<i class="fa fa-circle-o-notch fa-spin"></i>')
                        .attr('disabled', 'disabled');
                },
            }).done(function(resp) {
                if(resp.code=='ok'){
                    $innerFrame.find('#hIdArchivoRespuesta').val(resp.idtarea_archivos);
                    mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
                }else{
                    mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                throw errorThrown;
                mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
            }).always(function() {
                $('#visor-html .btn.guardar_progreso').removeAttr('disabled');
                $('#visor-html .btn.guardar_progreso i.fa').remove();
            })
        };

        var idTareaRspta = $innerFrame.find('#hIdArchivoRespuesta').val();
        if(idTareaRspta>0){ 
            $('#visor-html .btn.guardar_progreso').hide(); 
        }

        var tipoAdj = $('#hTipoAdjunto').val();
        if(tipoAdj=='E'){
            $innerFrame.find('#examen_preview').iniciarVisor({
                btnGuardar: '#visor-html .btn.guardar_progreso',
                fnGuardarProgreso: guardarProgresoExamen,
                fnAlIniciar: fnInit,
                texto: {
                    guardar: '<?php echo ucfirst(JrTexto::_('Save')); ?>',
                },
            });
        }
    });

    $('#visor-html ul.ejercicios>li>a').click(function(e) {
        var id = $(this).closest('li').data('id');
        var ruta = $('#visor-html .tab-content iframe').data('ruta');
        $('#visor-html .tab-content iframe').attr('src', ruta+id);
    });


    initVisorHTML();
});

</script>