<?php 
$RUTA_BASE = $this->documento->getUrlBase();
$tarea = @$this->datos;
$arrIcon=array('D'=> 'fa fa-paperclip', 'V'=> 'fa fa-video-camera', 'L'=> 'fa fa-link', 'G'=> 'fa fa-microphone', 'A'=> 'fa fa-font', 'J'=> 'fa fa-puzzle-piece', 'E'=> 'fa fa-list' );
/*
var_dump($this->habilidades);
var_dump($this->habilidadColor);
var_dump($tarea['tarea_asignacion']);
echo '<pre>'; print_r($tarea); echo '</pre>';
echo '<pre>'; print_r($tarea['tarea_asignacion']['detalle']); echo '</pre>';
*/
 ?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
 <input type="hidden" id="hIdTarea_Asignacion" name="hIdTarea_Asignacion" value="<?php echo @$this->datos['idtarea_asignacion']; ?>">
 <input type="hidden" name="hIdTarea" id="hIdTarea" value="<?php echo @$tarea['tarea_asignacion']['idtarea']; ?>">
<div class="" id="tarea-see">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $RUTA_BASE;?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $RUTA_BASE;?>/tarea"><?php echo ucfirst(JrTexto::_('Activity')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $tarea['idtarea']; ?>">
                <div class="panel-heading titulo">
                    <h3><?php echo $tarea['nombre']; ?></h3>
                    <!-- <div class="opciones">
                        <a href="<?php echo $RUTA_BASE.'/tarea/editar/?id='.$tarea['idtarea'];?>" class="color-blue editar istooltip" title="<?php echo ucfirst(JrTexto::_("Edit activity")); ?>"><i class="fa fa-pencil"></i></a>
                    </div> -->
                </div>
                <div class="panel-body contenido">
                    <div class="col-xs-5" style="padding-right: 0;">
                        <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                        if(!empty($tarea['foto'])){ $src = str_replace('__xRUTABASEx__', $RUTA_BASE, $tarea['foto']); }  ?>
                        <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                    </div>
                    <div class="col-xs-7 descripcion" style="overflow: auto;">
                        <?php echo $tarea['descripcion']; ?>
                    </div>
                    <div class="col-xs-12" style="padding:0;">
                        <div class="table-key-value">
                            <div class="table-kv-row">
                                 <div class="key"><?php echo ucfirst(JrTexto::_("Date of presentation")); ?></div>
                                 <div class="value"><span data-value="<?php echo $tarea['fechaentrega'].' '.$tarea['horaentrega']; ?>"><?php echo  date('d-m-Y', strtotime($tarea['fechaentrega'])).' '. date('h:ia', strtotime($tarea['horaentrega'])); ?></span> <a href="#" class="color-blue btn-live-edit pull-right istooltip" data-liveedit="date" title="<?php echo ucfirst(JrTexto::_("Edit date of presentation")); ?>"><i class="fa fa-pencil"></i></a></div>
                             </div>
                             <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Course")); ?></div>
                                <div class="value"><?php echo $tarea['curso']['nombre']; ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Session")); ?></div>
                                <div class="value"><?php $arrCDet = array();
                                foreach ($tarea['curso_detalles'] as $cdet) {
                                    $arrCDet[] = $cdet['nombre'];
                                }
                                echo implode(' - ', $arrCDet) ?></div>
                            </div>
                            <div class="table-kv-row puntajemaximo">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Maximum score")); ?></div>
                                <div class="value"><?php echo $tarea['puntajemaximo']; ?></div>
                            </div>
                            <div class="table-kv-row puntajeminimo">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?></div>
                                <div class="value"><?php echo $tarea['puntajeminimo']; ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Assigned")); ?></div>
                                <div class="value"><?php echo count($tarea['tarea_asignacion']['detalle']).' '.JrTexto::_("students"); ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Submitted")); ?></div>
                                <div class="value"><?php $cantPresentados=0;
                                foreach ($tarea['tarea_asignacion']['detalle'] as $asig_alum) {
                                    if($asig_alum['estado']=='P' || $asig_alum['estado']=='E'){ $cantPresentados++; }
                                }
                                echo $cantPresentados.' '.JrTexto::_("activity"); 
                                ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="max-height: 270px; overflow: auto;">
                    <table class="table table-striped">
                        <tbody>
                        <?php if(!empty($tarea['tarea_archivos'])){
                            foreach ($tarea['tarea_archivos'] as $adj){ ?>
                            <tr data-id="<?php echo $adj['idtarea_archivos'] ?>">
                                <td style="width:7%"><i class="<?php echo $arrIcon[$adj['tipo']] ?>"></i></td>
                                <td><?php echo $adj['nombre'] ?></td>
                                <td style="width:18%">
                                    <a href="<?php $ruta=$adj['ruta'];
                                    if($adj['tipo']=='A' || $adj['tipo']=='J' || $adj['tipo']=='E'){
                                        $ruta.=$adj['idtarea_archivos'];
                                    }
                                    echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta); ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-8">
        <?php foreach ($tarea['tarea_asignacion']['detalle'] as $asig_alum) { 
            $activo=($asig_alum['estado']=='P' || $asig_alum['estado']=='E')?'active':''; ?>
            <div class="panel pnl-contenedor asignacion_alumno" id="asignacion_alumno-<?php echo $asig_alum['iddetalle']; ?>" data-idasignacionalumno="<?php echo $asig_alum['iddetalle']; ?>">
                <div class="panel-body" style="padding: 0;">
                    <div class="col-xs-3 col-sm-1 alumno_foto">
                    <?php
                    $src = $this->documento->getUrlStatic().'/media/imagenes/user_avatar.jpg';
                    // if(!empty($asig_alum['foto'])){ $src = str_replace('__xRUTABASEx__', $RUTA_BASE, $asig_alum['foto']); }
                    // if(!empty($asig_alum['foto'])){ $src = $this->documento->getUrlStatic().'/media/imagenes/'.$asig_alum['foto']; }
                    ?>
                        <img src="<?php echo $src;?>" alt="avatar" class="img-responsive">
                    </div>
                    <div class="col-xs-9 col-sm-8 nombre_fecha">
                        <div class="alumno_nombre" style="text-transform: capitalize;"><?php echo $asig_alum['ape_paterno'].' '.$asig_alum['ape_materno'].' '.$asig_alum['nombre']; ?></div>

                        <small class="presentacion"><?php if( ($asig_alum['estado']=='P' || $asig_alum['estado']=='E') && isset($asig_alum['fechapresentacion']) ){ ?>
                            Subido: <span class="fecha_hora"><?php echo date('d-m-Y',strtotime($asig_alum['fechapresentacion'])).' '.date('h:i a',strtotime($asig_alum['horapresentacion'])); ?><span><?php }elseif($asig_alum['estado']=='D'){ echo '<i>'.JrTexto::_('Activity was returned to student').'.</i>'; }else{ echo '<i>'.JrTexto::_('Activity not completed, yet').'.</i>'; } ?></small>
                    </div>
                    <div class="col-xs-9 col-sm-3 puntaje <?php if($asig_alum['notapromedio']!=''){ echo ($asig_alum['notapromedio']>=$tarea['puntajeminimo'])?'color-blue':'color-red'; } ?>"><div class="pull-right">
                        <span class="obtenido"><?php echo ($asig_alum['notapromedio']!='')?$asig_alum['notapromedio']:'---'; ?></span>
                        <span class="total"><?php echo round($tarea['puntajemaximo']);?></span>
                        <small><?php echo JrTexto::_('Score');?></small>
                    </div></div>
                </div>
                <div class="panel-footer text-center ver_respuesta <?php echo $activo;?>" title="<?php echo JrTexto::_('View answer');?>"><i class="fa fa-chevron-down fa-2x"></i></div>
                
                <?php if($asig_alum['estado']=='P' || $asig_alum['estado']=='E'){ ?>
                <div class="panel-body border-top respuesta" id="respuesta-<?php echo @$asig_alum['idtarea_respuesta']; ?>" style="display: none;">
                    <div class="col-xs-12 descripcion">
                        <?php if(!empty(@$asig_alum['comentario'])){ ?>
                        <div class="alert alert-info" role="alert"> 
                            <strong><?php echo ucfirst(JrTexto::_('Student comments')); ?>:</strong>
                            <p><?php echo @$asig_alum['comentario']; ?></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12" style="max-height: 171px; overflow: auto;">
                        <table class="table table-striped" id="tblRespuestaArchivos">
                            <tbody>
                            <?php if(!empty($asig_alum['respuesta_archivos'])){
                                foreach ($asig_alum['respuesta_archivos'] as $arch){ ?>
                                <tr data-id="<?php echo $arch['idtarea_archivos'] ?>">
                                    <td style="width:7%"><i class="<?php echo $arrIcon[$arch['tipo']] ?>"></i></td>
                                    <td><?php echo $arch['nombre'] ?></td>
                                    <td style="width:10%">
                                        <a href="<?php $ruta=$arch['ruta'];
                                        if($arch['tipo']=='A' || $arch['tipo']=='J' || $arch['tipo']=='E'){
                                            $ruta.=$arch['idtarea_archivos_padre'].'&idasig_alum='.@$asig_alum['iddetalle'].'&idalumno='.$asig_alum['idalumno'];
                                        }
                                        echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta) ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                    </td>
                                    <td style="width:15%">
                                        <?php if($asig_alum['estado']!='E'){ ?>
                                        <button class="btn btn-xs btn-default habilidades" data-toggle="popover" title="<?php echo ucfirst(JrTexto::_('Choose skills')); ?>"><i class="fa fa-paint-brush"></i></button>
                                        <?php }else{ 
                                            $arrHabilid = json_decode($arch['habilidad'],true);
                                            if(!empty($arrHabilid))
                                            foreach ($arrHabilid as $idHab) { ?>
                                        <div class="btn btn-xs istooltip" style="background: <?php echo $this->habilidadColor[$idHab] ?>; color:#fff;" title="<?php echo $this->habilidades[$idHab]; ?>"><?php echo $this->habilidades[$idHab][0]; ?></div>
                                        <?php }
                                    } ?>
                                    </td>
                                    <td style="width:20%" class="puntaje">
                                        <div class="col-xs-8" style="padding: 0;">
                                            <input type="number" class="form-control text-right txtCalificacion" id="txtCalificacion-<?php echo $arch['idtarea_archivos'] ?>" name="txtCalificacion-<?php echo $arch['idtarea_archivos'] ?>" min="0" max="<?php echo round($tarea['puntajemaximo']); ?>" data-idarchivo="<?php echo $arch['idtarea_archivos'] ?>" value="<?php echo ($arch['puntaje']!='')?$arch['puntaje']:'0'; ?>" <?php echo ($asig_alum['estado']=='E')?'readonly':'';?> >
                                        </div>
                                        <label class="col-xs-4 total text-left" style="padding: 0;"><?php echo round($tarea['puntajemaximo']);?></label>
                                    </td>
                                </tr>
                            <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 form-group border-top">
                        <label class="text-right col-xs-9"><?php echo JrTexto::_('Average score');?></label>
                        <div class="puntaje col-xs-3">
                            <div class="col-xs-9" style="padding: 0;">
                                <input type="text" class="form-control text-right txtPromedio" id="txtPromedio-<?php echo $asig_alum['iddetalle'] ?>" name="txtPromedio-<?php echo $asig_alum['iddetalle'] ?>" min="0" max="<?php echo round($tarea['puntajemaximo']); ?>" data-idasignacionalumno="<?php echo $asig_alum['iddetalle'] ?>" value="<?php echo ($asig_alum['notapromedio']!='')?$asig_alum['notapromedio']:'0'; ?>" readonly>
                            </div>
                            <label class="col-xs-3 total text-left" style="padding: 0;"><?php echo round($tarea['puntajemaximo']);?></label>
                        </div>
                    </div>
                    <?php if($asig_alum['estado']!='E'){ ?>
                    <div class="col-xs-12 botones_accion text-center">
                        <button class="btn btn-danger devolver"><?php echo JrTexto::_('Return');?></button>&nbsp;&nbsp;
                        <button class="btn btn-success evaluar"><?php echo JrTexto::_('Evaluate and save');?></button>
                    </div>
                    <div class="col-xs-12 devolucion" style="display: none;">
                        <label  class="col-xs-12" for="txtDevolucion-<?php echo $asig_alum['iddetalle']; ?>">
                            <?php echo JrTexto::_('Why are you returning this activity?');?>
                        </label>
                        <div class="col-xs-12 form-group">
                            <textarea name="txtDevolucion-<?php echo $asig_alum['iddetalle']; ?>" id="txtDevolucion-<?php echo $asig_alum['iddetalle']; ?>" rows="3" class="form-control txtDevolucion" data-idasignacionalumno="<?php echo $asig_alum['iddetalle']; ?>" placeholder="<?php echo JrTexto::_('Write some text');?>..."></textarea>
                        </div>
                        <div class="col-xs-12 botones_accion text-center">
                            <button class="btn btn-default cancelar"><?php echo JrTexto::_('Cancel');?></button>
                            <button class="btn btn-blue enviar"><?php echo ucfirst(JrTexto::_('Send'));?></button>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
$('.istooltip').tooltip({container:'#tarea-see'});
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;
var HABILIDAD=JSON.parse('<?php echo json_encode($this->habilidades);?>');
var HABILIDAD_COLOR=JSON.parse('<?php echo json_encode($this->habilidadColor);?>');
$('.habilidades[data-toggle="popover"]').popover({
    html:true,
    placement:'bottom',
    container:'#tarea-see',
    title: '<?php echo JrTexto::_('Choose skills'); ?>',
    content: function(){
        var skills = $(this).attr('data-idhabilidades');
        var arrSkills = [];
        if(skills!=undefined){
            arrSkills = JSON.parse(skills);
        }
        var popoverBody='<div class="escoger_habilidades">'+
            '<div class="row habilidades edicion"><div class="col-xs-12">';
        $.each(HABILIDAD, function(idHab, nombre) {
            if(idHab<8){
            var activo=($.inArray(idHab, arrSkills)!=-1)?'active':'';
            popoverBody+='<div class="col-xs-12 skill-container" title="<?php echo JrTexto::_('Click to activate'); ?>">'+
                    '<div class="btn-skill vertical-center '+activo+'" data-id-skill="'+idHab+'">'+nombre+'</div>'+
                '</div>';
            }
        });
        popoverBody+='</div></div>'+
            '<div class="row btns_accion border-top" style="padding-top:10px;"><div class="col-xs-12">'+
                '<div class="col-xs-6">'+
                    '<button class="btn btn-block btn-default cerrar"><?php echo JrTexto::_('Close'); ?></button>'+
                '</div>'+
                '<div class="col-xs-6">'+
                    '<button class="btn btn-block btn-primary guardar"><?php echo JrTexto::_('Save'); ?></button>'+
                '</div>'+
            '</div></div>'+
        '</div>'

        return popoverBody;
    },
});

$(document).ready(function() {
    var fnAjaxFail = function(xhr, textStatus, errorThrown) {
        //console.log("Error");
        //console.log(xhr);
        //console.log(textStatus);
        throw errorThrown;
        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    };

    var verRespuesta = function($obj) {
        if($obj.hasClass('open')){
            $obj.siblings('.panel-body.respuesta').hide('fast');
            $obj.removeClass('open');
            $obj.find('i').addClass('fa-chevron-down').removeClass('fa-chevron-up');
        }else{
            $obj.siblings('.panel-body.respuesta').show('fast');
            $obj.addClass('open');
            $obj.find('i').addClass('fa-chevron-up').removeClass('fa-chevron-down');
        }
    };

    var activarBtnHabilidad = function ($btn) {
        var skills = $btn.attr('data-idhabilidades');
        try { 
            var arrSkills = JSON.parse(skills); 
            if(arrSkills.length>0){ 
                $btn.addClass('btn-red completado').removeClass('btn-default');
                $btn.siblings('i.fa').remove();
            }
            else{ $btn.addClass('btn-default').removeClass('btn-red completado'); }
        }catch (e) { 
            $btn.addClass('btn-red completado').removeClass('btn-default');
        }
    };

    var initFechaYHora = function($selFecha, $selHora){
        var ahora = Date.now();
        var now = new Date();
        var fecha_hora = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+(now.getDate()+1)+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
        fecha = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
        hora = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
        $selFecha.datetimepicker({
            defaultDate: fecha_hora,
            date: fecha,
            format: 'DD-MM-YYYY',
            locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
            minDate: "<?php echo date("Y-m-d H:i:s") ?>",
        });
        $selHora.datetimepicker({
            defaultDate: fecha_hora,
            date: hora,
            format: 'LT',
            locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
        });
    };

    $('.panel.asignacion_alumno').on('click', '.panel-footer.ver_respuesta.active', function(e) {
        var $this = $(this);
        $('.ver_respuesta.active.open').not($this).each(function(i, el) {
            var $boton = $(el);
            verRespuesta($boton);
        });
        verRespuesta($this);
    }).on('click', '.panel-body.respuesta>.botones_accion>.devolver', function(e) {
        e.preventDefault();
        $(this).closest('.respuesta').find('.devolucion').show('fast');
        $(this).closest('.botones_accion').find('.btn').attr('disabled', 'disabled');
    }).on('click', '.panel-body.respuesta>.botones_accion>.evaluar', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var ptjeMax = parseFloat($('.tarea-item').find('.table-key-value .puntajemaximo>.value').text());
        var ptjeMin = parseFloat($('.tarea-item').find('.table-key-value .puntajeminimo>.value').text());
        var $rspta = $btn.closest('.respuesta');
        var $asignacion = $rspta.closest('.asignacion_alumno');
        var promedio=$rspta.find('.txtPromedio').val()||0;
        var arrPuntajes={
            'idasignacionalumno': $rspta.find('.txtPromedio').data('idasignacionalumno'),
            'notapromedio': promedio,
            'archivos': [],
        };
        var arrIdArch_SinHabilidades=[];
        $rspta.find('#tblRespuestaArchivos tr').each(function(i, tr) {
            /********      Comprobando que todos los Arch. tengan Habilidades      *******/
            if($(tr).find('.btn.habilidades').attr('data-idhabilidades')==undefined){
                arrIdArch_SinHabilidades.push($(tr).data('id'));
            }
        });
        if(arrIdArch_SinHabilidades.length>0){
            /*****      Hay Arch. sin habilidades, entonces NO puede continuar      ******/
            $.each(arrIdArch_SinHabilidades, function(i, idArch) {
                $rspta.find('#tblRespuestaArchivos tr[data-id="'+idArch+'"]').find('.btn.habilidades').siblings('i.fa').remove();
                $rspta.find('#tblRespuestaArchivos tr[data-id="'+idArch+'"]').find('.btn.habilidades').after(' <i class="fa fa-exclamation-circle color-danger" style="display: none;"></i>');
                $rspta.find('#tblRespuestaArchivos tr[data-id="'+idArch+'"]').find('.btn.habilidades').siblings('i.fa').show('fast');
            });
            return false;
        }
        $rspta.find('#tblRespuestaArchivos tr').each(function(i, tr) {
            var arch={
                'idtarea_archivos': $(tr).data('id'),
                'puntaje': $(tr).find('.txtCalificacion').val(),
                'habilidades': $(tr).find('.btn.habilidades').attr('data-idhabilidades'),
            };
            arrPuntajes['archivos'].push(arch);
        });
        arrPuntajes['archivos']=JSON.stringify(arrPuntajes['archivos']);

        $.confirm({
            title: '<?php echo JrTexto::_('Evaluate and save');?>',
            content: '<?php echo JrTexto::_('The average score will be saved and can not longer modify');?>'+'.<br><center><strong><?php echo JrTexto::_('Average score'); ?>: '+promedio+'</strong></center><br>'+'<?php echo JrTexto::_('Do you want to continue?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-success',
            cancelButtonClass: 'btn-default',
            closeIcon: true,
            confirm: function(){
                $.ajax({
                    url: _sysUrlBase_+'/tarea/xGuardarPuntajes',
                    type: 'POST',
                    dataType: 'json',
                    data: arrPuntajes,
                    beforeSend: function() {
                        $btn.prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
                        $btn.siblings('.btn').attr('disabled', 'disabled');
                    },
                }).done(function(resp) {
                    if(resp.code=='ok'){
                        if(promedio>=ptjeMin){
                            $asignacion.find('.puntaje').first().addClass('color-blue');
                        }else{
                            $asignacion.find('.puntaje').first().addClass('color-red');
                        }
                        $asignacion.find('.puntaje .obtenido').html(promedio);
                        $rspta.find('.txtCalificacion').attr('readonly', 'readonly');
                        $rspta.find('.botones_accion').remove();
                        $rspta.find('.devolucion').remove();

                        /** Cambiar boton Choose-Skills **/
                        $rspta.find('#tblRespuestaArchivos tr .btn.habilidades').each(function(index, el) {
                            var arrIdHabilidades = JSON.parse($(el).attr('data-idhabilidades'));
                            var divHabilidades = '';
                            $.each(arrIdHabilidades, function(i, idHab) {
                                divHabilidades+='<div class="btn btn-xs istooltip" style="background: '+HABILIDAD_COLOR[idHab]+'; color:#fff;" title="'+HABILIDAD[idHab]+'">'+HABILIDAD[idHab].charAt(0)+'</div> ';
                            });
                            $(el).replaceWith(divHabilidades);
                        });
                        $('.istooltip').tooltip({container:'#tarea-see'});

                        mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>!', resp.msj, 'success');
                    }else{
                        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.msj, 'error');
                    }
                }).fail(fnAjaxFail).always(function() {
                    $btn.removeAttr('disabled');
                    $btn.find('i.fa').remove();
                    $btn.siblings('.btn').removeAttr('disabled');
                });
            },
        });
    }).on('click', '.panel-body.respuesta .devolucion .botones_accion>.cancelar', function(e) {
        e.preventDefault();
        $(this).closest('.respuesta').find('.devolucion').hide('fast');
        $(this).closest('.respuesta').children('.botones_accion').find('.btn').removeAttr('disabled');
    }).on('click', '.panel-body.respuesta .devolucion .botones_accion>.enviar', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var $rspta = $btn.closest('.respuesta');
        if($rspta.find('.txtDevolucion').val().trim()==''){
            $rspta.find('.txtDevolucion').closest('.form-group').addClass('has-error');
            $rspta.find('.txtDevolucion').focus();
            return false;
        }
        $.ajax({
            url: _sysUrlBase_+'/tarea/xDevolver',
            type: 'POST',
            dataType: 'json',
            data: {
                'idtarea_asignacion_alumno': $rspta.closest('.asignacion_alumno').data('idasignacionalumno'),
                'mensajedevolucion': $rspta.find('.txtDevolucion').val().trim()  
            },
            beforeSend: function() {
                $btn.prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
                $btn.siblings('.btn').attr('disabled', 'disabled');
            },
        }).done(function(resp) {
            if(resp.code=='ok'){
                $rspta.closest('.asignacion_alumno').find('.ver_respuesta.active').trigger('click');
                $rspta.closest('.asignacion_alumno').find('.ver_respuesta.active').removeClass('active');
                $rspta.closest('.asignacion_alumno').find('.nombre_fecha .presentacion>i').text(resp.msj);
                $rspta.remove();
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'info');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.msj, 'error');
            }
        }).fail(fnAjaxFail).always(function() {
            $btn.removeAttr('disabled');
            $btn.find('i.fa').remove();
            $btn.siblings('.btn').removeAttr('disabled');
        });
    }).on('change', '.panel-body.respuesta table .txtCalificacion', function(e) {
        var valor = parseFloat($(this).val());
        var min = parseFloat($(this).attr('min'));
        var max = parseFloat($(this).attr('max'));
        if(valor<min){ $(this).val(min); }
        if(valor>max){ $(this).val(max); }

        var $rspta = $(this).closest('.panel-body.respuesta');
        var suma=0.0;
        var cant=$rspta.find('.txtCalificacion').length;
        $rspta.find('.txtCalificacion').each(function(i, input) {
            suma+=parseFloat($(input).val());
        });
        var promedio=suma/cant;
        $rspta.find('.txtPromedio').val(promedio.toFixed(2));
    }).on('keyup', '.panel-body.respuesta .devolucion .txtDevolucion', function(e) {
        if(!$(this).val() || $(this).val().trim()==''){
            $(this).closest('.form-group').addClass('has-error');
        }else{
            $(this).closest('.form-group').removeClass('has-error');
        }
    });

    $('.tarea-item').on('click', '.panel-body.contenido .btn-live-edit', function(e) {
        e.preventDefault();
        var $btnEdit = $(this);
        var $contenedor = $btnEdit.parent();
        var now = Date.now();
        var tipo = $btnEdit.data('liveedit');
        var $spanValor = $contenedor.find('span');
        var valor=$spanValor.data('value') || $spanValor.text();
        var html = '<div id="live_editing_'+now+'" class="live_editing">';
        html+='<div class="col-xs-11" style="padding:0;">';
        if(tipo=='date'){
            html+='<div class="col-xs-12" style="padding:0;">'+
                        '<div class="input-group datetimepicker fechaentrega">'+
                            '<input type="text" name="dtpFecha" class="form-control dtpFecha" value="'+valor+'" required>'+
                            '<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-xs-12" style="padding:0;">'+
                        '<div class="input-group datetimepicker horaentrega">'+
                            '<input type="text" name="dtpHora" class="form-control dtpHora" value="'+valor+'" required>'+
                            '<span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>'+
                        '</div>'+
                    '</div>';
        }
        html+='</div>';
        html+='<div class="col-xs-1 text-right opciones" style="padding:0;"><br>';
        html+='<a href="#" class="color-green2 aplicar_edicion" title="<?php echo JrTexto::_('Apply'); ?>"><i class="fa fa-check"></i></a>';
        html+='<a href="#" class="color-red cancelar_edicion" title="<?php echo JrTexto::_('Cancel'); ?>"><i class="fa fa-times"></i></a>';
        html+='</div>';
        html+='</div>';

        $contenedor.attr('aria-liveedit', 'live_editing_'+now);
        $spanValor.hide();
        $btnEdit.hide();
        $contenedor.prepend(html);
        if(tipo=='date'){
            initFechaYHora( $('#live_editing_'+now+' .datetimepicker.fechaentrega') , $('#live_editing_'+now+' .datetimepicker.horaentrega') );
        }
    }).on('click', '.live_editing .opciones .aplicar_edicion', function(e) {
        e.preventDefault();
        var $live_edit= $(this).closest('.live_editing');
        var idEdition= $live_edit.attr('id');
        var $contenedor = $('.tarea-item .panel-body.contenido [aria-liveedit="'+idEdition+'"]');
        var arrTarea_Asignacion=[];
        var asign = {
            'idtarea_asignacion': $('#hIdTarea_Asignacion').val(),
            'idtarea': $('#hIdTarea').val(),
            'fechaentrega': $live_edit.find('.datetimepicker.fechaentrega input.dtpFecha').val(),
            'horaentrega': $live_edit.find('.datetimepicker.horaentrega input.dtpHora').val(),
        };
        arrTarea_Asignacion.push(asign);
        $.ajax({
            url: _sysUrlBase_+'/tarea_asignacion/xGuardar',
            type: 'POST',
            dataType: 'json',
            data: {'arrTarea_asignacion':JSON.stringify(arrTarea_Asignacion)},
            beforeSend: function(){
                $live_edit.find('.datetimepicker.fechaentrega input.dtpFecha').attr('disabled', 'disabled');
                $live_edit.find('.datetimepicker.horaentrega input.dtpHora').attr('disabled', 'disabled');
                $live_edit.find('.live_editing .opciones .cancelar_edicion').attr('disabled', 'disabled');
            },
        }).done(function(resp) {
            if(resp.code=='ok'){
                var idAsign = resp.data.idtarea_asignacion;
                var fecha = resp.data.arrFechaHora[idAsign].fechaentrega;
                var hora = resp.data.arrFechaHora[idAsign].horaentrega;
                $contenedor.find('span').text(fecha+' '+hora);
                mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Date updated') ?>','success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
            }
        }).fail(fnAjaxFail).always(function(){
            $live_edit.remove();
            $contenedor.find('span').show();
            $contenedor.find('.btn-live-edit').show();
            $contenedor.removeAttr('aria-liveedit');
        });
    }).on('click', '.live_editing .opciones .cancelar_edicion', function(e) {
        e.preventDefault();
        var $live_edit= $(this).closest('.live_editing');
        var idEdition= $live_edit.attr('id');
        var $contenedor = $('.tarea-item .panel-body.contenido [aria-liveedit="'+idEdition+'"]');
        $live_edit.remove();
        $contenedor.find('span').show();
        $contenedor.find('.btn-live-edit').show();
        $contenedor.removeAttr('aria-liveedit');
    });

    $('#tarea-see').on('click', '.popover .escoger_habilidades .habilidades .btn-skill', function(e) {
        $(this).toggleClass('active');
    }).on('click', '.popover .btns_accion .cerrar', function(e) {
        var idPopover= $(this).closest('.popover').attr('id');
        $('#tblRespuestaArchivos .btn.habilidades[aria-describedby="'+idPopover+'"]').trigger('click');
    }).on('click', '.popover .btns_accion .guardar', function(e) {
        e.preventDefault();
        var idPopover= $(this).closest('.popover').attr('id');
        var $boton = $('#tblRespuestaArchivos .btn.habilidades[aria-describedby="'+idPopover+'"]');
        var arrIdSkills = [];
        $(this).closest('.escoger_habilidades').find('.btn-skill.active').each(function(i, btn) {            
            var idSkill = $(this).attr('data-id-skill');
            arrIdSkills.push(idSkill);
        });
        $boton.attr('data-idhabilidades', JSON.stringify(arrIdSkills));
        activarBtnHabilidad($boton);
        $boton.trigger('click');
    });

    $('.panel.asignacion_alumno .panel-body.respuesta table .txtCalificacion').trigger('change');
});
</script>