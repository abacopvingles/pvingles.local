<?php 
$cursos=$grado=$seccion=array();
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-list">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li class="active"><?php echo ucfirst(JrTexto::_('activity')); ?></li>
        </ol>
    </div> </div>

    <div class="row" id="filtros-actividad"> 
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul" >
                    <select id="idcolegio" class="form-control select-ctrl ">
                        <?php if(!empty($this->miscolegios)){
                        foreach ($this->miscolegios  as $c) {
                            if(empty($cursos)) $cursos=$c["cursos"];
                            echo '<option value="'.$c["idlocal"].'">'.$c["iiee"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul">
                    <select name="idcurso" id="idcurso" class="form-control select-ctrl">
                        <?php 
                        if(!empty($cursos)){
                        foreach ($cursos  as $c){
                            if(empty($grados)) $grados=$c["grados"];                           
                            echo '<option value="'.$c["idcurso"].'">'.$c["strcurso"].'</option>';
                        }} ?>
                    </select>
                    
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul">
                    <select name="grados" id="idgrados" class="form-control select-ctrl">
                        <?php if(!empty($grados)){
                        foreach ($grados as $c){
                            if(empty($seccion)) $seccion=$c["secciones"];
                            echo '<option value="'.$c["idgrado"].'">'.$c["grado"].'</option>';
                        }} ?>
                    </select>
                    <?php //var_dump($seccion); ?>
                </div>
            </div>
        </div>
        
        
        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <div class="col-xs-12 select-ctrl-wrapper select-azul">
                    <select name="seccion" id="idseccion" class="form-control select-ctrl select-nivel">
                        <?php if(!empty($seccion)){
                        foreach ($seccion  as  $c) {
                            echo '<option value="'.$c["idgrupoauladetalle"].'">'.$c["seccion"].'</option>';
                        }} ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="listados">
        <div class="col-xs-12">
            <!--a href="<?php echo $this->documento->getUrlBase();?>/tarea/agregar" class="col-xs-12 col-sm-2 btn btn-success crear-tarea"> <?php echo JrTexto::_("Create")?></a-->
            <div class="col-xs-12 col-sm-10">
                <ul class="nav nav-tabs">
                    <li class="tabpendientes active">
                        <a href="#pnl-pendientes" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Pending")); 
                        if(!empty($this->tareasPend)){ echo ' <span class="badge">'.count($this->tareasPend).'</span>'; }?></a>
                    </li>
                    <li class="tabfinalizados">
                        <a href="#pnl-finalizados" data-toggle="tab"><?php echo ucfirst(JrTexto::_("Expired"))?></a>
                    </li>
                    <li class="tabtodos">
                        <a href="#pnl-todos" data-toggle="tab"><?php echo ucfirst(JrTexto::_("My Activity"))?></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 tab-content" style="padding-top: 15px;">
            <div id="pnl-pendientes" class="row tab-pane fade active in">
                <?php if(!empty($this->tareasPend)) {
                foreach ($this->tareasPend as $t) { ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                        <div class="panel-heading titulo">
                            <h3><?php echo $t['nombre']; ?></h3>
                            <!--<div class="opciones hidden">
                                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id='.$t['idtarea'];?>" class="color-blue editar"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-idtarea="<?php echo $t['idtarea'];?>" class="color-red eliminar"><i class="fa fa-times"></i></a>
                            </div>-->
                        </div>
                        <div class="panel-body contenido">
                            <div class="col-xs-4 portada">
                                <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                            </div>
                            <div class="col-xs-8 descripcion">
                                <?php echo $t['descripcion']; ?>
                            </div>
                            <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                <div class="col-xs-8 fecha-entrega">
                                    <div class="dato"><?php echo $t['fechaentrega'] .' '. date('h:i a', strtotime($t['horaentrega'])); ?></div>
                                    <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                </div>
                                <div class="col-xs-4 presentados border-left">
                                    <div class="dato"><?php echo $t['cant_presentados']; ?></div>
                                    <div class="info"><?php echo JrTexto::_("Submitted")?></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id='.$t['idtarea_asignacion'];?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                        </div>
                    </div>
                </div>
                <?php } }else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("no activity found").'</h4></div>'; } ?>
            </div>
            <div id="pnl-finalizados" class="row tab-pane fade">
                <?php if(!empty($this->tareasFin)) {
                foreach ($this->tareasFin as $t) { ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                        <div class="panel-heading titulo">
                            <h3><?php echo $t['nombre']; ?></h3>
                            <!--<div class="opciones hidden">
                                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id='.$t['idtarea'];?>" class="color-blue editar"><i class="fa fa-pencil"></i></a>
                                <a href="#" data-idtarea="<?php echo $t['idtarea'];?>" class="color-red eliminar"><i class="fa fa-times"></i></a>
                            </div>-->
                        </div>
                        <div class="panel-body contenido">
                            <div class="col-xs-4 portada">
                                <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                            </div>
                            <div class="col-xs-8 descripcion">
                                <?php echo $t['descripcion']; ?>
                            </div>
                            <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                <div class="col-xs-8 fecha-entrega">
                                    <div class="dato"><?php echo $t['fechaentrega'] .' '. date('h:i a', strtotime($t['horaentrega'])); ?></div>
                                    <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                </div>
                                <div class="col-xs-4 presentados border-left">
                                    <div class="dato"><?php echo $t['cant_presentados']; ?></div>
                                    <div class="info"><?php echo JrTexto::_("Submitted")?></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id='.$t['idtarea_asignacion'];?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
                        </div>
                    </div>
                </div>
                <?php } }else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("no activity found").'</h4></div>'; } ?>
            </div>
            <div id="pnl-todos" class="row tab-pane fade">
                <?php if(!empty($this->todoTareas)) {
                foreach ($this->todoTareas as $t) { ?>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $t['idtarea']; ?>">
                        <div class="panel-heading titulo">
                            <h3><?php echo $t['nombre']; ?></h3>
                            <div class="opciones">
                                <a href="#" data-idtarea="<?php echo $t['idtarea'];?>" class="color-red eliminar"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="panel-body contenido">
                            <div class="col-xs-4 portada">
                                <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                                if(!empty($t['foto'])){ $src = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $t['foto']); }  ?>
                                <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                            </div>
                            <div class="col-xs-8 descripcion">
                                <?php echo $t['descripcion']; ?>
                            </div>
                            <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                                <div class="col-xs-8 fecha-entrega">
                                    <div class="dato"><?php //echo $t['fechaentrega'] .' '. date('h:i a', strtotime($t['horaentrega'])); ?></div>
                                    <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                                </div>
                                <div class="col-xs-4 asignados border-left">
                                    <div class="dato"><?php echo $t['cant_asignaciones']; ?></div>
                                    <div class="info"><?php echo JrTexto::_("Students assigned")?></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id='.$t['idtarea'];?>" class="btn btn-blue editartarea"><?php echo ucfirst(JrTexto::_("Edit"))?></a>
                        </div>
                    </div>
                </div>
                <?php } }else{ echo '<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4>'.JrTexto::_("No activity found").'</h4></div>'; } ?>
            </div>
        </div>
    </div>
</div>

<section class="hidden">
    <div class="col-xs-12 col-sm-6" id="clone-tarea_item">
        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="">
            <div class="panel-heading titulo">
                <h3>title</h3>
                <!--<div class="opciones hidden">
                    <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id=';?>" class="color-blue editar"><i class="fa fa-pencil"></i></a>
                    <a href="#" data-idtarea="" class="color-red eliminar"><i class="fa fa-times"></i></a>
                </div>-->
            </div>
            <div class="panel-body contenido">
                <div class="col-xs-4 portada">
                    <img src="<?php echo $this->documento->getUrlStatic();?>/media/web/nofoto.jpg" alt="img" class="img-responsive">
                </div>
                <div class="col-xs-8 descripcion">
                    descripcion
                </div>
                <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                    <div class="col-xs-8 fecha-entrega">
                        <div class="dato">dd-mm-yyyy hh:ii aa</div>
                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                    </div>
                    <div class="col-xs-4 presentados border-left">
                        <div class="dato">00</div>
                        <div class="info"><?php echo JrTexto::_("Submitted")?></div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/tarea/ver/?id=';?>" class="btn btn-blue vertarea"><?php echo ucfirst(JrTexto::_("View"))?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6" id="clone-tarea_item_todo">
        <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="">
            <div class="panel-heading titulo">
                <h3>title</h3>
                <div class="opciones">
                    <a href="#" data-idtarea="" class="color-red eliminar"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="panel-body contenido">
                <div class="col-xs-4 portada">
                    <img src="<?php echo $this->documento->getUrlStatic();?>/media/web/nofoto.jpg" alt="img" class="img-responsive">
                </div>
                <div class="col-xs-8 descripcion">
                    descripcion
                </div>
                <div class="col-xs-12 col-lg-8 text-center fechahora_presentados">
                    <div class="col-xs-8 fecha-entrega">
                        <div class="dato">dd-mm-yyyy hh:ii aa</div>
                        <div class="info"><?php echo JrTexto::_("Expiration Date")?></div>
                    </div>
                    <div class="col-xs-4 asignados border-left">
                        <div class="dato">00</div>
                        <div class="info"><?php echo JrTexto::_("Students assigned")?></div>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/tarea/editar/?id=';?>" class="btn btn-blue editartarea"><?php echo ucfirst(JrTexto::_("Edit"))?></a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>
</section>

<script>
var $contenedorFiltros = $('#filtros-actividad');
var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            var options = '';
            if(resp.data.length) {
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E") {
                        options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.addClass('select-cursodetalle');
                $select.append(options);
                $select.closest('.form-group').parent().removeClass('hidden');
            }else{
                $select.attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().prev().find('select.sel-cursodet').attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.form-group').parent().remove();
        }
    }).fail(function(err) {
        $select.closest('.form-group').parent().remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var borrarSobrantes = function($select) {
    var $listSelect = $contenedorFiltros.find('select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).length>=1){
        $contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).closest('.form-group').parent().remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};

var nuevoSelectFiltro = function( idPadre ) {
    var $divSelect_new = $('#clonar-select-nivel').clone();
    var idSelect = $divSelect_new.find('select.select-nivel').attr('id');
    var nuevoIdSelect = idSelect+'_'+idPadre;
    $divSelect_new.removeAttr('id');
    $divSelect_new.find('select.select-nivel').attr({
        'id': nuevoIdSelect,
        'name': nuevoIdSelect,
        'required' : 'required'
    });
    $contenedorFiltros.append($divSelect_new);
    return $contenedorFiltros.find('#'+nuevoIdSelect);
};

var fnAjaxFail = function(xhr, textStatus, errorThrown) {
    /*console.log("Error");
    console.log(xhr);
    console.log(textStatus);*/
    throw errorThrown;
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
};

var nuevoItemTarea = function(tarea, $contenedor) {
    var $itemTarea = $('#clone-tarea_item').clone();
    $itemTarea.removeAttr('id');
    $itemTarea.find('.panel-heading.titulo>h3').html(tarea.nombre);
    $itemTarea.find('.tarea-item').attr('data-idtarea',tarea.idtarea);
    var img_src=(tarea.foto!='')?tarea.foto:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemTarea.find('.panel-body.contenido>.portada>img').attr('src', img_src.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
    $itemTarea.find('.panel-body.contenido>.descripcion').html(tarea.descripcion);
    $itemTarea.find('.panel-body.contenido .fecha-entrega>.dato').html(tarea.fechaentrega+' '+tarea.horaentrega);
    $itemTarea.find('.panel-body.contenido .presentados>.dato').html(tarea.cant_presentados);
    $itemTarea.find('.panel-footer a.vertarea').attr('href', _sysUrlBase_+'/tarea/ver/?id='+tarea.idtarea_asignacion);

    $contenedor.append($itemTarea);
};

var nuevoItemTodoTarea = function(tarea) {
    var $itemTarea = $('#clone-tarea_item_todo').clone();
    $itemTarea.removeAttr('id');
    $itemTarea.find('.panel-heading.titulo>h3').html(tarea.nombre);
    $itemTarea.find('.tarea-item').attr('data-idtarea',tarea.idtarea);
    $itemTarea.find('.panel-heading.titulo>.opciones>.editar').attr('href', _sysUrlBase_+'/tarea/editar/?id='+tarea.idtarea);
    $itemTarea.find('.panel-heading.titulo>.opciones>.eliminar').attr('data-idtarea', tarea.idtarea);
    var img_src=(tarea.foto!='')?tarea.foto:_sysUrlStatic_+'/media/web/nofoto.jpg';
    $itemTarea.find('.panel-body.contenido>.portada>img').attr('src', img_src.replace(/__xRUTABASEx__/gi,_sysUrlBase_));
    $itemTarea.find('.panel-body.contenido>.descripcion').html(tarea.descripcion);
    $itemTarea.find('.panel-body.contenido .fecha-entrega>.dato').html(' ');
    $itemTarea.find('.panel-body.contenido .asignados>.dato').html(tarea.cant_asignaciones);
    $itemTarea.find('.panel-footer a.editartarea').attr('href', _sysUrlBase_+'/tarea/editar/?id='+tarea.idtarea);

    $('#pnl-todos').append($itemTarea);
};

var currentRequest = null;
var cargarTareas = function(filtros=[]){
    currentRequest = $.ajax({
        url: _sysUrlBase_+'/tarea/listarTareasPendientesYFinalizadas/',
        type: 'POST',
        dataType: 'json',
        data: filtros,
        beforeSend: function(){
            if(currentRequest != null) { currentRequest.abort(); }
            $('.tab-content .tab-pane').each(function(index, el) {
                $(el).html('<div class="cargando" style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
            });
        },
    }).done(function(resp) {
        var pendientes = resp.data.pendientes;
        var finalizadas = resp.data.finalizadas;
        var todas = resp.data.todo;
        var msjeVacio='<div class="text-center"><h1><i class="fa fa-minus-circle fa-3x"></i></h1><h4><?php echo JrTexto::_('No activity found'); ?></h4></div>';
        if(pendientes.length>0){
            $.each(pendientes, function(i, tarea) {
                nuevoItemTarea(tarea, $('#pnl-pendientes'));
            });
        }else{
            $('#pnl-pendientes').append(msjeVacio);
        }
        if(finalizadas.length>0){
            $.each(finalizadas, function(i, tarea) {
                nuevoItemTarea(tarea, $('#pnl-finalizados'));
            });
        }else{
            $('#pnl-finalizados').append(msjeVacio);
        }
        if(todas.length>0){
            $.each(todas, function(i, tarea) {
                nuevoItemTodoTarea(tarea);
            });
        }else{
            $('#pnl-todos').append(msjeVacio);
        }
        currentRequest=null;
    }).fail(fnAjaxFail).always(function() {
        $('.tab-content .tab-pane .cargando').remove();
    });
};

var leerniveles=function(data){
    try{
        var res = xajax__('', 'niveles', 'getxPadre', data);
        if(res){ return res; }
        return false;
    }catch(error){
        return false;
    }       
};

var addniveles=function(data,obj){
    var objini=obj.find('option:first').clone();
    obj.find('option').remove();
    obj.append(objini);
    if(data!==false){
        var html='';
        $.each(data,function(i,v){
            html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
        });
        obj.append(html);
    }
    id=obj.attr('id');
    if(id==='activity-item') {
        var filtros = {
            'idnivel': $('#level-item').val(),
            'idunidad': $('#unit-item').val(),
            'idactividad': $('#activity-item').val(),
        };
        cargarTareas(filtros);
    }
};

var orderjson=function(obj){
    
    console.log(obj);

    /*for (var i = 0; i < options.length - 1; i ++){
        if (options[i].text <= options[i + 1].text)	{
            temptext = options[i].text;
            tempvalue = options[i].value;
            options[i].text = options[i + 1].text;
            options[i].value = options[i + 1].value;
            options[i + 1].text = temptext;
            options[i + 1].value = tempvalue;
        }
    }*/
    return obj;
}

var datoscurso=<?php echo json_encode($this->miscolegios); ?>;
var actualizarcbo=function(cbo,predatos){
    if(cbo.attr('id')=='idcolegio'){
        $('#idcurso').html('');
        $('#idgrados').html('');
        $('#idseccion').html('');
        $.each(predatos,function(e,v){
            $('#idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
        $('#idcurso').trigger('change');
    }else if(cbo.attr('id')=='idcurso'){
        $('#idgrados').html('');
        $('#idseccion').html('');                 
        $.each(predatos,function(e,v){           
            $('#idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
        $('#idgrados').trigger('change');
    }else if(cbo.attr('id')=='idgrados'){
        $('#idseccion').html('');                
        $.each(predatos,function(e,v){           
            $('#idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
        $('#idseccion').trigger('change');
    }else if(cbo.attr('id')=='idseccion'){
        var idgrupoauladetalle=$('#idseccion').val()||'';
        console.log(idgrupoauladetalle);
    }
}
var cursosSel=[];
$(document).ready(function() {
    $('#filtros-actividad').on('change','#idcolegio',function(ev){
        var idcolegio=$(this).val()||'';
        for (var i = 0; i < datoscurso.length; i++) {
           if(datoscurso[i].idlocal==idcolegio){              
                datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
               actualizarcbo($(this),datoscurso[i].cursos);
           }
        }
        
    })
    $('#filtros-actividad').on('change','#idcurso',function(ev){
        var idcurso=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;                
                actualizarcbo($(this),grados);
            }
    })
    $('#filtros-actividad').on('change','#idgrados',function(ev){
        var idgrados=$(this).val()||'';
        var idcolegio=$('#idcolegio').val()||'';
        var idcurso=$('#idcurso').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;
                for (var h = 0; h < grados.length; h++){
                    if(grados[h].idgrado==idgrados){
                        var secciones=grados[h].secciones;
                        secciones.sort(function(a, b){return a.seccion - b.seccion;});
                        actualizarcbo($(this),secciones);
                    }
                }
                
            }
    })
   
   /* $('#filtros-actividad').on('change','#idseccion',function(ev){
        var idcurso=$(this).val()||'';
        var grados=[];
        for (var i = 0; i < grados.length; i++) {
           if(grados[i].idlocal==idcolegio){
            grados=grados[i].cursos;
           }
        }       
        actualizarcbo($(this),grados);
    })*/

    $('#filtros-actividad').on('change', '.select-nivel', function(e) {
        var idCurso = $('#opcIdCurso').val() || 0;
        if( $(this).attr('id') == "opcIdCurso" ) { $('#hIdCurso').val( idCurso ); }

        var idPadre = 0;
        if( $(this).attr('id') !== "opcIdCurso" ) {
            idPadre = $(this).val();
        }
        borrarSobrantes($(this));
        if(idCurso==0){ return false; }

        $select_new = nuevoSelectFiltro(idPadre);
        getCursoDetalle({
            'idcurso': idCurso,
            'idpadre': idPadre,
        }, $select_new);

        var idCursoDetalle = null;
        if(idPadre>0) idCursoDetalle = idPadre;
        cargarTareas({'idcurso': idCurso, 'idcursodetalle': idCursoDetalle});
    });
    $('#level-item').change(function(){
        var idnivel=$(this).val();
        var data={tipo:'U','idpadre':idnivel}
        var donde=$('#unit-item');
        if(idnivel!=='') addniveles(leerniveles(data),donde);
        else addniveles(false,donde);
        donde.trigger('change');
    });
    $('#unit-item').change(function(){
        var idunidad=$(this).val();
        var data={tipo:'L','idpadre':idunidad}
        var donde=$('#activity-item');
        if(idunidad!=='') addniveles(leerniveles(data),donde);
        else addniveles(false,donde);
    });
    $('#activity-item').change(function(){
        var filtros = {
            'idnivel': $('#level-item').val(),
            'idunidad': $('#unit-item').val(),
            'idactividad': $('#activity-item').val(),
        };
        cargarTareas(filtros);
    });

    $('#tarea-list').on('click', '.panel.tarea-item .opciones .eliminar', function(e) {
        e.preventDefault();
        var idtarea = $(this).attr('data-idtarea');
        $.confirm({
            title: '<?php echo JrTexto::_('Delete');?>',
            content: '<?php echo JrTexto::_('Are you sure to delete this record?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                $.ajax({
                    url: _sysUrlBase_+'/tarea/xEliminar_logica',
                    type: 'POST',
                    dataType: 'json',
                    data: {'idtarea': idtarea},
                }).done(function(resp) {
                    if(resp.code=='ok'){
                        $('#tarea-list').find('.panel.tarea-item[data-idtarea="'+idtarea+'"]').each(function() { $(this).closest('[class^="col-"]').remove(); });
                    }else{
                        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', resp.msj, 'error');
                    }
                }).fail(fnAjaxFail);
            },
        });
    });

    /***** escribir Tab en la URL *****/
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })
    /**** FIN escribir Tab en la URL ****/
});
</script>