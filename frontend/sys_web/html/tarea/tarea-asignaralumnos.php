<?php 
$RUTA_BASE = $this->documento->getUrlBase();
$arrSkills = $frm = array();
if(!empty($this->datos)) $frm=$this->datos;
if(!empty(@$frm["habilidades"])) $arrSkills=json_decode(@$frm["habilidades"], true);
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<div class="" id="tarea-add">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $this->documento->getUrlBase();?>/tarea"><?php echo ucfirst(JrTexto::_('activity')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>
    
    <form class="form-horizontal" id="frmAgregarTarea" name="frmAgregarTarea">
        <input type="hidden" name="accion" id="accion" value="<?php echo $this->frmaccion; ?>">
        <input type="hidden" name="pkIdtarea" id="pkIdtarea" value="<?php echo @$frm['idtarea']; ?>">
        <div class="panel pnl-contenedor <?php if($this->frmaccion=='Nuevo'){echo 'panel-default'; } ?>">
            <?php if($this->frmaccion=='Editar'){ ?>
            <div class="panel-heading bg-blue" style="">
                <h3 class="panel-title" style="font-size: 1.8em;">
                    <i class="fa fa-briefcase"></i> &nbsp;<?php echo ucfirst(@$frm['nombre']); ?>
                </h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <?php } ?>
            <div class="panel-body">                
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="txtNombre" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Title')); ?> <span>*</span></label>
                            <div class="col-xs-12 col-sm-9">
                                <input type="text" id="txtNombre" readonly="true" name="txtNombre" class="form-control"  autocomplete="off" required value="<?php echo @$frm['nombre'];?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Objectives of activity')); ?></label>
                            <div class="col-xs-12 col-sm-9">
                                <textarea type="text" id="txtDescripcion" readonly="true" name="txtDescripcion" class="form-control" rows="5"><?php echo @$frm['descripcion'];?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtFoto" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Cover')); ?></label>
                            <div class="col-xs-12 col-sm-5">
                                <a href="#" class="thumbnail btnportada istooltip" data-tipo="image" data-url=".img-portada" title="<?php echo ucfirst(JrTexto::_('Select an image')); ?>">
                                    <?php
                                     $imgSrc=($this->frmaccion=='Nuevo')?$this->documento->getUrlStatic().'/media/web/nofoto.jpg':str_replace('__xRUTABASEx__', $RUTA_BASE, @$frm['foto']); 
                                    ?>
                                    <img src="<?php echo $imgSrc; ?>" alt="cover" class="img-responsive img-portada">
                                    <input type="hidden" id="txtFoto" name="txtFoto" value="<?php echo $imgSrc; ?>">
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="txtHabilidades" class="col-xs--12 col-sm-3 control-label"><?php echo JrTexto::_("Skills"); ?><span>*</span></label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="contenedor-lista-habilidades" style="max-height:none;">
                                    <table id="lista-habilidades">
                                        <thead>
                                            <tr> <th width="90%"></th> <th width="10%"></th> </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($this->habilidades)) {
                                            foreach ($this->habilidades as $h) {
                                                if($h["idmetodologia"]<8){
                                                $isActive = (in_array($h["idmetodologia"], $arrSkills));
                                            ?>
                                            <tr>
                                                <td><a href="#" class="list-group-item <?php echo ($isActive)?'active':''; ?>" data-idhabilidad="<?php echo $h["idmetodologia"] ?>"><?php echo $h["nombre"] ?></a></td>
                                                <td><input type="radio" name="opcHabilidad_destacada" class="radio-ctrl <?php echo (!$isActive)?'hidden':''; ?>" value="<?php echo $h["idmetodologia"] ?>" title="<?php echo ucfirst(JrTexto::_("Outstanding skill")); ?>" <?php echo (@$frm["habilidad_destacada"]==$h["idmetodologia"])?'checked="checked"':'' ?> ></td>
                                            </tr>
                                            <?php } } }?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="opcPuntajemaximo" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Max. score")?> </label>
                            <div class="col-xs-12 col-sm-3 "><h2 style="margin:0px"><?php echo intval($frm["puntajemaximo"]);?>%</h2></div>
                            <label for="opcPuntajeminimo" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_("Min. score to pass")?> </label>
                            <div class="col-xs-12 col-sm-3 "><h2 style="margin:0px"><?php echo intval($frm["puntajeminimo"]); ?>%</h2></div>
                        </div>
                        <div class="form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Attachment')); ?></label>
                            <!--div class="col-xs-12 col-sm-9">
                                <input type="file" name="inp_CargadorArchivos" id="inp_CargadorArchivos" class="hidden">
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="documento" title="<?php echo JrTexto::_('Document'); ?>"><i class="fa fa-paperclip"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="video" title="<?php echo JrTexto::_('Video'); ?>"><i class="fa fa-video-camera"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="audio" title="<?php echo JrTexto::_('Audio'); ?>"><i class="fa fa-headphones"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="grabacionvoz" title="<?php echo JrTexto::_('Voice recording'); ?>"><i class="fa fa-microphone"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="enlace" title="<?php echo JrTexto::_('Link'); ?>"><i class="fa fa-link"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="actividad" title="<?php echo JrTexto::_('Activity'); ?>"><i class="fa fa-font"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="juego" title="<?php echo JrTexto::_('Game'); ?>"><i class="fa fa-puzzle-piece"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="examen" title="<?php echo JrTexto::_('Exam'); ?>"><i class="fa fa-list"></i></button>
                            </div-->
                            <div class="col-xs-12 col-sm-9" style="height: 20px;">
                                <div id="barra-progreso" style="display: none;">
                                    <div class="progress" style="margin: 0;">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only">0%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-offset-3 col-xs-12 col-sm-9 lista-archivos">
                                <input type="hidden" name="txtIdTarea_archivos" id="txtIdTarea_archivos" value="">
                                <table class="table table-striped" id="tblAdjuntos">
                                    <tbody>
                                    <?php if($this->frmaccion=='Editar'){ 
                                    $arrAttach=@$frm['tarea_archivos'];
                                    $arrIcon=array('D'=> 'fa fa-paperclip', 'V'=> 'fa fa-video-camera', 'L'=> 'fa fa-link', 'G'=> 'fa fa-microphone', 'A'=> 'fa fa-font', 'J'=> 'fa fa-puzzle-piece', 'E'=> 'fa fa-list', 'U'=> 'fa fa-headphones' );
                                    if(!empty($arrAttach)){
                                        foreach ($arrAttach as $adj) { ?>
                                        <tr data-id="<?php echo $adj['idtarea_archivos'] ?>">
                                            <td style="width: 7%"><i class="<?php echo $arrIcon[$adj['tipo']] ?>"></i></td>
                                            <td><?php echo $adj['nombre'] ?></td>
                                            <td style="width: 18%">
                                                <a href="<?php $ruta=$adj['ruta'];
                                                if($adj['tipo']=='A' || $adj['tipo']=='J' || $adj['tipo']=='E'){
                                                    $ruta.=$adj['idtarea_archivos'];
                                                }  
                                                echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta); ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                <!--button title="<?php //echo ucfirst(JrTexto::_('Delete')); ?>" class="btn btn-xs btn-default color-red eliminararchivo"><i class="fa fa-trash"></i></button-->
                                            </td>
                                        </tr>
                                    <?php } } } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    <?php if($this->frmaccion=='Editar') {
   // $arrTarea_Asignaciones=@$frm['tarea_asignacion'];
    if(!empty($this->tareasAsignadas)){
    foreach ($this->tareasAsignadas as $asign) { 
        $cursos=array();
        ?>
    <form class="form-horizontal frmAsignacion" id="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" name="frmAsignacion_<?php echo $asign['idtarea_asignacion']; ?>" data-id="<?php echo $asign['idtarea_asignacion']; ?>">
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-success">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <small class="sr-only">
                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    <?php echo date('d-m-Y', strtotime($asign['fechaentrega'])).' '.date('h:i a', strtotime($asign['horaentrega'])); ?>
                </small>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl idcolegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php 
                                    if(!empty($this->miscolegios))
                                    foreach ($this->miscolegios as $l){ 
                                        $sel=$l["idlocal"]==$asign["idlocal"]?true:false;
                                        if($sel==true){ $cursos=$l["cursos"];}
                                    ?>
                                    <option value="<?php echo $l['idlocal']?>" <?php echo $sel==true?'selected="selected"':''; ?>><?php echo $l['iiee']; ?></option>
                                    <?php } ?>
                                </select>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl idcurso">
                                <?php 
                                    if(!empty($cursos)){
                                    foreach ($cursos  as $c){
                                        $sel=$c["idcurso"]==$asign["idcurso"]?true:false;
                                        if($sel==true){ $grados=$c["grados"];}?>                                       
                                        <option value="<?php echo @$c["idcurso"];?>"  <?php echo $sel==true?'selected="selected"':''; ?> ><?php echo @$c["strcurso"]; ?></option>
                                   <?php }} ?>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                <select class="form-control select-ctrl idgrados">
                                <?php if(!empty($grados)){
                                    foreach ($grados as $c){
                                        $sel=$c["idgrado"]==$asign["idgrado"]?true:false;
                                        if($sel==true){ $seccion=$c["secciones"];}?>                                       
                                        <option value="<?php echo @$c["idgrado"];?>"  <?php echo $sel==true?'selected="selected"':''; ?> ><?php echo @$c["grado"]; ?></option>
                                   <?php }} ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                <select class="form-control select-ctrl idseccion">
                                <?php if(!empty($seccion)){
                                    foreach ($seccion  as  $c) {
                                         $sel=$c["idgrupoauladetalle"]==$asign["idseccion"]?true:false;
                                        ?>
                                        <option value="<?php echo @$c["idgrupoauladetalle"];?>"  <?php echo $sel==true?'selected="selected"':''; ?> ><?php echo @$c["seccion"]; ?></option>
                                   <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('date / time of activity delivery')); ?></label>
                            <div class="col-xs-12 col-sm-3">
                                <div class="input-group datetimepicker fechaentrega">
                                    
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">    
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" value="<?php echo $asign['fechaentrega'].' '.$asign['horaentrega']; ?>" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <a class="btn btn-lg btn-blue pull-right asignargrupoaula"><i class="fa fa-save"></i> </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php }} ?>
    <div class="row" id="botones-accion"> <div class="col-xs-12 text-center" style="margin-bottom: 20px;">
        <a href="<?php echo $this->documento->getUrlBase();?>/tarea#pnl-todos" class="btn btn-lg btn-default pull-left"><i class="fa fa-arrow-left"></i> <?php echo JrTexto::_("Back to list")?></a>
        <button class="btn btn-lg btn-green agregar_asignacion"><i class="fa fa-users"></i> <?php echo JrTexto::_("Assign to students")?></button>
        <!--button class="btn btn-lg btn-blue pull-right guardar_edicion"><i class="fa fa-save"></i> <?php //echo JrTexto::_("Save")?></button-->
    </div> </div>
    <?php } ?>
</div>

<section class="hidden">
    <!-- select-box nivel del curso -->
    <div class="col-xs-12 col-sm-3 hidden" id="clonar-select-nivel"><div class="form-group">
        <div class="col-xs-12 select-ctrl-wrapper select-azul">
            <select name="opcIdCursoDet" id="opcIdCursoDet" class="form-control select-ctrl select-nivel sel-cursodet">
                <option value="-1">- <?php echo ucfirst(JrTexto::_("Select")); ?> <span>*</span> -</option>
            </select>
        </div>
    </div></div>

 



    <?php if($this->frmaccion=='Editar'){ ?>
    <!-- nuevo FORM para asignar la atrea a alumno(s) -->
    <form class="form-horizontal frmAsignacion" id="frmAsignacion" name="" data-id="">
        <?php if($this->frmaccion=='Editar'){ ?>
        <input type="hidden" name="txtTablapadre" id="txtTablapadre" value="T">
        <input type="hidden" name="txtIdpadre" id="txtIdpadre" value="<?php echo @$frm['idtarea'];?>">
        <?php } ?>
        <div class="panel pnl-contenedor">
            <div class="panel-heading bg-green">
                <h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo ucfirst(JrTexto::_('Assignment')); ?></h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12"><a href="#" class="color-red pull-right eliminar_asignacion"><i class="fa fa-times fa-2x"></i></a></div>

                    <div class="col-xs-12 col-sm-5 filtros-alumnos">
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl idcolegio">
                                    <option value="-1">- <?php echo JrTexto::_('Select Educational Institution'); ?> -</option>
                                    <?php 
                                    if(!empty($this->miscolegios))
                                    foreach ($this->miscolegios as $l) { ?>
                                    <option value="<?php echo $l['idlocal']?>"><?php echo $l['iiee']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12  select-ctrl-wrapper select-azul">
                                <select class="form-control select-ctrl idcurso">
                                    <option value="-1">- <?php echo JrTexto::_('Select Classroom'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                <select class="form-control select-ctrl idgrados">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 select-ctrl-wrapper select-azul ">
                                <select class="form-control select-ctrl idseccion">
                                    <option value="-1">- <?php echo JrTexto::_('Select Group'); ?> -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 border-left listado-alumnos" style="display: none;">
                        <label class="col-xs-12" style="padding: 0;"><input type="checkbox" class="checkbox-ctrl check-all" checked> <?php echo ucfirst(JrTexto::_('Select all')); ?></label>
                        <ul class="col-xs-12" style="list-style: none;"></ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <hr> 
                       <div class="form-group">
                            <label for="dtpFecha" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('date / time of activity delivery')); ?></label>
                            <div class="col-xs-12 col-sm-3">
                                <div class="input-group datetimepicker fechaentrega">
                                    <input type="text" name="dtpFecha" class="form-control dtpFecha" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <div class="input-group datetimepicker horaentrega">
                                    <input type="text" name="dtpHora" class="form-control dtpHora" required>
                                    <span class="input-group-addon btn"><span class="glyphicon glyphicon-time"></span></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <a class="btn btn-lg btn-blue pull-right asignargrupoaula"><i class="fa fa-save"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php } ?>
</section>

<script>
$('.istooltip').tooltip();
var _sysUrlSmartquiz_ = '<?php echo URL_SMARTQUIZ; ?>';
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;
var fnAjaxFail = function(xhr, textStatus, errorThrown){
    mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    throw errorThrown;
};

var $contenedorFiltros = $('#filtros-actividad');
var getCursoDetalle = function( dataSend , $select ) {
    $.ajax({
        async: false,
        url: _sysUrlBase_+'/acad_cursodetalle/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            var options = '';
            if(resp.data.length) {
                $.each(resp.data, function(i, det) {
                    if(det.tiporecurso!="E") {
                        options += '<option value="'+det.idcursodetalle+'">'+det.nombre+'</option>';
                    }
                });
                $select.addClass('select-cursodetalle');
                $select.append(options);
                $select.closest('.form-group').parent().removeClass('hidden');
            }else{
                $select.attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().prev().find('select.sel-cursodet').attr('name', 'opcIdcursodetalle');
                $select.closest('.form-group').parent().remove();
            }
        } else {
            mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
            $select.closest('.form-group').parent().remove();
        }
    }).fail(function(err) {
        $select.closest('.form-group').parent().remove();
        mostrar_notificacion('<?php echo JrTexto::_("Error") ?>',resp.msj,'error');
    }).always(function() {  });
};

var arrIndicadores = [];
var getIndicadores = function ( dataSend ) {
    $.ajax({
        url: _sysUrlBase_+'/acad_cursohabilidad/buscarjson',
        type: 'GET',
        dataType: 'json',
        data: dataSend,
    }).done(function(resp) {
        if(resp.code=='ok') {
            arrIndicadores = resp.data;
            cargarIndicadores();
        } else {
        }
    }).fail(function(err, txtErr) {
        console.log("error : ", err);
        mostrar_notificacion('Error',  txtErr, 'error');
    }).always(function() {});
};

var arrHabilidades = JSON.parse('<?php echo !empty(@$frm["habilidades"])?@$frm["habilidades"]:'[]'; ?>');
var habilidadDestacada = '<?php echo @$frm["habilidad_destacada"]; ?>';
var cargarIndicadores = function(){
    if(arrIndicadores.length==0) return false;
    $tblIndicadores = $('#lista-habilidades');
    var strSkills = '';
    $.each(arrIndicadores, function(i, indic) {
        let isActive = $.inArray(indic.idcursohabilidad, arrHabilidades) >= 0;
        strSkills += '<tr>';
        strSkills += '<td><a href="#" class="list-group-item '+(isActive?'active':'')+'" data-idhabilidad="'+indic.idcursohabilidad+'">'+indic.texto+'</a></td>';
        strSkills += '<td><input type="radio" name="opcHabilidad_destacada" class="radio-ctrl '+(!isActive?'hidden':'')+'" value="'+indic.idcursohabilidad+'" title="<?php echo ucfirst(JrTexto::_("Outstanding skill")); ?>" '+(habilidadDestacada==indic.idcursohabilidad?'checked="checked"':'')+' ></td>';
        strSkills += '</tr>';
    });
    $tblIndicadores.html(strSkills);
};

var borrarSobrantes = function($select) {
    var $listSelect = $contenedorFiltros.find('select.select-nivel');
    var indexOfSelect = $listSelect.index($select);
    var cantSelect = $listSelect.length;
    x=0;
    while($contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).length>=1){
        $contenedorFiltros.find('select.select-nivel').eq(indexOfSelect+1).closest('.form-group').parent().remove();
        if(x>=100){ console.log('algo salio mal'); break; }
        x++;
    }
};


var crearModal = function(param){
    if(param.deDonde==undefined || param.deDonde==''){ return false; }
    if(param.nombreContenedor==undefined || param.nombreContenedor=='') {
        param.nombreContenedor=param.deDonde;
    }
    var $modal = $('#modalclone').clone();
    $modal.attr('id','mdl-'+param.nombreContenedor);
    if(param.small){$modal.find('.modal-dialog').removeClass('modal-lg');}
    else{$modal.find('.modal-dialog').addClass('modal-lg');}
    $modal.find('.modal-header #modaltitle').html('<?php echo JrTexto::_('Attach'); ?> '+param.titulo);
    $modal.find('#modalfooter .btn.cerrarmodal').addClass('pull-left');
    $modal.find('#modalfooter').append('<button class="btn btn-success guardar_adjunto"><?php echo JrTexto::_('Save'); ?></button>');
    $('body').append($modal);
    $('#mdl-'+param.nombreContenedor).modal({keyboard:false, backdrop:'static'});
    $modal.find('#modalcontent').html($('#'+param.deDonde).html());
    $modal.find('#modalcontent').find('form').attr({'id':'frm-'+param.nombreContenedor,'name':'frm-'+param.nombreContenedor});
};

var listarResultados = function(arrElems, $divResults){
    arrElems = arrElems||{};
    if($.isEmptyObject(arrElems)) {return false;}
    var botones = '';
    var i = 0;
    $.each(arrElems, function(idelem, elem) {
        if($divResults.closest('.modal.in').find('.divVistaTabs .nav-tabs li[data-id="'+idelem+'"]').length>0){
            var color=' btn-success ';
            var activo=' active ';
        }else{
            var color=' btn-info ';
            var activo=' ';
        }
        botones+='<div class="col-xs-12 col-sm-3">'+
        '<button class="btn btn-xs btn-block  seleccionar'+color+activo+'" data-id="'+idelem+'" data-tipo="'+elem.tipo_adj+'">'+(i+1)+'. '+elem.titulo+'</button>'+
        '</div>';
        i++;
    });
    $divResults.html(botones);
};

var reordenarTabs = function ($contenedor) {
    $contenedor.find('.nav-tabs li>a').each(function(index, el) {
        $(this).html(index+1);
    });
};




<?php if($this->frmaccion=='Editar'){ ?>
/********************** Editar **********************/
var initFechaYHora = function($selFecha, $selHora){
    var ahora = Date.now();
    var now = new Date();
    var fecha_hora = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+(now.getDate()+1)+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    // fecha = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
    hora = ($selFecha.find('input').val()=='')? fecha_hora : $selFecha.find('input').val();
    $selFecha.datetimepicker({
        defaultDate: fecha_hora,
        date: fecha_hora,
        format: 'DD-MM-YYYY',
        locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
        minDate: "<?php echo date("Y-m-d H:i:s") ?>",
    });
    $selHora.datetimepicker({
        defaultDate: fecha_hora,
        date: hora,
        format: 'LT',
        locale: '<?php echo $this->documento->getIdioma()=="ES"? "es":"en";?>',
    });
};

var guardarAsignaciones = function() {
    var arrTarea_Asignacion=[];
    $('#tarea-add .frmAsignacion').each(function(i, frm) {
        var $frm = $(frm);
        var asignacion={
            'idtarea_asignacion': $frm.data('id'),
            'idtarea': $('#pkIdtarea').val(),
            'idgrupo': $frm.find('select.idseccion').val(),
            'fechaentrega': $frm.find('.dtpFecha').val(),
            'horaentrega': $frm.find('.dtpHora').val(),
            'alumnos':[],
        };
        $frm.find('.checkbox-ctrl.item-alumno').each(function(i, chkbox) {
            asig_alum = {
                'iddetalle': $(chkbox).data('iddetalle'),
                'idalumno': $(chkbox).val(),
                'estado': $(chkbox).data('estado'),
                'registrar': $(chkbox).is(':checked'),
            };
            asignacion.alumnos.push(asig_alum);
        });
        if(asignacion.alumnos.length<=0){ 
            mostrar_notificacion('<?php echo JrTexto::_('Warning') ?>', '<?php echo JrTexto::_('No students selected in assignment Assigning') ?>');
            return false; 
        }
        arrTarea_Asignacion.push(asignacion);
    });

    if(arrTarea_Asignacion.length<=0){
        return false;
    }

    $.ajax({
        url: _sysUrlBase_+'/tarea_asignacion/xGuardar',
        type: 'POST',
        dataType: 'json',
        data: {'arrTarea_asignacion':JSON.stringify(arrTarea_Asignacion)},
    }).done(function(resp) {
        if(resp.code=='ok'){
            var idTarea = resp.data;
            setTimeout(function() {
                window.location.reload(true);
            }, '1000');
            mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Assigning successful') ?>','success');
        }else{
            mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
        }
    }).fail(fnAjaxFail);
};


<?php } ?>

$(document).ready(function(){

<?php if($this->frmaccion=='Editar'){ ?>
    /********************** Editar **********************/
    $('#botones-accion').on('click', '.agregar_asignacion', function(e) {
        e.preventDefault();
        var now = Date.now();
        var idFrm = $('section #frmAsignacion').attr('id');
        var $frm = $('section #frmAsignacion').clone();
        $frm.attr({ 'id': idFrm+'_'+now, 'name': idFrm+'_'+now, });
        $('#botones-accion').before($frm);
        initFechaYHora($('#'+idFrm+'_'+now).find('.datetimepicker.fechaentrega'), $('#'+idFrm+'_'+now).find('.datetimepicker.horaentrega'));
    })

    var arrAlumnosXGrupo={};
    $('body').on('change', '.check-all', function(e) {
        var isChecked = $(this).is(':checked');
        var $contenedor = $(this).closest('form.frmAsignacion');
        var $item = $contenedor.find('.listado-alumnos ul li input.item-alumno').prop('checked', isChecked);
    }).on('click', 'form .eliminar_asignacion', function(e) {
        e.preventDefault();
        var $frm = $(this).closest('.frmAsignacion');
        $.confirm({
            title: '<?php echo JrTexto::_('Delete');?>',
            content: '<?php echo JrTexto::_('Are you sure to delete this record?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                var idAsig = $frm.attr('data-id') || 0;
                if(idAsig<=0){ $frm.remove(); } 
                else {
                    $.ajax({
                        url: _sysUrlBase_+'/tarea_asignacion/xEliminarAsignacion',
                        type: 'POST',
                        dataType: 'json',
                        data: {'idtarea_asignacion': idAsig},
                    }).done(function(resp) {
                        if(resp.code='ok'){ $frm.remove(); }
                        else{ mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error'); }
                    }).fail(fnAjaxFail);
                }
            },
        });
    });

    setTimeout(function(){
    $('.frmAsignacion').each(function(i,frm){
        initFechaYHora($(frm).find('.datetimepicker.fechaentrega'),$(frm).find('.datetimepicker.horaentrega'));
    }) },1000)
<?php } ?>

});
var actualizarcbo=function(cbo,predatos){
    var pd=cbo.closest('.filtros-alumnos');
    if(cbo.hasClass('idcolegio')){
       pd.find('select.idcurso').html('');
       pd.find('select.idgrados').html('');
       pd.find('select.idseccion').html('');
        $.each(predatos,function(e,v){
            console.log(pd.find('select.idcurso'));
            pd.find('select.idcurso').append('<option value="'+v["idcurso"]+'">'+v["strcurso"]+'</option>')
        })
       pd.find('select.idcurso').trigger('change');
    }else if(cbo.hasClass('idcurso')){
       pd.find('select.idgrados').html('');
       pd.find('select.idseccion').html('');
        $.each(predatos,function(e,v){
           pd.find('select.idgrados').append('<option value="'+v["idgrado"]+'">'+v["grado"]+'</option>')
        })
       pd.find('select.idgrados').trigger('change');
    }else if(cbo.hasClass('idgrados')){
       pd.find('select.idseccion').html('');
        $.each(predatos,function(e,v){
           pd.find('select.idseccion').append('<option value="'+v["idgrupoauladetalle"]+'">'+v["seccion"]+'</option>')
        })
       pd.find('select.idseccion').trigger('change');
    }
}
var datoscurso=<?php echo !empty($this->miscolegios)?json_encode($this->miscolegios):'[{}]'; ?>;
$(document).ready(function(){
    $('#tarea-add').on('change','select.idcolegio',function(ev){
        var idcolegio=$(this).val()||'';       
        for (var i = 0; i < datoscurso.length; i++) {
           if(datoscurso[i].idlocal==idcolegio){
                datoscurso[i].cursos.sort(function(a, b){return a.strcurso - b.strcurso;});
               actualizarcbo($(this),datoscurso[i].cursos);
           }
        }
    })
    $('#tarea-add').on('change','select.idcurso',function(ev){

        var idcurso=$(this).val()||'';
        var pd=$(this).closest('.filtros-alumnos');
        var idcolegio=pd.find('select.idcolegio').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;
                actualizarcbo($(this),grados);
            }
    })
    $('#tarea-add').on('change','select.idgrados',function(ev){
        var idgrados=$(this).val()||'';
        var pd=$(this).closest('.filtros-alumnos');
        var idcolegio=pd.find('select.idcolegio').val()||'';
        var idcurso=pd.find('select.idcurso').val()||'';
        for (var i = 0; i < datoscurso.length; i++)
        if(datoscurso[i].idlocal==idcolegio)
        for (var j = 0; j < datoscurso[i].cursos.length; j++)
            if(datoscurso[i].cursos[j].idcurso==idcurso){
                var grados=datoscurso[i].cursos[j].grados;
                for (var h = 0; h < grados.length; h++){
                    if(grados[h].idgrado==idgrados){
                        var secciones=grados[h].secciones;
                        secciones.sort(function(a, b){return a.seccion - b.seccion;});
                        actualizarcbo($(this),secciones);
                    }
                }
                
            }
    })
    $('#tarea-add').on('change','select.idseccion',function(ev){
        var idcursodetalle=$(this).val()||0;
        var $this=$(this);
        var idcursodetalle = $this.val();
        var $contenedor = $this.closest('form.frmAsignacion');
        if(idcursodetalle<=0){
            $contenedor.find('.listado-alumnos').hide();
            $contenedor.find('.listado-alumnos ul').html('');
            return false;
        }
        var list = '';
        $.ajax({
            url: _sysUrlBase_+'/tarea_asignacion/asignadosjson/',
            async: false,
            type: 'POST',
            dataType: 'json',
            data: {
                'idgrupoauladetalle':idcursodetalle,
                'idtarea':$('#pkIdtarea').val(),
            },
            beforeSend: function(){  
                $contenedor.find('.opcAlumno').attr('disabled','disabled'); 
            }
        }).done(function(resp) {
            if(resp.code=='ok'){ /***************    Alumnos    ***************/
                var matriculas = resp.data;
                var isnuevo=resp.isnuevo;
                if(matriculas.length){
                    $.each(matriculas, function(i, alum){
                        if(alum.estado=='X') alum.checked=false;
                        list += '<li class="col-xs-12 col-sm-6"> <label style="margin:0px;text-transform: capitalize;">';
                        list += '<input type="checkbox" '+(alum.checked==true?'checked="checked"':'')+' class="checkbox-ctrl item-alumno" name="chkIdAlumno[]" value="'+alum.idalumno+'" data-estado="'+alum.estado+'" data-iddetalle="'+alum.iddetalle+'">';
                        list += alum.stralumno;
                        list += '</label> </li>';
                    });
                } else {
                    list += '<span><?php echo JrTexto::_('There are no students in this group') ?></span>';
                }
                $contenedor.find('ul').html(list);
                $contenedor.find('.listado-alumnos').show();
                if(isnuevo==true){
                $contenedor.find('.check-all').prop('checked',true);
                $contenedor.find('.check-all').trigger('change');
                }
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error'); 
            }
        }).fail(fnAjaxFail).always(function(){
            $contenedor.find('.opcAlumno').removeAttr('disabled');
        });
        
    })    
    $('#tarea-add').on('click','.asignargrupoaula',function(ev){
        ev.preventDefault();
        var arrTarea_Asignacion=[];       
        $(this).closest('.frmAsignacion').each(function(i, frm){
            var $frm = $(frm);
            var asignacion={
                'idtarea_asignacion': $frm.data('id'),
                'idtarea': $('#pkIdtarea').val(),
                'idgrupo': $frm.find('.idseccion').val(),
                'fechaentrega': $frm.find('.dtpFecha').val(),
                'horaentrega': $frm.find('.dtpHora').val(),
                'alumnos':[],
            };
            $frm.find('.checkbox-ctrl.item-alumno').each(function(i, chkbox) {
                asig_alum = {
                    'iddetalle': $(chkbox).data('iddetalle'),
                    'idalumno': $(chkbox).val(),
                    'estado': $(chkbox).data('estado'),
                    'registrar': $(chkbox).is(':checked'),
                };
                asignacion.alumnos.push(asig_alum);
            });
            if(asignacion.alumnos.length<=0){ 
                mostrar_notificacion('<?php echo JrTexto::_('Warning') ?>', '<?php echo JrTexto::_('No students selected in assignment Assigning') ?>');
                return false;
            }
            arrTarea_Asignacion.push(asignacion);
        });

        if(arrTarea_Asignacion.length<=0){
            return false;
        }

        $.ajax({
            url: _sysUrlBase_+'/tarea_asignacion/xGuardar',
            type: 'POST',
            dataType: 'json',
            data: {'arrTarea_asignacion':JSON.stringify(arrTarea_Asignacion)},
        }).done(function(resp) {
            if(resp.code=='ok'){
                var idTarea = resp.data;
                setTimeout(function() {
                    window.location.reload(true);
                }, '1000');
                mostrar_notificacion('<?php echo JrTexto::_('Done') ?>', '<?php echo JrTexto::_('Assigning successful') ?>','success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error') ?>',resp.msj,'error');
            }
        }).fail(fnAjaxFail);
       
    })
    $('select.idseccion').trigger('change');
})
</script>