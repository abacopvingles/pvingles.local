<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/IIEE'">&nbsp;<?php echo JrTexto::_('IIEE'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>">
  <div class="col-md-12 col-sm-12 col-xs-12" id="pnlbotonesde<?php echo $idgui; ?>">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <div class="alert alert-info" role="alert">
            <strong>Paso 01</strong> Descargue plantilla excel para subir datos.
        </div>
        <a class="btn btn-primary" href="<?php echo $this->documento->getUrlStatic();?>/importar_xls/iiee.xlsx" download ><i class="fa fa-cloud-download"></i> <?php echo JrTexto::_('Download')?></a>
        <hr>
        <div class="alert alert-info" role="alert">
            <strong>Paso 02</strong> Suba plantilla excel llenada para importar.
        </div>
        <a class="btn btn-primary btnimport<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Upload')?></a>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide" id="paneldatosaimportar<?php echo $idgui; ?>" >
    
  </div>
</div>
<div id="datosimport<?php echo $idgui; ?>" class=" hide col-md-12">
  <div class="panel">
  <div class="panel-body">
  <table class="table table-striped table-responsive tableimport<?php echo $idgui; ?>">
    <tr class="tr01">
      <th><?php echo JrTexto::_("Id DRE");?></th>
      <th><?php echo JrTexto::_("DRE");?></th>
      <th><?php echo JrTexto::_("Id Ugel");?></th>
      <th><?php echo ucfirst(JrTexto::_("Ugel")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("id IIEE")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("IIEE")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("Direccion IIEE")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("Ubigeo IIEE")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("Tipo")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("N. Vacantes")); ?></th>
      <th><?php echo ucfirst(JrTexto::_('Actions')); ?></th>
    </tr>
  </table>
  <div class="col-md-12 text-center">
  <hr>
    <a href="javascript:void(0)" class="btnsaveimport<?php echo $idgui; ?> btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Import') ?></a>
  </div>
  </div>
</div>
<input type="file" name="importjson" id="importjson" class="hide" accept="file_extension|*/xls|*/xlsx|media_type">
<script src="<?php echo $this->documento->getUrlStatic();?>/libs/importexcel/xlsx.full.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.btnimport<?php echo $idgui; ?>').click(function(ev){
        $('#importjson').trigger('click');
    })
  var id_ubigeo='<?php echo !empty($frm["ubigeo"])?$frm["ubigeo"]:"010000" ?>';
 $('#paneldatosaimportar<?php echo $idgui; ?>').on('click','.btnsaveimport<?php echo $idgui; ?>',function(ev){
    var tableimport=$('#paneldatosaimportar<?php echo $idgui; ?> table.tableimport<?php echo $idgui; ?>');
    if(tableimport.length){
      var tr=tableimport.find("tr.addregistroimporttr");
      var datosimportados=[];
      $.each(tr,function(i,v){
        var data={
          iddre:$(v).find("td:eq(0)").text().trim(),
          dre:$(v).find("td:eq(1)").text().trim(),
          idugel:$(v).find("td:eq(2)").text().trim(),
          ugel:$(v).find("td:eq(3)").text().trim(),
          idiiee:$(v).find("td:eq(4)").text().trim(),
          iiee:$(v).find("td:eq(5)").text().trim(),
          direccion:$(v).find("td:eq(6)").text().trim(),
          ubigeo:$(v).find("td:eq(7)").text().trim(),
          tipo:$(v).find("td:eq(8)").text().trim(),
          nvacantes:$(v).find("td:eq(9)").text().trim(),
        }
        datosimportados.push(data);
      });
     
      var formData = new FormData();
      formData.append('datosimportados', JSON.stringify(datosimportados));
      var data={
        fromdata:formData,
        url:_sysUrlBase_+'/local/importardatos',
        msjatencion:'<?php echo JrTexto::_('Attention');?>',
        type:'json',
        showmsjok : true,
        callback:function(data){
            var rd=<?php echo $ventanapadre ; ?>;
            if(_isFunction(rd)){ rd(); $('.close').trigger('click');}
        }
      }
      sysajax(data);
      return false;
    }    
  }).on('click','.btn_removetr',function(ev){
      $(this).closest('tr').remove();
  })
});


</script>
<script>
  //para importar json
var X = XLSX;
//var XW = {msg: 'xlsx',worker: './xlsxworker.js'};

var global_wb;
var process_wb = (function() {
  var OUT = document.getElementById('out');
  var to_json = function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function(sheetName) {
      var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});
      if(roa.length) result[sheetName] = roa;
    });
    return JSON.stringify(result, 2, 2);
  };

  return function process_wb(wb) {
    global_wb = wb;
    var output = to_json(wb);
    if(_isJson(output)){
      $table=$('#datosimport<?php echo $idgui; ?>').clone();
      $table.removeClass('hide').attr('id','');
      var nceldas=$table.find('tr').find('th').length-1;
      var hoja1=JSON.parse(output).Hoja1;
      var ix_=0;
      hoja1.forEach(function(v){
        if(ix_>0){
            $tr='<tr class="addregistroimporttr">';
            $i=1;
            v.forEach(function(v1){
                $i++;
            $tr+='<td>'+v1+'</td>';
            })
            if($i<=nceldas)
            for(i=$i;i<=nceldas;i++) $tr+='<td></td>';
            $tr+='<td><i class="fa fa-trash btn_removetr"></i> </td>';
            $tr+='</tr>';
            $table.find('table').append($tr);
        } ix_++;
      });      
      $('#paneldatosaimportar<?php echo $idgui; ?>').html($table);
    }
  };
})();


var do_file = (function() {
  var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
  var use_worker = typeof Worker !== 'undefined';

  return function do_file(files) {
    rABS = true;//domrabs.checked;
    use_worker = true; //domwork.checked;
    var f = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      //if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
      var data = e.target.result;
      if(!rABS) data = new Uint8Array(data);
      else process_wb(X.read(data, {type: rABS ? 'binary' : 'array'}));
    };
    if(rABS) reader.readAsBinaryString(f);
    else reader.readAsArrayBuffer(f);
  };
})();


(function() {
  var xlf = document.getElementById('importjson');
  if(!xlf.addEventListener) return;
  function handleFile(e) { do_file(e.target.files);
    $('#pnlbotonesde<?php echo $idgui; ?>').addClass('hide');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide');
}
  xlf.addEventListener('change', handleFile, false);
})();
</script>

