<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$datareturn=$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Personal'">&nbsp;<?php echo JrTexto::_('Persona'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>">
  <div class="col-md-12 col-sm-12 col-xs-12" id="pnlbotonesde<?php echo $idgui; ?>">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <div class="alert alert-info" role="alert">
            <strong>Paso 01</strong> Descargue plantilla excel para subir datos.
        </div>
        <a class="btn btn-primary" href="<?php echo $this->documento->getUrlStatic();?>/importar_xls/matriculas.xlsx" download ><i class="fa fa-cloud-download"></i> <?php echo JrTexto::_('Download')?></a>
        <hr>
        <div class="alert alert-info" role="alert">
            <strong>Paso 02</strong> Suba plantilla excel llenada para importar.
        </div>
        <a class="btn btn-primary btnimport<?php echo $idgui; ?>" href="javascript:void(0)" ><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Upload')?></a>
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12 hide" id="paneldatosaimportar<?php echo $idgui; ?>" ></div>
</div>
<div id="datosimport<?php echo $idgui; ?>" class=" hide col-md-12">
  <div class="panel">
    <div class="panel-body">
        <form id="mfrmEstudiantes<?php echo $idgui; ?>">
          <input type="hidden" name="plt" value="blanco">
          <input type="hidden" name="fcall" value="<?php echo $ventanapadre; ?>">
          <input type="hidden" name="datareturn" value="<?php echo $datareturn; ?>">
          <input type="hidden" name="fkcbrol" value="3">
          <input type="hidden" name="midgrupoauladetalle" id="midgrupoauladetalle" value="">
          <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4 form-group" >
                <label><?php echo ucfirst(JrTexto::_('Dre')); ?></label>            
                <div class="cajaselect">
                    <select name="dre" id="mfkcbdre" class="form-control mfkcbdre"></select>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                <label><?php echo ucfirst(JrTexto::_('UGEL')); ?></label>
                <div class="cajaselect">
                    <select  name="ugel" class="form-control mfkcbugel"></select>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-4 form-group"  >
                <label><?php echo ucfirst(JrTexto::_('IIEE')); ?></label>
                <div class="cajaselect">
                    <select  name="iiee" class="form-control mfkcbiiee"></select>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
              <label><?php echo ucfirst(JrTexto::_('Courses')); ?></label>
              <div class="cajaselect">
                <select  name="fkcbcurso" class="form-control mfkcbcurso">
                </select>
              </div>
            </div>            
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                <label><?php echo JrTexto::_('Grade'); ?></label>
                <div class="cajaselect">
                <select  name="fkcbgrado" class="form-control mfkcbgrado" >
                    <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php 
                    if(!empty($this->grados))
                    foreach ($this->grados as $r) { ?><option value="<?php echo $r["idgrado"]?>" ><?php echo $r["abrev"]." : ".$r["descripcion"] ?></option>
                    <?php } ?>                        
                </select>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-4 form-group">
                <label><?php echo JrTexto::_('Section'); ?></label>
                <div class="cajaselect">
                <select  name="sexo" class="form-control mfkcbseccion" >
                    <!--option value=""><?php //echo JrTexto::_('All'); ?></option-->
                    <?php 
                    if(!empty($this->seccion))
                    foreach ($this->seccion as $s) { ?><option value="<?php echo $s["idsesion"]?>" ><?php echo $s["descripcion"] ?></option>
                    <?php } ?>                        
                </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 form-group">
              <label><?php echo  ucfirst(JrTexto::_("Assigned teacher"))?></label>
                <div class="input-group" style="margin:0px">
                  <input type="hidden" id="midteacher" name="midteacher" class="form-control border0 midteacher" >
                  <input style="background: #fff; border: 1px #ccc solid;" type="text" name="mteachername" id="mteachername" readonly="readonly" class="form-control border0  mteachername" placeholder="<?php echo  ucfirst(JrTexto::_("Name teacher"))?>">
                  <span class="input-group-addon btn mbtnbuscarteacher<?php echo $idgui;?>"> <i class="fa fa-search"></i></span>  
                </div>
            </div>
        </div>
    </div>
</form>
  <div class="col-md-12 text-center"><hr><h2><?php echo JrTexto::_("Listado de alumnos a matricular");?></h2></div>
  <div class="col-md-12 table-striped table-responsive panel-body">  
   <table class="table  tableimport<?php echo $idgui; ?>">
   <thead>
    <tr class="tr01">
      <th><?php echo JrTexto::_("DNI");?></th>
      <th><?php echo JrTexto::_("ape_paterno");?></th>
      <th><?php echo JrTexto::_("ape_materno");?></th>
      <th><?php echo ucfirst(JrTexto::_("nombre")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("fecha_nacimiento")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("sexo")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("N. Celular")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("email")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("usuario")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("clave")); ?></th>
      <th><?php echo ucfirst(JrTexto::_("Acciones")); ?></th>
    </tr>
    <thead>
    <tbody>
    </tbody>
    </table>
    <div>
  <div class="col-md-12 text-center">
  <hr>
    <a href="javascript:void(0)" class="btnsaveimport<?php echo $idgui; ?> btn btn-primary"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save Import') ?></a>
  </div>
  </div>
</div>
<input type="file" name="importjson" id="importjson" class="hide" accept="file_extension|*/xls|*/xlsx|media_type">
<script src="<?php echo $this->documento->getUrlStatic();?>/libs/importexcel/xlsx.full.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.btnimport<?php echo $idgui; ?>').click(function(ev){
        $('#importjson').trigger('click');
    })
    var id_ubigeo='<?php echo !empty($frm["ubigeo"])?$frm["ubigeo"]:"010000" ?>';
   
    $('#paneldatosaimportar<?php echo $idgui; ?>').on('click','.btnsaveimport<?php echo $idgui; ?>',function(ev){
        var tableimport=$('#paneldatosaimportar<?php echo $idgui; ?> table.tableimport<?php echo $idgui; ?>');
        if(tableimport.length){
        var tr=tableimport.find("tr.addregistroimporttr");
        var datosimportados=[];
        $.each(tr,function(i,v){
            var data={
            tipodoc:1, 
            dni:$(v).find("td:eq(0)").text().trim(),
            ape_paterno:$(v).find("td:eq(1)").text().trim(),
            ape_materno:$(v).find("td:eq(2)").text().trim(),
            nombre:$(v).find("td:eq(3)").text().trim(),
            fechanac:$(v).find("td:eq(4)").text().trim(),
            sexo:$(v).find("td:eq(5)").text().trim(),
            estado_civil:'S',
            ubigeo:'',
            urbanizacion:'',
            direccion:'',
            telefono:'',
            celular:$(v).find("td:eq(6)").text().trim(),
            email:$(v).find("td:eq(7)").text().trim(),
            usuario:$(v).find("td:eq(8)").text().trim(),
            clave:$(v).find("td:eq(9)").text().trim(),
            idrol:$(v).find("td:eq(10)").text().trim(),
            rol:$(v).find("td:eq(11)").text().trim(),
            foto:'',
            estado:1,
            situacion:1,
            idioma:'EN'
            }
            datosimportados.push(data);
        });
        
        var formData = new FormData();
        formData.append('datosimportados', JSON.stringify(datosimportados));
        formData.append('iddocente', $('#midteacher').val()||'');
        formData.append('idgrupoauladetalle', $('#midgrupoauladetalle').val()||'');
        formData.append('idiiee', $('.mfkcbiiee').val()||'');
        formData.append('idcurso', $('.mfkcbcurso').val()||'');
        formData.append('seccion', $('.mfkcbseccion').val()||'');
        formData.append('grado', $('.mfkcbgrado').val()||'');
        formData.append('ugel', $('.mfkcbugel').val()||'');
        var data={
            fromdata:formData,
            url:_sysUrlBase_+'/matricula/importardatos',
            msjatencion:'<?php echo JrTexto::_('Attention');?>',
            type:'json',
            showmsjok : true,
            callback:function(data){
                var rd=<?php echo $ventanapadre ; ?>;
                if(_isFunction(rd)){ rd(); $('.close').trigger('click');}
            }
        }
        sysajax(data);
        return false;
        }    
    }).on('click','.btn_removetr',function(ev){
        $(this).closest('tr').remove();
    })

    $('#vent-<?php echo $idgui;?>').on('change','select#mfkcbdre',function(ev){
      ev.preventDefault();
      var iddre=$(this).val();
      var fd2= new FormData();
      fd2.append("iddepartamento", iddre);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/ugel/buscarjson',
      callback:function(rs){
          $sel=$('select.mfkcbugel');
          dt=rs.data;
          $sel.html('');
          $.each(dt,function(i,v){$sel.append('<option value="'+v.idugel+'" >'+v.descripcion+'</option>'); })
          $sel.trigger("change");
      }})
    }).on('change','select.mfkcbugel',function(ev){
        ev.preventDefault();
        var idugel=$(this).val();
        var fd2= new FormData();
        fd2.append("idugel", idugel);
        sysajax({
        fromdata:fd2,
        url:_sysUrlBase_+'/local/buscarjson',
        callback:function(rs){
            $sel=$('select.mfkcbiiee');
            dt=rs.data;
            $sel.html('');
            $.each(dt,function(i,v){
              $sel.append('<option value="'+v.idlocal+'" >'+v.nombre+'</option>');
            })
            $sel.trigger("change");
        }})
    }).on('change','select.mfkcbiiee, select.mfkcbcurso , select.mfkcbgrado, select.mfkcbseccion',function(ev){
        buscargrupoauladetalle<?php echo $idgui; ?>();
    }).on('click','.mbtnbuscarteacher<?php echo $idgui; ?>',function(e){
      e.preventDefault();
      e.stopPropagation();
 		  var clstmp=Date.now();
	    var fcall=$(this).attr('data-fcall')||clstmp;
	    var url=_sysUrlBase_+'/personal/?fcall='+fcall+'&datareturn=true&rol=2';
	    var claseid='docente_'+clstmp;
	    var show={header:true,footer:false,borrarmodal:true};
	    var titulo=$(this).attr('data-titulo')||'';
	    titulo='<?php echo JrTexto::_('Teacher'); ?>';
	    url+='&plt=modal';
	    $(this).addClass('tmp'+clstmp);
	    $(this).on('returndata',function(ev){
	    	$(this).removeClass('tmp'+clstmp);
        var tmpdata=JSON.parse($(this).attr('data-return'));
	    	$('#midteacher').val(tmpdata.id);
	    	$('#mteachername').val(tmpdata.nombre);
	    });
	    openModal('lg',titulo,url,true,claseid,show);
   });
   
    var buscargrupoauladetalle<?php echo $idgui; ?>=function(){
    var fd2= new FormData();
    var idiiee=$('.mfkcbiiee').val()||-1;
    var idcurso=$('.mfkcbcurso').val()||-1;
    var idgrado=$('.mfkcbgrado').val()||-1;
    var idsecion=$('.mfkcbseccion').val()||-1;
    fd2.append("idcurso", idcurso);
    fd2.append("idlocal", idiiee);
    fd2.append("idgrado", idgrado);
    fd2.append("idsesion", idsecion);
    fd2.append("sql2", true);
    sysajax({
    fromdata:fd2,
    url:_sysUrlBase_+'/acad_grupoauladetalle/buscarjson',
    callback:function(rs){
      console.log(rs);
      if(rs.code=='ok'||rs.code==200){
        var dt=rs.data[0]||-1;
        if(dt!=-1){
          $('.midteacher').val(dt.iddocente);
          $('.mteachername').val(dt.strdocente);
          $('#midgrupoauladetalle').val(dt.idgrupoauladetalle)
        }else{
          $('.midteacher').val('');
          $('.mteachername').val('');
          $('#midgrupoauladetalle').val('')
        }
      }
     
    }
    })
  }
  



});
</script>
<script>
  //para importar json
var X = XLSX;
//var XW = {msg: 'xlsx',worker: './xlsxworker.js'};

var global_wb;
var process_wb = (function() {
  var OUT = document.getElementById('out');
  var to_json = function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function(sheetName) {
      var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:1});
      if(roa.length) result[sheetName] = roa;
    });
    return JSON.stringify(result, 2, 2);
  };

  return function process_wb(wb) {
    global_wb = wb;
    var output = to_json(wb);
    if(_isJson(output)){
      $table=$('#datosimport<?php echo $idgui; ?>').clone();
      $table.removeClass('hide').attr('id','');
      var nceldas=$table.find('tr').find('th').length-1;
      var hoja1=JSON.parse(output).Hoja1;
      var ix_=0;
      hoja1.forEach(function(v){
        if(ix_>0){
            $tr='<tr class="addregistroimporttr">';
            $i=1;
            v.forEach(function(v1){
                $i++;
            $tr+='<td>'+v1+'</td>';
            })
            if($i<=nceldas)
            for(i=$i;i<=nceldas;i++) $tr+='<td></td>';
            $tr+='<td><i class="fa fa-trash btn_removetr"></i> </td>';
            $tr+='</tr>';
            $table.find('table tbody').append($tr);
        } ix_++;
      });      
      $('#paneldatosaimportar<?php echo $idgui; ?>').html($table);
      setTimeout(function(){$('table.tableimport<?php echo $idgui; ?>').dataTable({"pageLength": 50,
        "language": { "url": _sysIdioma_.toUpperCase()!='EN'?(_sysUrlBase_+"/static/libs/datatable1.10/idiomas/"+_sysIdioma_.toUpperCase()+".json"):''}
      })},500);
    }
  };
})();


var do_file = (function() {
  var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
  var use_worker = typeof Worker !== 'undefined';
  return function do_file(files) {
    rABS = true;//domrabs.checked;
    use_worker = true; //domwork.checked;
    var f = files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
      //if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
      var data = e.target.result;
      if(!rABS) data = new Uint8Array(data);
      else process_wb(X.read(data, {type: rABS ? 'binary' : 'array'}));
    };
    if(rABS) reader.readAsBinaryString(f);
    else reader.readAsArrayBuffer(f);
  };
})();


(function() {
  var xlf = document.getElementById('importjson');
  if(!xlf.addEventListener) return;
  function handleFile(e) { do_file(e.target.files);
    $('#pnlbotonesde<?php echo $idgui; ?>').addClass('hide');
    $('#paneldatosaimportar<?php echo $idgui; ?>').removeClass('hide');
    $('.mfkcbdre').html($('.fkcbdre').html());
    $('.mfkcbugel').html($('.fkcbugel').html());
    $('.mfkcbiiee').html($('.fkcbiiee').html());
    $('.mfkcbcurso').html($('.fkcbcurso').html());
    $('.mfkcbgrado').html($('.fkcbgrado').html());
    $('.mfkcbseccion').html($('.fkcbseccion').html());
    $('.midteacher').val($('.idteacher').val());
    $('.mteachername').val($('.teachername').val());
    $('#midgrupoauladetalle').val($('#idgrupoauladetalle').val());
    
}
  xlf.addEventListener('change', handleFile, false);
})();
</script>

