<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'));?>"><?php echo JrTexto::_('Bib_libro'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success" href="<?php echo JrAplicacion::getJrUrl(array("Bib_libro", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('Add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content" style="overflow: auto;">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Codigo") ;?></th>
                    <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Editorial") ;?></th>
                    <th><?php echo JrTexto::_("Foto") ;?></th>
                    <th><?php echo JrTexto::_("Archivo") ;?></th>
                    <th><?php echo JrTexto::_("Tipo")."(".JrTexto::_("Idtipo").")"; ?></th>
                    <th><?php echo JrTexto::_("Autor")."(".JrTexto::_("Idautor").")"; ?></th>
                    <th><?php echo JrTexto::_("Condicion") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td class="text-right"><?php echo $reg["codigo"] ;?></td>
                    <td><?php echo $reg["nombre"] ;?></td>
                    <td><?php echo $reg["editorial"] ;?></td>
                    <td class="text-center"><img src="<?php echo $this->documento->getUrlStatic().'/libreria/image/'.$reg["foto"]; ?>" class="img-thumbnail" style="max-height:70px; max-width:50px;"></td>
                    <td class="text-center"><a href="<?php echo $this->documento->getUrlStatic().'/libreria/pdf/'.$reg["archivo"]; ?>" target="_blank"><?php echo JrTexto::_("ver documento"); ?></a></td>
                    <td><?php echo $reg["tipo"]; ?></td>
                    <td><?php echo $reg["autor"]; ?></td>
                    <td><?php echo $reg["condicion"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('bib_libro'))?>ver/?id=<?php echo $reg["idlibro"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("bib_libro", "editar", "id=" . $reg["idlibro"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idlibro"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'bib_libro', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('bib_libro'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'bib_libro', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('bib_libro'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>