<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Local'">&nbsp;<?php echo JrTexto::_('IIEE'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdlocal" id="pkidlocal" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo $this->frmaccion;?>">
          <input type="hidden"  id="txtVacantes" name="txtVacantes" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo !empty($frm["vacantes"])?$frm["vacantes"]:500;?>">
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('DRE');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                  <select id="idDrefrm" class="form-control select-ctrl " name="idDre"></select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('UGEL');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                  <select id="txtIdugel" class="form-control select-ctrl " name="txtIdugel"></select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Direccion');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtId_ubigeo">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                  <select id="txtId_ubigeo" class="form-control select-ctrl " name="txtId_ubigeo"></select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="select-ctrl-wrapper select-azul">
                  <select id="txtTipo" class="form-control select-ctrl " name="txtTipo">
                    <option value="C" selected="selected">IIEE</option>
                    <option value="L">Local</option>
                  </select>
                </div>
              </div>
            </div>
            <!--div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdproyecto">
              <?php //echo JrTexto::_('Idproyecto');?> <span class="required"> * </span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdproyecto" name="txtIdproyecto" required="required" class="form-control col-md-7 col-xs-12" value="<?php //echo @$frm["idproyecto"];?>">
              </div>
            </div-->

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveLocal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning cerramodal" href="<?php echo JrAplicacion::getJrUrl(array('local'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var _cargarDRE=function(idsel,recarga){
    sysajax({
      url:_sysUrlBase_+'/min_dre/buscarjson',
      callback:function(rs){
        $sel=$('#idDrefrm');
          dt=rs.data;
          $sel.html('');
          $.each(dt,function(i,v){
            $sel.append('<option value="'+v.ubigeo+'" '+(v.ubigeo==idsel?'selected="selected"':'')+ '>'+v.descripcion+'</option>');
            if(recarga)$sel.trigger('change');
          })
      }})
  }
  <?php if(empty($frm)){?> _cargarDRE(-1,true); <?php }else{?>
    _cargarDRE('<?php echo $frm["idugel"] ?> ',true);
  <?php } ?>
  $('#frm-<?php echo $idgui;?>').on('change','select#idDrefrm',function(ev){
      ev.preventDefault();
      var iddre=$(this).val();
      var fd2= new FormData();
      var idugel=<?php echo !empty($frm["idugel"])?$frm["idugel"]:'-1'?>;
      fd2.append("iddepartamento", iddre);
      sysajax({
      fromdata:fd2,
      url:_sysUrlBase_+'/ugel/buscarjson',
      callback:function(rs){
        $sel=$('#txtIdugel');
         $sel.html('');
          dt=rs.data;
          $.each(dt,function(i,v){
            var selop='';
            if(idugel!=-1 && v.idugel==idugel) selop='selected="selected"';
            $sel.append('<option value="'+v.idugel+'" '+selop+ '>'+v.descripcion+'</option>');
          })
      }})

      var iddep=($(this).val()||'00').substr(0,2);
      var idprovincia='<?php echo !empty($frm["id_ubigeo"])?$frm["id_ubigeo"]:'-1';?>';
      var fd = new FormData();
      fd.append("departamento", iddep);
      fd.append("provincia", 'all');
      fd.append("distrito", "00");
      fd.append("pais", "PE");
      sysajax({
        fromdata:fd,
        url:_sysUrlBase_+'/ubigeo/buscarjson',
        callback:function(rs){
          $sel=$('#txtId_ubigeo');
          $sel.html('');
            dt=rs.data;
            $.each(dt,function(i,v){
              $sel.append('<option value="'+v.id_ubigeo+'" '+(v.id_ubigeo==idprovincia?'selected="selected"':'')+ '>'+v.ciudad+'</option>');
            })
        }})
    })

            
$('#frm-<?php echo $idgui;?>').bind({
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'local', 'saveLocal', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Local"))?>');
      }
     }
  });
});
</script>

