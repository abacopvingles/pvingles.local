<?php 
defined('RUTA_BASE') or die();
$idgui=uniqid();
$ismodal=$this->documento->plantilla!='modal'?false:true;
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?>
  <div class="row" id="breadcrumb">
  <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/academico">&nbsp;<?php echo JrTexto::_('Academic'); ?></a></li>
        <li><a href="<?php echo $this->documento->getUrlBase();?>/Personal'">&nbsp;<?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active">&nbsp;<?php echo JrTexto::_($this->frmaccion);?></li>
    </ol>
  </div>
</div>
<?php } ?>
<div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">            
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdpersona" id="pkidpersona" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipodoc">
              <?php echo JrTexto::_('Tipodoc');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTipodoc" name="txtTipodoc" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tipodoc"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDni">
              <?php echo JrTexto::_('Dni');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDni" name="txtDni" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["dni"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_paterno">
              <?php echo JrTexto::_('Ape paterno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_paterno" name="txtApe_paterno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_paterno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_materno">
              <?php echo JrTexto::_('Ape materno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_materno" name="txtApe_materno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_materno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechanac">
              <?php echo JrTexto::_('Fechanac');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechanac" name="txtFechanac" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechanac"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSexo">
              <?php echo JrTexto::_('Sexo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSexo" name="txtSexo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["sexo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado_civil">
              <?php echo JrTexto::_('Estado civil');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado_civil" name="txtEstado_civil" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado_civil"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUbigeo" name="txtUbigeo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ubigeo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUrbanizacion">
              <?php echo JrTexto::_('Urbanizacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUrbanizacion" name="txtUrbanizacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["urbanizacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Direccion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTelefono">
              <?php echo JrTexto::_('Telefono');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTelefono" name="txtTelefono" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["telefono"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCelular">
              <?php echo JrTexto::_('Celular');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCelular" name="txtCelular" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["celular"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEmail">
              <?php echo JrTexto::_('Email');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEmail" name="txtEmail" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Idugel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdugel" name="txtIdugel" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idugel"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegusuario">
              <?php echo JrTexto::_('Regusuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegusuario" name="txtRegusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegfecha">
              <?php echo JrTexto::_('Regfecha');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegfecha" name="txtRegfecha" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUsuario">
              <?php echo JrTexto::_('Usuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUsuario" name="txtUsuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtClave">
              <?php echo JrTexto::_('Clave');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtClave" name="txtClave" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["clave"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtToken">
              <?php echo JrTexto::_('Token');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtToken" name="txtToken" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["token"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRol">
              <?php echo JrTexto::_('Rol');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRol" name="txtRol" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["rol"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFoto" name="txtFoto" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["foto"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado" name="txtEstado" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSituacion">
              <?php echo JrTexto::_('Situacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtSituacion" name="txtSituacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["situacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdioma">
              <?php echo JrTexto::_('Idioma');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdioma" name="txtIdioma" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idioma"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipousuario">
              <?php echo JrTexto::_('Tipousuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTipousuario" name="txtTipousuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tipousuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdlocal">
              <?php echo JrTexto::_('Idlocal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdlocal" name="txtIdlocal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idlocal"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-savePersonal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'personal', 'savePersonal', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Personal"))?>');
      }
     }
  });

 
  
});


</script>

