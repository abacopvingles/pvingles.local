<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Nota_detalle'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdnota_detalle" id="pkidnota_detalle" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdcurso">
              <?php echo JrTexto::_('Idcurso');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdcurso" name="txtIdcurso" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idcurso"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdmatricula">
              <?php echo JrTexto::_('Idmatricula');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdmatricula" name="txtIdmatricula" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idmatricula"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdalumno">
              <?php echo JrTexto::_('Idalumno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdalumno" name="txtIdalumno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idalumno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIddocente">
              <?php echo JrTexto::_('Iddocente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIddocente" name="txtIddocente" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["iddocente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdcursodetalle">
              <?php echo JrTexto::_('Idcursodetalle');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdcursodetalle" name="txtIdcursodetalle" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idcursodetalle"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNota">
              <?php echo JrTexto::_('Nota');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNota" name="txtNota" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nota"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTipo" name="txtTipo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["tipo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtObservacion">
              <?php echo JrTexto::_('Observacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtObservacion" name="txtObservacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["observacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFecharegistro">
              <?php echo JrTexto::_('Fecharegistro');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFecharegistro" name="txtFecharegistro" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fecharegistro"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEstado" name="txtEstado" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["estado"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveNota_detalle" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('nota_detalle'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'nota_detalle', 'saveNota_detalle', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Nota_detalle"))?>');
      }
     }
  });

 
  
});


</script>

