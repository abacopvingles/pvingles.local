<?php 


defined("RUTA_BASE") or die(); 

?>
<input type="hidden" />
<style>

</style>

<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/examenes/general.css"
>

<div class="back2"><h1>docente</h1></div>

<div class="page-title">
    <div class="title_left"><h3></h3>
        <?php echo JrTexto::_('Reporte');?>        
    </div>
</div>

<table class="table table-striped table-responsive">
        <thead>
        <tr class="headings">
            <th>#</th>
            <th><?php echo JrTexto::_("Nombre") ;?></th>
            <th><?php echo JrTexto::_("Tiempo") ;?></th>
        </tr>
        </thead>
        <tbody>
        <?php $i=0; 
        if(!empty($this->alumnos))
        foreach ($this->alumnos as $reg){ $i++; ?>
        <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $reg["nombre"] ;?></td>
            <td><?php echo $reg["tiempo"]; ?>
            </td>                       
    </tr>
    <?php } ?>
            </tbody>
</table>

<script type="text/javascript">

$('document').ready(function(){
    console.log(_sysIdioma_);
    $('.table').DataTable( {
    "language": {
            "url": _sysUrlStatic_+'/libs/datatable1.10/idiomas/'+_sysIdioma_.toUpperCase() + ".json"
    }
  });

});
</script>