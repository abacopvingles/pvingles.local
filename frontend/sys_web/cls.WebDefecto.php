<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAgenda', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno_logro', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{	
	private $oNegNiveles;
	private $oNegResources;
    protected $oNegMetodologia;
	protected $oNegAlumno_logro;
	protected $oNegGrupoaulaDetalle;
	protected $oNegAcad_curso;
	protected $oNegPersonasetting;
	protected $oNegLocal;
	protected $oNegMin_dre;
	protected $oNegUgel;
	protected $oNegUbigeo;
	protected $oNegPersonal;

	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;
		$this->oNegResources = new NegResources;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegAgenda = new NegAgenda;
        $this->oNegAlumno_logro = new NegAlumno_logro;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegGrupoaulaDetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegPersonasetting = new NegPersona_setting;
		$this->oNegLocal = new NegLocal;
		$this->oNegMin_dre = new NegMin_dre;
		$this->oNegUgel = new NegUgel;
		$this->oNegUbigeo = new NegUbigeo;
		$this->oNegPersonal = new NegPersonal;
	}

	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Inicio'), true);
			$user=$this->usuarioAct;
			$this->user = $this->usuarioAct;
			$this->esdemo=!empty($this->user["esdemo"])?1:0;
			//var_dump($this->user);
			
			//Chequear estado de la bandera del localstorage 
			$this->localstorage_flag = false;
			if(NegSesion::get('localstorage_flag', '__admin_m3c') == true){
				$this->localstorage_flag = true;
				//NegSesion::set('localstorage_flag', false, '__admin_m3c');	
			}
			$rol=$this->usuarioAct["rol"];
			$idrol=(int)$this->usuarioAct["idrol"];
			$this->idexamenUbicacion=326;
			$this->showExaUbicacion=true;
			if($idrol==3){
				$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
				$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
				$examenUbicacion=$this->oNegPersonasetting->buscar(array('idproyecto'=>$user["idproyecto"],'idpersona'=>$user["idpersona"],'tipo'=>'EU','idrol'=>$user["idrol"]));
					if(!empty($examenUbicacion[0])){
					$dt=json_decode($examenUbicacion[0]["datos"]);
					$fecha=date('Y-m-d');
					if($dt->fecha==$fecha|| strtolower($dt->accion)=='no'|| strtolower($dt->accion)=='si'){
						$this->showExaUbicacion=false;
					}
				} 
				$this->cursosMatric=array();
				foreach($cursosProyecto as $cur){
					$cur["activo"]=false;			
					foreach($cursosMat as $curM){ 
						if($cur["idcurso"]==$curM["idcurso"]){
							$cur['activo']=true;
						}
					}
					// if($cur["idcurso"]!=31)	
					$this->cursosMatric[]=$cur;
				}
				//var_dump($this->cursosMatric);
				/*Posibles vistas al antojo del Jefe:*/
				#$this->vistaConLogrosYAgenda();
				$this->vistaConHabilidades();
			}else{			
				$this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
				$this->documento->script('circle', '/libs/graficos/progressbar/');
				$this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
				if($this->usuarioAct["idrol"]==2){//docente								
					$this->idexamenUbicacion=363;
					//$this->miscursos=$this->oNegGrupoaulaDetalle->buscar(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));
					$examenUbicacion=$this->oNegPersonasetting->buscar(array('idproyecto'=>$user["idproyecto"],'idpersona'=>$user["idpersona"],'tipo'=>'EU','idrol'=>$user["idrol"]));
						if(!empty($examenUbicacion[0])){
						$dt=json_decode($examenUbicacion[0]["datos"]);
						$fecha=date('Y-m-d');
						if($dt->fecha==$fecha|| strtolower($dt->accion)=='no'|| strtolower($dt->accion)=='si'){
							$this->showExaUbicacion=false;
						}
					} 
					$this->documento->script('inicio_new', '/js/alumno/');
					$this->documento->plantilla = 'alumno/general_new';
					$this->esquema = 'docente/panel';
				}else{ // administrador
					$filtros=array();
				
					if ($this->usuarioAct["idrol"]==1){ // rol administrador
						$this->showExaUbicacion=false;
						$this->esquema = 'docente/inicio';	
						$this->documento->plantilla = 'inicio';
					}elseif($this->usuarioAct["idrol"]==10){ // rol socio
						try{
							global $aplicacion;	
							$this->documento->script('slick.min', '/libs/sliders/slick/');
							$this->documento->stylesheet('slick', '/libs/sliders/slick/');
							$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
							$this->documento->setTitulo(JrTexto::_('Socio'), true);
							$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
							//$this->documento->setTitulo(JrTexto::_('Alumno'), true);
							$this->esquema = 'academico/inicio_socio';
							return parent::getEsquema();
						}catch(Exception $e) {
							return $aplicacion->error(JrTexto::_($e->getMessage()));
						}
						$this->documento->plantilla = 'inicio';
					}else{
						if ($this->usuarioAct["idrol"]==4){
							$aplicacion->redir('viewsincronizacion');
						}else
							$aplicacion->redir('reportes/minedu');
						
					}
				}				
			}
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function vistaConLogrosYAgenda()
	{
		try {
			$this->documento->script('fullcalendar', '/libs/fullcalendar/');
            $this->documento->script('calendar', '/libs/fullcalendar/');
            $this->documento->script('events', '/libs/fullcalendar/');
        	$this->documento->script('moment.min', '/libs/fullcalendar/');
        	$this->documento->stylesheet('fullcalendar', '/libs/fullcalendar/');
        	$this->documento->script('agenda', '/js/alumno/');
			$this->documento->script('inicio', '/js/alumno/');
			$this->agenda_listado=true;			
			$this->agenda = array(
				'hoy' => $this->oNegAgenda->buscar(array('fecha_inicio_mayorigual'=>date('Y-m-d', strtotime("now")), 'fecha_inicio_menor'=>date('Y-m-d', strtotime("+1 day")), 'alumno_id'=>$this->usuarioAct['idpersona'] )),
				'maniana' => $this->oNegAgenda->buscar(array('fecha_inicio_mayorigual'=>date('Y-m-d', strtotime("+1 day")), 'fecha_inicio_menor'=>date('Y-m-d', strtotime("+2 day")), 'alumno_id'=>$this->usuarioAct['idpersona'] )),
			);
			$this->logros= $this->oNegAlumno_logro->buscar(array('id_alumno'=>$this->usuarioAct['idpersona']));
			
			$this->esquema = 'alumno/inicio';
			$this->documento->plantilla = 'alumno/general';
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function vistaConHabilidades()
	{
		try {
			$this->documento->script('inicio_new', '/js/alumno/');
			$this->esquema = 'alumno/inicio_new';
			$this->documento->plantilla = 'alumno/general_new';
		} catch (Exception $e) {
			throw new Exception( JrTexto::_($e->getMessage()) );
		}
	}
	
	private function vistaDiplomadoTICs() /* obedece a BD de Ingenio&Talento */
	{
		try {
			#Cargador de clases e Instanciación
			 JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
        	 $this->oNegAcad_categorias = new NegAcad_categorias;
        	 
        	$search = $this->oNegAcad_categorias->buscarxIDs(array('idcategoria'=>array(14, 13, 16, 5) ));

        	$this->categorias = array();
        	foreach ($search as $elem) {
        		if($elem["idcategoria"]==14) {
        			$this->categorias[0] = $elem;
        		} else if($elem["idcategoria"]==13) {
        			$this->categorias[1] = $elem;
        		}else if($elem["idcategoria"]==16) {
        			$this->categorias[2] = $elem;
        		}else if($elem["idcategoria"]==5) {
        			$this->categorias[3] = $elem;
        		}
        	}
			ksort($this->categorias);

			$this->esquema = 'alumno/inicio_diplomado';
			$this->documento->plantilla = 'alumno/diplomado';
		} catch (Exception $e) {
			throw new Exception( JrTexto::_($e->getMessage()) );
			
		}
	}

}