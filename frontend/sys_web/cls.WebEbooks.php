<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016 
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class WebEbooks extends JrWeb
{
	public function __construct()
	{
		parent::__construct();
		$this->usuarioAct = NegSesion::getUsuario();
		
	}
	public function defecto(){
		return $this->ver();
	}
	
	public function ver(){
		$this->breadcrumb = [
			[ 'texto'=> ucfirst(JrTexto::_('Ebooks')) ],
		];
			$this->documento->plantilla = 'alumno/general';
			$this->documento->setTitulo( ucfirst(JrTexto::_('Ebooks')), true);
			$rol=$this->usuarioAct["idrol"];
			$this->idrol=$rol;			
			$this->esquema = 'libre_tema/ebooks';
			return parent::getEsquema();
	}
}