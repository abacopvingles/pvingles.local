<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017 
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
class WebTarea_respuesta extends JrWeb
{
	private $oNegTarea_respuesta;
	private $oNegTarea_asignacion_alumno;
	public function __construct()
	{
		parent::__construct();
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegTarea_respuesta = new NegTarea_respuesta;
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
	}

	public function defecto(){
		//return $this->listado();
	}

	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['idtarea_asignacion_alumno']) && empty($_POST['actualizarEstadoAsignacion'])){
				throw new Exception(JrTexto::_('No data to insert'));
			}
			if(!empty($_POST['pkIdtarea_respuesta'])){
				$this->oNegTarea_respuesta->idtarea_respuesta = $_POST['pkIdtarea_respuesta'];
			}
			$idgrupo=$this->oNegTarea_asignacion_alumno->buscar(array('iddetalle'=>$_POST['idtarea_asignacion_alumno']));
			$idgrupotarea=0;
			if(!empty($idgrupo[0])){
				$idgrupotarea=$idgrupo[0]["idgrupotarea"];
			}
			$this->oNegTarea_respuesta->__set('idtarea_asignacion_alumno',@$_POST['idtarea_asignacion_alumno']);
			$this->oNegTarea_respuesta->__set('comentario',@$_POST['comentario']);
			$this->oNegTarea_respuesta->__set('fechapresentacion', date('Y-m-d'));
			$this->oNegTarea_respuesta->__set('horapresentacion',date('H:i:s'));
			$this->oNegTarea_respuesta->__set('idpersona',$this->usuarioAct["idpersona"]);
			$this->oNegTarea_respuesta->__set('idgrupotarea',$idgrupotarea);
			if(empty($_POST['pkIdtarea_respuesta'])){
				$idTareaRspta=$this->oNegTarea_respuesta->agregar();
			}else{
				$idTareaRspta=$this->oNegTarea_respuesta->editar();
		    }
		    /*************	Actualizar `tarea_asignacion_alumno`.`estado`	*************/
		    if($_POST['actualizarEstadoAsignacion']=='true'){
		    	$this->oNegTarea_asignacion_alumno->iddetalle=@$_POST['idtarea_asignacion_alumno'];
		    	$this->oNegTarea_asignacion_alumno->__set('estado', 'P');
		    	$resp = $this->oNegTarea_asignacion_alumno->editar();
		    }

			$data=array('code'=>'ok','data'=>$idTareaRspta, 'msj'=>JrTexto::_('Submitted successfully'));
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
	     
}