<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-04-2017 
 * @copyright	Copyright (C) 21-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
class WebGrupo_matricula extends JrWeb
{
	private $oNegGrupo_matricula;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGrupo_matricula = new NegGrupo_matricula;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Grupo_matricula', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegGrupo_matricula->buscar();

			
			$this->fkidalumno=$this->oNegGrupo_matricula->listarno();
			$this->fkidgrupo=$this->oNegGrupo_matricula->listaros();			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupo_matricula'), true);
			$this->esquema = 'grupo_matricula-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Grupo_matricula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Grupo_matricula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Grupo_matricula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegGrupo_matricula->idgrupo_matricula = @$_GET['id'];
			$this->datos = $this->oNegGrupo_matricula->dataGrupo_matricula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Grupo_matricula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegGrupo_matricula->idgrupo_matricula = @$_GET['id'];
			$this->datos = $this->oNegGrupo_matricula->dataGrupo_matricula;
			
			$this->fkidalumno=$this->oNegGrupo_matricula->listarno();
			$this->fkidgrupo=$this->oNegGrupo_matricula->listaros();						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Grupo_matricula').' /'.JrTexto::_('see'), true);
			$this->esquema = 'grupo_matricula-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			$this->fkidalumno=$this->oNegGrupo_matricula->listarno();
			$this->fkidgrupo=$this->oNegGrupo_matricula->listaros();
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'grupo_matricula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function getByAula()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            $filtros=[];
            $alumnos=[];
            $grupos=[];
            $filtros['idambiente']=$_POST['idambiente'];
            $alumnosYgrupos=$this->oNegGrupo_matricula->buscarAlumGrup($filtros);
            foreach ($alumnosYgrupos as $elem) {
            	/***********	Extrayendo alumnos	************/
            	$alum = [
            		'dni'=> $elem['dni'],
            		'nombre'=> $elem['nombre'],
            		'ape_paterno'=> $elem['ape_paterno'],
            		'ape_materno'=> $elem['ape_materno'],
            		'email'=> $elem['email'],
            	];
            	if(!in_array($alum, $alumnos)){
            		$alumnos[]=$alum;
            	}

            	/***********	Extrayendo grupos	************/
            	$gru = [
            		'idgrupo'=> $elem['idgrupo'],
            		'dias'=> $elem['dias'],
            		'horas'=> $elem['horas'],
            		'horainicio'=> $elem['horainicio'],
            		'horafin'=> $elem['horafin'],
            	];
            	if(!in_array($gru, $grupos)){
            		$grupos[]=$gru;
            	}
            }
            $i=0;
            foreach ($grupos as $gr) {
                $grupos[$i]['alumnos'] = $this->oNegGrupo_matricula->buscar(array('idgrupo'=>$gr['idgrupo']));
                $i++;
            }

            $data=array('code'=>'ok','data'=>['alumnos'=>$alumnos,'grupos'=>$grupos]);
            echo json_encode($data);
            return parent::getEsquema();
        } catch(Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

	// ========================== Funciones xajax ========================== //
	public function xSaveGrupo_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdgrupo_matricula'])) {
					$this->oNegGrupo_matricula->idgrupo_matricula = $frm['pkIdgrupo_matricula'];
				}
				
				$this->oNegGrupo_matricula->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegGrupo_matricula->__set('idgrupo',@$frm["txtIdgrupo"]);
					$this->oNegGrupo_matricula->__set('fechamatricula',@$frm["txtFechamatricula"]);
					$this->oNegGrupo_matricula->__set('estado',@$frm["txtEstado"]);
					$this->oNegGrupo_matricula->__set('regusuario',@$frm["txtRegusuario"]);
					$this->oNegGrupo_matricula->__set('regfecha',@$frm["txtRegfecha"]);
					$this->oNegGrupo_matricula->__set('subgrupos',@$frm["txtSubgrupos"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegGrupo_matricula->agregar();
					}else{
									    $res=$this->oNegGrupo_matricula->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegGrupo_matricula->idgrupo_matricula);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDGrupo_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGrupo_matricula->__set('idgrupo_matricula', $pk);
				$this->datos = $this->oNegGrupo_matricula->dataGrupo_matricula;
				$res=$this->oNegGrupo_matricula->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegGrupo_matricula->__set('idgrupo_matricula', $pk);
				$res=$this->oNegGrupo_matricula->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}