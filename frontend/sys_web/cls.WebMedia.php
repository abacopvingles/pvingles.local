<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-02-2018 
 * @copyright	Copyright (C) 09-02-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebMedia extends JrWeb
{			
	public function __construct()
	{
		parent::__construct();	
				
	}

	public function defecto(){
		return 'Error en media';
	}


	public function subir(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $dirmedia=!empty($dirmedia)?$dirmedia:'';
            $oldmedia=!empty($oldmedia)?$oldmedia:'';
			$typefile=!empty($typefile)?$typefile:'';
			$nombre=!empty($nombre)?$nombre:'';          
            $newmedia='';
			if(!empty($_FILES['media']['name'])){
				$file=$_FILES["media"];
				$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
				$escorn=$typefile=='scorm1.2'?true:false;
				if($escorn==true){$typefile='html';}
				if($typefile=='html'&&$ext=='html'){
					$newnombre=$_FILES['media']['name'];
				}else $newnombre=(!empty($nombre)?$nombre:(date("Ymdhis").rand(0,100))).'.'.$ext;
				$dir=RUTA_BASE.'/static' . SD . 'media' . SD .$dirmedia;
				if(!file_exists($dir)){@mkdir($dir, 0777, true);}
				if(move_uploaded_file($file["tmp_name"],$dir. SD.$newnombre)) 
				{	
		  			$rutamedia='/static/media/'.$dirmedia.$newnombre;
		  			$newmedia=URL_BASE.$rutamedia;		  			
		  			if($oldmedia!=''&& $newmedia!=$oldmedia){
		  				$rutaoldmedia=str_replace(URL_BASE,RUTA_BASE,$oldmedia);
		  				if(is_file($rutaoldmedia)) unlink($rutaoldmedia);
		  			}
		  			if($ext=='zip'){
		  				$zip = new ZipArchive;
		  				$fileszip = array();	
						if ($zip->open($dir. SD.$newnombre) === TRUE) 
						{
							$next=(strlen('.'.$ext)*-1);
							$nombresinext=substr($newnombre,0,$next);
							$hayfile=false;
							for($i=0; $i<$zip->numFiles; $i++)
						   	{							
								$namefile=$zip->getNameIndex($i); //obtenemos nombre del fichero
								$rutamedia='/static/media/'.$dirmedia.$nombresinext."/".$namefile;
								$fileszip['tmp_name'][$i] = $rutamedia; //obtenemos ruta que tendrán los documentos cuando los descomprimamos
								$fileszip['name'][$i] = $namefile;
								$extfile=strtolower(pathinfo($namefile, PATHINFO_EXTENSION));
								if($extfile==$typefile && $hayfile==false){
									$hayfile=true;
									$newmedia=URL_BASE.$rutamedia;
								}else if($hayfile==true && in_array(strtolower($namefile), array('index.html','index.htm','default.html','index_scorm.html'))){
									$newmedia=URL_BASE.$rutamedia;	
								}
						   	}
						   	if($hayfile==true){
						   		$zip->extractTo($dir.SD.$nombresinext);
						   		$zip->close();	
						   		@unlink($dir. SD.$newnombre);
						   	}else{
						   		$zip->close();
						   		@unlink($dir. SD.$newnombre);
						   		echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File Zip no valido')));
           						exit(0);
						   	}
						   							
						}
		  			}
		  		}
			}
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Upload successfully')),'media'=>$newmedia,'oldmedia'=>@$rutaoldmedia,'filesdezip'=>@$fileszip)); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
  
}