<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		15-06-2017 
 * @copyright	Copyright (C) 15-06-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
class WebTarea_asignacion extends JrWeb
{
	private $oNegTarea_asignacion;
	private $oNegTarea_asignacion_alumno;
	private $oNegTarea;
	protected $oNegMatricula;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTarea_asignacion = new NegTarea_asignacion;
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
		$this->oNegTarea = new NegTarea;
		$this->oNegMatricula = new NegAcad_matricula;
	}

	public function defecto(){
		//return $this->listado();
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_asignacion', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegTarea_asignacion->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_asignacion'), true);
			$this->esquema = 'tarea_asignacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/
public function buscarjson()
{
	$this->documento->plantilla = 'returnjson';
	try {
		global $aplicacion;
		if(empty($_POST)){
			$data=array('code'=>'Error','msj'=>JrTexto::_('data incompleta'));
			throw new Exception();
		}
		$filtros=array();
		if(!empty($_POST["idtarea_asignacion"])){
			$filtros["idtarea_asignacion"]=$_POST["idtarea_asignacion"];
		}
		if(!empty($filtro)){
			$data=array('code'=>'ok','data'=>array());
		}else {
			$this->datos=$this->oNegTarea_asignacion->buscar($filtros);
			$data=array('code'=>'ok','data'=>$this->datos);
		}
		echo json_encode($data);
	} catch (Exception $e) {
		$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
		echo json_encode($data);
	}
}


public function asignadosjson()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST)){
				$data=array('code'=>'Error','msj'=>JrTexto::_('Data incomplete'));
			}
			if(empty($_POST["idtarea"])||empty($_POST["idgrupoauladetalle"])){
				$data=array('code'=>'Error','msj'=>JrTexto::_('Data incomplete'));
			}
			$this->AlumnoMatriculados=$this->oNegMatricula->buscar(array('idgrupoauladetalle'=>$_POST["idgrupoauladetalle"]));
			$this->AsignacionTarea=$this->oNegTarea_asignacion->buscar(array('idgrupo'=>$_POST["idgrupoauladetalle"],'idtarea'=>$_POST["idtarea"]));
			if(!empty($this->AsignacionTarea[0])){
				$idtareaasignacion=$this->AsignacionTarea[0]["idtarea_asignacion"];
				$this->alumnosasignados=$this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$idtareaasignacion));
			}
			$this->datos=array();
			$dtisnuevo=true;
			if(!empty($this->AlumnoMatriculados))
			foreach($this->AlumnoMatriculados as $alm){
				$dt=array('idalumno'=>@$alm["idalumno"],'stralumno'=>@$alm["stralumno"],'estado'=>'N','iddetalle'=>'');
				//var_dump($this->alumnosasignados);
				if(!empty($this->alumnosasignados)){
						$dt["checked"]=false;
					foreach($this->alumnosasignados as $asi){
						if($alm["idalumno"]==$asi["idalumno"]){
							$dt["iddetalle"]=$asi["iddetalle"];
							$dt["checked"]=true;
							$dt["estado"]=$asi["estado"];
						}
					}
					$dtisnuevo=false;
				}else{
					$dt["checked"]=true;
				}
				$datos[]=$dt;
			}			
			$data=array('code'=>'ok','data'=>$datos,'isnuevo'=>$dtisnuevo);
            echo json_encode($data);
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['arrTarea_asignacion'])){
				throw new Exception(JrTexto::_('No data to insert'));
			}
            $usuarioAct = NegSesion::getUsuario();
			$arrTarea_asignacion = json_decode(@$_POST['arrTarea_asignacion'],true);
			$arrFechas = array();
			if(!empty($arrTarea_asignacion)){
				foreach ($arrTarea_asignacion as $tar_asig) {
					if(!empty($tar_asig['idtarea_asignacion'])){
						$this->oNegTarea_asignacion->idtarea_asignacion = $tar_asig['idtarea_asignacion'];
					}
					$this->oNegTarea_asignacion->__set('idtarea',@$tar_asig['idtarea']);
					$this->oNegTarea_asignacion->__set('iddocente',@$usuarioAct['idpersona']);
					if(!empty(@$tar_asig['idgrupo'])) { $this->oNegTarea_asignacion->__set('idgrupo',@$tar_asig['idgrupo']); }
					$this->oNegTarea_asignacion->__set('fechaentrega',date('Y-m-d', strtotime(@$tar_asig['fechaentrega'])));
					$this->oNegTarea_asignacion->__set('horaentrega',date('H:i:s', strtotime(@$tar_asig['horaentrega'])));
					if(empty($tar_asig['idtarea_asignacion'])){
						$idTareaAsig=$this->oNegTarea_asignacion->agregar();
					}else{					
						$idTareaAsig=$this->oNegTarea_asignacion->editar();
				    }
				    $arrFechas[$idTareaAsig]=array('fechaentrega'=>date('d-m-Y', strtotime(@$tar_asig['fechaentrega'])), 'horaentrega'=>date('h:ia', strtotime(@$tarea['horaentrega'])) );
				    /************** Guardando Tarea_Asignacion_Alumno **************/
					
					if(!empty(@$tar_asig['alumnos'])){
						$arrIdDetalles = array();						
						foreach (@$tar_asig['alumnos'] as $asig_alum){
							if(!empty($asig_alum['iddetalle'])){
								if($asig_alum['registrar']){
									if($asig_alum['estado']=='X')$asig_alum['estado']='N';
									$this->oNegTarea_asignacion_alumno->__set('iddetalle',$asig_alum['iddetalle']);
									$this->oNegTarea_asignacion_alumno->__set('idtarea_asignacion',$idTareaAsig);
									$this->oNegTarea_asignacion_alumno->__set('idalumno',$asig_alum['idalumno']);
									$this->oNegTarea_asignacion_alumno->__set('estado',$asig_alum['estado']);
									$this->oNegTarea_asignacion_alumno->__set('idgrupotarea',"T".$tar_asig['idtarea']."G".$tar_asig['idgrupo']);
									$this->oNegTarea_asignacion_alumno->__set('iddocente',@$usuarioAct['idpersona']);
									$arrIdDetalles[]=$this->oNegTarea_asignacion_alumno->editar();
								}else{
									$this->oNegTarea_asignacion_alumno->__set('iddetalle',$asig_alum['iddetalle']);
									$this->oNegTarea_asignacion_alumno->__set('idtarea_asignacion',$idTareaAsig);
									$this->oNegTarea_asignacion_alumno->__set('idalumno',$asig_alum['idalumno']);
									$this->oNegTarea_asignacion_alumno->__set('estado','X');
									$this->oNegTarea_asignacion_alumno->__set('idgrupotarea',"T".$tar_asig['idtarea']."G".$tar_asig['idgrupo']);
									$this->oNegTarea_asignacion_alumno->__set('iddocente',@$usuarioAct['idpersona']);
									$this->oNegTarea_asignacion_alumno->editar();
								}
								//$this->oNegTarea_asignacion_alumno->eliminar();
							}else if($asig_alum['registrar']){
								$this->oNegTarea_asignacion_alumno->__set('idtarea_asignacion',$idTareaAsig);
								$this->oNegTarea_asignacion_alumno->__set('idalumno',$asig_alum['idalumno']);
								$this->oNegTarea_asignacion_alumno->__set('estado',$asig_alum['estado']);
								$this->oNegTarea_asignacion_alumno->__set('idgrupotarea',"T".$tar_asig['idtarea']."G".$tar_asig['idgrupo']);
								$this->oNegTarea_asignacion_alumno->__set('iddocente',@$usuarioAct['idpersona']);
								$arrIdDetalles[]=$this->oNegTarea_asignacion_alumno->agregar();
							}
						}
					}
					$this->actualizarEstadoTarea(@$tar_asig['idtarea']);
				}
			}
			$data=array('code'=>'ok','data'=>array('idtarea_asignacion'=>$idTareaAsig, 'idtarea_asignacion_alumno'=>@$arrIdDetalles, 'arrFechaHora'=>@$arrFechas ));
            echo json_encode($data);
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	private function actualizarEstadoTarea($idtarea=0)
	{
		try {
			if($idtarea<1){ throw new Exception(JrTexto::_('No id_tarea for update estado field')); }
			$tareas_asignadas = $this->oNegTarea_asignacion->buscar(array('idtarea'=>$idtarea, 'fechahora_mayorigual'=>date('Y-m-d H:i:s'), ));
			$this->oNegTarea->idtarea = $idtarea;
			if(empty($tareas_asignadas)){ $this->oNegTarea->__set('estado', 'NA'); }
			else{ $this->oNegTarea->__set('estado', 'A'); }
			//var_dump($this->oNegTarea);
			$resp=$this->oNegTarea->editar();
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function xEliminarAsignacion()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['idtarea_asignacion'])){
				throw new Exception(JrTexto::_('No data to insert'));
			}
			$pk = @$_POST['idtarea_asignacion'];
			$tar_asig_alum = $this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$pk));
			if(!empty($tar_asig_alum)){
				foreach ($tar_asig_alum as $taa) {
					$this->oNegTarea_asignacion_alumno->__set('iddetalle',$taa['iddetalle']);
					$this->oNegTarea_asignacion_alumno->eliminar();
				}
			}
			$this->oNegTarea_asignacion->__set('idtarea_asignacion', $pk);
			$idTarea = $this->oNegTarea_asignacion->idtarea;
			$res=$this->oNegTarea_asignacion->eliminar();
			$this->actualizarEstadoTarea($idTarea);
			$data=array('code'=>'ok','data'=>$pk );
            echo json_encode($data);
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveTarea_asignacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtarea_asignacion'])) {
					$this->oNegTarea_asignacion->idtarea_asignacion = $frm['pkIdtarea_asignacion'];
				}
				
				$this->oNegTarea_asignacion->__set('idtarea',@$frm["txtIdtarea"]);
				$this->oNegTarea_asignacion->__set('idgrupo',@$frm["txtIdgrupo"]);
				$this->oNegTarea_asignacion->__set('fechaentrega',@$frm["txtFechaentrega"]);
				$this->oNegTarea_asignacion->__set('horaentrega',@$frm["txtHoraentrega"]);
				$this->oNegTarea_asignacion->__set('eliminado',@$frm["txtEliminado"]);
				
			   if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegTarea_asignacion->agregar();
				}else{
					$res=$this->oNegTarea_asignacion->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTarea_asignacion->idtarea_asignacion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTarea_asignacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_asignacion->__set('idtarea_asignacion', $pk);
				$this->datos = $this->oNegTarea_asignacion->dataTarea_asignacion;
				$res=$this->oNegTarea_asignacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_asignacion->__set('idtarea_asignacion', $pk);
				$res=$this->oNegTarea_asignacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}	     
}