<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-01-2019 
 * @copyright	Copyright (C) 29-01-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTest_criterios', RUTA_BASE, 'sys_negocio');
class WebTest_criterios extends JrWeb
{
	private $oNegTest_criterios;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTest_criterios = new NegTest_criterios;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_criterios', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtestcriterio"])&&@$_REQUEST["idtestcriterio"]!='')$filtros["idtestcriterio"]=$_REQUEST["idtestcriterio"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["criterio"])&&@$_REQUEST["criterio"]!='')$filtros["criterio"]=$_REQUEST["criterio"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
			
			$this->datos=$this->oNegTest_criterios->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Test_criterios'), true);
			$this->esquema = 'test_criterios-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_criterios', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Test_criterios').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_criterios', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTest_criterios->idtestcriterio = @$_GET['id'];
			$this->datos = $this->oNegTest_criterios->dataTest_criterios;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Test_criterios').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'test_criterios-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;		
			
			$filtros=array();
			if(isset($_REQUEST["idtestcriterio"])&&@$_REQUEST["idtestcriterio"]!='')$filtros["idtestcriterio"]=$_REQUEST["idtestcriterio"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["criterio"])&&@$_REQUEST["criterio"]!='')$filtros["criterio"]=$_REQUEST["criterio"];
			if(isset($_REQUEST["mostrar"])&&@$_REQUEST["mostrar"]!='')$filtros["mostrar"]=$_REQUEST["mostrar"];
						
			$this->datos=$this->oNegTest_criterios->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarTest_criterios(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idtestcriterio)) {
				$this->oNegTest_criterios->idtestcriterio = $idtestcriterio;
				$accion='_edit';
			}
           //	$usuarioAct = NegSesion::getUsuario();
           	$this->oNegTest_criterios->idtest=@$txtIdtest;
			$this->oNegTest_criterios->criterio=@$txtCriterio;
			$this->oNegTest_criterios->mostrar=@$txtMostrar;
					
            if($accion=='_add') {
            	$res=$this->oNegTest_criterios->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_criterios')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegTest_criterios->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_criterios')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminarTest_criterios(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idtestcriterio)) {
				$this->oNegTest_criterios->idtestcriterio = $idtestcriterio;
				$this->oNegTest_criterios->eliminar();
			}
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_criterios')).' '.JrTexto::_('delete successfully')));           
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}



	
	// ========================== Funciones xajax ========================== //
	public function xSaveTest_criterios(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtestcriterio'])) {
					$this->oNegTest_criterios->idtestcriterio = $frm['pkIdtestcriterio'];
				}
				
				$this->oNegTest_criterios->idtest=@$frm["txtIdtest"];
					$this->oNegTest_criterios->criterio=@$frm["txtCriterio"];
					$this->oNegTest_criterios->mostrar=@$frm["txtMostrar"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegTest_criterios->agregar();
					}else{
									    $res=$this->oNegTest_criterios->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTest_criterios->idtestcriterio);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTest_criterios(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_criterios->__set('idtestcriterio', $pk);
				$this->datos = $this->oNegTest_criterios->dataTest_criterios;
				$res=$this->oNegTest_criterios->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_criterios->__set('idtestcriterio', $pk);
				$res=$this->oNegTest_criterios->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegTest_criterios->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}