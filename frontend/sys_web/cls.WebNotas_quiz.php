<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-09-2018 
 * @copyright	Copyright (C) 16-09-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE, 'sys_negocio');
class WebNotas_quiz extends JrWeb
{
	private $oNegNotas_quiz;
	protected $oNegPersonasetting;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegPersonasetting = new NegPersona_setting;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
			if(isset($_REQUEST["idexamenalumnoquiz"])&&@$_REQUEST["idexamenalumnoquiz"]!='')$filtros["idexamenalumnoquiz"]=$_REQUEST["idexamenalumnoquiz"];
			if(isset($_REQUEST["idexamenproyecto"])&&@$_REQUEST["idexamenproyecto"]!='')$filtros["idexamenproyecto"]=$_REQUEST["idexamenproyecto"];
			if(isset($_REQUEST["idexamenidalumno"])&&@$_REQUEST["idexamenidalumno"]!='')$filtros["idexamenidalumno"]=$_REQUEST["idexamenidalumno"];
			
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
			$this->datos=$this->oNegNotas_quiz->buscar(array('idrecurso' => 115, 'idexamenalumnoquiz' => 2517));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz'), true);
			$this->esquema = 'notas_quiz-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegNotas_quiz->idnota = @$_GET['id'];
			$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Notas_quiz').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'notas_quiz-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Notas_quiz', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idnota"])&&@$_REQUEST["idnota"]!='')$filtros["idnota"]=$_REQUEST["idnota"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["notatexto"])&&@$_REQUEST["notatexto"]!='')$filtros["notatexto"]=$_REQUEST["notatexto"];
			if(isset($_REQUEST["regfecha"])&&@$_REQUEST["regfecha"]!='')$filtros["regfecha"]=$_REQUEST["regfecha"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["calificacion_en"])&&@$_REQUEST["calificacion_en"]!='')$filtros["calificacion_en"]=$_REQUEST["calificacion_en"];
			if(isset($_REQUEST["calificacion_total"])&&@$_REQUEST["calificacion_total"]!='')$filtros["calificacion_total"]=$_REQUEST["calificacion_total"];
			if(isset($_REQUEST["calificacion_min"])&&@$_REQUEST["calificacion_min"]!='')$filtros["calificacion_min"]=$_REQUEST["calificacion_min"];
			if(isset($_REQUEST["tiempo_total"])&&@$_REQUEST["tiempo_total"]!='')$filtros["tiempo_total"]=$_REQUEST["tiempo_total"];
			if(isset($_REQUEST["tiempo_realizado"])&&@$_REQUEST["tiempo_realizado"]!='')$filtros["tiempo_realizado"]=$_REQUEST["tiempo_realizado"];
			if(isset($_REQUEST["calificacion"])&&@$_REQUEST["calificacion"]!='')$filtros["calificacion"]=$_REQUEST["calificacion"];
			if(isset($_REQUEST["habilidades"])&&@$_REQUEST["habilidades"]!='')$filtros["habilidades"]=$_REQUEST["habilidades"];
			if(isset($_REQUEST["habilidad_puntaje"])&&@$_REQUEST["habilidad_puntaje"]!='')$filtros["habilidad_puntaje"]=$_REQUEST["habilidad_puntaje"];
			if(isset($_REQUEST["intento"])&&@$_REQUEST["intento"]!='')$filtros["intento"]=$_REQUEST["intento"];
			if(isset($_REQUEST["idexamenalumnoquiz"])&&@$_REQUEST["idexamenalumnoquiz"]!='')$filtros["idexamenalumnoquiz"]=$_REQUEST["idexamenalumnoquiz"];
			if(isset($_REQUEST["idexamenproyecto"])&&@$_REQUEST["idexamenproyecto"]!='')$filtros["idexamenproyecto"]=$_REQUEST["idexamenproyecto"];
			if(isset($_REQUEST["idexamenidalumno"])&&@$_REQUEST["idexamenidalumno"]!='')$filtros["idexamenidalumno"]=$_REQUEST["idexamenidalumno"];
						
			$this->datos=$this->oNegNotas_quiz->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarNotas_quiz(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idnota)) {
				$this->oNegNotas_quiz->idnota = $idnota;
				$accion='_edit';
			}
			   $usuarioAct = NegSesion::getUsuario();
			   $idalumno=!empty($idalumno)?$idalumno:$usuarioAct["idpersona"];

			   $notaquiz=$this->oNegNotas_quiz->buscar(array('idexamenidalumno'=>@$idexamenidalumno,'idexamenproyecto'=>@$idexamenproyecto,'idalumno'=>$idalumno,'idrecurso'=>@$idrecurso,'idproyecto'=>$usuarioAct["idproyecto"]));
			if(!empty($notaquiz[0])){
				$notaold=$notaquiz[0]["nota"];
				$this->oNegNotas_quiz->idnota=$notaquiz[0]["idnota"];
				if(empty($update)) // cuando viene revicion del docente;				
					if(@$calificacion=='M' && $notaold>$nota){
						echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Usted Tiene una mejor nota'))));
						exit(0);
					}
					else $accion='_edit';
				else $accion='_edit';
			}
			if(@$idrecurso==326||$idrecurso==363){ //examen de ubicacion
				$this->oNegPersonasetting->idproyecto=$usuarioAct["idproyecto"];
				$this->oNegPersonasetting->idpersona=$usuarioAct["idpersona"];
				$this->oNegPersonasetting->idrol=$usuarioAct["idrol"];
				$this->oNegPersonasetting->tipo='EU';
				$this->oNegPersonasetting->datos=json_encode(array('idexamen'=>$idrecurso,'fecha'=>date('Y-m-d'),'accion'=>'si'));
				$this->oNegPersonasetting->agregar();
				$tipo='U';
			}

			$strJson = array();
			$strJson['tipo'] = (isset($_GET['tipexamen'])) ? $_GET['tipexamen'] : @$tipo; 
			$strJson['num_examen'] = (isset($_GET['numexamen'])) ? $_GET['numexamen'] : 0; 

			$preguntas = str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$preguntas);

			
			$this->oNegNotas_quiz->idcursodetalle=!empty($idcursodetalle)?$idcursodetalle:0;
			$this->oNegNotas_quiz->idrecurso=@$idrecurso;
			$this->oNegNotas_quiz->idalumno=$idalumno;
			$this->oNegNotas_quiz->tipo=@$tipo;
			$this->oNegNotas_quiz->nota=@$nota;
			$this->oNegNotas_quiz->notatexto=@$notatexto;
			$this->oNegNotas_quiz->regfecha=!empty($regfecha)?$regfecha:date('Y-m-d');
			$this->oNegNotas_quiz->idproyecto=$usuarioAct["idproyecto"];
			$this->oNegNotas_quiz->calificacion_en=@$calificacion_en;
			$this->oNegNotas_quiz->calificacion_total=@$calificacion_total;
			$this->oNegNotas_quiz->calificacion_min=@$calificacion_min;
			$this->oNegNotas_quiz->tiempo_total=@$tiempo_total;
			$this->oNegNotas_quiz->tiempo_realizado=@$tiempo_realizado;
			$this->oNegNotas_quiz->calificacion=@$calificacion;
			$this->oNegNotas_quiz->habilidades=@$habilidades;
			$this->oNegNotas_quiz->habilidad_puntaje=@$habilidad_puntaje;
			$this->oNegNotas_quiz->intento=@$intento;
			$this->oNegNotas_quiz->idexamenalumnoquiz=@$idexamenalumnoquiz;
			$this->oNegNotas_quiz->idexamenproyecto=@$idexamenproyecto;
			$this->oNegNotas_quiz->idexamenidalumno=@$idexamenidalumno;
			$this->oNegNotas_quiz->datos = json_encode($strJson);
			$this->oNegNotas_quiz->preguntas=@$preguntas;
			$this->oNegNotas_quiz->preguntasoffline=@$preguntasoffline;
            if($accion=='_add') {
            	$res=$this->oNegNotas_quiz->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_quiz')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas_quiz->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Notas_quiz')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnota'])) {
					$this->oNegNotas_quiz->idnota = $frm['pkIdnota'];
				}
				
				$this->oNegNotas_quiz->idcursodetalle=@$frm["txtIdcursodetalle"];
				$this->oNegNotas_quiz->idrecurso=@$frm["txtIdrecurso"];
				$this->oNegNotas_quiz->idalumno=@$frm["txtIdalumno"];
				$this->oNegNotas_quiz->tipo=@$frm["txtTipo"];
				$this->oNegNotas_quiz->nota=@$frm["txtNota"];
				$this->oNegNotas_quiz->notatexto=@$frm["txtNotatexto"];
				$this->oNegNotas_quiz->regfecha=@$frm["txtRegfecha"];
				$this->oNegNotas_quiz->idproyecto=@$frm["txtIdproyecto"];
				$this->oNegNotas_quiz->calificacion_en=@$frm["txtCalificacion_en"];
				$this->oNegNotas_quiz->calificacion_total=@$frm["txtCalificacion_total"];
				$this->oNegNotas_quiz->calificacion_min=@$frm["txtCalificacion_min"];
				$this->oNegNotas_quiz->tiempo_total=@$frm["txtTiempo_total"];
				$this->oNegNotas_quiz->tiempo_realizado=@$frm["txtTiempo_realizado"];
				$this->oNegNotas_quiz->calificacion=@$frm["txtCalificacion"];
				$this->oNegNotas_quiz->habilidades=@$frm["txtHabilidades"];
				$this->oNegNotas_quiz->habilidad_puntaje=@$frm["txtHabilidad_puntaje"];
				$this->oNegNotas_quiz->intento=@$frm["txtIntento"];
				$this->oNegNotas_quiz->idexamenalumnoquiz=@$frm["txtIdexamenalumnoquiz"];
				$this->oNegNotas_quiz->idexamenproyecto=@$frm["txtIdexamenproyecto"];
				$this->oNegNotas_quiz->idexamenidalumno=@$frm["txtIdexamenidalumno"];
				$this->oNegNotas_quiz->preguntasoffline=@$frm["preguntasoffline"];
					
				    if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegNotas_quiz->agregar();
					}else{
						$res=$this->oNegNotas_quiz->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas_quiz->idnota);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas_quiz(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$this->datos = $this->oNegNotas_quiz->dataNotas_quiz;
				$res=$this->oNegNotas_quiz->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas_quiz->__set('idnota', $pk);
				$res=$this->oNegNotas_quiz->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas_quiz->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}