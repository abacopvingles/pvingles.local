<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-01-2018 
 * @copyright	Copyright (C) 06-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAmbiente', RUTA_BASE, 'sys_negocio');
class WebAcad_grupoauladetalle extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegAcad_grupoaula;
	private $oNegAcad_curso;
	private $oNegPersonal;
	private $oNegLocal;
	private $oNegAmbiente;
		
	public function __construct()
	{
	parent::__construct();		
	$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
	$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
	$this->oNegAcad_curso = new NegAcad_curso;
	$this->oNegPersonal = new NegPersonal;
	$this->oNegLocal = new NegLocal;
	$this->oNegAmbiente = new NegAmbiente;
			
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
			if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			
			$this->datos=$this->oNegAcad_grupoauladetalle->buscar($filtros);
			$this->fkidgrupoaula=$this->oNegAcad_grupoaula->buscar();
			$this->fkidcurso=$this->oNegAcad_curso->buscar();
			$this->fkiddocente=$this->oNegPersonal->buscar();
			$this->fkidlocal=$this->oNegLocal->buscar();
			$this->fkidambiente=$this->oNegAmbiente->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('grupoauladetalle'), true);
			$this->esquema = 'acad_grupoauladetalle-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//*if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('grupoauladetalle').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			//*if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = @$_GET['id'];
			$this->datos = $this->oNegAcad_grupoauladetalle->dataAcad_grupoauladetalle;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('grupoauladetalle').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkidgrupoaula=$this->oNegAcad_grupoaula->buscar();
			$this->fkidcurso=$this->oNegAcad_curso->buscar();
			$this->fkiddocente=$this->oNegPersonal->buscar();
			$this->fkidlocal=$this->oNegLocal->buscar();
			$this->fkidambiente=$this->oNegAmbiente->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_grupoauladetalle-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			

			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];

			if(empty($_REQUEST["idproyecto"])){
				$usu=NegSesion::getUsuario();
				$filtros["idproyecto"]=$usu["idproyecto"];
			}
			
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!=''){
				if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
				if(ucfirst($_REQUEST["tipo"])!='V'){
					if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
					if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
				}
			}else{
				if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
				if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			}
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["idsesion"])&&@$_REQUEST["idsesion"]!='')$filtros["idsesion"]=$_REQUEST["idsesion"];
			if(isset($_REQUEST["idgrado"])&&@$_REQUEST["idgrado"]!='')$filtros["idgrado"]=$_REQUEST["idgrado"];
			if(isset($_REQUEST["sql2"])&&@$_REQUEST["sql2"]!='')$filtros["sql2"]=$_REQUEST["sql2"];

			$this->datos=$this->oNegAcad_grupoauladetalle->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function gruposjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();

			//if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			//if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			//if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')
			$usuarioAct = NegSesion::getUsuario();			
			$idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			$filtros["idproyecto"]=$idproyecto;
			if(isset($_REQUEST["anio"])&&@$_REQUEST["anio"]!='')$filtros["anio"]=$_REQUEST["anio"];
			
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!=''){
				if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
				if(ucfirst($_REQUEST["tipo"])!='V'){
					if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];					
				}
			}else{
				if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
				//if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			}
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegAcad_grupoauladetalle->filtrogrupos($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function jxMisAmbientes(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			

			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];

			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='') {
				$filtros["tipo"]=$_REQUEST["tipo"];
				if(ucfirst($_REQUEST["tipo"])!='V'){
					if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
					if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
				}
			}else{
				if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$_REQUEST["idlocal"];
				if(isset($_REQUEST["idambiente"])&&@$_REQUEST["idambiente"]!='')$filtros["idambiente"]=$_REQUEST["idambiente"];
			}
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];

			//var_dump($filtros);
			$datos=$this->oNegAcad_grupoauladetalle->mis_ambientes($filtros);
			echo json_encode(array('code'=>'ok','data'=>$datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarAcad_grupoauladetalle(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
			$accion='_add'; 
			$gpa=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula'=>$txtIdgrupoaula,'idcurso'=>$txtIdcurso));
			if(!empty($gpa[0]))$idgrupoauladetalle=(int)$gpa[0]['idgrupoauladetalle'];
            if(!empty($idgrupoauladetalle)) {
				$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $idgrupoauladetalle;
				$accion='_edit';
			}			
           	$usuarioAct = NegSesion::getUsuario();           	
	        $this->oNegAcad_grupoauladetalle->idgrupoaula=@$txtIdgrupoaula;
			$this->oNegAcad_grupoauladetalle->idcurso=@$txtIdcurso;
			$this->oNegAcad_grupoauladetalle->iddocente=@$txtIddocente;
			$this->oNegAcad_grupoauladetalle->idlocal=!empty($txtIdlocal)?$txtIdlocal:'0';
			$this->oNegAcad_grupoauladetalle->idambiente=!empty($txtIdambiente)?$txtIdlocal:'0';
			$this->oNegAcad_grupoauladetalle->nombre=@$txtNombre;
			$this->oNegAcad_grupoauladetalle->fecha_inicio=!empty($txtFecha_inicio)?$txtFecha_inicio:date('Y-m-d H:i:s');
			$this->oNegAcad_grupoauladetalle->fecha_final=!empty($txtFecha_final)?$txtFecha_final:((date('Y')+1).date('-m-d H:i:s'));
					
            if($accion=='_add') {
            	$res=$this->oNegAcad_grupoauladetalle->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('grupoauladetalle')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_grupoauladetalle->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('grupoauladetalle')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
        }
	}

	public function eliminar_grupoauladetalle(){		
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            if(!empty($idgrupoauladetalle)){
            	$this->oNegAcad_grupoauladetalle->__set('idgrupoauladetalle', $idgrupoauladetalle);
				$res=$this->oNegAcad_grupoauladetalle->eliminar();	
            }elseif(!empty($idgrupoaula)){
            	$this->oNegAcad_grupoaula->__set('idgrupoaula', $idgrupoaula);
				$res=$this->oNegAcad_grupoaula->eliminar();
            }
            
			if(!empty($res))
				 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('grupoauladetalle')).' '.JrTexto::_('Delete record successfully'),'newid'=>$res));
			else{
				 echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('grupoauladetalle')).' '.JrTexto::_('Delete record'),'newid'=>$res));
			}           
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        } 
	}
	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_grupoauladetalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdgrupoauladetalle'])) {
					$this->oNegAcad_grupoauladetalle->idgrupoauladetalle = $frm['pkIdgrupoauladetalle'];
				}
				
				$this->oNegAcad_grupoauladetalle->idgrupoaula=@$frm["txtIdgrupoaula"];
					$this->oNegAcad_grupoauladetalle->idcurso=@$frm["txtIdcurso"];
					$this->oNegAcad_grupoauladetalle->iddocente=@$frm["txtIddocente"];
					$this->oNegAcad_grupoauladetalle->idlocal=@$frm["txtIdlocal"];
					$this->oNegAcad_grupoauladetalle->idambiente=@$frm["txtIdambiente"];
					$this->oNegAcad_grupoauladetalle->nombre=@$frm["txtNombre"];
					$this->oNegAcad_grupoauladetalle->fecha_inicio=@$frm["txtFecha_inicio"];
					$this->oNegAcad_grupoauladetalle->fecha_final=@$frm["txtFecha_final"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_grupoauladetalle->agregar();
					}else{
									    $res=$this->oNegAcad_grupoauladetalle->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_grupoauladetalle->idgrupoauladetalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_grupoauladetalle(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_grupoauladetalle->__set('idgrupoauladetalle', $pk);
				$this->datos = $this->oNegAcad_grupoauladetalle->dataAcad_grupoauladetalle;
				$res=$this->oNegAcad_grupoauladetalle->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_grupoauladetalle->__set('idgrupoauladetalle', $pk);
				$res=$this->oNegAcad_grupoauladetalle->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_grupoauladetalle->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	    
}