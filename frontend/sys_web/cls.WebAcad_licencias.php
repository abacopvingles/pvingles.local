<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-12-2018 
 * @copyright	Copyright (C) 12-12-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_licencias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRoles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebAcad_licencias extends JrWeb
{
	private $oNegAcad_licencias;
	private $oNegEmpresas;
	private $oNegRoles;
	private $ONegPersonal;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_licencias = new NegAcad_licencias;
		$this->oNegEmpresas = new NegBolsa_empresas;
		$this->oNegRoles = new NegRoles;
		$this->oNegPersona_rol = new NegPersona_rol;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idlicencia"])&&@$_REQUEST["idlicencia"]!='')$filtros["idlicencia"]=$_REQUEST["idlicencia"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["costoxrol"])&&@$_REQUEST["costoxrol"]!='')$filtros["costoxrol"]=$_REQUEST["costoxrol"];
			if(isset($_REQUEST["costoxdescarga"])&&@$_REQUEST["costoxdescarga"]!='')$filtros["costoxdescarga"]=$_REQUEST["costoxdescarga"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			
			$this->datos=$this->oNegAcad_licencias->buscar($filtros);
			$this->empresas=$this->oNegEmpresas->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Licencias'), true);
			$this->esquema = 'acad_licencias-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function reporte1(){
		try{
			global $aplicacion;			
		
			$filtros=array();
			if(isset($_REQUEST["idlicencia"])&&@$_REQUEST["idlicencia"]!='')$filtros["idlicencia"]=$_REQUEST["idlicencia"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			$this->datos=$this->oNegAcad_licencias->buscar($filtros);
			$this->roles=$this->oNegRoles->buscar();
			$this->empresas=$this->oNegEmpresas->buscar();
			
			$this->documento->script('Chart.min', '/libs/chartjs/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Licencias'), true);
			$this->esquema = 'acad_licencias-reporte1';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;
			$this->empresas=$this->oNegEmpresas->buscar();
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_licencias').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->empresas=$this->oNegEmpresas->buscar();
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_licencias->idlicencia = @$_GET['id'];
			$this->datos = $this->oNegAcad_licencias->dataAcad_licencias;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_licencias').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;
			$this->roles=$this->oNegRoles->buscar();
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_licencias-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idlicencia"])&&@$_REQUEST["idlicencia"]!='')$filtros["idlicencia"]=$_REQUEST["idlicencia"];
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["costoxrol"])&&@$_REQUEST["costoxrol"]!='')$filtros["costoxrol"]=$_REQUEST["costoxrol"];
			if(isset($_REQUEST["costoxdescarga"])&&@$_REQUEST["costoxdescarga"]!='')$filtros["costoxdescarga"]=$_REQUEST["costoxdescarga"];
			if(isset($_REQUEST["fecha_inicio"])&&@$_REQUEST["fecha_inicio"]!='')$filtros["fecha_inicio"]=$_REQUEST["fecha_inicio"];
			if(isset($_REQUEST["fecha_final"])&&@$_REQUEST["fecha_final"]!='')$filtros["fecha_final"]=$_REQUEST["fecha_final"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegAcad_licencias->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function buscarjson1(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];

			$this->datos=$this->oNegPersona_rol->buscarcantidad($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
	public function getresumen(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$resultado = $this->oNegAcad_licencias->licenciasEmpresas();
			echo json_encode(array('code'=>'ok','data'=>$resultado));
			exit(0);
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
		}
	}

	public function guardarAcad_licencias(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idlicencia)) {
				$this->oNegAcad_licencias->idlicencia = $idlicencia;
				$accion='_edit';
			}else{
				$licencia=$this->oNegAcad_licencias->buscar(array('idempresa'=>$idempresa));
				if(!empty($licencia[0])){
					$this->oNegAcad_licencias->idlicencia = $licencia[0]["idlicencia"];
					$accion='_edit';
				}
			}
           	$usuarioAct = NegSesion::getUsuario();
           	$this->oNegAcad_licencias->idempresa=@$idempresa;
			$this->oNegAcad_licencias->costoxrol=@$costoxrol;
			$this->oNegAcad_licencias->costoxdescarga=@$costoxdescarga;
			$this->oNegAcad_licencias->fecha_inicio=@$fecha_inicio;
			$this->oNegAcad_licencias->fecha_final=@$fecha_final;
			$this->oNegAcad_licencias->estado=!empty($estado)?$estado:1;
				
            if($accion=='_add') {
            	$res=$this->oNegAcad_licencias->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Licencias')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_licencias->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Licencias')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_licencias(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdlicencia'])) {
					$this->oNegAcad_licencias->idlicencia = $frm['pkIdlicencia'];
				}
				
				$this->oNegAcad_licencias->idempresa=@$frm["txtIdempresa"];
					$this->oNegAcad_licencias->costoxrol=@$frm["txtCostoxrol"];
					$this->oNegAcad_licencias->costoxdescarga=@$frm["txtCostoxdescarga"];
					$this->oNegAcad_licencias->fecha_inicio=@$frm["txtFecha_inicio"];
					$this->oNegAcad_licencias->fecha_final=@$frm["txtFecha_final"];
					$this->oNegAcad_licencias->estado=@$frm["txtEstado"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_licencias->agregar();
					}else{
									    $res=$this->oNegAcad_licencias->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_licencias->idlicencia);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_licencias(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_licencias->__set('idlicencia', $pk);
				$this->datos = $this->oNegAcad_licencias->dataAcad_licencias;
				$res=$this->oNegAcad_licencias->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_licencias->__set('idlicencia', $pk);
				$res=$this->oNegAcad_licencias->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_licencias->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}