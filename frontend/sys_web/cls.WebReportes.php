<?php
set_time_limit(0);
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
//JrCargador::clase('sys_negocio::NegLinks', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_setting', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre',RUTA_BASE,'sys_negocio');
JrCargador::clase('sys_negocio::NegResumen', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_competencias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_unidad_capacidad', RUTA_BASE, 'sys_negocio');
class WebReportes extends JrWeb
{
	private $oNegAcad_grupoauladetalle;
	private $oNegAcad_grupoaula;
	private $oNegAcad_matricula;
	private $oNegAcad_cursodetalle;
	private $oNegNotas_quiz;
	private $oNegAcad_curso;
	private $oNegPersona_setting;
	private $oNegPersonal;
	private $oNegHistorial_sesion;
	private $oNegActividad_alumno;
	private $oNegActividad;
	private $oNegMinedu;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegUgel;
	private $oNegDre;
	private $oNegResumen;
	private $oNegCompetencias;
	private $oNegMin_unidad_capacidad;
	//private $oNegLinks;

	public function __construct()
	{
		parent::__construct();		
	//	$this->oNegLinks = new NegLinks;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegNotas_quiz = new NegNotas_quiz;
		$this->oNegReportealumno = new NegReportealumno;
		$this->oNegMatricula = new NegAcad_matricula;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegPersona_setting = new NegPersona_setting;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegHistorial_sesion = new NegHistorial_sesion;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad = new NegActividad;
		$this->oNegMinedu = new NegMinedu;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegUgel = new NegUgel;
		$this->oNegResumen = new NegResumen;
		$this->oNegDre = new NegMin_dre;
		$this->oNegCompetencias = new NegMin_competencias;
		$this->oNegMin_unidad_capacidad = new NegMin_unidad_capacidad;
		$this->usuarioAct = NegSesion::getUsuario();

	}

	public function defecto(){
		return $this->listado();
	}

	public function listado(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Reports'), true);
        $this->esquema = 'reportes/reportes_list';
        return parent::getEsquema();
	}

	public function versetting(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Reports'), true);
        $this->esquema = 'reportes/reportes_list';
        return parent::getEsquema();
	}
	public function ubicacion(){
		try{
			global $aplicacion;
			$this->examen = 0;
			$this->user = NegSesion::getUsuario();
			
			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			
			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
			}

			$filtro = array(
				'idpersona'=>$this->user['idpersona'],
				'idrol' => $this->user['idrol'],
				'tipo' => 'EU',
				'idproyecto' =>$this->user['idproyecto']
			);

			$this->setting = $this->oNegPersona_setting->buscar($filtro);
			if(!empty($this->setting)){
				$datos = json_decode($this->setting[0]['datos'],true);
				if($datos['accion'] == 'si'){
					$examen = $this->oNegNotas_quiz->buscar(array('idalumno' => $this->user['idpersona'],'idrecurso' => $datos['idexamen'], 'tipo' => 'U'));
					if(isset($examen[0])){
						$nombreNivel = 'A1';
						//calcular la calificacion final acuerdo del total del nivel obtenido
						$_json = json_decode($examen[0]['calificacion_total']);
						$notaInt = intval($examen[0]['nota']);
						$max = 20;
						foreach($_json as $value){
							$max = intval($value->max);
							$min = intval($value->min);
							if($notaInt >= $min && $notaInt <= $max){
								$nombreNivel = $value->nombre;
								break;
							}
						}
						$totalxNivel = $max;
						$calculo = ( ($examen[0]['nota'] * 100) / $totalxNivel ) * 0.20;
						//Ubicar nuevos valores para enviar a la vista
						$examen[0]['nivelUbicado'] = $nombreNivel;
						$examen[0]['notaFinal'] = intval(floor($calculo)); //round($calculo,2);
						$this->examen = $examen[0];
					}//end if isset examen
				}

			}//endif empty
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			/*
			var_dump($this->miscolegios[0]['cursos']);
			var_dump($this->miscolegios[0]['cursos'][0]['grados']);
			var_dump($this->miscolegios[0]['cursos'][0]['grados'][0]['secciones']);
			*/
			

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$this->documento->setTitulo(JrTexto::_('Reports'), true);
	        $this->esquema = 'reportes/ubicacion';
	        return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function tiempo(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			
			if(!isset($_REQUEST['isIframe'])){
				$this->cursos = $this->oNegAcad_grupoauladetalle->cursosDocente(array('iddocente'=>$this->user['idpersona'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));
			}

			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];

			$this->lista_tiempo = null;
			$this->lista_tiempo=$this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'P')
			));


			$this->tareas = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'lugar' => 'T')
			));
			$this->actividades = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'lugar' => 'A')
			));
			$this->smartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'lugar' => 'TR')
			));
			$this->games = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'lugar' => 'G')
			));
			$this->examenes = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
				'idusuario' => $idalumno,
				'lugar' => 'E')
			));
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->esquema = 'reportes/tiempopv';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reporte de Tiempo en la Plataforma Virtual'), true);
			return parent::getEsquema();

		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function setUltimavez(&$contenedor,$row,$idalumno,$letter){
		if(isset($row)){
			$contenedor[$letter]['fecha'] = 0;
			$contenedor[$letter]['tiempo'] = 0;
			if($valor = $this->oNegHistorial_sesion->tiempo_fecha_ingreso(array('idusuario' => $idalumno, 'lugar' => $letter,'fechaentrada' => $row)) ){
  
			  $contenedor[$letter]['fecha'] = $row;
			  $contenedor[$letter]['tiempo'] = round($valor[0]['tiempo'] / 3600, 2);
			}//valor
		}
	}
	public function usodominio(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'] ;
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = $sqlName[0]['foto'];
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			//$this->cursos = $this->oNegAcad_grupoauladetalle->cursosDocente(array('iddocente'=>$this->user['idpersona'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));
			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			
			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
			}


			/*
				Plataforma Virtual 20 horas

				SmartBook 10 horas
				HomeWork 5 horas
				Activity 10 horas
				SmartQuiz 10 horas
			*/

			$fechas = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'P'));
			$fechasplataforma = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'P',
					'idproyecto' => $idproyecto)
				));;
			$fechassmartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'TR',
					'idproyecto' => $idproyecto)
				));
			$fechaspractice = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'A',
					'idproyecto' => $idproyecto)
				));
			$fechassmarquiz = $this->examenes = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'E',
					'idproyecto' => $idproyecto)
				));
			$fechashomework = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
					'idusuario' => $idalumno,
					'lugar' => 'T',
					'idproyecto' => $idproyecto)
				));

			$hrplataforma = (!empty($fechasplataforma)) ? $fechasplataforma[0]['tiempo'] : 0;
			$hrsmartbook = (!empty($fechassmartbook)) ? $fechassmartbook[0]['tiempo'] : 0;
			$hrpractice = (!empty($fechaspractice)) ? $fechaspractice[0]['tiempo'] : 0;
			$hrsmarquiz = (!empty($fechassmarquiz)) ? $fechassmarquiz[0]['tiempo'] : 0;
			$hrhomework = (!empty($fechashomework)) ? $fechashomework[0]['tiempo'] : 0;

			$hrplataforma = ($hrplataforma != 0) ? (100 * ($hrplataforma / 3600)) / 20 : 0;
			$hrsmartbook = ($hrsmartbook != 0) ? (100 * ($hrsmartbook / 3600)) / 10 : 0;
			$hrpractice = ($hrpractice != 0) ? (100 * ($hrpractice / 3600)) / 10 : 0;
			$hrsmarquiz = ($hrsmarquiz != 0) ? (100 * ($hrsmarquiz / 3600)) / 10 : 0;
			$hrhomework = ($hrhomework != 0) ? (100 * ($hrhomework / 3600)) / 5 : 0;

			$hrplataforma = ($hrplataforma > 100) ? 100 : $hrplataforma;
			$hrsmartbook = ($hrsmartbook > 100) ? 100 : $hrsmartbook;
			$hrpractice = ($hrpractice > 100) ? 100 : $hrpractice;
			$hrsmarquiz = ($hrsmarquiz > 100) ? 100 : $hrsmarquiz;
			$hrhomework = ($hrhomework > 100) ? 100 : $hrhomework;

			$sumaHr = $hrplataforma + $hrsmartbook + $hrpractice + $hrsmarquiz + $hrhomework;
			$dominio = (100 * $sumaHr) / 500;
			$Uso = array(
				'dominio' => $dominio,
				'smartbook' =>$hrsmartbook,
				'activity' =>$hrpractice,
				'smarquiz' =>$hrsmarquiz,
				'homework' =>$hrhomework,
			);
			$fechas_smartbook = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'TR'));
			$fechas_smartask = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'T'));
			$fechas_smarquiz = $this->oNegHistorial_sesion->fechas_de_ingreso(array('idusuario' => $idalumno, 'lugar' => 'E'));
			$tiempos_utlimavez = array();
			if(!empty($fechas_smartbook)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smartbook[0]['fecha'],$idalumno,'TR');
			}
			if(!empty($fechas_smartask)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smartask[0]['fecha'],$idalumno,'T');
			}
			if(!empty($fechas_smarquiz)){
				$this->setUltimavez($tiempos_utlimavez,$fechas_smarquiz[0]['fecha'],$idalumno,'E');
			}
			$tiempos = array();
			foreach($fechas as $value){
				if(isset($value['fecha'])){
					if($valor = $this->oNegHistorial_sesion->tiempo_fecha_ingreso(array('idusuario' => $idalumno, 'lugar' => 'P','fechaentrada' => $value['fecha'])) ){
						$tiempos[$value['fecha']] = round($valor[0]['tiempo'] / 3600, 2);
					}//valor
				}
			}//endforeach
			if(!empty($tiempos) && !empty($fechas)){
				$tiempos_utlimavez['P']['fecha'] = $fechas[0]['fecha'];
				$tiempos_utlimavez['P']['tiempo'] = $tiempos[$fechas[0]['fecha']];
			}
			$_examen = array();
			$examen_docente = $this->oNegNotas_quiz->buscar(array('idalumno' => $this->user['idpersona'],'idrecurso' => 373, 'tipo' => 'E'));
			if(!empty($examen_docente)){
				$_examen['nota']= ($examen_docente[0]['calificacion_en'] == 'N') ? ((100 / 20) * $examen_docente[0]['nota']) :  $examen_docente[0]['nota'];
				$_examen['notatexto']= $examen_docente[0]['notatexto'];
				$json_habilidades = json_decode($examen_docente[0]['habilidades'],true);
				$json_habilidades_datos = json_decode($examen_docente[0]['habilidad_puntaje'],true);
				$infocurso = array();
				$infocursoname = array();
				foreach($json_habilidades as $key => $value){					
					$infocursoname[] = (isset($value['skill_name'])) ? $value['skill_name'] : 0 ;
					$infocurso[] = (isset($json_habilidades_datos[$value['skill_id']])) ? $json_habilidades_datos[$value['skill_id']] : 0 ;
				}
				$_examen['habilidades'] = $infocursoname;
				$_examen['habilidades_puntaje'] = $infocurso;
				$_examen['resultado'] = $examen_docente[0]['preguntas'];
			}

			$this->examen = $_examen;
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			$this->fechas = $fechas;
			$this->tiempos = $tiempos;
			$this->dominio = $Uso;
			$this->ultimavez = $tiempos_utlimavez;

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Reports -- SmartEnglish'), true);
			$this->esquema = 'reportes/usodominio';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function entrada(){
		try{
			global $aplicacion;
			$this->entrada = true;
			$this->user = NegSesion::getUsuario();
			$this->cursos = array();
			
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));

			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
				
			}

			// var_dump($this->cursos);
			// $datos= $this->oNegAcad_cursodetalle->sesiones('31',0);
			// var_dump($datos);
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			// $this->cursos = $this->oNegAcad_grupoauladetalle->cursosDocente(array('iddocente'=>$this->user['idpersona'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));
			// $this->cursosMatriculado = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->user['dni'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));;

			$datos = null;
			$TotalEntrada = null;
			$habilidad_sum = array();
			
			foreach($this->cursos as $key => $value){
				$habilidad_sum[$value['idcurso']]['4'] = 0;
				$habilidad_sum[$value['idcurso']]['5'] = 0;
				$habilidad_sum[$value['idcurso']]['6'] = 0;
				$habilidad_sum[$value['idcurso']]['7'] = 0;
				$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
				foreach($datos as $keyU => $unity){
					$sum = 0;
					if(isset($unity['idrecurso'])){
						if($unity['tiporecurso'] == 'E'){
							$jsonSesion = json_decode($unity['txtjson'],true);
							if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'){
								if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $this->user['idpersona'],'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
									foreach($valor as $value2){
										$notaConvert = $value2['nota'];
										if($value2['calificacion_en'] == 'N'){
											$notaConvert = ((100/20) * $value2['nota']);
										}
										$sum += $notaConvert;
										if(isset($value2['habilidad_puntaje'])){
											$json = json_decode($value2['habilidad_puntaje'],true);
											foreach($json as $k => $v){
												if(intval($k) <= 7){
													$habilidad_sum[$value['idcurso']][$k] += $v;											
												}
											}
										}
									}
								}//end if valorr

							}//end if jsonsesion
						}
					}
					$TotalEntrada[$value['idcurso']]['E'][] = $sum;
				}
			}

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->Total = $TotalEntrada;
			$this->TotalHab = $habilidad_sum;

			$this->documento->setTitulo(JrTexto::_('Reports'), true);
	        $this->esquema = 'reportes/reporte_examene';
	        return parent::getEsquema();
		}
		catch(Exception $e){
      		return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function salida(){
		try{
			global $aplicacion;
			$this->entrada = false;
			$this->user = NegSesion::getUsuario();

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->cursos = $this->oNegAcad_grupoauladetalle->cursosDocente(array('iddocente'=>$this->user['idpersona'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));
			// $this->cursosMatriculado = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->user['dni'], 'estado'=>1, "idproyecto"=>@$this->user["idproyecto"]));;
			
			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			
			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
			}

			$datos = null;
			$TotalEntrada = null;
			$habilidad_sum = array();
			
			foreach($this->cursos as $key => $value){
				$habilidad_sum[$value['idcurso']]['4'] = 0;
				$habilidad_sum[$value['idcurso']]['5'] = 0;
				$habilidad_sum[$value['idcurso']]['6'] = 0;
				$habilidad_sum[$value['idcurso']]['7'] = 0;
				$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
				foreach($datos as $keyU => $unity){
					$sum = 0;
					if(isset($unity['idrecurso'])){
						if($unity['tiporecurso'] == 'E'){
							$jsonSesion = json_decode($unity['txtjson'],true);
							if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'){
								if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $this->user['idpersona'],'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
									foreach($valor as $value2){
										$notaConvert = $value2['nota'];
										if($value2['calificacion_en'] == 'N'){
											$notaConvert = ((100/20) * $value2['nota']);
										}
										$sum += $notaConvert;
										if(isset($value2['habilidad_puntaje'])){
											$json = json_decode($value2['habilidad_puntaje'],true);
											foreach($json as $k => $v){
												if(intval($k) <= 7){
													$habilidad_sum[$value['idcurso']][$k] += $v;											
												}
											}
										}
									}
								}

							}
						}
					}
					$TotalEntrada[$value['idcurso']]['S'][] = $sum;
				}
			}
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			$this->Total = $TotalEntrada;
			$this->TotalHab = $habilidad_sum;

			$this->documento->setTitulo(JrTexto::_('Reports'), true);
	        $this->esquema = 'reportes/reporte_examene';
	        return parent::getEsquema();
		}
		catch(Exception $e){
      		return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function comparativo(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$isentrada = (isset($_REQUEST['entrada'])) ? boolval($_REQUEST['entrada']) : true;
			$this->entrada = $isentrada;
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			
			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
			}

			$datos = null;
			$datos_examen_entrada = null;

			$TotalEntrada = null;
			//Obtener todas las habilidades de la bd
			$habilidades = $this->oNegReportealumno->buscar(array('tipo'=>'lista_habilidades'));
			$habilidad_sum = array();
			$habilidad_sum_contrario = array();

			$infoentrada = array();
			$infosalida = array();
			// 4 Listening 5 reading 6 writing 7 speaking
			$letter = ($isentrada == true) ? 'E': 'S';
			$letterContra = ($isentrada == true) ? 'S': 'E';
			
			foreach($this->cursos as $key => $value){
				// if($key == 1){
					//inicializar sumatoria de habilidades por curso
					$habilidad_sum[$value['idcurso']]['4'] = 0;
					$habilidad_sum[$value['idcurso']]['5'] = 0;
					$habilidad_sum[$value['idcurso']]['6'] = 0;
					$habilidad_sum[$value['idcurso']]['7'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['4'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['5'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['6'] = 0;
					$habilidad_sum_contrario[$value['idcurso']]['7'] = 0;
					//Buscar las unidades del curso...

					$datos= $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
					foreach($datos as $keyU => $unity){
						//$datos[$keyU]['tiporecurso'] == 'E' 
						$sum = 0;
						$sum2 = 0;
						//recorrer las unidades para encontrar los examenes
						if(isset($unity['idrecurso'])){
							// foreach($datos[$keyU]['hijo'] as $hijo){
								if($unity['tiporecurso'] == 'E'){
									$jsonSesion = json_decode($unity['txtjson'],true);
									if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'E'){
										if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
											//verificar si solo hay una fila en el resutlado de la consulta
											//recorrer varias notas obtenidas
											foreach($valor as $value2){
												$notaConvert = $value2['nota'];
												if($value2['calificacion_en'] == 'N'){
													$notaConvert = ((100/20) * $value2['nota']);
												}
												$sum += $notaConvert;
												$infoentrada[$value['idcurso']] = $value2['regfecha'];
												if(isset($value2['habilidad_puntaje'])){
													$json = json_decode($value2['habilidad_puntaje'],true);
													foreach($json as $k => $v){
														if(intval($k) <= 7){
															$habilidad_sum[$value['idcurso']][$k] += $v;											
														}
													}
												}
											}//endforeach recorrer notas
	
										}//End if valor
									}//end condition !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
									if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == 'S'){
										//Obtener las habilidades del contrario
										if( $valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
											//verificar si solo hay una fila en el resutlado de la consulta
											//recorrer varias notas obtenidas
											foreach($valor as $value2){
												$sum2 = $value2['nota'];
												$notaConvert2 = $value2['nota'];
												if($value2['calificacion_en'] == 'N'){
													$notaConvert2 = ((100/20) * $value2['nota']);
												}
												$sum2 = $notaConvert2;
												$infosalida[$value['idcurso']] = $value2['regfecha'];
												if(isset($value2['habilidad_puntaje'])){
													$json = json_decode($value2['habilidad_puntaje'],true);
													foreach($json as $k => $v){
														if(intval($k) <= 7){
															$habilidad_sum_contrario[$value['idcurso']][$k] += $v;											
														}
													}
												}
											}//endforeach recorrer notas
	
										}//End if valor
									}//end condition !empty($jsonSesion) && strtoupper($jsonSesion['tipo'])
								} //end if condicion del hijo == E
								
							// }//end foreach recorrida hijos
						}//end if check si esta definido $datos[$keyU]['hijo']
						$TotalEntrada[$value['idcurso']]['E'][] = $sum;
						$TotalEntrada[$value['idcurso']]['S'][] = $sum2;

					}

				// }//end key filter
				
			}//End foreach cursos
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			$this->TotalJSON = json_encode($TotalEntrada);
			$this->Total = $TotalEntrada;
			$this->infoEntrada = $infoentrada;
	  		$this->infoSalida = $infosalida;
			$this->TotalHab = $habilidad_sum;
			$this->TotalHabContrario = $habilidad_sum_contrario;
			
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"), true);
			$this->esquema = 'reportes/comparativo';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function getBimestreTrimestre($idcurso,$idalumno,$datos,&$sumBimestre ,&$sumBimestre_habilidad,&$sumBimestre_habilidad_total,$esBimestre = true){
		$resultado = false;
		try{
			$letter = ($esBimestre == true) ? 'B' : 'T';
			foreach($datos as $keyU => $unity){
				//verificar si es examen en la unidad
				if($unity['tiporecurso'] == 'E'){
					$jsonSesion = json_decode($unity['txtjson'],true);
					if(!empty($jsonSesion) && strtoupper($jsonSesion['tipo']) == $letter){
						if($valor = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno,'idrecurso' => $unity['idrecurso'], 'tipo' => 'E')) ){
							$jsonDatos = json_decode($valor[0]['datos'],true);
							if(!empty($jsonDatos)){
								$index = strval($jsonDatos['num_examen']);
								if(!empty($index)){
									if(isset($sumBimestre[$idcurso][$index])){
										$convert = $valor[0]['nota'];
										if($valor[0]['calificacion_en'] == 'N'){
											$convert = ((100/20) * $valor[0]['nota']);										
										}
										$sumBimestre[$idcurso][$index] += $convert;
										//resultado por habilidad de cada examen bimestral
										if(isset($valor[0]['habilidad_puntaje'])){
											$jsonHabilidades = json_decode($valor[0]['habilidad_puntaje'],true);
											foreach($jsonHabilidades as $k => $v){
												if(intval($k) <= 7){
													if(isset($sumBimestre_habilidad[$idcurso][$index][$k]) && !empty($sumBimestre_habilidad[$idcurso][$index][$k])){
														
														$sumBimestre_habilidad[$idcurso][$index][$k] += $v; //total por bimestre y habilidad
													}else{
														$sumBimestre_habilidad[$idcurso][$index][$k] = $v; //total por bimestre y habilidad
													}
													$sumBimestre_habilidad_total[$idcurso][$k] += $v; //total por habilidad
												}//end if check if habilidad is true
											}//endforeach
										}//end if isset
									}

								}//end if empty index
							}//end empty
						}//busqueda de recurso
					}
				}//end condition
				$resultado = true;
			}//end foreach
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
		return $resultado;
	}

	public function examenes(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));

			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
				// if($cur["idcurso"]!=31)	
			}

			// Mostrar/Seleccionar los cursos asignado en matricula
			//buscar los examenes del alumno segun su id, filtrar de tipo B y T para mostrar
			$examenes_bimestre = $this->oNegPersonal->buscar(array(
				"idalumno" => $idalumno
				,"tipoJson" => '"tipo":"B"'
				,"idproyecto" => $this->user["idproyecto"]
			));
			$examenes_trimestre = $this->oNegPersonal->buscar(array(
				"idalumno" => $idalumno
				,"tipoJson" => '"tipo":"T"'
				,"idproyecto" => $this->user["idproyecto"]
			));

			$sumBimestre = array();
			$sumTrimestre = array();
			$sumBimestre_habilidad = array();
			$sumTrimestre_habilidad = array();
			$sumBimestre_habilidad_total = array();
			$_sumBimestre_habilidad_total = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
			$sumTrimestre_habilidad_total = array();
			$_sumTrimestre_habilidad_total = array('4' => 0, '5' => 0, '6' => 0, '7' => 0);
			if(!empty($this->cursos)){
				foreach ($this->cursos as $key => $val) {
					for($c = 1; $c <= 4; $c++){
						$sumBimestre[$val['idcurso']][strval($c)] = 0;
					}
					for($c = 1; $c <= 3; $c++){
						$sumTrimestre[$val['idcurso']][strval($c)] = 0;
					}
					$sumBimestre_habilidad_total[$val['idcurso']] = $_sumBimestre_habilidad_total;
					$sumTrimestre_habilidad_total[$val['idcurso']] = $_sumTrimestre_habilidad_total;
					//obtiene datos de la sesion por el curso..
					$datos= $this->oNegAcad_cursodetalle->sesiones($val['idcurso'],0);
					$result_b = $this->getBimestreTrimestre($val['idcurso'],$idalumno,$datos,$sumBimestre,$sumBimestre_habilidad,$sumBimestre_habilidad_total); //Bimestre
					$result_t = $this->getBimestreTrimestre($val['idcurso'],$idalumno,$datos,$sumTrimestre,$sumTrimestre_habilidad,$sumTrimestre_habilidad_total,false); //Trimestre

				}
			}//check if not is empty cursos

			if(empty($sumBimestre_habilidad)){
				if(!empty($this->cursos)){
					foreach ($this->cursos as $key => $val) {
						for($i = 1; $i <= 4; $i++){
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][4] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][5] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][6] = 0;
							$sumBimestre_habilidad[$val['idcurso']][strval($i)][7] = 0;
						}
					}
				}
			}
			if(empty($sumTrimestre_habilidad)){
				if(!empty($this->cursos)){
					foreach ($this->cursos as $key => $val) {
						for($i = 1; $i <= 3; $i++){
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][4] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][5] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][6] = 0;
							$sumTrimestre_habilidad[$val['idcurso']][strval($i)][7] = 0;
						}
					}
				}
			}
			//limitar solamente hasta 100%
			foreach($sumBimestre_habilidad_total as $k => $habilidad){
				foreach($habilidad as $key => $value){
					if($value > 100){
						$sumBimestre_habilidad_total[$k][$key] = ($value / ((ceil($value / 100)) * 100)) * 100;
					}
				}//endforeach
			}
			foreach($sumTrimestre_habilidad_total as $k => $habilidad){
				foreach($habilidad as $key => $value){
					if($value > 100){
						$sumTrimestre_habilidad_total[$k][$key] = ($value / ((ceil($value / 100)) * 100)) * 100;
					}
				}//endforeach
			}

			//bimestre
			$this->sumBimestre = $sumBimestre;
			$this->sumBimestre_habilidad = $sumBimestre_habilidad;
			$this->sumBimestre_habilidad_total = $sumBimestre_habilidad_total;
			
			//trimestre
			$this->sumTrimestre = $sumTrimestre;
			$this->sumTrimestre_habilidad = $sumTrimestre_habilidad;
			$this->sumTrimestre_habilidad_total = $sumTrimestre_habilidad_total;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"), true);
			$this->esquema = 'reportes/examenes';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function progreso(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			
			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));

			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
			}

			$progresos = array();

			if(!empty($this->cursos)){
				foreach ($this->cursos as $key => $value) {
					$progresos[$value['idcurso']] = null;
					$temas = $this->oNegAcad_cursodetalle->sesiones($value['idcurso'],0);
					foreach($temas as $tem){
					$tem["progreso"]=0;
					$tem["nactividad"]=0;
					
					if( $tem['tiporecurso'] != 'E'){
						$tem=$this->oNegAcad_curso->getProgresoUnidad($tem,$value['idcurso'],$idalumno,false);
						$progresos[$value['idcurso']][] = $tem;
					}//endif
				}//endforeach

				}//endforeach cursos
			}
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			$this->progresos = $progresos;

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/progreso';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function obtenerequivalente($v,$max,$isInteger = true){
		$r = 0;
		if($v != 0){
			$r = ($isInteger == true) ? intval(($v * 100) / $max) : ($v * 100) / $max;
		}
		return ($r > 100) ? 100 : $r;
	}
	public function progreso_habilidad(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
			$this->datos_perfil = $this->oNegPersonal->dataPersonal;
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));

			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
			}

			$datos = array();
			$habilidad_total = array();
			$habilidad_bimestre_all = array();
			$habilidad_trimestre_all = array();

			if(!empty($this->cursos)){
				//buscar total de las habilidades en tabla resumen
				$resultadoTotal = $this->oNegMinedu->progresosresumen(array('idalumno' => $idalumno));
				//constantes
				$porciento_bimestre = 100/4; $porciento_trimestre = 100/3;
				foreach($this->cursos as $key => $value){
					$habilidad_total[$value['idcurso']] = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
					$habilidad_bimestre_all[$value['idcurso']] = array('1' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'2' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'3' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'4' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0));
					$habilidad_trimestre_all[$value['idcurso']] = array('1' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'2' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0),'3' => array('4' =>0,'5' =>0,'6' =>0,'7' =>0));

					if(!empty($resultadoTotal) && isset($resultadoTotal[0])){
						$habilidad_total[$value['idcurso']] = array('4'=>$resultadoTotal[0]['prog_hab_L_'.$value['nombre']],'5'=>$resultadoTotal[0]['prog_hab_R_'.$value['nombre']],'6'=>$resultadoTotal[0]['prog_hab_W_'.$value['nombre']],'7'=>$resultadoTotal[0]['prog_hab_S_'.$value['nombre']]);
						$habilidad_bimestre_all[$value['idcurso']] = array(
							'1' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre)),
							'2' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 2),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 2),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 2),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 2)),
							'3' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 3),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 3),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 3),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 3)),
							'4' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_bimestre * 4),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_bimestre * 4),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_bimestre * 4),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_bimestre * 4))
						);
						$habilidad_trimestre_all[$value['idcurso']] = array(
							'1' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre)),
							'2' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre * 2),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre * 2),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre * 2),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre * 2)),
							'3' => array('4' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['4'],$porciento_trimestre * 3),'5' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['5'],$porciento_trimestre * 3),'6' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['6'],$porciento_trimestre * 3),'7' =>$this->obtenerequivalente($habilidad_total[$value['idcurso']]['7'],$porciento_trimestre * 3))
						);
					}

				}//endforeach
			}
			// var_dump($habilidad_bimestre_all);
			// var_dump($habilidad_trimestre_all);
			// var_dump($habilidad_total);
			
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->habilidad_bimestre_all = $habilidad_bimestre_all;
			$this->habilidad_trimestre_all = $habilidad_trimestre_all;
			$this->habilidad_total = $habilidad_total;

			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/progresoxhabilidad';
			
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function progreso_competencia(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
			$this->datos_perfil = $this->oNegPersonal->dataPersonal;
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			
			$this->cursos = array();
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			foreach($cursosProyecto as $cur){
				$cur["activo"]=false;			
				foreach($cursosMat as $curM){ 
					if($cur["idcurso"]==$curM["idcurso"]){
						$cur['activo']=true;
						$this->cursos[]=$cur;
						break;
					}
				}
			}
			$habilidadxcurso = array();
			if(!empty($this->cursos)){
				$resultadoTotal = $this->oNegMinedu->progresosresumen(array('idalumno' => $idalumno));
				foreach($this->cursos as $key => $value){
					$habilidadxcurso[$value['idcurso']] = array('4'=>$resultadoTotal[0]['prog_hab_L_'.$value['nombre']],'5'=>$resultadoTotal[0]['prog_hab_R_'.$value['nombre']],'6'=>$resultadoTotal[0]['prog_hab_W_'.$value['nombre']],'7'=>$resultadoTotal[0]['prog_hab_S_'.$value['nombre']]);
				}
			}

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));
			
			$this->habilidadxcurso = $habilidadxcurso;
			$this->competencia = $this->oNegCompetencias->competencias();
			$this->capacidadesxunidad = $this->oNegMin_unidad_capacidad->capacidades();
			
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"));
			$this->esquema = 'reportes/progresoxcompetencia';

			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function getHabilidadxUnidad(){
		$this->documento->plantilla = "returnjson";
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idrecurso = (isset($_REQUEST['idrecurso'])) ? $_REQUEST['idrecurso'] : null;
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			$resultado = array();
			if(!is_null($idcurso) && !is_null($idrecurso)){
				$resultado = $this->oNegMinedu->habilidadCompetencia_unidad($idcurso,$idrecurso,$idalumno);
			}
			$data=array('code'=>'ok','data'=>$resultado);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	private function getubicaciones($obj){
		if(!empty($obj)){
			$tmp_obj = array();
			foreach($obj as $value){
				$tmp_obj[] = $value;
			}
			return $tmp_obj;
		}
		return null;
	}

	public function minedu(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
			$this->datos_perfil = $this->oNegPersonal->dataPersonal;
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			$departamentos = null;
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			$ubicaciones_filtros = array();
			$idlocal = null;
			$idugel = null;
			$idrol = null;
			$ubigeo = null;
			if($persona = $this->oNegPersonal->buscar(array('idpersona' => $idalumno))){
				if(!empty($persona[0])){
					$idlocal = $persona[0]['idlocal'];
					$idugel = $persona[0]['idugel'];
					$idrol = $persona[0]['rol'];
					$ubigeo = $persona[0]['ubigeo'];
				}
			}
			//codigo
			switch(intval($this->user['idrol'])){
				//reportes minedu
				case 9: {
					$ubicaciones_filtros = array('departamentos' => null,'provincias' => null, 'distrito' => null,'ugel' => null);
					$departamentos = $this->oNegDre->buscar();
					$ubicaciones_filtros['departamentos'] = $this->getubicaciones($departamentos);
					//$provincias = $this->oNegMinedu->buscarubigeo(array('diferente_provincia' => '00', 'distrito' => '00'));
					//$ubicaciones_filtros['provincias'] = $this->getubicaciones($provincias);
					$distrito = $this->oNegMinedu->buscarubigeo(array('diferente_provincia' => '00', 'diferente_distrito' => '00'));
					$ugel = $this->oNegUgel->buscar();
					$ubicaciones_filtros['ugel'] = $this->getubicaciones($ugel);
					
				}
				break;
				//Reportes DRE
				case 8: {
					$ubicaciones_filtros = array('departamentos' => null,'provincias' => null, 'distrito' => null,'ugel' => null);
					//buscar el ugel para que nos de el ubigeo y ubicar el departamento
					$tmp_ugel = $this->oNegUgel->buscar(array('idugel' => $idugel));
					$departamentos = $this->oNegDre->buscar(array('ubigeo' => $tmp_ugel[0]['iddepartamento']));
					$ubicaciones_filtros['departamentos'] = $this->getubicaciones($departamentos);
					//$provincias = $this->oNegMinedu->buscarubigeo(array('diferente_provincia' => '00', 'distrito' => '00'));
					//$ubicaciones_filtros['provincias'] = $this->getubicaciones($provincias);
					$ugel = $this->oNegUgel->buscar(array('iddepartamento' => $tmp_ugel[0]['iddepartamento']));
					$ubicaciones_filtros['ugel'] = $this->getubicaciones($ugel);
					
				}
				break;
				//reportes UGEL
				case 7: {
					$ubicaciones_filtros = array('departamentos' => null,'provincias' => null, 'distrito' => null,'ugel' => null);
					$ugel = $this->oNegUgel->buscar(array('idugel' => $idugel));
					$ubicaciones_filtros['ugel'] = $this->getubicaciones($ugel);
					$departamentos = $this->oNegDre->buscar(array('ubigeo' =>$ugel[0]['iddepartamento']));
					$ubicaciones_filtros['departamentos'] = $this->getubicaciones($departamentos);
					//$provincias = $this->oNegMinedu->buscarubigeo(array('diferente_provincia' => '00', 'distrito' => '00'));
					//$ubicaciones_filtros['provincias'] = $this->getubicaciones($provincias);					
				}
				break;
				//reportes IIEE
				case 6: {
					$ubicaciones_filtros = array('departamentos' => null,'provincias' => null, 'distrito' => null,'ugel' => null);
					$ugel = $this->oNegUgel->buscar(array('idugel' => $idugel));
					$ubicaciones_filtros['ugel'] = $this->getubicaciones($ugel);
					$departamentos = $this->oNegDre->buscar(array('ubigeo' =>$ugel[0]['iddepartamento']));
					$ubicaciones_filtros['departamentos'] = $this->getubicaciones($departamentos);
					
				}
				break;
			}

			//DATOS
			$this->idrol = $this->user['idrol'];
			$this->departamentos = $departamentos;
			$this->ubicaciones_filtros = $ubicaciones_filtros;
			
			//Importar libreria para los charts 
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->stylesheet('bootstrap-select.min', '/libs/bootstrap-select/css/');
			$this->documento->script('bootstrap-select.min', '/libs/bootstrap-select/js/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_("Reporte MINEDU"),true);
			$this->esquema = 'reportes/minedu';

			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/**--------------------JSON-------------------- */
	public function listaGrupo(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : 0;
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : 0;
			$this->grupos = $this->oNegAcad_grupoauladetalle->buscar(array('idcurso' => $idcurso, 'iddocente' => $this->user['idpersona'],'idproyecto' => $idproyecto ));
			//  var_dump($this->grupos);
			$this->grupoaula = $this->oNegAcad_grupoaula->buscar();
			$_data = array();
			if(!empty($this->grupos)){
				foreach($this->grupos as $value){
					$searchGroup = array_search($value['idgrupoaula'], array_column($this->grupoaula, 'idgrupoaula'));
					//$value['idgrupoaula']
					$_data[]= array('id' => $value['idgrupoaula'],'nombre' => $this->grupoaula[$searchGroup]['nombre']);
					
				}
			}else{
				throw new Exception("Error la consultas CURSOS esta vacio", 1);
			}
			$data=array('code'=>'ok','data'=>['grupos'=>$_data]);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
      		$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function listaGrupoAlumnos(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idgrupo = (isset($_REQUEST['idgrupoaula'])) ? $_REQUEST['idgrupoaula'] : 0;
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : 0;
			//$this->alumnos = $this->oNegAcad_matricula->buscar(array('idgrupoaula' => $idgrupo,'iddocente' => $this->user['idpersona'], 'idproyecto' => $this->user['idproyecto']));
			$this->alumnos = $this->oNegAcad_matricula->buscar(array('idgrupoauladetalle' => $idgrupo,'iddocente' => $this->user['idpersona'], 'idproyecto' => $this->user['idproyecto']));
			$data = array();
			if(!empty($this->alumnos)){
				foreach($this->alumnos as $value){
					$clv = null;
					$user = null;
					if($v = $this->oNegPersonal->buscar(array('idpersona' => $value['idalumno']))){
						$clv = $v[0]['clave'];
						$user= $v[0]['usuario'];
					}
					$data[] = array('id' => $value['idalumno'],'nombre'=>$value['stralumno'], 'dni' => $value['dni'], 'key' => $clv, 'u' => $user);
				}
			}else{
				throw new Exception("Error la consultas ALUMNOS esta vacio", 1);
			}
			$data=array('code'=>'ok','data'=>['alumnos'=>$data]);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
      		$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function listarinstituciones(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idugel = (isset($_REQUEST['idugel'])) ? $_REQUEST['idugel'] : null;
			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$idproyecto = (isset($_REQUEST['idproyecto'])) ? $_REQUEST['idproyecto'] : $this->user['idproyecto'];
			$filtros['SQL_sinugel'] = true;
			if(!is_null($idubigeo)){
				// $filtros['id_ubigeo'] = $idubigeo;
				$filtros['like_id_ubigeo'] = substr($idubigeo,0,2);
			}
			if(!is_null($idugel)){
				$filtros['idugel'] = $idugel;
			}
			// $filtros = (is_null($idubigeo)) ? array('SQL_sinugel' => true) : array('SQL_sinugel' => true,'id_ubigeo' => $idubigeo);
			$instituciones = $this->oNegMinedu->buscariiee($filtros);
			$_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function comparativa_alumno(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	
	public function comparativa_alumno_examenu(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno05';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function comparativa_alumno_tiempo(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno06';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function comparativa_alumno_usodominio(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno07';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function comparativa_alumno_habilidades(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;
			$this->competencias = $this->oNegMinedu->competencias();
			$capacidades = array();
			if(!empty($this->competencias)){
				foreach($this->competencias as $key => $value){
					$tmp_resultado = $this->oNegMinedu->capacidades($value['id']);
					if(!empty($tmp_resultado)){
						$capacidades[$value['id']] = null;
						foreach($tmp_resultado as $key2 => $value2){
							$capacidades[$value['id']][] = $value2;
						}
					}
				}
			}
			$this->capacidades = $capacidades;
			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno09';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function comparativa_alumno_entradasalida(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno08';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function comparativa_alumno_examene(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno02';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function comparativa_alumno_examens(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno03';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function comparativa_alumno_examenes(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$idalumno = (isset($_REQUEST['idalumno'])) ? $_REQUEST['idalumno'] : $this->user['idpersona'];

			$this->datos_Perfil = $this->oNegPersonal->dataPersonal;
			//$this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))
			$name = $this->user['nombre_full']; //defecto
			$foto = 'user_avatar.jpg'; //defecto
			if(isset($_REQUEST['idalumno'])){
				if($sqlName = $this->oNegPersonal->buscar(array('idpersona' => $idalumno,'rol' => 3))){
					$name =  "{$sqlName[0]['nombre']} {$sqlName[0]['ape_paterno']} {$sqlName[0]['ape_materno']}";
					$foto = (!empty($sqlName[0]['foto'])) ? $sqlName[0]['foto'] : $foto;
				}
			}
			$this->fullname = $name;
			$this->foto = $foto;

			$this->miscolegios = $this->oNegAcad_grupoauladetalle->micolegio(array('iddocente'=>$this->user["idpersona"],'idproyecto'=>$this->user["idproyecto"]));

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimiento';
			$this->documento->setTitulo(JrTexto::_("Reports -- SmartEnglish"),true);
			$this->esquema = 'reportes/minedutabla_comparativoalumno04';
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function notasmatricula(){
		$this->documento->plantilla = 'returnjson';
		try{
			$this->user = NegSesion::getUsuario();

			$filtros=array();
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(isset($_REQUEST["trimestre"])) $filtros['trimestre'] = $_REQUEST["trimestre"];
			// if(empty($_REQUEST["idproyecto"])){
			// 	$filtros["idproyecto"]=$this->user["idproyecto"];
			// }
			$resultado = array();
			$resultado = $this->oNegNotas_quiz->buscarNotas($filtros);

			$data=array('code'=>'ok','data'=>$resultado);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function notasunidadmatricula(){
		$this->documento->plantilla = 'returnjson';
		try{
			$this->user = NegSesion::getUsuario();
			$filtros=array();
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			if(is_null($idcurso)){
				throw new Exception("se requiere el id de curso");
			}
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];

			$resultado = $this->oNegNotas_quiz->buscarNotasUnidad($filtros,$idcurso);
			echo json_encode(array('code'=>'ok','data'=>$resultado));
			exit();
            return parent::getEsquema();
		}catch(Exception $e){
			echo json_encode(array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage())));
		}
	}

	public function getdocentexcurso(){
		$this->documento->plantilla = 'returnjson';
		try{
			$idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null ;
			$_data = array();
			if($idlocal != null){
				$_data = $this->oNegMinedu->getdocente($idlocal);
			}
			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	
	public function minedu_ubicacion_seccion(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'Alumno';
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$departamentocompleto = (isset($_REQUEST['departamentocompleto'])) ? $_REQUEST['departamentocompleto'] : false ;
			$ubigeo = (isset($_REQUEST['ubigeo'])) ? $_REQUEST['ubigeo'] : null;
			$idugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			
			$this->cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$this->seccion = $this->oNegSeccion->buscar();
			$this->grados = $this->oNegGrado->buscar();

			$ubicaciones_filtros = array('departamentos' => null,'provincias' => null, 'distrito' => null,'ugel' => null);

			$filtros_ubigeo = array('provincia' =>'00', 'distrito' => '00');
			$filtros_ugel = null;

			if($departamentocompleto == false){
				if($ubigeo != null){
					$filtros_ubigeo = array('id_ubigeo' => $ubigeo);
					$filtros_ugel = array('id_ubigeo' => $ubigeo);
				}
				if($idugel != null){
					$filtros_ugel = array('idugel' => $idugel);
				}
			}

			$departamentos = $this->oNegMinedu->buscarubigeo($filtros_ubigeo);
			$ubicaciones_filtros['departamentos'] = $this->getubicaciones($departamentos);

			$ugel = $this->oNegUgel->buscar($filtros_ugel);
			$ubicaciones_filtros['ugel'] = $this->getubicaciones($ugel);

			$this->ubicaciones_filtros = $ubicaciones_filtros;

			$this->type = 0;
			$this->ubigeo = ($departamentocompleto == false) ? $ubigeo : 0;
			$this->idugel = $idugel;
			$this->iiee = $iiee;
			$this->anio = $anio; 
			$this->persona = $persona;

			$this->title = 'Reporte de Ubicación';

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop2';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/mineduxseccion';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function minedu_ubicacion_seccion_json(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			
			$local = (isset($_REQUEST['local'])) ? $_REQUEST['local'] : null;
			$grado = (isset($_REQUEST['grado'])) ? $_REQUEST['grado'] : null;
			$curso = (isset($_REQUEST['curso'])) ? $_REQUEST['curso'] : null;
			$grado = (isset($_REQUEST['grado'])) ? $_REQUEST['grado'] : null;
			$grupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
			$seccion = (isset($_REQUEST['seccion'])) ? $_REQUEST['seccion'] : null;
			
			$resultado = array();

			if($grupoauladetalle != null){
				$header = array('DRE','UGEL','IIEE','Curso','Grado','Seccion','Nota','Ubicacion','Listening','Reading','Writing','Speaking');
			}else{
				$header = array('DRE','UGEL','IIEE','Curso','Grado','Seccion','Prom. Nota','A1','A2','B1');
			}

			//select (SELECT nq.idnota FROM notas_quiz nq where nq.idalumno = ma.idalumno and nq.idrecurso = 326 and nq.tipo = 'U') as notaexamen, ma.* from acad_grupoauladetalle gad inner join acad_matricula ma on gad.idgrupoauladetalle = ma.idgrupoauladetalle
			//WHERE gad.idlocal = 4 and gad.idcurso = 31 and gad.idgrado = 1 and gad.idsesion = ?;

			//select distinct sec.*, ( SELECT count(*) from acad_matricula ma2 inner join acad_grupoauladetalle gad2 on ma2.idgrupoauladetalle = gad2.idgrupoauladetalle where gad.idcurso = gad2.idcurso and gad.idgrado = gad2.idgrado and gad.idlocal = gad2.idlocal and gad2.idsesion = sec.idsesion) AS total FROM min_sesion sec INNER JOIN acad_grupoauladetalle gad ON gad.idsesion = sec.idsesion inner join acad_matricula ma ON gad.idgrupoauladetalle = ma.idgrupoauladetalle
			//where gad.idcurso = 31 and gad.idgrado = 1 and gad.idlocal = 4

			$data=array('code'=>'ok','data'=>$resultado);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
	public function _minedu_ubicacion(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			/**Receive params */
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'Alumno';
			$departamentocompleto = (isset($_REQUEST['departamentocompleto'])) ? $_REQUEST['departamentocompleto'] : false ;
			$ubigeo = (isset($_REQUEST['ubigeo'])) ? $_REQUEST['ubigeo'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iieecompleto = (isset($_REQUEST['iieecompleto'])) ? $_REQUEST['iieecompleto'] : false;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$gradocompleto = (isset($_REQUEST['gradocompleto'])) ? $_REQUEST['gradocompleto'] : false;
			$grado = (isset($_REQUEST['grado'])) ? $_REQUEST['grado'] : null;

			

			$this->title = 'Reports -- SmartEnglish';

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedutablas';
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function minedu_ubicacion(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';
			/**Start to party */
			
			$rows_t = array();
			$habilidades = array();

			if($tipoReporte === 1){
				$column = 'dre';
				$entidad = 'DRE';
				$entidad2 = 'Región';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'alumno_ubicacion','ubicacion_A1','ubicacion_A2','ubicacion_B1','ubicacion_B2','ubicacion_C1'),'habilidad'=> array('ubicacion_hab_L','ubicacion_hab_R','ubicacion_hab_W','ubicacion_hab_S')),array('SQL_DRE' => true,'tipo' =>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$entidad = 'UGEL';
				$entidad2 = 'Unidad';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'alumno_ubicacion','ubicacion_A1','ubicacion_A2','ubicacion_B1','ubicacion_B2','ubicacion_C1'),'habilidad'=> array('ubicacion_hab_L','ubicacion_hab_R','ubicacion_hab_W','ubicacion_hab_S')),array('SQL_UGEL' => true,'iddre'=>$dre,'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$entidad = 'IIEE';
				$entidad2 = 'Intituto';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'alumno_ubicacion','ubicacion_A1','ubicacion_A2','ubicacion_B1','ubicacion_B2','ubicacion_C1'),'habilidad'=> array('ubicacion_hab_L','ubicacion_hab_R','ubicacion_hab_W','ubicacion_hab_S')),array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$entidad = 'Grado';
				$entidad2 = $entidad;
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'alumno_ubicacion','ubicacion_A1','ubicacion_A2','ubicacion_B1','ubicacion_B2','ubicacion_C1'),'habilidad'=> array('ubicacion_hab_L','ubicacion_hab_R','ubicacion_hab_W','ubicacion_hab_S')),array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}
			//ubicaciones
			if(!empty($resultado)){
				$total = 0;
				foreach($resultado as $v){
					$val1 = (!empty($v['ubicacion_A1'])) ? ($v['ubicacion_A1'] * 100 / $v['alumno_ubicacion']) : 0;
					$val2 = (!empty($v['ubicacion_A2'])) ? ($v['ubicacion_A2'] * 100 / $v['alumno_ubicacion']) : 0;
					$val3 = (!empty($v['ubicacion_B1'])) ? ($v['ubicacion_B1'] * 100 / $v['alumno_ubicacion']) : 0;
					$val4 = (!empty($v['ubicacion_B2'])) ? ($v['ubicacion_B2'] * 100 / $v['alumno_ubicacion']) : 0;
					$val5 = (!empty($v['ubicacion_C1'])) ? ($v['ubicacion_C1'] * 100 / $v['alumno_ubicacion']) : 0;
					$rows_t[] = array(
						'nombre' => $v[$column],
						'A1' => round($val1,2),
						'A2' => round($val2,2),
						'B1' => round($val3,2),
						'B2' => round($val4,2),
						'C1' => round($val5,2),
						'4' => round($v['ubicacion_hab_L'],2),
						'5' => round($v['ubicacion_hab_R'],2),
						'6' => round($v['ubicacion_hab_W'],2),
						'7' => round($v['ubicacion_hab_S'],2)
					);
				}
			}

			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;
			$this->Entidad2 = $entidad2;	

			$this->title = 'Reporte de Ubicación';

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedu_ubicacion';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function examenes_trimestre_general(&$_tablamacro,&$ubigeougel,&$cursos,$recursos,$persona,$anio){
		foreach($ubigeougel as $key1 => $value1){
			foreach($cursos as $keycurso => $valuecurso){
				$preparerow = array();
					$preparerow['cod_ubigeo'] = $value1['id_ubigeo'];
					$preparerow['departamento'] = $value1['ciudad'];
					$preparerow['ugel'] = $value1['descripcion'];
					$preparerow['anio'] = $anio;
					$preparerow['persona'] = $persona;
					$preparerow['curso'] = $valuecurso['nombre'];
					$B1 = 0;$B2 = 0;$B3 = 0;
					$L1 = 0;$R1 = 0;$W1 = 0;$S1 = 0;$L2 = 0;$R2 = 0;$W2 = 0;$S2 = 0;$L3 = 0;$R3 = 0;$W3 = 0;$S3 = 0;
					$count_b1=0;$count_b2=0;$count_b3=0;
					$key_recursos = array_keys(array_column($recursos,'idcurso'), $valuecurso['idcurso']);
					// var_dump($key_recursos);
					if(!empty($key_recursos)){
						$valor1 = (isset($recursos[$key_recursos[0]])) ? $recursos[$key_recursos[0]]['idrecurso'] : 0;
						$valor2 = (isset($recursos[$key_recursos[1]])) ? $recursos[$key_recursos[1]]['idrecurso'] : 0;
						$valor3 = (isset($recursos[$key_recursos[2]])) ? $recursos[$key_recursos[2]]['idrecurso'] : 0;
						
						$resultado_bi = $this->oNegMinedu->buscartrimestre(array('rol' => 3,'recurso1'=>$valor1,'recurso2'=>$valor2,'recurso3'=>$valor3,'idcurso' => $valuecurso['idcurso']));
						if(!empty($resultado_bi)){
							$keys = array_keys(array_column($resultado_bi,'idugel'), $value1['idugel']);
							if(!empty($keys)){
								foreach($keys as $v){
									// if($resultado_bi[$v]['idcurso']  == $valuecurso['idcurso']){
										if(!is_null($resultado_bi[$v]['notaBi01'])){
											$B1 += $resultado_bi[$v]['notaBi01'];
											$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi01'],true);
											$L1 = (isset($json_habilidades['4'])) ? $L1 + $json_habilidades['4'] : $L1;
											$R1 = (isset($json_habilidades['5'])) ? $R1 + $json_habilidades['5'] : $R1;
											$W1 = (isset($json_habilidades['6'])) ? $W1 + $json_habilidades['6'] : $W1;
											$S1 = (isset($json_habilidades['7'])) ? $S1 + $json_habilidades['7'] : $S1;
											$count_b1++;
										}
										if(!is_null($resultado_bi[$v]['notaBi02'])){
											$B2 += $resultado_bi[$v]['notaBi02'];
											$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi02'],true);
											$L2 = (isset($json_habilidades['4'])) ? $L2 + $json_habilidades['4'] : $L2;
											$R2 = (isset($json_habilidades['5'])) ? $R2 + $json_habilidades['5'] : $R2;
											$W2 = (isset($json_habilidades['6'])) ? $W2 + $json_habilidades['6'] : $W2;
											$S2 = (isset($json_habilidades['7'])) ? $S2 + $json_habilidades['7'] : $S2;
											$count_b2++;
										}
										if(!is_null($resultado_bi[$v]['notaBi03'])){
											$B3 += $resultado_bi[$v]['notaBi03'];
											$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi03'],true);
											$L3 = (isset($json_habilidades['4'])) ? $L3 + $json_habilidades['4'] : $L3;
											$R3 = (isset($json_habilidades['5'])) ? $R3 + $json_habilidades['5'] : $R3;
											$W3 = (isset($json_habilidades['6'])) ? $W3 + $json_habilidades['6'] : $W3;
											$S3 = (isset($json_habilidades['7'])) ? $S3 + $json_habilidades['7'] : $S3;
											$count_b3++;
										}
										
									// }//end if curso
								}//end ofreach 
							}
						}//end if resultado
					}//endif keyrecursos
					
					$L1 = (!empty($L)) ? ($L1 *100) / (100 * $count_b1) : $L1;
					$R1 = (!empty($R)) ? ($R1 *100) / (100 * $count_b1) : $R1;
					$W1 = (!empty($W)) ? ($W1 *100) / (100 * $count_b1) : $W1;
					$S1 = (!empty($S)) ? ($S1 *100) / (100 * $count_b1) : $S1;
					$L2 = (!empty($L2)) ? ($L2 *100) / (100 * $count_b2) : $L2;
					$R2 = (!empty($R2)) ? ($R2 *100) / (100 * $count_b2) : $R2;
					$W2 = (!empty($W2)) ? ($W2 *100) / (100 * $count_b2) : $W2;
					$S2 = (!empty($S2)) ? ($S2 *100) / (100 * $count_b2) : $S2;
					$L3 = (!empty($L3)) ? ($L3 *100) / (100 * $count_b3) : $L3;
					$R3 = (!empty($R3)) ? ($R3 *100) / (100 * $count_b3) : $R3;
					$W3 = (!empty($W3)) ? ($W3 *100) / (100 * $count_b3) : $W3;
					$S3 = (!empty($S3)) ? ($S3 *100) / (100 * $count_b3) : $S3;


					$preparerow['Trimestre_1'] = (!empty($B1)) ? ($B1 / $count_b1)*0.20 : $B1;
					$preparerow['Trimestre_2'] = (!empty($B2)) ? ($B2 / $count_b2)*0.20 : $B2;
					$preparerow['Trimestre_3'] = (!empty($B3)) ? ($B3 / $count_b3)*0.20 : $B3;
					$preparerow['habilidades_trimestre1'] = "Listening: {$L1} <br> Reading: {$R1} <br> Writing: {$W1} <br> Speaking: {$S1}"; 
					$preparerow['habilidades_trimestre2'] = "Listening: {$L2} <br> Reading: {$R2} <br> Writing: {$W2} <br> Speaking: {$S2}"; 
					$preparerow['habilidades_trimestre3'] = "Listening: {$L3} <br> Reading: {$R3} <br> Writing: {$W3} <br> Speaking: {$S3}"; 

					$_tablamacro['body'][]=$preparerow;
			}//end foreach cursos
		}//end foreach ubigeougel
	}

	private function examenes_trimestre(&$_tablamicro,&$_ubigeougel,&$grado,&$cursos,$recursos,$_instituciones,$persona,$anio){
		foreach($_ubigeougel as $key1 => $value1){
			foreach($grado as $gradovalue){
				foreach($cursos as $keycurso => $valuecurso){
					$preparerow = array();
						$preparerow['cod_ubigeo'] = $value1['id_ubigeo'];
						$preparerow['departamento'] = $value1['ciudad'];
						$preparerow['ugel'] = $value1['descripcion'];
						$preparerow['iiee'] = (count($_instituciones) == 1) ? $_instituciones[0]['nombre'] : 'Instituciones';
						$preparerow['anio'] = $anio;
						$preparerow['persona'] = $persona;
						$preparerow['curso'] = $valuecurso['nombre'];
						$preparerow['grado'] = $gradovalue['descripcion'];

						$B1 = 0;$B2 = 0;$B3 = 0;
						$L1 = 0;$R1 = 0;$W1 = 0;$S1 = 0;$L2 = 0;$R2 = 0;$W2 = 0;$S2 = 0;$L3 = 0;$R3 = 0;$W3 = 0;$S3 = 0;
						$count_b1=0;$count_b2=0;$count_b3=0;
						$key_recursos = array_keys(array_column($recursos,'idcurso'), $valuecurso['idcurso']);
						// var_dump($key_recursos);
						if(!empty($key_recursos)){
							$valor1 = (isset($recursos[$key_recursos[0]])) ? $recursos[$key_recursos[0]]['idrecurso'] : 0;
							$valor2 = (isset($recursos[$key_recursos[1]])) ? $recursos[$key_recursos[1]]['idrecurso'] : 0;
							$valor3 = (isset($recursos[$key_recursos[2]])) ? $recursos[$key_recursos[2]]['idrecurso'] : 0;
							
							$resultado_bi = $this->oNegMinedu->buscartrimestre(array('rol' => 3,'recurso1'=>$valor1,'recurso2'=>$valor2,'recurso3'=>$valor3,'idcurso' => $valuecurso['idcurso']));
							if(!empty($resultado_bi)){
								$keys = array_keys(array_column($resultado_bi,'idugel'), $value1['idugel']);
								$keys = (!empty($keys)) ? array_keys(array_column($resultado_bi,'idgrado'), $gradovalue['idgrado']) : null;
								if(!empty($keys)){
									foreach($keys as $v){
										// if($resultado_bi[$v]['idcurso']  == $valuecurso['idcurso']){
											if(!is_null($resultado_bi[$v]['notaBi01'])){
												$B1 += $resultado_bi[$v]['notaBi01'];
												$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi01'],true);
												$L1 = (isset($json_habilidades['4'])) ? $L1 + $json_habilidades['4'] : $L1;
												$R1 = (isset($json_habilidades['5'])) ? $R1 + $json_habilidades['5'] : $R1;
												$W1 = (isset($json_habilidades['6'])) ? $W1 + $json_habilidades['6'] : $W1;
												$S1 = (isset($json_habilidades['7'])) ? $S1 + $json_habilidades['7'] : $S1;
												$count_b1++;
											}
											if(!is_null($resultado_bi[$v]['notaBi02'])){
												$B2 += $resultado_bi[$v]['notaBi02'];
												$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi02'],true);
												$L2 = (isset($json_habilidades['4'])) ? $L2 + $json_habilidades['4'] : $L2;
												$R2 = (isset($json_habilidades['5'])) ? $R2 + $json_habilidades['5'] : $R2;
												$W2 = (isset($json_habilidades['6'])) ? $W2 + $json_habilidades['6'] : $W2;
												$S2 = (isset($json_habilidades['7'])) ? $S2 + $json_habilidades['7'] : $S2;
												$count_b2++;
											}
											if(!is_null($resultado_bi[$v]['notaBi03'])){
												$B3 += $resultado_bi[$v]['notaBi03'];
												$json_habilidades = json_decode($resultado_bi[$v]['habilidadBi03'],true);
												$L3 = (isset($json_habilidades['4'])) ? $L3 + $json_habilidades['4'] : $L3;
												$R3 = (isset($json_habilidades['5'])) ? $R3 + $json_habilidades['5'] : $R3;
												$W3 = (isset($json_habilidades['6'])) ? $W3 + $json_habilidades['6'] : $W3;
												$S3 = (isset($json_habilidades['7'])) ? $S3 + $json_habilidades['7'] : $S3;
												$count_b3++;
											}
											
										// }//end if curso
									}//end ofreach 
								}
							}//end if resultado
						}//endif keyrecursos
						
						$L1 = (!empty($L)) ? ($L1 *100) / (100 * $count_b1) : $L1;
						$R1 = (!empty($R)) ? ($R1 *100) / (100 * $count_b1) : $R1;
						$W1 = (!empty($W)) ? ($W1 *100) / (100 * $count_b1) : $W1;
						$S1 = (!empty($S)) ? ($S1 *100) / (100 * $count_b1) : $S1;
						$L2 = (!empty($L2)) ? ($L2 *100) / (100 * $count_b2) : $L2;
						$R2 = (!empty($R2)) ? ($R2 *100) / (100 * $count_b2) : $R2;
						$W2 = (!empty($W2)) ? ($W2 *100) / (100 * $count_b2) : $W2;
						$S2 = (!empty($S2)) ? ($S2 *100) / (100 * $count_b2) : $S2;
						$L3 = (!empty($L3)) ? ($L3 *100) / (100 * $count_b3) : $L3;
						$R3 = (!empty($R3)) ? ($R3 *100) / (100 * $count_b3) : $R3;
						$W3 = (!empty($W3)) ? ($W3 *100) / (100 * $count_b3) : $W3;
						$S3 = (!empty($S3)) ? ($S3 *100) / (100 * $count_b3) : $S3;

						$preparerow['Trimestre_1'] = (!empty($B1)) ? ($B1 / $count_b1)*0.20 : $B1;
						$preparerow['Trimestre_2'] = (!empty($B2)) ? ($B2 / $count_b2)*0.20 : $B2;
						$preparerow['Trimestre_3'] = (!empty($B3)) ? ($B3 / $count_b3)*0.20 : $B3;
						$preparerow['habilidades_trimestre1'] = "Listening: {$L1} <br> Reading: {$R1} <br> Writing: {$W1} <br> Speaking: {$S1}"; 
						$preparerow['habilidades_trimestre2'] = "Listening: {$L2} <br> Reading: {$R2} <br> Writing: {$W2} <br> Speaking: {$S2}"; 
						$preparerow['habilidades_trimestre3'] = "Listening: {$L3} <br> Reading: {$R3} <br> Writing: {$W3} <br> Speaking: {$S3}"; 

						$_tablamicro['body'][]=$preparerow;
				}//end foreach cursos

			}//end foreach grado
		}//end foreach ubigeougel
	}

	public function minedu_examenes(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			$this->isExamenes = true;
			$_data =  array();
			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';
			/**Start to party */
			$rows_t = array('bimestre'=>array(),'trimestre'=>array());
			$valores = array(
				'general'=> array('examen_b1_prom','examen_b2_prom','examen_b3_prom','examen_b4_prom','examen_t1_prom','examen_t2_prom','examen_t3_prom'),
				'habilidad'=> array('examen_b1_hab_L','examen_b1_hab_R','examen_b1_hab_W','examen_b1_hab_S','examen_b2_hab_L','examen_b2_hab_R','examen_b2_hab_W','examen_b2_hab_S','examen_b3_hab_L','examen_b3_hab_R','examen_b3_hab_W','examen_b3_hab_S','examen_b4_hab_L','examen_b4_hab_R','examen_b4_hab_W','examen_b4_hab_S','examen_t1_hab_L','examen_t1_hab_R','examen_t1_hab_W','examen_t1_hab_S','examen_t2_hab_L','examen_t2_hab_R','examen_t2_hab_W','examen_t2_hab_S','examen_t3_hab_L','examen_t3_hab_R','examen_t3_hab_W','examen_t3_hab_S')
			);
			if($tipoReporte === 1){
				$column = 'dre';
				$valores['general'][] = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_DRE' => true,'tipo'=>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$valores['general'][] = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_UGEL' => true,'iddre'=>$dre,'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$valores['general'][] = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$valores['general'][] = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}

			if(!empty($resultado)){
				$total = 0;
				foreach($resultado as $v){
					$prepare = array(); $preparet = array();
					for($i = 1; $i <= 4; $i++){
						$prepare[strval('b'.$i)]['4'] = $v['examen_b'.$i.'_hab_L'];
						$prepare[strval('b'.$i)]['5'] = $v['examen_b'.$i.'_hab_R'];
						$prepare[strval('b'.$i)]['6'] = $v['examen_b'.$i.'_hab_W'];
						$prepare[strval('b'.$i)]['7'] = $v['examen_b'.$i.'_hab_S'];
						if($i <= 3){
							$preparet[strval('t'.$i)]['4'] = $v['examen_t'.$i.'_hab_L'];
							$preparet[strval('t'.$i)]['5'] = $v['examen_t'.$i.'_hab_R'];
							$preparet[strval('t'.$i)]['6'] = $v['examen_t'.$i.'_hab_W'];							
							$preparet[strval('t'.$i)]['7'] = $v['examen_t'.$i.'_hab_S'];							
						}
					}
					$rows_t['bimestre'][] = array(
						'nombre' => $v[$column],
						'promedios' => array(
							'b1' =>round($v['examen_b1_prom'],2),
							'b2' =>round($v['examen_b2_prom'],2),
							'b3' =>round($v['examen_b3_prom'],2),
							'b4' =>round($v['examen_b4_prom'],2)
						),
						'habilidades' => $prepare
					);
					$rows_t['trimestre'][] = array(
						'nombre' => $v[$column],
						'promedios' => array(
							't1' =>round($v['examen_t1_prom'],2),
							't2' =>round($v['examen_t2_prom'],2),
							't3' =>round($v['examen_t3_prom'],2)
						),
						'habilidades' => $preparet
					);
				}//end foreach resultado
			}


			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;

			$this->title = 'Reporte de examenes Bimestre y Trimestre';

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedu_examenes';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function sumarhabilidad($curso,$keyname,$valores,&$contenedor){
		$resultado = array();
		
		if(!empty($valores)){
			
			$contenedor['4'] += $valores[0][$keyname[0]]; 
			$contenedor['5'] += $valores[0][$keyname[1]]; 
			$contenedor['6'] += $valores[0][$keyname[2]]; 
			$contenedor['7'] += $valores[0][$keyname[3]]; 
			$contenedor['cursos'][$curso] = $contenedor;
		}
	}
	public function minedu_progresos_habilidad(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			
			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';
			/**Start to party */
			$rows_t = array();
			$valores = array(
				'habilidad'=> array('prog_hab_A1_L','prog_hab_A1_R','prog_hab_A1_W','prog_hab_A1_S','prog_hab_A2_L','prog_hab_A2_R','prog_hab_A2_W','prog_hab_A2_S','prog_hab_B1_L','prog_hab_B1_R','prog_hab_B1_W','prog_hab_B1_S','prog_hab_B2_L','prog_hab_B2_R','prog_hab_B2_W','prog_hab_B2_S','prog_hab_C1_L','prog_hab_C1_R','prog_hab_C1_W','prog_hab_C1_S')
			);
			if($tipoReporte === 1){
				$column = 'dre';
				$valores['general'][] = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_DRE' => true,'tipo'=>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$valores['general'][] = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_UGEL' => true,'iddre'=>$dre,'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$valores['general'][] = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$valores['general'][] = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->busqueda_filtrada($valores,array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}

			if(!empty($resultado)){
				foreach($resultado as $v){
					$rows_t[] = array(
						'nombre' => $v[$column],
						'A1' => array('4'=>round($v['prog_hab_A1_L'],2),'5'=>round($v['prog_hab_A1_R'],2),'6'=>round($v['prog_hab_A1_W'],2),'7'=>round($v['prog_hab_A1_S'],2)),
						'A2' => array('4'=>round($v['prog_hab_A2_L'],2),'5'=>round($v['prog_hab_A2_R'],2),'6'=>round($v['prog_hab_A2_W'],2),'7'=>round($v['prog_hab_A2_S'],2)),
						'B1' => array('4'=>round($v['prog_hab_B1_L'],2),'5'=>round($v['prog_hab_B1_R'],2),'6'=>round($v['prog_hab_B1_W'],2),'7'=>round($v['prog_hab_B1_S'],2)),
						'B2' => array('4'=>round($v['prog_hab_B2_L'],2),'5'=>round($v['prog_hab_B2_R'],2),'6'=>round($v['prog_hab_B2_W'],2),'7'=>round($v['prog_hab_B2_S'],2)),
						'C1' => array('4'=>round($v['prog_hab_C1_L'],2),'5'=>round($v['prog_hab_C1_R'],2),'6'=>round($v['prog_hab_C1_W'],2),'7'=>round($v['prog_hab_C1_S'],2))
					);
				}//end foreach $v
			}
			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;

			$this->title = 'Reporte de progresos en la plataforma virtual';
			
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_($this->title),true);
			$this->esquema = 'reportes/minedu_progresohabilidad';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function minedu_progresos(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';

			$rows_t = array();

			if($tipoReporte === 1){
				$column = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->buscar(array('SQL_DRE' => true,'iddre'=>$dre,'tipo'=>$persona));				
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->buscar(array('SQL_UGEL' => true,'iddre'=>$dre, 'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->buscar(array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->buscar(array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}

			if(!empty($resultado)){
				foreach($resultado as $v){
					$rows_t[] = array(
						'nombre' => $v[$column],
						'A1' => round($v['prog_A1'],2),
						'A2' => round($v['prog_A2'],2),
						'B1' => round($v['prog_B1'],2),
						'B2' => round($v['prog_B2'],2),
						'C1' => round($v['prog_C1'],2)
					);
				}//end foreach $v
			}

			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;

			$this->title = 'Reporte de progreso';

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_($this->title),true);
			$this->esquema = 'reportes/minedu_progreso';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	private function conversorSegundosHoras($tiempo_en_segundos) {
		$horas = floor($tiempo_en_segundos / 3600);
		$minutos = floor(($tiempo_en_segundos - ($horas * 3600)) / 60);
		$segundos = $tiempo_en_segundos - ($horas * 3600) - ($minutos * 60);
	
		return $horas . 'h:' . $minutos . "m:" . $segundos."s";
	}
	public function minedu_tiempos(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';
			/**Start to party */

			$rows_t = array();

			if($tipoReporte === 1){
				$column = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->buscar(array('SQL_DRE' => true,'tipo' =>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->buscar(array('SQL_UGEL' => true,'iddre'=>$dre,'tipo' =>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->buscar(array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo' =>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->buscar(array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo' =>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}	

			if(!empty($resultado)){
				foreach($resultado as $v){
					$rows_t[] = array(
						'nombre' => $v[$column],
						'tiempopv' => $this->conversorSegundosHoras($v['tiempopv']),
						'tiempo_exam' => $this->conversorSegundosHoras($v['tiempo_exam']),
						'tiempo_task' => $this->conversorSegundosHoras($v['tiempo_task']),
						'tiempo_smartbook' => $this->conversorSegundosHoras($v['tiempo_smartbook']),
						'tiempo_practice' => $this->conversorSegundosHoras($v['tiempo_practice'])
					);
				}
			}

			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad  = $entidad;

			$this->title = 'Reporte Tiempo en la Plataforma Virtual';

			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedu_tiempos';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function minedu_usodominios(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();
			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';

			/**Start to party */

			$rows_t = array();

			if($tipoReporte === 1){
				$column = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->buscar(array('SQL_DRE' => true,'tipo'=>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->buscar(array('SQL_UGEL' => true,'iddre'=>$dre,'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->buscar(array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->buscar(array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}
			
			if(!empty($resultado)){
				foreach($resultado as $v){
					$tiempopv = !empty($v['tiempopv']) ? (100 * ($v['tiempopv'] / 3600)) / (20) : 0 ;
					$tiempo_exam = !empty($v['tiempo_exam']) ? (100 * ($v['tiempo_exam'] / 3600)) / (10) : 0 ;
					$tiempo_task = !empty($v['tiempo_task']) ? (100 * ($v['tiempo_task'] / 3600)) / (5) : 0 ;
					$tiempo_smartbook = !empty($v['tiempo_smartbook']) ? (100 * ($v['tiempo_smartbook'] / 3600)) / (10) : 0 ;
					$tiempo_practice = !empty($v['tiempo_practice']) ? (100 * ($v['tiempo_practice'] / 3600)) / (10) : 0 ;
					$dominio = $tiempopv+$tiempo_exam+$tiempo_task+$tiempo_smartbook+$tiempo_practice;
					$dominio = !empty($dominio) ? round((100*$dominio) / 500,2) : 0 ;
					$rows_t[] = array(
						'nombre' => $v[$column],
						'dominio' => $dominio
					);
				}
			}

			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;

			$this->title = 'Reporte Tiempo en la Plataforma Virtual';

			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedu_usodominio';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	
	public function minedu_entradasalida(){
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			/**Receive params */
			$tipoReporte = (isset($_REQUEST['idreporte'])) ? intval($_REQUEST['idreporte']) : 1;
			$dre = (isset($_REQUEST['dre'])) ? $_REQUEST['dre'] : null;
			$ugel = (isset($_REQUEST['ugel'])) ? $_REQUEST['ugel'] : null;
			$iiee = (isset($_REQUEST['iiee'])) ? $_REQUEST['iiee'] : null;
			$anio = (isset($_REQUEST['anio'])) ? $_REQUEST['anio'] : 2018;
			$persona = (isset($_REQUEST['persona'])) ? $_REQUEST['persona'] : 'A';
			/**Start to party */

			$rows_t = array('entrada'=>array(),'salida'=>array());

			if($tipoReporte === 1){
				$column = 'dre';
				$entidad = 'DRE';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'entrada_prom','alumno_entrada','salida_prom','alumno_salida'),'habilidad'=> array('entrada_hab_L','entrada_hab_R','entrada_hab_W','entrada_hab_S','salida_hab_L','salida_hab_R','salida_hab_W','salida_hab_S')),array('SQL_DRE' => true,'tipo'=>$persona));
			}elseif($tipoReporte === 2){
				$column = 'ugel';
				$entidad = 'UGEL';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'entrada_prom','alumno_entrada','salida_prom','alumno_salida'),'habilidad'=> array('entrada_hab_L','entrada_hab_R','entrada_hab_W','entrada_hab_S','salida_hab_L','salida_hab_R','salida_hab_W','salida_hab_S')),array('SQL_UGEL' => true,'iddre'=>$dre,'tipo'=>$persona));
			}elseif($tipoReporte === 3){
				$column = 'local';
				$entidad = 'IIEE';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'entrada_prom','alumno_entrada','salida_prom','alumno_salida'),'habilidad'=> array('entrada_hab_L','entrada_hab_R','entrada_hab_W','entrada_hab_S','salida_hab_L','salida_hab_R','salida_hab_W','salida_hab_S')),array('SQL_LOCAL' => true,'iddre'=>$dre,'idugel'=>$ugel,'tipo'=>$persona));
			}elseif($tipoReporte === 4){
				$column = 'grado';
				$entidad = 'Grado';
				$resultado = $this->oNegResumen->busqueda_filtrada(array('general'=> array($column,'entrada_prom','alumno_entrada','salida_prom','alumno_salida'),'habilidad'=> array('entrada_hab_L','entrada_hab_R','entrada_hab_W','entrada_hab_S','salida_hab_L','salida_hab_R','salida_hab_W','salida_hab_S')),array('SQL_GRADO' => true,'iddre'=>$dre,'idugel'=>$ugel, 'idlocal'=>$iiee,'tipo'=>$persona));
			}else{
				throw new Exception('Sin id en tipo de reportes');
			}
			
			if(!empty($resultado)){
				$total = 0;
				foreach($resultado as $v){

					$rows_t['entrada'][] = array(
						'nombre' => $v[$column],
						'promedio'=>$v['entrada_prom'],
						'4' =>round($v['entrada_hab_L'],2) ,
						'5' =>round($v['entrada_hab_R'],2),
						'6' =>round($v['entrada_hab_W'],2),
						'7' =>round($v['entrada_hab_S'],2)
					);
					$rows_t['salida'][] = array(
						'nombre' => $v[$column],
						'promedio'=>round($v['salida_prom'],2),
						'4' =>round($v['salida_hab_L'],2),
						'5' =>round($v['salida_hab_R'],2),
						'6' =>round($v['salida_hab_W'],2),
						'7' =>round($v['salida_hab_S'],2)
					);
				}
			}

			/**Send data */
			$this->RowTabla = $rows_t;
			$this->Entidad = $entidad;
			$this->title = 'Reporte de Examen de Entrada y Salida';
			
			$this->documento->script('Chart.min', '/libs/chartjs/');
			$this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
			$this->documento->script('dataTables.buttons.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.flash.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('jszip.min', '/libs/datatable1.10/extensions/jszip/');
			$this->documento->script('pdfmake.min', '/libs/datatable1.10/extensions/pdfmake/');
			$this->documento->script('vfs_fonts', '/libs/datatable1.10/extensions/vfs_fonts/');
			$this->documento->script('buttons.html5.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->script('buttons.print.min', '/libs/datatable1.10/extensions/Buttons/js/');
			$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
			$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'sintop';
			$this->documento->setTitulo(JrTexto::_("Tablas Minedu"),true);
			$this->esquema = 'reportes/minedu_entradasalida';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// public function json_minedu_ubicacion(){
		/*
		select m.idalumno, per.ubigeo,per.idugel,ubi.ciudad,per.idlocal,
(select r.rol from personal p inner join roles r on p.rol = r.idrol limit 1) as persona
,nq.tipo,nq.nota,nq.notatexto,nq.habilidades,nq.habilidad_puntaje
from acad_matricula m 
inner join personal per on m.idalumno = per.idpersona 
inner join ubigeo ubi on per.ubigeo = ubi.id_ubigeo inner join notas_quiz nq on nq.idalumno = per.idpersona
where nq.tipo = 'U';

select l.idlocal,l.nombre,l.direccion,l.id_ubigeo,l.idproyecto,l.idugel from local l;
select ubigeo.id_ubigeo,ubigeo.ciudad,ugel.idugel,ugel.cod_ugel,ugel.descripcion,ugel.iddepartamento from ubigeo inner join ugel on ubigeo.id_ubigeo = ugel.iddepartamento order by ubigeo.ciudad asc;
		*/
	// 	$this->documento->plantilla = 'returnjson';
	// 	try{
	// 		global $aplicacion;
	// 		$this->user = NegSesion::getUsuario();

	// 		$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
	// 		$depar_all = (isset($_REQUEST['departamentocompleto'])) ? $_REQUEST['departamentocompleto'] : null;
			
	// 		$filtros = array();
			
	// 		if($depar_all != null){
	// 			if($depar_all == true){
					
	// 			}
	// 		}

	// 		$_data = array();
			
			

	// 		$data=array('code'=>'ok','data'=>$_data);
	// 		echo json_encode($data);
    //         return parent::getEsquema();
	// 	}catch(Exception $e){
	// 		$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
	// 		echo json_encode($data);
	// 	}
	// }

	public function minedu_tiempo(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	

	public function minedu_usodominio(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function minedu_entrada(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function minedu_salida(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function minedu_comparativo(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function minedu_progreso(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}

	public function minedu_progresoxhabilidad(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$this->user = NegSesion::getUsuario();

			$idubigeo = (isset($_REQUEST['idubigeo'])) ? $_REQUEST['idubigeo'] : null;
			$_data = array();
			// $instituciones = $this->oNegMinedu->buscariiee($filtros);
			// $_data = (!empty($instituciones)) ? $instituciones : array();

			$data=array('code'=>'ok','data'=>$_data);
			echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function reportehabilidades(){
		global $aplicacion;			
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
        $this->cursos = array();
		$this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));  
		//       $cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
		// $cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
		
		// foreach($cursosProyecto as $cur){
		// 	$cur["activo"]=false;			
		// 	foreach($cursosMat as $curM){ 
		// 		if($cur["idcurso"]==$curM["idcurso"]){
		// 			$cur['activo']=true;
		// 			$this->cursos[]=$cur;
		// 			break;
		// 		}
		// 	}
		// 	// if($cur["idcurso"]!=31)	
		// }
        
		$this->documento->setTitulo(JrTexto::_('Skills Report'));
		$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
		// print_r($this->cursos);
		$this->esquema = 'reportes/reportehabilidades';
		return parent::getEsquema();
	}
	public function reportehabilidadesgeneral(){
		global $aplicacion;			
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
        $this->cursos = array();
        $this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
		$this->documento->setTitulo(JrTexto::_('Skills Report'));
		$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
		// print_r($this->cursos);
		$this->esquema = 'reportes/reportehabilidadesgeneral';
		return parent::getEsquema();
	}
	public function reportehabilidadesgeneralesxunidades(){
		global $aplicacion;			
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
        $this->cursos = array();
        $this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
		$this->documento->setTitulo(JrTexto::_('Skills Report'));
		$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
		// print_r($this->cursos);
		$this->esquema = 'reportes/reportehabilidadesgeneralesxunidades';
		return parent::getEsquema();
	}
	public function reportehabilidadesgeneralesxcurso(){
		global $aplicacion;			
		$this->documento->script('slick.min', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick', '/libs/sliders/slick/');
		$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
		$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery-confirm.min', '/libs/alert/');
        $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
        $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
        $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
        $this->cursos = array();
        $this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
		$this->documento->setTitulo(JrTexto::_('Skills Report'));
		$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
		// print_r($this->cursos);
		$this->esquema = 'reportes/reportehabilidadesgeneralesxcurso';
		return parent::getEsquema();
	}
	 public function listarhabilidades() //
    {
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            global $aplicacion;
            $nivel = $_REQUEST['valor'];
            // print_r($nivel);

            // $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
            $this->ejerciciostotal=array();
            // if(empty($_POST["idcurso"]) && !$flag) {
            //     throw new Exception(JrTexto::_('Error in filtering'));
            // }elseif(!$flag && !empty($_POST)){
            //     $idgrupoauladetalle=@$_POST["idcursodetalle"];
            //     $idcurso=@$_POST["idcurso"];
            // }
            // if(empty($idcurso)){
            //     throw new Exception(JrTexto::_('Error in Course'));
            // }
            
			$usuarioAct = NegSesion::getUsuario();
			$elresto = array();
			$sesiones = array();
			$elcurso = $this->oNegAcad_cursodetalle->sesiones($nivel,0);
			// var_dump($elcurso[2]['hijo']); exit();
			foreach($elcurso as $value){
				if($value['tiporecurso'] == 'U'){
					unset($value['hijo']);
					$elresto[]=$value;
				}
			}
			foreach($elcurso as $value){
				if($value['tiporecurso'] == 'U'){
					$lesiones =array();
					foreach($value['hijo'] as $hijo){
						if($hijo['tiporecurso'] == 'L'){
							$lesiones[]=$hijo;
						}
					}
					$sesiones = array_merge($sesiones,$lesiones);
				}
			}
			$this->todoCurso = array_merge($sesiones,$elresto);
			//$this->todoCurso=$this->oNegAcad_cursodetalle->buscar(array('idcurso'=>$nivel));
           // print_r($this->todoCurso);
           foreach ($this->todoCurso as $t){
			   // var_dump($t['idrecurso']);	
			
           	$this->sesion = $t['idrecurso'];
           	$this->ejercicios = $this->oNegActividad->fullActividades(array('sesion'=>$this->sesion));
           	// $this->ejerciciostotal[] = $this->ejercicios;
           	foreach ($this->ejercicios as $r){
				   $this->ejerc = $r['act'];
           		// var_dump($r['act']['det']);
           			$this->ejerciciostotal[] = $this->ejerc;
           		// foreach ($this->ejerc as $s){
           		// }
           	}
		   }
		//    var_dump($this->ejerciciostotal[19]['det']); exit();
           	// var_dump($this->ejerciciostotal[1]);
            // $recursos=$this->oNegCursodetalle->sesiones($idcurso,0);
            // $idrecursos=$this->getidrecursos($recursos);
            // $allidrecursos=join(',',$idrecursos);
            // $this->oNegTarea->setLimite(0,9999);
            // $this->todoTareas=$this->oNegTarea->buscar(array('allrecursos'=>$allidrecursos,'eliminado'=>0,'idproyecto'=>$usuarioAct["idproyecto"]));
            // $x=0;
            // if(!empty($this->todoTareas))
            // foreach ($this->todoTareas as $t){
            //     $cant_asiganaciones_activas=0;
            //     $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea'], 'iddocente'=> $usuarioAct['idpersona'],'idgrupo'=>$idgrupoauladetalle));
            //     foreach ($asignaciones as $a){
            //         $hoy = new DateTime(date('Y-m-d H:i:s'));
            //         $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
            //         $fechahoy=date('d-m-Y');
            //         $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
            //         $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
            //         $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
            //         $a['cant_presentados']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'estado'=>['P','E'] )));
            //         $a['cant_asignaciones']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'],'estado'=>['N','P','E','D'])));
            //         if($fecha_hora>$hoy){
            //             $this->tareasPend[] = array_merge($t,$a);
            //             $cant_asiganaciones_activas+=$a['cant_asignaciones'];
            //         }else{
            //             $this->tareasFin[] = array_merge($t,$a);
            //         }
            //     }
            //     $this->todoTareas[$x]['cant_asignaciones'] = $cant_asiganaciones_activas;
            //     if($cant_asiganaciones_activas==0){ /* ya no hay asignciones, entonces actualizar tarea a "No Asignada"='NA' */
            //         $this->oNegTarea->idtarea= $t['idtarea'];
            //         $this->oNegTarea->__set('estado', 'NA');
            //         $this->oNegTarea->editar();
            //     }
            //     $x++;
            // }

            // $respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, "todo"=>$this->todoTareas);
            $respuesta = array("Niveles"=>$this->todoCurso, "Ejercicios"=>$this->ejerciciostotal);
            // if($flag){ return $respuesta; }
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            // if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
}