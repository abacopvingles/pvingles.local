<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
//JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegReportealumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMinedu', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');

class WebDocente extends JrWeb
{	
	
    protected $oNegAlumno;
    protected $oNegMetodologia;
    //protected $oNegGrupos;
    protected $oNegGrupoAulaDet;
    protected $oNegLocal;
    protected $oNegNiveles;
    protected $oNegPersonal;
    private $oNegMatricula;
    private $prueba;
    private $marcoantonio;
    private $oNegHistorial_sesion;
    private $oNegActividad_alumno;
    private $oNegNotas_quiz;
    private $oNegMinedu;
    private $oNegReportealumno;
    private $oNegAcad_curso;
    private $oNegAcad_cursodetalle;
    
	public function __construct()
	{
		parent::__construct();
        $this->oNegAlumno = new NegAlumno;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        //$this->oNegGrupos = new NegGrupos;
        $this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
        $this->oNegLocal = new NegLocal;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegPersonal = new NegPersonal;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegHistorial_sesion = new NegHistorial_sesion;
        $this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegNotas_quiz = new NegNotas_quiz;
        $this->oNegReportealumno = new NegReportealumno;
        $this->oNegMinedu = new NegMinedu;
        $this->oNegAcad_curso =  new NegAcad_curso;
        $this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
        
        $this->usuarioAct = NegSesion::getUsuario();
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('docente'), true);
			$this->esquema = 'alumno/tester';
			$this->documento->plantilla = 'general';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function panelcontrol()
	{
		try {
			global $aplicacion;
            #Librerias
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             //Importar libreria para los charts 

             $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
             $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
             $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
             $this->documento->script('loader', '/libs/googlecharts/');
             $this->documento->script('chartist.min', '/libs/chartist/');
             $this->documento->stylesheet('chartist.min', '/libs/chartist/');
             $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
             $this->documento->script('circle', '/libs/graficos/progressbar/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('html2canvas.min', '/libs/html2canvas/');
             $this->documento->script('rgbcolor', '/libs/svg2canvas/');
             $this->documento->script('StackBlur', '/libs/svg2canvas/');
             $this->documento->script('canvg.min', '/libs/svg2canvas/');
             $this->documento->script('SVG2Bitmap', '/libs/chartist2image/');
            
             //Importar libreria para los charts 
	        $this->documento->script('Chart.min', '/libs/chartjs/');


            $filtros = [];
            if($this->usuarioAct["rol"]=="Alumno") {
                $this->vistaAlumno();
            } else{
                /*if(!NegSesion::tiene_acceso('docente', 'add')) {
                    throw new Exception(JrTexto::_('Restricted access').'!!');
                }*/
                $this->breadcrumb = [
                    [ 'texto'=> ucfirst(JrTexto::_('Student Progress Tracking'))],
                ];
                $filtros['iddocente'] = $this->usuarioAct["idpersona"];
                // $filtros["idproyecto"] = $this->usuarioAct["idproyecto"];
                // $this->grupos=$this->oNegGrupos->buscar($filtros);
                $this->grupos=$this->oNegGrupoAulaDet->buscar($filtros);
                $this->locales=[];
                $arrLocales = [];
                if(!empty($this->grupos)){
                    foreach ($this->grupos as $g) {
                        if(!in_array($g['idlocal'], $arrLocales)){
                            if(!empty($g['idlocal'])){
                                $this->oNegLocal->idlocal = $g['idlocal'];                        
                                $this->locales[]=$this->oNegLocal->getXid();
                                $arrLocales[]=$g['idlocal'];
                            }
                        }
                    }

                }
                $this->cursos = array();
                $cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>'3'));
                $cursosMat = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));

                foreach($cursosProyecto as $cur){
                    $cur["activo"]=false;           
                    foreach($cursosMat as $curM){ 
                        if($cur["idcurso"]==$curM["idcurso"]){
                            $cur['activo']=true;
                            $this->cursos[]=$cur;
                            break;
                        }
                    }
                }
                $this->miscolegios = $this->oNegGrupoAulaDet->micolegio(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));

                /*$this->alumnos=$this->oNegAlumno->buscar();*/
                $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'seguimientoalumno/inicio';
                //var_dump( $this->documento->plantilla );
                $this->documento->setTitulo(JrTexto::_('Smartracking'), true);
                //$this->esquema = 'docente/panelcontrol';
                $this->esquema = 'docente/trackingdocente';
            }
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function configuracion()
    {
        try {
            global $aplicacion;
            if(!NegSesion::tiene_acceso('Configuracion_docente', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');

            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));

            $this->documento->plantilla = 'seguimientoalumno/inicio';
            $this->documento->setTitulo(JrTexto::_('Setting'), true);
            $this->esquema = 'docente/configuracion';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function curriculum()
    {
        try {
            global $aplicacion;
            if(empty($_GET['id'])){ throw new Exception(JrTexto::_("Teacher not found")); }
            $this->idDocente = (isset($_GET['id']))?$_GET['id']:0;
            $this->idCurso = (isset($_GET['idcurso']))?$_GET['idcurso']:0;
            $this->documento->setTitulo(JrTexto::_('Curriculum'), true);

            $this->oNegPersonal->dni = $this->idDocente;
            $this->docente = $this->oNegPersonal->getXid();

            #var_dump($this->docente);

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('teacher')), ],
                //[ 'texto'=> $this->docente['ape_paterno'].' '.$this->docente['ape_materno'].' '.$this->docente['nombre'] ],
            ];
            $this->documento->plantilla = 'mantenimientos';

            if(!empty($this->idCurso)){
                $this->breadcrumb = [
                   // [ 'texto'=> ucfirst(JrTexto::_('teacher')), 'link'=> '/docente/?id='.$this->idDocente ],
                    [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('information')), 'link'=> '/curso/informacion/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('teacher profile')) ],
                    //[ 'texto'=> $this->docente['ape_paterno'].' '.$this->docente['ape_materno'].' '.$this->docente['nombre'] ],
                ];
                $this->documento->plantilla = 'alumno/curso';
            }
            $this->esquema = 'docente/curriculum';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    // ========================== Funciones Privadas ========================== //
    private function tieneAccesoAlumno()
    {
        $rol = $this->usuarioAct["rol"];
        $this->idCurso = (isset($_GET['idcurso']))?$_GET['idcurso']:null;

        if($rol=="Alumno" && empty($this->idCurso)){ 
            return false;
            //throw new Exception(JrTexto::_("Course not found"));
        }
        $this->cursoActual = $this->oNegMatricula->estaMatriculado($this->idCurso,$this->usuarioAct['idpersona']);
        if(empty($this->cursoActual)){
            return false;
        }
        return true;
    }

    private function elegirCurso()
    {
        try {
            global $aplicacion;
            $this->cursosMatric = $this->oNegMatricula->cursosAlumno(array('idalumno'=>$this->usuarioAct['idpersona'], 'estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            
            $this->documento->setTitulo(JrTexto::_('Smartracking'), true);
            $this->esquema = 'alumno/cursos_matriculados';
            if(@$this->usuarioAct['idproyecto'] === '1'){
                $this->documento->plantilla = 'sintop';
            }else{
                $this->documento->plantilla = 'alumno/curso';
            }
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    private function vistaAlumno()
    {
        try {
            global $aplicacion;
            if(!$this->tieneAccesoAlumno()) {
                $this->elegirCurso();
            } else {
                $this->breadcrumb = [
                    [ 'texto'=> ucfirst(JrTexto::_('course')) , 'link'=> '/curso/?id='.$this->idCurso ],
                    [ 'texto'=> ucfirst(JrTexto::_('tracking')) /*, 'link'=> '/curso/?id='.$this->idCurso*/ ],
                    [ 'texto'=> $this->cursoActual['nombre'] ],
                ];
                #var_dump($this->cursoActual);
                if(@$this->usuarioAct['idproyecto'] === '1'){
                    $this->documento->plantilla = 'sintop';
                }else{
                    $this->documento->plantilla = 'alumno/curso';
                }
                // $this->esquema = 'docente/panelcontrol';
                //Obtener el ultimo tracking
                
                $_ultimavisita = $this->oNegHistorial_sesion->ultimavisita(array('idcurso'=>$this->idCurso,'idproyecto' => $this->usuarioAct['idproyecto'], 'idalumno' => $this->usuarioAct['idpersona'],'lugar' =>'TK'));
                $this->ultimavisita = (!empty($_ultimavisita)) ? $_ultimavisita[0]['ultimavisita'] : null;
                //Registrar la visita del tracking
                $this->actualvisita = date('Y-m-d H:i:s');
                $this->oNegHistorial_sesion->idcurso = $this->idCurso;
                $this->oNegHistorial_sesion->tipousuario = ($this->usuarioAct['rol']=='Alumno')?'A':'P';
                $this->oNegHistorial_sesion->idusuario = $this->usuarioAct['idpersona'];
                $this->oNegHistorial_sesion->lugar = 'TK';
                $this->oNegHistorial_sesion->fechaentrada = $this->actualvisita;
                $this->oNegHistorial_sesion->fechasalida = $this->actualvisita;
                $this->oNegHistorial_sesion->agregar();
                //calcular...

                $this->esquema = 'docente/tracking';
            }
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function entradasalida_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{

            $entradasalida = array();

            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $preparerow = null;$datosalumno=null;
            $entradasalida =  array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));

                if(!empty($matricula)){

                    $recursos = $this->oNegMinedu->getrecursos($curso,array('entrada' => true));
                    $recursos2 = $this->oNegMinedu->getrecursos($curso,array('salida' => true));
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;$nota = 0;
                        $sum_42=0; $sum_52=0; $sum_62=0; $sum_72=0;$nota2 = 0;
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        $count_42=0; $count_52=0; $count_62=0; $count_72=0;
                        if(!empty($recursos)){
                            foreach($recursos as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    $nota = ($examenactual[0]['nota']) * 0.20;
                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;

                                }//endif examenactual
                            }//end ofreach recursos
                        }//end if recurso
                        if(!empty($recursos2)){
                            foreach($recursos2 as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    $nota2 = ($examenactual[0]['nota']) * 0.20;
                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_42,$count_42) :  $sum_42;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_52,$count_52) :  $sum_52;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_62,$count_62) :  $sum_62;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_72,$count_72) :  $sum_72;
                                }//endif examenactual
                            }//end foreach
                        }

                        $preparerow['alumno'] = $alumno['nombreAlumno'];
                        $preparerow['e_nota'] = $nota;
                        $preparerow['e_4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $preparerow['e_5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $preparerow['e_6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $preparerow['e_7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $preparerow['s_nota'] = $nota2;
                        $preparerow['s_4'] = (!empty($sum_42)) ? round($sum_42 / $count_42) : $sum_42;
                        $preparerow['s_5'] = (!empty($sum_52)) ? round($sum_52 / $count_52) : $sum_52;
                        $preparerow['s_6'] = (!empty($sum_62)) ? round($sum_62 / $count_62) : $sum_62;
                        $preparerow['s_7'] = (!empty($sum_72)) ? round($sum_72 / $count_72) : $sum_72;
                        $datosalumno[] = $preparerow;
                    }//end foreach
                }//end if matricula
                $entradasalida = array('resultados' => $datosalumno);
            }

            $data=array('code'=>'ok','data'=>$entradasalida);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function habilidades_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $preparerow = null;$datosalumno=null;

            $habilidades = array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                $tmp_cursoname = $this->oNegAcad_curso->buscar(array('idcurso' => $curso,'estado'=>1, "idproyecto"=>'3'));
                $cursoname = (!empty($tmp_cursoname)) ? strtoupper($tmp_cursoname[0]['nombre']) : 'A1';
                if(!empty($matricula)){
                    foreach($matricula as $key => $alumno){
                        $hab_4=0; $hab_5=0; $hab_6=0; $hab_7=0;
                        $obtenerprogresos = $this->oNegMinedu->progresosresumen(array('idalumno' => $alumno['idalumno']));
                        if(!empty($obtenerprogresos)){
                            $hab_4 = $obtenerprogresos[0]['prog_hab_L_'.$cursoname];
                            $hab_5 = $obtenerprogresos[0]['prog_hab_R_'.$cursoname];
                            $hab_6 = $obtenerprogresos[0]['prog_hab_W_'.$cursoname];
                            $hab_7 = $obtenerprogresos[0]['prog_hab_S_'.$cursoname];
                        }
                        $preparerow['alumno'] = $alumno['nombreAlumno'];
                        $preparerow['Listening'] = $hab_4;
                        $preparerow['Reading'] = $hab_5;
                        $preparerow['Writing'] = $hab_6;
                        $preparerow['Speaking'] = $hab_7;
                        $datosalumno[] = $preparerow;
                    }
                }
                $habilidades = array('resultados' => $datosalumno);
            }
            
            $data=array('code'=>'ok','data'=>$habilidades);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function usodominio_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $preparerow = null;
            $datosalumno = array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente)){
                $tiemposmatricula = $this->oNegMinedu->buscartiempos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'idcurso' => $curso,
                    'idgrado' => $idgrado,
                    'idlocal' => $idlocal
                ));
                foreach($tiemposmatricula as $key => $value){
                    $P = ($value['tiempopv'] != 0) ? (100 * ($value['tiempopv']/ 3600)) / (20) : 0;
                    $S = ($value['smartquiz'] != 0) ? (100 * ($value['smartquiz']/ 3600)) / (10) : 0;
                    $A = ($value['activity'] != 0) ? (100 * ($value['activity']/ 3600)) / (10) : 0;
                    $SM = ($value['smartbook'] != 0) ? (100 * ($value['smartbook']/ 3600)) / (10) : 0;
                    $H = ($value['homework'] != 0) ? (100 * ($value['homework']/ 3600)) / (5) : 0;

                    $P = ($P > 100) ? 100 : $P;
                    $S = ($S > 100) ? 100 : $S;
                    $A = ($A > 100) ? 100 : $A;
                    $SM = ($SM > 100) ? 100 : $SM;
                    $H = ($H > 100) ? 100 : $H;

                    $D = $A+$P+$S+$SM+$H;
                    $D = ($D == 0) ? $D : (100*$D) / 500;
                    $preparerow['alumno'] = $value['nombre'];
                    $preparerow['dominio'] = round($D,2);
                    $preparerow['tiempopv'] = round($P,2);
                    $preparerow['Activity'] = round($A,2);
                    $preparerow['SmartQuiz'] = round($S,2);
                    $preparerow['SmartBook'] = round($SM,2);
                    $preparerow['HomeWork'] = round($H,2);
                    $datosalumno[]= $preparerow;
                }
                $tiempos = array('usodominio' => $datosalumno);
            }
            $data=array('code'=>'ok','data'=>$tiempos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function tiempos_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            /**Receive params */
            $tiempoactual = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : date('Y-m-d H:i:s');
            
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $fecha = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : null;
            $ultimos = null; $actual = null;
            $tiempos = array();
            /**Make the logic controller */
            if(!is_null($idgrupoauladetalle) && !is_null($iddocente)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                if(!empty($matricula)){
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $tiempopvnow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                            'idusuario' => $idalumno,
                            'lugar' => 'P')
                        ));
                        $smartquiznow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                            'idusuario' => $idalumno,
                            'lugar' => 'E')
                        ));
                        $smartbooknow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                            'idusuario' => $idalumno,
                            'lugar' => 'TR')
                        ));
                        $homeworknow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                            'idusuario' => $idalumno,
                            'lugar' => 'T')
                        ));
                        $alumnodate['alumno'] = $alumno['nombreAlumno'];
                        $alumnodate['total'] = NegTools::conversorSegundosHoras($tiempopvnow[0]['tiempo']+$smartquiznow[0]['tiempo']+$smartbooknow[0]['tiempo']+$homeworknow[0]['tiempo']);
                        $alumnodate['tiempopv'] = NegTools::conversorSegundosHoras($tiempopvnow[0]['tiempo']);
                        $alumnodate['smartquiz'] = NegTools::conversorSegundosHoras($smartquiznow[0]['tiempo']);
                        $alumnodate['smartbook'] = NegTools::conversorSegundosHoras($smartbooknow[0]['tiempo']);
                        $alumnodate['homework'] = NegTools::conversorSegundosHoras($homeworknow[0]['tiempo']);
                        $actual[]= $alumnodate;

                        if($fecha != null){
                            $tiempopv = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                                'idusuario' => $idalumno,
                                'lugar' => 'P',
                                'hastafecha' => $fecha)
                            ));
                            $smartquiz = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                                'idusuario' => $idalumno,
                                'lugar' => 'E',
                                'hastafecha' => $fecha)
                            ));
                            $smartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                                'idusuario' => $idalumno,
                                'lugar' => 'TR',
                                'hastafecha' => $fecha)
                            ));
                            $homework = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                                'idusuario' => $idalumno,
                                'lugar' => 'T',
                                'hastafecha' => $fecha)
                            ));
                            $ultimosalumnos['total'] = NegTools::conversorSegundosHoras($tiempopv[0]['tiempo']+$smartquiz[0]['tiempo']+$smartbook[0]['tiempo']+$homework[0]['tiempo']);
                            $ultimosalumnos['tiempopv'] = NegTools::conversorSegundosHoras($tiempopv[0]['tiempo']);
                            $ultimosalumnos['smartquiz'] = NegTools::conversorSegundosHoras($smartquiz[0]['tiempo']);
                            $ultimosalumnos['smartbook'] = NegTools::conversorSegundosHoras($smartbook[0]['tiempo']);
                            $ultimosalumnos['homework'] = NegTools::conversorSegundosHoras($homework[0]['tiempo']);
                            $ultimos[] = $ultimosalumnos;
                        }
                    }//endforeach alumno
                    $tiempos = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
                }//end if matricula
            }//end if validation

            $data=array('code'=>'ok','data'=>$tiempos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function tiempos(){
        $this->documento->plantilla = 'returnjson';
        try{
            $tiempoactual = (isset($_REQUEST['actual'])) ? $_REQUEST['actual'] : date('Y-m-d H:i:s');
            $tiempoanterior = (isset($_REQUEST['anterior'])) ? $_REQUEST['anterior'] : null;
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $idalumno = $this->usuarioAct['idpersona'];
            $tiempos = array();

            if($curso != null){
                $tiempopvnow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                    'idusuario' => $idalumno,
                    'lugar' => 'P')
                ));
                $smartquiznow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                    'idusuario' => $idalumno,
                    'lugar' => 'E')
                ));
                $smartbooknow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                    'idusuario' => $idalumno,
                    'lugar' => 'TR')
                ));
                $homeworknow = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                    'idusuario' => $idalumno,
                    'lugar' => 'T')
                ));
    
                $actual['total'] = NegTools::conversorSegundosHoras($tiempopvnow[0]['tiempo']+$smartquiznow[0]['tiempo']+$smartbooknow[0]['tiempo']+$homeworknow[0]['tiempo']);
                $actual['tiempopv'] = NegTools::conversorSegundosHoras($tiempopvnow[0]['tiempo']);
                $actual['smartquiz'] = NegTools::conversorSegundosHoras($smartquiznow[0]['tiempo']);
                $actual['smartbook'] = NegTools::conversorSegundosHoras($smartbooknow[0]['tiempo']);
                $actual['homework'] = NegTools::conversorSegundosHoras($homeworknow[0]['tiempo']);
    
                if($tiempoanterior != null){
                    $tiempopv = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                        'idusuario' => $idalumno,
                        'lugar' => 'P',
                        'hastafecha' => $tiempoanterior)
                    ));
                    $smartquiz = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                        'idusuario' => $idalumno,
                        'lugar' => 'E',
                        'hastafecha' => $tiempoanterior)
                    ));
                    $smartbook = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                        'idusuario' => $idalumno,
                        'lugar' => 'TR',
                        'hastafecha' => $tiempoanterior)
                    ));
                    $homework = $this->oNegReportealumno->buscar(array('tipo'=>'tiempo_pv','cond'=>array(
                        'idusuario' => $idalumno,
                        'lugar' => 'T',
                        'hastafecha' => $tiempoanterior)
                    ));
                    $ultimos['total'] = NegTools::conversorSegundosHoras($tiempopv[0]['tiempo']+$smartquiz[0]['tiempo']+$smartbook[0]['tiempo']+$homework[0]['tiempo']);
                    $ultimos['tiempopv'] = NegTools::conversorSegundosHoras($tiempopv[0]['tiempo']);
                    $ultimos['smartquiz'] = NegTools::conversorSegundosHoras($smartquiz[0]['tiempo']);
                    $ultimos['smartbook'] = NegTools::conversorSegundosHoras($smartbook[0]['tiempo']);
                    $ultimos['homework'] = NegTools::conversorSegundosHoras($homework[0]['tiempo']);
                }else{
                    $ultimos['total'] = $actual['total'];
                    $ultimos['tiempopv'] = $actual['tiempopv'];
                    $ultimos['smartquiz'] = $actual['smartquiz'];
                    $ultimos['smartbook'] = $actual['smartbook'];
                    $ultimos['homework'] = $actual['homework'];
                }//end if tiempoactual
                
                
                $tiempos = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);

            }//end if curso


            
            $data=array('code'=>'ok','data'=>$tiempos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function examenes_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $actualTRI = null; $actual = null;

            $examenes = array();
            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                if(!empty($matricula)){
                    $recursos = $this->oNegMinedu->getrecursos($curso,array('bimestre' => true));
                    $recursos_t = $this->oNegMinedu->getrecursos($curso,array('trimestre' => true));
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                        $notas = array();
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        if(!empty($recursos)){
                            foreach($recursos as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    
                                    $notas[] = ($examenactual[0]['nota']) * 0.20;

                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                }//end if
                            }//endforeach recurso 1
                        }//end if recurso 1

                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['nota1'] = (isset($notas[0])) ? $notas[0] : 0;
                        $actualalumno['nota2'] = (isset($notas[1])) ? $notas[1] : 0;
                        $actualalumno['nota3'] = (isset($notas[2])) ? $notas[2] : 0;
                        $actualalumno['nota4'] = (isset($notas[3])) ? $notas[3] : 0;
                        $actualalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $actual[] = $actualalumno;

                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                        $notas = array();
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        if(!empty($recursos_t)){
                            foreach($recursos_t as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    
                                    $notas[] = ($examenactual[0]['nota']) * 0.20;

                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                }//end if
                            }//endforeach recurso 2
                        }//end if recurso 2

                        $actualalumnoTRI['alumno'] = $alumno['nombreAlumno'];
                        $actualalumnoTRI['nota1'] = (isset($notas[0])) ? $notas[0] : 0;
                        $actualalumnoTRI['nota2'] = (isset($notas[1])) ? $notas[1] : 0;
                        $actualalumnoTRI['nota3'] = (isset($notas[2])) ? $notas[2] : 0;
                        $actualalumnoTRI['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumnoTRI['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumnoTRI['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumnoTRI['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $actualTRI[] = $actualalumnoTRI;
                    }//end foreach matricula
                }//end if matricula
                $examenes = array('trimestre' => $actualTRI, 'bimestre' => $actual);
            }//end if  validation

            $data=array('code'=>'ok','data'=>$examenes);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function salida_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{

            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $ultimos = null; $actual = null;
            
            $examen = array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));

                if(!empty($matricula)){
                    $recursos = $this->oNegMinedu->getrecursos($curso,array('salida' => true));
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;$nota = 0;
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        if(!empty($recursos)){
                            foreach($recursos as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    
                                    $nota = ($examenactual[0]['nota']) * 0.20;

                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                }//end if
                            }//end foreach recurso
                            
                        }//endif recurso

                        
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['nota'] = $nota;
                        $actualalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $actual[] = $actualalumno;
                    }// end foreach
                    
                }//end if matricula

                $examen = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
            }//end if validation


            $data=array('code'=>'ok','data'=>$examen);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function entrada_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{

            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $ultimos = null; $actual = null;
            
            $examen = array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));

                if(!empty($matricula)){
                    $recursos = $this->oNegMinedu->getrecursos($curso,array('entrada' => true));
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;$nota = 0;
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        if(!empty($recursos)){
                            $flag = false;
                            foreach($recursos as $value){
                                $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    
                                    $nota = ($examenactual[0]['nota']) * 0.20;

                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                }//end if
                            }//end foreach recurso
                            
                        }//endif recurso

                        
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['nota'] = $nota;
                        $actualalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $actual[] = $actualalumno;
                    }// end foreach
                    
                }//end if matricula

                $examen = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
            }//end if validation


            $data=array('code'=>'ok','data'=>$examen);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function ubicacion_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{

            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $ultimos = null; $actual = null;
            
            $examen = array();

            if(!is_null($idgrupoauladetalle) && !is_null($iddocente)){
                
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));

                if(!empty($matricula)){
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;$nota = 0;
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        $nombreNivel = 'A1';

                        $examenactual = $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'tipo' => 'U'));
                        if(!empty($examenactual)){
                            
                            $nota = ($examenactual[0]['nota']) * 0.20;

                            $json = json_decode($examenactual[0]['calificacion_total']);
                            $notaInt = intval($examenactual[0]['nota']);
                            $max = 20;
                            foreach($json as $value){
                                $max = intval($value->max);
                                $min = intval($value->min);
                                if($notaInt >= $min && $notaInt <= $max){
                                    $nombreNivel = $value->nombre;
                                    break;
                                }
                            }
                            $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                            $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                            $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                            $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                            $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                        }//end if
                            

                        
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['curso'] = $nombreNivel;
                        $actualalumno['nota'] = $nota;
                        $actualalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        $actual[] = $actualalumno;
                    }// end foreach
                    
                }//end if matricula

                $examen = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
            }//end if validation


            $data=array('code'=>'ok','data'=>$examen);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function progreso_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            /**Receive params */
            $tiempoactual = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : date('Y-m-d H:i:s');
            
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $ultimos = null; $actual = null;
            $progresos = array();
            /**Make the logic controller */
            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                $tmp_cursoname = $this->oNegAcad_curso->buscar(array('idcurso' => $curso,'estado'=>1, "idproyecto"=>'3'));
                $cursoname = (!empty($tmp_cursoname)) ? strtoupper($tmp_cursoname[0]['nombre']) : 'A1';
                if(!empty($matricula)){
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $hab_4=0; $hab_5=0; $hab_6=0; $hab_7=0;
                        $prog_curso=0;
                        $obtenerprogresos = $this->oNegMinedu->progresosresumen(array('idalumno' => $alumno['idalumno']));
                        if(!empty($obtenerprogresos)){
                            $hab_4 = $obtenerprogresos[0]['prog_hab_L_'.$cursoname];
                            $hab_5 = $obtenerprogresos[0]['prog_hab_R_'.$cursoname];
                            $hab_6 = $obtenerprogresos[0]['prog_hab_W_'.$cursoname];
                            $hab_7 = $obtenerprogresos[0]['prog_hab_S_'.$cursoname];
                            $prog_curso=$obtenerprogresos[0]['prog_curso_'.$cursoname];
                        }
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['4'] = $hab_4; 
                        $actualalumno['5'] = $hab_5; 
                        $actualalumno['6'] = $hab_6; 
                        $actualalumno['7'] = $hab_7;
                        $actualalumno['progresopv'] = $prog_curso;

                        // $progresoactual = $this->oNegActividad_alumno->countprogreso(array('idpersona' => $idalumno));
                        // $key = array_search($curso, array_column($progresoactual, 'idcurso'));
                        
                        // if(!is_bool($key)){
                        //     $actualalumno['4'] = (!empty($progresoactual) && !empty($progresoactual[$key]['total_L_T'])) ? round(( intval($progresoactual[$key]['total_L_T'])*100 / intval($progresoactual[$key]['total_L'])),2) : 0; 
                        //     $actualalumno['5'] = (!empty($progresoactual) && !empty($progresoactual[$key]['total_R_T'])) ? round(( intval($progresoactual[$key]['total_R_T'])*100 / intval($progresoactual[$key]['total_R'])),2) : 0; 
                        //     $actualalumno['6'] = (!empty($progresoactual) && !empty($progresoactual[$key]['total_W_T'])) ? round(( intval($progresoactual[$key]['total_W_T'])*100 / intval($progresoactual[$key]['total_W'])),2) : 0; 
                        //     $actualalumno['7'] = (!empty($progresoactual) && !empty($progresoactual[$key]['total_S_T'])) ? round(( intval($progresoactual[$key]['total_S_T'])*100 / intval($progresoactual[$key]['total_S'])),2) : 0;
                        // }else{
                        //     $actualalumno['4'] = 0; 
                        //     $actualalumno['5'] = 0; 
                        //     $actualalumno['6'] = 0; 
                        //     $actualalumno['7'] = 0;
                        // }//endif booleankey

                        // $progresoalumno = $this->oNegAcad_cursodetalle->getprogresounidad(array('idcurso' => $curso, 'idusuario' => $idalumno));
                        // $progresopv = 0;
                        // if(!empty($progresoalumno)){
                        //     foreach($progresoalumno as $progresovalue){
                        //         $progresopv += $progresovalue['progreso'];
                        //     }    
                        // }//end if progresoalumno
                        // $actualalumno['progresopv'] = (!empty($progresopv)) ? ($progresopv / 8) : 0;
                        $actual[] = $actualalumno;
                    }//endforeach matricula
                    $progresos = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
                }//endif matricula
                
            } //end if validation

            $data=array('code'=>'ok','data'=>$progresos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function progresoxactividad_grupo(){
        $this->documento->plantilla = 'returnjson';
        try{
            /**Receive params */
            $tiempoactual = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : date('Y-m-d H:i:s');
            
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $fecha = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : null;
            $ultimos = null; $actual = null;
            $progresos = array();
            /**Make the logic controller */
            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                if(!empty($matricula)){
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $progresoactual = $this->oNegActividad_alumno->countprogreso(array('idpersona' => $idalumno));
                        $key = array_search($curso, array_column($progresoactual, 'idcurso'));
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        if(!is_bool($key)){
                            $actualalumno['4'] = (!empty($progresoactual)) ? round(( intval($progresoactual[$key]['total_L_T'])*100 / intval($progresoactual[$key]['total_L'])),2) : 0; 
                            $actualalumno['5'] = (!empty($progresoactual)) ? round(( intval($progresoactual[$key]['total_R_T'])*100 / intval($progresoactual[$key]['total_R'])),2) : 0; 
                            $actualalumno['6'] = (!empty($progresoactual)) ? round(( intval($progresoactual[$key]['total_W_T'])*100 / intval($progresoactual[$key]['total_W'])),2) : 0; 
                            $actualalumno['7'] = (!empty($progresoactual)) ? round(( intval($progresoactual[$key]['total_S_T'])*100 / intval($progresoactual[$key]['total_S'])),2) : 0;
                        }else{
                            $actualalumno['4'] = 0; 
                            $actualalumno['5'] = 0; 
                            $actualalumno['6'] = 0; 
                            $actualalumno['7'] = 0;
                        }//endif booleankey
                        $actual[] = $actualalumno;

                        if($fecha != null){
                            $progresoultimo = $this->oNegActividad_alumno->countprogreso(array('idpersona' => $idalumno,'fecha' => $fecha));
                            $key = array_search($curso, array_column($progresoultimo, 'idcurso'));
                            if(!is_bool($key)){
                                $ultimoalumno['4'] = (!empty($progresoultimo)) ? round(( intval($progresoultimo[$key]['total_L_T']) *100 / intval($progresoultimo[$key]['total_L'])),2) : 0; 
                                $ultimoalumno['5'] = (!empty($progresoultimo)) ? round(( intval($progresoultimo[$key]['total_R_T']) *100 / intval($progresoultimo[$key]['total_R'])),2) : 0; 
                                $ultimoalumno['6'] = (!empty($progresoultimo)) ? round(( intval($progresoultimo[$key]['total_W_T']) *100 / intval($progresoultimo[$key]['total_W'])),2) : 0; 
                                $ultimoalumno['7'] = (!empty($progresoultimo)) ? round(( intval($progresoultimo[$key]['total_S_T']) *100 / intval($progresoultimo[$key]['total_S'])),2) : 0;
                            }else{
                                $ultimoalumno['4'] = 0;
                                $ultimoalumno['5'] = 0;
                                $ultimoalumno['6'] = 0;
                                $ultimoalumno['7'] = 0;    
                            }//end if key
                            $ultimos[] = $ultimoalumno;
                        }
                    }//endforeach matricula
                    $progresos = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
                }//endif matricula
                
            } //end if validation

            $data=array('code'=>'ok','data'=>$progresos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function progresoxactividad(){
        $this->documento->plantilla = 'returnjson';
        try {
            $tiempoactual = (isset($_REQUEST['actual'])) ? $_REQUEST['actual'] : date('Y-m-d H:i:s');
            $tiempoanterior = (isset($_REQUEST['anterior'])) ? $_REQUEST['anterior'] : null;
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;

            $progresos = array();

            if($curso != null){
                $progresoactual = $this->oNegActividad_alumno->countprogreso(array('idpersona' => $this->usuarioAct['idpersona']));
                $key = array_search($curso, array_column($progresoactual, 'idcurso'));
                $actual = null;
                $ultimo = null;
                if(!is_bool($key)){
                    $actual['4'] = (!empty($progresoactual)) ? round((intval($progresoactual[$key]['total_L']) / intval($progresoactual[$key]['total_L_T'])),2) : 0; 
                    $actual['5'] = (!empty($progresoactual)) ? round((intval($progresoactual[$key]['total_R']) / intval($progresoactual[$key]['total_R_T'])),2) : 0; 
                    $actual['6'] = (!empty($progresoactual)) ? round((intval($progresoactual[$key]['total_W']) / intval($progresoactual[$key]['total_W_T'])),2) : 0; 
                    $actual['7'] = (!empty($progresoactual)) ? round((intval($progresoactual[$key]['total_S']) / intval($progresoactual[$key]['total_S_T'])),2) : 0;
                    
                    if($tiempoanterior != null){
                        $progresoultimo = $this->oNegActividad_alumno->countprogreso(array('idpersona' => $this->usuarioAct['idpersona'],'fecha' => $tiempoanterior));
                        $key = array_search($curso, array_column($progresoultimo, 'idcurso'));
                        if(!is_bool($key)){
                            $ultimo['4'] = (!empty($progresoultimo)) ? round((intval($progresoultimo[$key]['total_L']) / intval($progresoultimo[$key]['total_L_T'])),2) : 0; 
                            $ultimo['5'] = (!empty($progresoultimo)) ? round((intval($progresoultimo[$key]['total_R']) / intval($progresoultimo[$key]['total_R_T'])),2) : 0; 
                            $ultimo['6'] = (!empty($progresoultimo)) ? round((intval($progresoultimo[$key]['total_W']) / intval($progresoultimo[$key]['total_W_T'])),2) : 0; 
                            $ultimo['7'] = (!empty($progresoultimo)) ? round((intval($progresoultimo[$key]['total_S']) / intval($progresoultimo[$key]['total_S_T'])),2) : 0;
                        }else{
                            $ultimo['4'] = $actual['4'];
                            $ultimo['5'] = $actual['5'];
                            $ultimo['6'] = $actual['6'];
                            $ultimo['7'] = $actual['7'];    
                        }//end if key
                    }else{
                        $ultimo['4'] = $actual['4'];
                        $ultimo['5'] = $actual['5'];
                        $ultimo['6'] = $actual['6'];
                        $ultimo['7'] = $actual['7'];
                    }
                }//end if key
                $progresos = array('tiemposanterior' => $ultimo, 'tiemposactual' => $actual);
            }//endcurso

            $data=array('code'=>'ok','data'=>$progresos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    private function sumarHabilidad($valor,$sumaactual,&$contador = null){
        $contador= (!is_null($contador)) ? ($contador + 1) : null;
        return $valor + $sumaactual;
    }

    public function progresoxexamenes_group(){
        $this->documento->plantilla = 'returnjson';
        try{
            /**Receive params */
            $tiempoactual = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : date('Y-m-d H:i:s');
            
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $iddocente = (isset($_REQUEST['iddocente'])) ? $_REQUEST['iddocente'] : null;
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $idgrado = (isset($_REQUEST['idgrado'])) ? $_REQUEST['idgrado'] : null;
            $idgrupoauladetalle = (isset($_REQUEST['idgrupoauladetalle'])) ? $_REQUEST['idgrupoauladetalle'] : null;
            $fecha = (isset($_REQUEST['fecha'])) ? $_REQUEST['fecha'] : null;
            $ultimos = null; $actual = null;
            $progresos = array();
            /**Make the logic controller */
            if(!is_null($idgrupoauladetalle) && !is_null($iddocente) && !is_null($curso)){
                $matricula = $this->oNegGrupoAulaDet->mialumnos(array(
                    'idgrupoauladetalle' => $idgrupoauladetalle,
                    'iddocente'=>$iddocente,
                    'idgrado' => $idgrado
                ));
                if(!empty($matricula)){
                    $recursos = $this->oNegMinedu->getrecursos($curso);
                    foreach($matricula as $alumno){
                        $idalumno = $alumno['idalumno'];
                        $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                        $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                        if(!empty($recursos)){
                            foreach($recursos as $value){
                                $examenactual= $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                                if(!empty($examenactual)){
                                    $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                    $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                    $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                    $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                    $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                }
                            }//end foreach recurso
                        }//endif recurso
                        $actualalumno['alumno'] = $alumno['nombreAlumno'];
                        $actualalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                        $actualalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                        $actualalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                        $actualalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                        
                        $actual[] = $actualalumno;
                        if($fecha != null){
                            $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                            if(!empty($recursos)){
                                foreach($recursos as $value){
                                    $examenactual= $this->oNegNotas_quiz->buscar(array('idalumno' => $idalumno, 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso'],'hastafecha' => $fecha));
                                    if(!empty($examenactual)){
                                        $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                        $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                        $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                        $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                        $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                                    }
                                }//end foreach
                                $ultimoalumno['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4,2) : $sum_4;
                                $ultimoalumno['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5,2) : $sum_5;
                                $ultimoalumno['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6,2) : $sum_6;
                                $ultimoalumno['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7,2) : $sum_7;
                            }else{
                                $ultimoalumno['4'] = 0;
                                $ultimoalumno['5'] = 0;
                                $ultimoalumno['6'] = 0;
                                $ultimoalumno['7'] = 0;
                            }//endif recurso

                            $ultimos[] = $ultimoalumno;
                        }
                    }//endforeach matricula
                    $progresos = array('tiemposanterior' => $ultimos, 'tiemposactual' => $actual);
                }//endif matricula
                
            } //end if validation

            $data=array('code'=>'ok','data'=>$progresos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function progresoxexamenes(){
        $this->documento->plantilla = 'returnjson';
        try {
            $tiempoactual = (isset($_REQUEST['actual'])) ? $_REQUEST['actual'] : date('Y-m-d H:i:s');
            $tiempoanterior = (isset($_REQUEST['anterior'])) ? $_REQUEST['anterior'] : null;
            $curso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
            $progresos = array();

            if( $curso != null){
                //hay que buscar los examenes del curso
                $recursos = $this->oNegMinedu->getrecursos($curso);
                $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                $count_4=0; $count_5=0; $count_6=0; $count_7=0;
                if(!empty($recursos)){
                    foreach($recursos as $value){
                        $examenactual= $this->oNegNotas_quiz->buscar(array('idalumno' => $this->usuarioAct['idpersona'], 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso']));
                        if(!empty($examenactual)){
                            $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                            $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                            $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                            $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                            $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                        }
                    }//end foreach
                }//endif recurso
    
                $actual['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4) : $sum_4;
                $actual['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5) : $sum_5;
                $actual['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6) : $sum_6;
                $actual['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7) : $sum_7;
                if($tiempoanterior != null){
                    $sum_4=0; $sum_5=0; $sum_6=0; $sum_7=0;
                    if(!empty($recursos)){
                        foreach($recursos as $value){
                            $examenactual= $this->oNegNotas_quiz->buscar(array('idalumno' => $this->usuarioAct['idpersona'], 'idproyecto' => $this->usuarioAct['idproyecto'],'idrecurso' => $value['idrecurso'],'hastafecha' => $tiempoanterior));
                            if(!empty($examenactual)){
                                $json_hab = json_decode($examenactual[0]['habilidad_puntaje'],true);
                                $sum_4 = (isset($json_hab['4'])) ? $this->sumarHabilidad($json_hab['4'],$sum_4,$count_4) :  $sum_4;
                                $sum_5 = (isset($json_hab['5'])) ? $this->sumarHabilidad($json_hab['5'],$sum_5,$count_5) :  $sum_5;
                                $sum_6 = (isset($json_hab['6'])) ? $this->sumarHabilidad($json_hab['6'],$sum_6,$count_6) :  $sum_6;
                                $sum_7 = (isset($json_hab['7'])) ? $this->sumarHabilidad($json_hab['7'],$sum_7,$count_7) :  $sum_7;
                            }
                        }//end foreach
                    }//endif recurso
                    $ultimo['4'] = (!empty($sum_4)) ? round($sum_4 / $count_4,2) : $sum_4;
                    $ultimo['5'] = (!empty($sum_5)) ? round($sum_5 / $count_5,2) : $sum_5;
                    $ultimo['6'] = (!empty($sum_6)) ? round($sum_6 / $count_6,2) : $sum_6;
                    $ultimo['7'] = (!empty($sum_7)) ? round($sum_7 / $count_7,2) : $sum_7;
                }else{
                    $ultimo['4'] = $actual['4'];
                    $ultimo['5'] = $actual['5'];
                    $ultimo['6'] = $actual['6'];
                    $ultimo['7'] = $actual['7'];
                }

                 $progresos = array('tiemposanterior' => $ultimo, 'tiemposactual' => $actual);
            }


            $data=array('code'=>'ok','data'=>$progresos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

}