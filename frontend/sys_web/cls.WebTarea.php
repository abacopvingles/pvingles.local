<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       31-05-2017 
 * @copyright   Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
#JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebTarea extends JrWeb
{
    private $oNegTarea;
    private $oNegTarea_asignacion;
    private $oNegTarea_asignacion_alumno;
    protected $oNegNiveles;
    protected $oNegTarea_archivos;
    #protected $oNegGrupos;
    protected $oNegGrupoAulaDet;
    protected $oNegLocal;
    private $oNegTarea_respuesta;
    protected $oNegHabilidad;
    protected $oNegMatricula;
    protected $oNegCursodetalle;
    protected $oNegCurso;

    public function __construct()
    {
        parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegTarea = new NegTarea;
        $this->oNegTarea_asignacion = new NegTarea_asignacion;
        $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegTarea_archivos = new NegTarea_archivos;
        #$this->oNegGrupos = new NegGrupos;
        $this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
        $this->oNegLocal = new NegLocal;
        $this->oNegTarea_respuesta = new NegTarea_respuesta;
        $this->oNegHabilidad = new NegMetodologia_habilidad;
        $this->oNegAcad_habilidad = new NegAcad_cursohabilidad;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
        $this->oNegCurso = new NegAcad_curso;
    }

    public function defecto(){
        return $this->listado();
    }

    public function listado()
    {
        try{
            global $aplicacion;
            /*if(!NegSesion::tiene_acceso('Tarea', 'list') && $this->usuarioAct['rol']!='Alumno') {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }*/
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->miscolegios=$hayidcolegios=array();
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            $this->idgrupoauladetalle;
            $this->idcurso;
            if($this->idrol==3){
                header('Location: '. $this->documento->getUrlBase().'/curso/tarea'); exit(0);
            }else if($this->idrol==1){
                $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
                $this->esquema = 'tarea/tarea-list-edit';
            }else{
                $this->miscolegios=$this->oNegGrupoAulaDet->micolegio(array('iddocente'=>$usu["idpersona"],'idproyecto'=>$usu["idproyecto"]));
                $this->esquema = 'tarea/tarea-asignar';
                if(!empty($this->miscolegios[0])){
                    $this->idgrupoauladetalle=$this->miscolegios[0]["idgrupoauladetalle"];
                    if(!empty($this->miscolegios[0]["cursos"])){
                        $cursos=$this->miscolegios[0]["cursos"];
                        if(!empty($cursos[0])){
                            $this->idgrupoauladetalle=$cursos[0]["idgrupoauladetalle"];
                            $this->idcurso=$cursos[0]["idcurso"];
                            if(!empty($cursos[0]["grados"])){
                                $grados=$cursos[0]["grados"];
                                if(!empty($grados[0])){
                                    $this->idgrupoauladetalle=$grados[0]["idgrupoauladetalle"];
                                    if(!empty($grados[0]["secciones"])){
                                        $secciones=$grados[0]["secciones"];
                                        if(!empty($secciones[0])){
                                            $this->idgrupoauladetalle=$secciones[0]["idgrupoauladetalle"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $pend_final = $this->listarTareasPendientesYFinalizadas(true,$this->idgrupoauladetalle,$this->idcurso);
            }
            

            
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->documento->setTitulo(JrTexto::_('Activity'), true);
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function agregar()
    {
        try {
            global $aplicacion;
            $this->getPlugins();
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            if($this->rol==1){
                $this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            }else if($this->rol==2){
                $this->cursos=$this->oNegGrupoaulaDetalle->buscar(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));
            }else {
                $this->listado();
            }
            /*$this->cursos = $this->oNegGrupoAulaDet->cursosDocente(array("iddocente"=>$this->usuarioAct["idpersona"]));
            // var_dump($this->usuarioAct);
            if( NegSesion::tieneRol('Administrador') ) {
                $otrosCursos = $this->oNegCurso->getCursos_Not_In_GrupoAulaDetalle(array("estado"=>1), $this->usuarioAct["idpersona"]);
                if(!empty($otrosCursos)){
                    foreach ($otrosCursos as $c) { $this->cursos[] = $c; }
                }
            }*/
            $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>1));
            $this->frmaccion='Nuevo';
            $this->breadcrumb=JrTexto::_('Add');
            $this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('New'), true);
            return $this->form();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function redirigir() {
        try {
            global $aplicacion;
            if($this->usuarioAct["rol"]=="Alumno") {
                header('Location: '. $this->documento->getUrlBase().'/curso/tarea'); exit(0);
            } else {
                header('Location: '. $this->documento->getUrlBase().'/tarea'); exit(0);
            }
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function asignar(){
        try {
            global $aplicacion;
            $this->getPlugins();
            $usuarioAct = NegSesion::getUsuario();
            $this->frmaccion='Editar';
            $this->breadcrumb=JrTexto::_('Edit');
            $this->id=@$_GET['id'];
            $tareas = $this->oNegTarea->buscar(array('idtarea'=>$this->id));//, 'iddocente'=>$usuarioAct["dni"]
            if(empty($tareas[0])){ throw new Exception(JrTexto::_('Activity not found')); }
            $this->datos = $tareas[0];
            $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->id));
            $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('fechahora_mayorigual'=>date('Y-m-d H:i:s'), 'idtarea'=>$this->id, 'iddocente'=>$usuarioAct["idpersona"]));
            $this->datos["curso_detalles"] = $this->oNegCursodetalle->getTodosPadresXCursoDet(array("idcursodetalle"=>$this->datos['idcursodetalle']),'DESC');
            $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>1));
            $usu=$this->usuarioAct;
            $this->tareasAsignadas=array();
            $this->idrol=$usu['idrol'];
            if($this->idrol==2){
                $this->miscolegios=$this->oNegGrupoAulaDet->micolegio(array('iddocente'=>$usu["idpersona"],'idproyecto'=>$usu["idproyecto"]));
                if(!empty($this->datos['tarea_asignacion']))
                foreach($this->datos['tarea_asignacion'] as $tareasiganda){
                    $detallegrupoaula=$this->oNegGrupoAulaDet->buscar(array('idgrupoauladetalle'=>$tareasiganda["idgrupo"]));
                    if(!empty($detallegrupoaula[0])){
                    $tareasiganda["idcurso"]=$detallegrupoaula[0]["idcurso"];
                    $tareasiganda["idgrado"]=$detallegrupoaula[0]["idgrado"];
                    $tareasiganda["idseccion"]=$detallegrupoaula[0]["idgrupoauladetalle"];
                    }
                    $this->tareasAsignadas[]=$tareasiganda;
                }
            }
            
            $this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('Edit'), true);
            $this->esquema = 'tarea/tarea-asignaralumnos';
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    public function editar()
    {
        try {
            global $aplicacion;
            $this->getPlugins();
            
            $usuarioAct = NegSesion::getUsuario();
            $this->frmaccion='Editar';
            $this->breadcrumb=JrTexto::_('Edit');
            $this->pk=@$_GET['id'];
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            if($this->rol==1){
                $this->cursos = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            }else if($this->rol==2){
                $this->cursos=$this->oNegGrupoaulaDetalle->buscar(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));
            }else {
                $this->listado();
            }
            if(!empty($this->cursos)&&!empty($_GET["idcurso"])){
                $this->idcurso=$_GET["idcurso"];
            }
               

            $this->oNegTarea->setLimite(0,9999);
            $tareas = $this->oNegTarea->buscar(array('idtarea'=>$_GET['id']));
            if(empty($tareas)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = $tareas[0];
            $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));
           // $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('fechahora_mayorigual'=>date('Y-m-d H:i:s'), 'idtarea'=>$this->datos['idtarea']));
            $this->datos["curso_detalles"] = $this->oNegCursodetalle->getTodosPadresXCursoDet(array("idcursodetalle"=>$this->datos['idcursodetalle']),'DESC');
            $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>1));
           // var_dump($this->habilidades);
            /*$this->grupos=$this->oNegGrupoAulaDet->buscar(array('iddocente'=>$usuarioAct["idpersona"]));
            $this->locales=[];
            $arrLocales = [];
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $buscarLocales = $this->oNegLocal->buscar(array("idlocal"=>$g['idlocal']));
                    if(!empty($buscarLocales)) {
                        $this->locales[] = $buscarLocales[0];
                    }
                    $arrLocales[]=$g['idlocal'];
                }
            }*/
            
            $this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('Edit'), true);
            return $this->form();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function ver()
    {
        try{
            global $aplicacion; 
            /*bibliotecas JS y CSS*/
            $this->getPlugins();

            $usuarioAct = NegSesion::getUsuario();
            $this->breadcrumb=JrTexto::_('View');
            $filtroTarAsig = array();
            if($usuarioAct['idrol']=="3"){
                $this->oNegTarea_asignacion_alumno->iddetalle = @$_GET['id'];
                $asignacion_alumno = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;

                $idTarea_asignacion = $asignacion_alumno['idtarea_asignacion'];
            }else{
                $idTarea_asignacion = @$_GET['id'];
                $filtroTarAsig['iddocente'] = $usuarioAct["idpersona"];
            }
            $filtroTarAsig['idtarea_asignacion'] = $idTarea_asignacion;
            $tarea_asignacion = $this->oNegTarea_asignacion->buscar($filtroTarAsig);
            if(empty($tarea_asignacion)){ throw new Exception(JrTexto::_('Assignment not found')); }
            $tarea_asignacion = $tarea_asignacion[0];
            $filtrosTarea = array('idtarea'=>$tarea_asignacion['idtarea']);

            $this->oNegTarea->setLimite(0,9999);
            $tareas = $this->oNegTarea->buscar($filtrosTarea);
            if(empty($tareas)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = array_merge($tareas[0], $tarea_asignacion);
            $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));

            $filtrosAsignAlum=array();
            if($usuarioAct['rol']=="Alumno"){ $filtrosAsignAlum['idalumno']= $usuarioAct['idpersona']; }

            $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('idtarea_asignacion'=>$tarea_asignacion['idtarea_asignacion']), $filtrosAsignAlum);
            
            $i=0;
            if(!empty($this->datos['tarea_asignacion'])){
                $this->datos['tarea_asignacion']= $this->datos['tarea_asignacion'][0];
                foreach ($this->datos['tarea_asignacion']['detalle'] as $asig_alum) {
                    $hoy= new DateTime(date('Y-m-d H:i:s'));
                    $fecha_vencimiento= new DateTime($this->datos['fechaentrega'].' '.$this->datos['horaentrega']);
                    if($fecha_vencimiento<$hoy && $this->datos['tarea_asignacion']['detalle'][$i]['estado']!='E'){
                        $this->datos['tarea_asignacion']['detalle'][$i]['estado']='P';
                        $this->oNegTarea_asignacion_alumno->iddetalle=$asig_alum['iddetalle'];
                        $this->oNegTarea_asignacion_alumno->__set('estado', 'P');
                        $this->oNegTarea_asignacion_alumno->editar();
                    }
                    if($asig_alum['estado']=='P' || $asig_alum['estado']=='E' || $usuarioAct['rol']=="Alumno"){
                        $tarea_resp=$this->oNegTarea_respuesta->buscarConArchivos(array('idtarea_asignacion_alumno'=>$asig_alum['iddetalle']));
                        if(!empty($tarea_resp)){
                            $this->datos['tarea_asignacion']['detalle'][$i]=array_merge($this->datos['tarea_asignacion']['detalle'][$i], $tarea_resp[0]);
                        }
                    }
                    $i++;
                }
            }

            $this->datos['curso_detalles']=$this->oNegCursodetalle->getTodosPadresXCursoDet(array('idcursodetalle'=>$this->datos['idcursodetalle']));
            $this->datos['curso']=$this->oNegCurso->buscar(array('idcurso'=>$this->datos['curso_detalles'][0]['idcurso']))[0];

            $this->habilidades = $this->habilidadColor = $arrHabilidades = array();
            foreach ($this->datos['curso_detalles'] as $cdet) {
                $filtros = array(
                    'idcurso' => $this->datos['curso_detalles'][0]['idcurso'],
                    'idcursodetalle' => $cdet['idcursodetalle'],
                );
                $arrHabilidades = $this->oNegAcad_habilidad->buscar($filtros);
                if(!empty($arrHabilidades)) return false;
            }
            if(empty($arrHabilidades)) {
                $arrHabilidades=$this->oNegAcad_habilidad->buscar(array(
                    'idcurso' => $this->datos['curso_detalles'][0]['idcurso']
                ));
            }
            $arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
           
            foreach ($arrHabilidades as $index=>$hab) {
                $this->habilidades[$hab["idcursohabilidad"]]=$hab["texto"];
                $indice = $index % count($arrColoresHab);
                $this->habilidadColor[$hab["idcursohabilidad"]]=$arrColoresHab[$indice];
            } 

            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('Activities'), true);
            if($usuarioAct['idrol']=="3"){
                $this->esquema = 'tarea/tarea-see-alum';
            }else{
                $this->esquema = 'tarea/tarea-see';
            }
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function practicar()
    {
        try {
            global $aplicacion;
            $this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
            $this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
            $this->idHabilidad = !empty(@$_REQUEST["idhab"])?$_REQUEST["idhab"]:0;

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Practice')) , 'link'=> '/tarea/practicar/'.($this->idHabilidad==0?'':'?idhab='.$this->idHabilidad)],
            ];

            $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>'1'));

            $this->cursos_nivel = array();
            if($this->idCurso==0) {
                $this->cursos_nivel = $this->oNegMatricula->cursosAlumno(array("idalumno"=>$this->usuarioAct["idpersona"]));
                $this->breadcrumb[] = [ 'texto'=> ucfirst(JrTexto::_('Courses')), ];
            }

            $this->esquema = 'tarea/practicar';
            $this->documento->plantilla = 'mantenimientos';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function asignar_alummo()
    {
        try {
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['rol']!="Alumno"){ throw new Exception(JrTexto::_('You are not a Student').'.'); }

            $idTarea = $_REQUEST["idtarea"];
            if(empty($idTarea)){ throw new Exception(JrTexto::_('The task does not exists').'.'); }
           
            $this->oNegTarea->setLimite(0,9999);
            $tarea = $this->oNegTarea->buscar(array("idtarea"=>$idTarea));
            $tarea = @$tarea[0];

            $iddocente= $tarea["iddocente"];
            $idcursodetalle= $tarea["idcursodetalle"];
            $cursodetalle = $this->oNegCursodetalle->buscar(array("idcursodetalle"=>@$tarea['idcursodetalle']));
            if(empty($cursodetalle)){ throw new Exception(JrTexto::_('Something went wrong').'.'); }
          
            $cursodetalle = $cursodetalle[0];
            $cursodetalle['idcurso']=$cursodetalle['idcurso']==1?31:($cursodetalle['idcurso']==2?35:($cursodetalle['idcurso']==3?36:$cursodetalle['idcurso']));

            $matricula = $this->oNegMatricula->cursosAlumno(array("idalumno"=>$this->usuarioAct["idpersona"], "idcurso"=>$cursodetalle['idcurso']));
            if(empty($matricula)){ throw new Exception(JrTexto::_('Something went wrong').'.'); }

            $matricula = $matricula[0];
            $fechaentrega = date('Y-m-d' , strtotime(@$matricula['fecha_final']));
            $horaentrega = date('H:i:s' , strtotime(@$matricula['fecha_final']));
            $idgrupo = @$matricula['idgrupoauladetalle'];
            $iddocente = @$matricula['iddocente'];

            $asignacion = $this->oNegTarea_asignacion->buscar(array("idtarea"=>$idTarea, "idgrupo"=>$idgrupo, "iddocente"=>$iddocente, "fechaentrega"=>$fechaentrega, "horaentrega"=>$horaentrega, "eliminado"=>0));
            if(empty($asignacion)) {
                $this->oNegTarea_asignacion->idtarea = @$idTarea;
                $this->oNegTarea_asignacion->idgrupo = @$idgrupo;
                $this->oNegTarea_asignacion->iddocente = @$iddocente;
                $this->oNegTarea_asignacion->fechaentrega = @$fechaentrega;
                $this->oNegTarea_asignacion->horaentrega = @$horaentrega;
                $idasignacion = $this->oNegTarea_asignacion->agregar();
            } else {
                $idasignacion = $asignacion[0]['idtarea_asignacion'];
            }

            $asignacion_alumno = $this->oNegTarea_asignacion_alumno->buscar(array("idtarea_asignacion"=>$idasignacion, "idalumno"=>$usuarioAct['idpersona']));
            if(empty($asignacion_alumno)) {
                $this->oNegTarea_asignacion_alumno->idtarea_asignacion = @$idasignacion;
                $this->oNegTarea_asignacion_alumno->idalumno = @$usuarioAct['idpersona'];
                $this->oNegTarea_asignacion_alumno->estado = 'N';
                $this->oNegTarea_asignacion_alumno->idgrupotarea="T".@$idTarea."G".@$idgrupo;
                $this->oNegTarea_asignacion_alumno->iddocente=@$iddocente;
                $idasignacion_alumno = $this->oNegTarea_asignacion_alumno->agregar();
            } else {
                $idasignacion_alumno = $asignacion_alumno[0]['iddetalle'];
            }

            $this->actualizarEstadoTarea($idTarea);

            $redirToURL = $this->documento->getUrlBase().'/tarea/ver/?id='.$idasignacion_alumno;
            header('Location: ' . $redirToURL );
            exit(0);
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function getidrecursos($recursos){
        $idrecursos=array();
        if(!empty($recursos))
        foreach($recursos as $rec){
            if($rec["tiporecurso"]=='L')
            $idrecursos[]=intVal($rec["idrecurso"]);
            if(!empty($rec["hijo"])){
                $hijos=$this->getidrecursos($rec["hijo"]);
                if(!empty($hijos))
                $idrecursos= array_merge($idrecursos,$hijos);
            }
        }
       return $idrecursos;
    }

    public function tareasjson(){
        $this->listarTareasPendientesYFinalizadas(false,0,0);
    }

    public function listarTareasparaEditar() //
    {
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            global $aplicacion;
            $this->todoTareas=array();           
            if(empty($_REQUEST["idrecurso"])){ echo "salir"; exit(); }
            $this->idrecurso=$_REQUEST["idrecurso"];
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
            $this->todoTareas=$this->oNegTarea->buscar(array('allrecursosnull'=>$this->idrecurso,'eliminado'=>0,'idproyecto'=>$usuarioAct["idproyecto"]));
            }else{
                $this->todoTareas=$this->oNegTarea->buscar(array('allrecursos'=>$this->idrecurso,'eliminado'=>0,'idproyecto'=>$usuarioAct["idproyecto"]));
            }
            $x=0;
            if(!empty($this->todoTareas))
            foreach ($this->todoTareas as $t){
                $cant_asiganaciones_activas=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea']));
                $this->todoTareas[$x]['cant_asignaciones'] = count($asignaciones);//$cant_asiganaciones_activas;
                if($cant_asiganaciones_activas==0){ /* ya no hay asignciones, entonces actualizar tarea a "No Asignada"='NA' */
                    $this->oNegTarea->idtarea= $t['idtarea'];
                    $this->oNegTarea->__set('estado', 'NA');
                    $this->oNegTarea->editar();
                }
                $x++;
            }

            $respuesta = array("todo"=>$this->todoTareas);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function listarTareasPendientesYFinalizadas($flag=false,$idgrupoauladetalle,$idcurso) //
    {
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            global $aplicacion;
            $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
            if(empty($_POST["idcurso"]) && !$flag) {
                throw new Exception(JrTexto::_('Error in filtering'));
            }elseif(!$flag && !empty($_POST)){
                $idgrupoauladetalle=@$_POST["idcursodetalle"];
                $idcurso=@$_POST["idcurso"];
            }
            if(empty($idcurso)){
                throw new Exception(JrTexto::_('Error in Course'));
            }
            
            $usuarioAct = NegSesion::getUsuario();
           
            $recursos=$this->oNegCursodetalle->sesiones($idcurso,0);
            $idrecursos=$this->getidrecursos($recursos);
            $allidrecursos=join(',',$idrecursos);
            $this->oNegTarea->setLimite(0,9999);
            $this->todoTareas=$this->oNegTarea->buscar(array('allrecursos'=>$allidrecursos,'eliminado'=>0,'idproyecto'=>$usuarioAct["idproyecto"]));
            $x=0;
            if(!empty($this->todoTareas))
            foreach ($this->todoTareas as $t){
                $cant_asiganaciones_activas=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea'], 'iddocente'=> $usuarioAct['idpersona'],'idgrupo'=>$idgrupoauladetalle));
                foreach ($asignaciones as $a){
                    $hoy = new DateTime(date('Y-m-d H:i:s'));
                    $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
                    $fechahoy=date('d-m-Y');
                    $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
                    $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
                    $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
                    $a['cant_presentados']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'estado'=>['P','E'] )));
                    $a['cant_asignaciones']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'],'estado'=>['N','P','E','D'])));
                    if($fecha_hora>$hoy){
                        $this->tareasPend[] = array_merge($t,$a);
                        $cant_asiganaciones_activas+=$a['cant_asignaciones'];
                    }else{
                        $this->tareasFin[] = array_merge($t,$a);
                    }
                }
                $this->todoTareas[$x]['cant_asignaciones'] = $cant_asiganaciones_activas;
                if($cant_asiganaciones_activas==0){ /* ya no hay asignciones, entonces actualizar tarea a "No Asignada"='NA' */
                    $this->oNegTarea->idtarea= $t['idtarea'];
                    $this->oNegTarea->__set('estado', 'NA');
                    $this->oNegTarea->editar();
                }
                $x++;
            }

            $respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, "todo"=>$this->todoTareas);
            if($flag){ return $respuesta; }
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function listarNuevasDevueltasYFinalizadas($flag=false)
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
            #if(empty($_POST["idnivel"]) && empty($_POST["idunidad"]) && empty($_POST["idactividad"]) && !$flag){
            if(empty($_POST["idcurso"]) && !$flag) {
                throw new Exception(JrTexto::_('Error in filtering'));
            }
            $usuarioAct = NegSesion::getUsuario();
            $filtrosTarea = array('eliminado'=>0);
            if(!$flag){
                /*if($_POST['idnivel']>0) $filtrosTarea["idnivel"] = $_POST['idnivel'];
                if($_POST['idunidad']>0) $filtrosTarea["idunidad"] = $_POST['idunidad'];
                if($_POST['idactividad']>0) $filtrosTarea["idactividad"] = $_POST['idactividad'];*/
                $filtrosTarea["idcurso"] = !empty($_POST['idcurso'])?$_POST['idcurso']:null;
                $filtrosTarea["idcursodetalle"] = !empty($_POST['idcursodetalle'])?$_POST['idcursodetalle']:null;
            }
            $filtrosTarea["idproyecto"] = @$usuarioAct["idproyecto"];
            $this->oNegTarea->setLimite(0,9999);
            $this->todoTareas=$this->oNegTarea->buscar($filtrosTarea);
            foreach ($this->todoTareas as $t) {
                $cant_asiganaciones=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea']));
                foreach ($asignaciones as $a) {
                    $hoy = new DateTime(date('Y-m-d H:i:s'));
                    $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
                    $fechahoy=date('d-m-Y');
                    $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
                    $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
                    $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
                    $asignacion_alumno=$this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'idalumno'=>$usuarioAct['idpersona'] , /*'estado'=>['N','D','E','P']*/));
                    if(!empty($asignacion_alumno)){
                        $a['asignacion_alumno'] = $asignacion_alumno[0];
                        if($fecha_hora>$hoy && ($asignacion_alumno[0]['estado']=='N' || $asignacion_alumno[0]['estado']=='D') ){
                            $this->tareasPend[] = array_merge($t,$a);
                        }
                        if($fecha_hora<=$hoy || $asignacion_alumno[0]['estado']=='E' || $asignacion_alumno[0]['estado']=='P'){
                            $this->tareasFin[] = array_merge($t,$a);
                        }
                    }
                }
            }

            $respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, );
            if($flag){ return $respuesta; }
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function jxTareasXHabilidad()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            $this->idCurso = !empty(@$_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
            $this->idCursoDetalle = !empty(@$_REQUEST["iddetalle"])?$_REQUEST["iddetalle"]:0;
            $this->idHabilidad = !empty(@$_REQUEST["idhab"])?$_REQUEST["idhab"]:0;

            $this->oNegTarea->setLimite(0,9999);
            $this->datos = $this->oNegTarea->buscar(array("habilidades"=>array($this->idHabilidad),"habilidad_destacada"=>$this->idHabilidad, "idcursodetalle"=>$this->idCursoDetalle, "eliminado"=>0));
            $data=array('code'=>'ok','data'=>$this->datos);
            echo json_encode($data);
            exit(0);
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
            exit(0);
        }
    }

    public function xGuardar()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST)){
                throw new Exception(JrTexto::_('No data to insert'));
            }
            $frm = $_POST;
            if(@$frm["accion"]=="Editar"){
                $this->oNegTarea->idtarea =  @$frm['pkIdtarea'];
            };
            $usuarioAct = NegSesion::getUsuario();
            $esAdmin = NegSesion::tieneRol('Administrador');
            $asignacion = $esAdmin?'A':'M'; // [A]utomatica | [M]anual
            $this->oNegTarea->__set('iddocente',$usuarioAct['idpersona']);
            // $this->oNegTarea->__set('idnivel',@$frm["opcIdnivel"]);
            // $this->oNegTarea->__set('idunidad',@$frm["opcIdunidad"]);
            $this->oNegTarea->__set('idnivel',null);
            $this->oNegTarea->__set('idunidad',null);
            $this->oNegTarea->__set('idactividad',@$frm["opcIdactividad"]);
            $this->oNegTarea->__set('idcursodetalle',@$frm["opcIdcursodetalle"]);
            $this->oNegTarea->__set('idproyecto',@$usuarioAct["idproyecto"]);
            $this->oNegTarea->__set('nombre',@$frm["txtNombre"]);
            $this->oNegTarea->__set('descripcion',@$frm["txtDescripcion"]);
            $this->oNegTarea->__set('foto',str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$frm["txtFoto"]));
            $this->oNegTarea->__set('asignacion',$asignacion);
            $this->oNegTarea->__set('habilidades',@$frm["txtHabilidades"]);
            $this->oNegTarea->__set('habilidad_destacada',@$frm["opcHabilidad_destacada"]);
            $this->oNegTarea->__set('puntajemaximo',@$frm["opcPuntajemaximo"]);
            $this->oNegTarea->__set('puntajeminimo',@$frm["opcPuntajeminimo"]);
            $arrIdTarea_archivos = json_decode(@$frm['txtIdTarea_archivos'], true);
            if(@$frm["accion"]=="Nuevo"){
                $idTarea=$this->oNegTarea->agregar();
            }else{
                $idTarea=$this->oNegTarea->editar();
            }
            $this->actualizarTarea_archivos($arrIdTarea_archivos, $idTarea);
            $this->actualizarEstadoTarea($idTarea);

            $data=array('code'=>'ok','data'=>$idTarea);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function xEliminar_logica()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST['idtarea'])){
                throw new Exception(JrTexto::_('No idtarea to delete'));
            }
            $this->oNegTarea->__set('idtarea', $_POST['idtarea']);
            $res=$this->oNegTarea->eliminar_logica();
            if(empty($res)){
                throw new Exception(JrTexto::_('Error').' '.JrTexto::_('Delete Record'));
            }
            $data=array('code'=>'ok','data'=>$_POST['idtarea']);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function xGuardarPuntajes()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST['idasignacionalumno']) && empty($_POST['archivos'])){
                throw new Exception(JrTexto::_('No data to update'));
            }
            $this->oNegTarea_asignacion_alumno->iddetalle=$_POST['idasignacionalumno'];
            $this->oNegTarea_asignacion_alumno->__set('notapromedio',@$_POST['notapromedio']);
            $this->oNegTarea_asignacion_alumno->__set('estado', 'E');

            $resp = $this->oNegTarea_asignacion_alumno->editar();
            $arrArchivos=json_decode($_POST['archivos'],true);
            if(!empty($arrArchivos)){
                foreach ($arrArchivos as $arch) {
                    if(empty($arch['habilidades'])){ throw new Exception("Skills are missing in some evaluation"); }
                    $this->oNegTarea_archivos->idtarea_archivos = $arch['idtarea_archivos'];
                    $this->oNegTarea_archivos->__set('puntaje', $arch['puntaje']);
                    $this->oNegTarea_archivos->__set('habilidad', $arch['habilidades']);
                    $this->oNegTarea_archivos->editar();
                }
            }
            $data=array('code'=>'ok','data'=>$_POST['idasignacionalumno'], 'msj'=>JrTexto::_('Scores saved successfully'));
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function xDevolver()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST['idtarea_asignacion_alumno']) && empty($_POST['mensajedevolucion'])){
                throw new Exception(JrTexto::_('No data to update'));
            }
            $this->oNegTarea_asignacion_alumno->iddetalle=$_POST['idtarea_asignacion_alumno'];
            $this->oNegTarea_asignacion_alumno->__set('mensajedevolucion',@$_POST['mensajedevolucion']);
            $this->oNegTarea_asignacion_alumno->__set('estado', 'D');
            $resp = $this->oNegTarea_asignacion_alumno->editar();
            $data=array('code'=>'ok','data'=>$_POST['idtarea_asignacion_alumno'], 'msj'=>JrTexto::_('Homework was returned to student').'.');
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    /**** Para el módulo "Seguimiento del Estudiante" ****/
    public function xSeguimientoAlumno($idgrupo=null, $idalumno=null)
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            if(empty($_POST["idalumno"]) && empty($_POST["idgrupo"]) && $idalumno==null && $idgrupo==null) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $arrNotasxFechas=$filtros=$filtros2=array();
            $presentYear = date('Y');
            $filtros["idgrupo"]=($idgrupo==null)?$_POST["idgrupo"]:$idgrupo;
            $filtros["fechahora_mayorigual"]=$presentYear.'-01-01 00:00:00';
            $asignaciones=$this->oNegTarea_asignacion->buscar($filtros);
            foreach ($asignaciones as $asig) {
                $this->oNegTarea->setLimite(0,9999);
                $tarea = $this->oNegTarea->buscar([ 'idtarea'=>$asig['idtarea'] ]);
                $ptjeMax = (float)$tarea[0]['puntajemaximo'];
                $promedioAsig = 0.0;
                $filtros2["idtarea_asignacion"]=$asig['idtarea_asignacion'];
                if ($idalumno!=null){ $filtros2["idalumno"]=$idalumno; }
                elseif (!empty($_POST["idalumno"])) { $filtros2["idalumno"]=$_POST["idalumno"]; }
                $asignaciones_alum = $this->oNegTarea_asignacion_alumno->buscar($filtros2);
                if(!empty($asignaciones_alum)){
                    foreach ($asignaciones_alum as $asig_alum) {
                        $puntaje = ((float)$asig_alum['notapromedio']*100)/$ptjeMax;
                        $promedioAsig += $puntaje;
                    }
                    $promedioAsig = $promedioAsig/count($asignaciones_alum);
                }

                $arrNotasxFechas[] = [
                    'tarea' => $tarea[0],
                    'asignacion' => $asig,
                    'asignaciones_alumno' => $asignaciones_alum,
                    'promedioAsignaciones' => $promedioAsig,
                ];
            }
            if($idgrupo==null && $idalumno==null){
                $data=array('code'=>'ok','data'=>$arrNotasxFechas);
                echo json_encode($data);
            } else {
                return $arrNotasxFechas;
            }
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function xSeguimientoGrupo()
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            if(empty($_POST["idgrupo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $arrNotasxFechas=array();
            $arrNotasxFechas = $this->xSeguimientoAlumno($_POST["idgrupo"]);

            $data=array('code'=>'ok','data'=>$arrNotasxFechas);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function xTareas(){
        $this->documento->plantilla = 'blanco';
        try{
            global $aplicacion;         

            $filtros=array("idalumno"=>$this->usuarioAct['idpersona']);

            $this->datos=$this->oNegTarea->xTarea(array("idalumno"=>$this->usuarioAct['idpersona']));
            $calendario = array();
            foreach($this->datos as $datos){
                //print_r($datos);
                $elemento=array(
                    'id' => $datos['iddetalle'],
                    'title'=> $datos['nombre'],
                    'start'=> $datos['fecha'],
                    'end'=> $datos['fecha'],
                    'backgroundColor'=> '#9B59B6',
                    'borderColor'=> '#9B59B6',
                    'url' => $this->documento->getUrlBase().'/tarea/ver/?id='.$datos['iddetalle'],
                );
                $calendario[]=$elemento;
            }
            
            echo json_encode(array('code'=>'ok','data'=>$calendario));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

    /* =================== Funciones PRIVADAS ====================*/
    private function form()
    {
        try {
            global $aplicacion;             
            //$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');
            $this->newacc=!empty($_REQUEST["acc"])?$_REQUEST["acc"]:false;
            $this->esquema = 'tarea/tarea-frm';
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    private function actualizarEstadoTarea($idtarea=0)
    {
        try {
            if($idtarea<1){ throw new Exception(JrTexto::_('No id_tarea for update estado field')); }
            $tareas_asignadas = $this->oNegTarea_asignacion->buscar(array('idtarea'=>$idtarea, 'fechahora_mayorigual'=>date('Y-m-d H:i:s'), ));
            
            $this->oNegTarea->idtarea = $idtarea;
            if(empty($tareas_asignadas)){ $this->oNegTarea->estado = 'NA'; }
            else{ $this->oNegTarea->estado = 'A'; }
            $resp=$this->oNegTarea->editar();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function actualizarTarea_archivos($arrIdTarea_archivos=array(), $idTarea=0)
    {
        // print_r($arrIdTarea_archivos);
        try {
            if($idTarea<1){ throw new Exception(JrTexto::_('No id_tarea for update estado field')); }
            if(!empty($arrIdTarea_archivos)){
                $usuarioAct = NegSesion::getUsuario();
                foreach ($arrIdTarea_archivos as $idTar_Arch) {
                    $this->oNegTarea_archivos->idtarea_archivos=$idTar_Arch;
                    // $this->oNegTarea_archivos->__set('idtarea_archivos',$idTar_Arch);
                    $this->oNegTarea_archivos->__set('tablapadre','T');
                    $this->oNegTarea_archivos->__set('idpadre',@$idTarea);
                    $this->oNegTarea_archivos->__set('idpersona',$usuarioAct['idpersona']);
                    $res=$this->oNegTarea_archivos->editar();
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    private function obtenerTareas_Habilidad()
    {
        try {

            /*$this->oNegCursoDetalle->idcursodetalle = $this->idCursoDetalle;
            $curso_det = $this->oNegCursoDetalle->getXid();
            $sesion = $this->oNegNiveles->buscar(array('idnivel'=>$curso_det['idrecurso']));
            $s = !empty($sesion)?$sesion[0]['idnivel']:0;
            $unidad = $this->oNegNiveles->buscar(array('idnivel'=>$sesion[0]['idpadre']));
            $u = !empty($unidad)?$unidad[0]['idnivel']:0;
            $nivel = $this->oNegNiveles->buscar(array('idnivel'=>$unidad[0]['idpadre']));
            $n = !empty($nivel)?$nivel[0]['idnivel']:0;*/

            $this->oNegTarea->setLimite(0,9999);
            $tarea = $this->oNegTarea->buscar(array("habilidades"=>array($this->idHabilidad), "idcursodetalle"=>$this->idCursoDetalle));

            return $actividades;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            
        }
    }

    private function getPlugins()
    {
        try {
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('editactividad', '/js/new/');
            $this->documento->script('actividad_completar', '/js/new/');
            $this->documento->script('actividad_ordenar', '/js/new/');
            $this->documento->script('actividad_imgpuntos', '/js/new/');
            $this->documento->script('actividad_verdad_falso', '/js/new/');
            $this->documento->script('actividad_fichas', '/js/new/');
            $this->documento->script('actividad_dialogo', '/js/new/');
            $this->documento->script('manejadores_dby', '/js/new/');
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('jquery.md5', '/tema/js/');            
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('completar', '/js/new/');

            $this->documento->script('tools_games', '/js/');
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');

            #$this->documento->script('wavesurfer.min', '/libs/audiorecord/');
            #$this->documento->script('audioRecord', '/libs/audiorecord/');
            #$this->documento->script('recorderWorker', '/libs/audiorecord/');

            $this->documento->script('main', '/libs/audiorecord_wav/');
            $this->documento->script('audiodisplay', '/libs/audiorecord_wav/');
            $this->documento->script('recorder', '/libs/audiorecord_wav/js/');
            $this->documento->script('recorderWorker', '/libs/audiorecord_wav/js/');
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}