<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRecord', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');

class WebTools extends JrWeb
{
    protected $oNegActividad;
    private $oNegRecord;
    private $oNegHerramientas;
    private $oNegResources;  
    protected $oNegNiveles;
    protected $oNegCursoDetalle;
    protected $oNegHistorial_sesion;

    public function __construct()
    {
        parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegHerramientas = new NegHerramientas;
        $this->oNegRecord = new NegRecord;
        $this->oNegResources = new NegResources;
        $this->oNegActividad=new NegActividad;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegCursoDetalle = new NegAcad_cursodetalle;
        $this->oNegHistorial_sesion = new NegHistorial_sesion;

    }

    public function defecto(){
        return false;
    }

    public function smartbook() // antes teacherresources()
    {
        try{
            global $aplicacion;
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            /*$this->documento->script('chi_inputadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_saveedit', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_imageadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_videoadd', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chi_audioadd', '/libs/tinymce/plugins/chingo/');*/
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('editactividad', '/js/new/');
            $this->documento->script('actividad_completar', '/js/new/');
            $this->documento->script('actividad_ordenar', '/js/new/');
            $this->documento->script('actividad_imgpuntos', '/js/new/');
            $this->documento->script('actividad_verdad_falso', '/js/new/');
            $this->documento->script('actividad_fichas', '/js/new/');
            $this->documento->script('actividad_dialogo', '/js/new/');
            $this->documento->script('manejadores_dby', '/js/new/');
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('jquery.md5', '/tema/js/');            
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('completar', '/js/new/');

            $this->documento->script('tools_games', '/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
            
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $idnivel=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
            $usuarioAct = NegSesion::getUsuario();
            $this->idnivel=!empty($idnivel)?$idnivel:0;
            $this->idunidad=!empty($idunidad)?$idunidad:0;
            $this->idactividad=!empty($idactividad)?$idactividad:0;
            $this->orden=!empty($_GET["orden"])?$_GET["orden"]:0;
            $this->dnidoc=!empty($_GET["dni"])?$_GET["dni"]:$usuarioAct["dni"];
            if($this->dnidoc=='00000000') $this->dnidoc=$usuarioAct["dni"];
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'P','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosi=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'I','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosv=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'V','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosA=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'A','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosp=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'D','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosVoc=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'O','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosE=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'E','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosG=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'G','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->datosX=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'X','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));

            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/teacherresources';  
            
            //Para edicion de actividades
           

            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function speakinglabs()
    {
        try{
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;          
            $this->datos=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'A'));          

            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/recording';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function workbook()
    {
        try{
            global $aplicacion;         
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;          
            $this->datos=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'P'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/workbook';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function vocabulary()
    {
        try{
           
            global $aplicacion;          
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            //$this->datos=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'V'));
            $this->datos=$this->oNegHerramientas->buscar(Array('idactividad'=>$this->idactividad,'tool'=>'V'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            $this->documento->script('editactividad','/js/');
            $this->documento->script('chinput', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chimage', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chivideo', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chiaudio', '/libs/tinymce/plugins/chingo/');
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/vocabulary';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }
    public function games()
    {
        try{
            global $aplicacion;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            $this->documento->setTitulo(JrTexto::_('Game'), true);
           // $this->datos=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'G'));
            $this->datos=$this->oNegHerramientas->buscar(Array('idactividad'=>$this->idactividad,'tool'=>'G'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/games-varios';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }



    public function links()
    {
        try{          
            global $aplicacion;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->datos=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'L'));
            $this->esquema = 'tools/links';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

//funciones ajax
    public function xSaveTools(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $frm=$args[0];
                $rutabase=$this->documento->getUrlBase();
                $texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
                if(empty($texto)) return;
                $usuarioAct = NegSesion::getUsuario();
                
                if(!empty($frm["idTool"])){
                    $this->oNegHerramientas->setIdtool(@$frm['idTool']);
                    $this->oNegHerramientas->__set('idtool',@$frm['idTool']);
                }else{
                    if(@$frm['tool']=='V'){
                        $idtool=$this->oNegHerramientas->buscar(array('idactividad'=>@$frm['idActividad'],'tool'=>@$frm['tool']));
                        $frm["idTool"]='';
                        if(!empty($idtool))
                        foreach($idtool as $tool){
                            if(empty( $frm["idTool"]))  $frm["idTool"]=$tool["idtool"];
                            else {
                                $this->oNegHerramientas->idtool=$tool["idtool"];
                                $this->oNegHerramientas->Eliminar();
                            }
                        }
                    }
                }
                if(!empty($frm["idTool"]))  $this->oNegHerramientas->idtool=$frm["idTool"];

               // echo $texto;

                $this->oNegHerramientas->__set('idnivel',@$frm['idNivel']);
                $this->oNegHerramientas->__set('idunidad',@$frm['idUnidad']);
                $this->oNegHerramientas->__set('idactividad',@$frm['idActividad']);
                $this->oNegHerramientas->__set('titulo',@$frm['titulo']);
                $this->oNegHerramientas->__set('descripcion',@$frm['descripcion']);
                $this->oNegHerramientas->__set('texto',$texto);
                $this->oNegHerramientas->__set('orden',($usuarioAct["rol"]==1?0:1));
               
                if(empty($frm["idTool"])) {
                    $this->oNegHerramientas->__set('idpersonal',@$usuarioAct["dni"]);
                    $res=$this->oNegHerramientas->agregar(@$frm['tool']);
                }
                else{                   
                    $res=$this->oNegHerramientas->editar(@$frm['tool']);
                }
                if(!empty($res)) {
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Data updated successfully')),'success');
                    $oRespAjax->setReturnValue($this->oNegHerramientas->idtool);
                }else{
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
                    $oRespAjax->setReturnValue(false);
                }
                            
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }

    public function xEliminar(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $pk = $args[0];
                $this->oNegHerramientas->__set('idtool', $pk);
                $res=$this->oNegHerramientas->eliminar();
                if(!empty($res))
                    $oRespAjax->setReturnValue($res);
                else{
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
                    $oRespAjax->setReturnValue(false);
                }
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
                $oRespAjax->setReturnValue(false);
            } 
        }
    }    

    public function json_buscarGames(){
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $filtros=array();

            $filtros["titulo"]=!empty($_REQUEST["txtBuscar"])?$_REQUEST["txtBuscar"]:'';
            $filtros["idnivel"]=!empty($_REQUEST["nivel"])?$_REQUEST["nivel"]:'';
            $filtros["idunidad"]=!empty($_REQUEST["unidad"])?$_REQUEST["unidad"]:'';
            $filtros["idactividad"]=!empty($_REQUEST["actividad"])?$_REQUEST["actividad"]:'';
            $juegos=$this->oNegHerramientas->buscarJuegos($filtros);
            $data=array('code'=>'ok','data'=>$juegos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function buscarGamesXCursoDet()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty(@$_REQUEST["idcursodetalle"])){ throw new Exception(JrTexto::_("You have not selected a course or level")); }

            $idCursoDet=(!empty(@$_REQUEST["idcursodetalle"]))?@$_REQUEST["idcursodetalle"]:null;

            $this->oNegCursoDetalle->idcursodetalle = $idCursoDet;
            $curso_det = $this->oNegCursoDetalle->getXid();
            $sesion = $this->oNegNiveles->buscar(array('idnivel'=>$curso_det['idrecurso']));
            $s = !empty($sesion)?$sesion[0]['idnivel']:0;
            $unidad = $this->oNegNiveles->buscar(array('idnivel'=>$sesion[0]['idpadre']));
            $u = !empty($unidad)?$unidad[0]['idnivel']:0;
            $nivel = $this->oNegNiveles->buscar(array('idnivel'=>$unidad[0]['idpadre']));
            $n = !empty($nivel)?$nivel[0]['idnivel']:0;

            $filtros=array();
            $filtros["titulo"] = !empty($_REQUEST["txtBuscar"])?$_REQUEST["txtBuscar"]:'';
            $filtros["idnivel"] = $n;
            $filtros["idunidad"] = $u;
            $filtros["idactividad"] = $s;
            $juegos=$this->oNegHerramientas->buscarJuegos($filtros);
            $data=array('code'=>'ok','data'=>$juegos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    private function diferenciaxminutos(&$fecha, $fecha1,$fecha2){
        $minutos = abs( ceil((( strtotime($fecha1) - strtotime($fecha2) ) / 60)) );

        if ($minutos > 30) {
            //si excede hacer esto
            $_fechanueva = strtotime ( '+30 minute' , strtotime ($fecha1) ) ;
            $_fechanueva = date('Y-m-d H:i:s', $_fechanueva );
            $fecha = $_fechanueva;
        }
        return true;
    }
   public function insertarfechahistorial(){
        $this->documento->plantilla = 'returnjson';
        try{
           $data = array('code'=>'empty','data'=> null);
            
            
            //verificar los datos extraidos con post
             @extract($_POST);
             $_fecha = (!empty($fecha)) ? $fecha : date('Y-m-d H:i:s'); 
             
             if(empty($idhistorialsesion)){
                if(empty($idpersona) || empty($idproyecto)) { throw new Exception(JrTexto::_("No existe id usuario y proyecto"));  }

                $resultado = $this->oNegHistorial_sesion->buscar(array(
                    'idusuario' => $idpersona,
                    'fechasalida' => array('isnull' => true),
                    'lugar' => 'P',
                    'idproyecto' => $idproyecto,
                    'OrderByDesc' => 'idhistorialsesion'
                ));
                
                if(!empty($resultado)){ 
                    $idhistorial = null;
                      foreach($resultado as $value){
                        if(is_null($value['fechasalida'])){
                            $idhistorial = $value['idhistorialsesion'];
                            //verificar si la fecha actual y la anterior no exceda a media hora
                            // $fech1 = new datetime($value['fechaentrada']);
                            // $fech2 = new datetime($value['fechasalida']);
                            $fech1 = $value['fechaentrada'];
                            $fech2 = $_fecha;
                            
                            $this->diferenciaxminutos($_fecha,$fech1,$fech2);
                            break;
                        }
                    }
                    
                    if(!is_null($idhistorial)){
                        // if($verificar = $this->oNegHistorial_sesion->buscar(array('idhistorialsesion' => $idhistorial))){
                        //     $_f1 = date('Y-m-d', strtotime($verificar[0]['fechaentrada']));
                        //     $_f2 = date('Y-m-d', strtotime($_fecha));
                        //     if(strcmp($_f1,$_f2) !== 0 &&){

                        //     }
                        // }
                        if($edicion = $this->oNegHistorial_sesion->setCampo2($idhistorial,'fechasalida',$_fecha)){
                            $data = array('code'=>'ok','data'=> 'se modifico un null a una fecha actualizada');
                        }
                    }
                }else{
                    $data = array('code'=>'ok','data'=> 'se realizo la busqueda, no se encontro ninguna fecha en null');
                }

            }else{
                // $resultado = $this->oNegHistorial_sesion->buscar(array(
                //     'idhistorialsesion' => $idhistorialsesion,
                //     'idproyecto' => $idproyecto,
                // ));
                // if(!empty($resultado)){
                //     if(($resultado[0]['fechasalida'] == null)){
                //         $fech1 = $resultado[0]['fechaentrada'];
                //         $fech2 = $_fecha;
                //         $this->diferenciaxminutos($_fecha,$fech1,$fech2);
                //     }
                // }
                if($edicion = $this->oNegHistorial_sesion->setCampo2($idhistorialsesion,'fechasalida',$_fecha)){
                    $data = array('code'=>'ok','data'=> 'se edito una fecha actualizada por localstorage');
                }
            }

            NegSesion::set('localstorage_flag', false, '__admin_m3c');
            //Mostrar resultado
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function testsesion(){
        $this->documento->plantilla = 'returnjson';
        try{

            $tiempo_inicial = microtime(true); //true es para que sea calculado en segundos
            //$datos = $this->oNegCursoDetalle->getSoloSesiones(31,0);
            $url = 'https://abaco/pvingles.local/score/progresohabilidadunidad?idcurso=31&idrecurso=6';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
            //curl_setopt($ch, CURLOPT_NOBODY, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
            curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
//var_dump($info);
/*
            $datos = $this->oNegCursoDetalle->sesiones2(31,0);
            var_dump($datos);
            $tiempo_final = microtime(true);
            echo $tiempo = $tiempo_final - $tiempo_inicial; //este resultado estará en segundos*/
            return json_encode(array('codigo'=> true));
        }catch(Exception $e){
            $data=array('code'=>'Error','msje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
}