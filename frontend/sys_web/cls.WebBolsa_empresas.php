<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		30-11-2018 
 * @copyright	Copyright (C) 30-11-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
class WebBolsa_empresas extends JrWeb
{
	private $oNegBolsa_empresas;
	private $oNegPersonal;
	private $oNegGeneral;
	private $oNegProyecto;
		
	public function __construct()
	{
		parent::__construct();
		//exit(NegSesion::existeSesion());
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegGeneral = new NegGeneral;
		$this->oNegProyecto = new NegProyecto;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["rason_social"])&&@$_REQUEST["rason_social"]!='')$filtros["rason_social"]=$_REQUEST["rason_social"];
			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"];
			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["representante"])&&@$_REQUEST["representante"]!='')$filtros["representante"]=$_REQUEST["representante"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			
			$this->datos=$this->oNegBolsa_empresas->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Partners'), true);
			$this->esquema = 'bolsa_empresas-list';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function socios(){
		try{
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			$this->idempresa=0;
			if(isset($_REQUEST["idempresa"]) && @$_REQUEST["idempresa"]!='')
			{
				$filtros["idempresa"]=$_REQUEST["idempresa"];
				$this->idempresa=$_REQUEST["idempresa"];
			}
			//$this->datos=$this->oNegAcad_licencias->buscar($filtros);
			$this->empresas=$this->oNegBolsa_empresas->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Partner'), true);
			$this->esquema = 'acad_socios';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function cursos(){
		try{
			global $aplicacion;
			//if(!NegSesion::tiene_acceso('Acad_licencias', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			//$this->documento->script('slick.min', '/libs/sliders/slick/');
			//$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			//$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			//$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            //$this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            //$this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			$this->idempresa=0;
			if(isset($_REQUEST["idempresa"]) && @$_REQUEST["idempresa"]!='')
			{
				$filtros["idempresa"]=$_REQUEST["idempresa"];
				$this->idempresa=$_REQUEST["idempresa"];
			}
			//$this->datos=$this->oNegAcad_licencias->buscar($filtros);
			$this->empresas=$this->oNegBolsa_empresas->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Partner'), true);
			$this->esquema = 'acad_socios_cursos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}



	}
	public function agregarsocio(){
		try {			
			global $aplicacion;
			
			$this->empresas=$this->oNegBolsa_empresas->buscar();
			$this->fktipodoc=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipodocidentidad','mostrar'=>1));
			$this->fksexo=$this->oNegGeneral->buscar(array('tipo_tabla'=>'sexo','mostrar'=>1));
			$this->fkestado_civil=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadocivil','mostrar'=>1));
			//$this->roles=$this->oNegRoles->buscar();
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_socios-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function getProyecto(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			if(empty($_REQUEST["idempresa"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
				exit();
			}
			
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			$hayproyecto=$this->oNegProyecto->buscar($filtros);
			if(empty($hayproyecto[0])){
				$this->oNegProyecto->idempresa=@$_REQUEST["idempresa"];
				$this->oNegProyecto->jsonlogin='{}';
				$this->oNegProyecto->fecha=date('Y-m-d');
				$this->oNegProyecto->idioma=!empty($idioma)?$idioma:'EN';
				$idproyecto=$this->oNegProyecto->agregar();
			}else{
				$idproyecto=$hayproyecto[0]["idproyecto"];
			}
			echo json_encode(array('code'=>'ok','data'=>$idproyecto));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}





	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Partners').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegBolsa_empresas->idempresa = @$_GET['id'];
			$this->datos = $this->oNegBolsa_empresas->dataBolsa_empresas;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Partners').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bolsa_empresas-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Bolsa_empresas', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')$filtros["idempresa"]=$_REQUEST["idempresa"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["rason_social"])&&@$_REQUEST["rason_social"]!='')$filtros["rason_social"]=$_REQUEST["rason_social"];
			if(isset($_REQUEST["ruc"])&&@$_REQUEST["ruc"]!='')$filtros["ruc"]=$_REQUEST["ruc"];
			if(isset($_REQUEST["logo"])&&@$_REQUEST["logo"]!='')$filtros["logo"]=$_REQUEST["logo"];
			if(isset($_REQUEST["direccion"])&&@$_REQUEST["direccion"]!='')$filtros["direccion"]=$_REQUEST["direccion"];
			if(isset($_REQUEST["telefono"])&&@$_REQUEST["telefono"]!='')$filtros["telefono"]=$_REQUEST["telefono"];
			if(isset($_REQUEST["representante"])&&@$_REQUEST["representante"]!='')$filtros["representante"]=$_REQUEST["representante"];
			if(isset($_REQUEST["usuario"])&&@$_REQUEST["usuario"]!='')$filtros["usuario"]=$_REQUEST["usuario"];
			if(isset($_REQUEST["clave"])&&@$_REQUEST["clave"]!='')$filtros["clave"]=$_REQUEST["clave"];
			if(isset($_REQUEST["correo"])&&@$_REQUEST["correo"]!='')$filtros["correo"]=$_REQUEST["correo"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
						
			$this->datos=$this->oNegBolsa_empresas->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarBolsa_empresas(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
			$accion='_add';

			if(!empty(@$idempresa)) {
				$this->oNegBolsa_empresas->idempresa = $idempresa;
				$accion='_edit';
			}
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegBolsa_empresas->nombre=!empty($nombre)?$nombre:'';
			$this->oNegBolsa_empresas->rason_social=!empty($rason_social)?$rason_social:'';
			$this->oNegBolsa_empresas->ruc=!empty($ruc)?$ruc:'';
			//$this->oNegBolsa_empresas->logo=@$logo;
			$this->oNegBolsa_empresas->direccion=!empty($direccion)?$direccion:'';
			$this->oNegBolsa_empresas->telefono=!empty($telefono)?$telefono:'';
			$this->oNegBolsa_empresas->representante=!empty($representante)?$representante:'';
			$this->oNegBolsa_empresas->usuario=!empty($usuario)?$usuario:'';
			$this->oNegBolsa_empresas->clave=!empty($clave)?$clave:'';
			$this->oNegBolsa_empresas->correo=!empty($correo)?$correo:'';
			$this->oNegBolsa_empresas->estado=!empty($estado)?$estado:1;
            if($accion=='_add') {
				$res=$this->oNegBolsa_empresas->agregar();
				if(!empty($_FILES['logo'])){
					$file=$_FILES["logo"];
					$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
					$newfoto='logo-'.$res.'.'.$ext;
					$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'empresa' ;
					if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto)) 
					  {
						$this->oNegBolsa_empresas->idempresa=$res;
						$this->oNegBolsa_empresas->logo='/static/media/empresa/'.$newfoto;
						$this->oNegBolsa_empresas->editar();
					  }
				}
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Partners')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
				if(!empty($_FILES['logo'])){
					$file=$_FILES["logo"];
					$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
					$newfoto='logo-'.$res.'.'.$ext;
					$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'empresa';
					if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newfoto))
					$this->oNegBolsa_empresas->logo='/static/media/empresa/'.$newfoto;
				}
				$res=$this->oNegBolsa_empresas->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Partners')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
			}
			$hayproyecto=$this->oNegProyecto->buscar(array('idempresa'=>$res));
			if(empty($hayproyecto[0])){
				$this->oNegProyecto->idempresa=@$res;
				$this->oNegProyecto->jsonlogin='{}';
				$this->oNegProyecto->fecha=date('Y-m-d');
				$this->oNegProyecto->idioma=!empty($idioma)?$idioma:'EN';
				$this->oNegProyecto->agregar();
			}
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveBolsa_empresas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdempresa'])) {
					$this->oNegBolsa_empresas->idempresa = $frm['pkIdempresa'];
				}
				
				$this->oNegBolsa_empresas->nombre=@$frm["txtNombre"];
				$this->oNegBolsa_empresas->rason_social=@$frm["txtRason_social"];
				$this->oNegBolsa_empresas->ruc=@$frm["txtRuc"];
				$this->oNegBolsa_empresas->logo=@$frm["txtLogo"];
				$this->oNegBolsa_empresas->direccion=@$frm["txtDireccion"];
				$this->oNegBolsa_empresas->telefono=@$frm["txtTelefono"];
				$this->oNegBolsa_empresas->representante=@$frm["txtRepresentante"];
				$this->oNegBolsa_empresas->usuario=@$frm["txtUsuario"];
				$this->oNegBolsa_empresas->clave=@$frm["txtClave"];
				$this->oNegBolsa_empresas->correo=@$frm["txtCorreo"];
				$this->oNegBolsa_empresas->estado=@$frm["txtEstado"];
				
				

				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegBolsa_empresas->agregar();
					}else{
					$res=$this->oNegBolsa_empresas->editar();
				}
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegBolsa_empresas->idempresa);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDBolsa_empresas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBolsa_empresas->__set('idempresa', $pk);
				$this->datos = $this->oNegBolsa_empresas->dataBolsa_empresas;
				$res=$this->oNegBolsa_empresas->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegBolsa_empresas->__set('idempresa', $pk);
				$res=$this->oNegBolsa_empresas->eliminar();
				if(!empty($res)){
					$oRespAjax->setReturnValue($res);
					$dir_media=RUTA_BASE . 'static' . SD . 'media' . SD . 'empresa'.SD ;
					@unlink($dir_media."logo-".$pk.".jpg");
					@unlink($dir_media."logo-".$pk.".gif");
					@unlink($dir_media."logo-".$pk.".png");
				}
					
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegBolsa_empresas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}