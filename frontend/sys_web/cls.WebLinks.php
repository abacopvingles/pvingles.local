<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLinks', RUTA_BASE, 'sys_negocio');
class WebLinks extends JrWeb
{
	private $oNegLinks;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLinks = new NegLinks;
	}

	public function defecto(){
		return $this->listado();
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveLinks(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm=$args[0];
				$rutabase=$this->documento->getUrlBase();				 
				$texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
				if(empty($texto)) return;
				$usuarioAct = NegSesion::getUsuario();
				$this->oNegLinks->__set('idnivel',@$frm['idNivel']);
				$this->oNegLinks->__set('idunidad',@$frm['idUnidad']);
				$this->oNegLinks->__set('idactividad',@$frm['idActividad']);
				$this->oNegLinks->__set('idpersonal',@$usuarioAct["dni"]);				
				$this->oNegLinks->__set('texto',$texto);
				$this->oNegLinks->__set('orden',($usuarioAct["rol"]==1?0:1));
				$res=$this->oNegLinks->agregar();				
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegLinks->idlink);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLinks->__set('idlink', $pk);
				$res=$this->oNegLinks->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
}