<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-10-2018 
 * @copyright	Copyright (C) 27-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_dre', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_grado', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMin_sesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebMatricula extends JrWeb
{
	private $oNegAcad_matricula;
	private $oNegDre;
	private $oNegUgel;
	private $oNegIiee;
	private $oNegGrado;
	private $oNegSeccion;
	private $oNegCursos;
	private $oNegGrupoauladetalle;
	private $oNegGrupoaula;
	private $oNegPersonal;
	private $oNegPersonalrol;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegDre = new NegMin_dre;
		$this->oNegUgel = new NegUgel;
		$this->oNegIiee = new NegLocal;
		$this->oNegGrado = new NegMin_grado;
		$this->oNegSeccion = new NegMin_sesion;
		$this->oNegCurso = new NegAcad_curso;
		$this->usuarioAct = NegSesion::getUsuario();
		$this->oNegGrupoauladetalle= New NegAcad_grupoauladetalle;
		$this->oNegGrupoaula= New NegAcad_grupoaula;
		$this->oNegPersonal= New NegPersonal;
		$this->oNegPersonalrol= New NegPersona_rol;
	}

	public function defecto(){
		return $this->filtros();
	}

	public function filtros(){
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');

			$filtros=array();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			if($this->usuarioAct["idrol"]==3){
				return $aplicacion->redir();
			}else if($this->usuarioAct["idrol"]==2){
				$this->miscursos=$this->oNegGrupoauladetalle->buscar(array('iddocente'=>$this->usuarioAct["idpersona"],'idproyecto'=>$this->usuarioAct["idproyecto"]));	
				if(!empty($_REQUEST["ver"])){
					$this->documento->script('tinymce.min', '/libs/tinymce/');
					//exit($_REQUEST["ver"]);
					$this->esquema = 'docente/'.strtolower($_REQUEST["ver"]);
				}else		
				$this->esquema = 'docente/misalumnos-filtros';
			}else{
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			$this->fkcursos=$this->oNegCurso->buscarxproyecto(array('idproyecto'=>$this->usuarioAct["idproyecto"],'orderby'=>'nombre','estado'=>1));
			$this->dress=$this->oNegDre->buscar();
			$this->grados=$this->oNegGrado->buscar();
			$this->seccion=$this->oNegSeccion->buscar($filtros);
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);			
			$this->esquema = 'academico/acad_matricula-filtros';
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			$this->esquema = 'acad_matricula-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}







	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Matricula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try {
			global $aplicacion;
			$this->esquema = 'importar/matriculas-importar';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importardatos(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','data'=>JrTexto::_("data incomplete")));
		 		exit(0);
			}else{
				if(!empty($_POST["datosimportados"])){
					$dt=json_decode($_POST["datosimportados"]);
					$idgrupoauladetalle=!empty($_POST['idgrupoauladetalle'])?$_POST['idgrupoauladetalle']:0;
					$iddocente=!empty($_POST['iddocente'])?$_POST['iddocente']:0;
					$idiiee=!empty($_POST['idiiee'])?$_POST['idiiee']:0;
					$idcurso=!empty($_POST['idcurso'])?$_POST['idcurso']:0;
					$seccion=!empty($_POST['seccion'])?$_POST['seccion']:0;
					$grado=!empty($_POST['grado'])?$_POST['grado']:0;
					$ugel=!empty($_POST['ugel'])?$_POST['ugel']:0;
					if(!empty($idgrupoauladetalle)){
						$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
						if(empty($grupoauladetalle[0]))$idgrupoauladetalle=0;
						else{
							$this->oNegGrupoauladetalle->setCampo($idgrupoauladetalle,'iddocente',$iddocente);
							$this->oNegPersonalrol->idpersonal=$iddocente;
							$this->oNegPersonalrol->idproyecto=$this->usuarioAct["idproyecto"];
							$this->oNegPersonalrol->idrol=2;
							$this->oNegPersonalrol->idempresa=$this->usuarioAct["idempresa"];
							$this->oNegPersonalrol->agregar2();
						}
					}
					if(empty($idgrupoauladetalle)){
						$grupoaula=$this->oNegGrupoaula->buscar(array('nombre'=>'G_'.$idcurso,'idproyecto'=>$this->usuarioAct["idproyecto"],'estado'=>1));
						if(!empty($grupoaula[0])) $idgrupoaula=$grupoaula[0]["idgrupoaula"];
						else{
							$this->oNegGrupoaula->nombre='G_'.$idcurso;
							$this->oNegGrupoaula->tipo='V';
							$this->oNegGrupoaula->comentario='';
							$this->oNegGrupoaula->nvacantes=1000;
							$this->oNegGrupoaula->estado=1;
							$this->oNegGrupoaula->idproyecto=$this->usuarioAct["idproyecto"];
							$idgrupoaula=$this->oNegGrupoaula->agregar();
						}
						$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idlocal'=>$idiiee,'idgrupoaula'=>$idgrupoaula,'idcurso'=>$idcurso,'idgrado'=>$grado,'idsesion'=>$seccion));
						if(!empty($grupoauladetalle[0])){
							$idgrupoauladetalle=$grupoauladetalle[0]["idgrupoauladetalle"];
							$this->oNegGrupoauladetalle->setCampo($idgrupoauladetalle,'iddocente',$iddocente);
							$this->oNegPersonalrol->idpersonal=$iddocente;
							$this->oNegPersonalrol->idproyecto=$this->usuarioAct["idproyecto"];
							$this->oNegPersonalrol->idrol=2;
							$this->oNegPersonalrol->idempresa=$this->usuarioAct["idempresa"];
							$this->oNegPersonalrol->agregar2();
						}else{
							$this->oNegGrupoauladetalle->idlocal=$idiiee;
						    $this->oNegGrupoauladetalle->idambiente=1;
						    $this->oNegGrupoauladetalle->nombre=$idiiee."-".$idcurso."-".$seccion."-".$grado;
						    $this->oNegGrupoauladetalle->fecha_inicio=date('Y-m-d');
						    $this->oNegGrupoauladetalle->fecha_final=date('Y-12-31');
						    $this->oNegGrupoauladetalle->idgrupoaula=$idgrupoaula;
						    $this->oNegGrupoauladetalle->idcurso=$idcurso;
						    $this->oNegGrupoauladetalle->iddocente=$iddocente;
						    $this->oNegGrupoauladetalle->idgrado=$grado;
						    $this->oNegGrupoauladetalle->idseccion=$seccion;
						    $idgrupoauladetalle=$this->oNegGrupoauladetalle->agregar();
						}
					}
					if(!empty($dt)){ // registrar persona
						$this->datos=array();						
						foreach($dt as $v){							
							//echo @;
							$idpersona=$this->oNegPersonal->importar(@$v->dni,$v->tipodoc,$v->dni,@$v->ape_paterno,@$v->ape_materno,@$v->nombre,@$v->fechanac,@$v->sexo,@$v->estado_civil,$v->ubigeo,$v->urbanizacion,@$v->direccion,@$v->telefono,@$v->celular,@$v->email,@$ugel,$this->usuarioAct["idpersona"],date('Y-m-d'),@$v->usuario,@$v->clave,@$v->clave,3,@$v->foto,@$v->estado,@$v->situacion,@$v->idioma,'n',$idiiee,'','','',$this->usuarioAct["idproyecto"],$this->usuarioAct["idempresa"]);
							$hayMatricula=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle,'idalumno'=>$idpersona));
							
							if(empty($hayMatricula[0])){
								//var_dump($v->dni,$idgrupoauladetalle,$idpersona);
								$this->oNegAcad_matricula->idgrupoauladetalle=$idgrupoauladetalle;
								$this->oNegAcad_matricula->idalumno=$idpersona;
								$this->oNegAcad_matricula->fecha_registro=date('Y-m-d');
								$this->oNegAcad_matricula->estado=1;
								$this->oNegAcad_matricula->idusuario=$this->usuarioAct["idpersona"];
								$this->oNegAcad_matricula->fecha_matricula=date('Y-m-d');
								if($idpersona==-1) $this->datos[]=array('error'=>@$v->dni);
								else $this->datos[]=$this->oNegAcad_matricula->agregar();
							}else $this->datos[]=$hayMatricula[0]["idmatricula"];
						}
					}
				}
			}
	
			echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("Datos Importados")));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function asignardocente(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','data'=>JrTexto::_("data incomplete")));
		 		exit(0);
			}else{
				$idgrupoauladetalle=!empty($_POST['idgrupoauladetalle'])?$_POST['idgrupoauladetalle']:0;
				$iddocente=!empty($_POST['iddocente'])?$_POST['iddocente']:0;
				$idiiee=!empty($_POST['idiiee'])?$_POST['idiiee']:0;
				$idcurso=!empty($_POST['idcurso'])?$_POST['idcurso']:0;
				$seccion=!empty($_POST['seccion'])?$_POST['seccion']:0;
				$grado=!empty($_POST['grado'])?$_POST['grado']:0;
				$ugel=!empty($_POST['ugel'])?$_POST['ugel']:0;
				if(!empty($idgrupoauladetalle)){
					$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
					if(empty($grupoauladetalle[0]))$idgrupoauladetalle=0;
					else{
						$this->oNegGrupoauladetalle->setCampo($idgrupoauladetalle,'iddocente',$iddocente);
						$this->oNegPersonalrol->idpersonal=$iddocente;
						$this->oNegPersonalrol->idproyecto=$this->usuarioAct["idproyecto"];
						$this->oNegPersonalrol->idrol=2;
						$this->oNegPersonalrol->idempresa=$this->usuarioAct["idempresa"];
						$this->oNegPersonalrol->agregar2();
					}
				}
				if(empty($idgrupoauladetalle)){
					$grupoaula=$this->oNegGrupoaula->buscar(array('nombre'=>'G_'.$idcurso,'idproyecto'=>$this->usuarioAct["idproyecto"],'estado'=>1));
					if(!empty($grupoaula[0])) $idgrupoaula=$grupoaula[0]["idgrupoaula"];
					else{
						$this->oNegGrupoaula->nombre='G_'.$idcurso;
						$this->oNegGrupoaula->tipo='V';
						$this->oNegGrupoaula->comentario='';
						$this->oNegGrupoaula->nvacantes=1000;
						$this->oNegGrupoaula->estado=1;
						$this->oNegGrupoaula->idproyecto=$this->usuarioAct["idproyecto"];
						$idgrupoaula=$this->oNegGrupoaula->agregar();
					}
					$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idlocal'=>$idiiee,'idgrupoaula'=>$idgrupoaula,'idcurso'=>$idcurso,'idgrado'=>$grado,'idsesion'=>$seccion));
					if(!empty($grupoauladetalle[0])){
						$idgrupoauladetalle=$grupoauladetalle[0]["idgrupoauladetalle"];
						$this->oNegGrupoauladetalle->setCampo($idgrupoauladetalle,'iddocente',$iddocente);
						$this->oNegPersonalrol->idpersonal=$iddocente;
						$this->oNegPersonalrol->idproyecto=$this->usuarioAct["idproyecto"];
						$this->oNegPersonalrol->idrol=2;
						$this->oNegPersonalrol->idempresa=$this->usuarioAct["idempresa"];
						$this->oNegPersonalrol->agregar2();
					}else{
						$this->oNegGrupoauladetalle->idlocal=$idiiee;
						$this->oNegGrupoauladetalle->idambiente=1;
						$this->oNegGrupoauladetalle->nombre=$idiiee."-".$idcurso."-".$seccion."-".$grado;
						$this->oNegGrupoauladetalle->fecha_inicio=date('Y-m-d');
						$this->oNegGrupoauladetalle->fecha_final=date('Y-12-31');
						$this->oNegGrupoauladetalle->idgrupoaula=$idgrupoaula;
						$this->oNegGrupoauladetalle->idcurso=$idcurso;
						$this->oNegGrupoauladetalle->iddocente=$iddocente;
						$this->oNegGrupoauladetalle->idgrado=$grado;
						$this->oNegGrupoauladetalle->idseccion=$seccion;
						$idgrupoauladetalle=$this->oNegGrupoauladetalle->agregar();
					}
				}
				echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("docente Asignado"),'idgrupoauladetalle'=>$idgrupoauladetalle));
				exit(0);
			}
		   }catch(Exception $e) {
			   echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			   exit(0);
		   }
	}


	public function matricular(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'Error','data'=>JrTexto::_("data incomplete")));
		 		exit(0);
			}else{
				$idgrupoauladetalle=!empty($_POST['idgrupoauladetalle'])?$_POST['idgrupoauladetalle']:0;
				$iddocente=!empty($_POST['iddocente'])?$_POST['iddocente']:0;
				$idiiee=!empty($_POST['idiiee'])?$_POST['idiiee']:0;
				$idcurso=!empty($_POST['idcurso'])?$_POST['idcurso']:0;
				$seccion=!empty($_POST['seccion'])?$_POST['seccion']:0;
				$grado=!empty($_POST['grado'])?$_POST['grado']:0;
				$ugel=!empty($_POST['ugel'])?$_POST['ugel']:0;
				$idalumno=!empty($_POST['idpersona'])?$_POST['idpersona']:0;
				if(!empty($idgrupoauladetalle)){
					$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle));
					if(empty($grupoauladetalle[0]))$idgrupoauladetalle=0;
				}
				if(empty($idgrupoauladetalle)){
					$grupoaula=$this->oNegGrupoaula->buscar(array('nombre'=>'G_'.$idcurso,'idproyecto'=>$this->usuarioAct["idproyecto"],'estado'=>1));
					if(!empty($grupoaula[0])) $idgrupoaula=$grupoaula[0]["idgrupoaula"];
					else{
						$this->oNegGrupoaula->nombre='G_'.$idcurso;
						$this->oNegGrupoaula->tipo='V';
						$this->oNegGrupoaula->comentario='';
						$this->oNegGrupoaula->nvacantes=1000;
						$this->oNegGrupoaula->estado=1;
						$this->oNegGrupoaula->idproyecto=$this->usuarioAct["idproyecto"];
						$idgrupoaula=$this->oNegGrupoaula->agregar();
					}
					$grupoauladetalle=$this->oNegGrupoauladetalle->buscar(array('idlocal'=>$idiiee,'idgrupoaula'=>$idgrupoaula,'idcurso'=>$idcurso,'idgrado'=>$grado,'idsesion'=>$seccion));
					if(!empty($grupoauladetalle[0])){
						$idgrupoauladetalle=$grupoauladetalle[0]["idgrupoauladetalle"];
					}else{
						$this->oNegGrupoauladetalle->idlocal=$idiiee;
						$this->oNegGrupoauladetalle->idambiente=1;
						$this->oNegGrupoauladetalle->nombre=$idiiee."-".$idcurso."-".$seccion."-".$grado;
						$this->oNegGrupoauladetalle->fecha_inicio=date('Y-m-d');
						$this->oNegGrupoauladetalle->fecha_final=date('Y-12-31');
						$this->oNegGrupoauladetalle->idgrupoaula=$idgrupoaula;
						$this->oNegGrupoauladetalle->idcurso=$idcurso;
						$this->oNegGrupoauladetalle->iddocente=$iddocente;
						$this->oNegGrupoauladetalle->idgrado=$grado;
						$this->oNegGrupoauladetalle->idseccion=$seccion;
						$idgrupoauladetalle=$this->oNegGrupoauladetalle->agregar();
					}
				}
				$hayMatricula=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle,'idalumno'=>$idalumno));
				if(empty($hayMatricula[0])){
					$this->oNegAcad_matricula->idgrupoauladetalle=$idgrupoauladetalle;
					$this->oNegAcad_matricula->idalumno=$idalumno;
					$this->oNegAcad_matricula->fecha_registro=date('Y-m-d');
					$this->oNegAcad_matricula->estado=1;
					$this->oNegAcad_matricula->idusuario=$this->usuarioAct["idpersona"];
					$this->oNegAcad_matricula->fecha_matricula=date('Y-m-d');
					if($idalumno==-1) $idmatricula=$idalumno;
					else $idmatricula=$this->oNegAcad_matricula->agregar();
				}else $idmatricula= $hayMatricula[0]["idmatricula"];

				echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("Estudiante Matriculado"),'idmatricula'=>$idmatricula));
				exit(0);
			}
		   }catch(Exception $e) {
			   echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			   exit(0);
		   }
	}




	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_matricula->idmatricula = @$_GET['id'];
			$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Matricula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_matricula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function notas(){
		try{
			global $aplicacion;	
			$this->user = NegSesion::getUsuario();

			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];

			$this->fkcursos=$this->oNegCurso->buscarxproyecto(array('idproyecto'=>$this->usuarioAct["idproyecto"],'orderby'=>'nombre','estado'=>1));
			$this->dress=$this->oNegDre->buscar();
			$this->grados=$this->oNegGrado->buscar();
			$this->seccion=$this->oNegSeccion->buscar($filtros);

			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			$this->esquema = 'academico/visornotas';
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
						
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarAcad_matricula(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdmatricula)) {
				$this->oNegAcad_matricula->idmatricula = $frm['pkIdmatricula'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();

			$this->oNegAcad_matricula->idgrupoauladetalle=@$txtIdgrupoauladetalle;
			$this->oNegAcad_matricula->idalumno=@$txtIdalumno;
			$this->oNegAcad_matricula->fecha_registro=@$txtFecha_registro;
			$this->oNegAcad_matricula->estado=@$txtEstado;
			$this->oNegAcad_matricula->idusuario=@$txtIdusuario;
			$this->oNegAcad_matricula->fecha_matricula=@$txtFecha_matricula;

            if($accion=='_add') {
            	$res=$this->oNegAcad_matricula->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Matricula')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_matricula->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Matricula')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdmatricula'])) {
					$this->oNegAcad_matricula->idmatricula = $frm['pkIdmatricula'];
				}
				
				$this->oNegAcad_matricula->idgrupoauladetalle=@$frm["txtIdgrupoauladetalle"];
					$this->oNegAcad_matricula->idalumno=@$frm["txtIdalumno"];
					$this->oNegAcad_matricula->fecha_registro=@$frm["txtFecha_registro"];
					$this->oNegAcad_matricula->estado=@$frm["txtEstado"];
					$this->oNegAcad_matricula->idusuario=@$frm["txtIdusuario"];
					$this->oNegAcad_matricula->fecha_matricula=@$frm["txtFecha_matricula"];
					
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegAcad_matricula->agregar();
					}else{
						$res=$this->oNegAcad_matricula->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_matricula->idmatricula);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_matricula->__set('idmatricula', $pk);
				$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
				$res=$this->oNegAcad_matricula->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_matricula->__set('idmatricula', $pk);
				$res=$this->oNegAcad_matricula->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_matricula->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}