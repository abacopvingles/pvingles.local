<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-11-2017 
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
class WebAcad_curso extends JrWeb
{
	private $oNegAcad_curso;
	private $oNegCursodetalle;
	private $oNegNiveles;
	protected $oNegActividad;	
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->oNegCursodetalle = new NegAcad_cursodetalle;
		$this->oNegNiveles = new NegNiveles;
		$this->oNegActividad = new NegActividad;		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Curse'), true);
			$this->esquema = 'acad_curso-list';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Curse').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->frmaccion='Editar';
			$this->oNegAcad_curso->idcurso = @$_REQUEST['id'];
			$this->datos = $this->oNegAcad_curso->dataAcad_curso;
			$this->pk=@$_REQUEST['id'];
			$this->documento->setTitulo(JrTexto::_('Curse').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');		
			$usuarioAct = NegSesion::getUsuario();
			$this->idproyecto=$usuarioAct["idproyecto"];
			$this->esquema = 'academico/curso-frm';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try{
			global $aplicacion;
			$this->tipoaImport=!empty($_REQUEST['tipo']) ? $_REQUEST['tipo'] : 'N';
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->esquema = 'academico/curso-importar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importarcontenido(){ // guardar lo importado
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			if(empty($_POST["idNivel"])){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $filtro=array();
            if(!empty($tipo)) $filtro['tipo']=$tipo;
            $filtro['idnivel']=($idNivel>0)?$idNivel:0;
            $this->datos=$this->oNegActividad->importarcontenido($filtro);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			$filtros=array();				
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];	
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])){
				$usuarioAct = NegSesion::getUsuario();
				$filtros["idproyecto"]=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			}
			/*var_dump($filtros["idproyecto"]);*/
			$this->datos=$this->oNegAcad_curso->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function addcampo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST); 
            if(!empty($idcurso)&&!empty($campo)) {
				$res=$this->oNegAcad_curso->setCampo($idcurso, $campo, $valor);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
				exit();			
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error inesperado')));           	
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

	public function guardarcurso(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            @extract($_POST);
            if(!empty($idcurso)) {
				$this->oNegAcad_curso->idcurso = $idcurso;
				$accion='_edit';
			}
            	$usuarioAct = NegSesion::getUsuario();
            	$imagen=@str_replace($this->documento->getUrlBase(),'',$imagen);           	
				$this->oNegAcad_curso->nombre=@$nombre;
				$this->oNegAcad_curso->descripcion=@$descripcion;
				$this->oNegAcad_curso->estado=@$estado;
				$this->oNegAcad_curso->imagen=$imagen;	
            if($accion=='_add'){
            	$this->oNegAcad_curso->idusuario=@$usuarioAct["idpersona"];
            	 $res=$this->oNegAcad_curso->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_curso->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function eliminarcurso(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;			
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);           
            if(!empty($idcurso)){
            	$res=$this->oNegAcad_curso->eliminar($idcurso);	
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('remove successfully'))));	
            	 exit(0);		
			}
            echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('remove'))));            
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarcursodetalle(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';           
            @extract($_POST);
            if(!empty($pkIdcurso)){
				$this->oNegAcad_curso->idcurso = $pkIdcurso;
				$datatmp=$this->oNegAcad_curso->dataAcad_curso;				
				if(empty($datatmp)){					
					echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Course')." ".JrTexto::_('not register')));
					exit(0);
				}
			}
			$json=json_decode($jsondetalle);
			if(!empty($json)){
					$this->oNegCursodetalle->agregarvarios($pkIdcurso,$json,$idpadre);
			}
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('saved successfully'))); 
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}    
}