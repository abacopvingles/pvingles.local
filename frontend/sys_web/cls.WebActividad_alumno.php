<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017 
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNotas_quiz', RUTA_BASE, 'sys_negocio');
class WebActividad_alumno extends JrWeb
{
	private $oNegActividad_alumno;
    protected $oNegMetodologia;
    private $oNegGrupo_matricula;
    private $oNegCursodetalle;
    private $oNegAcad_matricula;
    private $oNegNotas_quiz;
    private $NegAcad_grupoauladetalle;
	public function __construct()
	{
		parent::__construct();
		$this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
        $this->oNegCursohabilidad = new NegAcad_cursohabilidad;
        $this->oNegAcad_matricula = new NegAcad_matricula;
        $this->oNegNotas_quiz = new NegNotas_quiz;
        $this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$this->datos=$this->oNegActividad_alumno->buscar();
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno'), true);
			$this->esquema = 'actividad_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegActividad_alumno->idactalumno = @$_GET['id'];
			$this->datos = $this->oNegActividad_alumno->dataActividad_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegActividad_alumno->idactalumno = @$_GET['id'];
			$this->datos = $this->oNegActividad_alumno->dataActividad_alumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'actividad_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'actividad_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ajax_agregar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Actividad_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$usuarioAct = NegSesion::getUsuario();
			$act_alum = $this->oNegActividad_alumno->buscar(array('iddetalleactividad'=>@$_POST['iddetalle_actividad'], 'idalumno'=>!empty($_POST['idalumno'])?$_POST['idalumno']:$usuarioAct['idpersona']));
			if(empty($act_alum)){	$accion = 'nuevo';
			}else{
				$accion = 'editar';
				$this->oNegActividad_alumno->idactalumno = $act_alum[0]['idactalumno'];
			}
			$this->oNegActividad_alumno->iddetalleactividad = (int) $_POST['iddetalle_actividad'];
			$this->oNegActividad_alumno->idalumno = !empty($_POST['idalumno'])?$_POST['idalumno']:$usuarioAct['idpersona'];
			$this->oNegActividad_alumno->fecha = date('Y-m-d');
			$this->oNegActividad_alumno->porcentajeprogreso = (float) $_POST['progreso'];
			$this->oNegActividad_alumno->habilidades = $_POST['habilidades'];
			$this->oNegActividad_alumno->estado = $_POST['estado'];
			$this->oNegActividad_alumno->html_solucion =@str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$_POST['html_solucion']);
			$this->oNegActividad_alumno->file=$_POST['file'];
			$this->oNegActividad_alumno->tipofile=$_POST['tipofile'];
			if($accion=='nuevo'){
				$idactalumno = $this->oNegActividad_alumno->agregar();
			}else{
				$idactalumno = $this->oNegActividad_alumno->editar();
			}
			$data=array('code'=>'ok','data'=>['idactalumno'=>$idactalumno]);
            echo json_encode($data);
            return parent::getEsquema();
		} catch(Exception $e) {
			//return $aplicacion->error(JrTexto::_($e->getMessage()));
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function progresoxalumno($idalumno=null)
	{
		$this->documento->plantilla = 'returnjson';
		try{
            global $aplicacion;

            if(empty($_REQUEST["id"]) && $idalumno==null) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros=array();
            $filtros["idalumno"]=($idalumno==null)?$_REQUEST["id"]:$idalumno;
            $idCurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:null;
            $actividad_alumno= array();
            if(empty($idCurso)){
            	$actividad_alumno=$this->oNegActividad_alumno->buscar($filtros);
            }else{
            	$arrCursoDet = $this->oNegCursodetalle->buscar(array("idcurso"=>$idCurso, "tiporecurso"=>'L'));
            	foreach ($arrCursoDet as $cd) {
            		$filtros["sesion"] = $cd["idrecurso"];
            		$acts_alum = $this->oNegActividad_alumno->buscar($filtros);
            		if(!empty($acts_alum)){
            			foreach ($acts_alum as $a) { 
            				$actividad_alumno[] = $a; 
            			}
            		}
            	}
            }
            $arrPorcentajexHab = array();
            foreach ($actividad_alumno as $act_alum) {
            	$arrHab_Act = explode ('|', $act_alum['habilidades']);
            	foreach ($arrHab_Act as $idSkill) {
            		if(!isset($arrPorcentajexHab[$idSkill])) {
            			$cursoHabilidad = $this->oNegCursohabilidad->buscar(array('idcursohabilidad'=>$idSkill));
            			if(!empty($cursoHabilidad)){ $cursoHabilidad = $cursoHabilidad[0]; }
            			$arrPorcentajexHab[$idSkill] = array(
            				'suma_porcentajes' => 0.0,
		    				'total_elem' => 0,
		    				'promedio_porcentaje' => 0.0,
		    				'nombre' => $cursoHabilidad['texto'],
            			);
            		}
	            	$arrPorcentajexHab[$idSkill]['suma_porcentajes']+=$act_alum['porcentajeprogreso'];
            		$arrPorcentajexHab[$idSkill]['total_elem']+=1 ;
            		$arrPorcentajexHab[$idSkill]['promedio_porcentaje']=$arrPorcentajexHab[$idSkill]['suma_porcentajes']/$arrPorcentajexHab[$idSkill]['total_elem'] ;
            	}
            }
            if($idalumno==null){
	            $data=array('code'=>'ok','data'=>$arrPorcentajexHab);
	            echo json_encode($data);
            } else {
            	return $arrPorcentajexHab;
            }
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function progresoxgrupo()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST["id"])) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
			$filtros = [];
			$arrPtjesProm = [];
            $filtros['idgrupo']=$_POST['id'];
            $alum_grupo = $this->oNegGrupo_matricula->buscar($filtros);
            foreach ($alum_grupo as $al) {
            	$arrPtjes = $this->progresoxalumno($al['idalumno']);
            	foreach ($arrPtjes as $idHab => $array) {
            		if(!isset($arrPtjesProm[$idHab])){
            			$arrPtjesProm[$idHab] = [];
            			$arrPtjesProm[$idHab] = [
            				'suma_porcentajes' => 0.0,
		    				'total_elem' => 0,
		    				'promedio_porcentaje' => 0.0,
            			];
            		}
            		$arrPtjesProm[$idHab]['suma_porcentajes'] += $array['promedio_porcentaje'];
            		$arrPtjesProm[$idHab]['total_elem'] += 1;
            		$arrPtjesProm[$idHab]['promedio_porcentaje'] = $arrPtjesProm[$idHab]['suma_porcentajes']/$arrPtjesProm[$idHab]['total_elem'];
            	}
            }

            $data=array('code'=>'ok','data'=>$arrPtjesProm);
	        echo json_encode($data);
	        return parent::getEsquema();
		} catch (Exception $ex) {
			
		}
	}

	public function actividadoffline(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			//if(isset($_REQUEST["solooffline"])&&@$_REQUEST["solooffline"]!='')$filtros["solooffline"]=$_REQUEST["solooffline"];
			$datos=$this->oNegAcad_matricula->buscar($filtros);
			$dt=array();
			if(!empty($datos)){
				$temas=$this->oNegCursodetalle->sesiones3($_REQUEST["idcurso"],0);					
				$vtmp=array();
				foreach($datos as $k => $v){
					$actA=array();
					if(!empty($temas))
					foreach ($temas as $act){
						$act["sesiones"]='';
						if($act["tiporecurso"]=="L") $act["sesiones"]=$act["idrecurso"];
						else if(!empty($act["hijo"])){
							$ses=array();
							foreach ($act["hijo"] as $h){
								$ses[]=$h["idrecurso"];
							}
							if(!empty($ses)) $act["sesiones"]=join("','",$ses);
							else $act["sesiones"]='';
						}
						if($act["sesiones"]!='')
							$actAlumnos=$this->oNegActividad_alumno->buscar(array('idalumno'=>$v["idalumno"],'insesion'=>$act["sesiones"],'solooffline'=>@$_REQUEST["solooffline"]));
						else 
							$actAlumnos=array();
						if(!empty($actAlumnos))	$actA[]=$actAlumnos;
						
					}
					$v["actividades"]=$actA;
					$vtmp[$k]=$v;
				}
				$dt=$vtmp;
			}
			echo json_encode(array('code'=>'ok','data'=>$dt));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function examenesoffline(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			//if(isset($_REQUEST["solooffline"])&&@$_REQUEST["solooffline"]!='')$filtros["solooffline"]=$_REQUEST["solooffline"];
			$datos=$this->oNegAcad_matricula->buscar($filtros);
			$dt=array();
			if(!empty($datos)){
				$temas=$this->oNegCursodetalle->examenes(array('idcurso'=>$_REQUEST["idcurso"],'tiporecurso'=>'E'));
				$examenes=array();
				if(!empty($temas)){
					foreach ($temas as $k => $e){$examenes[]=$e["idrecurso"];}
					if(!empty($examenes)) $examenes=join("','",$examenes);
				}
				$vtmp=array();
				foreach($datos as $k => $v){
					$exaAlumnos=array();
					$v["examenes"]=array();
					if(!empty($examenes)){
						$exaAlumnos=$this->oNegNotas_quiz->buscar2(array('idalumno'=>$v["idalumno"],'inrecursos'=>$examenes,'preguntasoffline'=>@$_REQUEST["solooffline"]));
						$v["examenes"]=$exaAlumnos;
					}
					//var_dump($v["examenes"]);
					$vtmp[$k]=$v;
				}
				$dt=$vtmp;
			}
			echo json_encode(array('code'=>'ok','data'=>$dt));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	} 


	public function nexamenesoffline(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];

			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			else $filtros["iddocente"]=$usuarioAct["idpersona"];

			$gruposaulas=$this->oNegAcad_grupoauladetalle->buscar(array('iddocente'=>$filtros["iddocente"],'idproyecto'=>$usuarioAct["idproyecto"]));
			if(empty($gruposaulas)) {
				echo json_encode(array('code'=>'ok','nexamenes'=>0));
		 		exit(0);
			} 
			
			$haycurso=array();
			$curso=array();
			$nexamenes=0;
			foreach ($gruposaulas as $k => $ga) {
				$idgupoauladet=$ga["idgrupoauladetalle"];
				$idcurso=$ga["idcurso"];

				$alumnos=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgupoauladet));
				if(!empty($alumnos)){
					if(!in_array($idcurso, $haycurso)){
						$haycurso[]=$idcurso;	
						$tem=$this->oNegCursodetalle->examenes(array('idcurso'=>$idcurso,'tiporecurso'=>'E'));
						$curso[$idcurso]=$tem;
					}else{
						$tem=$curso[$idcurso];
					}
					
					$examenes=array();
					if(!empty($tem)){
						foreach ($tem as $k => $e){$examenes[]=$e["idrecurso"];}
						if(!empty($examenes)) $examenes=join("','",$examenes);
					}
					foreach($alumnos as $k => $v){
						if(!empty($examenes)){
							$exaAlumnos=$this->oNegNotas_quiz->buscar2(array('idalumno'=>$v["idalumno"],'inrecursos'=>$examenes,'preguntasoffline'=>true));
							$nexamenes =$nexamenes+count($exaAlumnos);
						}						
					}				
				}
			}			
			echo json_encode(array('code'=>'ok','nexamenes'=>$nexamenes));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	} 


	public function nactividadoffline(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			//if(isset($_REQUEST["solooffline"])&&@$_REQUEST["solooffline"]!='')$filtros["solooffline"]=$_REQUEST["solooffline"];

			$usuarioAct = NegSesion::getUsuario();
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			else $filtros["iddocente"]=$usuarioAct["idpersona"];
			$gruposaulas=$this->oNegAcad_grupoauladetalle->buscar(array('iddocente'=>$filtros["iddocente"],'idproyecto'=>$usuarioAct["idproyecto"]));
			if(empty($gruposaulas)) {
				echo json_encode(array('code'=>'ok','nactividades'=>0));
		 		exit(0);
			}

			$haycurso=array();
			$curso=array();
			$nactividades=0;
			foreach ($gruposaulas as $k => $ga) {
				$idgupoauladet=$ga["idgrupoauladetalle"];
				$idcurso=$ga["idcurso"];

				$alumnos=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgupoauladet));
				if(!empty($alumnos)){
					if(!in_array($idcurso, $haycurso)){
						$haycurso[]=$idcurso;	
						$tem=$this->oNegCursodetalle->sesiones3($idcurso,0);
						$curso[$idcurso]=$tem;
					}else{
						$tem=$curso[$idcurso];
					}

					
					$sesiones=array();
					if(!empty($tem)){
						foreach($tem as $k => $act){
							if($act["tiporecurso"]=="L") $sesiones[]=$act["idrecurso"];
							else if(!empty($act["hijo"])){
								$ses=array();
								foreach ($act["hijo"] as $h){
									$sesiones[]=$h["idrecurso"];
								}								
							}

						}
						if(!empty($sesiones)) $sesiones=join("','",$sesiones);
						else $sesiones='';
					}
					foreach($alumnos as $k => $v){
						if(!empty($sesiones)){
							if($sesiones!='')$actAlumnos=$this->oNegActividad_alumno->buscar(array('idalumno'=>$v["idalumno"],'insesion'=>$sesiones,'solooffline'=>true));
							else $actAlumnos=array();							
							$nactividades =$nactividades+count($actAlumnos);
						}						
					}				
				}
			}
			echo json_encode(array('code'=>'ok','nactividades'=>$nactividades));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

}