<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBitacora_alumno_smartbook', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebSmartbook extends JrWeb
{
	private $oNegActividad;
    private $oNegNiveles;
    private $oNegMetodologia;	
    private $oNegResources;
    private $oNegHerramientas;
    private $oNegMatricula;
    private $oNegActividad_alumno;
    private $oNegBitac_alum_smartbook;
    private $oNegBitacora_smartbook;
    private $oNegAcad_cursodetalle;
    private $oNegcurso;

	public function __construct()
	{
		parent::__construct();			
        $this->oNegNiveles = new NegNiveles;
        $this->oNegResources = new NegResources;
        $this->oNegActividad = new NegActividad;
        $this->oNegHerramientas = new NegHerramientas;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegMatricula = new NegAcad_matricula;
        $this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegBitac_alum_smartbook = new NegBitacora_alumno_smartbook;
        $this->oNegBitacora_smartbook = new NegBitacora_smartbook;
        $this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
        $this->oNegcurso = new NegAcad_curso;
	}

	public function defecto(){
		try{
			return $this->listar();
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}


    public function listar(){
        try{
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            $rolActivo=$usuarioAct["rol"];
            if($rolActivo==='Alumno'){ throw new Exception(JrTexto::_("Access denied")); }

            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');

            $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $idunidad_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($idunidad_)?$idunidad_:($_idunidad);  

            $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idactividad=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;

            /*if($rolActivo!='Alumno'){
                //$this->esquema = 'docente/actividades_listar';              
            }else{
                $filtros["idnivel"]=$this->idnivel;
                $filtros["idunidad"]=$this->idunidad;
                $filtros["idactividad"]=$this->idactividad;
                $filtros["tipo"]=$this->tipo;                
                //$this->esquema = 'alumno/sesiones';
            }*/

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Smartbook')) ],
            ];
            $this->documento->setTitulo(JrTexto::_('Smartbook'), true);
            $this->esquema = 'smartbook/listar';
            return parent::getEsquema();
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

	
	public function ver() // antes teacherresources()
    {
        try{
            global $aplicacion;
            # *** Librerías *** #
             $this->documento->script('tinymce.min', '/libs/tinymce/');
             $this->documento->script('encode', '/libs/chingo/');
             /*$this->documento->script('chi_inputadd', '/libs/tinymce/plugins/chingo/');
             $this->documento->script('chi_saveedit', '/libs/tinymce/plugins/chingo/');
             $this->documento->script('chi_imageadd', '/libs/tinymce/plugins/chingo/');
             $this->documento->script('chi_videoadd', '/libs/tinymce/plugins/chingo/');
             $this->documento->script('chi_audioadd', '/libs/tinymce/plugins/chingo/');*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('audioRecorder','/libs/speach/');
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('callbackManager', '/libs/speach/');
             $this->documento->stylesheet('speach', '/js/new/');
             $this->documento->script('speach', '/js/new/');
             $this->documento->stylesheet('actividad_nlsw', '/js/new/');
             $this->documento->script('actividad_nlsw', '/js/new/');
             $this->documento->script('completar', '/js/new/'); 
             $this->documento->script('tools_games', '/js/'); 

             $this->documento->script('tools_games', '/js/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             $this->documento->script('pronunciacion', '/js/');
            
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
            $idnivel=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;

            $usuarioAct = NegSesion::getUsuario();
            $this->rolActivo=$usuarioAct["rol"];


            $this->idnivel=!empty($idnivel)?$idnivel:0;
            $this->idunidad=!empty($idunidad)?$idunidad:0;
            $this->idactividad=!empty($idactividad)?$idactividad:0;
            $this->orden=!empty($_GET["orden"])?$_GET["orden"]:0;

            $this->dnidoc=!empty($_GET["dni"])?$_GET["dni"]:$usuarioAct["dni"];
            if($this->dnidoc=='00000000') $this->dnidoc=$usuarioAct["dni"];

            /* portada */
            $this->nivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->idnivel));
            $this->unidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->idunidad));
            $this->sesion=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad));
            $this->nivel =!empty($this->nivel)?$this->nivel[0]:null;
            $this->unidad=!empty($this->unidad)?$this->unidad[0]:null;
            $this->sesion=!empty($this->sesion)?$this->sesion[0]:null;
            if(empty($this->sesion)){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }

            /* Ejercicios - Look - Practice - D.B.Y. */
            $this->intentos=$this->oNegActividad->nintentos($this->idnivel);
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->ejercicios = $this->oNegActividad->fullActividades(array('nivel'=>$this->idnivel,'unidad'=>$this->idunidad,'sesion'=>$this->idactividad));
            $this->look = !empty($this->ejercicios)?$this->ejercicios[1]:null;
            $this->practice = !empty($this->ejercicios)?$this->ejercicios[2]:null;
            $this->dby = !empty($this->ejercicios)?$this->ejercicios[3]:null;

            /* Vocabulario */
            $this->vocabulario=$this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'V'));

            /* Games (juegos) */
            $this->games = $this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'G'));

            /* workbook */
            $this->workbook = $this->oNegHerramientas->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'P'));
            $this->workbook = !empty($this->workbook)?$this->workbook[0]:null;
            
            /* PDF */
           // $this->pdfs=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'D','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->pdfs = !empty($this->pdfs)?$this->pdfs[0]:null;

            /* Audios */
           // $this->audios=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'A','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->audios = !empty($this->audios)?$this->audios[0]:null;
            
            /* Images */
            //$this->imagenes=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'I','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->imagenes = !empty($this->imagenes)?$this->imagenes[0]:null; 
            
            /* Videos */
           // $this->videos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'V','idpersonal'=>$this->dnidoc,'orden'=>$this->orden));
            $this->videos = !empty($this->videos)?$this->videos[0]:null;

            $this->urlBtnRetroceder = '/smartbook/';
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Smartbook')), 'link'=> $this->urlBtnRetroceder ],
                [ 'texto'=> $this->nivel['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'] ],
                [ 'texto'=> $this->unidad['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'].'/'.$this->unidad['idnivel'] ],
                [ 'texto'=> $this->sesion['nombre'] ],
            ];

            /* Para la vista de alumno */
            if($this->rolActivo=='Alumno'){
                if(!$this->tieneAcceso()){ throw new Exception(JrTexto::_("Session not found")); }
                $this->ver_alumno();
                $this->logro();
            }

            $this->documento->setTitulo(JrTexto::_('Smartbook'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'smartbook/ver';  
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
            /*$aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();*/
        }
    }


    public function ver2() // antes teacherresources()
    {
        try{
            global $aplicacion;
            # *** Librerías *** #
             $this->documento->script('tinymce.min', '/libs/tinymce/');
             $this->documento->script('encode', '/libs/chingo/');
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.ui.touch-punch.min','/libs/jquery-ui-touch/');
             $this->documento->stylesheet('jquery-ui.min', '/tema/css/');
             $this->documento->script('slick.min', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick', '/libs/sliders/slick/');
             $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('audioRecorder','/libs/speach/');
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('callbackManager', '/libs/speach/');
             $this->documento->stylesheet('speach', '/js/new/');
             $this->documento->script('speach', '/js/new/');
             $this->documento->stylesheet('actividad_nlsw', '/js/new/');
             $this->documento->script('actividad_nlsw', '/js/new/');
             $this->documento->script('completar', '/js/new/'); 
             $this->documento->script('tools_games', '/js/');  
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             $this->documento->script('pronunciacion', '/js/');
             $this->documento->script('scorm1', '/js/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');



            $filtros['idcursodetalle']=!empty($_REQUEST["idcursodetalle"])?$_REQUEST["idcursodetalle"]:-1;
            $cursodetalle=$this->oNegAcad_cursodetalle->buscar2($filtros);
            if(empty($cursodetalle[0])){ throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }
            $this->cursodetalle=$cursodetalle[0];  
            $this->idcurso=$this->cursodetalle["idcurso"];
            $this->idcursoDetalle=$this->cursodetalle["idcursodetalle"];
            $cursos=$this->oNegcurso->buscar(array('idcurso'=>$this->idcurso));
            $this->curso=array();
            if(!empty($cursos[0])) $this->curso=$cursos[0];                     
            $this->idrecurso=$this->cursodetalle["idrecurso"];
            $this->idlogro=$this->cursodetalle["idlogro"];
            $this->orden=$this->cursodetalle["orden"];
            $this->idactividad=$this->idrecurso; 
            $this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->idactividad));
            $this->sesion=array();
            if(!empty($this->sesiones[0])) $this->sesion=$this->sesiones[0];
            else { throw new Exception(JrTexto::_("Sorry").'.'.JrTexto::_("Topic is not complete yet").'.'); }
            $usuarioAct = NegSesion::getUsuario();
            $this->idrol = $usuarioAct["idrol"];
            $this->rolActivo=($this->idrol==2||$this->idrol==3)?'Alumno':$usuarioAct["rol"];
            $this->idpersona=$usuarioAct["idpersona"];
            
            $intentos=$this->oNegActividad->nintentos($this->idcurso);
            $this->intentos=$intentos;//
           
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->ejercicios = $this->oNegActividad->fullActividades(array('sesion'=>$this->idactividad));
            $this->look = !empty($this->ejercicios)?$this->ejercicios[1]:null;
            $this->practice = !empty($this->ejercicios)?$this->ejercicios[2]:null;
            $this->dby = !empty($this->ejercicios)?$this->ejercicios[3]:null;

            /* Vocabulario */
            $vocab=$this->oNegHerramientas->buscar(Array('idactividad'=>$this->idactividad,'tool'=>'V'));
            $this->vocabulario=array();
            $nvoca=0;
            if(!empty($vocab)){  $nvoca=count($vocab)-1; }
            $this->vocabulario[]=!empty($vocab[$nvoca])?$vocab[$nvoca]:array();


            /* Games (juegos) */
            $this->games = $this->oNegHerramientas->buscar(Array('idactividad'=>$this->idactividad,'tool'=>'G'));

            /* workbook */
            $this->workbook = $this->oNegHerramientas->buscar(Array('idactividad'=>$this->idactividad,'tool'=>'P'));
            $this->workbook = !empty($this->workbook)?$this->workbook[0]:null;
            $this->pdfs = null;
            $this->audios = null;            
            $this->imagenes = null;            
            $this->videos = null;
            $this->urlBtnRetroceder = '/curso/?id='.$this->idcurso;
            $this->breadcrumb = [
                [ 'texto'=> $this->curso["nombre"], 'link'=> $this->urlBtnRetroceder ],
                //[ 'texto'=> $this->nivel['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'] ],
                //[ 'texto'=> $this->unidad['nombre'], 'link'=> '/smartbook/listar/'.$this->nivel['idnivel'].'/'.$this->unidad['idnivel'] ],
                [ 'texto'=> $this->sesion['nombre'] ],
            ];

            if(!empty($this->practice['act']['det'])){
                foreach ($this->practice['act']['det'] as $i=>$ejerc) {
                    $iddetalle=$ejerc["iddetalle"];
                    $desarrollo = $this->oNegActividad_alumno->buscar(array('iddetalleactividad'=>$iddetalle, 'idalumno'=>$usuarioAct["idpersona"]));
                    $this->practice['act']['det'][$i]['desarrollo'] = !empty($desarrollo)?$desarrollo[0]:null;
                }
            }
            /* Ejercicios - D.B.Y. */
            if(!empty($this->dby['act']['det'])){
                foreach ($this->dby['act']['det'] as $i=>$ejerc) {
                    $iddetalle=$ejerc["iddetalle"];
                    $desarrollo = $this->oNegActividad_alumno->buscar(array('iddetalleactividad'=>$iddetalle, 'idalumno'=>$usuarioAct["idpersona"]));
                    $this->dby['act']['det'][$i]['desarrollo'] = !empty($desarrollo)?$desarrollo[0]:null;
                }
            }
            $this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook->buscar(array('idusuario'=>$usuarioAct['idpersona'],'idcurso'=>$this->idCurso,'idsesion'=>$this->idactividad));           
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }

            $this->bitacora = $this->oNegBitacora_smartbook->buscar(array('idusuario'=>$usuarioAct['idpersona'],'idcurso'=>$this->idCurso,'idsesion'=>$this->idactividad));
            if(!empty($this->bitacora)){
                foreach ($this->bitacora as $i => $b) {
                    $otros_datos = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $b['otros_datos']);
                    $b['otros_datos'] = json_decode($otros_datos,true);
                    if($i===0){ $b['ultimo_visto'] = 'ultimo_visto'; }
                    switch ($b['pestania']) {                        
                        case 'div_practice':
                            $this->practice['bitacora'] = $b;
                            break;
                        case 'div_autoevaluar':
                            $this->dby['bitacora'] = $b;
                            break;
                        case 'div_look':
                            $this->look['bitacora'] = $b;
                            break;
                        case 'div_voca':
                            $this->vocabulario['bitacora'] = $b;
                            break;
                    }
                }
            }
            $this->documento->setTitulo(JrTexto::_('Smartbook'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'smartbook/ver2';  
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }


    /*========================= Funciones AJAX ===================== */
	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegNiveles->buscar($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xCargarRecurso(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegResources->buscar($filtro);

				return $oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $this->oNegResources->setCampo($args[0],$args[1],$args[2]);
                return $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
               return  $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }

    /*========================= Funciones PRIVADAS ===================== */
    private function tieneAcceso()
    {
        $this->idCurso = (isset($_GET['idcurso']))?$_GET['idcurso']:null;
        if(empty($this->idCurso)){ throw new Exception(JrTexto::_("Course not found")); }
        $this->cursoActual = $this->oNegMatricula->estaMatriculado( $this->idCurso);
        if(empty($this->cursoActual)){
            return false;
        }
        return true;
    }
    private function logro()
    {
        try {    
            $filtros['idcurso']=$this->idCurso;
            $filtros['idrecurso']=$this->sesion['idnivel'];
            $resp=$this->oNegAcad_cursodetalle->buscar2($filtros);
            $this->idLogro=@$resp[0]['idlogro'];
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
        }
    }
    
    private function ver_alumno()
    {
        try {
            $usuarioAct = NegSesion::getUsuario();
            //$this->logro();
            $this->urlBtnRetroceder = '/curso/?id='.$this->idCurso;
            $this->documento->script('alumno', '/js/alumno/');
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Course')), 'link'=> $this->urlBtnRetroceder ],
                [ 'texto'=> ucfirst(JrTexto::_('Smartbook')), /*'link'=> '/smartbook/'*/ ],
                [ 'texto'=> $this->sesion['nombre'] ],
            ];

            /* Ejercicios - Practice */
            if(!empty($this->practice['act']['det'])){
                foreach ($this->practice['act']['det'] as $i=>$ejerc) {
                    $iddetalle=$ejerc["iddetalle"];
                    $desarrollo = $this->oNegActividad_alumno->buscar(array('iddetalleactividad'=>$iddetalle, 'idalumno'=>$usuarioAct["idpersona"]));
                    $this->practice['act']['det'][$i]['desarrollo'] = !empty($desarrollo)?$desarrollo[0]:null;
                }
            }
            /* Ejercicios - D.B.Y. */
            if(!empty($this->dby['act']['det'])){
                foreach ($this->dby['act']['det'] as $i=>$ejerc) {
                    $iddetalle=$ejerc["iddetalle"];
                    $desarrollo = $this->oNegActividad_alumno->buscar(array('iddetalleactividad'=>$iddetalle, 'idalumno'=>$usuarioAct["idpersona"]));
                    $this->dby['act']['det'][$i]['desarrollo'] = !empty($desarrollo)?$desarrollo[0]:null;
                }
            }
            /* Bitacora_Smartbook (Progreso) */
            $this->bitac_alum_smbook = $this->oNegBitac_alum_smartbook->buscar(array('idusuario'=>$usuarioAct['idpersona'],'idcurso'=>$this->idCurso,'idsesion'=>$this->sesion['idnivel']));
            if(!empty($this->bitac_alum_smbook)) {
                $this->bitac_alum_smbook = $this->bitac_alum_smbook[0];
            }
            $this->bitacora = $this->oNegBitacora_smartbook->buscar(array('idusuario'=>$usuarioAct['idpersona'],'idcurso'=>$this->idCurso,'idsesion'=>$this->sesion['idnivel']));

            if(!empty($this->bitacora)){
                foreach ($this->bitacora as $i => $b) {
                    $otros_datos = str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $b['otros_datos']);
                    $b['otros_datos'] = json_decode($otros_datos,true);
                    if($i===0){ $b['ultimo_visto'] = 'ultimo_visto'; }
                    switch ($b['pestania']) {
                        case 'div_pdfs':
                            $this->pdfs['bitacora'] = $b;
                            break;
                        case 'div_audios':
                            $this->audios['bitacora'] = $b;
                            break;
                        case 'div_imagen':
                            $this->imagenes['bitacora'] = $b;
                            break;
                        case 'div_videos':
                            $this->videos['bitacora'] = $b;
                            break;
                        case 'div_practice':
                            $this->practice['bitacora'] = $b;
                            break;
                        case 'div_autoevaluar':
                            $this->dby['bitacora'] = $b;
                            break;
                        case 'div_look':
                            $this->look['bitacora'] = $b;
                            break;
                        case 'div_voca':
                            $this->vocabulario['bitacora'] = $b;
                            break;
                        case 'div_workbook':
                            $this->workbook['bitacora'] = $b;
                            break;
                    }
                }
            }

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}