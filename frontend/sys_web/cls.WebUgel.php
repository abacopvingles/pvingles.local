<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-10-2018 
 * @copyright	Copyright (C) 25-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
class WebUgel extends JrWeb
{
	private $oNegUgel;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUgel = new NegUgel;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ugel', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["abrev"])&&@$_REQUEST["abrev"]!='')$filtros["abrev"]=$_REQUEST["abrev"];
			if(isset($_REQUEST["iddepartamento"])&&@$_REQUEST["iddepartamento"]!='')$filtros["iddepartamento"]=$_REQUEST["iddepartamento"];
			if(isset($_REQUEST["idprovincia"])&&@$_REQUEST["idprovincia"]!='')$filtros["idprovincia"]=$_REQUEST["idprovincia"];
			
			$this->datos=$this->oNegUgel->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ugel'), true);
			$this->esquema = 'ugel-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ugel', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ugel').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ugel', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegUgel->idugel = @$_GET['id'];
			$this->datos = $this->oNegUgel->dataUgel;
			$_iddep=$this->datos['iddepartamento'];			
			$this->provincias=$this->oNegUgel->ubigeo(array('idpadre'=>$_iddep));
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Ugel').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'ugel-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importar(){
		try {
			global $aplicacion;
			$this->esquema = 'importar/ugel-importar';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function importardatos(){
		try{
			global $aplicacion;
			$this->documento->plantilla = 'blanco';
			if(empty($_POST)){
				echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 		exit(0);
			}else{
				if(!empty($_POST["datosimportados"])){
					$dt=json_decode($_POST["datosimportados"]);
					if(!empty($dt)){
						$this->datos=array();
						foreach($dt as $v){
							$this->datos[]=$this->oNegUgel->importar(@$v->idugel,@$v->descripcion,@$v->abrev,@$v->iddre,@$v->dre,@$v->idprovincia);
						}
					}
				}
			}
			echo json_encode(array('code'=>'ok','data'=>$this->datos,'msj'=>JrTexto::_("Datos Importados")));
		 	exit(0);
		}catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Ugel', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idugel"])&&@$_REQUEST["idugel"]!='')$filtros["idugel"]=$_REQUEST["idugel"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["abrev"])&&@$_REQUEST["abrev"]!='')$filtros["abrev"]=$_REQUEST["abrev"];
			if(isset($_REQUEST["iddepartamento"])&&@$_REQUEST["iddepartamento"]!='')$filtros["iddepartamento"]=$_REQUEST["iddepartamento"];
			if(isset($_REQUEST["idprovincia"])&&@$_REQUEST["idprovincia"]!='')$filtros["idprovincia"]=$_REQUEST["idprovincia"];
						
			$this->datos=$this->oNegUgel->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarUgel(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$pkIdugel)) {
				$this->oNegUgel->idugel = $frm['pkIdugel'];
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
				$this->oNegUgel->descripcion=@$txtDescripcion;
					$this->oNegUgel->abrev=@$txtAbrev;
					$this->oNegUgel->iddepartamento=@$txtIddepartamento;
					$this->oNegUgel->idprovincia=@$txtIdprovincia;
					
            if($accion=='_add') {
            	$res=$this->oNegUgel->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ugel')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegUgel->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ugel')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveUgel(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdugel'])) {
					$this->oNegUgel->idugel = $frm['pkIdugel'];
				}
				
				$this->oNegUgel->descripcion=@$frm["txtDescripcion"];
					$this->oNegUgel->abrev=@$frm["txtAbrev"];
					$this->oNegUgel->iddepartamento=@$frm["txtIddepartamento"];
					$this->oNegUgel->idprovincia=@$frm["txtIdprovincia"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegUgel->agregar();
					}else{
									    $res=$this->oNegUgel->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegUgel->idugel);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDUgel(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUgel->__set('idugel', $pk);
				$this->datos = $this->oNegUgel->dataUgel;
				$res=$this->oNegUgel->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUgel->__set('idugel', $pk);
				$res=$this->oNegUgel->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegUgel->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegUgel->ubigeo($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}