<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');*/

JrCargador::clase('sys_datos::DatAcademico', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');

class WebAcademico extends JrWeb
{
	/*private $oNegAlumno;
    private $oNegGrupo_matricula;
	private $oNegActividad_alumno;
	private $oNegActividad_detalle;
	private $oNegNiveles;*/
	private $oNegAcad_curso;
	private $oDatAcademico;
	private $oNegAcad_cursodetalle;

	public function __construct()
	{
		parent::__construct();		
		/*$this->oNegAlumno = new NegAlumno;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad_detalle = new NegActividad_detalle;
		$this->oNegNiveles = new NegNiveles;*/
		$this->oDatAcademico = new DatAcademico;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
		$this->oNegAcad_curso = new NegAcad_curso;
		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){		
		if($this->usuarioAct["idrol"]==10){
			global $aplicacion;	
			return $aplicacion->redir();
		}
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Academic'), true);
			/*$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
						
			$this->datos=$this->oNegAlumno->buscar();
			*/
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			//$this->documento->setTitulo(JrTexto::_('Alumno'), true);
			//var_dump($this->usuarioAct);
			if($this->usuarioAct["idrol"]==3){
				return $aplicacion->redir();
			}else if($this->usuarioAct["idrol"]==2){
				$this->esquema = 'academico/docente';
			}else $this->esquema = 'academico/inicio_minedu';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}  

	public function matricula(){
		try{		
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Matriculate'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';

			$this->esquema = 'academico/matriculas';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function grupos(){
		try{		
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Matriculate'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';

			$this->esquema = 'academico/matriculas';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function curso(){
		try{
			//JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
			//$this->ocurso = new NegAcad_curso;		
			global $aplicacion;
			$filtros=array();
			//$this->datos=$this->ocurso->buscar($filtros);

			$this->documento->setTitulo(JrTexto::_('Course'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';			
			$this->esquema = 'academico/curso';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function preguntas_examen(){
		try{
			global $aplicacion;
			$this->usuarioAct = NegSesion::getUsuario();
			$result = array();
			//buscar todos los examenes por curso:
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$session = $this->oNegAcad_cursodetalle->sesiones($cursosProyecto[0]['idcurso'],0);
			//obtener preguntas...
			foreach($session as $v){
				if($v['tiporecurso'] == 'E'){
					$id = intval($v['idrecurso']);
					$prepare = array('idexamen'=>$id,'examen'=>$v['nombre'], 'general' => array(),'preguntas' => array());
					$tmp_g = $this->oDatAcademico->totalHab($id);
					if(!empty($tmp_g)){
						$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
					}
					$tmp = $this->oDatAcademico->preguntas(array($id));
					if(!empty($tmp)){
						$i= 0; $c = count($tmp);
						do{
							$hab = explode('|',$tmp[$i]['habilidades']);
							$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
							$prepare['preguntas'][] = array('nombre'=>$tmp[$i]['pregunta'],'habilidades'=>$count_hab);
							++$i;
						}while($i < $c);
					}
					$result[] = $prepare;
				}
			}
			$cursosProyecto[] = array('idcurso'=>0,'nombre'=>utf8_encode('Placement Test'));
			$this->examen = $result;
			$this->cursos = $cursosProyecto;
			//capsularlo en json y mostrarlos
			$this->documento->setTitulo(JrTexto::_("Question"));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
			$this->esquema = "academico/preguntas";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function examenes_totales(){
		try{
			global $aplicacion;
			$this->usuarioAct = NegSesion::getUsuario();
			$result = array();
			//buscar todos los examenes por curso:
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$session = $this->oNegAcad_cursodetalle->sesiones($cursosProyecto[0]['idcurso'],0);
			//obtener preguntas...
			foreach($session as $v){


				if($v['tiporecurso'] == 'E'){
					$id = intval($v['idrecurso']);
					$prepare = array('idexamen'=>$id,'examen'=>$v['nombre'], 'general' => array(),'preguntas' => array(),'total'=> 0);
					$tmp_g = $this->oDatAcademico->totalHab($id);
					if(!empty($tmp_g)){
						$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
						
					}
					
					$tmp = $this->oDatAcademico->preguntas(array($id));
					if(!empty($tmp)){
						$i= 0; $c = count($tmp);
						do{
							

							$hab = explode('|',$tmp[$i]['habilidades']);
							$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
							$prepare['preguntas'][] = array('nombre'=>$tmp[$i]['pregunta'],'habilidades'=>$count_hab);
							++$i;
						}while($i < $c);
					}
					$prepare['total']=$c;
					$result[] = $prepare;
				}
			}
			$cursosProyecto[] = array('idcurso'=>0,'nombre'=>utf8_encode('Placement Test'));

			$this->examen = $result;
			$this->cursos = $cursosProyecto;
			
			//capsularlo en json y mostrarlos

			$this->documento->setTitulo(JrTexto::_("Question"));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
			$this->esquema = "academico/examenes_totales";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function curso_total(){
		try{
			global $aplicacion;
			$this->usuarioAct = NegSesion::getUsuario();
			$result = array();
			//buscar todos los examenes por curso:
			$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
			$session = $this->oNegAcad_cursodetalle->sesiones($cursosProyecto[0]['idcurso'],0);
			//obtener preguntas...
			foreach($session as $v){
				if($v['tiporecurso'] == 'E'){
					$id = intval($v['idrecurso']);
					$prepare = array('idexamen'=>$id,'examen'=>$v['nombre'], 'general' => array(),'preguntas' => array());
					$tmp_g = $this->oDatAcademico->totalHab($id);
					if(!empty($tmp_g)){
						$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
					}
					$tmp = $this->oDatAcademico->preguntas(array($id));
					if(!empty($tmp)){
						$i= 0; $c = count($tmp);
						do{
							$hab = explode('|',$tmp[$i]['habilidades']);
							$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
							$prepare['preguntas'][] = array('nombre'=>$tmp[$i]['pregunta'],'habilidades'=>$count_hab);
							++$i;
						}while($i < $c);
					}
					$result[] = $prepare;
				}
			}
			$cursosProyecto[] = array('idcurso'=>0,'nombre'=>utf8_encode('Placement Test'));

			$this->examen = $result;
			$this->cursos = $cursosProyecto;
			//capsularlo en json y mostrarlos

			$this->documento->setTitulo(JrTexto::_("Question"));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
			$this->esquema = "academico/curso_total";
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function json_preguntas_examen(){
		$this->documento->plantilla = "returnjson";
		header('Content-Type: charset=utf-8');
		try{
			global $aplicacion;
			$idcurso = (isset($_REQUEST['idcurso'])) ? $_REQUEST['idcurso'] : null;
			$this->usuarioAct = NegSesion::getUsuario();
			$result = array();
			if(is_null($idcurso)){
				$cursosProyecto = $this->oNegAcad_curso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
				$idcurso =$cursosProyecto[0]['idcurso'];
			}
			$session = $this->oNegAcad_cursodetalle->sesiones($idcurso,0);
			//obtener preguntas...
			if($idcurso > 0){
				foreach($session as $v){

					if($v['tiporecurso'] == 'E' ){
						$id = intval($v['idrecurso']);
						$prepare = array('idexamen'=>$id,'examen'=>utf8_encode($v['nombre']), 'general' => array(),'preguntas' => array(),'total' => 0);
						// print_r($v['nombre']);
						$tmp_g = $this->oDatAcademico->totalHab($id);
						if(!empty($tmp_g)){
							$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
						}
						$tmp = $this->oDatAcademico->preguntas(array($id));
						if(!empty($tmp)){
							$i= 0; $c = count($tmp);
							do{
								$hab = explode('|',$tmp[$i]['habilidades']);
								$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
								$prepare['preguntas'][] = array('nombre'=>utf8_encode($tmp[$i]['pregunta']),'habilidades'=>$count_hab);
								++$i;
							}while($i < $c);
						}
						$prepare['total'] = $c;
						$result[] = $prepare;

					}
				}//end foreach
			}else{
				$id = 363;
				$prepare = array('idexamen'=>$id,'examen'=>utf8_encode('General Placement Test-Teacher'), 'general' => array(),'preguntas' => array(),'total' => 0);
				$tmp_g = $this->oDatAcademico->totalHab($id);
				if(!empty($tmp_g)){
					$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
				}
				$tmp = $this->oDatAcademico->preguntas(array($id));
				if(!empty($tmp)){
					$i= 0; $c = count($tmp);
					do{
						$hab = explode('|',$tmp[$i]['habilidades']);
						$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
						$prepare['preguntas'][] = array('nombre'=>utf8_encode($tmp[$i]['pregunta']),'habilidades'=>$count_hab);
						++$i;
					}while($i < $c);
				}
				$prepare['total'] = $c;
				$result[] = $prepare;
				$id = 326;
				$prepare = array('idexamen'=>$id,'examen'=>utf8_encode('General Placement Test-Student'), 'general' => array(),'preguntas' => array(),'total' => 0);
				$tmp_g = $this->oDatAcademico->totalHab($id);
				if(!empty($tmp_g)){
					$prepare['general'] = array('L'=>$tmp_g[0]['L'],'R'=>$tmp_g[0]['R'],'W'=>$tmp_g[0]['W'],'S'=>$tmp_g[0]['S']);
				}
				$tmp = $this->oDatAcademico->preguntas(array($id));
				if(!empty($tmp)){
					$i= 0; $c = count($tmp);
					do{
						$hab = explode('|',$tmp[$i]['habilidades']);
						$count_hab = array('L'=>in_array('4',$hab),'R'=>in_array('5',$hab),'W'=>in_array('6',$hab),'S'=>in_array('7',$hab));
						$prepare['preguntas'][] = array('nombre'=>utf8_encode($tmp[$i]['pregunta']),'habilidades'=>$count_hab);
						++$i;
					}while($i < $c);
				}
				$prepare['total'] = $c;
				$result[] = $prepare;
			}

			$data=array('code'=>'ok','data'=>$result);
			echo json_encode($data);
			
			return parent::getEsquema();
		}catch(Exception $e){
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
			echo json_encode($data);
		}
	}
	public function panel_skill(){
		try{		
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Balance reporting skills'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';

			$this->esquema = 'academico/panel_skill';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	public function examenes(){
		try{			
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$filtros=array();
			$this->documento->setTitulo(JrTexto::_('SmartQuiz'));
			$this->documento->plantilla = !empty($_REQUEST['plt'])?$_REQUEST['plt']:'mantenimientos';
            $this->url = URL_SMARTQUIZ.'?id='.@$usuarioAct['usuario'].'&pr='.IDPROYECTO.'&u='.@$usuarioAct['usuario'].'&p='.@$usuarioAct['clave']; 
			$this->esquema = 'academico/examenes';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}