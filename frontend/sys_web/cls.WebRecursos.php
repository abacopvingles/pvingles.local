<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebRecursos extends JrWeb
{
	private $oNegActividad;
    private $oNegNiveles;
    private $oNegMetodologia;	
	private $oNegResources;
	private $oNegAcad_cursodetalle;
    private $oNegcurso;

	public function __construct()
	{
		parent::__construct();			
        $this->oNegNiveles = new NegNiveles;
		$this->oNegResources = new NegResources;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
        $this->oNegcurso = new NegAcad_curso;
	}

	public function defecto(){
		try{
			
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function ver(){
		try{
			global $aplicacion;
		    $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

            $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
            $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
           	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
           	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
           	
           	$usuarioAct = NegSesion::getUsuario();
           	$this->iddoc=(!empty($_GET["iddoc"])&&@$_GET["iddoc"]!='00000000')?$_GET["iddoc"]:$usuarioAct["dni"];
           	$this->recursos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tipo'=>'P','idpersonal'=>$this->iddoc));
         	
			$this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
			$this->documento->script('circle', '/libs/graficos/progressbar/');          
			$this->documento->setTitulo(JrTexto::_('Resource'), true);
	        $this->esquema = 'docente/misrecursos';
	        return parent::getEsquema();
	    }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function listar(){
		try{
			
			$idcurso=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
			if($idcurso<30)
			$idcurso=$idcurso==1?31:($idcurso==2?35:($idcurso==3?36:($idcurso==4?38:($idcurso==5?39:$idcurso))));
			$this->usuarioAct = NegSesion::getUsuario();
			$filtro=array("idproyecto"=>@$this->usuarioAct["idproyecto"],'estado'=>1);
			$adminedit=array(1,5);
			if(!in_array($this->usuarioAct["idpersona"],$adminedit)){
				$filtro["estado"]=1;
			}

			$this->cursos = $this->oNegcurso->buscar($filtro);
			$this->idcurso=!empty($idcurso)?$idcurso:$idcurso;
			$temas=$this->oNegAcad_cursodetalle->sesiones($this->idcurso,0);
			$temastmp=array();
			$i=0;
			foreach($temas as $tem){				
				$tem["link"]='';
				if($tem["tiporecurso"]=='E'){
					$idproyecto='&pr='.ucfirst(IDPROYECTO);
					$urlsmartQuiz=$this->documento->getUrlBase().'/';
					$idexamen='?idexamen='.$tem["idrecurso"];
					$usuario='&u='.$this->usuarioAct['usuario'];
					$id='&id='.$this->usuarioAct['idpersona'];
					$clave='&p='.$this->usuarioAct['clave'];
					$callback='&callback=';
					$tem["link"] = $urlsmartQuiz.'examenes/resolver/'.$idexamen.$idproyecto.$id.$usuario.$clave.$callback;
					$tem["linkedit"] = $urlsmartQuiz.'smartquiz/examenes/ver/'.$idexamen.$idproyecto.$id.$usuario.$clave.'&type=superadmin&idi=EN';


				}else{$tem=$this->getProgreso2($tem); }                                       
				$temastmp[]=$tem;
				$i++;
            }
			$this->newsylabus=$temastmp;
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->setTitulo(JrTexto::_('Cursos vista Admin'), true);
	        //$this->esquema = 'doc_recursos_listar';
			$usuarioAct = NegSesion::getUsuario();
			$rolActivo=$usuarioAct["idrol"];

			//if($rolActivo==1){//administrador
				$this->esquema = 'academico/cursos_admin';
			/*}elseif($rolActivo==2){//docente
				$this->esquema = 'docente/cursos_docente';
			}elseif($rolActivo==3){//alumno
				$this->esquema = 'alumno/sesiones';
			}*/
	        return parent::getEsquema();
        }catch(Exception $e) {
             return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}
	
	private function getProgreso2($det){
        try{             
			$det["link"]='';			
            if($det["tiporecurso"]=='E'){               
				$det["link"]=$this->documento->getUrlBase()."/examenes/resolver/?idexamen=".$det["idrecurso"];
				$det["linkedit"]=$this->documento->getUrlBase()."/examenes/resolver/?idexamen=".$det["idrecurso"];
            }else{
				$idniv=$det["tiporecurso"]=='N'?$det["idrecurso"]:'';
				$iduni=$det["tiporecurso"]=='U'?$det["idrecurso"]:'';
				$idact=$det["tiporecurso"]=='L'?$det["idrecurso"]:'';               
				$det["link"]=$this->documento->getUrlBase()."/smartbook/ver2/?idcursodetalle=".$det["idcursodetalle"]."&idrecurso=".$det["idrecurso"];
				$det["linkedit"]=$this->documento->getUrlBase()."/actividad/agregar2/?idnivel=".$idniv."&txtUnidad=".$iduni."&txtsesion=".$idact;
                if(!empty($det['hijo'])){
                    $hijos=array();      
                    $nhijos=count($det['hijo']);
                    foreach($det['hijo'] as $hi){
                        $hijos[]=$this->getProgreso2($hi); 
                    }       
                    $det["hijo"]=$hijos;
                }
            }
            return $det;
        }catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegNiveles->buscar($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xCargarRecurso(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegResources->buscar($filtro);

				return $oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $this->oNegResources->setCampo($args[0],$args[1],$args[2]);
                return $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
               return  $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }

}