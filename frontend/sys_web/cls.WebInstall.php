<?php
set_time_limit(0);
defined('RUTA_BASE') or die();

JrCargador::clase('sys_negocio::NegInstall', RUTA_BASE, 'sys_negocio');

class WebInstall extends JrWeb {
    protected $oNegInstall;
    protected $filename;
    public function __construct(){
        parent::__construct();
        $this->oNegInstall = new NegInstall;
        $this->filename = "syncmanual.zip";
    }
    public function defecto(){
        global $aplicacion;
        
        $this->documento->stylesheet('bootstrap-select.min', '/libs/bootstrap-select/css/');
        $this->documento->script('bootstrap-select.min', '/libs/bootstrap-select/js/');
        $this->documento->script('plugin', '/libs/jQueryFormPlugin/');
        
        $this->documento->setTitulo(JrTexto::_('Configuración de la plataforma'), true);
        $this->documento->plantilla = 'install';
        $this->esquema = 'install/main';
        return parent::getEsquema();
    }
    public function exportfiles(){
        try{
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_("Exportar archivos"), true);
            $this->documento->plantilla = 'mantenimientos';
            $this->esquema = "install/export";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error($e->getMessage());
        }
    }
    public function syncupload(){
        try{
            global $aplicacion;
            $this->documento->script('plugin', '/libs/jQueryFormPlugin/');
            
            $this->documento->setTitulo(JrTexto::_('Sincronizar'), true);
            $this->documento->plantilla = 'mantenimientos';
            $this->esquema = "install/upload";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error($e->getMessage());
        }
    }
    public function syncdownload(){
        try{
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Descargar Sincronizacion'), true);
            $this->documento->plantilla = 'mantenimientos';
            $this->esquema = "install/download";
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error($e->getMessage());
        }
    }
    public function getinformation(){
        try{
            $type= (isset($_REQUEST['typeInfo'])) ? intval($_REQUEST['typeInfo']) : null;
            $ubigeo_departamento = (isset($_REQUEST['departamento'])) ? $_REQUEST['departamento'] : null;
            $ubigeo_provincia = (isset($_REQUEST['provincia'])) ? $_REQUEST['provincia'] : null;
            if(is_null($type)){
                throw new Exception("no se especifica el tipo de informacion");
            }
            $resultado = array();
            if($type === 1){
                $resultado = $this->oNegInstall->getdepartamentos();
            }else if($type === 2){
                $resultado = $this->oNegInstall->getprovincia($ubigeo_departamento);
            }else if($type === 3){
                $ubigeo = '';
                if(!empty($ubigeo_departamento) && $ubigeo_departamento != '0'){
                    if(!empty($ubigeo_provincia) && $ubigeo_provincia != '0'){
                        $ubigeo = $ubigeo_departamento.$ubigeo_provincia;
                        $resultado = $this->oNegInstall->getlocal(str_pad($ubigeo,6,'0'));
                    }
                }
            }
            echo json_encode(array('code'=>"ok",'data' => $resultado));
            exit();
        }catch(Exception $e){
            echo json_encode(array('code'=>"error",'message'=>$e->getMessage()));
            exit();
        }
    }
    public function save(){
        try{
            if(empty($_FILES["archivo"]["name"])){
                throw new Exception("archivo no fue subido");
            }
            $source = $_FILES["archivo"]["tmp_name"]; //Obtenemos un nombre temporal del archivo
            $target_path = RUTA_BASE."sync".DIRECTORY_SEPARATOR."storage".DIRECTORY_SEPARATOR."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR.$this->filename;
            if(!move_uploaded_file($source, $target_path)){	
                throw new Exception("Ha ocurrido un error, al momento de mover (subir) el archivo por favor, inténtelo de nuevo.");
            }
            echo json_encode(array('code'=>'ok','message'=>'complete'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
    private function listdir($pathtosearch){
        $file_array = array();
        foreach(glob($pathtosearch) as $filename){
            $file_array[] = basename($filename);
        }
        return $file_array;
    }
    private function unzip($dir,$container){
        try{
            $zip = new ZipArchive;
              $res = $zip->open($dir);
              if($res === TRUE){
                  $zip->extractTo($container);
                  $zip->close();
                  return true;
              }else{
                  printf("error en apertura de archivo zip: %s", $res);
                  return false;
              }
            return true;
        }catch(Exception $e){
            printf("error en descomprimir zip: %s",$e->getMessage());
            return false;
        }
    }
    public function sync(){
        try{
            $search = RUTA_BASE."sync".DIRECTORY_SEPARATOR."storage".DIRECTORY_SEPARATOR."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR.$this->filename;
            $contenedor = RUTA_BASE."sync".DIRECTORY_SEPARATOR."storage".DIRECTORY_SEPARATOR."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
            if(count(glob($search)) === 0){
                throw new Exception("No hay archivos subidos para sincronizar");
            }
            $files = $this->listdir($search);
            if(!empty($files)){
                foreach($files as $key => $value){
                    $newContenedor = $contenedor.substr($value,0,strpos($value,'.zip')).DIRECTORY_SEPARATOR;
                    //descompress
                    if($this->unzip($contenedor.$value,$contenedor)){
                        
                        $file_data = $this->listdir($newContenedor."*.txt");
                        foreach($file_data as $llave => $archivo){
                            // if($archivo == 'actividad_alumno.txt'){
                                $contenido = file($newContenedor.$archivo);
                                $output = '';
                                $inserts = array();
                                foreach($contenido as $row)
                                {
                                    $start_character = substr(trim($row), 0, 2);
                                    if($start_character != '--' || $start_character != '/*' || $start_character != '//' || $row != '')
                                    {
                                        $output = $output . $row;
                                        $end_character = substr(trim($row), -1, 1);
                                        if($end_character == ';')
                                        {
                                            $inserts[] = substr($output,0, strlen(rtrim($output)) -1);
                                            $output = '';
                                        }
                                    }
                                }
                                
                                // var_dump($inserts);
                                // exit();
                                //transaccion
                                if(!empty($inserts)){
                                    $this->oNegInstall->transacInsert($inserts);
                                }
                            // }
                            
                        }//end if foreach archivos .txt
                    }//end if unzip
                }//end foreach files
            }
            
            echo json_encode(array('code'=>'ok','message'=>'complete'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
    public function crearconfiguracion(){
        global $aplicacion;
        $this->documento->plantilla = 'blanco';
        try{
            $target = (isset($_POST['target'])) ? $_POST['target'] : null;
            $local = (isset($_POST['local'])) ? $_POST['local'] : null;
            $idproyecto = (isset($_POST['idproyecto'])) ? $_POST['idproyecto'] : null;
            if(is_null($idproyecto) || is_null($local) || is_null($target)){
                throw new Exception("Campos sin definir");
            }
$document = <<<'EOD'
<?php
  return array(
      'root' => '/pvingles.local/sync',
      'target' => $target,
      'offline' => false,
      'idproyecto' => $idproyecto,
      'local' => $local
  );
?>
EOD;
            $document=str_replace('$target',"'".$target."'",$document);
            $document=str_replace('$idproyecto',$idproyecto,$document);
            $document=str_replace('$local',$local,$document);
            var_dump($document);
            if(!file_put_contents(RUTA_BASE."sync/config/config.php",$document)){
                throw new Exception("No se logro crear el archivo de configuracion");
            }
            echo json_encode(array('code'=>'ok','data'=>$resultado));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
}
?>