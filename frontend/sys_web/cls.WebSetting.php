<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebSetting extends JrWeb
{
	private $oNegGeneral;
	private $usuarioAct;
	private $oNegAcad_curso;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegGeneral = new NegGeneral;
		$this->oNegAcad_curso = new NegAcad_curso;

		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->versetting();
	}

	public function versetting(){
		global $aplicacion;
		$idproyecto=$this->usuarioAct["idproyecto"];
		$cursos=$this->oNegAcad_curso->buscar( array('idproyecto'=>$idproyecto,'estado'=>1));
		$this->datosintentos=array();
		if(!empty($cursos))
			foreach ($cursos as $cur){
				$nintentos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'DBYnintento_P'.$idproyecto,'nombre'=>$cur["idcurso"]));
				$idgeneral='';
				$nintento=3;
				if(!empty($nintentos[0])){
					$idgeneral=$nintentos[0]["idgeneral"];
					$nintento=$nintentos[0]["codigo"];
				}

				$this->datosintentos[]=array('idgeneral'=>$idgeneral,'codigo'=>$nintento,'nombre'=>$cur["idcurso"],
					'tipo_tabla'=>'DBYnintento_P'.$idproyecto,'mostrar'=>1,'cursonombre'=>$cur["nombre"]);
			}

		//$this->nintentos=!empty($nintentos[0])?$nintentos[0]["codigo"]:3;
		$this->documento->setTitulo(JrTexto::_('Setting'), true);
        $this->esquema = 'doc_temas';            
        return parent::getEsquema();
	} 
}