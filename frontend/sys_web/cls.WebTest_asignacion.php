<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		01-02-2019 
 * @copyright	Copyright (C) 01-02-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTest_asignacion', RUTA_BASE, 'sys_negocio');
class WebTest_asignacion extends JrWeb
{
	private $oNegTest_asignacion;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTest_asignacion = new NegTest_asignacion;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_asignacion', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtestasigancion"])&&@$_REQUEST["idtestasigancion"]!='')$filtros["idtestasigancion"]=$_REQUEST["idtestasigancion"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
			
			$this->datos=$this->oNegTest_asignacion->buscar($filtros);
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Test_asignacion'), true);
			$this->esquema = 'test_asignacion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_asignacion', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Test_asignacion').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_asignacion', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTest_asignacion->idtestasigancion = @$_GET['id'];
			$this->datos = $this->oNegTest_asignacion->dataTest_asignacion;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Test_asignacion').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'test_asignacion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_asignacion', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtestasigancion"])&&@$_REQUEST["idtestasigancion"]!='')$filtros["idtestasigancion"]=$_REQUEST["idtestasigancion"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idrecurso"])&&@$_REQUEST["idrecurso"]!='')$filtros["idrecurso"]=$_REQUEST["idrecurso"];
			if(isset($_REQUEST["situacion"])&&@$_REQUEST["situacion"]!='')$filtros["situacion"]=$_REQUEST["situacion"];
						
			$this->datos=$this->oNegTest_asignacion->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarTest_asignacion(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idtestasigancion)) {
				$this->oNegTest_asignacion->idtestasigancion = $idtestasigancion;
				$accion='_edit';
			}
			$this->oNegTest_asignacion->idtest=@$txtIdtest;
			$this->oNegTest_asignacion->idcurso=@$txtIdcurso;
			$this->oNegTest_asignacion->idrecurso=@$txtIdrecurso;
			$this->oNegTest_asignacion->situacion=@$txtSituacion;
					
            if($accion=='_add') {
            	$res=$this->oNegTest_asignacion->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_asignacion')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegTest_asignacion->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_asignacion')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminarTest_asignacion()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idtestasigancion)) {
				$this->oNegTest_asignacion->idtestasigancion = $idtestasigancion;
				$this->oNegTest_asignacion->eliminar();
			}
		     echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_asignacion')).' '.JrTexto::_('delete successfully'))); 

            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}	
	// ========================== Funciones xajax ========================== //
	public function xSaveTest_asignacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtestasigancion'])) {
					$this->oNegTest_asignacion->idtestasigancion = $frm['pkIdtestasigancion'];
				}
				
				$this->oNegTest_asignacion->idtest=@$frm["txtIdtest"];
					$this->oNegTest_asignacion->idcurso=@$frm["txtIdcurso"];
					$this->oNegTest_asignacion->idrecurso=@$frm["txtIdrecurso"];
					$this->oNegTest_asignacion->situacion=@$frm["txtSituacion"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegTest_asignacion->agregar();
					}else{
									    $res=$this->oNegTest_asignacion->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTest_asignacion->idtestasigancion);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTest_asignacion(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_asignacion->__set('idtestasigancion', $pk);
				$this->datos = $this->oNegTest_asignacion->dataTest_asignacion;
				$res=$this->oNegTest_asignacion->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_asignacion->__set('idtestasigancion', $pk);
				$res=$this->oNegTest_asignacion->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegTest_asignacion->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}