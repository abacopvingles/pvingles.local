<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		10-01-2018 
 * @copyright	Copyright (C) 10-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoaula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_horariogrupodetalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGeneral', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersona_rol', RUTA_BASE, 'sys_negocio');
class WebAcad_matricula extends JrWeb
{
	private $oNegAcad_matricula;
	private $oNegAcad_grupoaula;
	private $oNegHorariogrupoaula;
	private $oNegAcad_grupoauladetalle;
	private $oNegGeneral;
	private $oNegLocal;
	private $oNegPersonal;
	private $oNegPersona_rol;	
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
		$this->oNegAcad_grupoaula = new NegAcad_grupoaula;
		$this->oNegAcad_grupoauladetalle = new NegAcad_grupoauladetalle;
		$this->oNegHorariogrupoaula = new NegAcad_horariogrupodetalle;	
		$this->oNegGeneral = new NegGeneral;
		$this->oNegLocal = new NegLocal;
		$this->oNegPersonal = new NegPersonal;
		$this->oNegPersona_rol = new NegPersona_rol;
		$this->usuarioAct = NegSesion::getUsuario();
	}

	public function defecto(){
		return $this->filtros();
	}


	public function filtros(){
		try{
			global $aplicacion;	
			$usuarioAct = NegSesion::getUsuario();
			$this->idproyecto=!empty($usuarioAct["idproyecto"])?$usuarioAct["idproyecto"]:3;
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			
			$filtros=array();
			$this->fkestados=$this->oNegGeneral->buscar(array('tipo_tabla'=>'estadogrupo','mostrar'=>1));
			if(!empty($_REQUEST["estadogrupo"]))$filtros["estado"]=$this->fkestado=$_REQUEST["estadogrupo"];

			$this->fktipos=$this->oNegGeneral->buscar(array('tipo_tabla'=>'tipogrupo','mostrar'=>1));
			$this->fklocales=$this->oNegLocal->buscar(array('idproyecto'=>$this->idproyecto));
			if(!empty($_REQUEST["tipo"])){
				$filtros["tipo"]=$this->fktipo=$_REQUEST["tipo"];
				if($this->fktipo!='V'){
					if(isset($_REQUEST["idlocal"])&&@$_REQUEST["idlocal"]!='')$filtros["idlocal"]=$this->fkidlocal=$_REQUEST["idlocal"];
				}
			}
			$filtros["idproyecto"]=$this->idproyecto;
			$this->gruposaula=$this->oNegAcad_grupoaula->buscar($filtros);
			$this->esquema = 'academico/matriculas';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idgrupoaula"])&&@$_REQUEST["idgrupoaula"]!='')$filtros["idgrupoaula"]=$_REQUEST["idgrupoaula"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$fktipo=ucfirst($_REQUEST["tipo"]);
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			//var_dump($_REQUEST);			
			$this->grupos=$this->oNegAcad_matricula->buscar($filtros);
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'mantenimientos';
			$vista=!empty($_REQUEST['vista']) ? $_REQUEST['vista'] : 1;
			$this->documento->setTitulo(JrTexto::_('Matricula'), true);
			$this->esquema = 'academico/matriculas-list-vista0'.$vista;			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_matricula->idmatricula = @$_GET['id'];
			$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_matricula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			if(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')$filtros["idproyecto"]=$_REQUEST["idproyecto"];
			if(empty($filtros["idproyecto"])) $filtros["idproyecto"]=$this->usuarioAct["idproyecto"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function eliminarmatricula(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			@extract($_POST);
			if(!empty($idmatricula)){
				$this->oNegAcad_matricula->__set('idmatricula', $idmatricula);
				$res=$this->oNegAcad_matricula->eliminar();
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('matricula')).' '.JrTexto::_('delete successfully')));
			}else{
				echo json_encode(array('code'=>'Error','msj'=>ucfirst(JrTexto::_('Matricula')).' '.JrTexto::_('empty')));
			}
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	private function registrarmatricula($datos){
		$idgrupoauladetalle=@$datos["idgrupoauladetalle"];
		$idalumno=@$datos["idalumno"];
		$idmatricula=@$datos["idmatricula"];
		$estado=!empty($datos["estado"])?$datos["estado"]:1;
		$fecha_matricula=!empty($datos["fecha_matricula"])?$datos["fecha_matricula"]:date('Y-m-d h:i:s');
		$accion='add_';
		$vermatricula='';
		if(!empty($idmatricula)){
			$vermatricula=$this->oNegAcad_matricula->buscar(array('idmatricula'=>$idmatricula));
		}else
			$vermatricula=$this->oNegAcad_matricula->buscar(array('idgrupoauladetalle'=>$idgrupoauladetalle,'idalumno'=>$idalumno,'estado'=>1));

		$accion='add_';
		if(!empty($vermatricula)){
			$this->oNegAcad_matricula->idmatricula = $vermatricula[0]["idmatricula"];
			$accion='editar';
		}
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegAcad_matricula->idgrupoauladetalle=$idgrupoauladetalle;
		$this->oNegAcad_matricula->idalumno=$idalumno;
		$this->oNegAcad_matricula->estado=$estado;
		$this->oNegAcad_matricula->idusuario=$usuarioAct["idpersona"];	
        $this->oNegAcad_matricula->fecha_registro=date('Y-m-d');
		$this->oNegAcad_matricula->fecha_matricula=$fecha_matricula;
		if($accion=='add_'){
			return $this->oNegAcad_matricula->agregar();
		}else{
			return $this->oNegAcad_matricula->editar();
		}
	}

	public function guardarAcad_matriculavarios(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $datos=json_decode($alumnos);
           if($datos)
            foreach ($datos as $key =>$alu){
            	$alumno=$this->oNegPersonal->buscar(array('dni'=>$alu->dni));
            	$accion='add';
				if(empty($alumno[0])){					
					$this->oNegPersonal->set('regusuario',$usuarioAct["dni"]);
					$this->oNegPersonal->ape_paterno=!empty($alu->ape_paterno)?$alu->ape_paterno:'';
					$this->oNegPersonal->ape_materno=!empty($alu->ape_materno)?$alu->ape_materno:'';
					$this->oNegPersonal->nombre=!empty($alu->nombre)?$alu->nombre:'';
					$this->oNegPersonal->fechanac=!empty($alu->fechanac)?$alu->fechanac:date('Y-m-d');
					$this->oNegPersonal->sexo=!empty($alu->sexo)?$alu->sexo:'M';
					$this->oNegPersonal->estado_civil=!empty($alu->estado_civil)?$alu->estado_civil:'S';
					$this->oNegPersonal->ubigeo='010101';
					$this->oNegPersonal->urbanizacion=!empty($alu->urbanizacion)?$alu->urbanizacion:'';
					$this->oNegPersonal->direccion=!empty($alu->direccion)?$alu->direccion:'';
					$this->oNegPersonal->telefono=!empty($alu->telefono)?$alu->telefono:'000';
					$this->oNegPersonal->celular=!empty($alu->celular)?$alu->celular:'111';
					$this->oNegPersonal->email=!empty($alu->email)?$alu->email:'';
					$this->oNegPersonal->idugel=!empty($alu->idugel)?$alu->idugel:'';
					$this->oNegPersonal->regfecha=!empty($alu->regfecha)?$alu->regfecha:date('Y-m-d');
					$this->oNegPersonal->usuario=!empty($alu->usuario)?$alu->usuario:'';
					$this->oNegPersonal->clave=!empty($alu->clave)?$alu->clave:'12345678';
					$this->oNegPersonal->token=!empty($alu->token)?$alu->token:'';
					$this->oNegPersonal->rol=!empty($alu->rol)?$alu->rol:3;
					$this->oNegPersonal->estado=!empty($alu->estado)?$alu->estado:1;
					$this->oNegPersonal->situacion=!empty($alu->situacion)?$alu->situacion:1;
					$this->oNegPersonal->idioma=!empty($alu->idioma)?$alu->idioma:'ES';
					$this->oNegPersonal->foto=!empty($alu->foto)?$alu->foto:'';
					$dni=$this->oNegPersonal->agregar($alu->dni);
				}else{
					$dni=$alu->dni;
				}
				$datos=array();
	            $datos["idgrupoauladetalle"]=@$idgrupoauladetalle;
				$datos["idalumno"]=$dni;
				$datos["idmatricula"]=@$idmatricula;
				$datos["estado"]=@$estado;
				$datos["fecha_matricula"]=@$fecha_matricula;
				if(!empty($idgrupoauladetalle)){
            		$newid=$this->registrarmatricula($datos);            	
	            }elseif(!empty($idgrupoaula)){
	            	$detallegrupoaulas=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula'=>$idgrupoaula));
	        		if(!empty($detallegrupoaulas)){        			
	        			foreach ($detallegrupoaulas as $ga){
	        				$datos["idgrupoauladetalle"]=$ga["idgrupoauladetalle"];
	        				$this->registrarmatricula($datos);        				
	        			}	        			
	        		}
	            }
            }
            echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('matriculas')).' '.JrTexto::_('saved successfully'),'newid'=>'all'));
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }

	}

	public function guardarAcad_matricula(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $datos=array();
            $datos["idgrupoauladetalle"]=@$idgrupoauladetalle;
			$datos["idalumno"]=@$idalumno;
			$datos["idmatricula"]=@$idmatricula;
			$datos["estado"]=!empty($estado)?$estado:1;
			$datos["fecha_matricula"]=!empty($fecha_matricula)?$fecha_matricula:date('Y-m-d');
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();			
			$this->oNegPersona_rol->idrol=$idrol;
			$this->oNegPersona_rol->idproyecto=$usuarioAct["idproyecto"];
			$this->oNegPersona_rol->idempresa=$usuarioAct["idempresa"];
			$per=$this->oNegPersonal->buscar(array('idpersona'=>@$idalumno));
			if(!empty($per[0])){
				$this->oNegPersona_rol->idpersonal=$per[0]['idpersona'];
				$this->oNegPersona_rol->agregar2();
			}
            if(!empty($idgrupoauladetalle)){
            	$newid=$this->registrarmatricula($datos);
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('matricula')).' '.JrTexto::_('saved successfully'),'newid'=>$newid)); 
            }elseif(!empty($idgrupoaula)){
            	$detallegrupoaulas=$this->oNegAcad_grupoauladetalle->buscar(array('idgrupoaula'=>$idgrupoaula));
        		if(!empty($detallegrupoaulas)){        			
        			foreach ($detallegrupoaulas as $ga){
        				$datos["idgrupoauladetalle"]=$ga["idgrupoauladetalle"];
        				$this->registrarmatricula($datos);        				
        			}
        			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('matricula')).' '.JrTexto::_('saved successfully'),'newid'=>'all'));
        		}
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveAcad_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdmatricula'])) {
					$this->oNegAcad_matricula->idmatricula = $frm['pkIdmatricula'];
				}
				
				$this->oNegAcad_matricula->idgrupoauladetalle=@$frm["txtIdgrupoauladetalle"];
					$this->oNegAcad_matricula->idalumno=@$frm["txtIdalumno"];
					$this->oNegAcad_matricula->fecha_registro=@$frm["txtFecha_registro"];
					$this->oNegAcad_matricula->estado=@$frm["txtEstado"];
					$this->oNegAcad_matricula->idusuario=@$frm["txtIdusuario"];
					$this->oNegAcad_matricula->fecha_matricula=@$frm["txtFecha_matricula"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAcad_matricula->agregar();
					}else{
									    $res=$this->oNegAcad_matricula->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAcad_matricula->idmatricula);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAcad_matricula(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_matricula->__set('idmatricula', $pk);
				$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
				$res=$this->oNegAcad_matricula->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAcad_matricula->__set('idmatricula', $pk);
				$res=$this->oNegAcad_matricula->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_matricula->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}