<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-01-2018 
 * @copyright	Copyright (C) 17-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegManuales_alumno', RUTA_BASE, 'sys_negocio');
class WebManuales_alumno extends JrWeb
{
	private $oNegManuales_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegManuales_alumno = new NegManuales_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Manuales_alumno', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["identrega"])&&@$_REQUEST["identrega"]!='')$filtros["identrega"]=$_REQUEST["identrega"];
			if(isset($_REQUEST["idgrupo"])&&@$_REQUEST["idgrupo"]!='')$filtros["idgrupo"]=$_REQUEST["idgrupo"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idmanual"])&&@$_REQUEST["idmanual"]!='')$filtros["idmanual"]=$_REQUEST["idmanual"];
			if(isset($_REQUEST["idaulagrupodetalle"])&&@$_REQUEST["idaulagrupodetalle"]!='')$filtros["idaulagrupodetalle"]=$_REQUEST["idaulagrupodetalle"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["serie"])&&@$_REQUEST["serie"]!='')$filtros["serie"]=$_REQUEST["serie"];
			
			$this->datos=$this->oNegManuales_alumno->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Manuales_alumno'), true);
			$this->esquema = 'manuales_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Manuales_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Manuales_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Manuales_alumno', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegManuales_alumno->identrega = @$_GET['id'];
			$this->datos = $this->oNegManuales_alumno->dataManuales_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Manuales_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'manuales_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Manuales_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["identrega"])&&@$_REQUEST["identrega"]!='')$filtros["identrega"]=$_REQUEST["identrega"];
			if(isset($_REQUEST["idgrupo"])&&@$_REQUEST["idgrupo"]!='')$filtros["idgrupo"]=$_REQUEST["idgrupo"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idmanual"])&&@$_REQUEST["idmanual"]!='')$filtros["idmanual"]=$_REQUEST["idmanual"];
			if(isset($_REQUEST["idaulagrupodetalle"])&&@$_REQUEST["idaulagrupodetalle"]!='')$filtros["idaulagrupodetalle"]=$_REQUEST["idaulagrupodetalle"];
			if(isset($_REQUEST["comentario"])&&@$_REQUEST["comentario"]!='')$filtros["comentario"]=$_REQUEST["comentario"];
			if(isset($_REQUEST["fecha"])&&@$_REQUEST["fecha"]!='')$filtros["fecha"]=$_REQUEST["fecha"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["serie"])&&@$_REQUEST["serie"]!='')$filtros["serie"]=$_REQUEST["serie"];
						
			$this->datos=$this->oNegManuales_alumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarManuales_alumno(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add'; 
            if(!empty($identrega)){
            	$rs=$this->oNegManuales_alumno->buscar(array('identrega'=>$identrega));				
			}elseif(!empty($idalumno)&&!empty($idmanual)){
				$rs=$this->oNegManuales_alumno->buscar(array('idalumno'=>$idalumno,'idmanual'=>$idmanual));
			}

			if(!empty($rs[0])){
				$identrega=$rs[0]["identrega"];
				$this->oNegManuales_alumno->identrega = $identrega;
				 $accion='_edit'; 
			}
           	//$usuarioAct = NegSesion::getUsuario();  
			$this->oNegManuales_alumno->idgrupo=@$idgrupo;
			$this->oNegManuales_alumno->idalumno=@$idalumno;
			$this->oNegManuales_alumno->idmanual=@$idmanual;
			$this->oNegManuales_alumno->idaulagrupodetalle=@$idaulagrupodetalle;
			$this->oNegManuales_alumno->comentario=@$comentario;			
			$this->oNegManuales_alumno->estado=@$estado;
			$this->oNegManuales_alumno->serie=@$serie;
					
            if($accion=='_add'){
            	$this->oNegManuales_alumno->fecha=@$fecha;
            	$res=$this->oNegManuales_alumno->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Manuales_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res,'accion'=>$accion)); 
            }else{
            	$res=$this->oNegManuales_alumno->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Manuales_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res,'accion'=>$accion)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveManuales_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdentrega'])) {
					$this->oNegManuales_alumno->identrega = $frm['pkIdentrega'];
				}
				
				$this->oNegManuales_alumno->idgrupo=@$frm["txtIdgrupo"];
					$this->oNegManuales_alumno->idalumno=@$frm["txtIdalumno"];
					$this->oNegManuales_alumno->idmanual=@$frm["txtIdmanual"];
					$this->oNegManuales_alumno->idaulagrupodetalle=@$frm["txtIdaulagrupodetalle"];
					$this->oNegManuales_alumno->comentario=@$frm["txtComentario"];
					$this->oNegManuales_alumno->fecha=@$frm["txtFecha"];
					$this->oNegManuales_alumno->estado=@$frm["txtEstado"];
					$this->oNegManuales_alumno->serie=@$frm["txtSerie"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegManuales_alumno->agregar();
					}else{
									    $res=$this->oNegManuales_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegManuales_alumno->identrega);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDManuales_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegManuales_alumno->__set('identrega', $pk);
				$this->datos = $this->oNegManuales_alumno->dataManuales_alumno;
				$res=$this->oNegManuales_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegManuales_alumno->__set('identrega', $pk);
				$res=$this->oNegManuales_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegManuales_alumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}