<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		06-02-2019 
 * @copyright	Copyright (C) 06-02-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTest_alumno', RUTA_BASE, 'sys_negocio');
class WebTest_alumno extends JrWeb
{
	private $oNegTest_alumno;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTest_alumno = new NegTest_alumno;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_alumno', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idtestalumno"])&&@$_REQUEST["idtestalumno"]!='')$filtros["idtestalumno"]=$_REQUEST["idtestalumno"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["idtestcriterio"])&&@$_REQUEST["idtestcriterio"]!='')$filtros["idtestcriterio"]=$_REQUEST["idtestcriterio"];
			if(isset($_REQUEST["idtestasigancion"])&&@$_REQUEST["idtestasigancion"]!='')$filtros["idtestasigancion"]=$_REQUEST["idtestasigancion"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			
			$this->datos=$this->oNegTest_alumno->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Test_alumno'), true);
			$this->esquema = 'test_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Test_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_alumno', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegTest_alumno->idtestalumno = @$_GET['id'];
			$this->datos = $this->oNegTest_alumno->dataTest_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Test_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'test_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Test_alumno', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idtestalumno"])&&@$_REQUEST["idtestalumno"]!='')$filtros["idtestalumno"]=$_REQUEST["idtestalumno"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["idtest"])&&@$_REQUEST["idtest"]!='')$filtros["idtest"]=$_REQUEST["idtest"];
			if(isset($_REQUEST["idtestcriterio"])&&@$_REQUEST["idtestcriterio"]!='')$filtros["idtestcriterio"]=$_REQUEST["idtestcriterio"];
			if(isset($_REQUEST["idtestasigancion"])&&@$_REQUEST["idtestasigancion"]!='')$filtros["idtestasigancion"]=$_REQUEST["idtestasigancion"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
						
			$this->datos=$this->oNegTest_alumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarTest_alumno(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty(@$idtestalumno)) {
				$this->oNegTest_alumno->idtestalumno = $idtestalumno;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           	
				$this->oNegTest_alumno->idalumno=@$usuarioAct["idpersona"];
					$this->oNegTest_alumno->idtest=@$idtest;
					$this->oNegTest_alumno->idtestcriterio=@$idtestcriterio;
					$this->oNegTest_alumno->idtestasigancion=@$idtestasigancion;
					$this->oNegTest_alumno->puntaje=@$puntaje;
					
            if($accion=='_add') {
            	$res=$this->oNegTest_alumno->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_alumno')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegTest_alumno->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Test_alumno')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveTest_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtestalumno'])) {
					$this->oNegTest_alumno->idtestalumno = $frm['pkIdtestalumno'];
				}
				
				$this->oNegTest_alumno->idalumno=@$frm["txtIdalumno"];
					$this->oNegTest_alumno->idtest=@$frm["txtIdtest"];
					$this->oNegTest_alumno->idtestcriterio=@$frm["txtIdtestcriterio"];
					$this->oNegTest_alumno->idtestasigancion=@$frm["txtIdtestasigancion"];
					$this->oNegTest_alumno->puntaje=@$frm["txtPuntaje"];
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegTest_alumno->agregar();
					}else{
									    $res=$this->oNegTest_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTest_alumno->idtestalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTest_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_alumno->__set('idtestalumno', $pk);
				$this->datos = $this->oNegTest_alumno->dataTest_alumno;
				$res=$this->oNegTest_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTest_alumno->__set('idtestalumno', $pk);
				$res=$this->oNegTest_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegTest_alumno->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}