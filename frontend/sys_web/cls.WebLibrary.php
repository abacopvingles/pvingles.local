    <?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       31-05-2017 
 * @copyright   Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLibre_tema', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLibre_palabras', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
// #JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_grupoauladetalle', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
// JrCargador::clase('sys_negocio::NegAcad_cursohabilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
class WebLibrary extends JrWeb
{
    protected $oNegbib_estudio;

    // private $oNegTarea_asignacion;
    // private $oNegTarea_asignacion_alumno;
    // protected $oNegNiveles;
    // protected $oNegTarea_archivos;
    // #protected $oNegGrupos;
    protected $oNegGrupoAulaDet;
    private $oNegTema;
    private $oNegPalabras;
    // protected $oNegLocal;
    // private $oNegTarea_respuesta;
    // protected $oNegHabilidad;
    protected $oNegMatricula;
    // protected $oNegCursodetalle;
    protected $oNegCurso;

    public function __construct()
    {
        parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegbib_estudio = new Negbib_estudio;
        $this->oNegTema = new NegLibre_tema;
        $this->oNegPalabras = new NegLibre_palabras;
        // $this->oNegTarea_asignacion = new NegTarea_asignacion;
        // $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        // $this->oNegNiveles = new NegNiveles;
        // $this->oNegTarea_archivos = new NegTarea_archivos;
        // #$this->oNegGrupos = new NegGrupos;
        $this->oNegGrupoAulaDet = new NegAcad_grupoauladetalle;
        // $this->oNegLocal = new NegLocal;
        // $this->oNegTarea_respuesta = new NegTarea_respuesta;
        // $this->oNegHabilidad = new NegMetodologia_habilidad;
        // $this->oNegAcad_habilidad = new NegAcad_cursohabilidad;
        $this->oNegMatricula = new NegAcad_matricula;
        // $this->oNegCursodetalle = new NegAcad_cursodetalle;
        $this->oNegCurso = new NegAcad_curso;
    }

    public function defecto(){
        return $this->listado();
    }

    public function listado()
    {
        try{
            global $aplicacion;
            /*if(!NegSesion::tiene_acceso('Tarea', 'list') && $this->usuarioAct['rol']!='Alumno') {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }*/
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->miscolegios=$hayidcolegios=array();
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            $this->idgrupoauladetalle;
            $this->idcurso;
            if($this->idrol==3){
                header('Location: '. $this->documento->getUrlBase().'/library/library'); exit(0);
                $this->esquema = 'libreria/libreria-list-alumno';
            }else if($this->idrol==1){
                $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
                $this->esquema = 'libreria/libreria-list';
            }
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->documento->setTitulo(JrTexto::_('Activity'), true);
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function library()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
                $this->esquema = 'libreria/libreria-list-alumno';
            }elseif ($rol==2) {
                $this->miscursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));     
                $this->esquema = 'libreria/libreria-list-docente';
            }

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function vocabulary()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
            }

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'libreria/vocabulary';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function audiobooks()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
            }

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'libreria/audiobooks';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function educationalvideo()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
            }

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'libreria/educational-video';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function songs()
    {
        try {
            global $aplicacion;            
            $rol=$this->usuarioAct["idrol"];
            $this->idrol=$rol;          
            $cursosProyecto = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>@$this->usuarioAct["idproyecto"]));
            $this->idCurso=!empty($cursosProyecto[0])?$cursosProyecto[0]["idcurso"]:-1;
            $this->cursos=$cursosProyecto;
            
            if ($rol==3){
                $this->miscursos = $this->oNegMatricula->buscar(array('idalumno'=>$this->usuarioAct['idpersona'], "idproyecto"=>@$this->usuarioAct["idproyecto"]));
                $this->idCurso=!empty($this->miscursos[0])?$this->miscursos[0]["idcurso"]:-1;
            }

            $this->documento->setTitulo(JrTexto::_('Homework').' /'.JrTexto::_('activities'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'libreria/songs';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function listarMaterial(){
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            if (isset($_REQUEST["idcurso"])) {
                $this->idcurso=$_REQUEST["idcurso"];
                // $this->nombre=$_REQUEST["nombre"];
                
            }else{
                $this->idcurso=0;
            }
            global $aplicacion;
            $this->todoLibros=array();
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>1,'orden'=>0));
                }else{
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>1,'orden'=>0));
                }
            // $this->todoTareas=$this->oNegbib_estudio->buscar(array('id_detalle'=>$this->idcurso,'id_tipo'=>1));
            
            }else if($usuarioAct['idrol']==3){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>1,'orden'=>0));
                }else{
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>1,'orden'=>0));
                }
            }elseif ($usuarioAct['idrol']==2) {
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>1,'orden'=>0));
                }else{
                    $this->todoLibros=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>1,'orden'=>0));
                }
            }
            $respuesta = array("todo"=>$this->todoLibros);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function listarVideos(){
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            if (isset($_REQUEST["idcurso"])) {
                $this->idcurso=$_REQUEST["idcurso"];
                
            }else{
                $this->idcurso=0;
            }
            global $aplicacion;
            $this->todoVideos=array();
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>6,'orden'=>0));
                }else{
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>6,'orden'=>0));
                }
            // $this->todoTareas=$this->oNegbib_estudio->buscar(array('id_detalle'=>$this->idcurso,'id_tipo'=>1));
            
            }else if($usuarioAct['idrol']==3){
                // $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>6,'orden'=>0));
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>6,'orden'=>0));
                }else{
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>6,'orden'=>0));
                }
            }elseif ($usuarioAct['idrol']==2) {
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>6,'orden'=>0));
                }else{
                    $this->todoVideos=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>6,'orden'=>0));
                }
            }
            $respuesta = array("todo"=>$this->todoVideos);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function listarAudios(){
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            if (isset($_REQUEST["idcurso"])) {
                $this->idcurso=$_REQUEST["idcurso"];
                
            }else{
                $this->idcurso=0;
            }
            global $aplicacion;
            $this->todoAudios=array();
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>4,'orden'=>0));
                }else{
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                }
            // $this->todoTareas=$this->oNegbib_estudio->buscar(array('id_detalle'=>$this->idcurso,'id_tipo'=>1));
            // $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
            }else if($usuarioAct['idrol']==3){
                // $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>4,'orden'=>0));
                }else{
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                }
            }elseif ($usuarioAct['idrol']==2) {
                // $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>4,'orden'=>0));
                }else{
                    $this->todoAudios=$this->oNegbib_estudio->buscarmaterial(array('id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                }
            }
            $respuesta = array("todo"=>$this->todoAudios);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function listarMusica(){
        $this->documento->plantilla = 'returnjson'; // como docente
        try {
            if (isset($_REQUEST["idcurso"])) {
                $this->idcurso=$_REQUEST["idcurso"];
                
            }else{
                $this->idcurso=0;
            }
            global $aplicacion;
            $this->todoMusica=array();
            $usuarioAct = NegSesion::getUsuario();
            if($usuarioAct['idrol']==1){
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>5,'orden'=>0));
                }else{
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'id_tipo'=>5,'orden'=>0));
                }
            // $this->todoTareas=$this->oNegbib_estudio->buscar(array('id_detalle'=>$this->idcurso,'id_tipo'=>1));
            
            }elseif($usuarioAct['idrol']==3){
                // $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'id_tipo'=>5,'orden'=>0));
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>5,'orden'=>0));
                }else{
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'id_tipo'=>5,'orden'=>0));
                }
            }elseif ($usuarioAct['idrol']==2) {
                // $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'id_tipo'=>5,'orden'=>0));
                if (isset($_REQUEST["nombre"])) {
                    $this->nombre=$_REQUEST["nombre"];
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'nombre'=>$this->nombre,'id_tipo'=>5,'orden'=>0));
                }else{
                    $this->todoMusica=$this->oNegbib_estudio->buscarmusica(array('id_detalle'=>$this->idcurso,'id_tipo'=>5,'orden'=>0));
                }
            }
            // print_r($this->todoMusica);
            $respuesta = array("todo"=>$this->todoMusica);
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function editar()
    {
        try {
            global $aplicacion;
            $this->getPlugins();
            
            $usuarioAct = NegSesion::getUsuario();
            $this->frmaccion='Editar';
            $this->breadcrumb=JrTexto::_('Edit');
            $this->pk=@$_GET['id'];
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            
            
               

            $this->oNegbib_estudio->setLimite(0,9999);
            $material = $this->oNegbib_estudio->buscar(array('id_estudio'=>$_GET['id_estudio']));
            if(empty($material)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = $material[0];
            if($this->idrol==1){
                 $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
            }
            if(!empty($this->cursos)&&!empty($_GET["idcurso"])){
                $this->idcurso=$_GET["idcurso"];
            }
            // $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
            // $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));
           // $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('fechahora_mayorigual'=>date('Y-m-d H:i:s'), 'idtarea'=>$this->datos['idtarea']));
            // $this->datos["curso_detalles"] = $this->oNegCursodetalle->getTodosPadresXCursoDet(array("idcursodetalle"=>$this->datos['idcursodetalle']),'DESC');
            // $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>1));
           // var_dump($this->habilidades);
            /*$this->grupos=$this->oNegGrupoAulaDet->buscar(array('iddocente'=>$usuarioAct["idpersona"]));
            $this->locales=[];
            $arrLocales = [];
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $buscarLocales = $this->oNegLocal->buscar(array("idlocal"=>$g['idlocal']));
                    if(!empty($buscarLocales)) {
                        $this->locales[] = $buscarLocales[0];
                    }
                    $arrLocales[]=$g['idlocal'];
                }
            }*/
            
            $this->documento->setTitulo(JrTexto::_('Library').' /'.JrTexto::_('Edit'), true);
            return $this->form();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function editar2()
    {
        try {
            global $aplicacion;
            $this->getPlugins();
            
            $usuarioAct = NegSesion::getUsuario();
            $this->frmaccion='Editar';
            $this->breadcrumb=JrTexto::_('Edit');
            $this->pk=@$_GET['id'];
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];
            
            
               

            $this->oNegTema->setLimite(0,9999);
            $material = $this->oNegTema->buscar(array('idtema'=>$_GET['idtema']));
            if(empty($material)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = $material[0];
            if($this->idrol==1){
                 $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
            }
            if(!empty($this->cursos)&&!empty($_GET["idcurso"])){
                $this->idcurso=$_GET["idcurso"];
            }
            // $this->cursos = $this->oNegCurso->buscar(array('estado'=>1, "idproyecto"=>$usu["idproyecto"]));
            // $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));
           // $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('fechahora_mayorigual'=>date('Y-m-d H:i:s'), 'idtarea'=>$this->datos['idtarea']));
            // $this->datos["curso_detalles"] = $this->oNegCursodetalle->getTodosPadresXCursoDet(array("idcursodetalle"=>$this->datos['idcursodetalle']),'DESC');
            // $this->habilidades = $this->oNegHabilidad->buscar(array("tipo"=>'H', "estado"=>1));
           // var_dump($this->habilidades);
            /*$this->grupos=$this->oNegGrupoAulaDet->buscar(array('iddocente'=>$usuarioAct["idpersona"]));
            $this->locales=[];
            $arrLocales = [];
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $buscarLocales = $this->oNegLocal->buscar(array("idlocal"=>$g['idlocal']));
                    if(!empty($buscarLocales)) {
                        $this->locales[] = $buscarLocales[0];
                    }
                    $arrLocales[]=$g['idlocal'];
                }
            }*/
            
            $this->documento->setTitulo(JrTexto::_('Library').' /'.JrTexto::_('Edit'), true);
            return $this->form2();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    private function form()
    {
        try {
            global $aplicacion;      
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');       
            //$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');
            $this->newacc=!empty($_REQUEST["acc"])?$_REQUEST["acc"]:false;
            $this->esquema = 'libreria/libreria-frm';
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    private function form2()
    {
        try {
            global $aplicacion;             
            //$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');
            $this->newacc=!empty($_REQUEST["acc"])?$_REQUEST["acc"]:false;
            $this->esquema = 'libreria/libreria-frmvoc';
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    private function formver($tipo)
    {
        try {
            global $aplicacion;   
            $usuarioAct = NegSesion::getUsuario();   
            $usu=$this->usuarioAct;
            $this->idrol=$usu['idrol'];       
            //$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');
            // $this->newacc=!empty($_REQUEST["acc"])?$_REQUEST["acc"]:false;
            if ($tipo=='L') {
                $this->esquema = 'libreria/libreria-ver-alumno';
            }elseif($tipo=='V'){
                $this->esquema = 'libreria/libreria-ver-alumno-video';
            }elseif ($tipo=='A') {
                // if($usuarioAct['idrol']==3){
                    $this->todoAudios=$this->oNegbib_estudio->buscaraudios(array('nombre'=>$this->datos['nombre'],'id_detalle'=>$this->idcurso,'id_tipo'=>4,'orden'=>0));
                // }
                
                $this->esquema = 'libreria/libreria-ver-alumno-audio';
            }elseif ($tipo=='M') {
                $this->esquema = 'libreria/libreria-ver-alumno-musica';
            }elseif ($tipo=='Vo') {
                return $this->contenido();
                // $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
                // $this->documento->script('jquery-confirm.min', '/libs/alert/');
                // $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
                // $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
                // $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
                // $this->documento->script('pronunciacion', '/js/');

                // $filtros1=array();
                // if(isset($_REQUEST["idpalabra"])&&@$_REQUEST["idpalabra"]!='')$filtros1["idpalabra"]=$_REQUEST["idpalabra"];
                // if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros1["idtema"]=$_REQUEST["idtema"];
                // if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros1["palabra"]=$_REQUEST["palabra"];
                // if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros1["audio"]=$_REQUEST["audio"];
                // if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros1["idagrupacion"]=$_REQUEST["idagrupacion"];
                // if(isset($_REQUEST["url_iframe"])&&@$_REQUEST["url_iframe"]!='')$filtros1["url_iframe"]=$_REQUEST["url_iframe"];

                // $tema = $this->oNegTema->buscar(array("idtema"=>@$_REQUEST["idtema"] ));
                // if(empty($tema)) { throw new Exception(JrTexto::_("Topic does not exists")); }
                // else { $tema = $tema[0]; }

                // $this->origen = @$tema['origen'];
                // $this->datos1 = $this->oNegPalabras->buscar($filtros1);
                // $this->esquema = 'libreria/libreria-ver-alumno-vocabulary';
            }{

            }
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            return parent::getEsquema();
                                   
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
        public function contenido()
    {
        try{
            global $aplicacion;
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('pronunciacion', '/js/');

            $filtros=array();
            if(isset($_REQUEST["idpalabra"])&&@$_REQUEST["idpalabra"]!='')$filtros["idpalabra"]=$_REQUEST["idpalabra"];
            if(isset($_REQUEST["idtema"])&&@$_REQUEST["idtema"]!='')$filtros["idtema"]=$_REQUEST["idtema"];
            if(isset($_REQUEST["palabra"])&&@$_REQUEST["palabra"]!='')$filtros["palabra"]=$_REQUEST["palabra"];
            if(isset($_REQUEST["audio"])&&@$_REQUEST["audio"]!='')$filtros["audio"]=$_REQUEST["audio"];
            if(isset($_REQUEST["idagrupacion"])&&@$_REQUEST["idagrupacion"]!='')$filtros["idagrupacion"]=$_REQUEST["idagrupacion"];
            if(isset($_REQUEST["url_iframe"])&&@$_REQUEST["url_iframe"]!='')$filtros["url_iframe"]=$_REQUEST["url_iframe"];

            $tema = $this->oNegTema->buscar(array("idtema"=>@$_REQUEST["idtema"] ));
            if(empty($tema)) { throw new Exception(JrTexto::_("Topic does not exists")); }
            else { $tema = $tema[0]; }

            // $this->breadcrumb = [
            //     /*[ 'texto'=> ucfirst(JrTexto::_('Free Lesson')), 'link'=>'/tema_libre/' ],*/
            //     [ 'texto'=> @$tema['tipo_nombre'], 'link'=>'/tema_libre/tema/?idtipo='.@$tema['idtipo'] ],
            //     [ 'texto'=> @$tema['nombre'] ],
            // ];
            switch (@$tema['tipo_contenido']) {
                case 'TABLE':
                    $this->oNegPalabras->setLimite(0,99999);
                    $this->datos1 = $this->oNegPalabras->buscarTable($filtros);
                    $this->esquema = 'libreria/libreria-ver-alumno-vocabulary';
                    // $this->esquema = 'libre_tema/alum-palabras_table';
                    break;
                case 'IFRAME':
                    $this->origen = @$tema['origen'];
                    $this->datos1 = $this->oNegPalabras->buscar($filtros);
                    $this->esquema = 'libre_tema/alum-palabras_iframe';
                    break;
                case 'HTML':
                    $this->origen = @$tema['origen'];
                    $this->datos1 = $this->oNegPalabras->buscar($filtros);
                    $this->esquema = 'libre_tema/alum-palabras_html';
                    break;
            }
            $this->documento->plantilla = 'alumno/general';
            $this->documento->setTitulo( ucfirst(JrTexto::_('Free Lesson')).' - '.@$this->datos1[0]['tema_nombre'] , true);
            return parent::getEsquema();
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function ver()
    {
        try{
            global $aplicacion; 
            /*bibliotecas JS y CSS*/
            $this->getPlugins();

            $usuarioAct = NegSesion::getUsuario();
            $this->breadcrumb=JrTexto::_('View');
            $filtroTarAsig = array();
            if($usuarioAct['idrol']=="3"){
                $this->oNegbib_estudio->id_estudio = @$_GET['id_estudio'];
                // $asignacion_alumno = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;

                // $idTarea_asignacion = $asignacion_alumno['idtarea_asignacion'];
            }
            $this->oNegbib_estudio->setLimite(0,9999);
            $material = $this->oNegbib_estudio->buscar(array('id_estudio'=>$_GET['id_estudio']));
            if(empty($material)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = $material[0];

            $this->documento->setTitulo(JrTexto::_('Library').' /'.JrTexto::_('Edit'), true);
            return $this->formver($_GET['tipo']);
            
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
    public function ver2()
    {
        try{
            global $aplicacion; 
            /*bibliotecas JS y CSS*/
            $this->getPlugins();

            $usuarioAct = NegSesion::getUsuario();
            $this->breadcrumb=JrTexto::_('View');
            $filtroTarAsig = array();
            if($usuarioAct['idrol']=="3"){
                $this->oNegTema->idtema = @$_GET['idtema'];
                // $asignacion_alumno = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;

                // $idTarea_asignacion = $asignacion_alumno['idtarea_asignacion'];
            }
            $this->oNegTema->setLimite(0,9999);
            $material = $this->oNegTema->buscar(array('idtema'=>$_GET['idtema']));
            if(empty($material)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = $material[0];

            $this->documento->setTitulo(JrTexto::_('Library').' /'.JrTexto::_('Edit'), true);
            return $this->formver($_GET['tipo']);
            
        }catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
   
    public function xGuardar()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST)){
                throw new Exception(JrTexto::_('No data to insert'));
            }
            $frm = $_POST;
            // print_r($frm);
            if(@$frm["accion"]=="Editar"){
                $this->oNegbib_estudio->id_estudio =  @$frm['pkIdestudio'];
            };

            $this->todoAudios=$this->oNegbib_estudio->buscaraudios(array('id_estudio'=>$this->oNegbib_estudio->id_estudio));
            // print_r($_POST);
           // if(!empty($_POST['opcImage'])){
           //      $file=$_POST["opcImage"];
           //      // print_r($file);
           //      $ext=strtolower(pathinfo($file, PATHINFO_EXTENSION));
           //      $nombre = explode('/', $file);
           //      $count = count($nombre);
           //      $newfoto=$nombre[$count-1];
           //      $dir_media=RUTA_BASE . 'static' . SD . 'libreria' . SD . 'image' ;
           //      if(move_uploaded_file($file,$dir_media. SD.$newfoto)) 
           //      {
           //          $this->oNegbib_estudio->set('foto',$newfoto);
           //      }                   
           //  }
            // if(!empty($_POST['opcImage'])){
            //     $file='D:'. SD .'imagenes'. SD .$_POST["opcImage"];
            //      // print_r($file);
            //     // $ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
            //     $newfoto=$_POST["opcImage"];
            //     $dir_media=RUTA_BASE . 'static' . SD . 'libreria' . SD . 'image' ;
            //     // var_dump($file);
            //     // var_dump($dir_media);
            //     // var_dump($dir_media. SD.$newfoto);
            //     if(copy($file,$dir_media.SD.$newfoto)) 
            //     {
            //         // var_dump('hola');
            //         $this->oNegbib_estudio->set('foto',$newfoto);
            //     }                   
            // }
            if ($this->todoAudios[0]['id_tipo']==4) {
                // var_dump($this->todoAudios[0]['nombre']);
                if(@$frm["accion"]=="Nuevo"){
                    $idMaterial=$this->oNegbib_estudio->agregar();
                }else{
                    $this->todoAudiosbuscados=$this->oNegbib_estudio->buscaraudios(array('nombre'=>$this->todoAudios[0]['nombre'],'id_tipo'=>$this->todoAudios[0]['id_tipo']));

                    foreach ($this->todoAudiosbuscados as $value) {
                        $valor = $value['id_estudio'];
                        $this->oNegbib_estudio->id_estudio = $valor;
                        $this->oNegbib_estudio->__set('id_detalle',@$frm["opcIdCurso"]);
                        $this->oNegbib_estudio->__set('nombre',@$frm["txtNombre"]);
                        $idMaterial=$this->oNegbib_estudio->editar();
                    }
                    
                }
            }else{
                $this->oNegbib_estudio->__set('id_detalle',@$frm["opcIdCurso"]);
                $this->oNegbib_estudio->__set('nombre',@$frm["txtNombre"]);
                if(@$frm["accion"]=="Nuevo"){
                    $idMaterial=$this->oNegbib_estudio->agregar();
                }else{
                    $idMaterial=$this->oNegbib_estudio->editar();
                }
            }
            
            $data=array('code'=>'ok','data'=>$idMaterial);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
    public function xGuardar1()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST)){
                throw new Exception(JrTexto::_('No data to insert'));
            }
            $frm = $_POST;
            // print_r($frm);
            if(@$frm["accion"]=="Editar"){
                $this->oNegTema->idtema =  @$frm['pkIdestudio'];
            };
            $this->oNegTema->__set('idcurso',@$frm["opcIdCurso"]);
            $this->oNegTema->__set('nombre',@$frm["txtNombre"]);
            if(@$frm["accion"]=="Nuevo"){
                $idMaterial=$this->oNegTema->agregar();
            }else{
                $idMaterial=$this->oNegTema->editar();
            }
            $data=array('code'=>'ok','data'=>$idMaterial);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }
  

    private function getPlugins()
    {
        try {
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('editactividad', '/js/new/');
            $this->documento->script('actividad_completar', '/js/new/');
            $this->documento->script('actividad_ordenar', '/js/new/');
            $this->documento->script('actividad_imgpuntos', '/js/new/');
            $this->documento->script('actividad_verdad_falso', '/js/new/');
            $this->documento->script('actividad_fichas', '/js/new/');
            $this->documento->script('actividad_dialogo', '/js/new/');
            $this->documento->script('manejadores_dby', '/js/new/');
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('jquery.md5', '/tema/js/');            
            $this->documento->script('manejadores_practice', '/js/new/');
            $this->documento->script('completar', '/js/new/');

            $this->documento->script('tools_games', '/js/');
            $this->documento->stylesheet('estilo', '/libs/crusigrama/');
            $this->documento->script('crossword', '/libs/crusigrama/');
            $this->documento->script('micrusigrama', '/libs/crusigrama/');
            $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
            $this->documento->script('wordfind', '/libs/sopaletras/js/');
            $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
            $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');

            #$this->documento->script('wavesurfer.min', '/libs/audiorecord/');
            #$this->documento->script('audioRecord', '/libs/audiorecord/');
            #$this->documento->script('recorderWorker', '/libs/audiorecord/');

            $this->documento->script('main', '/libs/audiorecord_wav/');
            $this->documento->script('audiodisplay', '/libs/audiorecord_wav/');
            $this->documento->script('recorder', '/libs/audiorecord_wav/js/');
            $this->documento->script('recorderWorker', '/libs/audiorecord_wav/js/');
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}