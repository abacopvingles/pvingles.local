function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}

function geturl() {
    var loc = window.location;
    var pathname = window.location.pathname;
    return loc+pathname;
}
function medidas(id){
    return {w:document.getElementById(id).offsetWidth,h:document.getElementById(id).offsetHeight}
}

function misdatosuser(){
    var datousertmp=sessionStorage.getItem('datosuser')||false;
    if(datousertmp!=false) return JSON.parse(datousertmp);
    return datousertmp;
}

function configmedios(){
    var datousertmp=sessionStorage.getItem('medios')||false;
    if(datousertmp!=false) return JSON.parse(datousertmp);
    return datousertmp;
}
var datosuser={
    userid:$('._user').text().trim(),
    roomid:$('._sala').text().trim(),
    user:$('._username').text().trim(),
    email:$('._useremail').text().trim(),
    tipo:$('._typeuser').text().trim()
}
function savedatosuser(){
    datosuser.userid=userid;
    datosuser.roomid=roomid; 
    datosuser.tipo=tipouser;
    sessionStorage.setItem('datosuser',JSON.stringify(datosuser));
}
var datotmp=misdatosuser();
var configdispo=configmedios();
var __salainiciada=false;
if(datotmp!=false){
    if(datotmp.roomid==datosuser.roomid){
        datosuser.userid=(datotmp.userid!=datosuser.userid)?datotmp.userid:datosuser.userid;       
        datosuser.tipo=(datotmp.tipo!=datosuser.tipo)?datotmp.tipo:datosuser.tipo;
        __salainiciada=true;
    }
}else sessionStorage.clear();
sessionStorage.setItem('datosuser',JSON.stringify(datosuser));  
var roomid=datosuser.roomid;
var userid=datosuser.userid;
var tipouser=datosuser.tipo;
var audioInId=configdispo.audioIn||false;
var audioOutId=configdispo.audioOut||false;
var camaraInId=configdispo.camaraIn||false;
navigator.getMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia);
var URL=window.URL;
var urlBase=getAbsolutePath();