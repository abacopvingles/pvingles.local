;
var __PDF_DOC,
    __PDFCURRENT_PAGE=0,
    __PDFTOTAL_PAGES,
    __PDFPAGE_RENDERING_IN_PROGRESS = 0,
    __CANVAS = document.getElementById('micanvaspdf'),
    __CANVAS_CTX = __CANVAS.getContext('2d');
    PDFJS.disableWorker = true;
var colorcanvas='#fff';    
var colorborde='#000000';
var colorfondo='#000000';
var acccionincanvas=false;
var canvasfabric =  new fabric.Canvas('micanvas');
var divPos = {};
$(document).ready(function(ev){
$(".istooltip").tooltip();
$(".ventanas" ).draggable({ containment: "#pantalla1",  handle: ".panel-heading" }).resizable({containment: "#pantalla1",minHeight: 120,minWidth: 50});
$(".ventanas .panel-heading").mousedown(function(){
	$('.ventanas').removeClass('active');
	$(this).closest(".ventanas").addClass('active');
});
$('header').addClass('static');
var redimencionarchat=function(){
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
    var _height=(height-60);
    var _heightmitad=_height/2;
    $('#pantalla1').css('height',_height+'px');//.css('width',(width-5)+'px');
    var heightchat =$('#vchat .chatAll').height();
    heightchat=heightchat>150?heightchat:150;
    $('#vchat .mensajes').css('height',heightchat-110+'px');    	
    var widthcanvas=$('#vpizarra .panel-body').width();
    var heightcanvas=$('#vpizarra .panel-body').height();
    __CANVAS.width=widthcanvas;
    __CANVAS.height=heightcanvas;	  
    canvasfabric.setWidth(widthcanvas);
	canvasfabric.renderAll();
}
$(window).resize(function(){
     redimencionarchat(); 
});
redimencionarchat();
var mostrarventa=function(ventana,acc){		
	var dacc=acc||false;		
	if(acc==true){
		$('.ventanas').removeClass('active');
		$(ventana).addClass('active').show(300,function(){
			if(ventana==='#vpizarra'){
				var widthcanvas=$('#vpizarra .panel-body').width()+1;
			    var heightcanvas=$('#vpizarra .panel-body').height()+1;
			    __CANVAS.width=widthcanvas;
			    __CANVAS.height=heightcanvas;	  
			    canvasfabric.setWidth(widthcanvas);
		    	canvasfabric.setHeight(heightcanvas);
		    	canvasfabric.renderAll();
		    	redimencionarchat();
	    	}				
		});
	}else{
		$(ventana).hide(300).removeClass('active');
	}
}
$('#pantalla1').on('click','.clickclose',function(){
	var vid=$(this).closest('.ventanas').attr('id');
	$('#'+vid).hide(100);
	$('#menupersonalizado .showwindow[data-ventana="'+vid+'"]').removeClass('active');
	 screenfull.exit();		
	if(vid==='vvideos'){
		$('#'+vid).find('video').trigger('pause');
	}
}).on('click','.clickable',function(){
	var vid=$(this).closest('.ventanas').attr('id');
	var i=$('i',this);
	var vidcur=$('#'+vid);
	var hayfooter=vidcur.find('.panel-footer').length;
	if(i.hasClass('glyphicon-chevron-up')){
		if(hayfooter>0)vidcur.find('.panel-footer').hide(0);
		vidcur.attr('data-height',vidcur.height()).css('height','auto');	
	}else{
		if(hayfooter>0)vidcur.find('.panel-footer').show(0);
		vidcur.css('height',vidcur.attr('data-height')+'px').removeAttr('data-height');
	}		
}).on('click','.clickmaximizable',function(ev){
	ev.preventDefault();
	var vid=$(this).closest('.ventanas').attr('id');
	var i=$('i',this);
	var vidcur=$('#'+vid);		
	if(i.hasClass('fa-window-maximize')){
		i.removeClass('fa-window-maximize').addClass('fa-window-restore');			
		$('#menupersonalizado .showwindow.active').each(function(){
			var idv=$(this).attr('data-ventana');
			var _idv=$('#'+idv);
			_idv.attr('data-zindex',_idv.css('z-index'));
			if(idv==vid){
				_idv.attr('data-style',vidcur.attr('style')).attr('style','display:block; top:0px; right:0px; left:0px; bottom:0px; width:100%; height:99% !important; z-index:92;').css('top','0px');
				//screenfull.toggle(_idv[0]);
			}
		});			
	}else{
		i.removeClass('fa-window-restore').addClass('fa-window-maximize');
		$('#menupersonalizado .showwindow.active').each(function(){
			var idv=$(this).attr('data-ventana');
			var _idv=$('#'+idv);
			_idv.css('z-index',_idv.attr('data-zindex')).removeAttr('data-zindex');
			if(idv==vid){
				_idv.removeAttr('style').attr('style',vidcur.attr('data-style')).removeAttr('data-style');
			}
		});
	}
	redimencionarchat();
}).on('click','.btnfullscreen',function(){
	screenfull.toggle($(this).closest('.panel.ventanas')[0]);
	redimencionarchat();
});
$('#menupersonalizado').on("click",".resetwindow",function(){
		$('.ventanas').removeAttr('style');
		$('#menupersonalizado .showwindow').removeClass('active');
		$('#menupersonalizado .showwindow.init').addClass('active');
		$('.clickmaximizable').find('i').removeAttr('class').addClass('fa fa-window-maximize');
		/*$('#menupersonalizado .showwindow.active').each(function(){
			var idv=$(this).attr('data-ventana');
			$('#'+idv).attr('style',$('#'+idv).attr('data-style'));
		});*/
	}).on("click",".showwindow",function(){
		var obj=$(this);
		var v1=$(this).attr('data-ventana');
		obj.toggleClass('active');		
		if(obj.hasClass('active'))
			mostrarventa('#'+v1,true);
		else 
			mostrarventa('#'+v1,false);
	});	

	var mostrarEditorMCE  = function(obj,showtoolstiny){
	  var showtools=showtoolstiny||'';
	  tinymce.init({
	  	theme_advanced_resizing : "true",
		theme_advanced_resize_horizontal : "true",
	    relative_urls : false,
	    remove_script_host: false,
	    convert_newlines_to_brs : true,
	    menubar: false,
	    statusbar: false,
	    verify_html : false,
	    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
	    selector: obj,
	    height: 200,
	    paste_auto_cleanup_on_paste : true,
	    paste_preprocess : function(pl, o) {
	        var html='<div>'+o.content+'</div>';
	        var txt =$(html).text(); 
	        o.content = txt;
	    },paste_postprocess : function(pl, o) {       
	        o.node.innerHTML = o.node.innerHTML;
	    },
	    plugins:[showtools+"  link image textcolor paste" ],  //chingosave chingoinput chingoimage chingoaudio chingovideo styleselect
	    toolbar: ' undo redo |  bold italic underline | alignleft aligncenter alignright alignjustify | numlist |  forecolor backcolor |  '+showtools // chingosave chingoinput chingoimage chingoaudio chingovideo 
	  });
	};
	mostrarEditorMCE('#vtxtnotes');	
	/*var init=function(){
		resetprivilegios();
	}*/
	/*init();*/
	//$(".userchatsms").select2();
		
	$('#vencuesta').on('click','.encuesta_removealternativa',function(){
		$(this).closest('tr').remove();
	}).on('click','#encuesta_addalternativa',function(){
		var newalt=$('#vencuesta #encuesta_clonealternativa').clone(true);
		newalt.removeClass('no').addClass('si').removeAttr('style').removeAttr('id');
		newalt.find('input').val('');
		$('#vencuesta table ').append(newalt);
	});
});
function subirfile(file,callback){
     var formData = new FormData();
        formData.append("aula",$('#infousersala ._salaid').text());
        formData.append("filearchivo",file);
        $.ajax({
          url: _sysUrlBase_ +'/aulavirtual/cargarmedia',
          type: "POST",
          data:  formData,
          contentType: false,
          processData: false,
          dataType :'json',
          cache: false,
          processData:false,
          xhr:function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
                    $('#divprecargafile .progress-bar').width(percentComplete+'%');
                    $('#divprecargafile .progress-bar span').text(percentComplete+'%');
                  }
                }, false);
                xhr.addEventListener("progress", function(evt){
                  if (evt.lengthComputable) {
                    var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                  }
                }, false);
                return xhr;
          },
          beforeSend: function(XMLHttpRequest){
                div=$('#divprecargafile');
                $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                $('#divprecargafile .progress-bar').width('0%');
                $('#divprecargafile .progress-bar span').text('0%'); 
                $('#divprecargafile').show('fast'); 
          },      
          success: function(data)
          { 
            if(data.code==='ok'){
              $('#divprecargafile .progress-bar').width('100%');
              $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
              $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');           
              $('#divprecargafile').hide('fast'); 
              if(callback) callback(data);  
              
            }else{
                $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');           
                return false;
            }
          },
          error: function(e) 
          {
              $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
              $('#divprecargafile .progress-bar').html('Error <span>-1%</span>');           
              return false;
          },
          complete: function(xhr){
             $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
             $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
          }
        });
}