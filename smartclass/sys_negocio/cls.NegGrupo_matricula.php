<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		21-04-2017
 * @copyright	Copyright (C) 21-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatGrupo_matricula', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegGrupo_matricula 
{
	protected $idgrupo_matricula;
	protected $idalumno;
	protected $idgrupo;
	protected $fechamatricula;
	protected $estado;
	protected $regusuario;
	protected $regfecha;
	protected $subgrupos;
	
	protected $dataGrupo_matricula;
	protected $oDatGrupo_matricula;	

	public function __construct()
	{
		$this->oDatGrupo_matricula = new DatGrupo_matricula;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatGrupo_matricula->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatGrupo_matricula->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatGrupo_matricula->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function buscarAlumGrup($filtros = array())
	{
		try {
			return $this->oDatGrupo_matricula->buscarAlumGrup($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatGrupo_matricula->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatGrupo_matricula->get($this->idgrupo_matricula);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('grupo_matricula', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatGrupo_matricula->iniciarTransaccion('neg_i_Grupo_matricula');
			$this->idgrupo_matricula = $this->oDatGrupo_matricula->insertar($this->idalumno,$this->idgrupo,$this->fechamatricula,$this->estado,$this->regusuario,$this->regfecha,$this->subgrupos);
			//$this->oDatGrupo_matricula->terminarTransaccion('neg_i_Grupo_matricula');	
			return $this->idgrupo_matricula;
		} catch(Exception $e) {	
		   //$this->oDatGrupo_matricula->cancelarTransaccion('neg_i_Grupo_matricula');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('grupo_matricula', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatGrupo_matricula->actualizar($this->idgrupo_matricula,$this->idalumno,$this->idgrupo,$this->fechamatricula,$this->estado,$this->regusuario,$this->regfecha,$this->subgrupos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Grupo_matricula', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatGrupo_matricula->eliminar($this->idgrupo_matricula);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgrupo_matricula($pk){
		try {
			$this->dataGrupo_matricula = $this->oDatGrupo_matricula->get($pk);
			if(empty($this->dataGrupo_matricula)) {
				throw new Exception(JrTexto::_("Grupo_matricula").' '.JrTexto::_("not registered"));
			}
			$this->idgrupo_matricula = $this->dataGrupo_matricula["idgrupo_matricula"];
			$this->idalumno = $this->dataGrupo_matricula["idalumno"];
			$this->idgrupo = $this->dataGrupo_matricula["idgrupo"];
			$this->fechamatricula = $this->dataGrupo_matricula["fechamatricula"];
			$this->estado = $this->dataGrupo_matricula["estado"];
			$this->regusuario = $this->dataGrupo_matricula["regusuario"];
			$this->regfecha = $this->dataGrupo_matricula["regfecha"];
			$this->subgrupos = $this->dataGrupo_matricula["subgrupos"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('grupo_matricula', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataGrupo_matricula = $this->oDatGrupo_matricula->get($pk);
			if(empty($this->dataGrupo_matricula)) {
				throw new Exception(JrTexto::_("Grupo_matricula").' '.JrTexto::_("not registered"));
			}

			return $this->oDatGrupo_matricula->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdalumno($idalumno)
	{
		try {
			$this->idalumno= NegTools::validar('todo', $idalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdgrupo($idgrupo)
	{
		try {
			$this->idgrupo= NegTools::validar('todo', $idgrupo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechamatricula($fechamatricula)
	{
		try {
			$this->fechamatricula= NegTools::validar('todo', $fechamatricula, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRegusuario($regusuario)
	{
		try {
			$this->regusuario= NegTools::validar('todo', $regusuario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setRegfecha($regfecha)
	{
		try {
			$this->regfecha= NegTools::validar('todo', $regfecha, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setSubgrupos($subgrupos)
	{
		try {
			$this->subgrupos= NegTools::validar('todo', $subgrupos, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		

	/****************************** FK alumno ***************************/
	/*public function listarno(){
		try {
			return $this->oDatGrupo_matricula->listarno();			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listarXidalumno(){
		try {
			return $this->oDatGrupo_matricula->listarXidalumno($this->dni);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/
	

	/****************************** FK grupos ***************************/
	/*public function listaros(){
		try {
			return $this->oDatGrupo_matricula->listaros();			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listarXidgrupo(){
		try {
			return $this->oDatGrupo_matricula->listarXidgrupo($this->idgrupo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}*/
	
}