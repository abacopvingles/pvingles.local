<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		23-12-2016
 * @copyright	Copyright (C) 23-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMenu', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegMenu 
{
	protected $idmenu;
	protected $nombre;
	
	protected $dataMenu;
	protected $oDatMenu;	

	public function __construct()
	{
		$this->oDatMenu = new DatMenu;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMenu->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMenu->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMenu->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatMenu->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatMenu->get($this->idmenu);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('menu', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatMenu->iniciarTransaccion('neg_i_Menu');
			$this->idmenu = $this->oDatMenu->insertar($this->nombre);
			//$this->oDatMenu->terminarTransaccion('neg_i_Menu');	
			return $this->idmenu;
		} catch(Exception $e) {	
		   //$this->oDatMenu->cancelarTransaccion('neg_i_Menu');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('menu', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatMenu->actualizar($this->idmenu,$this->nombre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Menu', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatMenu->eliminar($this->idmenu);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdmenu($pk){
		try {
			$this->dataMenu = $this->oDatMenu->get($pk);
			if(empty($this->dataMenu)) {
				throw new Exception(JrTexto::_("Menu").' '.JrTexto::_("not registered"));
			}
			$this->idmenu = $this->dataMenu["idmenu"];
			$this->nombre = $this->dataMenu["nombre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('menu', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataMenu = $this->oDatMenu->get($pk);
			if(empty($this->dataMenu)) {
				throw new Exception(JrTexto::_("Menu").' '.JrTexto::_("not registered"));
			}
			return $this->oDatMenu->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}