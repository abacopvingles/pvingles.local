<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		13-02-2017
 * @copyright	Copyright (C) 13-02-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatDiccionario', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegDiccionario 
{
	protected $id;
	protected $palabra;
	protected $definicion;
	
	protected $dataDiccionario;
	protected $oDatDiccionario;	

	public function __construct()
	{
		$this->oDatDiccionario = new DatDiccionario;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatDiccionario->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatDiccionario->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,10);
			return $this->oDatDiccionario->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatDiccionario->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatDiccionario->get($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('diccionario', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatDiccionario->iniciarTransaccion('neg_i_Diccionario');
			$this->id = $this->oDatDiccionario->insertar($this->palabra,$this->definicion);
			//$this->oDatDiccionario->terminarTransaccion('neg_i_Diccionario');	
			return $this->id;
		} catch(Exception $e) {	
		   //$this->oDatDiccionario->cancelarTransaccion('neg_i_Diccionario');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('diccionario', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatDiccionario->actualizar($this->id,$this->palabra,$this->definicion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Diccionario', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatDiccionario->eliminar($this->id);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId($pk){
		try {
			$this->dataDiccionario = $this->oDatDiccionario->get($pk);
			if(empty($this->dataDiccionario)) {
				throw new Exception(JrTexto::_("Diccionario").' '.JrTexto::_("not registered"));
			}
			$this->id = $this->dataDiccionario["id"];
			$this->palabra = $this->dataDiccionario["palabra"];
			$this->definicion = $this->dataDiccionario["definicion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('diccionario', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataDiccionario = $this->oDatDiccionario->get($pk);
			if(empty($this->dataDiccionario)) {
				throw new Exception(JrTexto::_("Diccionario").' '.JrTexto::_("not registered"));
			}

			return $this->oDatDiccionario->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setPalabra($palabra)
	{
		try {
			$this->palabra= NegTools::validar('todo', $palabra, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setDefinicion($definicion)
	{
		try {
			$this->definicion= NegTools::validar('todo', $definicion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}