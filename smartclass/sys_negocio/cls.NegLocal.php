<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-01-2017
 * @copyright	Copyright (C) 11-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatLocal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegLocal 
{
	protected $idlocal;
	protected $nombre;
	protected $direccion;
	protected $id_ubigeo;
	protected $tipo;
	protected $vacantes;
	protected $idugel;
	
	protected $dataLocal;
	protected $oDatLocal;	

	public function __construct()
	{
		$this->oDatLocal = new DatLocal;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatLocal->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatLocal->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatLocal->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatLocal->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatLocal->get($this->idlocal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('local', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatLocal->iniciarTransaccion('neg_i_Local');
			$this->idlocal = $this->oDatLocal->insertar($this->nombre,$this->direccion,$this->id_ubigeo,$this->tipo,$this->vacantes,$this->idugel);
			//$this->oDatLocal->terminarTransaccion('neg_i_Local');	
			return $this->idlocal;
		} catch(Exception $e) {	
		   //$this->oDatLocal->cancelarTransaccion('neg_i_Local');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('local', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatLocal->actualizar($this->idlocal,$this->nombre,$this->direccion,$this->id_ubigeo,$this->tipo,$this->vacantes,$this->idugel);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Local', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatLocal->eliminar($this->idlocal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdlocal($pk){
		try {
			$this->dataLocal = $this->oDatLocal->get($pk);
			if(empty($this->dataLocal)) {
				throw new Exception(JrTexto::_("Local").' '.JrTexto::_("not registered"));
			}
			$this->idlocal = $this->dataLocal["idlocal"];
			$this->nombre = $this->dataLocal["nombre"];
			$this->direccion = $this->dataLocal["direccion"];
			$this->id_ubigeo = $this->dataLocal["id_ubigeo"];
			$this->tipo = $this->dataLocal["tipo"];
			$this->vacantes = $this->dataLocal["vacantes"];
			$this->idugel = $this->dataLocal["idugel"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('local', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataLocal = $this->oDatLocal->get($pk);
			if(empty($this->dataLocal)) {
				throw new Exception(JrTexto::_("Local").' '.JrTexto::_("not registered"));
			}

			return $this->oDatLocal->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setNombre($nombre)
	{
		try {
			$this->nombre= NegTools::validar('todo', $nombre, false, JrTexto::_("Please enter a valid value"), array("longmax" => 500));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setDireccion($direccion)
	{
		try {
			$this->direccion= NegTools::validar('todo', $direccion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 500));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setId_ubigeo($id_ubigeo)
	{
		try {
			$this->id_ubigeo= NegTools::validar('todo', $id_ubigeo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 6));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setTipo($tipo)
	{
		try {
			$this->tipo= NegTools::validar('todo', $tipo, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setVacantes($vacantes)
	{
		try {
			$this->vacantes= NegTools::validar('todo', $vacantes, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdugel($idugel)
	{
		try {
			$this->idugel= NegTools::validar('todo', $idugel, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}