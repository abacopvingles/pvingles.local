<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		28-08-2016
 * @copyright	Copyright (C) 28-08-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatRol', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegRol
{
	protected $rol;
	protected $flag_estado;	
	protected $oDatRol;
	protected $dataRol;	
	public function __construct()
	{
		$this->oDatRol = new DatRol;
	}	
	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if(method_exists($this, $metodo)){
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}	
	public function __set($prop, $valor)
	{
		$this->set__($prop, $valor);
	}	
	private function set__($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}	
	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set__($prop_, $valor);
			}
		}		
		$this->set__($prop, $valor);
	}	
	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatRol->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	////////// Fin - Metodos magicos //////////	
	public function getNumRegistros($flag_estado = null)
	{
		try {
			return $this->oDatRol->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($flag_estado = null)
	{
		try {
			return $this->oDatRol->buscar($flag_estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	
	public function listar_privilegios($rol, $recurso)
	{
		try {
			return $this->oDatRol->listar_privilegios($rol, $recurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	
	public function agregar($rol, $flag=1)
	{
		try {
			$rol = NegTools::validar('todo', $rol, true, JrTexto::_('Invalid role name'), array('longmax' => 30));
			$rol = NegTools::getCadUrl(strtolower($rol));
			return $this->oDatRol->insertar($rol, $flag);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function editar($pk, $rol, $flag=1)
	{
		try {
			$rol = NegTools::validar('todo', $rol, true, JrTexto::_('Invalid role name'), array('longmax' => 30));
			return $this->oDatRol->actualizar($pk,$rol, $flag);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	


	public function asignar_privilegios($rol, $privilegios)
	{
		try {
			if(!NegSesion::tiene_acceso('permiso-sistema', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			$rol_ = $this->oDatRol->get($rol);
			if(empty($rol_)) {
				throw new Exception(JrTexto::_('Select a role'));
			}			
			$this->oDatRol->limpiar_privilegios($rol);			
			if(!empty($privilegios) && is_array($privilegios)) {
				foreach($privilegios as $privilegio) {
					$priv = explode(':', $privilegio);
					$this->oDatRol->insertar_privilegio($rol, $priv[0], $priv[1]);
				}
			}
			
			return true;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setRol($pk){
		try {
			$this->dataRol= $this->oDatRol->get($pk);
			if(empty($this->dataRol)) {
				throw new Exception(JrTexto::_("role").' '.JrTexto::_("not registered"));
			}
			$this->rol = $this->dataRol["rol"];
			$this->flag_estado = $this->dataRol["flag_activo"];
			
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar($rol)
	{
		try {
			return $this->oDatRol->eliminar($rol);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('rol', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataRol= $this->oDatRol->get($pk);
			if(empty($this->dataRol)) {
				throw new Exception(JrTexto::_("role").' '.JrTexto::_("not registered"));
			}

			return $this->oDatRol->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
}