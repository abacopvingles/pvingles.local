<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-04-2017
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividad_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatActividad_detalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatActividades', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegActividad_alumno
{

	protected $idactalumno;
	protected $iddetalleactividad;
	protected $idalumno;
	protected $fecha;
	protected $porcentajeprogreso;
	protected $habilidades;
	protected $estado;

	protected $dataActividad_alumno;
	protected $oDatActividad_alumno;
 	protected $oDatActividad_detalle;
 	protected $oDatActividades;
	protected $oDatNiveles;

	public function __construct()
	{
		$this->oDatActividad_alumno = new DatActividad_alumno;
 		$this->oDatActividad_detalle = new DatActividad_detalle;
 		$this->oDatActividades = new DatActividades;
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatActividad_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////
	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividad_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatActividad_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatActividad_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividad_alumno->get($this->idactalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function agregar()
	{
		try {
			//if(!NegSesion::tiene_acceso('actividad_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			//$this->oDatActividad_alumno->iniciarTransaccion('neg_i_Actividad_alumno');
			$this->idactalumno = $this->oDatActividad_alumno->insertar($this->iddetalleactividad,$this->idalumno,$this->fecha,$this->porcentajeprogreso,$this->habilidades,$this->estado);
			//$this->oDatActividad_alumno->terminarTransaccion('neg_i_Actividad_alumno');
			return $this->idactalumno;
		} catch(Exception $e) {
		   //$this->oDatActividad_alumno->cancelarTransaccion('neg_i_Actividad_alumno');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('actividad_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatActividad_alumno->actualizar($this->idactalumno,$this->iddetalleactividad,$this->idalumno,$this->fecha,$this->porcentajeprogreso,$this->habilidades,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatActividad_alumno->eliminar($this->idactalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('actividad_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataActividad_alumno = $this->oDatActividad_alumno->get($pk);
			if(empty($this->dataActividad_alumno)) {
				throw new Exception(JrTexto::_("Actividad_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatActividad_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function ultimaActividadxAlum($idalumno){
		try {
			$ultima_activ = $this->oDatActividad_alumno->ultimaActividad($idalumno);
			if(empty($ultima_activ)) return null;
            $det_act=$this->oDatActividad_detalle->get($ultima_activ['iddetalleactividad']);
            $activ=$this->oDatActividades->get($det_act['idactividad']);

            $filtroN=array('idnivel'=>$activ['nivel']);
            $filtroU=array('idnivel'=>$activ['unidad']);
            $filtroS=array('idnivel'=>$activ['sesion']);
            $nivel  = $this->oDatNiveles->buscar($filtroN);
            $unidad = $this->oDatNiveles->buscar($filtroU);
            $sesion = $this->oDatNiveles->buscar($filtroS);

			return array('nivel'=>$nivel[0], 'unidad'=>$unidad[0], 'sesion'=>$sesion[0]);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
/*
	public function set($pk){
		try {
			$this->dataActividad_alumno = $this->oDatActividad_alumno->get($pk);
			if(empty($this->dataActividad_alumno)) {
				throw new Exception(JrTexto::_("Actividad_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idactalumno = $this->dataActividad_alumno["idactalumno"];
			$this->iddetalleactividad = $this->dataActividad_alumno["iddetalleactividad"];
			$this->idalumno = $this->dataActividad_alumno["idalumno"];
			$this->fecha = $this->dataActividad_alumno["fecha"];
			$this->porcentajeprogreso = $this->dataActividad_alumno["porcentajeprogreso"];
			$this->estado = $this->dataActividad_alumno["estado"];
						//falta campos
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function setIdactalumno($idactalumno)
	{
		try {
			$this->idactalumno= NegTools::validar('todo', $idactalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setIddetalleactividad($iddetalleactividad)
	{
		try {
			$this->iddetalleactividad= NegTools::validar('todo', $iddetalleactividad, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setIdalumno($idalumno)
	{
		try {
			$this->idalumno= NegTools::validar('todo', $idalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setFecha($fecha)
	{
		try {
			$this->fecha= NegTools::validar('todo', $fecha, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setPorcentajeprogreso($porcentajeprogreso)
	{
		try {
			$this->porcentajeprogreso= NegTools::validar('todo', $porcentajeprogreso, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
*/
}
