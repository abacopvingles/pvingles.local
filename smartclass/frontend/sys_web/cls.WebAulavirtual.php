<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016 
 * @copyright	Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegPersonal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulasvirtuales', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');
class WebAulavirtual extends JrWeb
{
	private $oNegAlumno;
	private $oNegPersonal;
	private $oNegAulasvirtuales;
	protected $oNegAulavirtualinvitados;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegPersonal = new NegPersonal;
		$this->oNegAlumno=new NegAlumno;
		$this->oNegAulasvirtuales = new NegAulasvirtuales;
		$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
	}
	public function defecto(){
		return $this->ver();
	}

	public function ver()
	{
		try{
			global $aplicacion;	
            $filtros=array();
            @extract($_REQUEST);
            if(empty($_REQUEST)){
            	$aplicacion->redir();
            }
            $filtros["aulaid"]=@$id;
            $this->aula=$this->oNegAulasvirtuales->buscar($filtros); 
            $this->userssmart=@$user;
            $this->emailsmart=@$email;
            $this->tiposmart=@$tipo;
            if(empty($user)||empty($email)||empty($tipo)){
            	throw new Exception('Smartclass '.JrTexto::_('data incorrect').'!!');
            	exit();
            }
            if(empty($this->aula)){
            	throw new Exception('Smartclass '.JrTexto::_('empty').'!!');
            	exit();
            }
            $aula=$this->aula=$this->aula[0];
            if(NegSesion::existeSesion()){
	            $this->usuario = NegSesion::getUsuario();
            }

            $fini=new DateTime($aula["fecha_inicio"]);
			$fini->modify('-30 minutes');
			$ffin=new DateTime($aula["fecha_final"]);
			$fnow=new DateTime("now");
			$msj='';
			$errorlogin=false;
			if($aula["estado"]=='CL'){
				throw new Exception('Smartclass '.JrTexto::_('closed').'!!');
				exit();
			}
			if($aula["estado"]=='BL'){
				throw new Exception('Smartclass '.JrTexto::_('blocked, no more participants').'!!');
				exit();
			}
			if($aula["estado"]=='0'&&$this->tiposmart=='M'){
				$this->oNegAulasvirtuales->setCampo($aula["aulaid"], 'estado', 'AB');
			}
			if($fini>=$fnow&&$fnow<$ffin){
				throw new Exception('Smartclass '.JrTexto::_('not yet active for user connection').'!!');
				exit();
			}else if($fnow>$ffin){
				throw new Exception('Smartclass '.JrTexto::_('expired').'!!');
				exit();
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
			$this->documento->stylesheet('emoji', '/libs/emoji/');
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            $this->documento->script('screenfull.min', '/libs/screenfull/');
            $this->documento->script('chartist.min', '/libs/chartist/');
            $this->documento->stylesheet('chartist.min', '/libs/chartist/');
            $this->documento->script('html2canvas.min', '/libs/html2canvas/');
        	$this->documento->script('pdf', '/libs/pizarra/');
        	$this->documento->script('pdf.worker', '/libs/pizarra/');
        	$this->documento->script('getScreenId', '/libs/pizarra/');
        	$this->documento->script('getStats', '/libs/aulavirtual/');
        	$this->documento->script('RTCMultiConnection.min', '/libs/pizarra/');
        	$this->documento->script('MultiStreamsMixer', '/libs/pizarra/');
        	$this->documento->script('adapter', '/libs/pizarra/');
        	$this->documento->script('socket.io', '/libs/pizarra/');
        	$this->documento->script('screenshot', '/libs/pizarra/');
        	$this->documento->script('RecordRTC', '/libs/pizarra/');        	
            $this->documento->script('emitpdf', '/libs/pizarra/');
            //$this->documento->script('generales', '/libs/aulavirtual/');
            $this->documento->script(null, URL_BASE.'/static/libs/aulavirtual/aulavirtual.js?data='.date('dHis'));
            $this->documento->script('pizarra', '/libs/pizarra/');
			$this->documento->plantilla ='aulavirtual/trasmitir';
			$this->documento->setTitulo('Smartclass'.'- (Aula Virtual)');
			$this->esquema = 'aulavirtual/ver';
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			//$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->usuarioAct = NegSesion::getUsuario();
			$this->personal=$this->oNegPersonal->buscar(array('dni'=>$this->usuarioAct["dni"]));
			$this->documento->setTitulo(JrTexto::_('Smartclass'));
			
			$idnivel_=!empty($this->datos["idnivel"])?$this->datos["idnivel"]:0;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
	        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	        $_idunidad=!empty($this->datos["idunidad"])?$this->datos["idunidad"]:0;
	        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
	        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
	        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

	        $_idactividad=!empty($this->datos["idactividad"])?$this->datos["idactividad"]:0;
	        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
	       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
	       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
	       	
			$usuarioAct = NegSesion::getUsuario();
            $filtros = array();
            $filtros['iddocente'] = $usuarioAct["dni"];
            $this->locales=array();
            $arrLocales = array();
	       	$this->grupos=$this->oNegGrupos->buscar($filtros);
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $this->oNegLocal->idlocal = $g['idlocal'];
                    $this->locales[]=$this->oNegLocal->getXid();
                    $arrLocales[]=$g['idlocal'];
                }
            }
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'aulavirtual/formulario';
			$this->documento->plantilla ='aulavirtual/inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function requisitos(){
		try {
			global $aplicacion;
			$this->usuarioAct=null;
			$filtros=array();
			if(NegSesion::existeSesion()){
				$this->usuarioAct = NegSesion::getUsuario();
			}
			if(empty($_REQUEST["id"])){
				throw new Exception('Smartclass '. JrTexto::_('does not exist').'!!');
			}
            $filtros["aulaid"]=@$_REQUEST["id"];
            $this->aula=$this->oNegAulasvirtuales->buscar($filtros);
            if(empty($this->aula)) throw new Exception('Smartclass '. JrTexto::_('does not exist').'!!');
			$this->documento->setTitulo(JrTexto::_('Smartclass').'- Clases virtuales');
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'aulavirtual/general';
			$this->esquema = 'aulavirtual/requisitos';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function json_verificarusuario(){
		try {
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			if(empty($_REQUEST)){
				echo json_encode(array('code'=>'Error','msj'=>'Smartclass '.JrTexto::_('No data to send')));
				exit();
			}
			@extract($_REQUEST);
			if(!empty($usuario))
			$usuario=json_decode($usuario)[0];	
			$email=$usuario->email;
			$user=$usuario->usuario;
			$idaula=intval($idaula);
			if(empty($idaula)){
				echo json_encode(array('code'=>'Error','msj'=>'Smartclass '. JrTexto::_("empty")));				
				exit();
			}
			$usuarioAct=null;
			$filtros["aulaid"]=$idaula;
            $aula=$this->oNegAulasvirtuales->buscar($filtros);
            if(empty($aula[0])){
            	echo json_encode(array('code'=>'Error','msj'=>'Smartclass '.JrTexto::_('data incorrect')));
				exit();
            }
            $aula=$aula[0];
            $dni=$aula["dni"];
            $estado=$aula["estado"];
            $dirigidoa=$aula["dirigidoa"];
            $moderadorini=$aula["moderadores"];
            if(NegSesion::existeSesion()){
				$usuarioAct = NegSesion::getUsuario();
				$dniuser=@$usuarioAct["dni"];
				$emailuser=@$usuarioAct["email"];
				if($dniuser==$dni){
					echo json_encode(array('code'=>'ok','msj'=>array('user'=>'miaula','estado'=>$estado,'como'=>'M')));
					exit();
				}
				if($moderadorini==$emailuser){
					echo json_encode(array('code'=>'ok','msj'=>array('user'=>'miaula','estado'=>$estado,'como'=>'M')));
					exit();
				}
				$useraula=$this->oNegAulavirtualinvitados->buscar(array("idaula"=>$idaula,'email'=>$emailuser));
				if(!empty($useraula[0])){
					echo json_encode(array('code'=>'ok','msj'=>array('user'=>'miaula','estado'=>$estado,'como'=>$useraula["como"])));
					exit();	
				}
			}

			
			$useraula=$this->oNegAulavirtualinvitados->buscar(array("idaula"=>$idaula,'email'=>$email));
			if(!empty($useraula[0])){
				$useraula=$useraula[0];
				echo json_encode(array('code'=>'ok','msj'=>array('user'=>$user,'estado'=>$estado,'como'=>$useraula["como"])));
				exit();	
			}
			if($dirigidoa=='P'){
				echo json_encode(array('code'=>'ok','msj'=>array('user'=>$user,'estado'=>$estado,'como'=>'U')));
				exit();	
			}
			echo json_encode(array('code'=>'error','msj'=>'Smartclass '.JrTexto::_('user email has not been invited')));
			exit();	
            return parent::getEsquema();
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
			exit();			
		}
	}

	public function cargarmedia(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incorrect')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$tipo="aulasvirtuales";			
			try{
				$newname=@$_POST["aula"].date("Ymdhis")."_".$file["name"];
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File upload success'),'namefile'=>$newname,'extension'=>$ext));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error loading file')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}

	public function subirmediamedia(){
		try {			
			$this->documento->plantilla = 'returnjson';
			if(empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incorrect'),'aa'=>$_FILES["filearchivo"]));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$ext=strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));
			$tipo="aulasvirtuales";			
			try{
				$newname=@$_POST["aula"]."_".date('Ymdhis').$ext;
				$dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo ;
				@mkdir($dir_media = RUTA_BASE . 'static' . SD . 'media' . SD . $tipo,'0777');
				if(move_uploaded_file($file["tmp_name"],$dir_media. SD.$newname)) 
			  	{
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File upload success'),'namefile'=>$newname,'extension'=>$ext));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error loading file')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}
	}

	public function addcampo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST); 
            if(!empty($id)&&!empty($campo)) {
				$res=$this->oNegAulasvirtuales->setCampo($id, $campo, $valor);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
				exit();			
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error inesperado')));           	
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }


	public function subirblob(){
		try {
			global $aplicacion;			
			$this->documento->plantilla = 'returnjson';
			if(empty($_POST["type"])||empty($_FILES["filearchivo"])){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
				exit();
			}
			$file=$_FILES["filearchivo"];
			$type=$_POST["type"];
			$intipo='';
			$tipe_=@explode('/',$type);
	        $tipo=$tipe_[0];
	        $ext=$tipe_[1];
			if($tipo=='image'){
				$intipo=array('jpg','gif','png');
			}elseif($tipo=='audio'){
				$intipo=array('mp3','wav');
			}
			$name=!empty($_POST["name"])?$_POST["name"]:date("Ymdhis");
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
			try{			
				$newname="_".$name.".".$ext;	
				$rutatmp=SD.'media'.SD."aulasvirtuales".SD."grabaciones".SD.$tipo;
				$dir_media = RUTA_BASE.'static'.$rutatmp;
				$linkbase=$dir_media. SD.$newname;
				if(is_file($linkbase))@unlink($linkbase);
				@mkdir($dir_media = RUTA_BASE . 'static' . $rutatmp,'0777');				
				if(move_uploaded_file($file["tmp_name"],$linkbase)) 
			  	{			   			
			   		$namelink='static/media/record/'.$tipo."/".$newname;		
			   		echo json_encode(array('code'=>'ok','msj'=>JrTexto::_('File Upload success'),'namelink'=>$namelink,'nombre'=>$name));
					exit(0);
			  	}
			}catch(Exception $e) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error upload File')));
			}
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error in function')));
		}

	}







}