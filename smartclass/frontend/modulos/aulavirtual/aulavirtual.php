<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
JrCargador::clase('modulos::aulavirtual::Aulavirtual', RUTA_SITIO, 'modulos::aulavirtual');
$oMod = new Aulavirtual;
echo $oMod->mostrar(@$atributos['posicion']);