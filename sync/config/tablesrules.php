<?php
// return array(
//     'personal' => array(
//         'idUnicos' => array(
//             'dni','usuario','idlocal'
//         ),
//         'relacion' => false
//         //'relacion' => array('persona_rol')
//     ),
//     'persona_rol' => array(
//         'idUnicos' => array(
//             'idpersonal','idempresa','idproyecto'
//         ),
//         'relacion' => false
//     ),
//     'acad_grupoauladetalle'=> array(
//         'idUnicos' => array(
//             'idgrupoaula','idcurso','idlocal'
//         ),
//         'relacion' => false
//     ),
//     'acad_matricula' => array(
//         'idUnicos' => array(
//             'idgrupoauladetalle','idalumno','fecha_registro'
//         ),
//         'relacion' => false
//     ),
//     'notas_quiz' => array(
//         'idUnicos' => array(
//             'idrecurso','idalumno','tipo'
//         ),
//         'relacion' => false
//     ),
//     'bitacora_alumno_smartbook' => array(
//         'idUnicos' => array(
//             'idcurso','idsesion','idusuario'
//         ),
//         'relacion' => array('bitacora_smartbook'),
//         'miId' => 'idbitacora_smartbook'
//     ),
//     'bitacora_smartbook' => array(
//         'idUnicos' => array(
//             'idcurso','idsesion','idsesionB','idusuario','pestania'
//         ),
//         'relacion' => false,
//         'idesRel' => array('bitacora_alumno_smartbook'=> array('idcurso'=>'idcurso','idsesion'=>'idsesionB','idusuario'=>'idusuario'))
//         'id' =>array('bitacora_alumno_smartbook'=>'idbitacora_alum_smartbook'),

//     ),
//     'actividad_alumno' => array(
//         'idUnicos' => array(
//             'iddetalleactividad','idalumno','fecha','habilidades'
//         ),
//         'relacion' => false
//     )
// );

return array(
    'personal' => array(
        'idUnicos' => array(
            'dni','usuario','idlocal'
        ),
        'relacion' => false,
        'miId' => 'idpersona'
        //'relacion' => array('persona_rol')
    ),
    'persona_rol' => array(
        'idUnicos' => array(
            'idpersonal','idempresa','idproyecto'
        ),
        'relacion' => false,
        'miId' => 'iddetalle'
    ),
    'persona_setting' => array(
        'idUnicos' => array(
            'idproyecto','idpersona','idrol'
        ),
        'relacion' => false,
        'miId' => 'idpersonasetting'
        //'relacion' => array('persona_rol')
    ),
    'acad_grupoauladetalle'=> array(
        'idUnicos' => array(
            'idgrupoaula','idcurso','idlocal'
        ),
        'relacion' => false,
        'miId' => 'idgrupoauladetalle'

    ),
    'acad_matricula' => array(
        'idUnicos' => array(
            'idgrupoauladetalle','idalumno','fecha_registro'
        ),
        'relacion' => false,
        'miId' => 'idmatricula'
    ),
    'bitacora_alumno_smartbook' => array(
        'idUnicos' => array(
            'idcurso','idsesion','idusuario'
        ),
        'relacion' => array('bitacora_smartbook'),
        'miId' => 'idbitacora_smartbook'
    ),
    'bitacora_smartbook' => array(
        'idUnicos' => array(
            'idcurso','idsesion','idsesionB','idusuario','pestania'
        ),
        'relacion' => false,
        'idesRel' => array('bitacora_alumno_smartbook'=> array('idcurso'=>'idcurso','idsesion'=>'idsesionB','idusuario'=>'idusuario')),
        'id' =>array('bitacora_alumno_smartbook'=>'idbitacora_alum_smartbook'),
        'miId' => 'idbitacora'
    ),
    'actividad_alumno' => array(
        'idUnicos' => array(
            'iddetalleactividad','idalumno','fecha','habilidades'
        ),
        'relacion' => false,
        'miId' => 'idactalumno'
    )
);
?>