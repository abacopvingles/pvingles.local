<?php

class request{
    private $_hostname;
    private $_uri;
    private $_postParams;
    private $_getParams;
    private $_params;

    public function __construct($uri =''){
        $this->setUri($uri);
    }
    /**SETER */
    public function setHostname($v){
        $this->_hostname = $v;
    }
    public function setUri($v){
        $this->_uri = $v;
    }
    public function setPostParams($v){
        $this->_postParams = $v;
    }
    public function setGetParams($v){
        $this->_getParams = $v;
    }
    public function setParams($v){
        $this->_params = $v;
    }
    /**GETER */
    public function getHostname(){
        return $this->_hostname;
    }
    public function getUri(){
        return $this->_uri;
    }
    public function getPostParams(){
        return $this->_postParams;
    }
    public function getGetParams(){
        return $this->_getParams;
    }
    public function getParams(){
        return $this->_params;
    }
    private static function getRequestUri(){
        $ruta   = SYNC_CONFIG_PATH.'config.php';
        $uri    = $_SERVER['REQUEST_URI'];

        if(!is_file($ruta)){
            throw new Exception("No se encontro el archivo config especificado");
        }
        $tmp_config = @include($ruta);
        if(!isset($tmp_config['root']) && empty($tmp_config['root'])){
            throw new Exception("No se encuentra definido la ruta indice");
        }
        if(strpos($uri,$tmp_config['root']) >= 0){
            $uri = str_replace($tmp_config['root'],'',$uri);
        }

        unset($tmp_config);

        return $uri;
    }
    private static function getParamsUri($uri){
        $arr_uri = array_slice(explode('/',$uri),3);
        return (!empty($arr_uri) && !empty($arr_uri[0])) ? $arr_uri : null;
    }
    private static function setControllerAction($uri){
        $arr_uri = array_slice(explode('/',$uri),1);
        $control = (isset($arr_uri[0]) && !empty($arr_uri[0])) ? $arr_uri[0] : 'index';
        $action = (isset($arr_uri[1]) && !empty($arr_uri[1])) ? $arr_uri[1] : 'index';
        define("CONTROLLER",$control);
        define("ACTION",$action);
    }
    public static function newHTTP(){
        $request = new request();
        $request->setHostname($_SERVER['SERVER_NAME']);
        $request->setUri(self::getRequestUri());
        $request->setPostParams($_POST);
        $request->setGetParams($_GET);
        $request->setParams(self::getParamsUri($request->getUri()));
        
        self::setControllerAction($request->getUri());
        
        return $request;
    }
}

?>