<?php

class app {
    protected static $_request;
    protected static $_router;

    /**GETER */
    public static function getRouter(){
        return self::$_router;
    }
    public static function getRequest(){
        return self::$_request;
    }
    /**SETER */
    public static function setRouter($v){
        self::$_router = $v;
    }
    public static function setRequest($v){
        self::$_request = $v;
    }
    private static function defines(){
        try{
            define('SYNC_PATH',getcwd().DIRECTORY_SEPARATOR);
            define('SYNC_CONFIG_PATH',SYNC_PATH."config".DIRECTORY_SEPARATOR);
            define('SYNC_STORAGE_PATH',SYNC_PATH."storage".DIRECTORY_SEPARATOR);
            define('SYNC_OBJ_PATH',SYNC_PATH."obj".DIRECTORY_SEPARATOR);
            define('SYNC_LOGS_PATH',SYNC_PATH."logs".DIRECTORY_SEPARATOR);
            define('STATIC_FOLDER',dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR."static".DIRECTORY_SEPARATOR);
        }catch(Exception $e){
            throw new Exception($e->getMessage());
            return;
        }
    }
    private static function autoload(){
        spl_autoload_register(array(__CLASS__,'load'));
    }
    private static function load($classname){
        try{
            if(substr($classname,-3) == 'Obj'){
                require_once(SYNC_OBJ_PATH."{$classname}.php");
            }
        }catch(Exception $e){
            return $e->getMessage();
        }
    }
    public static function run(request $request = null){
        try{
            self::defines();
            self::$_request = (is_null($request)) ? request::newHTTP() : $request;
            self::autoload();
            self::dispatch();
        }catch(Exception $e){
            return printf("Error en el Nucleo : %s",$e->getMessage());
        }
    }
    public static function dispatch($route = null){
        try{
            require_once(SYNC_OBJ_PATH."toolsObj.php");
            require_once(SYNC_OBJ_PATH."dbObj.php");
            $controllerName = CONTROLLER."Obj";
            $actionName = ACTION."Action";
            $controller = new $controllerName;
            $r = new ReflectionMethod($controllerName,$actionName);
            if($r->getNumberOfParameters() > 0  && !is_null(self::$_request->getParams()) ){
                call_user_func_array(array($controller,$actionName),self::$_request->getParams());
            }else{
                $controller->$actionName();
            }
        }catch(Exception $e){
            throw new Exception($e->getMessage());
            return false;
        }     
    }
}

?>