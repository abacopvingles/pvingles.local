<?php
date_default_timezone_set('America/Lima');
@require_once('./core/request.php');
@require_once('./core/app.php');
app::run();
?>