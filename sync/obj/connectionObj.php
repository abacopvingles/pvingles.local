<?php

class connectionObj {
    protected $_con;
    protected static $conn;

    public function __construct(){
        $this->connect();
    }
    public function __destruct(){
        $this->disconnect();
    }
    private function cofiguration(){
        $ruta_config = SYNC_CONFIG_PATH."db.php";
        if(!is_file($ruta_config)){
            throw new Exception("No existe el archivo de configuracion de la base de datos en RUTAPRINCIPAL/config");
        }
        $tmp_config['conconfig'] = include($ruta_config);
        $tmp_config['pdoconfig'] = array(
            PDO::ATTR_CASE => PDO::CASE_NATURAL,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
        );
        return $tmp_config;
    }
    public function disconnect(){
        $this->_con = null;
        self::$conn = null;
    }
    public function connect(){
        try{
            if(is_null($this->_con)){
                //obtener la configuracion de la base de datos
                $config = $this->cofiguration();
                $host = $config['conconfig']['host'];
                $dbname = $config['conconfig']['database'];
                $user = $config['conconfig']['user'];
                $password = $config['conconfig']['password'];
                $conn = new PDO("mysql:host=$host;dbname=$dbname",$user,$password,$config['pdoconfig']);
                if($conn){
                    $this->_con = $conn;
                    $conn = null;
               }else{
                 throw new Exception("Error: No connected to database");
               }
            }
            self::$conn = $this->_con;
            return $this->_con;
        }catch(Exception $e){
            throw new Exception("Error en la bd: ".$e->getMessage());
            return;
        }
    }
    public function getConnection(){
        return is_null($this->_con) ? $this->connect() : $this->_con;
    }
}

?>