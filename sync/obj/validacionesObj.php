<?php

class validacionesObj {
    protected $tablas_to_sync;
    protected $itablas_to_sync;
    protected $rutaProcess;
    protected $containerSend;
    protected $rulesPath;
    protected $tablasPath;
    protected $nombreProcess;
    protected $encryptBase64Path;
    protected $encryptBase64TablePath;

    public function __construct(){
      $this->nombreProcess = null;
      $this->rutaProcess = SYNC_LOGS_PATH."validacionProcess.json";
      $this->rulesPath = SYNC_CONFIG_PATH."tablesrules.php";
      $this->tablasPath = SYNC_LOGS_PATH."tablas.json";
      $this->encryptBase64Path = SYNC_LOGS_PATH."encryptDescryptBase64.json";
      $this->encryptBase64TablePath = SYNC_LOGS_PATH."encryptDescryptBase64Tables.json";
    }

    public function setTablasToSync($r){
      if(!is_file($r)){
        throw new Exception("No existe el archivo de configuracion de las tablas");
      }
      $this->tablas_to_sync = @include($r);
      $s = include($r);
    }
    public function getTablasToSync(){
      return $this->tablas_to_sync;
    }
    public function indexAction(){
      $this->tablasAction();
    }
    private function tablajson($ruta_ftabla){
      if(empty($this->tablas_to_sync)){
        $this->setTablasToSync(SYNC_CONFIG_PATH."tablasToSync.php");
      }
      if(!empty($this->tablas_to_sync)){
        $sql = "SELECT I.TABLE_NAME, I.TABLE_ROWS, I.CREATE_TIME, I.UPDATE_TIME FROM information_schema.tables I WHERE table_schema = DATABASE() AND I.TABLE_NAME IN ('".implode("','",$this->tablas_to_sync)."')";
      }else{
        throw new Exception("No indica ninguna tabla en la configuracion");
      }
      $result = dbObj::query($sql,true);
      if(!empty($result)){
        $result = toolsObj::tablaInfoOrder($result);
        file_put_contents($ruta_ftabla,json_encode($result));
      }else{
        throw new Exception("No se logro crear el archivo, el resultado esta vacio!");
      }
    }
    private function getValidacionProcess(){
      if(!is_file($this->rutaProcess)){
        return false;
      }
      return json_decode(file_get_contents($this->rutaProcess),true);
    }
    public function processAction(){
      try{
        $ruta = $this->rutaProcess;
        if($this->checkTableChange()){

          if(is_file($ruta)){
            return false;
          }
          $fecha = date('Y-m-d_h_i_s_').rand(0,60);
          $file = array(
            'nombre' => 'validacion_'.$fecha,
            'backup_path' => SYNC_STORAGE_PATH."backup".DIRECTORY_SEPARATOR,
            'backup_files_name' => array(
                'tablas' => 'tablas_'.$fecha.".json"
            )
          );
          if(!file_put_contents($ruta,json_encode($file))){
            throw new Exception("No se logro crear el archivo log del proceso de validacion");
          }
          return true;
        }
        return false;
      }catch(Exception $e){
        printf("Error en el proceso: %s",$e->getMessage());
        return;
      }
    }
    public function checkTableChange(){
      try{
        if(!is_file($this->tablasPath)){
          return true;
        }
        if(empty($this->tablas_to_sync)){
          $this->setTablasToSync(SYNC_CONFIG_PATH."tablasToSync.php");
        }
        $ftabla = json_decode(file_get_contents($this->tablasPath),true);
        if(!empty($this->tablas_to_sync)){
          //$sql = "SELECT I.TABLE_NAME, I.TABLE_ROWS, I.CREATE_TIME, I.UPDATE_TIME FROM information_schema.tables I WHERE table_schema = DATABASE() AND I.TABLE_NAME IN ('".implode("','",$this->tablas_to_sync)."')";
          $result = toolsObj::getTablaInfo($this->tablas_to_sync);
        }else{
          throw new Exception("No indica ninguna tabla en la configuracion");
        }
        // $result = dbObj::query($sql,true);
        if(!empty($result)){
          $i=0;$c=count($result);
          //verificar si los dos array tienen el mismo numero de filas, si no crear nuevamente el archivo
          if($c !== count($ftabla)){
            $this->tablajson($this->tablasPath);
          }
          do{
            if(strcmp($result[$i]['TABLE_NAME'],$ftabla[$i]['TABLE_NAME']) === 0){
              if(intval($result[$i]['TABLE_ROWS']) > intval($ftabla[$i]['TABLE_ROWS']) ){
                return true;
              }
              //validar campo de actualizacion con los null
              if(!is_null($result[$i]['UPDATE_TIME']) && !is_null($ftabla[$i]['UPDATE_TIME'])){
                if(strtotime($result[$i]['UPDATE_TIME']) > strtotime($ftabla[$i]['UPDATE_TIME'])){
                  return true;
                }
                if($result[$i]['isUpdate'] == true){
                  return true;
                }
              }else if(is_null($result[$i]['UPDATE_TIME']) && !is_null($ftabla[$i]['UPDATE_TIME'])){
                return true;
              }
            }
            ++$i;
          }while($i < $c);
        }
        return false;
      }catch(Exception $e){
        printf("error en el chequedo de tablas rapida %s",$e->getMessage());
        return false;
      }
    }
    public function tablasAction(){
      try{
        $ruta_ftabla = $this->tablasPath; 
        if(!is_file($ruta_ftabla)){
          $this->tablajson($ruta_ftabla);
        }
        if(empty($this->tablas_to_sync)){
          $this->setTablasToSync(SYNC_CONFIG_PATH."tablasToSync.php");
        }

        $ftabla = json_decode(file_get_contents($ruta_ftabla),true);
        
        if(!empty($this->tablas_to_sync)){
          //$sql = "SELECT I.TABLE_NAME, I.TABLE_ROWS, I.CREATE_TIME, I.UPDATE_TIME FROM information_schema.tables I WHERE table_schema = DATABASE() AND I.TABLE_NAME IN ('".implode("','",$this->tablas_to_sync)."')";
          $result = toolsObj::getTablaInfo($this->tablas_to_sync);          
        }else{
          throw new Exception("No indica ninguna tabla en la configuracion");
        }
        
        //$result = dbObj::query($sql,true); 

        if(!empty($result)){
          $i=0;$c=count($result);
          //verificar si los dos array tienen el mismo numero de filas, si no crear nuevamente el archivo

          if($c !== count($ftabla)){
            $this->tablajson($ruta_ftabla);
          }


          //AQUI HACER EL BACKUP
          $info = json_decode(file_get_contents(SYNC_LOGS_PATH."validacionProcess.json"),true);
          if (!file_exists($info['backup_path'])) {
            mkdir($info['backup_path'], 0777, true);
          }
          if(!is_file($info['backup_path'].$info['backup_files_name']['tablas'])){
            if(!file_put_contents($info['backup_path'].$info['backup_files_name']['tablas'],json_encode($ftabla))){
              throw new Exception("No se logro crear el archivo backup de las tablas actual");
            }
          }
          $this->itablas_to_sync = array();
          do{
            if(strcmp($result[$i]['TABLE_NAME'],$ftabla[$i]['TABLE_NAME']) === 0){
              $prepare = array('table'=> $result[$i]['TABLE_NAME'],'update'=> false,'insert' => false);
              if(intval($result[$i]['TABLE_ROWS']) > intval($ftabla[$i]['TABLE_ROWS']) ){
                $prepare['insert'] = true;
              }
              //validar campo de actualizacion con los null
              if(!is_null($result[$i]['UPDATE_TIME']) && !is_null($ftabla[$i]['UPDATE_TIME'])){
                if(strtotime($result[$i]['UPDATE_TIME']) > strtotime($ftabla[$i]['UPDATE_TIME'])){
                  $prepare['update'] = true;                
                }
                if($result[$i]['isUpdate'] == true){
                  $prepare['update'] = true;                
                }
              }else if(is_null($result[$i]['UPDATE_TIME']) && !is_null($ftabla[$i]['UPDATE_TIME'])){
                $prepare['update'] = true;                
              }
              $this->itablas_to_sync[] = $prepare;
            }
            ++$i;
          }while($i < $c);
        }else{
          throw new Exception("la ultima consulta esta vacia");
        }
        unset($ruta_ftabla);
        return true;
      }catch(Exception $e){
        printf("Error en el proceso: %s",$e->getMessage());
        return;
      }
    }
    private function createContainer(){
      $this->nombreProcess = $this->getValidacionProcess()['nombre'];
      $this->containerSend = SYNC_STORAGE_PATH."send".DIRECTORY_SEPARATOR.$this->nombreProcess;
      if (!file_exists($this->containerSend)) {
        mkdir($this->containerSend, 0777, true);
      }
      return true;
    }
    private function getReglas(){
      if(!is_file($this->rulesPath)){
        throw new Exception("no existe la configuracion con las reglas de tablas");
      }
      $r = @include($this->rulesPath);
      if(empty($r)){
        throw new Exception("Esta vacio las reglas de las tablas");
      }
      return $r;
    }
    /**
     * VALIDACIONES ENTRE TABLAS...
     * 
     */
    public function ontableAction(){
      try{
        
        if(empty($this->itablas_to_sync)){
          if($this->tablasAction() == false && empty($this->itablas_to_sync)){
            throw new Exception("no hay informacion de las tablas a recorrer");
          } 
        }
        
        //reglas de tablas
        $rules = $this->getReglas();

        //crear contenedor
        if($this->createContainer() != true){
          throw new Exception("No se logro crear el contenedor para el envio");
        }
        //insert process
        //$this->itablas_to_sync[5]['insert'] = true;
        $checkInsert_keys = array_keys(array_column($this->itablas_to_sync,'insert'), true);
        if(!empty($checkInsert_keys)){
          foreach($checkInsert_keys as $i){
            $checkInsert = $this->itablas_to_sync[$i];
            if($checkInsert['insert'] == true){
              $tabla = $checkInsert['table'];
              $this->$tabla($rules[$checkInsert['table']]);
              $this->itablas_to_sync[$i]['insert'] = false;
            }
          }
        }//end if
        //update process
        $checkUpdate_keys = array_keys(array_column($this->itablas_to_sync,'update'),true);
        if(!empty($checkUpdate_keys)){
          foreach($checkUpdate_keys as $i){
            $checkUpdate = $this->itablas_to_sync[$i];
            if($checkUpdate['update'] == true){
                $tabla = "u{$checkUpdate['table']}";
                $this->$tabla($rules[$checkUpdate['table']]);
                $this->itablas_to_sync[$i]['update'] = false;
            }
          }
        }//end if
        
        if(!empty($checkUpdate_keys) || !empty($checkInsert_keys)){
          //if(count(glob("$dir/*")) !== 0){
          //eliminar el proceso
          if(copy($this->rutaProcess,$this->containerSend.DIRECTORY_SEPARATOR."validacionProcess.json") && !unlink($this->rutaProcess)){
            throw new Exception("No se logro eliminar el log del proceso de validaciones");
          }
          //actualizar log de tablas
          $this->tablajson($this->tablasPath);
          
          return true;
        }//end if
        return false;
      }catch(Exception $e){
        throw new Exception("Error en el proceso :".$e->getMessage(),0201);
        return;
      }
    }
    private function comillar($numeros,$tipo,$value,$convert=false){
      $match = (str_replace($numeros, '', $tipo) != $tipo);
      if($match == true){
        $number = ($value == (int) $value) ? (int) $value : (float) $value;
        return strval($number);
      }else{
        $search = array("'",chr(145), chr(146),chr(133),chr(147),chr(148),chr(150),chr(151),chr(149),chr(132),chr(130),chr(152));
        $replace = array("\'","\'", "\'",'...','"','"','-','--','-','"',',','~'); 
        $escapado = str_replace($search,$replace,$value);
        $escapado = ($convert == true) ? "ZW1teXNlY28=0x17_".base64_encode($escapado) : $escapado;
        return "'{$escapado}'";
      }
    }
    private function procesarValores($tabla,$_id,$objeto){
      try{
        $resultado = array();
        $idesValues = array();
        $encryptBase64 = toolsObj::getFile($this->encryptBase64Path);
        $encryptBase64Table = toolsObj::getFile($this->encryptBase64TablePath);

        $sql = "SELECT t.DATA_TYPE AS tipo FROM information_schema.COLUMNS t WHERE TABLE_NAME = '{$tabla}' AND t.TABLE_SCHEMA = DATABASE()";
        $estructura = dbObj::query($sql,true);
        $numeros = array('int','bigint','float');
        $i = 0;
        $valorcomilladaFechaUpdate = $this->comillar($numeros,'string',$objeto['fechamodificacion']);
        $search_tabla = false;
        if(!empty($encryptBase64Table) && !empty($encryptBase64)){
          $search_tabla = array_search($tabla,$encryptBase64Table);          
        }
        foreach($objeto as $key => $value){
          $convertirB64 = false;
          if(strcmp($key,'fechamodificacion')===0 || strcmp($key,'previamodificacion')===0){
            $valueComillado = $valorcomilladaFechaUpdate;
          }else{
            //hacer el reemplazo
            if($tabla == 'notas_quiz' && strcmp($key,'preguntas')===0){
              $value = str_replace('\\', '\\\\', $value);
            }
            $tipo =  $estructura[$i]['tipo'];
            //convertir base64 los html
            if($search_tabla !== false && (str_replace($numeros, '', $tipo) != $tipo) != true ){
              foreach($encryptBase64 as $campo_value){
                if(strcmp($campo_value,$key) === 0){
                  $convertirB64 = true;
                  break;
                }
              }//end foreach
            }//end if saerch tabla
            $valueComillado = $this->comillar($numeros,$tipo,$value,$convertirB64);
            $valueComillado = (mb_detect_encoding($valueComillado,"UTF-8",true) != "UTF-8" || mb_detect_encoding($valueComillado,"UTF-8",true) == false) ? utf8_encode($valueComillado) : utf8_decode($valueComillado);
          }
          $valueComillado = ($tabla == 'actividad_alumno' || $tabla == 'notas_quiz') ? preg_replace("/[\r\n|\n|\r]+/", " ", $valueComillado) : $valueComillado;
          if(in_array($key,$_id)){
            $idesValues[$key] = $valueComillado;
          }
          $resultado[] = $valueComillado;
          ++$i;
        }
        if(empty($resultado)){
          return false;
        }
        return array('values'=> implode(',',$resultado),'idcheck' =>$idesValues);
      }catch(Exception $e){
        return false;
      }
    }

    private function procesarSets($tabla,$_id,$objeto){
      try{
        $resultado = array();
        $idesValues = array();
        $encryptBase64 = toolsObj::getFile($this->encryptBase64Path);
        $encryptBase64Table = toolsObj::getFile($this->encryptBase64TablePath);

        $sql = "SELECT t.DATA_TYPE AS tipo FROM information_schema.COLUMNS t WHERE TABLE_NAME = '{$tabla}' AND t.TABLE_SCHEMA = DATABASE()";
        $estructura = dbObj::query($sql,true);        
        $numeros = array('int','bigint','float');
        $i = 0;
        $valorcomilladaFechaUpdate = $this->comillar($numeros,'string',$objeto['fechamodificacion']);
        $search_tabla = false;
        if(!empty($encryptBase64Table) && !empty($encryptBase64)){
          $search_tabla = array_search($tabla,$encryptBase64Table);          
        }
        foreach($objeto as $key => $value){
          $convertirB64 = false;
          if(strcmp($key,'fechamodificacion')===0 || strcmp($key,'previamodificacion')===0){
            $valorcomillado = $valorcomilladaFechaUpdate;
          }else{
            $tipo =  $estructura[$i]['tipo'];
            //convertir base64 los html
            if($search_tabla !== false && (str_replace($numeros, '', $tipo) != $tipo) != true ){
              foreach($encryptBase64 as $campo_value){
                if(strcmp($campo_value,$key) === 0){
                  $convertirB64 = true;
                  break;
                }
              }//end foreach
            }//end if saerch tabla
            $valorcomillado = $this->comillar($numeros,$tipo,$value,$convertirB64);
            $valorcomillado = (mb_detect_encoding($valorcomillado,"UTF-8",true) != "UTF-8" || mb_detect_encoding($valorcomillado,"UTF-8",true) == false) ? utf8_encode($valorcomillado) : utf8_decode($valorcomillado);
          }
          $valorcomillado = ($tabla == 'actividad_alumno' || $tabla == 'notas_quiz') ? preg_replace("/[\r\n|\n|\r]+/", " ", $valorcomillado) : $valorcomillado;
          if(in_array($key,$_id)){
            $idesValues[$key] = $valorcomillado;
          }
          $resultado[] = "{$key} = ".$valorcomillado;
          ++$i;
        }
        if(empty($resultado)){
          return false;
        }
        return array('values' => implode(',',$resultado), 'idcheck' => $idesValues);
      }catch(Exception $e){
        return false;
      }
    }

    public function crearHistorial($config){
      try{
        if(!empty($this->containerSend) && count(glob($this->containerSend.DIRECTORY_SEPARATOR."*")) !== 0){
          $tablas = toolsObj::Historial_iiee($config['idproyecto']);
          $json = json_encode($tablas);
          if(!file_put_contents($this->containerSend.DIRECTORY_SEPARATOR."historialIIEE.json",$json)){
            throw new Exception("No se creo el historia de tabla correctamente");
          }
          return true;
        }
        return false;
      }catch(Exception $e){
        throw new Exception($e->getMessage());
      }
    }

    /**
     * Insert
     */
    private function oninsert($table,$pk,$rules){
      try{
        $v = dbObj::searchInsert($table);
        if(!empty($v)){
          if(is_array($rules['relacion'])){
            $_rules = $this->getReglas();
            $j = 0 ; $cc = count($rules['relacion']);
            $idRelacion = array();
            do{
              $idRelacion[] = array('nombre'=>$rules['relacion'][$j],'idUnicos'=>$_rules[$rules['relacion'][$j]]['idUnicos']);           
              ++$j;
            }while($j < $cc);
          }//endif
          $id = $rules['idUnicos'];
          $lista = array();
          $i = 0; $c = count( $v);
          $idForUpdate = array();
          do{
            $idForUpdate[] = $v[$i][$pk];
            $campos = array();
            foreach($v[$i] as $llave => $valor){
              $campos[] = $llave;
            }
            if(empty($campos)){
              throw new Exception("campos vacios");
            }
            $values = $this->procesarValores($table,$id,$v[$i]);
            $campos = implode(',',$campos);
            $sql = array('statement'=>"INSERT INTO {$table} ({$campos}) VALUES ({$values['values']})",'idcheck'=>$values['idcheck']);
           
            $lista[] = $sql;
            ++$i;
          }while($i < $c);
          if(dbObj::UpdateFecha($table,$pk,$idForUpdate) !== true){
            throw new Exception("No se logro actualizar los registro insertados");
          }
          $json = (isset($idRelacion) && !empty($idRelacion)) ? json_encode(array('idrel'=>$idRelacion,'id'=>$id,'sql'=> $lista)) : json_encode(array('id'=>$id,'sql'=> $lista));
          $gz = gzcompress($json,9);
          if(empty($this->containerSend)){
            if($this->createContainer() != true){
              throw new Exception("No se logro crear el contenedor para el envio");
            }
          }
          if(!file_put_contents($this->containerSend.DIRECTORY_SEPARATOR."{$table}.txt",$gz)){
            return false;
          }
          return true;  
        }//endif empty $v
        return false;
      }catch(Exception $e){
        throw new Exception($e->getMessage());
      }
    }
    private function notas_quiz($rules){
      try{
        return $this->oninsert('notas_quiz','idnota',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function personal($rules){
      try{
        return $this->oninsert('personal','idpersona',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function persona_rol($rules){
      try{
        return $this->oninsert('persona_rol','iddetalle',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function persona_setting($rules){
      try{
        return $this->oninsert('persona_setting','idpersonasetting',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function acad_matricula($rules){
      try{
        return $this->oninsert('acad_matricula','idmatricula',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function acad_grupoauladetalle($rules){
      try{
        return $this->oninsert('acad_grupoauladetalle','idgrupoauladetalle',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function bitacora_alumno_smartbook($rules){
      try{
        return $this->oninsert('bitacora_alumno_smartbook','idbitacora_smartbook',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function bitacora_smartbook($rules){
      try{
        return $this->oninsert('bitacora_smartbook','idbitacora',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    private function actividad_alumno($rules){
      try{
        return $this->oninsert('actividad_alumno','idactalumno',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0202);
      }
    }
    /**
     * Update
     */
    private function onupdate($table,$pk,$rules){
      try{
        //buscar diferencia entre fechamodificacion y previamodificacion
        $v = dbObj::searchUpdate($table);
        if(!empty($v)){
          if(is_array($rules['relacion'])){
            $_rules = $this->getReglas();
            $j = 0 ; $cc = count($rules['relacion']);
            $idRelacion = array();
            do{
              $idRelacion[] = array('nombre'=>$rules['relacion'][$j],'idUnicos'=>$_rules[$rules['relacion'][$j]]['idUnicos']);
              ++$j;
            }while($j < $cc);
          }//end if
          $lista = array();
          $i = 0; $c = count($v);
          $id = $rules['idUnicos'];
          $idForUpdate = array();
          do{
            $idForUpdate[] = $v[$i][$pk];

            $set = $this->procesarSets($table,$id,$v[$i]);
            $condition = array();
            foreach($set['idcheck'] as $key => $val){
              $condition[] = "{$key} = {$val}";
            }//end foreach
            $where = implode(' AND ',$condition);
            $sql = array('statement' => "UPDATE {$table} SET {$set['values']} WHERE {$where}" , 'idcheck'=>$set['idcheck']);            

            $lista[] = $sql;

            ++$i;
          }while($i < $c);
          if(dbObj::UpdateFecha($table,$pk,$idForUpdate) !== true){
            throw new Exception("No se logro actualizar los registro Actualizados");
          }
          //generar y comprimir archivo de tabla
          $json = (isset($idRelacion) && !empty($idRelacion)) ? json_encode(array('idrel'=>$idRelacion,'id'=>$id,'sql'=> $lista)) : json_encode(array('id'=>$id,'sql'=> $lista));
          $gz = gzcompress($json,9);
          if(empty($this->containerSend)){
            if($this->createContainer() != true){
              throw new Exception("No se logro crear el contenedor para el envio");
            }
          }
          if(!file_put_contents($this->containerSend.DIRECTORY_SEPARATOR."u_{$table}.txt",$gz)){
            return false;
          }
          return true;
        }//endif empty $v
        return false;
      }catch(Exception $e){
        throw new Exception($e->getMessage());
      }
    }
    private function unotas_quiz($rules){
      try{
        return $this->onupdate('notas_quiz','idnota',$rules);
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function upersonal($rules){
      try{
        return $this->onupdate('personal','idpersona',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function upersona_rol($rules){
      try{
        return $this->onupdate('persona_rol','iddetalle',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function upersona_setting($rules){
      try{
        return $this->onupdate('persona_setting','idpersonasetting',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function uacad_matricula($rules){
      try{
        return $this->onupdate('acad_matricula','idmatricula',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function uacad_grupoauladetalle($rules){
      try{
        return $this->onupdate('acad_grupoauladetalle','idgrupoauladetalle',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function ubitacora_alumno_smartbook($rules){
      try{
        return $this->onupdate('bitacora_alumno_smartbook','idbitacora_smartbook',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function ubitacora_smartbook($rules){
      try{
        return $this->onupdate('bitacora_smartbook','idbitacora',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
    private function uactividad_alumno($rules){
      try{
        return $this->onupdate('actividad_alumno','idactalumno',$rules);       
      }catch(Exception $e){
        throw new Exception($e->getMessage(),0203);
      }
    }
}

?>