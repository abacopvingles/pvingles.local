<?php
class toolsObj{
    protected static $inicio;
    public static function TimeCalculation_START(){
        self::$inicio = null;
        self::$inicio = microtime(true);
    }
    public static function TimeCalculation_FINISH(){
        $fin = microtime(true);
        return ($fin - self::$inicio);
    }
    public static function listOnlyDir_by_date($pathtosearch,$max = null){
        $file_array = array();
        foreach (glob($pathtosearch,GLOB_ONLYDIR) as $filename)
        {
            $time = filectime($filename);
            while(array_key_exists($time,$file_array)){
                ++$time;
            }
            $file_array[$time]=basename($filename); // or just $filename
        }
        ksort($file_array);
        if(!is_null($max) && $max > 0){
            $newlist = array();
            $i = 1;
            foreach($file_array as $key => $value){
                if($i > $max){ break; }
                $newlist[$key] = $value;
                ++$i;
            }
            $file_array = $newlist;
        }
        return $file_array;
    }
    public static function listdir_by_date($pathtosearch,$max = null)
    {
        $file_array = array();
        foreach (glob($pathtosearch) as $filename)
        {
            $time = filectime($filename);
            while(array_key_exists($time,$file_array)){
                ++$time;
            }
            $file_array[$time]=basename($filename); // or just $filename
        }
        ksort($file_array);
        if(!is_null($max) && $max > 0){
            $newlist = array();
            $i = 1;
            foreach($file_array as $key => $value){
                if($i > $max){ break; }
                $newlist[$key] = $value;
                ++$i;
            }
            $file_array = $newlist;
        }
        return $file_array;
    }
    public static function listdir($pathtosearch){
        $file_array = array();
        foreach(glob($pathtosearch) as $filename){
            $file_array[] = basename($filename);
        }
        return $file_array;
    }
    public static function encrypt($dir,$json){
        try{
            $gz = gzcompress($json,9);
            if(!file_put_contents($dir,$gz)){
                return false;
            }
            return true;
        }catch(Exception $e){
            return false;
        }
    }
    public static function descrypt($dir){
        try{
            if(!is_file($dir)){
              return false;
            }
            $fstream = file_get_contents($dir);
            $funcompress = gzuncompress($fstream);
            return $funcompress;
          }catch(Exception $e){
            throw new Exception($e->getMessage());
          }
    }
    // public static function deleteDirectory($dir) {
    //     system('rm -rf ' . escapeshellarg($dir), $retval);
    //     var_dump($dir);
    //     var_dump($retval);
    //     return $retval !== false; // UNIX commands return zero on success
    // }
    public static function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }
    
        if (!is_dir($dir)) {
            return unlink($dir);
        }
    
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
    
            if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
    
        }
    
        return rmdir($dir);
    }
    public static function updateFile($dir,$newArr){
        try{
            if(empty($newArr)){
                return false;
            }
            if(!file_put_contents($dir, json_encode($newArr))){
                return false;
            }
            return true;
        }catch(Exception $e){
            return false;
        }
    }
    public static function requestHTTP($uri,$request,$ssl = false){
        try{
            $ch = curl_init();
            if($ssl !== false){
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
            }
            curl_setopt($ch, CURLOPT_URL,$uri);
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            $result=curl_exec($ch);
            curl_close($ch);
            return $result;
        }catch(Exception $e){
            printf("error to send request : %s",$e->getMessage());
            return false;
        }
    }
    public static function desencrypt64SQL($sql,$llave){
        try{
            if(strcasecmp(substr($sql,0,6),"INSERT") === 0){
                $part = explode('VALUES',$sql);
                $busqueda = strpos($part[1],$llave);
                while($busqueda !== false){
                   $posContinuo = ($busqueda + 17);
                   $valor = substr($part[1],($busqueda+17),strpos($part[1],"'",($busqueda+17)) - ($busqueda+17));
                   $valorDescrypt = base64_decode($valor);
                   // var_dump($valor);
                   // var_dump($valorDescrypt);
                   $part[1] = substr($part[1],0,($busqueda)).$valorDescrypt.substr($part[1],strpos($part[1],"'",($busqueda+17)));
                   @$busqueda = strpos($part[1],$llave,$posContinuo);
                }
                $sql = implode('VALUES',$part);
             }else if(strcasecmp(substr($sql,0,6),"UPDATE") === 0){
                $part = explode('WHERE',$sql);
                $busqueda = strpos($part[0],$llave);
                while($busqueda !== false){
                   $posContinuo = ($busqueda + 17);
                   $valor = substr($part[0],($busqueda+17),strpos($part[0],"'",($busqueda+17)) - ($busqueda+17));
                   $valorDescrypt = base64_decode($valor);
                   // var_dump($valor);
                   // var_dump($valorDescrypt);
                   $part[0] = substr($part[0],0,($busqueda)).$valorDescrypt.substr($part[0],strpos($part[0],"'",($busqueda+17)));
                   
                   @$busqueda = strpos($part[0],$llave,$posContinuo);
                }
                $sql = implode('WHERE',$part);
             }
             return $sql;
        }catch(Exception $e){
            throw new Exception("error: ".$e->getMessage());
        }
    }
    public static function getFile($dir){
        try{
            $file = array();
            if($a = file_get_contents($dir)){
                $file = json_decode($a,true);
            }else{
                return false;
            }
            return $file;
        }catch(Exception $e){
            return false;
        }
    }
    public static function makezip($origin,$destiny){
        try{
            $zip = new ZipArchive();
            $zip->open($destiny,ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $arr = explode(DIRECTORY_SEPARATOR,$origin);
            $option = array('add_path' => end($arr).DIRECTORY_SEPARATOR, 'remove_all_path' => TRUE);
            $zip->addGlob($origin.DIRECTORY_SEPARATOR.'*',0,$option);
            $zip->close();
            if(!is_file($destiny)){
                throw new Exception("Por alguna razon no se creo el archivo");
            }
            return true;
        }catch(Exception $e){
            printf("error en hacer zip: %s",$e->getMessage());
            return false;
        }
    }
    public static function unzip($dir,$container){
        try{
            $zip = new ZipArchive;
              $res = $zip->open($dir);
              if($res === TRUE){
                  $zip->extractTo($container);
                  $zip->close();
                  return true;
              }else{
                  printf("error en apertura de archivo zip: %s", $res);
                  return false;
              }
            return true;
        }catch(Exception $e){
            printf("error en descomprimir zip: %s",$e->getMessage());
            return false;
        }
    }
    public static function getReglas($dir){
        try{
            if(!is_file($dir)){
                throw new Exception("no existe la configuracion con las reglas de tablas");
            }
            $r = @include($dir);
            if(empty($r)){
                throw new Exception("Esta vacio las reglas de las tablas");
            }
            return $r;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public static function getMainConfig($dir = null){
        try{
            $config = (!is_null($dir)) ? @include($dir) : @include(SYNC_CONFIG_PATH."config.php");
            return $config;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public static function Historial_iiee($idProyecto){
        try{
            $resultado = array();
            $resultado = dbObj::getIIEE($idProyecto);
            if(!empty($resultado)){
                $i = 0; $cc = count($resultado);
                do{
                    $resultado[$i]['sync'] = false;
                    ++$i;
                }while($i < $cc);
            }
            return $resultado;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public static function tablaInfoOrder($result){
        $orden = @include(SYNC_CONFIG_PATH."tablasToSync.php");
        $newResult = array();
        foreach($orden as $v){
            $search = array_search($v,array_column($result,'TABLE_NAME'));
            if($search !== false){
                $newResult[] = $result[$search];
            }
        }
        return $newResult;
    }
    public static function getTablaInfo($tablas){
        try{
            
            $stringTable = implode("','",$tablas);
            $resultado = dbObj::tabla_info($stringTable);
            $resultado = self::tablaInfoOrder($resultado);
            if(!empty($resultado)){
                $i = 0; $cc = count($resultado);
                do{
                    $obtenerUpdate = dbObj::getExistsTableUpdate($tablas[$i]);
                    $resultado[$i]['isUpdate'] = $obtenerUpdate;
                    ++$i;
                }while($i < $cc);
            }
            return $resultado;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}
?>