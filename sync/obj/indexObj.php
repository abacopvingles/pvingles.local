<?php
// set_time_limit(90);
class indexObj {
    public function indexAction(){
        var_dump("funcionando");
        exit(0);
    }
    private function chequearEnviar(){
        try{
            $envios = new enviosObj();
            if($envios->validarAction()){
                return true;
            }//end if envios
            //chequear si hay un proceso muerto...
            return false;
        }catch(Exception $e){
            printf("Error en el lanzador de envio: %s",$e->getMessage());            
            return false;
        }
    }
    private function prepararZip($type = 0, $force = false){
        try{
            $envios = new enviosObj();
            switch($type){
                case 0:{
                    if($envios->preparezipAction()){
                        return true;
                    }
                }
                break;
                case 1:{
                    if($envios->preparezipAction()){
                        //obtener los utlimos 20 y log de dichos registros
                        
                        $manualObj = new manualObj();
                        $obtener = $manualObj->zipdownloadAction($force);
                        if(!empty($obtener)){
                            return $obtener;
                        }
                    }
                }//end case
                break;
                case 2:{
                    if($envios->preparezipAction()){
                        //obtener los archivos de los registros y eliminarlos
                        $manualObj = new manualObj();
                        if($force == true){
                            //do it
                            //hacer primero el normal
                            $obtener = $manualObj->zipdownloadforcedAction();
                            if(!empty($obtener)){
                                return $obtener;
                            }//end if empty
                        }else{
                            $obtener = $manualObj->zipdownloadAction(true);
                            if(!empty($obtener)){
                                // if($manualObj->deletemanualdownAction()){
                                    return $obtener;
                                // }
                            }//end if empty
                        }//end if force
                    }
                }//end case
                break;
            }
            
            return false;
        }catch(Exception $e){
            printf("Error en el lanzador de prepara zip: %s",$e->getMessage());            
            return false;
        }
    }
    public function startAction(){
        try{
            $MainConfig = toolsObj::getMainConfig(SYNC_CONFIG_PATH."config.php");

            $result = false;
            $validaciones = new validacionesObj();

            if($validaciones->processAction() == true){
                if($validaciones->tablasAction()){
                    if($validaciones->ontableAction()){
                        if(!empty($MainConfig) && $MainConfig['offline'] == false){
                            /**
                             * online
                             */
                            $this->prepararZip();
                        }else{
                            /**
                             * offline
                             */
                            $this->chequearEnviar();
                        }
                    }// end if process on table
                }//end if process tablas
            }else{
                if($MainConfig['offline'] == true){
                    $this->chequearEnviar();
                }else{
                    $this->prepararZip();
                }
            }//end if process action
            //verificar si hay receptor
            $recepcion = new recepcionObj();
            if($MainConfig['offline'] == true){
                //... Sync de recepcion por comprobacion en el repositorio OFFLINE
                $recepcion->setConfiguration($MainConfig);
                $recepcion->requestfilesAction();
                // procesar sync
                $recepcion->verificarAction();
            }else{
                //... Sync de recepcion por comprobacion del repositorio local ONLINE
                $recepcion->verificarAction();
            }
            return true;
        }catch(Exception $e){
            printf("Error en el lanzador: %s",$e->getMessage());
            return false;
        }
    }
    public function manualdownAction($force = false){
        try{
            if(is_string($force)){
                $force = (strcmp( strtolower($force),'false')=== 0) ? !boolval($force) : boolval($force);
            }
            $MainConfig = toolsObj::getMainConfig(SYNC_CONFIG_PATH."config.php");

            $result = false;
            $validaciones = new validacionesObj();
            
            if($validaciones->processAction() == true){
                
                if($validaciones->tablasAction()){
                    
                    if($validaciones->ontableAction()){
                        if(!empty($MainConfig) && $MainConfig['offline'] == false){
                            /**
                             * online
                             */
                            echo $this->prepararZip(1,$force);
                            exit(0);
                            //download
                        }else{
                            /**
                             * offline
                             */
                            echo $this->prepararZip(2,$force);
                            exit(0);
                        }
                    }// end if process on table
                }//end if process tablas                
            }else{
                if($MainConfig['offline'] == true){
                    echo $this->prepararZip(2);
                    exit(0);                       
                }else{
                    echo $this->prepararZip(1,$force);
                    exit(0);
                }
            }
            return -1;
        }catch(Exception $e){
            printf("Error en el lanzador de la sincronizacion manual : %s",$e->getMessage());
            return false;
        }
    }
    public function pruebaAction(){
        try{
            $sql = array();
            $sql[] = "INSERT INTO personal (idpersona,tipodoc,dni,ape_paterno,ape_materno,nombre,fechanac,sexo,estado_civil,ubigeo,urbanizacion,direccion,telefono,celular,email,idugel,regusuario,regfecha,usuario,clave,token,rol,foto,estado,situacion,idioma,tipousuario,idlocal,oldid,fechamodificacion,previamodificacion) VALUES (1,1,43831104,'Chingo','Tello','Abel','1986-10-27','M','S','','','','942171837','','abelchingo@gmail.com','',1,'2017-11-11','admin','f3f1c26545d2424e5bbc7bf12a8f2dc6','admin2018',1,'43831104 -20180116062511.jpg',1,'1','EN','s',1,0,'2019-02-21 17:33:40','2019-02-21 17:33:40')";
            if(!dbObj::InsertTransac($sql)){
                throw new Exception("no se logro completar la actualizacion por lotes");
            }
        }catch(Exception $e){
            printf("Error en el lanzador de la prueba : %s",$e->getMessage());
        }
    }
    public function manualupAction(){
        try{
            $MainConfig = toolsObj::getMainConfig(SYNC_CONFIG_PATH."config.php");

            //verificar si hay receptor
            $manual = new manualObj();
            if($manual->unzipreceiveAction($MainConfig['offline'])){
                $recepcion = new recepcionObj();
                // procesar sync
                $recepcion->verificarAction($MainConfig['offline']);
            }
            echo json_encode(array("code"=>'ok',"message"=>'Sync Completed'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array("code"=>'error',"message"=>'"Error en el lanzador sincronizacion subida : '.$e->getMessage()));
            exit(0);
        }
    }
    public function manualupaudioAction(){
        try{
            $manual = new manualObj();
            if(!$manual->syncAudio()){
                throw new Exception("no logro sincronizar el audio");
            }            
            echo json_encode(array("code"=>'ok',"message"=>'Sync Completed'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array("code"=>'error',"message"=>'"Error en el lanzador sincronizacion (audio) subida : '.$e->getMessage()));
            exit(0);
        }
    }
    public function obtenermanualAction(){
        try{
            //realizar sincronización manual
            // header("Content-type: application/zip");
            // header("Content-Disposition: attachment; filename=syncmanual.zip");
            //header("Content-Length: " . filesize($filePath));
            // header('Pragma: public');
            // header("Expires: 0");
            $manual = new manualObj();
            echo $manual->getFile();
        }catch(Exception $e){
            printf("Error en el lanzador: %s",$e->getMessage());
            return false;
        }
    }
    public function obtenermanualaudioAction(){
        try{
            $manual = new manualObj();
            echo $manual->getAudio();
        }catch(Exception $e){
            printf("Error en el lanzador: %s", $e->getMessage());
            return false;
        }
    }
}

?>