<?php
class recepcionObj {
  protected $receiveProcessPath;
  protected $receivePath;
  protected $tablesToSyncPath;
  protected $tablerulesPath;
  protected $folderActual;
  protected $configurationMain;
  protected $historialSyncPath;
  protected $historialSyncObj;
  protected $itemstodownload;

  public function __construct(){
    $this->receiveProcessPath = SYNC_LOGS_PATH."receiveProcess.json";
    $this->receivePath = SYNC_STORAGE_PATH."receive".DIRECTORY_SEPARATOR;
    $this->tablesToSyncPath = SYNC_CONFIG_PATH."tablasToSync.php";
    $this->tablerulesPath = SYNC_CONFIG_PATH."tablesrules.php";
    $this->historialSyncPath = SYNC_LOGS_PATH."receiveSyncList.json";
    $configurationMain = null;
    $historialSyncObj = null;
    $itemstodownload = null;
  }
  public function setConfiguration($v){
    $this->configurationMain = $v;
  }
  private function createReceiveProcess($arr){
    try{
      $newarr = array();
      foreach($arr as $k=>$v){
        $newarr[$k] = array('name'=> $v,'process'=>false,'finalizado' => false);
      }
      if(!file_put_contents($this->receiveProcessPath,json_encode($newarr))){
        throw new Exception("No se creo el archivo del proceso de receive");
      }
      return true;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  
  private function getListRepository(){
    try{
      $this->historialSyncObj = null;
      $this->configurationMain = (is_null($this->configurationMain)) ? toolsObj::getMainConfig(SYNC_CONFIG_PATH."config.php") : $this->configurationMain;
      $isssl = strpos($configMain['target'],'https') !== false ? false : true;
      $result = toolsObj::requestHTTP($this->configurationMain['target']."sync/envios/listsync",array('key'=>md5("getListRepository")),$isssl);
      if($result != true){
        $jsonresult = json_decode($result,true);
        if($jsonresult['code'] == 'ok' && !empty($jsonresult['data'])){
          // al recibir la lista comparar el historial de sincronizacion local
          $listaRecibida = $result['data'];
          $nuevoarr = array();

          if(!is_file($this->historialSyncPath)){
            $nuevoarr = $result['data'];
          }else{
            $_json = file_get_contents($this->historialSyncPath);
            $nuevoarr = json_decode($_json,true);
            //busqueda
            $i = 0; $cc = count($result);
            foreach($listaRecibida as $keys => $value){
              $searc_key = array_search($value,$nuevoarr);
              if($searc_key === false){
                $nuevoarr[$keys]=$value;
              }
            }
          }//end if
          if(!empty($nuevoarr)){
            //ver si es necesario crear un archivo local
            ksort($nuevoarr);
          }
          $this->itemstodownload = $nuevoarr;
          $historySync = $this->itemstodownload;

          // $this->historialSyncObj = $historySync;
          toolsObj::updateFile($this->historialSyncPath,$historySync);
          
          return true;
        }//end if validacion code = ok
      }//end if resultado del request
      return false;
    }catch(Exception $e){
      printf(" error en el request ,%s",$e->getMessage());
      return false;
    }
  }
  private function downloadFile($name){
    try{
      $this->configurationMain = (is_null($this->configurationMain)) ? toolsObj::getMainConfig(SYNC_CONFIG_PATH."config.php") : $this->configurationMain;
      $isssl = strpos($configMain['target'],'https') !== false ? false : true;
      $r = toolsObj::requestHTTP($this->configurationMain['target']."sync/envios/getfile",array('key'=>md5("downloadFile"),'name'=>$name),$isssl);
      if($r != true){
        $jsonresult = json_decode($result,true);
        if($jsonresult['code'] == 'ok'){
          $base64_decode = base64_decode($result['data']);
          if(!file_put_contents($this->receivePath.$name,$base64_decode)){
            return false;
          }
          //send request to sync = true
          // $r = toolsObj::requestHTTP($this->configurationMain['target']."sync/envios/setstatus",array('key'=>md5("setstatus"),'status'=>true,'name'=>$name,'idlocal'=>$this->configurationMain['local']),$isssl);
          // if($r != true){
          //   return false;
          // }
          return true;
        }
      }
      return false;
    }catch(Exception $e){
      printf(" error en el request ,%s",$e->getMessage());
      return false;
    }
  }
  private function clearHistorialOffline($historySync){
    try{
      $newarr = array();
      if(!empty($historySync)){
        $i = 0; $cc = count($historySync);
        do{
          if($historySync[$i]['sync'] !== true){
            $newarr[] = $historySync[$i];
          }
          ++$i;
        }while($i < $cc);
      }//end if empty
      return $newarr;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  public function requestfilesAction(){
    try{
      
      // enviar peticion y recibir la lista de nombres de carpetas a sincronizar
      $this->getListRepository();
      // ejecutar el historial los archivos aun no procesados
      if(!empty($this->itemstodownload)){
        foreach($this->itemstodownload as $k => $v){
          if(!$this->downloadFile($v)){
            printf("el archivo %s no se logro descargar",$v);
          }
        }
        return true;
      }
      return false;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function verificarAction($offline = false){
    try{
      $offline = boolval($offline);
      if(count(glob($this->receivePath."*.zip")) !== 0){
        
        $arr = ($offline == true) ? toolsObj::listdir_by_date($this->receivePath."*.zip",20) : toolsObj::listdir_by_date($this->receivePath."*.zip");
        if($offline == true){
          $log = toolsObj::getFile($this->historialSyncPath);
          if(!empty($log) && !empty($arr)){
            foreach($arr as $keyfile => $namefile){
              $search_array = array_search($namefile,$log);
              if($search_array !== false){
                unset($arr[$keyfile]);
              }
            }//end foreach
          }//end if
        }
        if(!is_file($this->receiveProcessPath)){
          $this->createReceiveProcess($arr);
          if(!empty($arr)){
            foreach($arr as $k => $v){
              //descomprimir archivos
              if($this->descompress($v)){
                $__name = substr($v,0,strpos($v,'.zip'));
                if($this->processFiles($__name) == true){
                  if($offline == true){
                    $log[] = $v;
                  }//end if offline
                  if(!toolsObj::deleteDirectory($this->receivePath.$__name)){
                    throw new Exception("por ahora tiramos error de que no se borra la carpeta");
                  }
                  if(!unlink($this->receivePath.$v)){
                    throw new Exception("por ahora tiramos error de que no se borra el archivo zip");
                  }
                }else{
                  if(!toolsObj::deleteDirectory($this->receivePath.$__name)){
                    throw new Exception("por ahora tiramos error de que no se borra la carpeta");
                  }
                }
              }
            }//endforeach
            if($offline == true){
              if(count($log) > 20){
                $contador = 1;
                $arrayBackup = array();
                for($indice = (count($log) -1); $indice >= 0; $indice--){
                    if($contador > 20){
                        break;
                    }
                    $arrayBackup[] = $log[$indice];
                    ++$contador;
                }
                $log = $arrayBackup;
              }
              toolsObj::updateFile($this->historialSyncPath,$log);
            }//end if offline 
          }//end if empty
          if(!unlink($this->receiveProcessPath)){
            throw new Exception("no se pudo eliminar el proceso de recividor");
          }
          if($offline == true){
            $archives = toolsObj::listdir_by_date($this->receivePath."*.zip");
            foreach($archives as $filename){
              @unlink($this->receivePath.$filename);
            }
          }
          return true;
        }//end if check receive process              
      }//endif count glob
      return false;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function descompress($name){
    try{
      $zip = new ZipArchive;
      $res = $zip->open($this->receivePath.$name);
      if ($res === TRUE) {
          $zip->extractTo($this->receivePath);
          $zip->close();
          return true;
      } else {
        printf("error en apertura de archivo zip: %s", $res);
        return false;
      }
      return false;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function uncompress($dir){
    try{
      if(!is_file($dir)){
        return false;
      }
      $fstream = file_get_contents($dir);
      $funcompress = gzuncompress($fstream);
      return $funcompress;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function verifyValuesUpdates($campos){
      $indice = (count($campos) - 1);

      $continuar = true;

      while($continuar){
        if($indice < 0){
            $continuar = false;
            break;
        }
        $busqueda = strpos($campos[$indice],"=");
        if($busqueda === false){
            $saveIndice = $indice;
            $cadena = '';
            --$indice;
            while($indice >= 0){
              $_s = strpos($campos[$indice],"=");
              if($_s !== false){
                  if(is_numeric(substr($campos[$indice],($_s+1))) === true || strpos(substr($campos[$indice],($_s+1)),"'") !== false ){
                    //unir
                    $_cadena = explode(",",rtrim($cadena,","));
                    $_cadena = array_reverse($_cadena);
                    $cadena = implode(",",$_cadena);
                    $cadena .= ",";
                    $cadena .= $campos[$saveIndice];
                    $cadena = rtrim($cadena);
                    $campos[$saveIndice] = $campos[$indice].$cadena;
                    unset($campos[$indice]);
                    $arrayNuevo =array();
                    foreach($campos as $v){
                        $arrayNuevo[] = $v;
                    }
                    $campos = $arrayNuevo;
                    break;
                  }else{
                    $cadena .= $campos[$indice].",";
                    unset($campos[$indice]);
                  }
              }else{
                  $cadena .= $campos[$indice].",";
                  unset($campos[$indice]);
              }
              --$indice;
            }//end while busqueda
            $indice = (count($campos) - 1);

        }
        --$indice;
      }
      return $campos;
  }
  private function verifyValues($values){
    $continuar = true;
    $indice = 0;
    $contador = count($values);
    while($continuar){
    if($indice >= ($contador)){
          $continuar = false;
          break;
    }
    //iniciar comprobacion
    if(is_numeric($values[$indice]) == false){
        //hacer no es numerico comprobar string
        if(substr($values[$indice],0,1)  == "'"){
          if(substr($values[$indice],-1) != "'"){
                //hacer la movida
                $saveIndice = $indice;
                $cadena = '';
                ++$indice;
                while($indice < count($values)){
                  if(substr($values[$indice],-1) == "'"){
                    $cadena .= $values[$indice];
                    $values[$saveIndice] = $values[$saveIndice].$cadena;
                    unset($values[$indice]);
                    $arrayNuevo =array();
                    foreach($values as $v){
                      $arrayNuevo[] = $v;
                    }
                    $values = $arrayNuevo;
                    
                    break;
                  }else{
                    $cadena .= $values[$indice].",";
                    unset($values[$indice]);
                    $indice = $saveIndice;
                    $arrayNuevo =array();
                    foreach($values as $v){
                      $arrayNuevo[] = $v;
                    }
                    $values = $arrayNuevo;
                  }
                  ++$indice;
                }//end while
                $indice = $saveIndice;
                $contador = count($values);
          }
        }//endif comprobaciones
    }//endif numeric
    ++$indice;
    }//endwhile
    return $values;
  }
  private function updateToInsert($table,$sqlUpdate){
    try{
      $part = explode('WHERE',$sqlUpdate);
      $part2 = explode('SET',$part[0]);
      $campos = explode(',',$part2[1]);
      $indice = (count($campos) - 1);

      $continuar = true;

      while($continuar){
        if($indice < 0){
            $continuar = false;
            break;
        }
        $busqueda = strpos($campos[$indice],"=");
        if($busqueda === false){
            $saveIndice = $indice;
            $cadena = '';
            --$indice;
            while($indice >= 0){
              $_s = strpos($campos[$indice],"=");
              if($_s !== false){
                  if(is_numeric(substr($campos[$indice],($_s+1))) === true || strpos(substr($campos[$indice],($_s+1)),"'") !== false ){
                    //unir
                    $_cadena = explode(",",rtrim($cadena,","));
                    $_cadena = array_reverse($_cadena);
                    $cadena = implode(",",$_cadena);
                    $cadena .= ",";
                    $cadena .= $campos[$saveIndice];
                    $cadena = rtrim($cadena);
                    $campos[$saveIndice] = $campos[$indice].$cadena;
                    unset($campos[$indice]);
                    $arrayNuevo =array();
                    foreach($campos as $v){
                        $arrayNuevo[] = $v;
                    }
                    $campos = $arrayNuevo;
                    break;
                  }else{
                    $cadena .= $campos[$indice].",";
                    unset($campos[$indice]);
                  }
              }else{
                  $cadena .= $campos[$indice].",";
                  unset($campos[$indice]);
              }
              --$indice;
            }//end while busqueda
            $indice = (count($campos) - 1);

        }
        --$indice;
      }

      $_campos = array();
      $values = array();
      foreach($campos as $key => $valores){
        $position = strpos($valores,"=");
        if($position !== false){
            $position +=  1;
            $values[] = ltrim(substr($valores,$position));
            $_campos[]= trim(substr($valores,0,($position-1)));
        }
      }
      if(empty($_campos) || empty($values)){
        throw new Exception("estan vacios los campos o valores");
      }
      if(count($_campos) != count($values)){
        throw new Exception("no son iguales los arrays en update to insert");
      }
      $_campos = implode(",",$_campos);
      $values = implode(",",$values);
      $sql = "INSERT INTO {$table} ({$_campos}) VALUES ({$values})";      
      // $sql = (mb_detect_encoding($sql['statement'],"UTF-8",true) != "UTF-8" || mb_detect_encoding($sql['statement'],"UTF-8",true) == false) ? utf8_encode($sql['statement']) : utf8_decode($sql['statement']);
      return $sql;
    }catch(Exception $e){
      printf("error en la conversion updatotoinsert : %s", $e->getMessage());
      return false;
    }
  }
  private function insertToUpdate($table,$sqlInsert,$idcheck,$wheres,$name){
    try{
      
      $part = explode('VALUES',$sqlInsert);
      $campos = substr($part[0],strpos($part[0],'(')+1);
      $campos = rtrim($campos);
      $campos = rtrim($campos,')');
      $campos = explode(',',$campos);

      $values = substr($part[1],strpos($part[1],'(')+1);
      $values = rtrim($values,'');
      $values = rtrim($values,')');
      //dividir los valores por las comillas simples...
      $values = explode(",",$values);

      $values = $this->verifyValues($values);

      if(empty($campos) || empty($values) || empty($wheres)){
        throw new Exception("estan vacios los campos o valores");
      }
      
      $i = 0; $c = count($campos);
      if($c != count($values)){        
        throw new Exception("no son iguales los arrays al cambiar insert to update");
      }
      $sets = array();
      
      do{
        $sets[] = "{$campos[$i]} = {$values[$i]}";
        ++$i;
      }while($i < $c);
      $_sets = implode(',' ,$sets);
      
      $_wheres = implode(' AND ',$wheres);
      $sql = array('statement'=>"UPDATE {$table} SET {$_sets} WHERE {$_wheres}",'idcheck' => $idcheck);
      $sql['statement'] = (mb_detect_encoding($sql['statement'],"UTF-8",true) != "UTF-8" || mb_detect_encoding($sql['statement'],"UTF-8",true) == false) ? utf8_encode($sql['statement']) : utf8_decode($sql['statement']);
      //get file
      if(is_file($this->receivePath.$name.DIRECTORY_SEPARATOR."u_{$table}.txt")){
        $fjson = $this->uncompress($this->receivePath.$name.DIRECTORY_SEPARATOR."u_{$table}.txt");
        $jsoArr = json_decode($fjson,true);
        if(!is_array($jsoArr)){
          $jsoArr = stripslashes($jsoArr);
          $jsoArr = json_decode($jsoArr,true);
        }
        $jsoArr['sql'][]=$sql;
        //$jsoArr['sql']=array($sql);
      }else{
        
        //make file
        //check relacion
        $_rules = toolsObj::getReglas($this->tablerulesPath);
        
        $id = $_rules[$table]['idUnicos'];
        if(is_array($_rules[$table]['relacion']) && !empty($_rules[$table]['relacion'])){
          $i = 0; $c = count($_rules[$table]['relacion']);
          $idRelacion = array();
          do{
            $idRelacion[] = array('nombre'=>$_rules[$table]['relacion'][$i],'idUnicos' => $_rules[$_rules[$table]['relacion'][$i]]['idUnicos']);
            ++$i;
          }while($i < $c);
        }
        $lista = array();
        $lista[] = $sql;
        $jsoArr = (isset($idRelacion) && !empty($idRelacion)) ? json_encode(array('idrel'=>$idRelacion,'id'=>$id,'sql'=> $lista)) : json_encode(array('id'=>$id,'sql'=> $lista));
      }
      //encript file
      toolsObj::encrypt($this->receivePath.$name.DIRECTORY_SEPARATOR."u_{$table}.txt",json_encode($jsoArr) );
      return true;
    }catch(Exception $e){
      printf("Error en el cambio: %s", $e->getMessage());
      return false;
    }
  }
  private function changeId($rules,$tablaPadre,$tablaActual,$sql,$idnuevo){
    $partes = explode('VALUES',$sql);
    $campos = substr($partes[0],strpos($partes[0],'(')+1);
    $campos = rtrim($campos,' ');
    $campos = rtrim($campos,')');
    $camposArr = explode(',',trim($campos));
    $indice = 0;
    for($in2= 0;$in2 < count($camposArr);++$in2){
      if(strcmp($rules[$tablaActual]['id'][$tablaPadre],$camposArr[$in2]) === 0){
        $indice = $in2;
        break;
      }
    }//end for
    $valores = substr($partes[1],strpos($partes[1],'(')+1);
    $valores = rtrim($valores,' ');
    $valores = rtrim($valores,')');
    $valoresArr = explode(',',$valores);
    
    $valoresArr = $this->verifyValues($valoresArr);

    $valoresArr[$indice] = $idnuevo;
    $_campos = implode(',',$camposArr);
    $_valor = implode(',',$valoresArr);
    return "INSERT INTO {$tablaActual} ({$_campos}) VALUES ({$_valor})";
  }
  private function changeIdUpdate($rules,$tablaPadre,$tablaActual,$sql,$idnuevo){
    $partes = explode('SET',$sql);
    $partes2 = explode('WHERE',$partes[1]);
    $sets = explode(',',$partes2[0]);

    $sets = $this->verifyValuesUpdates($sets);
    //recorrer el ciclo, cambiar el id y luego reconstruir el string
    if(!empty($sets)){
      $i = 0; $cc = count($sets);
      do{
        $prepare = explode('=',$sets[$i]);
        if(strcmp(trim($prepare[0]),$rules[$tablaActual]['id'][$tablaPadre]) === 0){
          $prepare[1] = $idnuevo;
          $sets[$i] = implode(' = ',$prepare);
          break;
        }
        ++$i;
      }while($i < $cc);
    }
    $sets = implode(',',$sets);
    return "UPDATE {$tablaActual} SET {$sets} WHERE {$partes2[1]}";
  }
  private function syncRelInsert($table,$arr,$relation,$name){
    try{
      //sincronizar primero la tabla padre
      $rules = toolsObj::getReglas($this->tablerulesPath);
      $i = 0; $c = count($arr['sql']);
      $list = array();
      do{
        $wheres = array();
        foreach($arr['sql'][$i]['idcheck'] as $k => $v){
          $wheres[] = "{$k} = {$v}";
        }
        //verificar existencia
        $query = dbObj::existRow($table,$wheres);
        $bolean = boolval($query);
        if(!$bolean){
          //ejecutar
          $list[] = array('idChecks' => $arr['sql'][$i]['idcheck'],'sql' => $arr['sql'][$i]['statement']);
        }else{
          //cambio a updates
          //$this->insertToUpdate($table,$arr['sql'][$i]['statement'],$arr['sql'][$i]['idcheck'],$wheres,$name);
        }
        ++$i;
      }while($i < $c);
      if(!empty($list)){
        $valueIns = dbObj::InsertTransacRel($table,$rules[$table]['miId'],$list);
        if(!$valueIns){
          throw new Exception("no se logro completar la insersion por lotes");
        }
      }
      //seguir con los hijos en orden de las reglas de la tablas (tablesrules)
      //verificar si existe el archivo TXT de insertar de la tabla que se relacionan
      $updatesWithChange = array();
      $updateNormal = array();
      for($i = 0; $i < count($relation); ++$i){
        //$updatesWithChange[$relation[$i]['nombre']] = array();
        //$updateNormal[$relation[$i]['nombre']][] = array();
        $obj2 = $rules[$relation[$i]['nombre']]['idesRel'][$table];
        $max = count($obj2);
        // si no existe archivo entonces procesar la tabla como proceso normal con verificacion 
        if(count(glob($this->folderActual."{$relation[$i]['nombre']}.txt")) !== 0){
          $rfile =  toolsObj::descrypt($this->folderActual."{$relation[$i]['nombre']}.txt");
          $rjfile = json_decode($rfile,true);
          //recorrer registros para encontrar el id viejo y reemplazar el id nuevo
          if(empty($rjfile)){
            throw new Exception("no se logro realizar la conversion de string a json");
          }
          for($j = 0; $j < count($rjfile['sql']); ++$j){
            if(!empty($rjfile['sql'][$j])){
                $isNormal = true;
                $objactual = $rjfile['sql'][$j]['idcheck'];
                if(!empty($valueIns)){
                  for($jp = 0; $jp < count($valueIns);++$jp){
                    $contador = 0;
                    $isNormal = true;
                    foreach($obj2 as $k => $v){
                      if($objactual[$v] == $valueIns[$jp]['idChecksPadre'][$k]){
                        ++$contador;
                      }//endif
                      if($contador == $max){
                        $updatesWithChange[$relation[$i]['nombre']][] = array('sql'=>$rjfile['sql'][$j]['statement'],'idnuevo'=>$valueIns[$jp]['idnuevo']);
                        $isNormal = false;
                        break 2; /*break 1;*/
                      }
                    }//end foreach
                  }//end for jp
                }//end if diferente de empty valueIns
                if($isNormal){
                  $updateNormal[$relation[$i]['nombre']][] = array('sql'=>$rjfile['sql'][$j]['statement'],'idcheck'=>$rjfile['sql'][$j]['idcheck']);
                }
            }//end if !empty
          }//end foreach recorrido del array
          
          //aquellos registro no encontrados, enviar al proceso normal (DESMENUSAR LAS SENTENCIAS)
          if(!empty($updatesWithChange)){
            //recorrer el arreglo y cambiar el id
            $list_upwchange = array();
            $in = 0; $cin = count($updatesWithChange);
            foreach($updatesWithChange as $ktable => $vtable){
              foreach($vtable as $kk => $vv){
                // $partes = explode('VALUES',$vv['sql']);
                // $campos = substr($partes[0],strpos($partes[0],'('+1));
                // $campos = rtrim($campos,')');
                // $camposArr = explode(',',trim($campos));
                // $indice = 0;
                // for($in2= 0;$in2 < count($camposArr);++$in2){
                //   if(strcmp($rules[$ktable]['id'][$table],$camposArr[$in2]) === 0){
                //     $indice = $in2;
                //     break;
                //   }
                // }//end for
                // $valores = substr($partes[1],strpos($partes[1],'('+1));
                // $valores = rtrim($valores,')');
                // $valoresArr = explode(',',$valores);
                // $valoresArr[$indice] = $vv['idnuevo'];
                // $_campos = implode(',',$camposArr);
                // $_valor = implode(',',$valoresArr);
                // $list_upwchange[] = "INSERT INTO {$ktable} ({$_campos}) VALUES ({$_valor})";
                $list_upwchange[] = $this->changeId($rules,$table,$ktable,$vv['sql'],$vv['idnuevo']);
              }//end foreach second
              
            }//end foreach
              //luego ejecutar las sentencias
            if(!empty($list_upwchange)){
              if(dbObj::InsertTransac($list_upwchange)){
                unset($updatesWithChange);
              }
            }

          }//end if update with change
          //procesar normal los registros encontrando el id actual en la bd
          if(!empty($updateNormal)){
            foreach($updateNormal as $key_normal => $valor_normal){
              foreach($valor_normal as $kkn => $vvn){
                $wheres = array();
                foreach($rules[$key_normal]['idesRel'][$table] as $llaverules => $valuerules){
                  $wheres[] = "{$llaverules} = {$vvn['idcheck'][$valuerules]}";
                }
                //verificar existencia
                // de encontrar reemplazar el id actual con el de la sentencia
                // de no encontrar entonces ignorarlo..
                $orden = (strcmp($table,'bitacora_alumno_smartbook') === 0) ? "ORDER BY regfecha LIMIT 1" : null;
                $query = dbObj::existRowWithId($table,$wheres,$orden);
                if($query != false){
                  $list_upnormal[] = $this->changeId($rules,$table,$key_normal,$vvn['sql'],$query[$rules[$table]['miId']]);                
                }
              }//end foreach valor_normal
            }//end foreach update normal
            if(!empty($list_upnormal)){
              if(dbObj::InsertTransac($list_upnormal)){
                unset($updateNormal);
              }
            }
          }//end if update with normal
        }//end if count glob
      }//end for
      return true;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function syncSigleInsert($table,$arr,$name){
    try{
      //'idrel'=>$idRelacion,'id'=>$id,'sql'=> $lista
      //llamar la reglas
      $rules = toolsObj::getReglas($this->tablerulesPath);
      $i = 0; $c = count($arr['sql']);
      $list = array();
      do{
        $wheres = array();
        foreach($arr['sql'][$i]['idcheck'] as $k => $v){
          $wheres[] = "{$k} = {$v}";
        }

        //verificar existencia
        $query = dbObj::existRow($table,$wheres);
        $bolean = boolval($query);
        if(!$bolean){
          //ejecutar
          if(isset($rules[$table]['idesRel']) && !empty($rules[$table]['idesRel'])){
            foreach($rules[$table]['idesRel'] as $tableFather => $vtableFather){
              $_wheres2 = array();
              foreach($vtableFather as $keyN => $valN){
                $_wheres2[] = "{$keyN} = {$arr['sql'][$i]['idcheck'][$valN]}";
              }
              $orden = (strcmp($tableFather,'bitacora_alumno_smartbook') === 0) ? "ORDER BY regfecha LIMIT 1" : null;
              $query = dbObj::existRowWithId($tableFather,$_wheres2,$orden);
              if($query != false){
                $list[] = $this->changeId($rules,$tableFather,$table,$arr['sql'][$i]['statement'],$query[$rules[$tableFather]['miId']]);                
              }
            }
          }else{
            $list[] = $arr['sql'][$i]['statement'];
          }
        }else{
          //cambio a updates
          $this->insertToUpdate($table,$arr['sql'][$i]['statement'],$arr['sql'][$i]['idcheck'],$wheres,$name);
          
        }
        ++$i;
      }while($i < $c);
      //ejecutar en transaccion
      if(!empty($list)){
        if(!dbObj::InsertTransac($list)){
          throw new Exception("no se logro completar la insersion por lotes");
        }
      }else{
        return false;
      }
      return true;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function syncSigleUpdate($table,$arr){
    try{
      $i = 0; $c = count($arr['sql']);
      $rules = toolsObj::getReglas($this->tablerulesPath);
      $list = array();
      do{
        $wheres = array();
        foreach($arr['sql'][$i]['idcheck'] as $k => $v){
          $wheres[] = "{$k} = {$v}";
        }
        //verificar existencia
        $query = dbObj::existRow($table,$wheres);
        $bolean = boolval($query);
        if($bolean){
          //ejecutar
          if(isset($rules[$table]['idesRel']) && !empty($rules[$table]['idesRel'])){
            foreach($rules[$table]['idesRel'] as $tableFather => $vtableFather){
              $_wheres2 = array();
              foreach($vtableFather as $keyN => $valN){
                $_wheres2[] = "{$keyN} = {$arr['sql'][$i]['idcheck'][$valN]}";
              }
              $orden = (strcmp($tableFather,'bitacora_alumno_smartbook') === 0) ? "ORDER BY regfecha LIMIT 1" : null;
              $query = dbObj::existRowWithId($tableFather,$_wheres2,$orden);
              if($query != false){
                $list[] = $this->changeIdUpdate($rules,$tableFather,$table,$arr['sql'][$i]['statement'],$query[$rules[$tableFather]['miId']]);                
              }
            }
          }else{
            $busqueda_id = substr($arr['sql'][$i]['statement'],strpos($arr['sql'][$i]['statement'],$rules[$table]['miId']));
            $posi = strpos($busqueda_id,',');
            if($posi === false){
              $posi = strpos($busqueda_id,'WHERE');
              $posi = $posi - 1;
            }else{
              $posi = $posi + 1;
            }
            if($posi !== false){
              $busqueda_id = substr($busqueda_id,0,$posi);
              $exxplode = explode($busqueda_id,$arr['sql'][$i]['statement']);
              $list[] = "{$exxplode[0]}{$exxplode[1]}";
            }
          }
        }else{
          //comenzar de update to insert
          //chequear si existe relacion
          $updater = $arr['sql'][$i]['statement'];
          if(isset($rules[$table]['idesRel']) && !empty($rules[$table]['idesRel'])){
            //busqueda de relacion
            $sql = "";
            $query = dbObj::query($sql);
            //obtener el id de la otra tabla e insertarlo en la que se genera
            
          }
          //generar la consulta insert a traves de un insert
          $list[] = $this->updateToInsert($table,$updater);
        }
        ++$i;
      }while($i < $c);
      // ejecutar transaccion/update
      if(!empty($list)){
        if(!dbObj::InsertTransac($list)){
          throw new Exception("no se logro completar la actualizacion por lotes");
        }
      }
      return true;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  private function processFiles($name){
    try{
      //u_
      $this->folderActual = $this->receivePath.$name.DIRECTORY_SEPARATOR;
      if(count(glob($this->folderActual."*.txt")) !== 0){
        
        $updateList = array();
        $ignoreTable = array();
        //procesar insert
        //$arr = toolsObj::listdir_by_date($this->sendContainer.$name."*.txt");
        $arr = toolsObj::listdir($this->receivePath.$name.DIRECTORY_SEPARATOR."*.txt");
        if(!is_file($this->tablesToSyncPath)){
          throw new Exception("no existe la regla entre tablas");
        }
        $orden = @include($this->tablesToSyncPath);
        $i=0;$c=count($orden);
        do{
          if(in_array($orden[$i],$ignoreTable)){
            ++$i;
            continue;
          }
          
          $search = array_search("{$orden[$i]}.txt",$arr);
          if($search !== false){
            $fjson = $this->uncompress($this->receivePath.$name.DIRECTORY_SEPARATOR.$arr[$search]);
            
            if(empty($fjson) || $fjson == false ){
              throw new Exception("el archivo no se descomprime correctamente al formato original");
            }
            $fileArray = json_decode($fjson,true);
            
            if(isset($fileArray['idrel']) && !empty($fileArray['idrel'])){
              //$orden[$i]
              if($this->syncRelInsert($orden[$i],$fileArray,$fileArray['idrel'],$name)){
                $ignoreTable = array_merge($ignoreTable,$fileArray['idrel']);
              }
            }else{
              //procesar normal
              $this->syncSigleInsert($orden[$i],$fileArray,$name);
              $ignoreTable[] =$orden[$i];
            }
          }
          ++$i;
        }while($i < $c);
        //procesar update
        $i=0;
        $ignoreTable=array();
        do{
          if(in_array($orden[$i],$ignoreTable)){
            ++$i;
            continue;
          }
          $search = array_search("u_{$orden[$i]}.txt",$arr);
          if($search !== false){
            $fjson = $this->uncompress($this->receivePath.$name.DIRECTORY_SEPARATOR.$arr[$search]);
            if(empty($fjson) || $fjson == false ){
              throw new Exception("el archivo no se descomprime correctamente al formato original");
            }
            $fileArray = json_decode($fjson,true);
            // if(isset($fileArray['idrel']) && !empty($fileArray['idrel'])){
            //   if($this->syncRelUpdate($orden[$i],$fileArray,$fileArray['idrel'])){
            //     $ignoreTable = array_merge($ignoreTable,$fileArray['idrel']);
            //   }
            // }else{
              //procesar normal
              $this->syncSigleUpdate($orden[$i],$fileArray);
              $ignoreTable[] =$orden[$i];
            // }
          }
          ++$i;
        }while($i < $c);
        /**
         * falta borrar los archivos .json ya sincronizados
         */
        return true;
      }//end if count glob txt
      return false;
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
  public function procesarAction(){
    try{
      //do it
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
}
?>