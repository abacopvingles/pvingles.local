<?php
set_time_limit(0);

class manualObj {
    protected $filename;
    protected $tablasToSyncPath;
    protected $containerPath;
    protected $rulesPath;
    protected $containerSendPath;
    protected $historialSyncPath;

    public function __construct(){
        $this->filename = "download_manual_file.zip";
        $this->tablasToSyncPath = SYNC_CONFIG_PATH."tablasToSync.php";
        $this->containerPath = SYNC_STORAGE_PATH."manual".DIRECTORY_SEPARATOR."ExportFile".DIRECTORY_SEPARATOR;
        $this->containerCargadosPath = SYNC_STORAGE_PATH."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
        $this->rulesPath = SYNC_CONFIG_PATH."tablesrules.php";
        $this->containerSendPath = SYNC_STORAGE_PATH."send".DIRECTORY_SEPARATOR;
        $this->receivePath = SYNC_STORAGE_PATH."receive".DIRECTORY_SEPARATOR;
        $this->historialSyncPath = SYNC_LOGS_PATH."receiveSyncList.json";
        $this->containerAudioPath = STATIC_FOLDER."media".DIRECTORY_SEPARATOR."record".DIRECTORY_SEPARATOR."audio".DIRECTORY_SEPARATOR;
        $this->containerRecordPath = STATIC_FOLDER."media".DIRECTORY_SEPARATOR."record".DIRECTORY_SEPARATOR;
    }
    private function comillar($numeros,$tipo,$value){
        $match = (str_replace($numeros, '', $tipo) != $tipo);
        if($match == true){
            $number = ($value == (int) $value) ? (int) $value : (float) $value;
            return strval($number);
        }else{
            $search = array("'",chr(145), chr(146),chr(133),chr(147),chr(148),chr(150),chr(151),chr(149),chr(132),chr(130),chr(152));
            $replace = array("\'","\'", "\'",'...','"','"','-','--','-','"',',','~'); 
            $escapado = str_replace($search,$replace,$value);
            return "'{$escapado}'";
        }
    }
    private function procesarValores($tabla,$objeto){
        try{
            $resultado = array();
            $sql = "SELECT t.DATA_TYPE AS tipo FROM information_schema.COLUMNS t WHERE TABLE_NAME = '{$tabla}' AND t.TABLE_SCHEMA = DATABASE()";
            $estructura = dbObj::query($sql,true);
            $numeros = array('int','bigint','float');
            $i = 0;
            $valorcomilladaFechaUpdate = (isset($objeto['fechamodificacion'])) ? $this->comillar($numeros,'string',$objeto['fechamodificacion']) : null;
            foreach($objeto as $key => $value){
                if(strcmp($key,'fechamodificacion')===0 || strcmp($key,'previamodificacion')===0){
                    $valueComillado = $valorcomilladaFechaUpdate;
                }else{
                    //hacer el reemplazo
                    if($tabla == 'notas_quiz' && strcmp($key,'preguntas')===0){
                        $value = str_replace('\\', '\\\\', $value);
                    }
                    $tipo =  $estructura[$i]['tipo'];
                    $valueComillado = $this->comillar($numeros,$tipo,$value);
                    $valueComillado = (mb_detect_encoding($valueComillado,"UTF-8",true) != "UTF-8" || mb_detect_encoding($valueComillado,"UTF-8",true) == false) ? utf8_encode($valueComillado) : utf8_decode($valueComillado);
                }
                $resultado[] = ($tabla == 'actividad_alumno' || $tabla == 'notas_quiz') ? preg_replace("/[\r\n|\n|\r]+/", " ", $valueComillado) : $valueComillado;

                ++$i;
            }
            return implode(',',$resultado);
        }catch(Exception $e){
            printf("error en el procesamiento de valores : %s",$e->getMessage());
            return false;
        }
    }
    private function exportTable($table){
        try{
            if(is_null($table)){
                throw new Exception("la tabla por parametro es nulo");
            }
            $rule = @include($this->rulesPath);
            if(empty($rule)){
                throw new Exception("la tabla de regla no se importo");
            }
            $rows = dbObj::getAllOrderBy($table,$rule[$table]['miId'],'DESC',0,10000);

            if(!empty($rows)){
                $i = 0; $c = count($rows);
                $campos = array();
                foreach($rows[0] as $llave => $valor){
                    $campos[] = $llave;
                }
                if(empty($campos)){
                    throw new Exception("campos vacios");
                }
                
                $campos = implode(',',$campos);
                $primero = true;
                $medidor = '';
                $maxSizePacket = ($table == 'actividad_alumno') ? 600000 : 5000000;
                $lines = array();
                $lines[] = "SET GLOBAL max_allowed_packet=250000000;\n";
                $lines[] = "TRUNCATE TABLE `{$table}`;\n";                    
                do{   
                    $values = $this->procesarValores($table,$rows[$i]);
                    if($primero == true || strlen($medidor) >= $maxSizePacket){
                        $lines[] = "INSERT INTO {$table} ({$campos}) VALUES ({$values}),\n";
                        $medidor = "INSERT INTO {$table} ({$campos}) VALUES ({$values}),";
                        $primero = false;
                    }else{
                        $medidor .= "({$values}),";
                        $lines[] = ($i == ($c - 1) || strlen($medidor) >= $maxSizePacket) ? "({$values});\n" : "({$values}),\n";
                    }
                    ++$i;
                }while($i < $c);
                //delete the "," and replace for ";"
                $ultimoCaracter = substr(trim($lines[count($lines) - 1]),(trim($lines[count($lines) - 1]) - 1));
                $lines[count($lines) - 1] = (strcmp($ultimoCaracter,',') === 0) ? substr(rtrim($lines[count($lines) - 1]),0, (rtrim($lines[count($lines) - 1]) - 1)).";" : $lines[count($lines) - 1];
                if(!file_put_contents($this->containerPath."{$table}.txt",$lines)){
                    throw new Exception("no se logro generar el archivo exportador");
                }

                return true;
            }
            return false;
        }catch(Exception $e){
            printf("error en la exportacion de la tablas : %s",$e->getMessage());
            return false;
        }
    }
    public function getFile(){
        try{
            $tablas = @include($this->tablasToSyncPath);

            if(empty($tablas)){
                throw new Exception("No se logro abrir el archivo de configuracion de las tablas a sync");
            }
            if(!is_dir($this->containerPath)){
                if(!mkdir($this->containerPath)){
                    throw new Exception("no se logro crear la carpeta contenedora");
                }
            }
            for($indice = 0; $indice < count($tablas); ++$indice){
                $this->exportTable($tablas[$indice]);
            }
            //compress
            if(count(glob($this->containerPath."*.txt")) !== 0){
                $zip = new ZipArchive();
                $zip->open($this->containerPath."syncmanual.zip",ZipArchive::CREATE | ZipArchive::OVERWRITE);
                $arr = explode(DIRECTORY_SEPARATOR,rtrim($this->containerPath,DIRECTORY_SEPARATOR));
                $option = array('add_path' => 'syncmanual'.DIRECTORY_SEPARATOR, 'remove_all_path' => TRUE);
                $zip->addGlob($this->containerPath.'*.txt',0,$option);
                $zip->close();
                if(!is_file($this->containerPath."syncmanual.zip")){
                    throw new Exception("Por alguna razon no se creo el archivo");
                }
                $lis=  toolsObj::listdir($this->containerPath."*.txt");
                foreach($lis as $kill){
                    if(!unlink($this->containerPath.$kill)){
                        throw new Exception("no se borro un archivo");
                    }
                }
                return "/sync/storage/manual/ExportFile/syncmanual.zip";
            }
            return false;
        }catch(Exception $e){
            throw new Exception("Error en el sync manual: {$e->getMessage()}");
        }
    }
    public function getAudio(){
        try{
            if(count(glob($this->containerAudioPath.'*')) !== 0){
                $zip = new ZipArchive();
                $zip->open($this->containerPath."syncaudiomanual.zip",ZipArchive::CREATE | ZipArchive::OVERWRITE);
                $option = array('add_path' => 'audio'.DIRECTORY_SEPARATOR, 'remove_all_path' => TRUE);
                $zip->addGlob($this->containerAudioPath.'*',0,$option);
                $zip->close();
                return "/sync/storage/manual/ExportFile/syncaudiomanual.zip";
            }
            return false;
        }catch(Exception $e){
            throw new Exception("Error en el getAudio: {$e->getMessage()}");
        }
    }
    public function syncAudio(){
        try{
            if(!is_file($this->containerCargadosPath."syncaudiomanual.zip")){
                throw new Exception("no hay archivo a sincronizar");
            }
            if(toolsObj::unzip($this->containerCargadosPath."syncaudiomanual.zip",$this->containerRecordPath)){
                return true;
            }
            return false;
        }catch(Exception $e){
            throw new Exception("error en la sincronizacion de audio: {$e->getMessage()}");
        }
    }
    public function zipdownloadforcedAction(){
        try{
            $storageBackup = SYNC_STORAGE_PATH."backup".DIRECTORY_SEPARATOR;
            $nameFolder = date('Y-m');
            $zip = new ZipArchive();
            //colocar los backup mensuales
            if(is_dir($storageBackup.$nameFolder) && count(glob($storageBackup.$nameFolder.DIRECTORY_SEPARATOR."*.zip")) !== 0){
                $listar = toolsObj::listdir_by_date($storageBackup.$nameFolder.DIRECTORY_SEPARATOR."*.zip");
                if($zip->open($this->containerSendPath.$this->filename, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {                    
                    foreach($listar as $value){
                        $zip->addFile($storageBackup.$nameFolder.DIRECTORY_SEPARATOR.$value,$value);
                    }
                    $zip->close();
                    $dir = "/sync/storage/send/".$this->filename;
                }
                if(empty($dir)){
                    throw new Exception("direccion no definido");
                }
                return $dir;
            }
            return false;
        }catch(Exception $e){
            printf($e->getMessage());
            return false;
        }
    }
    public function zipdownloadAction($last20 = false){
        try{

            if(count(glob($this->containerSendPath."*.zip")) !== 0){
                $dir = 0;
                $listar = ($last20 == false) ? toolsObj::listdir_by_date($this->containerSendPath."*.zip",20) : toolsObj::listdir_by_date($this->containerSendPath."*.zip");
                $compressObj = array();
                if(!empty($listar)){
                    $zip = new ZipArchive();
                    if($zip->open($this->containerSendPath.$this->filename, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
                        foreach($listar as $value){
                            if(strcasecmp($value, $this->filename) !== 0){
                                $zip->addFile($this->containerSendPath.$value,$value);
                            }
                        }
                        $zip->close();
                        $dir = "/sync/storage/send/".$this->filename;
                    }else{
                        throw new Exception("no se creo el archivo zip");
                    }
                }
                if(empty($dir)){
                    throw new Exception("direccion no definido");
                }
                return $dir;
            }
            return false;
        }catch(Exception $e){
            return false;
        }
    }
    
    public function deletemanualdownAction($force = false){
        try{
            if(is_string($force)){
                $force = (strcmp(strtolower($force),'false') === 0) ? false : true;
            }
            if($force){
                if(is_file($this->containerSendPath.$this->filename)){
                    if(!unlink($this->containerSendPath.$this->filename)){
                        throw new Exception("no se borro ".$this->filename);
                    }
                }
            }else{
                if(count(glob($this->containerSendPath."*.zip")) !==0){
                    $listar = toolsObj::listdir_by_date($this->containerSendPath."*.zip");
                    if(!empty($listar)){
                        foreach($listar as $value){
                            if(strcasecmp($value, $this->filename) !== 0){
                                if(!unlink($this->containerSendPath.$value)){
                                    throw new Exception("no se borro ".$value);
                                }
                            }
                        }//end foreach
                    }
                }
            }
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function deletemanualdownfileAction(){
        try{
            if(is_file($this->filename)){
                if(!unlink($this->filename)){
                    throw new Exception("no se logro borrar el archivo");
                }
            }
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function manualupfileAction(){
        try{
            if(empty($_FILES["archivo"]["name"])){
                throw new Exception("archivo no fue subido");
            }
            $source = $_FILES["archivo"]["tmp_name"]; //Obtenemos un nombre temporal del archivo
            $target_path = $this->receivePath.$this->filename;            
            if(!move_uploaded_file($source, $target_path)){	
                throw new Exception("Ha ocurrido un error, al momento de mover (subir) el archivo por favor, inténtelo de nuevo.");
            }
            echo json_encode(array('code'=>'ok','message'=>'file uploaded'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
    public function manualupfileaudioAction(){
        try{
            if(empty($_FILES["archivo"]["name"])){
                throw new Exception("archivo no fue subido");
            }
            $source = $_FILES["archivo"]["tmp_name"]; //Obtenemos un nombre temporal del archivo
            $target_path = $this->containerCargadosPath."syncaudiomanual.zip";            
            if(!move_uploaded_file($source, $target_path)){	
                throw new Exception("Ha ocurrido un error, al momento de mover (subir) el archivo por favor, inténtelo de nuevo.");
            }
            echo json_encode(array('code'=>'ok','message'=>'file uploaded'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
    public function unzipreceiveAction($offline = false){
        try{
            if(is_string($offline)){
                $offline = (strcmp(strtolower($offline),'false') === 0) ? !boolval($offline) : boolval($offline);
            }
            $unzip = false;
            if($offline == true){
                $historySync = array();
                $make = false;
                if(is_file($this->historialSyncPath)){
                    $_json = file_get_contents($this->historialSyncPath);
                    $historySync = json_decode($_json,true);
                }else{
                    $make = true;
                    $unzip = true;
                }
                $zip = new ZipArchive();
                $zip->open($this->receivePath.$this->filename);
                for($i = 0; $i < $zip->numFiles; $i++ ){ 
                    $stat = $zip->statIndex( $i ); 
                    $name = basename( $stat['name'] );
                    if($make == true){
                        $historySync[]=$name;
                    }else{
                        $searc_key = array_search($name,$historySync);
                        if($searc_key === false){
                            $unzip = true;
                            break;
                        }
                    }
                }
                $zip->close();
                if(count($historySync) > 20){
                    $contador = 1;
                    $arrayBackup = array();
                    for($indice = (count($historySync) -1); $indice >= 0; $indice--){
                        if($contador > 20){
                            break;
                        }
                        $arrayBackup[] = $historySync[$indice];
                        ++$contador;
                    }
                    $historySync = $arrayBackup;
                }
                toolsObj::updateFile($this->historialSyncPath,$historySync);
            }else{
                $unzip = true;
            }
            if($unzip == false){
                return false;
            }
            if(!toolsObj::unzip($this->receivePath.$this->filename,$this->receivePath)){
                throw new Exception("No se logro descomprimir el archivo");
            }
            if(!unlink($this->receivePath.$this->filename)){
                throw new Exception("No se elimino el archivo");
            }
                         
            return true;
        }catch(Exception $e){
            printf("Error de la sincronizacion: %s",$e->getMessage());
            return false;
        }
    }
    public function syncAction(){
        try{
            $search = SYNC_STORAGE_PATH."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR."*.zip";
            $contenedor = SYNC_STORAGE_PATH."manual".DIRECTORY_SEPARATOR."cargados".DIRECTORY_SEPARATOR;
            if(count(glob($search)) === 0){
                throw new Exception("No hay archivos subidos para sincronizar");
            }
            $files = toolsObj::listdir($search);
            if(!empty($files)){
                foreach($files as $key => $value){
                    $newContenedor = $contenedor.substr($value,0,strpos($value,'.zip')).DIRECTORY_SEPARATOR;
                    //descompress
                    if(toolsObj::unzip($contenedor.$value,$contenedor)){
                        
                        $file_data = toolsObj::listdir($newContenedor."*.txt");
                        foreach($file_data as $llave => $archivo){
                            if($archivo == 'bitacora_smartbook.txt'){
                                $contenido = file($newContenedor.$archivo);
                                $output = '';
                                $inserts = array();
                                foreach($contenido as $row)
                                {
                                    $start_character = substr(trim($row), 0, 2);
                                    if($start_character != '--' || $start_character != '/*' || $start_character != '//' || $row != '')
                                    {
                                        $output = $output . $row;
                                        $end_character = substr(trim($row), -1, 1);
                                        if($end_character == '$')
                                        {
                                            $inserts[] = substr($output,0, strlen(rtrim($output)) -1);
                                            $output = '';
                                        }
                                    }
                                }
                                // var_dump($inserts);
                                //transaccion
                                if(!empty($inserts)){
                                    dbObj::execute($inserts);
                                }
                                // $inicio_t = microtime(true);
                                // $fin_t = microtime(true);
                                // echo ($fin_t - $inicio_t);
                                // echo "<br>";
                                // exit();
                            }
                            
                        }//end if foreach archivos .txt
                    }//end if unzip
                }//end foreach files
            }
            
            echo json_encode(array('code'=>'ok','message'=>'complete'));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
}
?>