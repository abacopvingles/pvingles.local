<?php
include_once(SYNC_OBJ_PATH."connectionObj.php");

class dbObj extends connectionObj {
    public static function query($sql,$getresult = null){
        $mycon = (is_null(parent::$conn)) ? new connectionObj() : self::$conn;
        $result = self::$conn->query($sql, PDO::FETCH_ASSOC);
        if(!$result){
            throw new Exception("Error no se ejecuto la consulta");
        }
        if($getresult === true){
            $v = array();
            foreach($result as $row) {
                $v[] = $row;
            }
            return $v;
        }else{
            return true;
		}
		//sef::disconnect();
	}
	public static function getAll($tabla,$inicio = null,$fin = null){
		$sql = (is_null($inicio) && is_null($fin)) ? "SELECT * FROM {$tabla} LIMIT 0,100000" : "SELECT * FROM {$tabla} LIMIT {$inicio},{$fin}";
		return self::query($sql,true);
	}
	public static function getAllOrderBy($tabla,$campo,$order = 'DESC',$inicio = null,$fin = null){
		$sql = (is_null($inicio) && is_null($fin)) ? "SELECT * FROM {$tabla} ORDER BY {$campo} {$order} LIMIT 0,100000" : "SELECT * FROM {$tabla} ORDER BY {$campo} {$order} LIMIT {$inicio},{$fin}";
		return self::query($sql,true);
	}
	public static function existRow($tabla,$where){
		if(!is_string($where)){
			$where = implode(' AND ',$where);
		}
		$sql = "SELECT EXISTS(SELECT * from {$tabla} WHERE {$where}) AS result";
		$result = self::query($sql,true);
		return $result[0]['result'];
	}
	public static function existRowWithId($tabla,$where,$order = null){
		try{
			$resultado = false;
			if(!is_string($where)){
				$where = implode(' AND ',$where);
			}
			$sql = (!is_null($order)) ? "SELECT * FROM {$tabla} WHERE {$where} {$order}" : "SELECT * from {$tabla} WHERE {$where}";
			$result = self::query($sql,true);
			if(!empty($result)){
				$resultado = $result[0];
			}
			return $resultado;
		}catch(Exception $e){
			return false;
		}
	}
	public static function getLastId($table,$pk){

	}
	public static function InsertTransacRel($table,$pk,$list){
		//hacer relaciones en base de datos
		// insert normal con transaccion
		try{
			$mycon = (is_null(parent::$conn)) ? new connectionObj() : self::$conn;
			self::$conn->beginTransaction();
			$id_nuevos = array();
			$i=0;$c=count($list);
			do{
				self::$conn->exec($list[$i]['sql']);
				//$lastid =  self::$conn->lastInsertId($table);
				//$lastid = self::getLastId($table,$pk);
				$lastid = self::$conn->query("SELECT MAX(idbitacora_smartbook) as lastid FROM bitacora_alumno_smartbook",PDO::FETCH_ASSOC);
				$v = array();
				foreach($lastid as $row) {
					$v[] = $row;
				}
				$lastid = intval($v[0]['lastid']);
				$id_nuevos[] = array('idChecksPadre'=>$list[$i]['idChecks'],'idnuevo'=> $lastid);
				++$i;
			}while($i < $c);
			self::$conn->commit();
			return $id_nuevos;
		}catch(Exception $e){
			echo $pdoe->getMessage();
			self::$conn->rollBack();
			return false;
		}catch(PDOException $pdoe){
			self::$conn->rollBack();
			return false;			
		}
	}
	public static function InsertTransac($list){
		// insert normal con transaccion
		$saveSQL = false;
		try{
			$mycon = (is_null(parent::$conn)) ? new connectionObj() : self::$conn;
			self::$conn->beginTransaction();
			$i=0;$c=count($list);
			do{
				if(!empty($list[$i])){
					$list[$i] = toolsObj::desencrypt64SQL($list[$i],"ZW1teXNlY28=0x17_");
					$saveSQL = $list[$i];
					try{
						self::$conn->exec($list[$i]);
					}catch(PDOException $noerror){
						$code = intval($noerror->getCode());
						if($code == 23000){
							var_dump("esta pasando por duplicado");
							var_dump($saveSQL);
							var_dump($noerror->getMessage());
							// //procesar un nuevo id
							// $rules = toolsObj::getReglas(SYNC_CONFIG_PATH."tablesrules.php");
							// if(empty($rules)){
							// 	throw new Exception("las reglas no se cargaron");
							// }
							// $explode = explode('INSERT INTO',$saveSQL);
							// $tabla = trim(substr($explode[1],0,strpos($explode[1],'(')));
							// $part = explode('VALUES',$saveSQL);
							
							// $campos = substr($part[0],strpos($part[0],'(')+1);
      						// $campos = rtrim($campos);
      						// $campos = rtrim($campos,')');
							// $campos = explode(',',trim($campos));
							// $values = substr($part[1],strpos($part[1],'(')+1);
							// $values = rtrim($values,'');
							// $values = rtrim($values,')');
							// //dividir los valores por las comillas simples...
							// $values = explode(",",$values);
							// $continuar = true;
							// $indice = 0;
							// $contador = count($values);
							// while($continuar){
							// if($indice >= ($contador)){
							// 	$continuar = false;
							// 	break;
							// }
							// //iniciar comprobacion
							// if(is_numeric($values[$indice]) == false){
							// 	//hacer no es numerico comprobar string
							// 	if(substr($values[$indice],0,1)  == "'"){
							// 	if(substr($values[$indice],-1) != "'"){
							// 			//hacer la movida
							// 			$saveIndice = $indice;
							// 			$cadena = '';
							// 			++$indice;
							// 			while($indice < count($values)){
							// 			if(substr($values[$indice],-1) == "'"){
							// 				$cadena .= $values[$indice];
							// 				$values[$saveIndice] = $values[$saveIndice].$cadena;
							// 				unset($values[$indice]);
							// 				$arrayNuevo =array();
							// 				foreach($values as $v){
							// 				$arrayNuevo[] = $v;
							// 				}
							// 				$values = $arrayNuevo;
											
							// 				break;
							// 			}else{
							// 				$cadena .= $values[$indice].",";
							// 				unset($values[$indice]);
							// 				$indice = $saveIndice;
							// 				$arrayNuevo =array();
							// 				foreach($values as $v){
							// 				$arrayNuevo[] = $v;
							// 				}
							// 				$values = $arrayNuevo;
							// 			}
							// 			++$indice;
							// 			}//end while
							// 			$indice = $saveIndice;
							// 			$contador = count($values);
							// 	}
							// 	}//endif comprobaciones
							// }//endif numeric
							// ++$indice;
							// }//endwhile
							// //buscar el id
							// if(count($values) != count($campos)){
							// 	throw new Exception("no contienen la misma cantidad de campos y valores");
							// }
							// //obtener el id y posicion actual
							// if(!isset($tabla) || empty($tabla)){
							// 	throw new Exception("no se definio una tabla");
							// }
							// $searchId = array_search($rules[$tabla]['miId'],$campos);
							// if($searchId === false){
							// 	throw new Exception("No se consiguio el id");
							// }
							// $myid = $values[$searchId];

							// //busqueda de un id sin usar
							// $ciclo = true;
							// $id = $myid;
							// $inicio_time = microtime(true); 
							// while($ciclo){
							// 	$final_time = microtime(true);
							// 	if((floatval($final_time) - floatval($inicio_time)) > 200.0 ){
							// 		throw new Exception("tiempo excedido en el ciclo de busqueda de nuevo id");
							// 	}
							// 	$arreglo = array();
							// 	$arreglo[] = "{$rules[$tabla]['miId']} = {$id}";
							// 	$b = self::existRow($tabla,$arreglo);
								
							// 	if(boolval($b) == false){
							// 		$ciclo = false;
							// 		break;
							// 	}else{
									
							// 		$_ides = self::query("SELECT max({$rules[$tabla]['miId']}) AS idmaximo FROM {$tabla}",true);
									
							// 		if(!empty($_ides)){
							// 			$id = doubleval($_ides[0]["idmaximo"]) + 1;
							// 		}else{
							// 			$id++;
							// 		}
							// 	}
							// }
							// //reemplazar el id viejo con el id nuevo
							// $values[$searchId] = $id;
							// $campos = implode(",",$campos);
							// $values = implode(",",$values);
							// $nuevoSQL = "INSERT INTO {$tabla} ({$campos}) VALUES ({$values})";
							// self::$conn->exec($nuevoSQL);
						}else{
							throw new Exception($noerror->getMessage());
						}
					}
				}
				++$i;
			}while($i < $c);
			self::$conn->commit();
			return true;
		}catch(PDOException $pdoe){
			echo base64_encode($saveSQL);
			var_dump($saveSQL);			
			echo $pdoe->getMessage();
			self::$conn->rollBack();
			return false;			
		}catch(Exception $e){
			echo base64_encode($saveSQL);
			var_dump($saveSQL);
			echo $e->getMessage();
			self::$conn->rollBack();
			return false;
		}
	}
	public static function execute($list){
		$mycon = (is_null(parent::$conn)) ? new connectionObj() : self::$conn;
		$i=0;$c=count($list);
		$sentencia = '';
		$ejecutar = true;
		do{
			if(empty($sentencia)){
				$sentencia = $list[$i].";";
				$ejecutar = true;
			}else if (strlen($sentencia) < 268435457){
				$sentencia .= $list[$i].";";
				$ejecutar = true;
			}else{
				var_dump($sentencia);
				exit();
				self::$conn->exec($sentencia);
				$sentencia = '';
				$ejecutar = false;
			}
			++$i;
		}while($i < $c);
		if($ejecutar ==  true){
			echo base64_encode($sentencia);
			// self::$conn->exec($sentencia);
			var_dump("asdasds11122");
		}
		return true;
	}
	public static function searchInsert($tabla){
		$sql = "SELECT * FROM {$tabla} t WHERE t.previamodificacion IS NULL AND t.fechamodificacion IS NOT NULL";
		return self::query($sql,true);
	}
	public static function searchUpdate($tabla){
		$sql = "SELECT * FROM {$tabla} t WHERE t.fechamodificacion <> t.previamodificacion AND  t.previamodificacion IS NOT NULL";
		return self::query($sql,true);
	}
	public static function UpdateFecha($tabla,$idcamp,$ids){
		if(empty($ids)){
			return false;
		}
		$ids = implode(',',$ids);
		$sql = "UPDATE {$tabla} SET previamodificacion = CURRENT_TIME() WHERE {$idcamp} IN ({$ids})";
		return self::query($sql);
	}
    public static function escapar($valor, $comillar = true)
	{
		try {
			if(is_array($valor)) {
				throw new Exception('Valor incorrecto (Array)');
			}
			
			if(is_int($valor) || is_float($valor)) {
				return $valor;
			}
			
			if(is_null($valor)) {
				return "''";
			}
			
			
			$escape_ = addslashes($valor);
			
			if(true === $comillar) {
				return "'" . $escape_ ."'";
			} else {
				return $escape_;
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public static function getIIEE($idproyecto,$tabla = null){
		try{
			$sql = (is_null($tabla)) ? "SELECT * FROM `local` WHERE idproyecto = {$idproyecto}":"SELECT * FROM {$tabla} WHERE idproyecto = {$idproyecto}";
			return self::query($sql,true);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public static function tabla_info($tablas){
		try{
		  $sql = "SELECT I.TABLE_NAME, I.TABLE_ROWS, I.CREATE_TIME, I.UPDATE_TIME FROM information_schema.tables I WHERE table_schema = DATABASE() AND I.TABLE_NAME IN ('".$tablas."')";
		  return self::query($sql,true);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public static function getExistsTableUpdate($tabla){
		try{
			$sql = "SELECT EXISTS(SELECT * FROM {$tabla} WHERE fechamodificacion <> previamodificacion) as exist";
			$r = self::query($sql,true);  
			return (!empty($r)) ? boolval($r[0]['exist']) : false;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
}

?>