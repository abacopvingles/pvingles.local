<?php
class enviosObj{
    protected $sendContainer;
    protected $configMain;
    protected $listProcessPath;
    protected $processSendPath;
    
    public function __construct(){
        $this->sendContainer = SYNC_STORAGE_PATH."send".DIRECTORY_SEPARATOR;
        $this->configMain = SYNC_CONFIG_PATH."config.php";
        $this->listProcessPath = SYNC_LOGS_PATH."sendListProcess.json";
        $this->processSendPath = SYNC_LOGS_PATH."SendProcessActive.json";
    }

    public function validarAction(){
        try{
            if(!is_file($this->processSendPath)){

                if(count(glob($this->sendContainer."*")) !== 0){
                  //recorrer las carpetas y ver si estan vacias, de estarlo eliminar
                  
                  $dir = $this->sendContainer."*";
                  $sorted_array = toolsObj::listdir_by_date($dir); //$this->listdir_by_date($dir);
      
                  $toCompress = array();
                  foreach($sorted_array as $value){
                      if(is_dir($this->sendContainer.$value)){
                          if(count(glob($this->sendContainer.$value.DIRECTORY_SEPARATOR."*")) === 0){
                              rmdir($this->sendContainer.$value);
                          }else{
                              $toCompress[] = $this->sendContainer.$value;
                          }
                      }
                  }
                  if(!empty($toCompress)){
                      if($this->compress($toCompress)){
                          $this->send();
                      }
                  }
                  return true;
                }
            }//end if check if exist send process actived
          return false;
        }catch(Exception $e){
            throw new Exception($e->getMessage(),0301);
        }
    }
    public function compressAction(){
        echo "hola";
    }
    public function enviarAction(){
        $this->send();
    }
    public function listsyncAction(){
        try{
            $list = array();
            if(count(glob($this->sendContainer."*.zip")) !== 0){
                $list = toolsObj::listdir_by_date($this->sendContainer."*.zip",20);
            }
            echo json_encode(array('code'=>'ok','data'=>$list));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error', 'message'=>$e->getMessage()));
            exit(0);
        }
    }
    public function getfileAction(){
        try{
            //do it send to some side one file
            $key = (isset($_REQUEST['key'])) ? $_REQUEST['key'] : null;
            $filename = (isset($_REQUEST['name'])) ? $_REQUEST['name'] : null;
            $filepath = $this->sendContainer.$filename;
            $base64 = false;

            if(is_null($key) || is_null($filename)){
                throw new Exception("Campos no definidos");
            }
            if(!is_file($filepath)){
                $_path = substr($filepath,0,strpos($filepath,'.zip'));
                if(is_dir($_path)){
                    if(!toolsObj::makezip($_path)){
                        throw new Exception("No se logro crear el archivo");
                    }
                    if(!toolsObj::deleteDirectory($_path)){
                        throw new Exception("No se logro eliminar la carpeta del archivo");
                    }
                }else{
                    throw new Exception("No existe el archivo y carpeta");
                }
            }
            $filebitString = file_get_contents($filepath);
            $base64 = base64_encode($filebitString);

            echo json_encode(array('code'=>'ok','data'=>$base64));
            exit(0);
        }catch(Exception $e){
            echo json_encode(array('code'=>'error','message'=>$e->getMessage()));
            exit(0);
        }
    }
    public function preparezipAction(){
        try{
            if(!is_file($this->processSendPath)){
                $dir = $this->sendContainer."*";
                $toCompress = array();
                if(count(glob($dir,GLOB_ONLYDIR)) !== 0){
                    $sorted_array = toolsObj::listOnlyDir_by_date($dir);
                    foreach($sorted_array as $value){
                        if(is_dir($this->sendContainer.$value)){
                            if(count(glob($this->sendContainer.$value.DIRECTORY_SEPARATOR."*")) === 0){
                                rmdir($this->sendContainer.$value);
                            }else{
                                $toCompress[] = $this->sendContainer.$value;
                            }
                        }
                    }//end foreach
                    if(!empty($toCompress)){
                        $this->compress($toCompress);
                    }
                }
                if(is_file($this->processSendPath)){
                    if(!unlink($this->processSendPath)){
                        throw new Exception("No se pudo eliminar el archivo de proceso");
                    }
                }
                return true;
            }//end if check if exist send process active
            return false;
        }catch(Exception $e){
            throw new Exception("no se logro prepara los zip");
        }
    }
    public function setstatusAction(){
        try{
            $idlocal = (isset($_REQUEST['idlocal'])) ? $_REQUEST['idlocal'] : null;
            $status = (isset($_REQUEST['status'])) ? boolval($_REQUEST['status']) : null;
            $filename = (isset($_REQUEST['name'])) ? boolval($_REQUEST['status']) : null;
            $historial = $this->sendContainer.$filename.DIRECTORY_SEPARATOR."historialIIEE.json";
            if(!is_dir($this->sendContainer.$filename)){
                throw new Exception("No se consigue la carpeta definida");
            }
            if(!is_file($historial)){
                throw new Exception("No se consigue el archivo historial de iiee");
            }
            $arrJson = json_decode(file_get_contents($historial),true);
            $index_search = array_search($idlocal, array_column($arrJson,'idlocal'));
            if($index_search === false){
                throw new Exception("No se consiguio el local en el archivo");
            }
            $arrJson[$index_search]['sync'] = true;

            toolsObj::updateFile($historial, $arrJson);
            //ver si se elimina el archivo y carpeta mencionado
            $index_search02 = array_search(false,array_column($arrJson,'sync'));
            if($index_search02 === false){
                if(!toolsObj::deleteDirectory($this->sendContainer.$filename)){
                    throw new Exception("no se elimino la carpeta {$filename}");
                }
                if(is_file($this->sendContainer.$filename.".zip")){
                    if(!unlink($this->sendContainer.$filename.".zip")){
                        throw new Exception("No se eliminio el archivo zip de {$filename}");
                    }
                }
            }
            return true;
        }catch(Exception $e){
            return json_encode(array('code'=>'error','message'=>$e->getMessage()));
        }
    }
    private function compress($compress){
        try{
            $zip = new ZipArchive();
            foreach($compress as $k => $v){
                $zip->open($v.".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE );
                $arr = explode(DIRECTORY_SEPARATOR,$v);
                $option = array('add_path' => end($arr).DIRECTORY_SEPARATOR, 'remove_all_path' => TRUE);
                $zip->addGlob($v.DIRECTORY_SEPARATOR.'*',0,$option);
                $zip->close();
                //enviar backup del contenerdor para los forzados
                $nameFolder = date('Y-m');
                if(!is_dir(SYNC_STORAGE_PATH."backup".DIRECTORY_SEPARATOR.$nameFolder)){
                    mkdir(SYNC_STORAGE_PATH."backup".DIRECTORY_SEPARATOR.$nameFolder,0777, true);
                }
                @copy($v.".zip",SYNC_STORAGE_PATH."backup".DIRECTORY_SEPARATOR.$nameFolder.DIRECTORY_SEPARATOR.end($arr).".zip");
                if(!toolsObj::deleteDirectory($v)){
                    throw new Exception("no elimino un directorio,{$v}");
                }
            }
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    private function createListProcess($arr){
        try{
            $newarr = array();
            foreach($arr as $k => $v){
                $newarr[$k] =array('name'=>$v,'sync' => false,'send'=>false);
            }
            $json = json_encode($newarr);
            if(!file_put_contents($this->listProcessPath,$json)){
                throw new Exception("no se logro crear el archivo del listado de procesos");
            }
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    private function actualizarListProcess($newArr){
        try{
            if(empty($newArr)){
                return false;
            }
            if(!file_put_contents($this->listProcessPath, json_encode($newArr))){
                return false;
            }
            return true;
        }catch(Exception $e){
            return false;
        }
    }
    
    private function send(){
        try{
            
            //identificar destino
            if(!is_file($this->configMain)){
                throw new Exception("No hay archivo de configuracion principal");
            }
            $configMain = @include($this->configMain);
            $sorted_array = array();
            if(count(glob($this->sendContainer."*.zip")) !== 0){
                $dir = $this->sendContainer."*.zip";
                $sorted_array = toolsObj::listdir_by_date($dir);
                if(!is_file($this->listProcessPath)){
                    $this->createListProcess($sorted_array);
                }
                $fListProcess = json_decode(file_get_contents($this->listProcessPath),true);
                
                foreach($sorted_array as $k => $v){
                    if($fListProcess[$k]['sync'] == false && $fListProcess[$k]['send'] == false){
                        $fListProcess[$k]['sync'] = true;
                        $this->actualizarListProcess($fListProcess); //importante actualizar para procesos anteriores
                        $post = file_get_contents($this->sendContainer.$v);
                        $base64 = array('file'=>base64_encode($post),'nombre'=>$v);
                        $ch = curl_init();
                        if(strpos($configMain['target'],'https') !== false){
                            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
                        }
                        curl_setopt($ch, CURLOPT_URL,$configMain['target']."sync/storage/receive/receive.php");
                        curl_setopt($ch, CURLOPT_POST,1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $base64);
                        $result=curl_exec($ch);
                        curl_close($ch);
                        // var_dump($result);
                        if($result == true){
                            //marcar archivo como enviado...
                            $fListProcess[$k]['send'] = true;
                            $this->actualizarListProcess($fListProcess); //importante actualizar para procesos anteriores
                            if(!unlink($this->sendContainer.$v)){
                                throw new Exception("No se pudo eliminar el archivo en el contenedore de envios");
                            }
                        }
                    }//end if listprocess
                }//end foreach
                if(is_file($this->processSendPath)){
                    if(!unlink($this->processSendPath)){
                        throw new Exception("No se pudo eliminar el archivo de proceso");
                    }
                }
                return true;
            }//end if count glob 
            $this->clearListProcess();

            return false;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    private function clearListProcess(){
        try{
            if(!is_file($this->listProcessPath)){
                return false;
            }
            $file = file_get_contents($this->listProcessPath);
            $jsonArr = json_decode($file,true);
            $newArr = array();
            foreach($jsonArr as $k => $v){
                if($v['sync'] == true && $v['send'] == true ){
                    continue;
                }
                $newArr[$k] = $jsonArr[$k];
            }
            $this->actualizarListProcess($newArr);
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }     
    }
}
?>