<?php
/**
 * @autor		Abel Chingo Tello : ACHT
 * @fecha		08/09/2016
 * @copyright	Copyright (C) 2016 Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();

class Sitio extends JrAplicacion{
	public static $msjs;
	private $BD = null;
	public function __construct()
	{
		parent::__construct();
	}
	
	public function iniciar($compat = array())
	{
		if(true === $this->inicio) {
			return;
		}		
		JrCargador::clase('jrAdwen::JrModulo', RUTA_LIBS, 'jrAdwen::');
		JrCargador::clase('sys_inc::ConfigSitio', RUTA_BASE, 'sys_inc::');
		JrCargador::clase('sys_datos::DatBase', RUTA_BASE, 'sys_datos');
		JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
		JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
		$oConfigSitio = ConfigSitio::getInstancia();
		parent::iniciar($oConfigSitio->get_());
	}	
	public function enrutar($rec = null, $ax = null)
 	{
		$usuarioAct = null;

		if(NegSesion::existeSesion()){
			$usuarioAct = NegSesion::getUsuario();
		}		
		$permitenologueado=array('aulavirtual', 'service','pronunciacion','proyecto','paginas','resumen','install');
		// session_destroy(); 
		/**Modificaciones para el historial de la plataforma virtual */
		/*if(isset($usuarioAct) && !empty($usuarioAct['idHistorialSesion'])){
			$id = $usuarioAct['idHistorialSesion'];
			$tipousuario = $usuarioAct['idrol'] == '3' ? 'A' : 'P';
			$idusuario = $usuarioAct['idpersona'];
			$lugar = 'P';
			$fechaentrada = !empty($usuarioAct['fechaentrada'])?$usuarioAct['fechaentrada']:date('Y-m-d H:m:s');
			$fechasalida = date('Y-m-d H:i:s');
			// var_dump($usuarioAct);
			// var_dump($this->EditarHitorial($id,$tipousuario,$idusuario,$lugar,$fechaentrada,$fechasalida));
			$this->EditarHitorial($id,$tipousuario,$idusuario,$lugar,$fechaentrada,$fechasalida);
		}*/
		/**/////////////////////////////////////////////// */
		
		if(IS_LOGIN===true&&!NegSesion::existeSesion()&&!in_array($rec,$permitenologueado)){
			$url=explode("/",$_SERVER["REQUEST_URI"]);
			$url1=strtolower(end($url));
			if($url1==''){
				array_pop($url);
				$url1=strtolower(end($url));
			}
			if(in_array($url1,array('buscarjson')) || ($rec=='historial_sesion' && in_array($url1,array('editar','agregar')))){
				echo json_encode(array('code'=>201,'Error'=>JrTexto::_('closed sesion')));
				exit();
			}
			parent::enrutar('sesion');
		}else{
			//Sitio::$idHistorialSesion;
			// echo $usuarioAct['idHistorialSesion'];
			// exit();
			
			$documento =& JrInstancia::getDocumento();
			$documento->setTitulo(NegSesion::get('nombre'), true);
			parent::enrutar($rec, $ax);
		}
	 }
	/*private function EditarHitorial($id,$tipousuario,$idusuario,$lugar,$fechaentrada,$fechasalida){
		try{
			$this->BD = new DatBase;
			$this->BD->conectar();
			$this->BD->iniciarTransaccion('dat_historial_sesion_update');
				
			$estados = array('tipousuario'=>$tipousuario
							,'idusuario'=>$idusuario
							,'lugar'=>$lugar
							,'fechaentrada'=>empty($fechaentrada)?date('Y-m-d H:m:s'):$fechaentrada
							,'fechasalida'=>$fechasalida
							);
			if(!empty($idcurso)){
				$estados["idcurso"] = $idcurso;
			}
			$this->BD->oBD->update('historial_sesion ', $estados, array('idhistorialsesion' => $id));
			$this->BD->terminarTransaccion('dat_historial_sesion_update');		
			// $sesion = JrSession::getInstancia();
			// $sesion->set('idHistorialSesion', $idHistSesion, '__admin_m3c');
			$this->BD = null;
			return true;

		}catch(Exception $e){			
			echo '<script type="text/javascript">console.log("'.$e.'");</script>';
		}	
		return false;
	}*/
	public static function &getInstancia()
	{
		if(empty(self::$instancia)) {
			self::$instancia = new self;
		}
		return self::$instancia;
	}

	public function error($msj, $plantilla = null)
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->error($msj, $plantilla);
	}
	public function noencontrado()
	{
		JrCargador::clase('sys_web::WebExcepcion', RUTA_SITIO, 'sys_web');
		$oWebExcepcion = new WebExcepcion;
		return $oWebExcepcion->noencontrado();
	}
}