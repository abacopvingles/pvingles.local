<?php
//@autor				: Abel Chingo Tello ACHT
defined('RUTA_BASE') or die();
class WebAdminxAjax
{
	public static function call()
	{//@comp, @rec(clase, funcion), @metodo
	 //@__modulo, @clase, @metodo, @args
		$oRespuestaXajax = new xajaxResponse();		
		try{
			$args = func_get_args();			
			$req_ = @$args[0];
			$rec  = @$args[1];
			$permite=array('sesion','idioma','aulavirtual','pronunciacion','proyecto','paginas');
			#var_dump($permite);
			if(!NegSesion::existeSesion() && !in_array(@$rec,$permite)) {
				throw new Exception(JrTexto::_('Access denied').': ' . @$rec);
			}			
			if(empty($rec)) {
				throw new Exception(JrTexto::_('Param incorrect'));
			}
			$prefMod = substr($req_, 0, 2);
			if('__' != $prefMod) {
				self::recurso($oRespuestaXajax, $rec, $args);
			} else {				
				self::modulo($oRespuestaXajax, substr($req_, 2), $rec, $args);
			}
		} catch(Exception $e) {
			$oRespuestaXajax->alert($e->getMessage());
		}		
		return $oRespuestaXajax;
	}	
	private static function recurso(&$oRespuestaXajax, $rec, $args)
	{
		try {
			$clase = 'Web' . ucfirst($rec);			
			try {
				$dir = RUTA_SITIO . 'sys_web' . SD . 'cls.' . $clase . '.php';				
				if(!is_file($dir)) {
					$metodo = false;
					throw new Exception();
				}				
				require_once($dir);				
				//solo si es metodo, anteponer xNombreMetodo
				$metodo = 'x' . ucfirst(@$args[2]);
				if(is_callable(array($clase, $metodo)) == false) {
					$metodo = false;
				}
			} catch(Exception $e) {
				$dir	= RUTA_SITIO . 'sys_web' . SD . 'xajax' . SD . 'funciones.php';				
				if(!is_file($dir)) {
					throw new Exception(JrTexto::_('Resource not found'));
				}				
				require_once($dir);
			}			
			if(false === $metodo) {
				if(function_exists($rec)) {
					$args = array_slice($args, 2);
					$rec($oRespuestaXajax, $args);
				}
			} else {
				$args = array_slice($args, 3);
				$obj = new $clase;
				$obj->$metodo($oRespuestaXajax, $args);
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	
	private static function modulo(&$oRespuestaXajax, $mod, $rec, $args)
	{
		try {
			$dir = RUTA_SITIO . 'modulos' . SD . $mod;
			//var_dump($dir);
			if(!file_exists($dir)) {
				throw new Exception(JrTexto::_('Param incorrect').$dir);
			}			
			$clase = ucfirst($rec);
			$dir .= SD  . 'cls.' . $clase . '.php';			
			try {
				if(!is_file($dir)) {
					$metodo = false;
					throw new Exception();
				}
				require_once($dir);				
				//solo si es metodo, anteponer xNombreMetodo
				$metodo = 'x' . ucfirst(@$args[2]);				
				if(is_callable(array($clase, $metodo)) == false) {
					$metodo = false;
				}
			} catch(Exception $e) {
				throw new Exception(JrTexto::_('Param incorrect'));
			}			
			if(false !== $metodo) {
				$args = array_slice($args, 3);
				$obj = new $clase;
				$obj->$metodo($oRespuestaXajax, $args);
			}
		} catch(Exception $e) {
			$oRespuestaXajax->alert($e->getMessage());
		}
	}
}