<style type="text/css">
   ul#menupersonalizado li a {
    color:#fff;
   } 
   ul#menupersonalizado li ul .dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover{
       background: #1641ad;
   } 
   ul#menupersonalizado li ul {
       background: #4466bb;
   }

    ul#menupersonalizado li a.active::after{
    content: "\f00c";
    font-family: FontAwesome;
    padding-right: 1em;
    position: absolute;
    right: 0;
   }

</style>
<header>
    <div class="container-fluid">
        <div class="navbar-header navbar-static-top"> 
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo.png" class="hvr-wobble-skew img-responsive hidden" alt="logo">
            </a>
        </div>       
        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav" id="menupersonalizado">
              <li class="active"><a href="<?php echo $this->documento->getUrlSitio() ?>"><?php echo JrTexto::_('Home'); ?></a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Window'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="resetwindow" href="#"><?php echo ucfirst(JrTexto::_('Reset windows')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vinformation" href="#"><?php echo ucfirst(JrTexto::_('Information')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vemiting" href="#"><?php echo ucfirst(JrTexto::_('Emitting class')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vchat" href="#"><?php echo ucfirst(JrTexto::_('Chat')); ?></a></li>
                  <li ><a class="showwindow init active" data-ventana="vparticipantes" href="#"><?php echo ucfirst(JrTexto::_('Participants')); ?></a></li>                  
                  <li ><a class="showwindow " data-ventana="vpizarra" href="#"><?php echo ucfirst(JrTexto::_('Board')); ?></a></li>
                  <li style="display: none"><a class="showwindow" data-ventana="vinvite" href="#"><?php echo ucfirst(JrTexto::_('Invite users')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vfiles" href="#"><?php echo ucfirst(JrTexto::_('Shared files')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vnotas" href="#"><?php echo ucfirst(JrTexto::_('Notes')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vwebcam" href="#"><?php echo ucfirst(JrTexto::_('Webcam')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vencuesta" href="#"><?php echo ucfirst(JrTexto::_('Questions')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vvideos" href="#"><?php echo ucfirst(JrTexto::_('Videos')); ?></a></li>
                  <li ><a class="showwindow " data-ventana="vaudios" href="#"><?php echo ucfirst(JrTexto::_('Audios')); ?></a></li>
                 
                </ul>
              </li>
              <li class="dropdown menucompartir">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Share'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="btnsharedesktop" href="#"><?php echo ucfirst(JrTexto::_('Full Screen')); ?></a></li>
                  <li ><a class="btnsharefile" href="#"><?php echo ucfirst(JrTexto::_('File')); ?></a></li>                  
                </ul>
              </li>
              <li class="dropdown menutools">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo JrTexto::_('Tools'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li ><a class="btnalzarmano" href="#"><?php echo ucfirst(JrTexto::_('Hand Raised')); ?></a></li>
                  <li ><a class="btnsharemicro" href="#"><?php echo ucfirst(JrTexto::_('Microphone')); ?></a></li>
                  <li ><a class="btnsharecamera" href="#"><?php echo ucfirst(JrTexto::_('Webcam')); ?></a></li>                  
                  <li ><a class="btnsharegrabar" href="#"><?php echo ucfirst(JrTexto::_('record the class')); ?></a></li>                  
                </ul>
              </li>
              <li class="istooltip" title="<?php echo JrTexto::_('Raise your hand'); ?>"><a class="btnalzarmano" href="#"><i class="fa fa-hand-stop-o animated infinite"></i></a></li>
              <li class="istooltip" title="<?php echo JrTexto::_('Enable and disable Microphone'); ?>"><a class="btnsharemicro" href="#"><i class="fa fa-microphone-slash animated infinite"></i></a></li>
              <li class="istooltip" title="<?php echo JrTexto::_('Enable and disable Webcam'); ?>"><a class="btnsharecamera" href="#"><i class="fa fa-camera animated infinite"></i></a></li>              
              <li class="istooltip" title="<?php echo JrTexto::_('Enable and disable record class'); ?>"><a class="btnsharegrabar" href="#"><i class="glyphicon glyphicon-record animated infinite"></i></a></li>
              <li class="istooltip" title="<?php echo JrTexto::_('Enable and disable screen sharing'); ?>"><a class="btnsharedesktop" href="#"><i class="fa fa-desktop animated infinite"></i></a></li>
              <!--li class="istooltip" title="Activar/Desactivar Compartir archivo"><a class="btnsharefile" href="#"><i class="fa fa-shared-file"></i></a></li-->
              <li class="istooltip" title="<?php echo JrTexto::_('Open/look'); ?> Smartclass"><a class="btnlokedroom" href="#"><i class="fa fa-unlock"></i></a></li>
              <li class="istooltip" title="<?php echo JrTexto::_('Exit'); ?> Smartclass"><a class="btnsaliraula" href="#"><i class="fa fa-power-off"></i></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right menutop1">
                <li class="dropdown"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        <span class="badge bg-danger">4</span> 
                    </a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-header">
                            <a href="#" class="capitalize btn slide-sidebar-right " data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/nuevo_mensaje">
                                <i class="fa fa-envelope-o"></i>
                                <?php echo JrTexto::_('New'); ?> 
                                <?php echo JrTexto::_('message'); ?>
                            </a>
                        </li>
                        <li class="dropdown-content styled-scroll">
                            <ul>
                            <?php for($i=1; $i<=4; $i++): ?>
                                <li>
                                    <a href="#" class="msg">
                                        <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" class="msg-photo" alt="Avatar" />
                                        <span class="msg-body">
                                            <span class="msg-title">
                                                <span class="blue">Alex:</span>
                                                Ciao sociis nato que pen a tibus et auctor ...
                                            </span>

                                            <span class="msg-time">
                                                <i class="fa fa-clock-o"></i>
                                                <span>a moment ago</span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_mensajes">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> 
                <!--li class="dropdown"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calendar"></i>
                        <span class="badge bg-success">2</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-header">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/nueva_actividad">
                                <i class="fa fa-thumb-tack"></i>
                                <?php echo JrTexto::_('New activity'); ?>
                            </a>
                        </li>
                        <li class="dropdown-content">
                            <ul>
                            <?php for($i=1; $i<=2; $i++): ?>
                                <li>
                                    <a href="#" class="activity">
                                        <span>Activity <?=$i; ?>.</span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_actividades">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> 
                <li class="dropdown"> 
                    <a ref="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-info">3</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-content">
                            <ul>
                            <?php for($i=1; $i<=3; $i++): ?>
                                <li>
                                    <a href="#" class="notification">
                                        <span class="notif-text">Notification <?=$i; ?>.</span>
                                        <span class="notif-time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>a moment ago</span>
                                        </span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_notificaciones">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li--> 
                <li class="dropdown" id="account"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php   
                                if($this->usuarioAct["rol_original"]!='Alumno'){
                                        $rol_=$this->usuarioAct["rol"];
                                        $rolo_=$this->usuarioAct["rol_original"];  
                                        $rol=$this->oNegRoles->buscar(array('idrol'=>$rolo_));
                                            if(!empty($rol[0])){
                                                $rol=$rol[0];
                                                $verrol=$rol_==$rolo_?ucfirst($rol["rol"]):'Alumno';
                                                $verme=$rol_==$rolo_?'Alumno':ucfirst($rol["rol"]);
                                                if(NegSesion::tiene_acceso('seralumno', 'list')||$rolo_!='Alumno'){
                                                  $verrol.=' <small class="changerol">('.JrTexto::_('Watch me as a')." ".$verme.')</small>';
                                                }
                                            }
                                        }else{ $verrol=$this->usuarioAct["rol"];}
                                    ?>
                                <div class="user-email">
                                 <?php echo $verrol; ?></div>

                                <button class="btn btn-blue capitalize slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/mi_perfil">
                                    <?php echo JrTexto::_('my profile'); ?>
                                </button>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="#" class="btn btn-default capitalize"><?php echo JrTexto::_('change password'); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlSitio() ?>/sesion/salir" class="btn btn-default capitalize"><?php echo JrTexto::_('log out'); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
                <li> 
                    <a  class="hvr-buzz-out" href="#" id="getting-started">
                        <i class="fa fa-th-large"></i>
                    </a> 
                </li> 
            </ul>
        </nav> 
    </div>
</header>