<style type="text/css">
    .cambiarrolmenu a ,.cambiarrolmenu a:hover{
         font-size: 11px !important;
        color: #c13838 !important;
        background: transparent !important;
        text-decoration: none !important;
    }
</style>
<header>
    <div class="container-fluid">
        <div class="navbar-header navbar-static-top"> 
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button> 
            <a href="<?php echo $this->documento->getUrlSitio() ?>" class="navbar-brand" id="logo">
                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/logo_minedu.png" class="hvr-wobble-skew_ img-responsive" alt="logo">
            </a>
        </div>       
        <nav class="collapse navbar-collapse" id="bs-navbar"> 
            <ul class="nav navbar-nav navbar-right menutop1"> 
               
                <!--li class="dropdown"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        <span class="badge bg-danger">4</span> 
                    </a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-header">
                            <a href="#" class="capitalize btn slide-sidebar-right " data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/nuevo_mensaje">
                                <i class="fa fa-envelope-o"></i>
                                <?php echo JrTexto::_('New'); ?> 
                                <?php echo JrTexto::_('message'); ?>
                            </a>
                        </li>
                        <li class="dropdown-content styled-scroll">
                            <ul>
                            <?php for($i=1; $i<=4; $i++): ?>
                                <li>
                                    <a href="#" class="msg">
                                        <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" class="msg-photo" alt="Avatar" />
                                        <span class="msg-body">
                                            <span class="msg-title">
                                                <span class="blue">Alex:</span>
                                                Ciao sociis nato que pen a tibus et auctor ...
                                            </span>

                                            <span class="msg-time">
                                                <i class="fa fa-clock-o"></i>
                                                <span>a moment ago</span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_mensajes">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li--> 
                <!--
                <li class="dropdown"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-calendar"></i>
                        <span class="badge bg-success">2</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-header">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/nueva_actividad">
                                <i class="fa fa-thumb-tack"></i>
                                <?php echo JrTexto::_('New activity'); ?>
                            </a>
                        </li>
                        <li class="dropdown-content">
                            <ul>
                            <?php for($i=1; $i<=2; $i++): ?>
                                <li>
                                    <a href="#" class="activity">
                                        <span>Activity <?=$i; ?>.</span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_actividades">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> 
                <li class="dropdown"> 
                    <a ref="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="badge bg-info">3</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="dropdown-content">
                            <ul>
                            <?php for($i=1; $i<=3; $i++): ?>
                                <li>
                                    <a href="#" class="notification">
                                        <span class="notif-text">Notification <?=$i; ?>.</span>
                                        <span class="notif-time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>a moment ago</span>
                                        </span>
                                    </a>
                                </li>
                            <?php endfor; ?>
                            </ul>
                        </li>
                        <li class="dropdown-footer">
                            <a href="#" class="capitalize btn slide-sidebar-right" data-source="<?php echo $this->documento->getUrlSitio() ?>/sidebar_pages/ver_notificaciones">
                                <?php echo JrTexto::_('see more'); ?>
                                <i class="fa fa-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </li> 
                -->
                <li class="dropdown" id="account"> 
                    <a href="#" class="dropdown-toggle hvr-buzz-out" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle"></i>
                    </a>

                    <div class="user-menu dropdown-menu">
                        <div class="account-info">
                            <a  class="user-photo">
                                <img src="<?php echo $this->documento->getUrlStatic() ?>/media/imagenes/user_avatar.jpg" alt="user" class="img-responsive">
                            </a>
                            <div class="data-user">
                                <div class="user-name"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                                <div class="user-email"><?php echo $this->usuarioAct['email'] ?></div>
                                <?php 
                                $roles=$this->usuarioAct["roles"]; 
                                $idrol_=$this->usuarioAct["idrol"];
                                $verrol=$this->usuarioAct["rol"];
                                if(count($roles)>1){                                    
                                    $verrol.=' <small class="cambiarrolmenu"><a href="'.$this->documento->getUrlBase().'/sesion/cambiarrol" class="changerol">('.JrTexto::_('Change Rol').')</a></small>';
                                }else{ $verrol=$this->usuarioAct["rol"];}
                                ?>
                                <div class="user-email">
                                 <?php echo $verrol; ?></div>
                                <a class="btn btn-blue capitalize btn-profile" href="<?php echo $this->documento->getUrlBase() ?>/personal/perfil">
                                    <?php echo JrTexto::_('my profile'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="action-buttons">
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/personal/cambiarclave/?id=<?php echo $this->usuarioAct["dni"];?>" class="btn btn-default capitalize"><?php echo JrTexto::_('change password'); ?></a>
                            </div>
                            <div>
                                <a href="<?php echo $this->documento->getUrlBase() ?>/sesion/salir" class="btn btn-default capitalize"><?php echo JrTexto::_('log out'); ?></a>
                            </div>
                        </div>
                    </div> 
                </li>
                <li> 
                    <a  class="hvr-buzz-out" href="#" id="getting-started">
                        <i class="fa fa-th-large"></i>
                    </a> 
                </li> 
            </ul>
        </nav> 
    </div>
</header>
