
<!DOCTYPE html>
<html>
    <head>
        <title>MP3</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta charset="utf-8"/>
        <link href="../../resources/plantilla/css/one-page-wonder.min.css" rel="stylesheet">
        <link href="../../resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../../resources/css/fileinput.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="../../resources/DataTables/datatables.min.css"/>

    </head>
    <body>
        <?php
            include("../../modulos/top/top.php");?>
        ?>
        <!-- <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top" style="position: sticky;">
          <div class="container">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
              </ul>
            </div>
          </div>
        </nav> -->
        <div class="container p-5">
            <div class="row">
                <div class="col-sm-7">
                    <div class="card">
                        <div class="card-header">
                            <h5>Importar MP3</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form enctype="multipart/form-data">
                                            <div class="file-loading">
                                                <input id="file-es" name="file-es[]" type="file" multiple>
                                            </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="row" id="recursos_audios">
                        <div class="col-xs-12">
                            <div class="panel">
                                
                                <div class="panel-body">
                                    <div class="col-xs-12 text-left">
                                        <span>LISTA DE AUDIOS:</span>
                                        <!-- <div class="panel panel-default"> -->
                                        <?php
                                            $carpeta = $_REQUEST["audio"];
                                            
                                            //$ruta = URL_RAIZ.'static/audios/'.$carpeta;
                                            $ruta = '../../../static/audios/'.$carpeta;
                                            if (is_dir($ruta)){
                                                // Abre un gestor de directorios para la ruta indicada
                                                $gestor = opendir($ruta);
                                                echo '<table id="example" class="table table-striped table-bordered" style="width:100%">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $cont = 0;
                                                // Recorre todos los elementos del directorio
                                                while (($archivo = readdir($gestor)) !== false)  {
                                                        
                                                    $extension = strtolower(substr($archivo, -3));
                                                    if ($extension == 'mp3') {
                                                        $ruta_completa = $ruta . "/" . $archivo;

                                                        // Se muestran todos los archivos y carpetas excepto "." y ".."
                                                        if ($archivo != "." && $archivo != "..") {
                                                            // Si es un directorio se recorre recursivamente
                                                            echo '<tr>';
                                                            echo '<td>';
                                                            echo $cont++;
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                                echo $archivo;
                                                            } else {
                                                                echo $archivo;
                                                            }
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                                echo "<a class='btn btn-default' style='background-color: #9afffe;' role='button' href='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</a>";
                                                            } else {
                                                                echo "<a class='btn btn-default' style='background-color: #9afffe;' role='button' href='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</a>";
                                                            }
                                                            echo '</td>';
                                                            echo '</tr>';
                                                        }
                                                    }           
                                                }
                                                if ($cont ==0) {
                                                    echo '<tr>';
                                                    echo '<td colspan="3">';
                                                    echo 'No hay datos en el directorio';
                                                    echo '</td>';
                                                    echo '</tr>;';
                                                }
                                                echo '</tbody>';
                                                echo '<tfoot>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</tfoot>';

                                                echo '</table>';
                                                // Cierra el gestor de directorios
                                                closedir($gestor);
                                            } else {
                                                echo "No es una ruta de directorio valida<br/>";
                                            }
                                            
                                            ?>
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <script src="../../resources/plantilla/vendor/jquery/jquery.min.js"></script>
        <script src="../../resources/plantilla/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../../resources/js/jquery.js"></script>
        <script type="text/javascript" src="../../resources/js/fileinput.min.js"></script>
        <script type="text/javascript" src="../../resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../resources/js/locales/es.js"></script>
        <script type="text/javascript" src="../../resources/DataTables/datatables.min.js"></script>

        <script>
        var myVar;
            var ruta = '<?php $carpeta = $_REQUEST["audio"]; echo $carpeta;?>';     
        $('#file-es').fileinput({
            language: 'es',
            uploadUrl: '../controller/importar-mp3.php?audios='+ruta,
            allowedFileExtensions: ['mp3'],
            uploadAsync: false,
            overwriteInitial: false,
            minFileCount: 1,
            maxFileCount: 5,
            maxFileSize: 1048576,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the markup
        });
        clearTimeout(myVar);
       $ ( '#file-es' ). on ( 'filebatchuploadsuccess' , function ( event , data ) { 
            myVar = setTimeout(function(){ location.reload(); }, 1000);
            
        });
       $(document).ready(function() {
            $('#table_audios').dataTable({
                "pageLength":10
            });
            var url = '';
            $('.btn-default').on('click',function(event) {

                var url = $(this).val();
                //$("#jquery_jplayer_1").jPlayer( "clearMedia" );
                $("#jquery_jplayer_1").jPlayer("destroy");
                $("#jquery_jplayer_1").jPlayer({
                    ready: function () {
                        console.log(url);
                        $(this).jPlayer("setMedia", {
                            title: "Bubble",
                            mp3: url
                        });
                    },
                    swfPath: "../../dist/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    useStateClassSkin: true,
                    autoBlur: true,
                    smoothPlayBar: true,
                    keyEnabled: true,
                    remainingDuration: true,
                    toggleDuration: true
                });
            });
            

            
        } );
        </script>
    </body>
        
</html>