
<!DOCTYPE html>
<html>
    <head>
        <title>MP3</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta charset="utf-8"/>
        <link href="../../resources/plantilla/css/one-page-wonder.min.css" rel="stylesheet">
        <link href="../../resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="../../resources/css/fileinput.min.css" rel="stylesheet"/>
        <!-- <link rel="stylesheet" type="text/css" href="../../resources/DataTables/datatables.min.css"/> -->
        <link rel="stylesheet" type="text/css" href="../../resources/datatable1.10/media/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet" type="text/css" href="../../resources/datatable1.10/extensions/Buttons/css/buttons.dataTables.min.css"/>
        <link href="../../resources/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />



    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top" style="position: sticky;">
          <div class="container">
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#"></a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div class="container p-5">
            <div class="row">
                <div class="col-sm-7">
                    <div class="row" id="recursos_audios">
                        <div class="col-xs-12">
                            <div class="panel">
                                
                                <div class="panel-body">
                                    <div class="col-xs-12 text-left">
                                        <span>LISTA DE AUDIOS:</span>
                                        <!-- <div class="panel panel-default"> -->
                                        <?php
                                            $carpeta = $_REQUEST["audio"];
                                            
                                            //$ruta = URL_RAIZ.'static/audios/'.$carpeta;
                                            $ruta = '../../../static/audios/'.$carpeta;
                                            if (is_dir($ruta)){
                                                // Abre un gestor de directorios para la ruta indicada
                                                echo '<div class="panel">';
                                                echo '<div class="panel-body">';
                                                $gestor = opendir($ruta);
                                                echo '<table id="example" class="table table-striped table-responsive" style="width:100%;">';
                                                echo '<thead>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</thead>';
                                                echo '<tbody>';
                                                $cont = 1;
                                                // Recorre todos los elementos del directorio
                                                while (($archivo = readdir($gestor)) !== false)  {
                                                        
                                                    $extension = strtolower(substr($archivo, -3));
                                                    if ($extension == 'mp3') {
                                                        $ruta_completa = $ruta . "/" . $archivo;

                                                        // Se muestran todos los archivos y carpetas excepto "." y ".."
                                                        if ($archivo != "." && $archivo != "..") {
                                                            // Si es un directorio se recorre recursivamente
                                                            echo '<tr>';
                                                            echo '<td>';
                                                            echo $cont++;
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                                echo $archivo;
                                                            } else {
                                                                echo $archivo;
                                                            }
                                                            echo '</td>';
                                                            echo '<td>';
                                                            if (is_dir($ruta_completa)) {
                                                            	echo "<button type='button' class='btn btn-default' id='mp3' value='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</button>";
                                                            } else {
                                                                echo "<button type='button' class='btn btn-default' id='mp3' value='".$ruta_completa."'>";
                                                                echo 'View';
                                                                echo "</button>";
                                                            }
                                                            echo '</td>';
                                                            echo '</tr>';
                                                        }
                                                    }           
                                                }
                                                if ($cont ==0) {
                                                    echo '<tr>';
                                                    echo '<td colspan="3">';
                                                    echo 'No hay datos en el directorio';
                                                    echo '</td>';
                                                    echo '</tr>;';
                                                }
                                                echo '</tbody>';
                                                echo '<tfoot>';
                                                    echo '<tr>';
                                                        echo '<th>N°</th>';
                                                        echo '<th>Nombre</th>';
                                                        echo '<th>Operacion</th>';
                                                    echo '</tr>';
                                                echo '</tfoot>';

                                                echo '</table>';
                                                echo '</div>';
                                                echo '</div>';
                                                // Cierra el gestor de directorios
                                                closedir($gestor);
                                            } else {
                                                echo "No es una ruta de directorio valida<br/>";
                                            }
                                            
                                            ?>
                                        <!-- </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-5">
					<div style="margin-top: 50%">
						REPRODUCTOR DE MUSICA:
						<div id="jquery_jplayer_1" class="jp-jplayer"></div>
						<div id="jp_container_1" class="jp-audio" role="application" aria-label="media player" >
							<div class="jp-type-single">
								<div class="jp-gui jp-interface">
									<div class="jp-controls">
										<button class="jp-play" role="button" tabindex="0">play</button>
										<button class="jp-stop" role="button" tabindex="0">stop</button>
									</div>
									<div class="jp-progress">
										<div class="jp-seek-bar">
											<div class="jp-play-bar"></div>
										</div>
									</div>
									<div class="jp-volume-controls">
										<button class="jp-mute" role="button" tabindex="0">mute</button>
										<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
										<div class="jp-volume-bar">
											<div class="jp-volume-bar-value"></div>
										</div>
									</div>
									<div class="jp-time-holder">
										<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
										<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
										<div class="jp-toggles">
											<button class="jp-repeat" role="button" tabindex="0">repeat</button>
										</div>
									</div>
								</div>
								<div class="jp-details">
									<div class="jp-title" aria-label="title">&nbsp;</div>
								</div>
								<div class="jp-no-solution">
									<span>Update Required</span>
									To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div>
        </div>
        <script src="../../resources/plantilla/vendor/jquery/jquery.min.js"></script>
        <script src="../../resources/plantilla/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../../resources/js/jquery.js"></script>
        <script type="text/javascript" src="../../resources/js/fileinput.min.js"></script>
        <script type="text/javascript" src="../../resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../../resources/js/locales/es.js"></script>
        <!-- <script type="text/javascript" src="../../resources/DataTables/datatables.min.js"></script> -->
        <script type="text/javascript" src="../../resources/datatable1.10/media/js/jquery.dataTables.min.js"></script>

        <script type="text/javascript" src="../../resources/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js"></script>
        </script>

        <script>
        var myVar;
            var ruta = '<?php $carpeta = $_REQUEST["audio"]; echo $carpeta;?>';     
        $('#file-es').fileinput({
            language: 'es',
            uploadUrl: '../controller/importar-mp3.php?audios='+ruta,
            allowedFileExtensions: ['mp3'],
            uploadAsync: false,
            overwriteInitial: false,
            minFileCount: 1,
            maxFileCount: 5,
            maxFileSize: 1048576,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the markup
        });
        clearTimeout(myVar);
       $ ( '#file-es' ). on ( 'filebatchuploadsuccess' , function ( event , data ) { 
            myVar = setTimeout(function(){ location.reload(); }, 1000);
            
        });
       $(document).ready(function() {
            $('#table_audios').dataTable({
            	"pageLength":10
            });
			var url = '';
            $('.btn-default').on('click',function(event) {

            	var url = $(this).val();
           	 	//$("#jquery_jplayer_1").jPlayer( "clearMedia" );
           	 	$("#jquery_jplayer_1").jPlayer("destroy");
           	 	$("#jquery_jplayer_1").jPlayer({
					ready: function () {
						console.log(url);
						$(this).jPlayer("setMedia", {
							title: "Bubble",
							mp3: url
						});
					},
					swfPath: "../../dist/jplayer",
					supplied: "mp3",
					wmode: "window",
					useStateClassSkin: true,
					autoBlur: true,
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true
				});
            });
            

            
        } );
        </script>
    </body>
        
</html>