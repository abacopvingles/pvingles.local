<?php 

    class Conexion{
        
        private $servidor="localhost";
        private $nombrebd="pvingles.local";
        private $usuario="root";
        private $password="root";
        
        public function conectarse(){
            
            try{
                $conn = new PDO("mysql:host=".$this->servidor.";dbname=".$this->nombrebd,$this->usuario,$this->password);
                $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                return $conn;   
            }catch(PDOException $ex){
                die("Error en: ".$ex->getMessage()."<br>En linea:".$ex->getLine());
            }
            
        }
        
        
    }


?>