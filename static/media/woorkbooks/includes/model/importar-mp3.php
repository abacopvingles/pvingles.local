<?php 
    
    class importar{
        
        private $db;
        
        public function __construct($db){
            $this->db = $db;
        } 
        
        public function query($sql){
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            return $stmt;
        }
        
        
    }

?>