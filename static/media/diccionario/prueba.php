<?php

 	if ($_POST['lenguaje']) {
 		if ($_POST['lenguaje']==1) {
			$valortexto = $_POST['texto'];
 			$url = 'diccionario.json';
 			$json = file_get_contents($url,true);
 			$data = json_decode($json);
			$valor = $data->datos;
			$cont = 0;
			foreach ($valor as $data1) {
				$valoringles = $data1->ingles;
				
				if ($valortexto == $valoringles) {
					$cont = 1;
					echo '<div class="row"><div class="col-xs-12 col-sm-12 col-md-12 " style="font-size:30px;text-transform: capitalize;color:#057879;background-color: white;text-align:center"><span class="texto">'.$valoringles.'</span> <i class="fa fa-volume-up pronunciar"></i></div></div>';
					$result = $data1->datos1;
					if (!empty($result)) {
						foreach ($result as $data2) {
							$valortipo = $data2->tipo;
							$valorpronunciacion = $data2->pronunciacion;
							$result2 = $data2->datos2;
							if (!empty($result2)) {
								foreach ($result2 as $data3) {
									if (!empty($data3->espaniol)) {$valorespaniol = $data3->espaniol;echo '<div class="row" style="background-color:#d9feff;margin-left:0px;width: 98%;"><div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;;color:#057879"><i class="fa fa-angle-double-right"></i>Translation:</div>';echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;font-weight: bold">'.$valorespaniol.'</div>';}
									if (!empty($data3->significado)) {$valorsignificado = $data3->significado;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;;color:#057879"><i class="fa fa-angle-double-right"></i>Meaning:</div>';echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;">'.$valorsignificado.'</div>';}
									$result3 = $data3->ejemplos;	
									echo '<div class="col-xs-12 col-sm-12 col-md-12" style="font-weight: bold;font-size:16px;;color:#057879"><i class="fa fa-angle-double-right"></i>Examples:</div>';
									foreach ($result3 as $data4) {
										if (!empty($data4->ejemplo_1)) {$valorejemplo_1 = $data4->ejemplo_1;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'.$valorejemplo_1.'</div>';}
										if (!empty($data4->ejemplo_2)) {$valorejemplo_2 = $data4->ejemplo_2;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'.$valorejemplo_2.'</div>';}
										if (!empty($data4->ejemplo_3)) {$valorejemplo_3 = $data4->ejemplo_3;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'.$valorejemplo_3.'</div>';}
										if (!empty($data4->ejemplo_4)) {$valorejemplo_4 = $data4->ejemplo_4;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'.$valorejemplo_4.'</div>';}
										if (!empty($data4->ejemplo_5)) {$valorejemplo_5 = $data4->ejemplo_5;echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;background-color:#cadbff;"><i class="fa fa-chevron-circle-right"></i>'.$valorejemplo_5.'</div>';}
									}
									echo "</div><br>";
								}
							}
						}
					}	
 				}
			}
			if($cont==0){
				echo '<div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px;">No se ha encontrado resultado</div>';
			} 
		}else{
			if ($_POST['lenguaje']==2) {
				$valortexto = $_POST['texto'];
	 			$url = 'diccionarioesp.json';
	 			$json = file_get_contents($url,true);
	 			$data = json_decode($json);
				$valor = $data->espaniol;
				$valor1 = $data->body;
				echo '<div class="row"><div class="col-xs-12 col-sm-12 col-md-12 " style="font-size:30px;text-transform: capitalize;color:#057879;background-color: white;text-align:center"><span class="texto">'.$valor.'</span> <i class="fa fa-volume-up pronunciar"></i></div></div>';
				echo $valor1;
			}
		}
 	}
 ?>
