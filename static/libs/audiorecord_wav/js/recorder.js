/*License (MIT)

Copyright © 2013 Matt Diamond
Downloaded from: https://github.com/cwilso/AudioRecorder 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of 
the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO 
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.
*/

(function(window){

    var ruta_lib = _sysUrlStatic_+'/libs/audiorecord_wav/';
    var WORKER_PATH = ruta_lib+'js/recorderWorker.js';
    var blob = null;  /* workaround from: https://stackoverflow.com/questions/25101037/record-audio-using-the-users-microphone-with-html5 */

    var Recorder = function(source, cfg){
        var config = cfg || {};
        var bufferLen = config.bufferLen || 4096;
        this.context = source.context;
        if(!this.context.createScriptProcessor){
            this.node = this.context.createJavaScriptNode(bufferLen, 2, 2);
        } else {
            this.node = this.context.createScriptProcessor(bufferLen, 2, 2);
        }

        var worker = new Worker(config.workerPath || WORKER_PATH);
        worker.postMessage({
            command: 'init',
            config: {
                sampleRate: this.context.sampleRate
            }
        });
        var recording = false,
        currCallback;

        this.node.onaudioprocess = function(e){
            if (!recording) return;
            worker.postMessage({
                command: 'record',
                buffer: [
                e.inputBuffer.getChannelData(0),
                e.inputBuffer.getChannelData(1)
                ]
            });
        }

        this.configure = function(cfg){
            for (var prop in cfg){
                if (cfg.hasOwnProperty(prop)){
                    config[prop] = cfg[prop];
                }
            }
        }

        this.record = function(){
            recording = true;
        }

        this.stop = function(){
            recording = false;
        }

        this.clear = function(){
            worker.postMessage({ command: 'clear' });
        }

        this.getBuffers = function(cb) {
            currCallback = cb || config.callback;
            worker.postMessage({ command: 'getBuffers' })
        }

        this.exportWAV = function(cb, type){
            currCallback = cb || config.callback;
            type = type || config.type || 'audio/wav';
            if (!currCallback) throw new Error('Callback not set');
            worker.postMessage({
                command: 'exportWAV',
                type: type
            });
        }

        this.exportMonoWAV = function(cb, type){
            currCallback = cb || config.callback;
            type = type || config.type || 'audio/wav';
            if (!currCallback) throw new Error('Callback not set');
            worker.postMessage({
                command: 'exportMonoWAV',
                type: type
            });
        }

        worker.onmessage = function(e){
            blob = e.data;
            currCallback(blob);
        }

        source.connect(this.node);
        this.node.connect(this.context.destination); /* if the script node is not connected to an output the "onaudioprocess" event is not triggered in chrome.*/
    };

    Recorder.setupDownload = function(blob, filename){ 
        /*Put the blob in a <a href> to download*/
        var url = (window.URL || window.webkitURL).createObjectURL(blob);
        var link = document.getElementById("save");
        link.href = url;
        link.download = filename || 'output.wav';
    }

    Recorder.setupPreview = function(blob, filename){ 
        /*Put the blob in a <audio> to hear it*/
        var url = (window.URL || window.webkitURL).createObjectURL(blob);
        var audioPlayer = $('#mdl-adjuntar_grabacionvoz #recording_player')[0];
        audioPlayer.src = url;
        audioPlayer.download = filename || 'recording.wav';
    }

    Recorder.uploadRecording= function(urlAjax, formData, fnAjax) {
        formData.append('filearchivo', blob);
        var fnBeforeSend = fnAjax.beforeSend||function () {};
        var fnSuccess = fnAjax.success||function (resp) { console.log(resp) };
        var fnFail = fnAjax.fail||function (err) { console.log(err) };
        var fnAlways = fnAjax.always||function () {};
        $.ajax({
            url: urlAjax,
            type: 'post',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend : fnBeforeSend
        }).done(fnSuccess).fail(fnFail).always(fnAlways);
    };

    window.Recorder = Recorder;

})(window);
