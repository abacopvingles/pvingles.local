/*************************************   General   ******************************************************/
(function() {
    var params = {},
        r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
        return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1)))
        params[d(match[1])] = d(match[2]);
    window.params = params;
})();

/***********************************************************************************************/
function showPage(page_no) {
        __PDFPAGE_RENDERING_IN_PROGRESS = 1;
        __PDFCURRENT_PAGE = page_no;
        $("#micanvas").hide();
        $("#pdf-current-page").text(page_no);
        __PDF_DOC.getPage(page_no).then(function(page) {// As the canvas is of a fixed width we need to set the scale of the viewport accordingly
            var wcanvas=$('#micanvaspdf').outerWidth();
            var scale_required = __CANVAS.width / page.getViewport(1).width;        
            var viewport = page.getViewport(1.5);// Get viewport of the page at required scale  
            __CANVAS.width = viewport.width;
            __CANVAS.height = viewport.height;
            canvasfabric.setWidth(viewport.width);
            canvasfabric.setHeight(viewport.height);
            var renderContext = {
                canvasContext: __CANVAS_CTX,
                viewport: viewport
            };
            
            // Render the page contents in the canvas
            page.render(renderContext).then(function() {
                __PDFPAGE_RENDERING_IN_PROGRESS = 0;

                var myImage = __CANVAS.toDataURL("image/png");
                fabric.Image.fromURL(myImage, function(img) {
                    canvasfabric.backgroundImage = img;
                    canvasfabric.backgroundImage.width = __CANVAS.width;
                    canvasfabric.backgroundImage.height =  __CANVAS.height;
                    canvasfabric.renderAll();
                });
                $("#micanvas").show();
                $(".pdffileok").show(); 
            });
        });
}

function showPDF(pdf_url) {
    // Show the pdf loader
    $("#pdf-loader").show();
    PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
        __PDF_DOC = pdf_doc;
        __PDFTOTAL_PAGES = __PDF_DOC.numPages;
        $("#pdf-total-pages").text(__PDFTOTAL_PAGES);
        showPage(1);
    }).catch(function(error) {            
    });
}