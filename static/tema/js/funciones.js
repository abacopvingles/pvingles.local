var swi_keys_neu = Array(0, 8, 9, 16, 17, 18, 46);
var swi_keys_int = Array(0, 8, 9, 16, 17, 18);
var nom_dia_semana = Array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
var _sysrdtjson='';
Array.prototype.existe = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}

function redir(pg){
	window.location.href=pg;	
}

function irpage(){
	document.location=document.location;
}

$(document).ready(function(){
	$('body').on('keypress', '.moneda', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_neu.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	
	$('body').on('keypress', '.entero', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_int.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	if( typeof datepicker !== 'undefined' && jQuery.isFunction(datepicker) ) {
		$('.fecha').datepicker({'format' : 'yyyy-mm-dd'});
	}
	
	$('body').on('click', '.fecha', function () {
        $(this).datepicker({autoclose: true});
		$(this).datepicker('show');
    });
	
	$('.fecha').keydown(function(event){
		if(9 == event.keyCode) {
			$(this).datepicker('hide');
		}
	});
	
	$(document).ajaxStart(function() {
		oncargandoxajax();
	});
	
	$(document).ajaxStop(function() {
		offcargandoxajax();
	});
});

function xFancy() {
	try {
		$.fancybox.close(true);
	} catch(e){}
}

function addFancyAjax(href_, btncerrar) {
	try {
		btncerrar = ('' == btncerrar) ? false : btncerrar;
		$.fancybox({type: 'ajax', href : href_, closeBtn  : btncerrar});
	} catch(e){}
}

function addFancy(href_, btncerrar) {
	try {
		btncerrar = ('' == btncerrar) ? false : btncerrar;
		$.fancybox({type: 'iframe', href : href_, closeBtn  : btncerrar});
	} catch(e){}
}

function reloadFancyBoxAjax() {
	try {
		$.fancybox({type: 'ajax', href : $.fancybox.current.href, closeBtn  : false});
	} catch(e){}
}

function agregar_msj_interno(tipo, mensaje) {
	html = '<div class="alert alert-' + tipo + '" role="alert">'
			+ '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'
			+ '<span class="sr-only">Error: </span>' + mensaje
			+ '</div>';
	
	$('#msj-interno').empty().html(html);
}

function moneda(value, decimals) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
	separators = [',', "'", '.'];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },   
    any: function(){
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    },
    getBrowserInfo :function(){
	    var ua= navigator.userAgent, tem, 
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	    if(/trident/i.test(M[1])){
	        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	        return 'IE '+(tem[1] || '');
	    }
	    if(M[1]=== 'Chrome'){
	        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	    }
	    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	    return {navegador:M[0],version:M[1]};
	}
};

function mostrar_notificacion(titulo, mensaje, tipo) {
	new PNotify({title: titulo, text: mensaje, type: tipo, hide: true,styling: 'bootstrap3'});
}

function diff_horas_AMPM(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(pasar_12_24(hora_i));
	h_f = pasar_hora_a_horas(pasar_12_24(hora_f));	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	} else {
		horas = 24 - h_i + h_f;
	}	
	return horas;
}

function pasar_12_24(hora) {
	if(!hora || '' == hora) {
		return;
	}
	
	if('PM' == hora.toString().toUpperCase().substr(-2)) {
		hora = hora.substr(0, 5).trim();
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) < 12) ? parseInt(hora[0]) + 12 : parseInt(hora[0]);
		hora = hora[0] + ':' + (parseInt(hora[1])<10?('0'+hora[1]):hora[1])+':00';
	} else {
		hora = hora.substr(0, 5).trim();
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) == 12) ? '00' : hora[0];
		hora = (parseInt(hora[0])<10?('0'+hora[0]):hora[0]) + ':' +hora[1]+':00';
	}
	return hora;
}

function diff_horas(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(hora_i);
	h_f = pasar_hora_a_horas(hora_f);	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	}else{
		horas = 24 - h_i + h_f;
	}	
	return horas;
}

function pasar_hora_a_horas(hora) {
	hora = hora.split(':');
	horas = parseInt(hora[0]);
	horas += Math.abs((parseInt(hora[1])/60));
	
	return horas;
}

function dia_semana(fecha) {
	var dia = new Date(fecha);	
	return nom_dia_semana[dia.getDay()];
}

function smkGMap(lat, long, zoom, ele) {
	var mapOptions = {
    	zoom: zoom,
		center: new google.maps.LatLng(lat, long),
		panControl: true,
		zoomControl: true,
		scaleControl: true,
		streetViewControl: false
	}	
	var mapa = new google.maps.Map(document.getElementById(ele), mapOptions);  	
	return mapa;
}

function smkGMapMarca_add(mapa, posicion, titulo, editable, ir) {
	//var imagen = 'images/beachflag.png';
	var marker = new google.maps.Marker({
		position: posicion,
		map: mapa,
		//icon: imagen,
		draggable:editable,
    	title: titulo
	});
	
	if(true == ir) {
		smkGMapPos_centrar(mapa, posicion);
	}
	
	/*google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(mapa, marker);
	});*/
	
	//marker.setMap(null);
	
	return marker;
}

function smkGMapPos_centrar(mapa, posicion) {
	mapa.setCenter(posicion);
}

function smkGMapPos_crear(lat, long) {
	var pos = new google.maps.LatLng(lat, long);
	return pos;
}

function genHorasDia(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;
}

function genHorasDiaInc(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];	
	if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
		horas.push('00:' + hora_inicio.format('mmA'));
	} else {
		horas.push(hora_inicio.format('hh:mmA'));
	}	
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;
}

function formatoHora12(time) {
	return time.match(/^(0?[0-9]|1[012])(:[0-5]\d)[APap][mM]$/);
}

function oncargandoxajax() {
	$('.m3c-cargando').css('display', 'block');
}

function offcargandoxajax() {
	$('.m3c-cargando').css('display', 'none');
}

function dias_entre_fechas(fecha_i, fecha_f) {//01.05.15
	fecha_i = Date.UTC(fecha_i.getFullYear(), fecha_i.getMonth(), fecha_i.getDate(), fecha_i.getHours(), fecha_i.getMinutes());
	fecha_f = Date.UTC(fecha_f.getFullYear(), fecha_f.getMonth(), fecha_f.getDate(), fecha_f.getHours(), fecha_f.getMinutes());
	var ms = Math.abs(fecha_i - fecha_f);
	return Math.floor(ms/1000/60/60/24);
}

function getValRadio(selector) {
	return ($("."+ selector +":checked").length > 0) ? $("."+ selector +":checked").val() : '';
}

function setValId(id, valor) {
	$('#' + id).val(valor);
}

function textToFloat(text) {
	return ('' == text) ? 0 : parseFloat(text.replace(',', ''));
}

function isNumber(n) {
    n = n.replace(/\./g, '').replace(',', '.');
    return !isNaN(parseFloat(n)) && isFinite(n);
}

modal_id = 0;
function openModal(tam,titulo,url, destruir,clase,form){
    var _newmodal = $('#modalclone').clone(true,true);
    var form=form||{header:true,footer:true,borrarmodal:false};
    var showheader=form.header!=undefined?form.header:true;
    var showfooter=form.footer!=undefined?form.footer:true;
    var borrarmodal=form.borrarmodal!=undefined?form.borrarmodal:false;
    var tmpurl=form.url!=undefined?form.url:'';
    var anchomodal=form.tam!=undefined?form.tam:'';
    var callback=form.callback||undefined;
    var urladd=url||tmpurl;
    var frmtype=form.method||'GET';
    var frmdata=form.datasend||'';
    var tam=tam||'lg';
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(clase);
    _newmodal.children('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam).css('width',anchomodal);    
    _newmodal.modal({backdrop: 'static', keyboard: true});
    if(true == destruir)
    $('body').on('click', '#modal-' + modal_id + ' .close , #modal-' + modal_id + ' .cerramodal' , function (ev){ 
	    var audio=$(_newmodal.find('audio'));
	    	if(audio.length) audio.trigger('pause');
	    var video=$(_newmodal.find('video'));
	    	if(video.length) video.trigger('pause');
		_newmodal.modal('hide');
		$('body').removeAttr('style');
	if(borrarmodal)_newmodal.on('hidden.bs.modal', function(){_newmodal.remove();});
    });
    ++modal_id; 
    if(urladd){
		$.ajax({
			url: urladd,
			type: frmtype,
			data:  frmdata,
			contentType: false,
			processData: false,
			cache: false,
			dataType:'html',
			beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
			success: function(data)
			{	_newmodal.find('#modaltitle').html(titulo);
	            _newmodal.find('#modalcontent').html(data);
	            if(!showheader) _newmodal.find('.modal-header').hide();
				if(!showfooter) _newmodal.find('.modal-footer').hide();
				if(_isFunction(callback)){callback(_newmodal)};
			},error: function(e){ console.log(e); },
			complete: function(xhr){ }
	    });
	}else{
		if(_isFunction(callback)){callback(_newmodal)};
	}	
    return true;
} 

var selectedfile=function(e,obj,txt,fcall){
  var tipo= $(obj).attr('data-tipo');
  var donde= $(obj).attr('data-url');
  var fcall='&fcall='+fcall;
  if(tipo==''||tipo==undefined)return false;
  var rutabiblioteca= _sysUrlBase_+'/biblioteca/?plt=modal&robj=afile&donde='+donde+'&type='+tipo+fcall;
  var data={
    titulo:txt+' - '+tipo,
    url:rutabiblioteca,
    ventanaid:'biblioteca-'+tipo,
    borrar:true
  }
  var modal=sysmodal(data);
}

var cerrarmodal=function(){   //cerrar ventana de seleccionar file
  $('.btncloseaddinfotxt').trigger('click');
  $('.addinfotitle').html('');
}
	
var sysmodal=function(obj){
    var tam=obj.tam||'lg';
    var htmlid=$(obj.html).html()||'';
    var htmltxt=obj.htmltxt||'';
    var url=obj.url||'';
    var borrar=obj.borrar||false;
    var titulo=obj.titulo||false;
    var claseid=obj.ventanaid||'';
    var cerrarconesc=obj.cerrarconesc||true; //cerrar modal con la tecla escape
    var backdrop=obj.backdrop||'static'; //true : con fondo oscuro, false fondo transaparente //static  no se cierra al hacer click fuera del modal
    var html=htmltxt||htmlid;
    var showfooter=obj.showfooter||false;

    if($('.modal').hasClass(claseid)){
    	$('.modal.'+claseid).modal('show');
    	return false;    	
    }
    var _newmodal = $('#modalclone').clone(true,true);
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(claseid);
    _newmodal.find('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam);
    _newmodal.find('#modaltitle').html(titulo);
    $('body').on('click', '#modal-' + modal_id + ' .cerrarmodal' , function (ev){ 
    	    var audio=$(_newmodal.find('audio'));
    	    	if(audio.length) audio.trigger('pause');
    	    var video=$(_newmodal.find('video'));
    	    	if(video.length) video.trigger('pause');
    		_newmodal.modal('hide');
    		$('body').removeAttr('style'); 
    		if(borrar){
				_newmodal.on('hidden.bs.modal', function () {
					$(this).remove();
				});    			
			}
    });
    
    ++modal_id;
    if(!titulo) _newmodal.find('.modal-header').remove();
    if(!showfooter) _newmodal.find('.modal-footer').remove();
    var donde=_newmodal.find('#modalcontent');
    var modalresize=function(){
        resizemodal($(_newmodal),-20);
    }
    var rdata=sysaddhtml(donde,url,html,modalresize);    
    _newmodal.modal({backdrop: backdrop, keyboard: cerrarconesc});
     //alto maximo del modal    
    return _newmodal;
}

var sysaddhtml=function(donde,url,html,fn){
	if(url){
        donde.html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
        $.get(url, function(data) {            
            $(donde).html(data);
            if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
        });
    }else if(html){
    	 $(donde).html(html);
    	 if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
    }
   
}

var resizemodal = function(modal,ntam) {
  var body, bodypaddings, header, headerheight, height, modalheight;
  ntam=ntam||85;
  header = $(".modal-header", modal);
  footer = $(".modal-footer", modal);
  body = $(".modal-body", modal);
  modalheight = parseInt(modal.css("height"));
  headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
  footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
  bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
  height = modalheight - headerheight - footerheight - bodypaddings - ntam;
  return body.css("max-height", "" + height + "px");
};

$(document).ready(function(){
    var altoventana=$(window).height();
	if(isMobile.any()||altoventana>700){
		$('header').addClass('static');
	}
    $(window).resize(function(){
      return resizemodal($(".modal"));
    });
    $(document).mousemove(function(event){    	
    	var header= $('header');
    	if(header.hasClass('static')){
    		return false;
    	}
        header.addClass('isshow');
        var toolbar=$('aside');
        //var anchopantalla=$(window).width()-100;
        if(event.pageY<=3){
            if(!header.hasClass('sysshow')&&header.hasClass('ishide')){
               header.removeClass('ishide').slideDown('fast');
            }           
        }else{
        	var submeduactive= $('.menutop1 li.dropdown.open');
            if(!toolbar.hasClass('toolbar-toggled')&&event.pageY>100&&submeduactive.length==0){ //&&event.pageX>anchopantalla;
                header.addClass('ishide').removeClass('sysshow').slideUp('fast');
            }
        }
    });

	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$this.parents('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});

	$('.changeidioma').click(function(){
        var idi=$(this).attr('idioma');
        localStorage.setItem("sysidioma", idi);
        xajax__('', 'idioma', 'cambiaridioma', idi);        
    });
    $('.changerol').click(function(){
        xajax__('', 'Sesion', 'cambiarRol');
    });
});
var showexamen=function(menu){
	var ul=$('ul.menus-examenes');
	var lis=ul.find('li');
	if(menu=='home'){
		lis.hide();
		ul.find('.menu-'+menu).show();
	}else{
		lis.removeClass('active').show();
		ul.find('.menu-'+menu).addClass('active');
	}
}

function _sysisFile(url){
	try{
	  url=url.substr(1 + url.lastIndexOf("/"));
	  cnsole.log(url);
  	index=url.indexOf('?');
  	if(index>-1) url=url.substr(0,index);
  	if(url==''||url=='undefined'||url==undefined) return false;
  	indexpunto=url.lastIndexOf(".");
	  if(indexpunto==-1) return false;
	}catch(ex){}
  	return true;  
}

function _sysfileExists(url){
	try{
		if(!_sysisFile(url)) return false;
		var http = new XMLHttpRequest();
		http.open('HEAD', url, true);
		http.send();
		if(http.status!=404) return true;
		return false;
	}catch(ex){}
}

function _isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
$.fn.extend({
    imprimir: function() {
		var cloned = this.clone();
        var printSection = $('#printSection');
        if (printSection.length == 0) {
            printSection = $('<div id="printSection"></div>')
            $('body').append(printSection);
		}
        printSection.html(cloned.html());
        var toggleBody = $('body *:visible');
        toggleBody.hide();
        $('#printSection, #printSection *').show();
        window.print();
        printSection.remove();
        toggleBody.show();
	}	
});
var __isFunctionok=function(o){ if (typeof o!= 'function') return false; else return true;}
var __idgui=function(){	return Date.now()+(Math.floor(Math.random() * (1 - 100)) + 1);}
var __isdomok=function(e){ try{ if(e.length) return true; else return false;}catch(er){return false}}
var __isEmptyok=function(e){return !$.trim(e.html());}
var __isJsonok=function(s){try{JSON.parse(s);}catch(e){return false;}return true;}
var __subirfileok=function(obj,fn){
	var opt={file:'',nombre:'',typefile:'',guardar:false,dirmedia:'',oldmedia:'',uploadtmp:false,fntmp:'h'};
	$.extend(opt,obj);
	var mostraren=__isdomok(opt.file)?opt.file:false;
	var acepta='';
	if(opt.typefile=='imagen'){acepta='image/x-png, image/gif, image/jpeg, image/*'; opt.uploadtmp=true;}
	else if(opt.typefile=='audio') acepta='audio/mp3';
	else if(opt.typefile=='video') acepta='video/mp4, video/*';
	else if(opt.typefile=='html') acepta='.html , .htm , application/zip';
	else if(opt.typefile=='flash') acepta='.swf , .flv  , application/zip';
	else if(opt.typefile=='pdf') acepta='application/pdf , application/zip';

	var file=document.createElement('input');
		file.id='file_'+__idgui();		
		file.type='file';
		file.accept=acepta;
		
		file.addEventListener('change',function(ev){			
			if(opt.uploadtmp && mostraren){
				var rd = new FileReader();
				rd.onload = function(filetmp){					
					var filelocal = filetmp.target.result;
					mostraren.attr('src',filelocal);
					var fntm=opt.fntmp;
					if(__isFunctionok(fntm))fntm(file);
				}
				rd.readAsDataURL(this.files[0]);
			}
			if(opt.guardar){
				var iduploadtmp='';	
				if(mostraren){
					var $idprogress=$('#clone_progressup').clone(true);
					iduploadtmp='idup'+__idgui();
					$idprogress.attr('id',iduploadtmp);
					mostraren.closest('div').append($idprogress);
					opt.oldmedia=mostraren.attr('oldmedia')||'';
					iduploadtmp='#'+iduploadtmp;
				}
				var data=new FormData()
				data.append('media',this.files[0]);
				data.append('dirmedia',opt.dirmedia);
				data.append('oldmedia',opt.oldmedia);
				data.append('typefile',opt.typefile);
				data.append('nombre',opt.nombre||'');
				sysajax({ 
					fromdata:data,
		        	url:_sysUrlBase_+'/media/subir',
		        	iduploadtmp:iduploadtmp ,
		        	callback:function(rs){
		        		if(rs.code=='ok'){
		        			if(rs.media!=''){
		        				if(mostraren){
		        					mostraren.attr('src',rs.media);
		        					mostraren.attr('data-olmedia',rs.media);
		        				}
		        				if(__isFunctionok(fn))fn(rs);
		        			} 
		        		}
		        	}
		    	});
			}else{
				if(mostraren){
					var nameimagen=mostraren.attr('alt');
					file.name=nameimagen;
					file.className="input-file-invisible file"+nameimagen;
					file.style='height:0px; width:0px;';
					var hayfile=mostraren.siblings('input[type="file"]');
          			if(hayfile) hayfile.remove();
          			mostraren.parent().append(file);
				}
				
			}
		});
    file.click();
}

var __syssubirfile=function(obj){
	var el=obj;
	var tipofile=el.attr('data-tipofile')||'imagen';
	var dirmedia=el.attr('data-dirmedia')||'imagenes/';
	var oldmedia=el.attr('data-oldmedia')||'';
	var nombre=el.attr('data-nombre')||'';
	if(tipofile=='imagen'){acepta='image/x-png, image/gif, image/jpeg, image/*';}
	else if(tipofile=='audio') acepta='audio/mp3';
	else if(tipofile=='video') acepta='video/mp4, video/*';
	else if(tipofile=='html') acepta='.html , .htm , application/zip';
	else if(tipofile=='flash') acepta='.swf , .flv  , application/zip';
	else if(tipofile=='pdf') acepta='application/pdf , application/zip';
	var file=document.createElement('input');
	file.id='file_'+Date.now();		
	file.type='file';
	file.accept=acepta;
	file.addEventListener('change',function(ev){
		var dt=new FormData();
			dt.append('filearchivo',this.files[0]);
			dt.append('dirmedia',dirmedia);
			dt.append('oldmedia',oldmedia);
			dt.append('typefile',tipofile);
			dt.append('nombre',nombre);
			$.ajax({				
				contentType: false,
				processData: false,
				cache: false,
				url:_sysUrlBase_+ '/biblioteca/subirmedia',
				type: 'POST',
				dataType: 'json',
				data: dt
			}).done(function(resp){
				console.log(resp);
			}).fail(function(er) {
				console.log("error",er);
			});	
	})
	file.click();
}
var __syssubirfileimg = function(obj,id_estudio){
	var el=obj;
	var tipofile=el.attr('data-tipofile')||'imagen';
	var dirmedia=el.attr('data-dirmedia')||'imagenes/';
	var oldmedia=el.attr('data-oldmedia')||'';
	var nombre=el.attr('data-nombre')||'';
	if(tipofile=='imagen'){acepta='image/x-png, image/gif, image/jpeg, image/*';}
	else if(tipofile=='audio') acepta='audio/mp3';
	else if(tipofile=='video') acepta='video/mp4, video/*';
	else if(tipofile=='html') acepta='.html , .htm , application/zip';
	else if(tipofile=='flash') acepta='.swf , .flv  , application/zip';
	else if(tipofile=='pdf') acepta='application/pdf , application/zip';
	var file=document.createElement('input');
	file.id='file_'+Date.now();		
	file.type='file';
	file.accept=acepta;
	file.addEventListener('change',function(ev){
		var dt=new FormData();
			dt.append('filearchivo',this.files[0]);
			dt.append('dirmedia',dirmedia);
			dt.append('oldmedia',oldmedia);
			dt.append('typefile',tipofile);
			dt.append('nombre',nombre);
			$.ajax({				
				contentType: false,
				processData: false,
				cache: false,
				url:_sysUrlBase_+ '/biblioteca/subirmedia2',
				type: 'POST',
				dataType: 'json',
				data: dt
			}).done(function(resp){
				console.log(resp);
				setTimeout(redir(_sysUrlBase_+'/library/editar/?id_estudio='+id_estudio), 3000);
			}).fail(function(er) {
				console.log("error",er);
			});	
	})
	file.click();
}
$.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            if (callback) {
              callback();
            }
        });
        return this;
    }
});

function mostrarLogroObtenido(dataAjax){
	$.ajax({
		url:_sysUrlBase_+ '/alumno_logro/actualizarBandera',
		type: 'POST',
		dataType: 'json',
		data: dataAjax,
	}).done(function(resp) {
		if(resp.code=='ok'){
			var idModal = "mdl-LogroObtenido";
			var imgSrc= _sysUrlBase_ +'/'+( (resp.data[0]['imagen']!==null)?resp.data[0]['imagen']:'static/media/imagenes/trofeo.png');
			var $modal = $('#modalclone').clone();
	        $modal.attr('id', idModal);
	        $modal.find('.modal-dialog').removeClass('modal-lg');
			$modal.find('.modal-content').css({"border":"0","background-color":"transparent","box-shadow": "0 0px 0px rgba(0,0,0,.5)"});
			$modal.find('.modal-footer').css({"border":"0","display":"flex","justify-content":"center"});
			$modal.find('.modal-header').css({"border":"0","padding-bottom":"0"});
			$modal.find('.modal-header').html('<h2 style="background-color: #fff; margin: 0; padding: 1rem 2rem; border-radius: 5em; border: 5px solid #ffc219;">'+resp.data[0]['titulo']+ '</h2>');
	        $modal.find('#modalcontent').html('<p style="color: #fff; font-size: 1.2em; padding: 1rem 2rem; text-align: justify; background: rgba(51, 51, 51, 0.85); border-radius: 1em; width: 80%; margin: auto;">'+ resp.data[0]['descripcion']+ '</p>'+
	        	'<img class="img-responsive" style=" height: 200px; margin: auto; border: 15px solid #f47c27; border-radius: 50%; background: rgba(255, 255, 255, 0.94); margin-top: 7px;" src='+imgSrc+'></img>');
	        $modal.find('#modalfooter .btn.cerrarmodal').addClass('pull-right').text('Ok');
	        $('body').append($modal);
	        $('#'+idModal).modal({keyboard:false, backdrop:'static'});
		}
	}).fail(function() {
		console.log("error");
	});	
}

function _isFunction(object){ if (typeof object != 'function') return false; else return true;}

var sysajax=function(infodata){
	var datasend=infodata.fromdata||'';
	var url=infodata.url||false;
	var type=infodata.type||'json';
	var callback=infodata.callback||false;
	var callbackerror=infodata.callbackerror||false;
	var method=infodata.method||'POST';
	var msjatencion=infodata.msjatencion||'';
	var showmsjok=infodata.showmsjok||false;	
	if(!url) return;
	$.ajax({
      url: url,
      type: method,
      data:  datasend,
      contentType: false,
      processData: false,
      cache: false,
      dataType:type,
      beforeSend: function(XMLHttpRequest){ /*cargando*/ },      
      success: function(data)
      {  
        
        if(type=='json'){
	        if(data.code=='Error'){
	          mostrar_notificacion(msjatencion,data.msj,'warning');
	          if(_isFunction(callbackerror))callback(data);
	        }else{
	          if(showmsjok) mostrar_notificacion(msjatencion,data.msj,'success');
	          if(_isFunction(callback))callback(data);
	        }
	    }else{
	    	if(_isFunction(callback))callback(data);
	    }
      },
      error: function(e){ console.log(e); },
      complete: function(xhr){ }
    });
}

function esURLValida(url) {
	var pattern = new RegExp('^(https:\\/\\/)?'+ /* protocol */
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ /* domain name */
		'((\\d{1,3}\\.){3}\\d{1,3}))'+ /* OR ip (v4) address */
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ /* port and path */
		'(\\?[;&a-z\\d%_.~+=-]*)?'+ /* query string */
		'(\\#[-a-z\\d_]*)?$','i'); /* fragment locater */
	if(!pattern.test(url)) {
		alert("Please enter a valid URL with HTTPS security protocol.");
		return false;
	} else {
		return true;
	}

}

function flagSesion(flag) {
	flag = flag || 0;
	localStorage.setItem('_sysFlagCerrar_', flag)
}

var _sysTiempoSesion_ = 100*60;/* en segundos */ 
var CONT_TIME = 0;
var _sysFlagCerrar_ = 0;


localStorage.setItem('_sysFlagCerrar_',_sysFlagCerrar_);

var tiempologin=setInterval(function() {
	_sysFlagCerrar_ = localStorage.getItem('_sysFlagCerrar_') || _sysFlagCerrar_;
	CONT_TIME=localStorage.getItem('CONT_TIME') || CONT_TIME;
    //console.log(_sysFlagCerrar_);
    if(_sysFlagCerrar_==1) {
        if(CONT_TIME>=_sysTiempoSesion_){
            redir(_sysUrlBase_+'/sesion/salir/');
        }
        CONT_TIME++;
    } else {
        CONT_TIME++;
	}
	localStorage.setItem('CONT_TIME',CONT_TIME);
}, 4000);
$(document).ready(function(){
	$(document).mousemove(function(e){
		CONT_TIME = 0;
		localStorage.setItem('CONT_TIME',CONT_TIME);
	}).click(function(){
		CONT_TIME = 0;
		localStorage.setItem('CONT_TIME',CONT_TIME);
	}).focusin(function(){
		CONT_TIME = 0;
		localStorage.setItem('CONT_TIME',CONT_TIME);
	})
})
var MSJES_PHP = {};
function cargarMensajesPHP(idContenedor) {
	var $sectionMensajes = $(idContenedor);
	$sectionMensajes.find('input').each(function() {
		var id = $(this).attr('id');
		var valor = $(this).val();
		MSJES_PHP[id] = valor;
	});
};

