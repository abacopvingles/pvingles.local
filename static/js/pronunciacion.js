;
var pronunciacionsetting={
  volume:1,
  rate:0.5,
  pitch:0.5,
  voice:localStorage.getItem('voicespeach')||'',
};
var __cambiarvoice=function(voice_){ 
  pronunciacionsetting.voice=speechSynthesis.getVoices().filter(function(voice) { return voice.name ==  voice_})[0];
  localStorage.setItem('voicespeach',voice_);
}

__cargarvoces=function($ele,idi){
  try{
    //console.log('cargar voces:',idi);
    if($ele.length<1) return;
    var idioma=idi||_sysIdioma_;  
    if(!navigator.onLine) {
      var idioma= 'EN';
    }
    console.log(idioma);  
    var voices=speechSynthesis.getVoices();
    $ele.empty();
    var cambiovoice=false;
    $ele.off('change');
    $ele.attr('data-voicetmp','estecambio');
    for(var i=0; i<voices.length; i++){ 
      if(voices[i].lang.substring(0,2).toLowerCase()==idioma.toLowerCase()){
        var option = document.createElement('option');
        if(pronunciacionsetting.voice==''||pronunciacionsetting.voice==voices[i]){
            pronunciacionsetting.voice=voices[i];
            option.selected=true;
            cambiovoice=true;
        }
        option.value=voices[i].name;
        option.textContent = voices[i].name;
        $ele.append(option);
      }
    }
    $('.speachvoives').each(function(){      
      var estecambio=$(this).attr('data-voicetmp')||'';
      if(estecambio!='estecambio'){
        $(this).empty();
        $(this).html($ele.html());
      }else{
        $(this).removeAttr('estecambio');
      }
    }) // falta seleccionar speach;
  
    $ele.on('change',function(){ 
      $('.speachvoives').val($ele.val())
      __cambiarvoice($ele.val()); 
    });
     __cambiarvoice($ele.val());
  }catch(ex){console.log(ex)}
}

function guarconfigpronunciacionsetting(){
    sessionStorage.setItem("pronunciacionsetting", JSON.stringify(pronunciacionsetting));   
}
var myText = '';
var rateSlider = 1;
function checkCompatibilty () {
  if(!('speechSynthesis' in window)){
    alert('Your browser is not supported. If google chrome, please upgrade!!');
  }
};
checkCompatibilty();
var voiceMap = [];
var voice;
function loadVoices(idioma) {
  try{
    //console.log(idioma);
    var idioma=idioma||'ES';
    var txttmp=''
    var voices = speechSynthesis.getVoices();    
    if($('#voicepronunciation').length>0) $('#voicepronunciation').empty();
    for(var i=0; i<voices.length; i++){ 
        if(voices[i].lang.substring(0,2)==idioma.toLowerCase()){         
          if(pronunciacionsetting.voice==''||typeof(pronunciacionsetting.voice)!='object'){
            pronunciacionsetting.voice=voices[i];
          }
          var option = document.createElement('option');
          option.textContent = voices[i].name;       
          if($('#voicepronunciation').length>0) $('#voicepronunciation').append(option);
        }
    }
    if($('#voicepronunciation').length>0) $('#voicepronunciation').trigger('change');   
  }catch(er){console.log(er);}
};

window.speechSynthesis.onvoiceschanged = function(e){
  loadVoices(_sysIdioma_);
};

  //console.log('_sysIdioma_:',_sysIdioma_);
loadVoices(_sysIdioma_);

function speak(){
   try{
    myText = document.getElementById('texto').value;
    pronunciar(myText);  
  }catch(ex){console.log(ex)}
};

function pronunciar(txt,deletrear,idioma){  
  try{
    idioma=idioma||_sysIdioma_.toUpperCase();
    if(_sysIdioma_.toUpperCase()!=idioma){
      loadVoices(idioma);
    }
    
    deletrear=deletrear||false;
    if(txt==''||txt==undefined) return;  
    var msg = new SpeechSynthesisUtterance();
    if(deletrear==true){
      txt=txt.split("");
      msg.rate=0.02;
    }else{
      msg.rate=pronunciacionsetting.rate;
    } 
    // console.log(pronunciacionsetting.voice);
    msg.volume =  pronunciacionsetting.volume;
    msg.voice =   pronunciacionsetting.voice;
    msg.Pitch =   pronunciacionsetting.pitch;
    msg.text = txt;
    // console.log(msg);
    window.speechSynthesis.speak(msg);
  }catch(ex){console.log(ex)}
}
$(document).ready(function(){  
  $('body').on('click','.pronunciar',function(ev){
    var txt=$(this).siblings('.texto').text();
    var deletrear=$(this).hasClass('deletrear')?true:false;
    pronunciar(txt,deletrear);
  }).on('change','#voicevolume',function(e){
    pronunciacionsetting.volume=$(this).val(); 
    guarconfigpronunciacionsetting();
  }).on('change','#voiceSpeed',function(e){
    pronunciacionsetting.rate=$(this).val(); 
    guarconfigpronunciacionsetting();
  }).on('change','#voicepitch',function(e){
    pronunciacionsetting.pitch=$(this).val(); 
    guarconfigpronunciacionsetting();
  }).on('click','.cargarimagenall',function(ev){
    var txt=$(this).attr('title');
    selectedfile(ev,this,txt);
  }); 
});