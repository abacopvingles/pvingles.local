<?php
	define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
	define('SD', DIRECTORY_SEPARATOR);
	define('RUTA_BASE',	dirname(dirname(__FILE__)) . SD);
	$ruta_ini=dirname(dirname(__FILE__));	
	define('RUTA_RAIZ',$ruta_ini.SD);
	$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
	define('URL_BASE',_http_.$host.'/pvingles.local');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Configuracion de la Plataforma Ingles</title>
	<link href="../static/tema/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="logica/css/form.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">  
	  <form id="contact" action="">
	    <h3 align="center">Configuracion Smart English</h3>

	    <fieldset>
	    	<div class="row">
	    		<div class="col-sm-9">
	    			 <input placeholder="ENTER NAME EDUCATIONAL INSTITUTION" id="nombreie" type="text" tabindex="1" required >
	    		</div>
	    		<div class="col-sm-3">
	    			 <a href="#" class="btn btn-primary" id="buscar" style="padding: 8px 54px;" role="button">Buscar</a>
	    		</div>
	    	</div>

	    </fieldset>
	    <fieldset>
	    	<select id="resultado" disabled>
	    		<option disabled selected>NO HAY RESULTADOS</option>
	    	</select>
	    </fieldset>
	    <h5 align="center" style="font-weight: bold">Resultado:</h5>
	    <fieldset>
	    	<input placeholder="ID EDUCATIONAL INSTITUTION" id="idieselect" type="text" tabindex="3" required onfocus="this.blur()">
	    </fieldset>
	    <fieldset>
      		<input placeholder="NAME EDUCATIONAL INSTITUTION" id="nombreieselect" type="text" tabindex="4" required onfocus="this.blur()">
	    </fieldset>
	      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">GUARDAR</button>
	    </fieldset>
	  </form>
	</div>

	<script src="../static/media/woorkbooks/resources/js/jquery.js"></script>
	<script src="../static/tema/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {  
			// $('body').css("background","#4c5caf");  
		    function changeColor(){
		    	
		    	console.log($('body').css("background"));
		    	if ($('body').css("background") == 'rgb(76, 92, 175) none repeat scroll 0% 0% / auto padding-box border-box') {
		    		$('body').css("background","#563e65");
		    	}else{
		    		if ($('body').css("background")=='rgb(86, 62, 101) none repeat scroll 0% 0% / auto padding-box border-box') {
		    			$('body').css("background","#4c5caf");
		    		}
		    		
		    	}
		    }

		    setInterval(changeColor, 5000);

		    $( "#buscar" ).on('click',function(){
				var valor = $('#nombreie').val()
				$.ajax({
					type: 'POST',
					data: {nombre:valor},
					url: 'consultar.php',
					success: function(resp){
						$('#resultado').removeAttr("disabled")
						$('#resultado').html(resp);
						var valor1 = $('#resultado option').length;
						if (valor1 ==1) {
							$('#resultado').attr("disabled", true);
						}else{
							var valor2 = $('#resultado').val();
							$.ajax({
								type: 'POST',
								data: {institucion:valor2},
								url: 'consultar.php',
								success: function(resp){
									var datos = JSON.parse(resp);
									$('#idieselect').val(datos.idlocal);
									$('#nombreieselect').val(datos.nombre);
								}
							});
							$('#idieselect').val();
							$('#nombreieselect').val();
						}
					}
				});
			});
			$('#resultado').on('change',function(){
				var valor2 = $('#resultado').val();
				$.ajax({
					type: 'POST',
					data: {institucion:valor2},
					url: 'consultar.php',
					success: function(resp){
						var datos = JSON.parse(resp);
						$('#idieselect').val(datos.idlocal);
						$('#nombreieselect').val(datos.nombre);
					}
				});
			});
			$('#contact').submit(function(e){
				e.preventDefault();
				var valor3 = $('#idieselect').val();
				$.ajax({
					type: 'POST',
					data: {idie:valor3},
					url: 'consultar.php',
					success: function(resp){
						var datos = JSON.parse(resp);
						if (datos == 1) {
							window.location.href = '<?php echo URL_BASE;?>';
						}else{
							window.location.href = '<?php echo URL_BASE;?>/offline/configuracion.php';
						}
						console.log(datos);
					}
				});	
			});
		});
		
	</script>
</body>
</html>