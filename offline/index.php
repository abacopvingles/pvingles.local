<?php
	define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
	define('SD', DIRECTORY_SEPARATOR);
	define('RUTA_BASE',	dirname(dirname(__FILE__)) . SD);
	$ruta_ini=dirname(dirname(__FILE__));	
	define('RUTA_RAIZ',$ruta_ini.SD);
	$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
	define('URL_BASE',_http_.$host.'/pvingles.local');

	require_once 'logica/Configuracion.class.php';
	$obj = new Configuracion();
	$total = $obj->cantconfiguracion();
	// var_dump($total);
	if ($total>0) {
		// var_dump('Entre aqui urlbase');
		header("Location: ".URL_BASE);
	}else{
		// var_dump('Entre aqui conf');
		header("Location: ".URL_BASE."/offline/configuracion.php");
		//echo "<script language='JavaScript'>var prueba = 'prueba';alert(prueba);location.href = \"configuracion.php\"</script>";
	}
?>
