<?php

define("BD_TIPO", "mysql");
define("BD_SERVIDOR", "localhost");
define("BD_PUERTO", "3306");
define("BD_USUARIO", "root");
define("BD_CLAVE", "root");
define("BD_NOMBRE_BD", "pvingles.local");

// Notificar todos los errores excepto E_NOTICE
error_reporting(E_ALL ^ E_NOTICE);

