<?php

require_once 'datos/Conexion.class.php';

class Configuracion extends Conexion {

    public function cantinstituciones() {
        try {
            $sql = "SELECT count(*) from local ORDER BY nombre ASC";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function listarinstituciones() {
        try {
            $sql = "SELECT * from local ORDER BY nombre ASC";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function listarinstitucionesporvalor($valor) {
        try {
            $sql = "SELECT * from local WHERE nombre like '".$valor."%' ORDER BY nombre ASC LIMIT 0,5";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function listarinstitucionesporid($id) {
        try {
            $sql = "SELECT * from local WHERE idlocal=".$id;
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            $datos = [];
            foreach ($resultado as $value) {
                $datos = $value;
            }

            return $datos;
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function cantconfiguracion() {
        try {
            $sql = "SELECT count(*) as cantidad from configuracionoffline ORDER BY nombre ASC";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado[0]['cantidad'];
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function listarconfiguracion() {
        try {
            $sql = "SELECT * FROM configuracionoffline";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
 
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    public function guardarie($valor1,$valor2) {
        try {
            $sql = "INSERT INTO configuracionoffline values(null,'".$valor2."',".$valor1.")";
            $sentencia = $this->dbLink->prepare($sql);
            $sentencia->execute();
            return true;
 
        } catch (Exception $exc) {
            throw $exc;
            return false;
        }
    }
}

