<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		24-07-2017
 * @copyright	Copyright (C) 24-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAlumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAlumno 
{
	protected $idalumno;
	protected $apellidopaterno;
	protected $apellidomaterno;
	protected $nombre;
	protected $fechanacimiento;
	protected $sexo;
	protected $dni;
	protected $idestadocivil;
	protected $direccion;
	protected $telefono;
	protected $celular;
	protected $email;
	protected $usuario;
	protected $clave;
	protected $idproyecto;
	protected $regusuario;
	protected $regfecha;
	protected $estado;
	protected $idempresa;
	protected $img;
	protected $idcargo;
	protected $clave1;
	protected $pais;
	protected $observacion;
	
	protected $dataAlumno;
	protected $oDatAlumno;	

	public function __construct()
	{
		$this->oDatAlumno = new DatAlumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAlumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setUsarBD($bd_usar)
	{
		try {
			$this->oDatAlumno->setUsarBD($bd_usar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getUsarBD()
	{
		try {
			return $this->oDatAlumno->getUsarBD();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAlumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAlumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAlumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAlumno->get($this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try{			
			$this->oDatAlumno->iniciarTransaccion('neg_i_Alumno');
			$this->idalumno = $this->oDatAlumno->insertar($this->apellidopaterno,$this->apellidomaterno,$this->nombre,$this->fechanacimiento,$this->sexo,$this->dni,$this->idestadocivil,$this->pais,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idproyecto,$this->regusuario,$this->regfecha,$this->usuario,md5($this->clave),$this->clave1,$this->img,$this->idcargo,$this->estado,$this->idempresa,$this->observacion);
			$this->oDatAlumno->terminarTransaccion('neg_i_Alumno');	
			return $this->idalumno;
		} catch(Exception $e) {	
		    $this->oDatAlumno->cancelarTransaccion('neg_i_Alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try{						
			$this->oDatAlumno->actualizar($this->idalumno,$this->apellidopaterno,$this->apellidomaterno,$this->nombre,$this->fechanacimiento,$this->sexo,$this->dni,$this->idestadocivil,$this->pais,$this->direccion,$this->telefono,$this->celular,$this->email,$this->idproyecto,$this->regusuario,$this->regfecha,$this->usuario,md5($this->clave),$this->clave1,$this->img,$this->idcargo,$this->estado,$this->idempresa,$this->observacion);
			if(!empty($this->clave)) {
				$this->oDatUsuario->set($this->idalumno,'clave', md5($this->clave));
			}
			return $this->idalumno;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAlumno->cambiarvalorcampo($this->idalumno,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatAlumno->eliminar($this->idalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdalumno($pk){
		try {
			$this->dataAlumno = $this->oDatAlumno->get($pk);
			if(empty($this->dataAlumno)) {
				throw new Exception(JrTexto::_("Alumno").' '.JrTexto::_("not registered"));
			}
			$this->idalumno = $this->dataAlumno["idalumno"];
			$this->apellidopaterno = $this->dataAlumno["apellidopaterno"];
			$this->apellidomaterno = $this->dataAlumno["apellidomaterno"];
			$this->nombre = $this->dataAlumno["nombre"];
			$this->fechanacimiento = $this->dataAlumno["fechanacimiento"];
			$this->sexo = $this->dataAlumno["sexo"];
			$this->idestadocivil = $this->dataAlumno["idestadocivil"];
			$this->pais = $this->dataAlumno["pais"];
			$this->direccion = $this->dataAlumno["direccion"];
			$this->telefono = $this->dataAlumno["telefono"];
			$this->celular = $this->dataAlumno["celular"];
			$this->email = $this->dataAlumno["email"];
			$this->idproyecto = $this->dataAlumno["idproyecto"];
			$this->regusuario = $this->dataAlumno["regusuario"];
			$this->regfecha = $this->dataAlumno["regfecha"];
			$this->usuario = $this->dataAlumno["usuario"];
			$this->clave = $this->dataAlumno["clave"];
			$this->clave1 = $this->dataAlumno["clave1"];
			$this->img = $this->dataAlumno["img"];
			$this->idcargo = $this->dataAlumno["idcargo"];
			$this->estado = $this->dataAlumno["estado"];
			$this->idempresa = $this->dataAlumno["idempresa"];
			$this->observacion = $this->dataAlumno["observacion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataAlumno = $this->oDatAlumno->get($pk);
			if(empty($this->dataAlumno)) {
				throw new Exception(JrTexto::_("Alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAlumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function setEmail($email)
	{
		try {
			$this->email= NegTools::validar('todo', $email, false, JrTexto::_("Please enter a valid email"), array("longmax" => 180));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	
	private function setUsuario($usuario)
	{
		try {
			$this->usuario= NegTools::validar('todo', $usuario, false, JrTexto::_("Please enter a valid user"), array("longmax" => 80));
			if($this->dataAlumno['usuario'] != $usuario) {
				$existe = $this->oDatAlumno->getxusuario($usuario);
				if(!empty($existe)) {
					throw new Exception(JrTexto::_('The user entered is already in use'));
				}
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setClave($clave)
	{
		try {
			if(empty($clave)) {
				throw new Exception(JrTexto::_('Enter password'));
			}
			$this->clave= $clave;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}