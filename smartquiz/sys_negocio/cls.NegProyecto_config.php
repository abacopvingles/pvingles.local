<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-10-2017
 * @copyright	Copyright (C) 11-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto_config', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegProyecto_config 
{
	protected $idconfig;
	protected $configuracion;
	protected $valor;
	protected $idproyecto;
	
	protected $dataProyecto_config;
	protected $oDatProyecto_config;	

	public function __construct()
	{
		$this->oDatProyecto_config = new DatProyecto_config;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto_config->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function setUsarBD($bd_usar)
	{
		try {
			$this->oDatExamenes->setUsarBD($bd_usar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getUsarBD()
	{
		try {
			return $this->oDatExamenes->getUsarBD();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto_config->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto_config->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatProyecto_config->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatProyecto_config->get($this->idconfig);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('proyecto_config', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->oDatProyecto_config->iniciarTransaccion('neg_i_Proyecto_config');
			$this->idconfig = $this->oDatProyecto_config->insertar($this->configuracion,$this->valor,$this->idproyecto);
			$this->oDatProyecto_config->terminarTransaccion('neg_i_Proyecto_config');	
			return $this->idconfig;
		} catch(Exception $e) {	
		    $this->oDatProyecto_config->cancelarTransaccion('neg_i_Proyecto_config');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			if(!NegSesion::tiene_acceso('proyecto_config', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatProyecto_config->actualizar($this->idconfig,$this->configuracion,$this->valor,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Proyecto_config', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatProyecto_config->eliminar($this->idconfig);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdconfig($pk){
		try {
			$this->dataProyecto_config = $this->oDatProyecto_config->get($pk);
			if(empty($this->dataProyecto_config)) {
				throw new Exception(JrTexto::_("Proyecto_config").' '.JrTexto::_("not registered"));
			}
			$this->idconfig = $this->dataProyecto_config["idconfig"];
			$this->configuracion = $this->dataProyecto_config["configuracion"];
			$this->valor = $this->dataProyecto_config["valor"];
			$this->idproyecto = $this->dataProyecto_config["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('proyecto_config', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataProyecto_config = $this->oDatProyecto_config->get($pk);
			if(empty($this->dataProyecto_config)) {
				throw new Exception(JrTexto::_("Proyecto_config").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto_config->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}