var swi_keys_neu = Array(0, 8, 9, 16, 17, 18, 46);
var swi_keys_int = Array(0, 8, 9, 16, 17, 18);
var nom_dia_semana = Array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');

Array.prototype.existe = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}

function redir(pg){
	window.location.href=pg;	
}

function irpage(){
	document.location=document.location;
}

$(document).ready(function(){
	$('body').on('keypress', '.moneda', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_neu.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	
	$('body').on('keypress', '.entero', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_int.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	if( typeof datepicker !== 'undefined' && jQuery.isFunction( datepicker ) ) {
		$('.fecha').datepicker({'format' : 'yyyy-mm-dd'});
	}
	
	$('body').on('click', '.fecha', function () {
        $(this).datepicker({autoclose: true});
		$(this).datepicker('show');
    });
	
	$('.fecha').keydown(function(event){
		if(9 == event.keyCode) {
			$(this).datepicker('hide');
		}
	});
	
	$(document).ajaxStart(function() {
		oncargandoxajax();
	});
	
	$(document).ajaxStop(function() {
		offcargandoxajax();
	});
});

function xFancy() {
	try {
		$.fancybox.close(true);
	} catch(e){}
}

function addFancyAjax(href_, btncerrar) {
	try {
		btncerrar = ('' == btncerrar) ? false : btncerrar;
		$.fancybox({type: 'ajax', href : href_, closeBtn  : btncerrar});
	} catch(e){}
}

function addFancy(href_, btncerrar) {
	try {
		btncerrar = ('' == btncerrar) ? false : btncerrar;
		$.fancybox({type: 'iframe', href : href_, closeBtn  : btncerrar});
	} catch(e){}
}

function reloadFancyBoxAjax() {
	try {
		$.fancybox({type: 'ajax', href : $.fancybox.current.href, closeBtn  : false});
	} catch(e){}
}

function agregar_msj_interno(tipo, mensaje) {
	html = '<div class="alert alert-' + tipo + '" role="alert">'
			+ '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'
			+ '<span class="sr-only">Error: </span>' + mensaje
			+ '</div>';
	
	$('#msj-interno').empty().html(html);
}

function moneda(value, decimals) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
	separators = [',', "'", '.'];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	},
	getBrowserInfo :function(){
	    var ua= navigator.userAgent, tem, 
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	    if(/trident/i.test(M[1])){
	        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	        return 'IE '+(tem[1] || '');
	    }
	    if(M[1]=== 'Chrome'){
	        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	    }
	    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	    return {navegador:M[0],version:M[1]};
	}
};

function mostrar_notificacion(titulo, mensaje, tipo) {
	new PNotify({title: titulo, text: mensaje, type: tipo, hide: true,styling: 'bootstrap3'});
}

function diff_horas_AMPM(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(pasar_12_24(hora_i));
	h_f = pasar_hora_a_horas(pasar_12_24(hora_f));
	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	} else {
		horas = 24 - h_i + h_f;
	}
	
	return horas;
}

function pasar_12_24(hora) {
	if(!hora || '' == hora) {
		return;
	}
	
	if('PM' == hora.substr(-2)) {
		hora = hora.substr(0, 5);
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) < 12) ? parseInt(hora[0]) + 12 : parseInt(hora[0]);
		hora = hora[0] + ':' + hora[1];
	} else {
		hora = hora.substr(0, 5);
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) == 12) ? '00' : hora[0];
		hora = hora[0] + ':' + hora[1];
	}
	return hora;
}

function diff_horas(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(hora_i);
	h_f = pasar_hora_a_horas(hora_f);
	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	} else {
		horas = 24 - h_i + h_f;
	}
	
	return horas;
}

function pasar_hora_a_horas(hora) {
	hora = hora.split(':');
	horas = parseInt(hora[0]);
	horas += Math.abs((parseInt(hora[1])/60));
	
	return horas;
}

function dia_semana(fecha) {
	var dia = new Date(fecha);
	
	return nom_dia_semana[dia.getDay()];
}

function smkGMap(lat, long, zoom, ele) {
	var mapOptions = {
    	zoom: zoom,
		center: new google.maps.LatLng(lat, long),
		panControl: true,
		zoomControl: true,
		scaleControl: true,
		streetViewControl: false
	}	
	var mapa = new google.maps.Map(document.getElementById(ele), mapOptions);  	
	return mapa;
}

function smkGMapMarca_add(mapa, posicion, titulo, editable, ir) {
	//var imagen = 'images/beachflag.png';
	var marker = new google.maps.Marker({
		position: posicion,
		map: mapa,
		//icon: imagen,
		draggable:editable,
    	title: titulo
	});
	
	if(true == ir) {
		smkGMapPos_centrar(mapa, posicion);
	}
	
	/*google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(mapa, marker);
	});*/
	
	//marker.setMap(null);
	
	return marker;
}

function smkGMapPos_centrar(mapa, posicion) {
	mapa.setCenter(posicion);
}

function smkGMapPos_crear(lat, long) {
	var pos = new google.maps.LatLng(lat, long);
	return pos;
}

function genHorasDia(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');
	
	horas = [];
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}
	
	return horas;
}

function genHorasDiaInc(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');
	
	horas = [];
	
	if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
		horas.push('00:' + hora_inicio.format('mmA'));
	} else {
		horas.push(hora_inicio.format('hh:mmA'));
	}
	
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}
	
	return horas;
}

function formatoHora12(time) {
	return time.match(/^(0?[0-9]|1[012])(:[0-5]\d)[APap][mM]$/);
}

function oncargandoxajax() {
	$('.m3c-cargando').css('display', 'block');
}

function offcargandoxajax() {
	$('.m3c-cargando').css('display', 'none');
}

function dias_entre_fechas(fecha_i, fecha_f) {//01.05.15
	fecha_i = Date.UTC(fecha_i.getFullYear(), fecha_i.getMonth(), fecha_i.getDate(), fecha_i.getHours(), fecha_i.getMinutes());
	fecha_f = Date.UTC(fecha_f.getFullYear(), fecha_f.getMonth(), fecha_f.getDate(), fecha_f.getHours(), fecha_f.getMinutes());
	var ms = Math.abs(fecha_i - fecha_f);
	return Math.floor(ms/1000/60/60/24);
}

function getValRadio(selector) {
	return ($("."+ selector +":checked").length > 0) ? $("."+ selector +":checked").val() : '';
}

function setValId(id, valor) {
	$('#' + id).val(valor);
}

function textToFloat(text) {
	return ('' == text) ? 0 : parseFloat(text.replace(',', ''));
}

function isNumber(n) {
    n = n.replace(/\./g, '').replace(',', '.');
    return !isNaN(parseFloat(n)) && isFinite(n);
}

modal_id = 0;

function openModal(tam,titulo,url, destruir,clase){
    var _newmodal = $('#modalclone').clone(true,true);
    var tam=tam||'lg';
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(clase);
    _newmodal.children('modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam);    
    _newmodal.modal({backdrop: 'static', keyboard: true});
    if(true == destruir){
        $('body').on('click', '#modal-' + modal_id + ' .close' , function () {
            $(this).closest('.modal').modal('hide');
        });
    }   
    ++modal_id; 
    if(url){
        $.get(url, function(data) {
            _newmodal.find('#modaltitle').html(titulo);
            _newmodal.find('#modalcontent').html(data);
        });
    } 

    return true;
} 

var selectedfile=function(e,obj,txt,fcall){
  var tipo= $(obj).attr('data-tipo');
  var donde= $(obj).attr('data-url');
  var fcall='&fcall='+fcall;
  if(tipo==''||tipo==undefined)return false;
  var rutabiblioteca= _sysUrlBase_+'/biblioteca/?plt=modal&robj=afile&donde='+donde+'&type='+tipo+fcall;
  var data={
    titulo:txt+' - '+tipo,
    url:rutabiblioteca,
    ventanaid:'biblioteca-'+tipo,
    borrar:true
  }
  var modal=sysmodal(data);

}

var cerrarmodal=function(){   //cerrar ventana de seleccionar file
  $('.btncloseaddinfotxt').trigger('click');
  $('.addinfotitle').html('');
}
	
var sysmodal=function(obj){
    var tam=obj.tam||'lg';
    var htmlid=$(obj.html).html()||'';
    var htmltxt=obj.htmltxt||'';
    var url=obj.url||'';
    var borrar=obj.borrar||false;
    var titulo=obj.titulo||false;
    var claseid=obj.ventanaid||'';
    var cerrarconesc=obj.cerrarconesc||true; //cerrar modal con la tecla escape
    var backdrop=obj.backdrop||'static'; //true : con fondo oscuro, false fondo transaparente //static  no se cierra al hacer click fuera del modal
    var html=htmltxt||htmlid;
    var showfooter=obj.showfooter||false;

    if($('.modal').hasClass(claseid)){
    	$('.modal.'+claseid).modal('show');
    	return false;    	
    }
    var _newmodal = $('#modalclone').clone(true,true);
    _newmodal.attr('id', 'modal-' + modal_id);
    _newmodal.addClass(claseid);
    _newmodal.children('modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam);
    _newmodal.find('#modaltitle').html(titulo);
    $('body').on('click', '#modal-' + modal_id + ' .cerrarmodal' , function (ev){ 
    	    var audio=$(_newmodal.find('audio'));
    	    	if(audio.length) audio.trigger('pause');
    	    var video=$(_newmodal.find('video'));
    	    	if(video.length) video.trigger('pause');
    		_newmodal.modal('hide');
    		$('body').removeAttr('style');
    		if(borrar){
    			_newmodal.on('hidden.bs.modal', function () {
					$(this).remove();
				});    			
    		}
    });
    ++modal_id;
    if(!titulo) _newmodal.find('.modal-header').remove();
    if(!showfooter) _newmodal.find('.modal-footer').remove();
    var donde=_newmodal.find('#modalcontent');
    var modalresize=function(){
        resizemodal($(_newmodal),-20);
    }
    var rdata=sysaddhtml(donde,url,html,modalresize);    
    _newmodal.modal({backdrop: backdrop, keyboard: cerrarconesc});

     //alto maximo del modal    
    return _newmodal;
}

var sysaddhtml=function(donde,url,html,fn){
	if(url){
        donde.html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/media/imagenes/loading.gif"><br><span style="color: #006E84;">Loading</span></div>');
        $.get(url, function(data) {            
            $(donde).html(data);
            if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
        });
    }else if(html){
    	 $(donde).html(html);
    	 if(typeof fn !== 'undefined' && $.isFunction(fn))fn();
    }
   
}

var resizemodal = function(modal,ntam) {
  var body, bodypaddings, header, headerheight, height, modalheight;
  ntam=ntam||85;
  header = $(".modal-header", modal);
  footer = $(".modal-footer", modal);
  body = $(".modal-body", modal);
  modalheight = parseInt(modal.css("height"));
  headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
  footerheight = parseInt(footer.css("height")) + parseInt(footer.css("padding-top")) + parseInt(footer.css("padding-bottom"));
  bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
  height = modalheight - headerheight - footerheight - bodypaddings - ntam;
  return body.css("max-height", "" + height + "px");
};

$(document).ready(function(){
    var altoventana=$(window).height();
	if(isMobile.any()||altoventana>700){
		$('header').addClass('static');
	}
    $(window).resize(function(){
      return resizemodal($(".modal"));
    });
    $(document).mousemove(function(event){    	
    	var header= $('header');
    	if(header.hasClass('static')){
    		return false;
    	}
        header.addClass('isshow');
        var toolbar=$('aside');
        //var anchopantalla=$(window).width()-100;
        if(event.pageY<=3){
            if(!header.hasClass('sysshow')&&header.hasClass('ishide')){
               header.removeClass('ishide').slideDown('fast');
            }           
        }else{
        	var submeduactive= $('.menutop1 li.dropdown.open');
            if(!toolbar.hasClass('toolbar-toggled')&&event.pageY>100&&submeduactive.length==0){ //&&event.pageX>anchopantalla;
                header.addClass('ishide').removeClass('sysshow').slideUp('fast');
            }
        }
    });

	$(document).on('click', '.panel .clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.closest('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$this.closest('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});

	$('.changeidioma').click(function(){
        var idi=$(this).attr('idioma');
        localStorage.setItem("sysidioma", idi);
        xajax__('', 'idioma', 'cambiaridioma', idi);        
    });
    $('.changerol').click(function(){
        xajax__('', 'Sesion', 'cambiarRol');
    });
});
var showexamen=function(menu){
	var ul=$('ul.menus-examenes');
	var lis=ul.find('li');
	if(menu=='home'){
		lis.hide();
		ul.find('.menu-'+menu).show();
	}else{
		lis.removeClass('active').show();
		ul.find('.menu-'+menu).addClass('active');
	}
}

var getInstruccionTmpl = function(nameTmpl) {
    var instruccion = '';
    if(_sysIdioma_=='ES'){
	    switch(nameTmpl){
	        case 'image': instruccion = ''; break;
	        case 'audio': instruccion = ''; break;
	        case 'video': instruccion = ''; break;
	        case 'click_drag': instruccion = 'Clic en la caja de borde azul y arrastre al espacio vacío'; break;
	        case 'options': instruccion = 'Clic en una de las alternativas'; break;
	        case 'select_box': instruccion = 'Clic en el espacio vacío y luego seleccione una opción'; break;
	        case 'gap_fill': instruccion = 'Clic en el espacio vacío y digite la respuesta correcta'; break;
	        case 'true_false': instruccion = 'Seleccione verdadero o falso según corresponda'; break;
	        case 'join': instruccion = 'Agrupe las tarjetas haciendo clic primero en una de las cartas superiores y luego seleccionando una de la parte inferior'; break;
	        case 'order_simple': instruccion = 'Ordene correctamente haciendo clic en cada una de las partes que están dentro de la caja'; break;
	        case 'order_paragraph': instruccion = 'Ordene correctamente haciendo clic en cada párrafo del lado derecho'; break;
	        case 'tag_image': instruccion = 'Clic en un punto de la imagen y luego seleccione la etiqueta que considere correcta'; break;
	        default: instruccion = ''; break;
	    }
    } else {
    	switch(nameTmpl){
	        case 'image': instruccion = ''; break;
	        case 'audio': instruccion = ''; break;
	        case 'video': instruccion = ''; break;
	        case 'click_drag': instruccion = 'Click on the blue border box and drag to the empty gap'; break;
	        case 'options': instruccion = 'Click on one of the alternatives'; break;
	        case 'select_box': instruccion = 'Click on the empty gap and then select an option'; break;
	        case 'gap_fill': instruccion = 'Click on the empty gap and type the correct answer'; break;
	        case 'true_false': instruccion = 'Select true or false as appropriate'; break;
	        case 'join': instruccion = 'Group the cards by first clicking on one of the top cards and then selecting one from the bottom'; break;
	        case 'order_simple': instruccion = 'Order correctly by clicking on each of the parts that are inside of the box'; break;
	        case 'order_paragraph': instruccion = 'Order correctly by clicking on each paragraph of the right side'; break;
	        case 'tag_image': instruccion = 'Click on a point in the image and then select the tag you think is correct'; break;
	        default: instruccion = ''; break;
	    }
    }
    return instruccion;
};

function _sysisFile(url){
  	url=url.substr(1 + url.lastIndexOf("/"));
  	index=url.indexOf('?');
  	if(index>-1) url=url.substr(0,index);
  	if(url==''||url=='undefined'||url==undefined) return false;
  	indexpunto=url.lastIndexOf(".");
  	if(indexpunto==-1) return false;
  	return true;  
}

function _sysfileExists(url){
	if(!_sysisFile(url)) return false;
    var http = new XMLHttpRequest();
    http.open('HEAD', url, true);
    http.send();
    if(http.status!=404) return true;
    return false;
}

function _isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


var MSJES_PHP = {};
function cargarMensajesPHP(idContenedor) {
	var $sectionMensajes = $(idContenedor);
	$sectionMensajes.find('input').each(function() {
		var id = $(this).attr('id');
		var valor = $(this).val();
		MSJES_PHP[id] = valor;
	});
};

//manipulafor ajax
/*var _sysajax=null;
$(document).ready(function(){
	function _isFunction(object){ if (typeof object != 'function') return false; else return true;}
	var x=10;
	var _syscallbacksuccess=function(data){console.log(data)};
	var _syscallbackerror=function(data){console.log(data)};
	var _syscallbackcarga=function(e){console.log(e)};
	_sysajax=function(url,formData,method,typeresult,callbacksuccess,callbackerror,callbackcarga){
		typeresult=typeresult||'json';
		method=method||'POST';
		callbacksuccess=_isFunction(callbacksuccess)?callbacksuccess:_syscallbacksuccess;
		callbackerror=_isFunction(callbackerror)?callbackerror:_syscallbackerror;
		callbackcarga=_isFunction(callbackcarga)?callbackcarga:_syscallbackcarga;
		$.ajax({
	      url: url,
	      type: "POST",
	      data:  formData,
	      contentType: false,
	      processData: false,
	      dataType:typeresult,
	      cache: false,
	      processData:false,     
	      beforeSend: function(XMLHttpRequest){ return callbackcarga(true); },      
	      success: function(data)
	      {        
	        if(data.code==='ok'){
	          	return callbacksuccess(data);
	        }else{
	            return callbackerror(data);
	        }
	      },
	      error: function(e){ return callbackerror(e);},
	      complete: function(xhr){ return callbackcarga(false);}
	    });
	}
});*/