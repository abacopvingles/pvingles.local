var speachonline=navigator.onLine?true:false; // con o sin internet
//speachonline=false;
//variables globales 
var URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
var constraints = window.constraints = { audio: true, video: false};
navigator.getUserMedia = ( navigator.getUserMedia ||  navigator.webkitGetUserMedia ||  navigator.mozGetUserMedia ||  navigator.msGetUserMedia);
window.AudioContext = window.AudioContext || window.webkitAudioContext;
window.speechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.mozSpeechRecognition || window.oSpeechRecognition || window.msSpeechRecognition;
var mediaRecorder;

//variables offline
var recognizer, recorder, callbackManager
var isRecorderReady = isRecognizerReady = false;
var wordList=[];
var grammarIds = [];
var grammars = [];
var farsesoffline=[];
var palabrasall='';
var _url_=_sysUrlBase_+'/static/libs/pocketsphinx.js/webapp/js';
function postRecognizerJob(message, callback){
    var msg = message || {};
    if (callbackManager) msg.callbackId = callbackManager.add(callback);
    if (recognizer) recognizer.postMessage(msg);
};
function spawnWorker(workerURL, onReady){
    recognizer = new Worker(workerURL);
    recognizer.onmessage = function(event) {
      onReady(recognizer);
    };
    recognizer.postMessage({'pocketsphinx.wasm': _url_+'/pocketsphinx.wasm', 'pocketsphinx.js': _url_+'/pocketsphinx.js'});
}
function updateUI() {if(isRecorderReady && isRecognizerReady) console.log('ya grabar offline')}
function updateStatus(newStatus) {console.log(newStatus);}
function displayRecording(display){
    if (display) console.log('grabando'); //document.getElementById('recording-indicator').innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    else console.log('no grabando');
};
var recognizerReady = function() {
    isRecognizerReady = true;
    updateUI();
    updateStatus("Recognizer ready");
};
var feedGrammar = function(g, index, id) {
    if (id && (grammarIds.length > 0)) grammarIds[0].id = id.id;
    if (index < g.length) {
      grammarIds.unshift({title: g[index].title});
      postRecognizerJob({command: 'addGrammar', data: g[index].g},function(id) {feedGrammar(grammars, index + 1, {id:id});});
    }else{ recognizerReady();}
};
var feedWords = function(words) {postRecognizerJob({command: 'addWords', data: words},function() {feedGrammar(grammars, 0);});};
var initRecognizer = function() {// You can pass parameters to the recognizer, such as : {command: 'initialize', data: [["-hmm", "my_model"], ["-fwdflat", "no"]]}
    postRecognizerJob({command: 'initialize', data: [["-kws_threshold", "1e-35"]]},function() { if (recorder) recorder.consumers = [recognizer];feedWords(wordList);});
};
var pnloffline='';
if(!speachonline){
window.onload = function(){
    callbackManager = new CallbackManager();    
    spawnWorker(_url_+"/recognizer.js", function(worker) {// This is the onmessage function, once the worker is fully loaded            
        worker.onmessage = function(e) { // This is the case when we have a callback id to be called
            if (e.data.hasOwnProperty('id')) {
                var clb = callbackManager.get(e.data['id']);
                var data = {};
                if ( e.data.hasOwnProperty('data')) data = e.data.data;
                if(clb) clb(data);
            }
            if (e.data.hasOwnProperty('hyp')) {// This is a case when the recognizer has a new hypothesis
                var newHyp = e.data.hyp;
                if (e.data.hasOwnProperty('final') &&  e.data.final) newHyp = newHyp;
                if(pnloffline.length){
                    var npaldoc=pnloffline.find('.txtalu1').text().split(' ').length;
					var npalalum=newHyp.split(' ').length;
                    pnloffline.find('.edittexto_ch.alumno').addClass('hide');
                    if(npalalum <= npaldoc)	pnloffline.find('.txtalu2').text(newHyp);

                }
                
            }
            if (e.data.hasOwnProperty('status') && (e.data.status == "error")) { // This is the case when we have an error
                updateStatus("Error in " + e.data.command + " with code " + e.data.code);
            }
        };
        postRecognizerJob({command: 'lazyLoad',data: {folders: [], files: [["/", "kws.txt", "../kws.txt"],["/", "kws.dict", "../kws.dict"]]}}, initRecognizer);
    });
    audioContext = new AudioContext();
}}

$(document).ready(function(ev){
    $('body').on('click','.pnl-speach.alumno .btnGrabarAudio',function(ev){
        ev.preventDefault();
        var pnlspeach=$(this).closest('.pnl-speach');
        var btn=$(this);
        var pnl=btn.closest('.pnl-speach')
        var idgui=pnl.attr('data-idgui');
        btn.toggleClass('btn-danger');
        if(!btn.hasClass('btn-danger')){
          if(speachonline){
            btn.addClass('btn-success').children('i').removeClass('fa fa-microphone-slash ').addClass('fa fa-microphone animated infinite zoomIn');
            btn.siblings('.btnPlaytexto').addClass('disabled').attr('disabled',true);
          }
          speach_iniciargrabacion(pnl,idgui);
        }else{
          btn.addClass('btn-danger').removeClass('btn-success').children('i').removeClass('fa fa-microphone animated infinite zoomIn').addClass('fa fa-microphone-slash');
          speach_detenergrabacion(pnl,idgui);
          btn.siblings('.btnPlaytexto').removeClass('disabled').attr('disabled',false).removeClass('hidden');
        }
    }).on('click','.pnl-speach .edittexto_ch',function(ev){
		/*ev.preventDefault();
		var como=$(this).attr('data-edittextocomo')||'input';
		if($(this).find(como).length>0){
            $(this).find(como).focus();
            return;
        }
        var txt=$(this).text();
        var placeholder=$(this).attr('placeholder')||'';
		var txtold=$(this).attr('data-textoold')||'';
		var txtedit=null;
		if(como=='textarea'){
			txtedit='<textarea class="form-control" placeholder="'+placeholder+'">'+(txt!=txtold?txt:'')+'</textarea>';
		}else{
			txtedit='<input type="text" class="form-control" placeholder="'+placeholder+'" value="'+(txt!=txtold?txt:'')+'">';
		}
		$(this).html(txtedit);
        $(this).find(como).focus();*/
	}).on('focusout','.edittexto_ch input',function(ev){
		ev.preventDefault();
		var txt=$(this).val();
        $(this).closest('.edittexto_ch').html(txt);
	}).on('focusout','.edittexto_ch textarea',function(ev){
		ev.preventDefault();
		var txt=$(this).val();
        $(this).closest('.edittexto_ch').html(txt);
	}).on('click','.pnl-speach .btnWriteTexto',function(ev){
		ev.preventDefault();
		var pnlspeach=$(this).closest('.pnl-speach');
		if(pnlspeach.hasClass('editable')){
			pnlspeach.find('.edittexto_ch').trigger('click');
		}
	}).on('click','.pnl-speach .btnPlaytexto',function(ev){
		ev.preventDefault();
		if(!$(this).hasClass('inwave')){
			var pnlspeach=$(this).closest('.pnl-speach');
			speachpronunciar(pnlspeach);
		}
	})

    if(!speachonline){
        console.log('offline');      
        var haypalabra=[];
        var alternativas=[];
        var generargrama=function(){
            var ji=0;
            $('.plantilla-speach').each(function(i,v){
                ji++;
                var frtmp=$(v).find('.pnl-speach.docente .txtalu1').text().trim();
                frtmp=frtmp.replace(/[^a-zA-Z 0-9']+/gi,'');
                if(frtmp!=''||frtmp!=undefined){
                    farsesoffline.push({id:ji,frase:frtmp});
                    var txt=frtmp.toString().toUpperCase();                    
                    palabrasall=palabrasall+' '+txt;
                }
            })
            $.ajax({
                async:false,
                url: _sysUrlBase_+'/../pronunciacion/buscarjson',
                data:{palabra:palabrasall}
            }).done(function(rs) {
                try{
                    var rs=JSON.parse(rs);
                    if(rs.code==200){
                        var dt=rs.data;                   
                        $.each(dt,function(i,v){                        
                            var I=''+i.toString().toUpperCase(); 
                            console.log(v);      
                            var pt=[I,v.toString().toUpperCase()];         
                            if(haypalabra.indexOf(I)==-1){
                                wordList.push(pt);
                                haypalabra.push(I);
                            }                        
                            var alt=v.alt||'';
                            var alt2=[];
                            if(alt)                          
                                $.each(alt,function(j,k){
                                    var K=k.palabra.toString().toUpperCase();
                                    var Pr=k.pron.toString().toUpperCase();
                                    var pt2=[K,Pr];
                                    if(haypalabra.indexOf(K)==-1){
                                        wordList.push(pt2);
                                        haypalabra.push(K);
                                    }
                                    alt2.push({pa:K,pr:Pr});
                                });
                                alternativas[I]=alt2;
                        })
                    }
                }catch(err){
                    console.log("Input is " + err);
                }
            });
            $.each(farsesoffline,function(i,t){
            console.log(t.frase);
            var tsplit=t.frase.split(" ");
            console.log(tsplit);
            var gramartmp = {numStates:tsplit.length, start: 0, end:tsplit.length-1, transitions:[]};
            for(j=0;j<tsplit.length;j++){
                var to=j==tsplit.length-1?j:j+1;
                var pal=tsplit[j].toString().toUpperCase();
                var tmptr={from: j, to: to, word:pal};
                gramartmp.transitions.push(tmptr);
                if(alternativas[pal].length>0){
                    $.each(alternativas[pal],function(ii,vv){
                        var tmptr={from: j, to: to, word:vv.pa};
                        gramartmp.transitions.push(tmptr);
                    });
                }
            }
            grammars.push({title: "gram"+i, g: gramartmp});
            })
           //console.log(wordList);           
        }
        generargrama();
    }
});
var speachpronunciar=function(pnl){ 
    var txt=$(pnl).find('.edittexto_ch').text();
    pronunciar(txt);    
}
var speach_inicializar=function(){

}

var speach_iniciargrabacion=function(pnl,idgui){
    if(speachonline) speach_onlinegrabar(pnl,idgui);
    else speach_offlinegrabar(pnl,idgui);
}
var speach_detenergrabacion=function(pnl,idgui){
    if(speachonline) speach_onlinestop(pnl,idgui);
    else speach_offlinestop(pnl,idgui);
}

var streamtest=null;
var speach_onlinegrabar=function(pnl,idgui){
    chunks=[];
    navigator.getUserMedia({audio:true,video:false},function(stream){
        streamtest=stream;
		mediaRecorder = new MediaRecorder(stream);
		mediaRecorder.onstop = function(e){            
            blob = new Blob(chunks, { 'type' : 'audio/wav' });            
    		speachuploadblob(blob,pnl); 
  		}
  		mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
		mediaRecorder.start();
		speachonlinefun(pnl,idgui);		
	},function(er){
		console.log(er)
	});
}
var speach_offlinestop=function(pnl,idgui){
    try{
    if(streamtest) streamtest.getTracks()[0].stop();
    recorder && recorder.stop();
    if(mediaRecorder!=undefined)
    if(mediaRecorder.state!='inactive')  mediaRecorder.stop();
    pnloffline='';
    }catch(ex){console.log(ex)}
}

var speachonlinefun=function(pnl,idgui){
	if(window.speechRecognition == undefined)return false;
	recognizeronline = new speechRecognition();
	recognizeronline.continuous = true;
	recognizeronline.lang = "en-US";
	recognizeronline.interimResults = true;
	recognizeronline.onstart = function(){ /*console.log('reconizer start');*/ }
	recognizeronline.onend = function(){ /*console.log('recong stop');*/		
		//recognizeronline = null;
	    //setTimeout(mostrartextospeach(tpl),7000);
	}	
	recognizeronline.onresult = function(event){
		var text = '';
	    if(event.results &&  event.results.length )
	    {
	      for ( var i = event.resultIndex, len = event.results.length; i < len; ++i ){
	        text += event.results[i][0].transcript;
          }
          pnl.find('.edittexto_ch.alumno').addClass('hide');
	      pnl.find('.edittexto_ch').text(text);
	    }
 		    
	}
	recognizeronline.start();
}
var speach_onlinestop=function(pnl,idgui){
       recognizeronline.stop();
    if(streamtest) streamtest.getTracks()[0].stop();
    mediaRecorder.stop();
}
var speach_offlinegrabar=function(pnl,idgui){
    chunks=[];
    navigator.getUserMedia({audio: true},function startUserMedia(stream){
        streamtest=stream;
        var input = audioContext.createMediaStreamSource(stream);
        mediaRecorder = new MediaRecorder(stream);
        window.firefox_audio_hack = input; // Firefox hack https://support.mozilla.org/en-US/questions/984179
        var audioRecorderConfig = {errorCallback: function(x) {updateStatus("Error from recorder: " + x);}};
        recorder = new AudioRecorder(input, audioRecorderConfig);// If a recognizer is ready, we pass it to the recorder
        if (recognizer) recorder.consumers = [recognizer];
        isRecorderReady = true;
        updateUI();
        //var txt=$('#gramarid').val().trim();
        pnloffline=pnl.closest('.plantilla-speach');
        var txt=pnloffline.find('.pnl-speach.docente .txtalu1').text().trim();
        var id=0;
        // console.log(farsesoffline);
        $.each(farsesoffline,function(i,t){
            txt=txt.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
			var tf=t.frase.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
          if(txt.toString().toUpperCase()==tf.toString().toUpperCase()){
            id=t.id;
          }
        });
        mediaRecorder.onstop = function(e){  
            console.log('Entre')          
            blob = new Blob(chunks, { 'type' : 'audio/wav' });            
    		speachuploadblob(blob,pnl); 
  		}
  		mediaRecorder.ondataavailable = function(e) { chunks.push(e.data);}
		//console.log(wordList,palabrasall,farsesoffline,grammars,id);
        if (recorder && recorder.start(id)) displayRecording(true);
        mediaRecorder.start();
        }, function(e) { updateStatus("No live audio input in this browser");
    });
}
var speachuploadblob=function(miblob,pnl){//funciones globales
	var name=pnl.attr('id')||'';
	var data = new FormData();
  	data.append('filearchivo',miblob);
  	data.append('type',miblob.type);
  	data.append('name',name);
	$.ajax({
        url : _sysUrlBase_+"/biblioteca/subirblob",
        type: 'POST',
        data: data,
        contentType: false,
        processData: false,
        dataType :'json',
        success: function(res){
         if(res["code"]=='ok'){                      
             crearonda(res.namelink,pnl);
             var plantilla=pnl.closest('.plantilla-speach');
             if(pnl.hasClass('alumno'))setTimeout(textosimilar(plantilla),4000);
         }
        },error: function(e) {
            console.log('Error inesperado intententelo mas tarde',e);
        }
    });
}

var isurfer=0;
var wave=[];
var onda=function(id){
    if(wave[id]==undefined){
        wave[id] = Object.create(WaveSurfer); 
    }
    return wave[id];
}
function crearonda(url,donde){
    console.log(url);
    if(url==''||url==undefined) return;  
	var idpnl=donde.attr('id');
	isurfer++;
	var iddonde=Date.now();
    var wavesurfer=null;
    if($('#'+idpnl).find('.wavesurfer').length>0){
    	iddonde=$('#'+idpnl).find('.wavesurfer').attr('idtmp');        
        wavesurfer=onda(iddonde);
        wavesurfer.destroy();
    	$('#'+idpnl).find('.wavesurfer').remove();
        $('#'+idpnl).find('audio').remove();        
    }
    $('#'+idpnl).find('.grafico').append('<div class="wavesurfer" idtmp="'+iddonde+'" id="wave_'+iddonde+'" ></div>');
    wavesurfer=onda(iddonde);
	wavesurfer.init({
        container: document.querySelector('#wave_'+iddonde),
        waveColor: '#A8DBA8',
        progressColor: '#3B8686',
        backend: 'MediaElement',
    });
    var newurl=''; 
    
    var index1_=url.indexOf('?',0);
    var tam_ =url.length;   
    if(index1_>10)url=url.substring(0,index1_);
    url=url.replace(_sysUrlBase_,'');
    newurl=_sysUrlBase_+'/'+url+'?tmp='+Date.now();
    wavesurfer.load(newurl);
    wavesurfer.on('ready', function(){
	  wavesurfer.drawer.container.style.display = '';
	  wavesurfer.drawBuffer();
	});
    $('#'+idpnl+' .btnPlaytexto').off('click');
    $('#'+idpnl+' .btnPlaytexto').on('click',function(ev){
    	$(this).addClass('inwave').attr('id','play_'+idpnl);
    	wavesurfer.playPause();
    });
}

var mostrarondaSpeach=function($tpldocente){   
    if($tpldocente.length){ 
    console.log(tpldocente);  
        if($tpldocente.find('audio').length){            
            var url=$tpldocente.find('audio').attr('src');
            var donde=$tpldocente.find('.grafico');
            donde.html('');              
            crearonda(url,$tpldocente,true);
        }
    }
 }
 var restextosimilar=function(plantilla,pt,res,np,nok){
    np=np||1;
    nok=nok||0;	
    id=Date.now();
    var btn='<button class="btncalcular" id="btn'+id+'" style="display:none;"></button>';
    plantilla.find('.textohablado').html(res+' '+'<span class="speachtexttotal" total-palabras="'+np+'" total-ok="'+nok+'">'+pt.toFixed(0)+'%</span>'+btn);
    plantilla.find('.textohablado').removeClass('hide');
    $('#btn'+id).trigger('click');
}

 var textosimilar=function(plantilla){
	var str1=plantilla.find('.pnl-speach.docente').find('.txtalu1').text()||'';
    var str2=plantilla.find('.pnl-speach.alumno').find('.txtalu2').text()||'';
    plantilla.find('.pnl-speach.alumno').find('.txtalu2').addClass('hide');
    var stroriginal=str1;
	var pt=0;
	var result='';
	str1=str1.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
	str2=str2.toLowerCase().trim().replace(/\s+/gi,' ').replace(/  /,' ');
	str3=str1.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');
	str4=str2.replace(/[^a-zA-Z 0-9]+/gi,'').replace(/\s+/gi,' ');

	str5=str3.replace(/\s/gi,'');
    str6=str4.replace(/\s/gi,'');
	if(str5===str6){
		restextosimilar(plantilla,100,'<span class="speachtextook">'+stroriginal+'</span>',1,1);
		return;
	}
	if(str1.length==0) return;
	if(str2.length==0) return;
	if(str3===str4){
		restextosimilar(plantilla,100,'<span class="speachtextook">'+stroriginal+'</span>',1,1);
		return;
	}
	var _rstr1=str1.split(" ");
	var rstr1=str3.split(" ");
	var rstr2=str4.split(" ");	
	n1=rstr1.length;
	n2=rstr2.length;
	n3=n1>n2?n1:n2;
	var rstmp=[];
	var iok=0;
	for(var i=0; i<rstr1.length; i++){
		if(rstr1[i]==rstr2[i]){
			rstmp[i]='<span class="speachtextook">'+_rstr1[i]+'</span>';
			iok++
		}else{//ierror++;
			rstmp[i]='<span class="speachtextobad">'+_rstr1[i]+'</span>';
		}
	}
	var ok=iok;
	iok=((iok*100)/n3);
	restextosimilar(plantilla,iok,rstmp.join(' ').trim(),n3,ok);
}
//functiones offline necesarias