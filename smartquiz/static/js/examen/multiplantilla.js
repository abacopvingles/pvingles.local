var agregarIconCorregido = function(input, valor){
    valor = valor || false;
    $(input).parents('.inp-corregido.wrapper').find('span.inner-icon').remove();
    if(valor === 'good'){
        var clsIcon = 'fa fa-check color-green2';
    } else {
        var clsIcon = 'fa fa-times color-red';
    }
    if(!$(input).parents('.wrapper').hasClass('inp-corregido')){
        if( $(input).parent('.wrapper').hasClass('input-flotantes') ){
            $(input).parent('.input-flotantes.wrapper').wrap('<div class="inp-corregido wrapper"></div>');
        } else {
            $(input).wrap('<div class="inp-corregido wrapper"></div>');
        }
    }

    $(input).parents('.inp-corregido.wrapper').attr('data-corregido',valor);
    //$(input).parents('.inp-corregido.wrapper').append('<span class="inner-icon"><i class="'+clsIcon+'"></i></span>');
};

var corregirRespuesta = function(inputMCE){
  var value = $(inputMCE).val();
  var rspta_crrta = $(inputMCE).attr('data-texto');
  var srcaudio=$(inputMCE).attr('data-audio');
  if(value.length>0){
    var $imgs = $('.metod.active .tabhijo.active .plantilla img[data-mce-object="image"]');
    if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){
      var valor = 'good';
      if(srcaudio!=undefined&&srcaudio!=''){
          srcaudio=srcaudio.trim();
          srcaudio=_sysUrlBase_+'/static/media/audio/'+srcaudio;
          $('#curaudio').attr('src',srcaudio);
          $('#curaudio').trigger('play');
      }
      if($imgs.length>0){ $imgs.removeClass('gray-scale'); }
    }else{
      var valor = 'bad';
      if($imgs.length>0){ $imgs.addClass('gray-scale'); }
      if( esDBYself($(inputMCE)) ) restarPuntaje();
    }
    agregarIconCorregido(inputMCE, valor);
    $(inputMCE).attr('value', value);
    return valor;
  }
};

var agregarRsptaInput_exam = function($panel, rsptaSelec){
    var $inputClicked = $panel.find('input.isclicked[data-mce-object="input"]');
    var arrRspta = rsptaSelec.split(' - ');
    var i = 0;
    $inputClicked.each(function() {
        $(this).val(arrRspta[i]);
        corregirRespuesta(this);
        i++;
    });
};

var fnDragStart=function(ev,_obj){ //isdragable
    obj=_obj.helper;
    var d = new Date();     
    var id="idtmp"+d.getDate()+"_"+d.getMonth()+"_"+d.getFullYear()+''+d.getHours()+''+d.getMinutes()+''+d.getSeconds();
    $(obj).addClass('active').attr('id',id); 
};

var fnDragEnd=function(ev,_obj){ //isdragable
    obj=_obj.helper;       
    $(obj).closest('.tpl_plantilla').find('.isdragable').removeClass('active');
    $(obj).removeAttr('id').attr('style','position:relative');
};

var fnDrop=function(event,ui,obj){ //isdrop
    var _this = obj;
    var isDBY = esDBYself($(_this));
    var dragable=ui.draggable;
    texto=$(dragable).text().trim();    

    $(_this).val(texto);
    corregirRespuesta(_this);
};

var funcionalidaddraganddrop=function(cont){
    $('.isdragable',cont).draggable({
        start:function(event,ui){
            fnDragStart(event,ui);
        },
        stop:function(event,iu){
           fnDragEnd(event,iu) 
        }
    });

    $('.isdrop',cont).droppable({
        drop:function(event,ui){
            fnDrop(event,ui,$(this));
        }
    });
};

var prepararInput = function(inputMCE, id){
  if(!$(inputMCE).parents().hasClass('input-flotantes wrapper')){
    $(inputMCE).wrap('<div class="input-flotantes wrapper" id="wrapper-'+id+'"></div>')
  }
};

var calcularPosicion = function(inputMCE, ubicacion){
    ubicacion = ubicacion||false;
  var estilos = '';
  var posicion = $(inputMCE).position();
  var ancho = $(inputMCE).outerWidth();
  estilos += ' width:'+ancho+'px; ';
  estilos += ' left:'+posicion.left+'px; ';
  if( ubicacion == 'up' ){
    var top = $(inputMCE).outerHeight() * (-1);
    estilos += ' top: '+top+'px; ' ;
  }
  return estilos;
};

var removerTodosFlotantes = function(idPanel){
    console.log(' -- removerTodosFlotantes() --');
    var $inputsMCE = $(idPanel).find('input.open');
    $(idPanel).find('.helper-zone').remove();
    $(idPanel).find('ul.options-list').remove();
    var $inputsMCE = $(idPanel).find('input[data-mce-object="input"]');
    $inputsMCE.each(function() {
        if( $(this).parents().hasClass('input-flotantes wrapper') ){
            $(this).parents('.input-flotantes.wrapper').children().unwrap();
        }
    });
};

var handlerInputMCE_focusin = function(e,params){
    params=params||false;
    var idPanel =  params.id;
    var _this =  params.obj;
    e.preventDefault();
    if(!$(_this).hasClass('open') ){
        console.log("pasando");
        $(idPanel).find('input.mce-object-input').removeClass('open active');
        removerTodosFlotantes(idPanel); 
    }
    if( !$(_this).hasClass('open') ){
        $(_this).addClass('open active');
        var id = generarID(_this);
        prepararInput(_this, id);
        // console.log("fix1",$(_this),$(_this).attr('class'),$(_this)[0].classList.contains('mce-object-input'));
        // if(!$(_this).hasClass('mce-object-input')){
        //     console.log("no lo tiene");
        // }
        $(_this).addClass('mce-object-input');
        // console.log("fix2",$(_this),$(_this).attr('class'));
        if( $(_this).hasClass('isayuda') ){
            desplegarAyuda(_this, id);
        }

        if( $(_this).hasClass('ischoice') ){
            desplegarOpciones(_this, id);
        }

        if( !$(_this).hasClass('iswrite') ) {
            //$(_this).attr('readonly', 'readonly');
        } else  {console.log("va a enfocar"); $(_this).focus();}
    }
};

var desplegarAyuda = function(inputMCE, id){
  var styles = calcularPosicion(inputMCE, 'up');
  var html = '<div class="helper-zone" id="help-'+id+'" style="'+styles+'">'+
  '<a href="#" class="btn-help"><i class="fa fa-question-circle"></i>&nbsp;Help!</a>'+
  '</div>';
  $(inputMCE).before(html);
};

var desplegarOpciones = function(inputMCE, id){
  var id = $(inputMCE).attr('id');
  var opc = $(inputMCE).attr('data-options');
  var opc_corr = $(inputMCE).attr('data-texto');
  var list = '';
  if(opc!='' && opc!=undefined){
    var arr_opc = opc.split(',');
    $.each(arr_opc, function(key, val) {
      list += '<li class="opt-item"><a href="#">'+val.trim()+'</a></li>';
    });
  }
  var styles = calcularPosicion(inputMCE, 'down');
  var html_opt = '<ul class="options-list" id="opts-'+id+'" style="'+styles+'">'+ list + '</ul>';
  $(inputMCE).after(html_opt);
  
  var correcta = '<li class="opt-item"><a href="#">'+opc_corr+'</a></li>';
  desordenarRspta('#opts-'+id, correcta);
};

var generarIdPanelEjerc = function($panelEjercicio){
    var now = Date.now();
    var idPanel = 'panel_ejerc_'+now;
    $panelEjercicio.attr('id', idPanel);
    return idPanel;
};

(function($){
    $.fn.examMultiplantilla=function(opciones){
        return this.each(function(){
            var objcont=$(this);
            var mobilhacklife = false;

            funcionalidaddraganddrop(objcont);
            objcont.on('focusin',  'input.mce-object-input', function(e){
                var $panelEjerc = $(this).closest('.panel.panelEjercicio');
                var idpnl=$panelEjerc.attr('id') || generarIdPanelEjerc($panelEjerc);
                var params = {
                    "id" : '#'+idpnl,
                    "attention_text" : MSJES_PHP.attention,
                    "obj" : this,
                };
                handlerInputMCE_focusin(e, params);
            }).on('focusout', 'input.mce-object-input', function(e){
                e.preventDefault();
                console.log("desenfocado");
                var _this = this;
                if(!$(_this).hasClass('iswrite') ) {
                    return false 
                }
                corregirRespuesta(_this);
            }).on('click', 'ul.options-list>li>a', function(e){
                e.preventDefault();
                e.stopPropagation();
                var _this = this;
               // var isDBY = esDBYself( $(_this));
                var fn = function(){
                    var value = $(_this).text();
                    var input = $(_this).parents('ul.options-list').siblings('input.open');
                    input.val(value);
                    cerrarFlotantes(input);
                   // corregirRespuesta(input);
                };

                /*if(isDBY){
                    var isCompleto = inputMCE_Completo( $(_this) );
                    var condicion = (isDBY && !isCompleto);
                    var buscar = 'input.mce-object-input',
                    buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                    fnTiempoIntentosPuntaje(condicion, fn, buscar, buscarCorrectos);
                }else{*/
                    var value = $(this).text();
                    var input = $(this).parents('ul.options-list').siblings('input.open');
                    input.val(value);
                    cerrarFlotantes(input);
                    corregirRespuesta(input);
                    //calcularBarraProgreso();
                //}
            }).on('click', 'div.helper-zone>a.btn-help', function(e){
                e.preventDefault();
                e.stopPropagation();
               /* var $tmplActiva = getTmplActiva();
                var $plantActiva = $(this).closest('.plantilla');
                var isDBY = esDBYself( $(this));
                if( $plantActiva.hasClass('tiempo-acabo') && isDBY){
                    return true;
                }*/
                var $inputMCE = $(this).parent().siblings('input[data-mce-object="input"]');
                if( $(this).find('span.tip-word').length==0 ){
                    $(this).append('<span class="tip-word"> : </span>');
                }
                var rspta= $(this).parent().siblings('input.open').attr('data-texto');
                var arr_rspta= rspta.split("");
                var i = $('span.tip-word').attr('data-pulsado');
                if(i==''||i==undefined){
                    i = 0;
                }
                i = parseInt(i);
                if( i < arr_rspta.length){
                    $('span.tip-word').attr('data-pulsado',(i+1));
                    var texto = $('span.tip-word').text();
                    texto = texto.concat(arr_rspta[i]);
                    $('span.tip-word').text(texto);
                }
                $inputMCE.focus();
            }).on('keypress', 'input.mce-object-input', function(e){
                if(e.which == 13){ 
                    e.preventDefault();
                    if( $(this).parents('.wrapper').hasClass('input-flotantes') ){
                        $(this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
                        $(this).parents('.input-flotantes.wrapper').find('.options-list').remove();
                    }
                    $(this).removeClass('active open');
                    if($(this).parents('.wrapper').hasClass('input-flotantes')){
                        $(this).parents('.input-flotantes.wrapper').children().unwrap();
                    }

                    var value = $(this).val();
                    var rspta_crrta = $(this).attr('data-texto');
                    rspta_crrta = rspta_crrta.replace(/''/gi, '"');
                    _this=$(this);
                    var fn = function(){
                        corregirRespuesta(_this);
                        //calcularBarraProgreso();
                    };
                    if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){
                        var valor = 'good';
                        agregarIconCorregido(this, valor);
                        fn();
                    }else{
                         var valor = 'bad';
                        agregarIconCorregido(this, valor);
                        fn();
                    }
                }
            }).on('keyup', 'input.mce-object-input', function(e){
                var _this = this;
                var buscar = 'input.mce-object-input',
                    buscarCorrectos = '.inp-corregido[data-corregido="good"]';
                if(e.which != 13){ 
                    var value = $(this).val();
                    var rspta_crrta = $(this).attr('data-texto');
                    rspta_crrta = rspta_crrta.replace(/''/gi, '"');
                    if(value.length>0){
                        if(rspta_crrta.toLowerCase().trim() === value.toLowerCase().trim()){
                            if( $(this).parents('.wrapper').hasClass('input-flotantes') ){
                                $(this).parents('.input-flotantes.wrapper').find('.helper-zone').remove();
                                $(this).parents('.input-flotantes.wrapper').find('.options-list').remove();
                            }
                            $(this).removeClass('active open');
                            if($(this).parents('.wrapper').hasClass('input-flotantes')){
                                $(this).parents('.input-flotantes.wrapper').children().unwrap();
                            }
                            var fn = function(){
                               corregirRespuesta(_this);
                               //calcularBarraProgreso();
                            };
                            var valor = 'good';
                            agregarIconCorregido(this, valor);
                            
                            $(this).addClass('esteejecuta'); // siguiente tan faltaria
                            var inputs=$(this).closest('.plantilla').find('input.mce-object-input')
                            var encontro=false;
                            $.each(inputs,function(){
                                if(encontro==true) {
                                    $(this).trigger('focusin');
                                    encontro=false;
                                }
                                if($(this).hasClass('esteejecuta')){
                                    $(this).removeClass('esteejecuta');
                                    encontro=true;
                                }
                            })
                            $(this).removeClass('esteejecuta');
                        }
                    }
                }
            }).on('click', '.panelAlternativas ul.alternativas-list li.alt-item a', function(e){
                e.preventDefault();
                e.stopPropagation();
                var pnlejericio=objcont.find('.panelEjercicio');
                var seleccionada = $(this).text();
                $('ul.alternativas-list li.alt-item a').each(function() {
                    if( $(this).parent().hasClass('wrapper') ){
                        $(this).siblings('span.inner-icon').remove();
                        $(this).unwrap();
                    }
                });
                var $inpClicked = objcont.find('input.isclicked[data-mce-object="input"]');                   
                var arr_opc_corr = [];
                if($inpClicked.length>0){
                    $inpClicked.each(function() {
                        var opc_corr = $(this).attr('data-texto');
                        arr_opc_corr.push(opc_corr);
                    });
                }
                var rspta_crrta = arr_opc_corr.join(' - ');
                if(rspta_crrta === seleccionada){
                    //agregarIconCorregido(this, 'good');
                    agregarRsptaInput_exam(pnlejericio,seleccionada);
                } else {        
                    //agregarIconCorregido(this, 'bad');
                    agregarRsptaInput_exam(pnlejericio,seleccionada);
                }
                //calcularBarraProgreso();
            }).on('click', 'ul.nav-tabs.ejercicios li a', function (e, esSgte) {
                esSgte=esSgte||false;
                var $li = $(this).parent();
                if( $li.hasClass('disabled') && !esSgte){
                    e.preventDefault();
                    e.stopPropagation();
                }if(esSgte){
                    if( $li.prev('li').hasClass('disabled') ) $li.prev('li').removeClass('disabled');
                    if( $li.hasClass('disabled') ) $li.removeClass('disabled');
                }
            }).on('click', function(ev) {
                var panelEjercicio=$(this).find('.panelEjercicio');
                var $input = $('input.mce-object-input.open',panelEjercicio);
                if(objcont.find('.options-list').length > 0 && $input.length == 0){
                    var _obj = objcont.find('.options-list').closest('.input-flotantes.wrapper').find('input.open.active');
                    if(!_obj.hasClass('mce-object-input')){
                        _obj.addClass('mce-object-input');
                    }
                    $input = _obj;
                }
                if($(window).width() <= 768){
                    if(objcont.find('.options-list').length > 0 || objcont.find('.input-flotantes').length > 0 ){
                        mobilhacklife = !mobilhacklife;
                    }else{
                        mobilhacklife = false;
                    }
                    // console.log(mobilhacklife);
                    if(!mobilhacklife){
                        cerrarFlotantes($input);
                    }
                }else{
                    cerrarFlotantes($input);
                }
            }).on('mousedown', function(e) {
              //if(_cureditable==true) return false;               
               //iniciarTiempo(finTiempo);
            });
            return this;
        });
    }
}(jQuery));



 
