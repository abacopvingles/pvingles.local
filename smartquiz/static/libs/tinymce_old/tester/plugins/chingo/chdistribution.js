
tinymce.PluginManager.add('chingodistribution', function(editor, url){ 
    function showDialog(){
        var dom=tinyMCE.activeEditor.dom;
        tpl1=dom.hasClass('tpl1','active');      
        if(tpl1){
            eltxt=new tinymce.dom.DomQuery(dom.get('tpl1_txt'));
            elalter=new tinymce.dom.DomQuery(dom.get('tpl1_alter'));
            dom.removeClass('tpl1','active');
            dom.addClass('tpl1','hide');
            dom.removeClass('tpl2','hide');
            dom.addClass('tpl2','active'); 
            dom.setHTML('tpl2_txt', eltxt.html());
            dom.setHTML('tpl2_alter', elalter.html());
        }else{
            eltxt=new tinymce.dom.DomQuery(dom.get('tpl2_txt'));
            elalter=new tinymce.dom.DomQuery(dom.get('tpl2_alter'));
            dom.removeClass('tpl2','active');           
            dom.addClass('tpl2','hide');          
            dom.removeClass('tpl1','hide');           
            dom.addClass('tpl1','active');
            dom.setHTML('tpl1_txt', eltxt.html());
            dom.setHTML('tpl1_alter', elalter.html());
        }
    }
    editor.addButton('chingodistribution', {
        tooltip: 'change distribution',
        icon: 'fa fa-th-large',
        onclick: showDialog,
        stateSelector: ['img[data-mce-object=image]']
    });
    editor.addMenuItem('chingodistribution', {
        icon: 'fa fa-th-large',
        text: 'change distribution',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceImage', showDialog);
    this.showDialog = showDialog;
});