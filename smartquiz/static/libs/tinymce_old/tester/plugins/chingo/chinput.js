
tinymce.PluginManager.add('chingoinput', function(editor, url) { 
    var embedChange = (tinymce.Env.ie && tinymce.Env.ie <= 8) ? 'onChange' : 'onInput';
    function showDialog(){
        var rutabase=tinymce.baseURL+'/../../../../';
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var txt=element.getContent();
        data = getData(editor.selection.getNode());       
        if(data['data-texto'])txt=data['data-texto'];
        if((txt==''||txt==undefined)) return false; 
        var _css=[],_option;    
        if(data["class"]){
            var _css=data["class"].split(" ");
        }    
        var _ayuda=_css.indexOf("isayuda")>=0?true:false; 
        var _write=_css.indexOf("iswrite")>=0?true:false;
        var _drop=_css.indexOf("isdrop")>=0?true:false;
        if(data['data-options'])_option=data['data-options'];  
        var generalFormItems = [
            {name: 'texto',  type: 'textbox', size: 40, autofocus: true, label: 'texto', value:txt.trim()},
            {name: 'ayuda', type: 'checkbox', checked: _ayuda, text: 'con ayuda'},
            {name: 'write', type: 'checkbox', checked: _write, text: 'con write'},
            {name: 'drop', type: 'checkbox', checked: _drop, text: 'con drop'},
            {name: 'options', type: 'textbox', label: 'Alternatives' , placeholder:'Ej: option1,option2' ,value:_option}
        ];
       
        win = editor.windowManager.open({
            title: 'Insert/edit Input',
            data: data,
            body: [
                {
                    title: 'General',
                    type: "form",                    
                    items: generalFormItems
                }
            ],
            onSubmit: function() {
                var beforeObjects, afterObjects, i, y;
                beforeObjects = editor.dom.select('input[data-mce-object]');
                editor.insertContent(dataToHtml(this.toJSON()));
                afterObjects = editor.dom.select('input[data-mce-object]');

                // Find new image placeholder so we can select it
                for (i = 0; i < beforeObjects.length; i++) {
                    for (y = afterObjects.length - 1; y >= 0; y--) {
                        if (beforeObjects[i] == afterObjects[y]) {
                            afterObjects.splice(y, 1);
                        }
                    }
                }

                editor.selection.select(afterObjects[0]);
                editor.nodeChanged();
            }
        });
    }

    function getSource() {
        var elm = editor.selection.getNode();
        if (elm.getAttribute('data-mce-object')) {
            return editor.selection.getContent();
        }
    }

    function dataToHtml(data) {
        var html = '',_addcss='';
        if (!data.texto) {
            data.texto = '';
        }
        console.log(data.ayuda);
        if (data.ayuda==true) _addcss += ' isayuda';
        if (data.write==true) _addcss += ' iswrite';
        if (data.drop==true) _addcss += ' isdrop';
        if (!data.options) {
            data.options = '';
        }
        html='<input type="text" data-texto="'+data.texto+'" data-options="'+data.options+'" class="mce-object-input valinput '+_addcss+'" />'; 
        return html;
    }

    function htmlToData(html) {
        var data = {};        
        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',
            start: function(name, attrs) {                             
                data = tinymce.extend(attrs.map, data);              
            }
        }).parse(html);
       // console.log(data);
        return data;
    }

    function getData(element) {
        if (element.getAttribute('data-mce-object')) {
            return htmlToData(editor.serializer.serialize(element, {selection: true}));
        }

        return {};
    }

    function sanitize(html) {
        if (editor.settings.media_filter_html === false) {
            return html;
        }

        var writer = new tinymce.html.Writer(), blocked;

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: false,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty) {
                blocked = true;

                for (var i = 0; i < attrs.length; i++) {
                    if (attrs[i].name.indexOf('on') === 0) {
                        return;
                    }

                    if (attrs[i].name == 'style') {
                        attrs[i].value = editor.dom.serializeStyle(editor.dom.parseStyle(attrs[i].value), name);
                    }
                }

                writer.start(name, attrs, empty);
                blocked = false;
            },

            end: function(name) {
                if (blocked) {
                    return;
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    function updateHtml(html, data, updateAll) {
        var writer = new tinymce.html.Writer();
        var sourceCount = 0, hasImage;

        function setAttributes(attrs, updatedAttrs) {
            var name, i, value, attr;

            for (name in updatedAttrs) {
                value = "" + updatedAttrs[name];

                if (attrs.map[name]) {
                    i = attrs.length;
                    while (i--) {
                        attr = attrs[i];

                        if (attr.name == name) {
                            if (value) {
                                attrs.map[name] = value;
                                attr.value = value;
                            } else {
                                delete attrs.map[name];
                                attrs.splice(i, 1);
                            }
                        }
                    }
                } else if (value) {
                    attrs.push({
                        name: name,
                        value: value
                    });

                    attrs.map[name] = value;
                }
            }
        }

        new tinymce.html.SaxParser({
            validate: false,
            allow_conditional_comments: true,
            special: 'script,noscript',

            comment: function(text) {
                writer.comment(text);
            },

            cdata: function(text) {
                writer.cdata(text);
            },

            text: function(text, raw) {
                writer.text(text, raw);
            },

            start: function(name, attrs, empty){                

                if (updateAll){                   
                    setAttributes(attrs, {
                        texto: data.texto,
                        ayuda: data.ayuda,
                        drop: data.drop,
                        write: data.write
                    });
                }
                writer.start(name, attrs, empty);
            },

            end: function(name) {
                if (name == "input" && updateAll && !hasImage) {
                    var imgAttrs = [];
                    imgAttrs.map = {};
                    writer.start("img", imgAttrs, true);
                }

                writer.end(name);
            }
        }, new tinymce.html.Schema({})).parse(html);

        return writer.getContent();
    }

    editor.on('ResolveName', function(e) {
        var name;
        if (e.target.nodeType == 1 && (name = e.target.getAttribute("data-mce-object"))) {
            e.name = name;
        }
    });

    editor.on('preInit', function() {
         // Converts iframe, video etc into placeholder images
        editor.parser.addNodeFilter('input', function(nodes, name) {
            var i = nodes.length, ai, node, placeHolder, attrName, attrValue, attribs, innerHtml;
           

            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }
                placeHolder = new tinymce.html.Node('img', 1);
                placeHolder.shortEnded = true;

                // Prefix all attributes except width, height and style since we
                // will add these to the placeholder
                attribs = node.attributes;
                ai = attribs.length;
                while (ai--) {
                    attrName = attribs[ai].name;
                    attrValue = attribs[ai].value;
                    placeHolder.attr(attrName, attrValue);
                    
                }

                placeHolder.attr({
                    width: "100",
                    height:  "30" ,
                    style: node.attr('style'),
                    //src: tinymce.Env.transparentSrc,
                    "data-mce-object": name,
                    //"class": "mce-object-" + name
                });

                node.replace(placeHolder);
            }
        });

        // Replaces placeholder images with real elements for video, object, iframe etc
        editor.serializer.addAttributeFilter('data-mce-object', function(nodes, name) {
            var i = nodes.length, node, realElm, ai, attribs, innerHtml, innerNode, realElmName;

            while (i--) {
                node = nodes[i];
                if (!node.parent) {
                    continue;
                }

                realElmName = node.attr(name);
                realElm = new tinymce.html.Node(realElmName, 1);

                realElm.attr({
                    style: node.attr('style')
                });

                // Unprefix all placeholder attributes
                attribs = node.attributes;
                ai = attribs.length;
                while (ai--) {
                    var attrName = attribs[ai].name;
                    realElm.attr(attrName, attribs[ai].value);
                   
                }               
                node.replace(realElm);
            }
        });
    });
    editor.on('ObjectSelected', function(e) {
        var objectType = e.target.getAttribute('data-mce-object');
    });    
    editor.addButton('chingoinput', {
        tooltip: 'Insert/edit textbox',
        icon: 'fa fa-edit',
        onclick: showDialog,
        stateSelector: ['input[data-mce-object=input]']
    });
    editor.addMenuItem('chingoinput', {
        icon: 'fa fa-edit',
        text: 'Insert/edit textbox',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceInput', showDialog);
    this.showDialog = showDialog;
});