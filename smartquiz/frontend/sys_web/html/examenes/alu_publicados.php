<style>
    .portada{
        overflow: hidden;
    }
    .portada img{
        height: 140px;
    }
</style>

<!--div class="alert alert-default panel-filtros">
  <div class="col-xs-6 col-sm-4 col-md-3">
  	<div class="cajaselect"> 
  		<select name="level" id="level-item" class="conestilo">
         	<option value="" ><?php echo JrTexto::_("All Levels")?></option>
         	<?php if(!empty($this->niveles))
                 foreach ($this->niveles as $nivel){?>
                 <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
          <?php }?>
        </select>
    </div>
  </div>
  <div class="col-xs-6 col-sm-4 col-md-3">
  	<div class="cajaselect"> 
  	<select name="level" id="unit-item" class="conestilo">
      	<option value="" ><?php echo JrTexto::_("All Unit")?></option>
      	<?php if(!empty($this->unidades))
                 foreach ($this->unidades as $unidad){?>
                 <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
          <?php }?>
    </select>
    </div>
  </div>
  <div class="col-xs-6 col-sm-4 col-md-3">
  	<div class="cajaselect"> 
  	<select name="level" id="activity-item" class="conestilo">
      	<option value="" ><?php echo JrTexto::_("All Activity")?></option>
      	 <?php if(!empty($this->actividades))
                 foreach ($this->actividades as $act){?>
                 <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
          <?php }?>
    </select>
    </div>
  </div>
  <div class="col-xs-6 col-sm-4 col-md-3">
  	<div class="cajaselect"> 
  	<select name="tipo" id="tipo-examen" class="conestilo">
      	<option value="" ><?php echo JrTexto::_("All Types")?></option>
      	<?php if(!empty($this->tipoexamen))
                 foreach ($this->tipoexamen as $tip){?>
                 <option value="<?php echo $tip["idtipo"]; ?>" <?php echo $tip["idtipo"]==$this->tipo?'Selected':'';?>><?php echo ucfirst(JrTexto::_($tip["tipo"])); ?></option>
          <?php }?>   	
    </select>
    </div>
  </div>
  <div class="clearfix"></div>
</div-->
<div class="panel panel-info">
	<div class="panel-heading row">
		<!--a href="#" class="btn btn-primary"><?php echo JrTexto::_("Publish") ?></a-->
		<div class="col-xs-6 col-sm-4 pull-right">
			<div class="input-group">
				<!--span class="input-group-addon"><?php echo ucfirst(JrTexto::_("Search")); ?></span-->
				<input type="text" class="form-control" name="txtTitulo" id="txtTitulo" placeholder="<?php echo ucfirst (JrTexto::_("Search")); ?>...">
				<span role="button" class="input-group-addon btn buscar-exam"><i class="fa fa-search"></i></span>
			</div>
		</div>	
	</div>
	<div class="panel-body pnllisexamenes">
		<?php 
		if(!empty($this->examenes))
		foreach ($this->examenes as $examen){
			if(!empty($examen["portada"]))
				$portada=@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$examen["portada"]);
			else
				$portada=$this->documento->getUrlStatic().'/img/sistema/examen_default.png';
			?>
			<div class="col-xs-6 col-sm-4 col-md-2 " style="padding: 0.7ex">
			    <a href="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.$examen["idexamen"];?>" title="<?php echo $examen["titulo"]; ?>" class="">
				<div class="exa-item">
					<div class="titulo"><?php echo $examen["titulo"]; ?></div>
					<div class="portada"><img class="img-responsive" width="100%" src="<?php echo $portada;?>"></div>
				</div>
				</a>
			</div>
		<?php } ?>			
	</div>	
</div>

<script type="text/javascript">
$(document).ready(function(){
//recargar listado de examenes		
	var restaurarListado = function(){
		$('#level-item').val('');
		$('#unit-item').val('');
		$('#activity-item').val('');
		$('#tipo-examen').val('')
		$('#txtTitulo').val('');
		cargarexamenes();
	};
	var cargarexamenes=function(){
		var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
		var nivel=$('#level-item').val()||0;
		var unidad=$('#unit-item').val()||0;
		var actividad=$('#activity-item').val()||0;
		var tipo=$('#tipo-examen').val()||0;
		var titulo=$('#txtTitulo').val()||'';
		var url=_sysUrlBase_+'/examenes/publicadosjson/'+nivel+'/'+unidad+'/'+actividad+'/'+tipo;
		try{
			var formData = new FormData();
			formData.append("nivel", nivel);
			formData.append("unidad", unidad);
			formData.append("actividad", actividad);
			formData.append("tipo", tipo);
			formData.append("titulo", titulo);
			$.ajax({
				url: url,
				type: "POST",
				data:  formData,
				contentType: false,
				dataType :'json',
				cache: false,
				processData:false,
				beforeSend: function(XMLHttpRequest){
					$('.pnllisexamenes').html('<div style="text-align: center; margin-top: 2em;"><img src="'+_sysUrlStatic_+'/img/sistema/loading.gif"><br><span style="font-size: 1.2em; font-weight: bolder; color: #006E84;"><?php echo ucfirst(JrTexto::_("loading")); ?>...</span></div>') 
				},
				success: function(data)
				{
					if(data.code==='ok'){
						var btnLimpiarBusqueda = '<button class="btn btn-default limpiar-busqueda">Limpiar búsqueda</button>';
						var examenes=data.data;
						html='';
						if(examenes)
							$.each(examenes,function(i,obj){
								if(obj.portada)
									var portada=obj.portada.replace(/__xRUTABASEx__/gi,_sysUrlBase_);
								else
									var portada=_sysUrlStatic_+'/img/sistema/examen_default.png';
								html+='<div class="col-xs-6 col-sm-4 col-md-2 " style="padding: 0.7ex">'
								+'<a href="'+_sysUrlBase_+'/examenes/resolver/?idexamen='+obj.idexamen+'" title="'+obj.titulo+'">'
								+'<div class="exa-item">'
								+'<div class="titulo">'+obj.titulo+'</div>'
								+'<div class="portada"><img class="img-responsive" width="100%" src="'+portada+'"></div>'
								+'</div></a></div>'
							});
						if(html!=''){
							$('.pnllisexamenes').html(html);
						}else{
							$('.pnllisexamenes').html('<div class="col-xs-12 text-center"><h3><?php echo JrTexto::_("No data for this search"); ?></h3></div>');
						}	
						if( $('#txtTitulo').val()!='' ){
							$('.pnllisexamenes').append('<div class="col-xs-12 text-center">'+btnLimpiarBusqueda+'</div>');
						}
					}else{
						mostrar_notificacion(msjeAttention, data.mensaje, 'warning');
					}
					$('#procesando').hide('fast');
					return false;
				},
				error: function(xhr,status,error){
					mostrar_notificacion(msjeAttention, status, 'warning');
					$('#procesando').hide('fast');
					return false;
				}               
			});
		}catch(error){
			mostrar_notificacion(msjeAttention, status, 'warning');
		}
	};

//recargar combos de niveles 
	var leerniveles=function(data){
		try{
			var res = xajax__('', 'niveles', 'getxPadre', data);
			if(res){ return res; }
			return false;
		}catch(error){
			return false;
		}       
	};
	var addniveles=function(data,obj){
		var objini=obj.find('option:first').clone();
		obj.find('option').remove();
		obj.append(objini);
		if(data!==false){
			var html='';
			$.each(data,function(i,v){
				html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
			});
			obj.append(html);
		}
		id=obj.attr('id');
		if(id==='activity-item')	cargarexamenes();
	};

	$('#level-item').change(function(){
		var idnivel=$(this).val();
		var data={tipo:'U','idpadre':idnivel}
		var donde=$('#unit-item');
		if(idnivel!=='') addniveles(leerniveles(data),donde);
		else addniveles(false,donde);
		donde.trigger('change');
	});
	$('#unit-item').change(function(){
		var idunidad=$(this).val();
		var data={tipo:'L','idpadre':idunidad}
		var donde=$('#activity-item');
		if(idunidad!=='') addniveles(leerniveles(data),donde);
		else addniveles(false,donde);
	});
	$('#activity-item').change(function(){
		cargarexamenes();
	});

	$('#tipo-examen').change(function(){
		cargarexamenes();
	});

	$('.btn.buscar-exam').click(function(e) {
		cargarexamenes();
	});

	$('body').on('keyup', '#txtTitulo', function(e) {
		if(e.which == 13){
			$(".btn.buscar-exam").trigger('click');
		}
	}).on('click', '.btn.limpiar-busqueda', function(e) {
		e.preventDefault();
		restaurarListado();
	});
   
    cargarexamenes();
    showexamen('home');

});
</script>