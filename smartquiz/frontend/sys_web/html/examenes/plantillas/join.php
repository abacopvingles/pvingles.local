<?php
$orden=empty($_GET["ord"])?1:$_GET["ord"];
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
$rolActivo=$usuarioAct["rol"];
$html_edicion = $this->pregunta['ejercicio'];
$rutabase = $this->documento->getUrlBase();
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">

<div class="plantilla plantilla-fichas editando" id="tmp_<?php echo $idgui; ?>" data-idgui="<?php echo $idgui ?>" data-clone="#panelEjercicio">
    <input type="hidden" value="<?php echo $this->pregunta['dificultad']; ?>"  name="hPregDificultad" id="hPreguntaDificultad">
    <div id="panelEjercicio">
        <input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
        <div class="row nopreview _lista-fichas" id="lista-fichas<?php echo $idgui ?>">

        </div>
        <div class="row nopreview">
            <div class="col-xs-12 text-center botones-creacion">
                <a href="#" class="btn btn-primary add-ficha" data-clone-from="#clone" data-clone-to="._lista-fichas"><i class="fa fa-plus-square"></i> <?php echo ucfirst(JrTexto::_('add')).' '.JrTexto::_('group').' '.JrTexto::_('of').' '.JrTexto::_('sheet'); ?>s</a>
            </div>

            <div class="col-xs-12 text-center botones-editar hidden" style="display: none !important;">
                <a href="#" class="btn btn-success back-edit"><i class="fa fa-pencil"></i> <?php echo JrTexto::_('back').' '.JrTexto::_('and').' '.JrTexto::_('edit'); ?></a>
            </div>

            <div class="hidden col-xs-12 ficha-edit" data-clase="f_" id="clone">
                <div class="col-xs-12 col-sm-11">
                    <div class="col-xs-12 part-1">
                        <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_1_" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                                <img src="" class="hidden    renameClass" data-clase="img_1_">
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-default btn-block selectmedia    renameDataUrl istooltip"  data-tipo="audio" data-url=".audio_1_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                                <audio src="" class="hidden    renameClass" data-clase="audio_1_"></audio>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-9">
                            <input type="text" class="form-control    renameClass" data-clase="txt_1_" name="txtPalabra">
                        </div>
                    </div>

                    <div class="col-xs-12 part-2">
                        <hr>
                        <div class="col-xs-12 ficha-row">
                            <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_2_" title="<?php echo JrTexto::_('select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                                    <img src="" class="hidden    renameClass" data-clase="img_2_">
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" class="btn btn-default btn-block selectmedia     renameDataUrl istooltip" data-tipo="audio" data-url=".audio_2_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                                    <audio src="" class="hidden    renameClass" data-clase="audio_2_"></audio>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-9">
                                <input type="text" class="form-control    renameClass" data-clase="txt_2_" name="txtPalabra">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1 btns-ficha">
                    <a href="#" class="col-xs-6 col-sm-12 btn btn-danger delete-ficha istooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_('sheet'); ?>"><i class="fa fa-trash-o"></i></a>

                    <a href="#" class="col-xs-6 col-sm-12 btn btn-primary add-ficha-row istooltip" data-clone-from="#clone_row" data-clone-to="#f_ .part-2" title="<?php echo JrTexto::_('add').' '.JrTexto::_('row'); ?>"><i class="fa fa-plus-square-o"></i></a>
                </div>
            </div>

            <div class="hidden col-xs-12 ficha-row" id="clone_row">
                <div class="col-xs-12 col-sm-5 col-md-3 btns-media">
                    <div class="col-xs-6">
                        <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="image" data-url=".img_2_"  title="<?php echo JrTexto::_('Select').' '.JrTexto::_('image') ?>"><i class="fa fa-picture-o"></i></a>
                        <img src="" class="hidden    renameClass" data-clase="img_2_">
                    </div>
                    <div class="col-xs-6">
                        <a href="#" class="btn btn-default btn-block selectmedia renameDataUrl istooltip" data-tipo="audio" data-url=".audio_2_" title="<?php echo JrTexto::_('select') ?> audio"><i class="fa fa-headphones"></i></a>
                        <audio src="" class="hidden    renameClass" data-clase="audio_2_"></audio>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-5 col-md-8">
                    <input type="text" class="form-control    renameClass" data-clase="txt_2_" name="txtPalabra">
                </div>
                <div class="col-xs-2 col-sm-2 col-md-1">
                    <a href="#" class="btn color-danger delete-ficha-row istooltip" title="<?php echo JrTexto::_('discard').' '.JrTexto::_('row'); ?>"><i class="fa fa-trash"></i></a>
                </div>
            </div>

            <div class="hidden ficha" data-key="" id="to_generate">
                <img src="" class="img-responsive hide">
                <a href="#" class="btn btn-orange btn-block hide"><i class="fa fa-play"></i></a>
                <p class="hide">word</p>
            </div>
        </div>
        <div class="row ejerc-fichas tpl_plantilla contenido" id="ejerc-fichas<?php echo $idgui ?>" style="display: none;">
            <div class="col-xs-12 partes-1">

            </div>
            <div class="col-xs-12 partes-2">

            </div>

            <audio src="" class="hidden" id="audio-ejercicio<?php echo $idgui ?>" style="display: none;"></audio>
        </div>
    </div>

    <a href="#" id="generarhtml" class="btn btn-success generar-fichas hidden" data-clone-from="#to_generate" data-clone-to=".ejerc-fichas.tpl_plantilla"><i class="fa fa-rocket" style="display: none !important;"></i> <?php echo JrTexto::_('finish').' '.JrTexto::_('and').' '.JrTexto::_('generate'); ?></a>
</div>

<section id="sectionPreCarga" class="hidden" style="display: none !important;">
<?php 
if($html_edicion!=''){ 
    echo str_replace('__xRUTABASEx__', $rutabase, $html_edicion);
} ?>
</section>

<script>
var lastBtnClicked = null;
var arrColores = ['blue','red', 'green', 'pink', 'orange', 'skyblue', 'purple', 'aqua'];
function activarBtn(){
    var $btn = $(lastBtnClicked);
    var classElem = $btn.attr('data-url');
    if( $btn.data('tipo') == 'image' ){
        var src = $btn.siblings('img'+classElem).attr('src');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-success');
        } else {
            $btn.removeClass('btn-success').addClass('btn-default');
        }
    }
    if( $btn.data('tipo') == 'audio' ){
        var src = $btn.siblings('audio'+classElem).data('audio');
        if( src!='' && src!=undefined ){
            $btn.removeClass('btn-default').addClass('btn-orange');
        } else {
            $btn.removeClass('btn-orange').addClass('btn-default');
        }
    }
    lastBtnClicked = null;
}

$(document).ready(function() {
    var $plantilla = $('#tmp_<?php echo $idgui; ?>');
    var isEditando = $plantilla.hasClass('editando');

    var crearFicha = function($row, idCloneFrom, key, index){
        var $cloneFrom = $(idCloneFrom, $plantilla);
        var image = $row.find('img').attr('src'),
        audio = $row.find('audio').attr('data-audio'),
        input = $row.find('input[type="text"]').val(),
        html = null;
        $row.find('input[type="text"]').attr('value', input);

        if( image!='' && image!=undefined ){
            if(html==null) html = $cloneFrom.clone();
            html.find('img').attr('src', image).removeClass('hide');
        } 
        if( audio!='' && audio!=undefined ) {
            if(html==null) html = $cloneFrom.clone();
            html.find('a').attr('data-audio', audio).removeClass('hide');
        } 
        if( input!='' && input!=undefined ){
            if(html==null) html = $cloneFrom.clone();
            html.find('p').text(input).removeAttr('class');
        }

        if(html!=null){
            html.removeClass('hidden').removeAttr('id');
            html.attr('data-key', key);
            html.attr('data-color', arrColores[index]);
            html.find('.hide').remove();
        }

        return html;
    };

    var generarFichas = function(idCloneFrom, idCloneTo){
        var $contendorFichas = $plantilla.find('._lista-fichas');
        var sFichaEdicion = '.ficha-edit';
        var rspta = false;
        var $cloneTo = $(idCloneTo, $plantilla);
        $cloneTo.find('.partes-1').html('');
        $cloneTo.find('.partes-2').html('');
        $contendorFichas.find(sFichaEdicion).each(function(i, elem) {
            /* * * * Generacion de las PARTES-1 * * * */
            var $rowPrincipal=$(this).find('.part-1');
            var key = $(this).attr('id').split('_')[1];

            var html = crearFicha($rowPrincipal, idCloneFrom, key, i);

            if(html!=null){
                $cloneTo.find('.partes-1').append(html);
                rspta = true;
            } else {
                mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.debes_completar_fichas, 'error');
                $cloneTo.find('.partes-1').html('');
                $cloneTo.find('.partes-2').html('');
                rspta = false;
                return false;
            }

            /* * * * Generacion de las PARTES-2 * * * */
            $(this).find('.part-2 .ficha-row').each(function() {
                var html2 = crearFicha($(this), idCloneFrom, key);

                if(html2!=null) {
                    /* Agregando nuevo Html en posic al azar */
                    var $fichas_Part2 = $cloneTo.find('.partes-2').children();
                    if( $fichas_Part2.length==0 ){
                        $cloneTo.find('.partes-2').append(html2);
                    } else {
                        var posic = Math.floor(Math.random()*$fichas_Part2.length),
                        aleat = Math.floor(Math.random()*10);
                        if(aleat%2 == 0) { $fichas_Part2.eq(posic).before(html2); }
                        else { $fichas_Part2.eq(posic).after(html2); }
                    }
                    rspta = true;
                } else {
                    mostrar_notificacion(MSJES_PHP.attention, MSJES_PHP.debes_completar_fichas, 'error');
                    $cloneTo.find('.partes-1').html('');
                    $cloneTo.find('.partes-2').html('');
                    rspta = false
                    return false;
                }
            });
            if(!rspta) return false;
        });

        return rspta;
    };

    /**
    *** EDICION del ejerc Fichas :
    **/
    $('.plantilla-fichas')
    .on('click','._lista-fichas .selectmedia', function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        var txt= MSJES_PHP.select_upload;
        lastBtnClicked = this;
        selectedfile(e,this,txt,'activarBtn');
    })
    .on('click', '._lista-fichas .delete-ficha', function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.ficha-edit').remove();
    })
    .on('click', '._lista-fichas .delete-ficha-row', function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.ficha-row').remove();
    })
    .on('click', '._lista-fichas .add-ficha-row', function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        var cloneFrom = $(this).attr('data-clone-from');
        var cloneTo = $(this).attr('data-clone-to');
        var index = $(this).parents('.ficha-edit').attr('id').split('_')[1];
        var now = Date.now();
        var newRow = $(cloneFrom, $plantilla).clone();

        newRow.find('.renameClass').each(function() {
            var clase = $(this).data('clase');
            $(this).addClass(clase+index+'_'+now);
            $(this).removeClass('renameClass').removeAttr('data-clase');
        });

        newRow.find('.renameDataUrl').each(function() {
            var url = $(this).data('url');
            $(this).removeAttr('data-url').attr('data-url', url+index+'_'+now);
            $(this).removeClass('renameDataUrl');
        });

        newRow.removeClass('hidden').removeAttr('id');

        $(cloneTo, $plantilla).append(newRow);
        $("*[data-tooltip=\"tooltip\"]").tooltip();
    })
    .on('click', '.add-ficha',function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        var cloneFrom = $(this).attr('data-clone-from');
        var cloneTo = $(this).attr('data-clone-to');
        var newFicha = $(cloneFrom, $plantilla).clone();
        var now = Date.now();

        newFicha.find('.renameClass').each(function() {
            var clase = $(this).attr('data-clase');
            $(this).addClass(clase+now);
            $(this).removeClass('renameClass').removeAttr('data-clase');
        });

        newFicha.find('.renameDataUrl').each(function() {
            var url = $(this).attr('data-url');
            $(this).removeAttr('data-url').attr('data-url', url+now);
            $(this).removeClass('renameDataUrl');
        });

        newFicha.find('.add-ficha-row').attr('data-clone-to', '#f_'+now+' .part-2');

        newFicha.removeClass('hidden').removeAttr('id');
        newFicha.attr( 'id', newFicha.attr('data-clase')+now ).removeAttr('data-clase');

        $(cloneTo, $plantilla).append(newFicha);
        $("*[data-tooltip=\"tooltip\"]").tooltip();
    })
    .on('click','.generar-fichas',function(e) {
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        var idCloneFrom = $(this).attr('data-clone-from');
        var idCloneTo = $(this).attr('data-clone-to');
        $plantilla.removeClass('editando');
        var resp = generarFichas(idCloneFrom, idCloneTo);
        if(resp){
            $(idCloneTo, $plantilla).show();
            $('._lista-fichas', $plantilla).hide();
            $('.botones-creacion', $plantilla).hide();
            //$('.botones-editar', $plantilla).show();
        } else {
            $(idCloneTo, $plantilla).hide();
            $('._lista-fichas', $plantilla).show();
            $('.botones-creacion', $plantilla).show();
            //$('.botones-editar', $plantilla).hide();
        }
    })
    .on('click','.botones-editar .back-edit', function(e){ //editar fichas
        if(!isEditando) return false;
        e.preventDefault();
        e.stopPropagation();
        //$plantilla.addClass('editando');
        $('.ejerc-fichas', $plantilla).hide();
        $('._lista-fichas', $plantilla).show();
        $('.botones-creacion', $plantilla).show();
        //$('.botones-editar', $plantilla).hide();
        $('.ejerc-fichas', $plantilla).find('.partes-1').html('');
        $('.ejerc-fichas', $plantilla).find('.partes-2').html('');
    });

    var existeHtml = <?php echo ($html_edicion!='')? 'true':'false'  ?>;
    if(existeHtml){
        var precarga = $('#sectionPreCarga').clone();
        var html = precarga.find('.ejercicio').html();
        $('#panelEjercicio').html(html);
        $('.back-edit').trigger('click');
        $('#sectionPreCarga').remove();
    }else{
        $('.add-ficha').trigger('click');
    }
});
</script>