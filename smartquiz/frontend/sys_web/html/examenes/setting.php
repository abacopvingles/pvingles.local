<?php $idgui = uniqid();
$rand =  mt_rand();
$fonts = ['Georgia', 'Palatino Linotype', 'Book Antiqua', 'Times New Roman', 'Arial', 'Helvetica', 'Arial Black', 'Impact', 'Lucida Sans Unicode', 'Tahoma', 'Verdana', 'Courier New', 'Lucida Console'];
sort($fonts);
$examen = null;
$certificado = $this->documento->getUrlStatic().'/img/sistema/certificado_default.png';
$portada = $this->documento->getUrlStatic().'/img/sistema/examen_default.png';
if(!empty($this->examen)) {
	$examen = $this->examen;
	/*$this->idtipo = $examen["tipo"];*/
	if(!empty(@$examen["portada"])){
		$portada = @$examen["portada"];
		$portada = @str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $portada);
	}
	if(!empty(@$examen["nombre_certificado"])){ 
		$certificado = @str_replace('__xRUTABASEx__', $this->documento->getUrlBase(), $examen["nombre_certificado"]); 
	}
	$total = @$examen["calificacion_total"];
	$minimo = @$examen["calificacion_min"];
}
?>
<style type="text/css">
	.popover{max-width: inherit;}
</style>
<form class="form-horizontal" method="post" id="frm-<?php echo $idgui;?>" onsubmit="return false;">
	<input type="hidden" name="idexamen" value="<?php echo @$examen["idexamen"]; ?>">
	<input type="hidden" name="tipo" value="<?php echo @$this->accion; ?>">
	<textarea class="hidden" name="habilidades_todas" id="habilidades_todas"><?php echo @$examen["habilidades_todas"] ?></textarea>
	<div class="panel panel-primary">
		<div class="panel-heading" style="overflow: hidden;">
			<ol class="breadcrumb pull-left" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
				<li><a href="<?php echo $this->documento->getUrlBase() ?>/examenes/" style="color:#fff"><?php echo JrTexto::_("Assessments"); ?></a></li>
				<li class="active"  style="color:#ccc"><?php echo JrTexto::_("Setting"); ?></li>
			</ol>
			<div class="pull-right"><a href="#" class="btn btn-xs btn-default showdemo" data-videodemo="settings.mp4"><i class="fa fa-question-circle"></i> <?php echo JrTexto::_("Help"); ?></a></div>
		</div>
		<div class="panel-body">
			<div class="col-xs-12">
				<div class="row">
					<?php if(!empty(@$examen["idexamen"])){ ?>
					<div class="col-xs-12">
						<div class="input-group" style="margin: 0;">
							<div class="input-group-addon"><?php echo ucfirst(JrTexto::_("Link to solve the exam"))?></div>
							<input type="url" class="form-control" id="link_resolver" name="link_resolver" value="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.@$examen["idexamen"]; ?>" readonly="readonly" style="background-color: #fff;">
						</div>
						<hr>
					</div>
					<?php } ?>
					<div class="col-xs-12 col-sm-4 col-md-3">
						<div class="form-group">
							<label class="col-xs-12" for="titulo" class="text-left"><?php echo ucfirst(JrTexto::_("cover")); ?>:</label>
							<div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-12" >
								<img class="img-portada img-responsive img-thumbnail istooltip" src="<?php echo $portada ?>" data-tipo="image" data-url=".img-portada" alt="" title="<?php echo JrTexto::_("Click here to change te exam cover"); ?>" >
								<input type="hidden" name="portada" value="<?php echo $portada ?>">
							</div>					  	
						</div>					  	
					</div>
					<div class="col-xs-12 col-sm-8 col-md-5 padding-0">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-12" for="titulo"><?php echo ucfirst(JrTexto::_("Title")); ?>:</label>
								<div class="col-xs-12">
									<input type="text" class="form-control gris" id="titulo" name="titulo" placeholder="<?php echo ucfirst(JrTexto::_("add title")); ?>" value="<?php echo @$examen["titulo"] ?>">
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-12" for="descripcion"><?php echo ucfirst(JrTexto::_("Description")); ?>:</label>
								<div class="col-xs-12">
									<textarea name="descripcion" rows="8" class="form-control gris" name="descripcion" placeholder="<?php echo ucfirst(JrTexto::_("Add Description")); ?>"><?php echo @$examen["descripcion"] ?></textarea>
								</div>					  	
							</div>					  	
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 padding-0 thumbnail">
						<legend><?php echo ucfirst(JrTexto::_("general settings"))?></legend>
						<div class="col-xs-6 <?php echo (@$this->accion!="G")?'':'hidden'; ?>">
							<div class="form-group">
								<label class="col-xs-12" for="tiempopor"><?php echo ucfirst(JrTexto::_("control time by")); ?>:</label>						
								<div class="col-xs-12 select-ctrl-wrapper select-azul">
									<select name="tiempopor" id="tiempopor" class="select-ctrl form-control">
										<option value="E" <?php echo @$examen["tiempo_por"]=='E'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Exam"))?></option> 
										<option value="Q" <?php echo @$examen["tiempo_por"]=='Q'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Question"))?></option>	
									</select>
								</div>
							</div>	
						</div>

						<div class="col-xs-<?php echo (@$this->accion!="G")?'5':'12'; ?>">
							<div class="form-group pnltiempopor">
								<label class="col-xs-<?php echo (@$this->accion!="G")?'12':'6'; ?>" for="tiempototal"><?php echo JrTexto::_("Time of exam"); ?>:</label>
								<div class="col-xs-<?php echo (@$this->accion!="G")?'12':'6'; ?>">
									<input type="text" class="form-control" id="tiempototal" name="tiempototal" value="<?php echo @$examen["tiempo_total"]?$examen["tiempo_total"]:'00:00:00';?>" readonly="readonly">
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-12" for="nintento"><?php echo ucfirst(JrTexto::_("number of attempts")); ?>:</label>							
								<div class="col-xs-12 select-ctrl-wrapper select-azul ">
									<select name="nintento" id="nintento" class="select-ctrl form-control">
										<option value="<?php echo $this->max_intento; ?>" <?php echo @$examen["nintento"]==$this->max_intento?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Unlimited")); ?></option>
										<?php for($j=1;$j<=$this->cant_intentos;$j++){?>
										<option value="<?php echo $j; ?>" <?php echo @$examen["nintento"]==$j?'Selected="selected"':''; ?>> <?php echo $j." ".ucfirst(JrTexto::_("Attempt")).($j==1?'':'s'); ?></option> 
										<?php }?>

									</select>
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group" id="pnlcalificacion" style="display: none;">
								<label class="col-xs-6" for="calificacion"><?php echo ucfirst(JrTexto::_("choose")); ?>:</label>
								<div class="col-xs-12 select-ctrl-wrapper select-azul ">
									<select name="calificacion" id="calificacion" class="select-ctrl form-control" >
										<option value="U" <?php echo @$examen["calificacion"]=='U'?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Last try")); ?></option>
										<option value="M" <?php echo @$examen["calificacion"]=='M'?'Selected="selected"':''; ?>> <?php echo ucfirst(JrTexto::_("Best try")); ?></option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-12" for="aleatorio"><?php echo ucfirst(JrTexto::_("order of questions")); ?>:</label>							
								<div class="col-xs-12 select-ctrl-wrapper select-azul ">
									<select name="aleatorio" id="aleatorio" class="select-ctrl form-control">
										<option value="0" <?php echo @$examen["aleatorio"]=='0'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("orderly")); ?></option> 
										<option value="1" <?php echo @$examen["aleatorio"]=='1'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("ramdon")); ?></option>	
									</select>
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-12" for="estado"><?php echo ucfirst(JrTexto::_("State")); ?>:</label>
								<div class="col-xs-12 select-ctrl-wrapper select-azul">
									<select name="estado" id="estado" class="select-ctrl form-control">
										<option value="1" <?php echo @$examen["estado"]=='1'?'Selected="selected"':''; ?>><?php echo JrTexto::_("Active")?></option> 
										<option value="0" <?php echo @$examen["estado"]=='0'?'Selected="selected"':''; ?>><?php echo JrTexto::_("Inactive")?></option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<?php if(@$this->accion!="G"){?>
					<div class="col-xs-12 col-sm-12 col-md-5 padding-0 thumbnail">
						<legend><?php echo ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills"))?></legend>
						<div class="col-xs-12"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("complete"))?>:</label>
							<div class="col-xs-12 select-ctrl-wrapper select-azul ">
								<select name="origen_habilidades" id="origen_habilidades" class="select-ctrl form-control">
									<option value="">- <?php echo ucfirst(JrTexto::_("Select"))?> -</option>
									<option value="MANUAL" <?php echo @$examen["origen_habilidades"]=='MANUAL'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("manually"))?></option>
									<option value="JSON" <?php echo @$examen["origen_habilidades"]=='JSON'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("external data source")).' ('.ucfirst(JrTexto::_("JSON format")).')'; ?></option>
								</select>
							</div>
						</div></div>

						<div class="col-xs-12" data-opcion="JSON" style="display: none;">
							<div class="col-xs-12">
								<div class="form-group">
									<div class="input-group" style="margin: 0;">
										<div class="input-group-addon"><?php echo ucfirst(JrTexto::_("URL"))?></div>
										<input type="url" class="form-control" id="fuente_externa" name="fuente_externa" placeholder="<?php echo ucfirst(JrTexto::_("e.g."))?>: https://www.myweb.com/skills/list" value="<?php echo empty($examen["fuente_externa"])?@$this->config_proyecto['url_skills']['valor'].'?idcurso='.$this->idcurso:@$examen["fuente_externa"]; ?>">
										<div class="input-group-addon btn info_habilidades" data-toggle="popover" ><i class="fa fa-info-circle color-info" aria-hidden="true"></i></div>
									</div>
									<button class="btn btn-primary pull-right get-external-source" data-tipo="habilidad"><i class="fa fa-cloud-download" aria-hidden="true"></i> <?php echo ucfirst(JrTexto::_("get"))?></button>
								</div>
							</div>
							<br><br><br>
						</div>

						<div class="col-xs-12" data-opcion="MANUAL" style="display: none;"><div class="form-group">
							<table class="table table-striped table-hover" id="tblHabilidades">
								<caption class="bg-danger text-center"><?php echo ucfirst(JrTexto::_("Complete")).' '.ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills"))?></caption>
								<thead class="bg-red">
									<tr>
										<th width="35%"><?php echo ucfirst(JrTexto::_("id"))?></th>
										<th><?php echo ucfirst(JrTexto::_("description"))?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr class="tr_clone" style="display: none;">
										<td><input type="text" class="form-control skill_id" value="" readonly="readonly"></td>
										<td><input type="text" class="form-control skill_name" value=""></td>
										<td><button class="btn btn-default delete-skill"  title="<?php echo ucfirst(JrTexto::_("delete"))?>"><i class="fa fa-trash color-red"></i></button></td>
									</tr>
								</tbody>
								<tfoot>
									<td class="text-right" colspan="3">
										<button class="btn btn-primary btn-xs btn-addfila">
											<i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_("add").' '.JrTexto::_("row"));?>
										</button>
									</td>
								</tfoot>
							</table>
						</div></div>
					</div>
					<?php } ?>

					<div class="col-xs-12 col-sm-6 col-md-<?php echo (@$this->accion!="G")?'4':'6'; ?> padding-0 thumbnail">
						<legend><?php echo ucfirst(JrTexto::_("set score"))?></legend>
						<div class="col-xs-12"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("scored by"))?>:</label>
							<div class="col-xs-12 select-ctrl-wrapper select-azul "> 				  	 
								<select name="calificacionpor" id="calificacionpor" class="select-ctrl form-control">
									<option value="E" <?php echo @$examen["calificacion_por"]=='E'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Exam"))?></option>
									<option value="Q" <?php echo @$examen["calificacion_por"]=='Q'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("Question"))?></option>
								</select>
							</div>
						</div></div>
						<div class="col-xs-12 pnlcalificacionpor"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("type of score"))?>:</label> 
							<div class="col-xs-12 select-ctrl-wrapper select-azul ">				  	
								<select name="calificacionen" id="calificacionen" class="select-ctrl form-control">
								<?php if(@$this->accion!="U"){ ?>
									<option value="P" <?php echo @$examen["calificacion_en"]=='P'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("percentage"))?></option>
									<option value="N" <?php echo @$examen["calificacion_en"]=='N'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("numeric"))?></option>
								<?php } ?>
									<option value="A" <?php echo @$examen["calificacion_en"]=='A'?'Selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_("alfabetic"))?></option>
								</select>
								<small id="msje_calificacionpor"><?php echo ucfirst(JrTexto::_("minimum score to pass"))?>: 51ptos.</small>
							</div>
						</div></div>
						<div class="col-xs-12 pnlcalificacionpor pnlcalificacionen"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("highest score"))?>:</label> 
							<div class="col-xs-12 select-ctrl-wrapper select-azul ">				  	
								<select name="calificaciontotal" id="calificaciontotal" class="select-ctrl form-control">
								</select>
							</div>
						</div></div>
						<div class="col-xs-12 pnlcalificacionen"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("minimum score to pass"))?>:</label> 
							<div class="col-xs-12 select-ctrl-wrapper select-azul ">				  	
								<select name="calificacionmin" id="calificacionmin" class="select-ctrl form-control">
								</select>
							</div>
						</div></div>
						<div class="col-xs-12 pnlcalificacionenshow" style="display: none"><div class="form-group">
							<?php if(@$this->accion=="U"){ ?>
							<div class="col-xs-12"><div class="col-xs-12">
								<div class="form-group">
									<div class="input-group" style="margin: 0;">
										<div class="input-group-addon"><?php echo ucfirst(JrTexto::_("URL"))?></div>
										<input type="url" class="form-control" id="fuente_externa" name="fuente_externa" placeholder="<?php echo (JrTexto::_("e.g."))?>: https://www.myweb.com/service/nivel" value="">
										<div class="input-group-addon btn info_niveles" data-toggle="popover" ><i class="fa fa-info-circle color-info" aria-hidden="true"></i></div>
									</div>
									<button class="btn btn-primary pull-right get-external-source" data-tipo="nivel"><i class="fa fa-cloud-download" aria-hidden="true"></i> <?php echo ucfirst(JrTexto::_("get"))?></button>
								</div>
							</div></div>
							<?php } ?>
							<table id="tblEscalas" class="table table-reponsive table-striped <?php echo @$this->accion=="U"?'hidden':'' ?>">
								<thead class="bg-blue">
									<tr>
										<th style="width: 40%;"><?php echo ucfirst(JrTexto::_("scale name"));?></th>
										<th><?php echo ucfirst(JrTexto::_("range of scale"))?></th>
										<th style="width: 1%;"></th>
									</tr>
								</thead>
								<tbody>
									<tr id="tr_<?php echo mt_rand(1000000000,9999999999); ?>">
										<td>
											<input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="<?php echo JrTexto::_("e.g.");?>: A+, good, ...">
										</td>
										<td>
											<div class="col-xs-6 padding-0">
												<select class="txtnota form-control opc_max">
												<?php for ($i=100; $i >= 1; $i--) { ?>
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php }?>
												</select>
											</div>
											<div class="col-xs-6 padding-0">
												<select class="txtnota form-control opc_min">
												<?php for ($i=99; $i >= 0; $i--) { ?>
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php }?>
												</select>
											</div>
										</td>
										<td></td>
									</tr>
									<tr class="tr_clone" style="display: none">
										<td>
											<input type="text" class="form-control gris txtNombreEscala" placeholder="<?php echo JrTexto::_("e.g.");?>: A+, good, ...">
										</td>
										<td>
											<div class="col-xs-6 padding-0">
												<select class="txtnota form-control opc_max">
												<?php for ($i=100; $i >= 1; $i--) { ?>
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php }?>
												</select>
											</div>
											<div class="col-xs-6 padding-0">
												<select class="txtnota form-control opc_min">
												<?php for ($i=99; $i >= 0; $i--) { ?>
												<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
												<?php }?>
												</select>
											</div>
										</td>
										<td><i class="btn btn-removefila fa fa-trash color-red" ></i></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td class="text-right" colspan="3">
											<span class="btn btn-primary  btn-xs btn-addfila"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_("add").' '.JrTexto::_("row"));?></span>
										</td>
									</tr>
								</tfoot>
							</table>
							<input type="hidden" name="txtAlfanumerico" id="txtAlfanumerico">
						</div></div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-3 padding-0 thumbnail">
						<legend><?php echo ucfirst(JrTexto::_("certificate"))?></legend>
						<div class="col-xs-12"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("Will the student receive a certificate once he passes the exam?"))?></label>
							<div class="col-xs-12 select-ctrl-wrapper select-azul"> 				  	 
								<select name="tiene_certificado" id="tiene_certificado" class="select-ctrl form-control">
									<option value="NO" <?php echo @$examen["tiene_certificado"]=='NO'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_('No'))?></option>
									<option value="SI" <?php echo @$examen["tiene_certificado"]=='SI'?'selected="selected"':''; ?>><?php echo ucfirst(JrTexto::_('Yes'))?></option>
								</select>
							</div>
						</div></div>
						<div class="col-xs-12"><div class="form-group img-certificado" style="display: none;">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("certificate template"))?></label> 
							<div class="col-xs-12" style="padding-bottom: 20px; overflow: hidden;">
								<img src="<?php echo $certificado; ?>" data-defecto="<?php echo $certificado; ?>" alt="certificate" class="img-responsive center-block" style="max-height: 100px;">
								<input type="file" accept="image/*" name="file_certificado" id="file_certificado" disabled="disabled" style="bottom: 0; cursor: pointer; padding-top: 100px; position: absolute; width: 100%;" value="<?php echo $certificado ?>">
								<input type="hidden" name="nombre_certificado" id="nombre_certificado" value="<?php 
								$aNombre = explode(SD, $examen['nombre_certificado']);
								if(!empty($aNombre)){ echo $aNombre[count($aNombre)-1]; }
								?>">
							</div>
						</div></div>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-3 padding-0 thumbnail">
						<legend><?php echo ucfirst(JrTexto::_("type and font size"))?></legend>
						<div class="col-xs-12"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("fonts"))?></label>
							<div class="col-xs-12 select-ctrl-wrapper select-azul"> 				  	 
								<select name="fuente" id="fuente" class="select-ctrl form-control">
									<?php foreach ($fonts as $f) { ?>
									<option value="<?php echo $f; ?>" style="font-family: '<?php echo JrTexto::_($f)?>';" <?php echo @$examen["fuente"]==$f?'Selected="selected"':''; ?>><?php echo JrTexto::_($f)?></option>
									<?php } ?>
								</select>
							</div>
						</div></div>
						<div class="col-xs-12"><div class="form-group">
							<label class="col-xs-12"><?php echo ucfirst(JrTexto::_("font size"))?></label> 
							<div class="col-xs-12 select-ctrl-wrapper select-azul">				  	
								<select name="fuentesize" id="size" class="select-ctrl form-control">
									<?php 
									$isel=!empty($examen["fuentesize"])?$examen["fuentesize"]:14;
									for ($i=10; $i < 36; $i++) { ?>
									<option value="<?php echo $i; ?>" <?php echo $i==$isel?'selected="selected"':''; ?> ><?php echo $i."px"; ?></option>
									<?php }?>
								</select>
							</div>
						</div></div>
					</div>	
				</div>	
			</div>
		</div>
		<div class="panel-footer text-right">
			<button class="btn btn-danger btneliminarexamen hidden" data-idexamen="<?php echo @$examen["idexamen"]; ?>"><i class="fa fa-trash"></i> <?php echo JrTexto::_('Remove Assessment'); ?></button>
			<button class="btn btn-primary btnsaveexamen"><i class="fa fa-save"></i> <?php echo JrTexto::_('Save and add question'); ?></button>
		</div>
	</div>
</form>

<section class="hidden">
	<div id="clone-popover-habilidades" style=" min-width: 300px; ">
		<p>El formato  deberá ser un Array que contenga elementos JSON de la siguiente forma:</p>
		<code>{</code><br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<code>skill_id:</code><i>&lt;identifier: string | number&gt;</i><code>,</code> <br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<code>skill_name: </code><i>&lt;description: string&gt;</i><br>
	  	<code>}</code><br>
	  	<p>Por ejemplo:</p>
		<pre>
[
  {
    skill_id:"50031", 
    skill_name: "Listening"
  }, {
    skill_id: 104843, 
    skill_name: "Reading"
  }, {
    skill_id:"1364", 
    skill_name: "Speaking"
  }
]
		</pre>
	</div>

	<div id="clone-popover-niveles" style=" min-width: 300px; ">
		<p>El formato  deberá ser un Array que contenga elementos JSON de la siguiente forma:</p>
		<code>{</code><br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<code>idcursodetalle:</code><i>&lt;identifier: number&gt;</i><code>,</code> <br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<code>idrecurso: </code><i>&lt;identifier: number&gt;</i><code>,</code><br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<code>nombre: </code><i>&lt;name of level: string&gt;</i><br>
	  	<code>}</code><br>
	  	<p>Por ejemplo:</p>
		<pre>
[
  {
    idcursodetalle: 53325, 
    idrecurso: 45,
    nombre: "Básico"
  }, {
    idcursodetalle: 481043, 
    idrecurso: 245,
    nombre: "Intermedio"
  }, {
    idcursodetalle: 3104 , 
    idrecurso: 653,
    nombre: "Avanzado"
  }
]
		</pre>
	</div>
</section>
<script type="text/javascript">
var esFuenteExterna = false;
var ARRAY_HABILIDADES = [];
var ARRAY_NIVELES = [];
var tieneIdCurso = <?php echo !empty($this->idcurso)?'true':'false' ?>;

//recargar combos de niveles 
var leerniveles=function(data){
	try{
		var res = xajax__('', 'niveles', 'getxPadre', data);
		if(res){ return res; }
		return false;
	}catch(error){
		return false;
	}       
};
var addniveles=function(data,obj){
	var objini=obj.find('option:first').clone();
	obj.find('option').remove();
	obj.append(objini);
	if(data!==false){
		var html='';
		$.each(data,function(i,v){
			html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
		});
		obj.append(html);
	}
	id=obj.attr('id');
    	//if(id==='activity-item')	cargarexamenes();
};

var vercalificacion=function(){
	var x=$('#nintento').val()||1;
	$('#pnlcalificacion').hide();
	if(x>1){
		$('#pnlcalificacion').show();
	}
};

var verengrupo=function(){
	var grupo=$('#engrupo').val();
	if(grupo===''){
		$('.pnlisgrupo').hide();
	}else{
		$('.pnlisgrupo').show();
	}
};

var vertiempopor=function(){
	var tiempopor=$('#tiempopor').val();
	//console.log($('.pnltiempopor #tiempototal'));
	if(tiempopor=='Q'){
		$('.pnltiempopor #tiempototal').removeAttr('required');
		$('.pnltiempopor #tiempototal').attr( "readonly", "readonly" );
	}else{
		$('.pnltiempopor #tiempototal').removeAttr('readonly');
		$('.pnltiempopor #tiempototal').attr('required', 'required');
	}
};

var vercalificacionpor=function(){
	var califPor=$('#calificacionpor').val();
	$('.pnlcalificacionpor').show();
	if(califPor=='Q'){
		$('#calificacionen option[value="A"]').hide();
		$('#calificaciontotal').closest('.pnlcalificacionpor').hide();
	} 
	if(califPor=='E'){
		$('#calificacionen option[value="A"]').show();
		$('#calificaciontotal').closest('.pnlcalificacionpor').show();
	}
};

var vercalificacionen=function(){
	var califEn=$('#calificacionen').val();
	$('#calificaciontotal').find('option').remove();
	$('#calificacionmin').find('option').remove();
	$('.pnlcalificacionen').show();
	var min=<?php echo !empty($minimo)?$minimo:1; ?>;
	var total=<?php echo !empty($total)?$total:100; ?>;
	if(califEn=='P'||califEn=='N'){ //porcentaje	
		var simbol='';
		if(califEn=='P') simbol='%';    		
		var option='';
		for (var i = 1; i <= 100; i++){
			option+='<option value="'+i+'" '+(min==i?'selected="selected"':'')+'>'+i+''+simbol+'</option>';
		}
		$('#calificacionmin').append(option);
		var option='';
		for (var i = 100; i >0 ; i--){
			option+='<option value="'+i+'" '+(total==i?'selected="selected"':'')+'>'+i+''+simbol+'</option>';
		}
		$('#calificaciontotal').append(option);
		if( $('#calificacionpor').val()=='Q' ) $('#calificaciontotal').closest('.pnlcalificacionpor').hide();
		if( $('#calificacionpor').val()=='E' ) $('#calificaciontotal').closest('.pnlcalificacionpor').show();
		$('.pnlcalificacionenshow').hide();
	}else if(califEn=='A' || $('#tipo').val()=='U'){ //alfanumerico
		$('#txtAlfanumerico').val(JSON.stringify(total));
		if(typeof total!="number") precargartable();
		$('.pnlcalificacionen').hide();
		$('.pnlcalificacionenshow').show();
		orndenartr();
	}

	if(califEn=='A') $('#msje_calificacionpor').show();
	else $('#msje_calificacionpor').hide();
};

var calificacionmin=function(_simbolo,valor,obj){
	var option='';	    
	var simbolo=_simbolo=='P'?'%':'';
	obj.find('option').remove();
	for (var i = 1; i <= valor; i++){
		option+='<option value="'+i+'">'+i+simbolo+'</option>';
	}
	obj.append(option);
};

//calificacion alfabetica
var orndenartr=function(){
	var $trs=$('#tblEscalas tbody tr:visible');
	var i=0;
	$trs.each(function(){
		i++;
		$(this).find('.txtnota').attr('data-fila',i);
		$(this).attr('data-fila',i);
	})
} ;

var agregarFilaTabla = function ($tabla) {
	var $filaAclonar=$tabla.find('tbody').find('.tr_clone');
	var $clonado=$filaAclonar.clone();
	var identifier = Math.floor(Math.random()*Math.pow(10,10));
	var idFila = 'tr_'+identifier;
	$clonado.attr('id', idFila);
	$clonado.removeAttr('class');
	$clonado.show();
	$tabla.find('tbody').find('.tr_clone').before($clonado);
	return idFila;
};

var borrarFilaTabla = function ($celda) {
	$celda.closest('tr').remove();
};

//configuracion habilidades
var limpiarTabla = function($tabla){
	$ultimoTR = $tabla.find('tbody tr:last').clone();
	$tabla.find('tbody').html($ultimoTR);
};

var arraySkillsToTable = function(){
	var $tabla = $('#tblHabilidades');
	try{
		if(!$.isArray(ARRAY_HABILIDADES)) throw "Data does not have a valid JSON format.";
		if(ARRAY_HABILIDADES.length==0) throw "Data not found to populate skill table.";
		$.each(ARRAY_HABILIDADES, function(i, node) {
			if(node.skill_id===undefined && node.skill_id!=='') throw  "'skill_id' is not defined at node ["+i+"]:"+ JSON.stringify(node);
			if(node.skill_name===undefined && node.skill_name!=='') throw  "'skill_name' is not defined at node ["+i+"]:"+ JSON.stringify(node);
			var idFila = agregarFilaTabla($tabla);
			var $fila = $tabla.find('#'+idFila);
			$fila.find('input.skill_id').val(node.skill_id).attr('value', node.skill_id);
			$fila.find('input.skill_name').val(node.skill_name).attr('value', node.skill_name);
			if(esFuenteExterna){
				$fila.find('input.skill_id').attr('readonly', 'readonly');
				$fila.find('input.skill_name').attr('readonly', 'readonly');
				$fila.find('.btn.delete-skill').hide();
				$tabla.find('tfoot').hide();
			}
		});
	}catch(err){
		limpiarTabla($tabla);
		alert('ERROR trying to populate skill table.\n' + err);
		throw err;
	}
};

var arrNivelesToTable = function(){
	var $tabla = $('#tblEscalas');
	try {
		if(!$.isArray(ARRAY_NIVELES)) throw "Data does not have a valid JSON format.";
		if(ARRAY_NIVELES.length==0) throw "Data not found to populate skill table.";

		$tabla.find('tbody tr').not($('.tr_clone')).remove();
		$tabla.find('.btn-addfila').addClass('hidden');
		for(let i=ARRAY_NIVELES.length-1; i>=0; i--){
			let node = ARRAY_NIVELES[i];

			if(typeof node.idcursodetalle=='undefined' && node.idcursodetalle!=='') throw  "'idcursodetalle' is not defined at node ["+i+"]:"+ JSON.stringify(node);
			if(typeof node.idrecurso=='undefined' && node.idrecurso!=='') throw  "'idrecurso' is not defined at node ["+i+"]:"+ JSON.stringify(node);
			if(typeof node.nombre=='undefined' && node.nombre!=='') throw  "'nombre' is not defined at node ["+i+"]:"+ JSON.stringify(node);

			let idFila = agregarFilaTabla($tabla);
			let $fila = $tabla.find('#'+idFila);
			$fila.attr({
				'data-idrecurso': node.idrecurso,
				'data-idcursodetalle': node.idcursodetalle
			});
			$fila.find('input.txtNombreEscala').val(node.nombre).attr({
				"value": node.nombre,
				"readonly": "readonly"
			});
			$fila.find('.btn-removefila').addClass('hidden');
			$fila.find('.opc_max').attr("readonly", "readonly");
		}
	} catch(err) {
		alert('ERROR trying to populate skill table.\n' + err);	
		throw err;
	}
};

var tableSkillsToArray = function(){
	var $tabla = $('#tblHabilidades');
	if($tabla.length==0){ return false; }
	ARRAY_HABILIDADES = [];
	$tabla.find('tbody tr').not('tr:last').each(function(i, fila) {
		var elem = {
			"skill_id" : $(fila).find('input.skill_id').val(),
			"skill_name" : $(fila).find('input.skill_name').val(),
		};
		ARRAY_HABILIDADES.push(elem);
	});
	$('#habilidades_todas').val(JSON.stringify(ARRAY_HABILIDADES));
	$('#habilidades_todas').html(JSON.stringify(ARRAY_HABILIDADES));
};

var enumerarSkill_id = function($tabla){
	$tabla.find('tbody tr').not('tr:last').each(function(i, fila) {
		var random = '<?php echo $rand ?>';
		random += (i+1<10?'0':'')+(i+1);
		if($(fila).find('input.skill_id').val()==''){
			$(fila).find('input.skill_id').val(random).attr({'value': random, 'readonly':'readonly'});
		}
	});
};

var preCarga_Skills = function () {
	var txtSkills = $('#habilidades_todas').val();
	$('#origen_habilidades').trigger('change');
	if(txtSkills!='[]' && txtSkills!='') {
		ARRAY_HABILIDADES = JSON.parse( txtSkills );
		arraySkillsToTable();
		$('div[data-opcion="MANUAL"]').show();
	}
};

var calificacionmin2 = function(simbolo,valor, $select){
	simbolo = simbolo||'';
	$select.find('option').remove();
	$select.each(function(index, elem) {
		let html_option='';
		for (let i = valor ; i >= 0; i--){
			html_option+='<option value="'+i+'">'+i+simbolo+'</option>';
		}
		$(elem).append(html_option);
		--valor;
	});
};

var cargartxtAlfanumerico=function(){
	var jEscalas=[];
	var $filas=$('#tblEscalas tbody tr[data-fila]');
	var cant_tr=$filas.length;
	if(cant_tr>0) {
		$filas.each(function(i, tr){
			let escala = {
				"idcursodetalle" : $(tr).attr('data-idcursodetalle'),
				"max" : $(tr).find('select.opc_max').val(),
				"min" : $(tr).find('select.opc_min').val(),
				"nombre" : $(tr).find('.txtNombreEscala').val(),
			};
			jEscalas.push(escala);
		});
	}
	$('#txtAlfanumerico').val(JSON.stringify(jEscalas));
};

var precargartable =function(){
	var $table=$('#tblEscalas');
	var tr1=$table.find('tr:first').nextAll().remove();
	var datos=JSON.parse($('#txtAlfanumerico').val());	    	
	var html=''; 
	var max=100;	    	
	if(!$.isEmptyObject(datos)){
		$table.find('tbody tr').not($('.tr_clone')).remove();
		var index = 0;
		$.each(datos,function(i, escala){
			let name = escala.nombre;
			let min_val = escala.min;
			let max_val = escala.max;
			let idcursodetalle = escala.idcursodetalle||'';
			html+='<tr data-idcursodetalle="'+idcursodetalle+'"><td>'
			+'<div class="">'
			+'<input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ..." value="'+name+'">'
			+'</div></td>'
			+'<td>'
			+'<div class="col-xs-6 padding-0"><select class="txtnota form-control opc_max" '+(index>0?'readonly="readonly"':'')+'>';
			for (let i = max; i >= 1; i--) {
				html +='<option value="'+i+'">'+i+'</option>';	
			}
			html +='</select></div>'
			+'<div class="col-xs-6 padding-0"><select class="txtnota form-control opc_min">';
			for (let i = max ; i >= 0; i--) {
				html +='<option value="'+i+'" '+(i==min_val?'selected':'')+'>'+i+'</option>';	
			}
			html +='</select></div>'
			+'</td>'
			+'<td><i class="btn btn-removefila fa fa-trash color-red" ></i></td>'
			+'</tr>';
			max=min_val;
			index++;
		});
	}
	/*html+='<tr style="display:none"><td>'
	+'<div class="">'
	+'<input type="text" class="form-control gris txtNombreEscala" name="txtNombreEscala" placeholder="<?php echo JrTexto::_("e.g.");?>: A, good, ..." value="">'
	+'</div></td>'
	+'<td>'			  	
	+'<div class="col-xs-6 padding-0"><select class="txtnota form-control">';
	for (let i = (max); i >= 1; i--) {
		html +='<option value="'+i+'">'+i+'</option>';	
	}
	html +='</select></div>'
	+'<div class="col-xs-6 padding-0"><select class="txtnota form-control">';
	for (let i = (max-1); i >= 0; i--) {
		html +='<option value="'+i+'">'+i+'</option>';	
	}
	html +='</select></div>'
	+'</td>'
	+'<td><i class="btn btn-removefila fa fa-trash color-red"></i></td>'
	+'</tr>';*/
	$table.find('.tr_clone').before(html);
	$table.removeClass('hidden');
	orndenartr();
};

var showFile_certificado = function () {
	var valor = $('#tiene_certificado').val();
	if(valor == 'SI'){
		$('#file_certificado').closest('.img-certificado').show();
		$('#file_certificado').removeAttr('disabled');
	}else{
		$('#file_certificado').closest('.img-certificado').hide();
		$('#file_certificado').attr('disabled','disabled');
	}
};

var leerInputFile = function(input){
	var $img = $('.img-certificado img');
	var srcDefault = $img.data('defecto');
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$img.attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}else{
		$img.attr('src', srcDefault);
	}
};

var guardarexamen=function(boton){
	var msjeAttention = '<?php echo JrTexto::_('Attention') ?>';
	var IDGUI = '<?php echo $idgui; ?>';
	tableSkillsToArray();
	var formData = new FormData($("#frm-"+IDGUI)[0]);
	$.ajax({
		url: _sysUrlBase_+'/examenes/guardar',
		type: "POST",
		data:  formData,
		contentType: false,
		dataType :'json',
		cache: false,
		processData:false,
		beforeSend: function(XMLHttpRequest){
			$('#procesando').show('fast'); 
			$(boton).attr('disabled', 'disabled');
			$(boton).find('i.fa').removeClass('fa-save').addClass('fa-circle-o-notch fa-spin');
		},
		success: function(data)
		{
			if(data.code==='ok'){
				mostrar_notificacion(msjeAttention, data.msj, 'success');
				$(boton).find('i.fa').removeClass('fa-circle-o-notch fa-spin').addClass('fa-save');
				$(boton).removeAttr('disabled', 'disabled');
				setTimeout(redir(_sysUrlBase_+'/examenes/preguntas/?idexamen='+data.new),800);
			}else{
				mostrar_notificacion(msjeAttention, data.msj, 'warning');
			}
			$('#procesando').hide('fast');
			return false;
		},
		error: function(xhr,status,error){
			mostrar_notificacion(msjeAttention, status, 'warning');
			$('#procesando').hide('fast');
			return false;
		}               
	});
};

var done_Habilidades = function(resp) {
	if(resp.code==200) {
		var data = resp.data;
		$('#habilidades_todas').val(data);
		ARRAY_HABILIDADES = data; /*el dataType de ajax indica que recibe un json, noe s necesario convertir*/

		limpiarTabla($('#tblHabilidades'));

		try{
			arraySkillsToTable();
			$('div[data-opcion="MANUAL"]').show();
		}catch(err){
			mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')).'.\n'.ucfirst(JrTexto::_('no valid data from external source')); ?>.', 'error');
			console.log(err)
		}
	} else {
		mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>!', message, 'error');
	}
};

var done_Niveles = function(resp) {
	if (resp.code!=200) { throw "An error has occurred." }
	ARRAY_NIVELES = resp.data;
	try {
		arrNivelesToTable();
		$('#tblEscalas').removeClass('hidden');
    	orndenartr();
		cargartxtAlfanumerico();
	}catch(err){
		mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')).'.\n'.ucfirst(JrTexto::_('no valid data from external source')); ?>.\n'+err.message, 'error');
		console.log(err)
	}
};

$(document).ready(function(){
	$('#tiempototal').mask("99:99:99");
	$('.istooltip').tooltip();
	$('.info_habilidades[data-toggle="popover"]').popover({
		html:true,
    	/*placement:'top',*/
		title: '<b>Accepted JSON format</b> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
		content: function(){
			var $contenido = $('#clone-popover-habilidades').clone();
			$contenido.removeAttr('id');
			return $contenido;
		},
	});
	$('.info_niveles[data-toggle="popover"]').popover({
		html:true,
    	/*placement:'top',*/
		title: '<b>Accepted JSON format</b> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
		content: function(){
			var $contenido = $('#clone-popover-niveles').clone();
			$contenido.removeAttr('id');
			return $contenido;
		},
	});

	$('body').on('click', '.popover button.close', function(e) {
		e.preventDefault();
		$('*[data-toggle="popover"]').trigger('click');
	});

    $('#level-item').change(function(){
    	var idnivel=$(this).val();
    	var data={tipo:'U','idpadre':idnivel}
    	var donde=$('#unit-item');
    	if(idnivel!=='') addniveles(leerniveles(data),donde);
    	else addniveles(false,donde);
    	donde.trigger('change');
    });
    $('#unit-item').change(function(){
    	var idunidad=$(this).val();
    	var data={tipo:'L','idpadre':idunidad}
    	var donde=$('#activity-item');
    	if(idunidad!=='') addniveles(leerniveles(data),donde);
    	else addniveles(false,donde);
    });

    $('#tipo-examen').change(function(){
    	cargarexamenes();
    });

    $('#nintento').change(function(){
    	vercalificacion();
    });

    $('#engrupo').change(function(){
    	verengrupo();
    });

    $('#tiempopor').change(function(){
    	vertiempopor();
    });

    $('.img-portada').click(function(e){ 
    	var txt="<?php echo JrTexto::_('Selected or upload'); ?> ";
    	selectedfile(e,this,txt);
    });

    $('.img-portada').load(function() {
    	var src = $(this).attr('src');
    	if(src!=''){
    		$(this).siblings('input').val(src);
    	}
    });

    $('#calificacionpor').change(function(){
    	vercalificacionpor();

    	$('#calificacionen').val($('#calificacionen option:first-child').val());
    	vercalificacionen();
    });

    $('#calificacionen').change(function(){
    	vercalificacionen();
    });

    $('#calificaciontotal').change(function(){
    	var val=$(this).val();
    	var _simbolo=$('#calificacionen').val();
    	calificacionmin(_simbolo,val,$('#calificacionmin'));
    });

    $('.pnlcalificacionenshow').on('click','.btn-removefila',function(){
    	borrarFilaTabla($(this));
    	orndenartr();
    	cargartxtAlfanumerico();
    }).on('click','.btn-addfila',function(){
    	var $tabla = $(this).closest('table');
    	var idFila = agregarFilaTabla($tabla);
    	var $selectNuevos = $tabla.find('#'+idFila+' select.form-control');
    	var ultimo_value = $tabla.find('#'+idFila).prev().find('select.form-control').last().val();

    	calificacionmin2('', ultimo_value, $selectNuevos);
    	orndenartr();
    	cargartxtAlfanumerico();
    	$selectNuevos.first().attr('readonly', 'readonly');
    });

	$('#origen_habilidades').change(function(e) {
		var valor = $(this).val();
		$('div[data-opcion]').hide();
		$('div[data-opcion="'+valor+'"]').show();
		ARRAY_HABILIDADES = [];
		if(valor=='MANUAL'){
			esFuenteExterna = false;
			limpiarTabla($('#tblHabilidades'));
			$('#tblHabilidades tfoot').show();
			//$('#fuente_externa').val('');
		}
		if(valor=='JSON'){
			esFuenteExterna = true;
			$('#tblHabilidades tfoot').hide();
		}
	});

	$('#tblHabilidades').on('click', '.btn.btn-addfila', function(e) {
		e.preventDefault();
		var $tabla = $(this).closest('table');
		agregarFilaTabla($tabla);
		enumerarSkill_id($tabla);
	}).on('click', '.btn.delete-skill', function(e) {
		e.preventDefault();
		var $tabla = $(this).closest('table');
		borrarFilaTabla($(this));
		enumerarSkill_id($tabla);
	});

	$('.btn.get-external-source').click(function(e) {
		e.preventDefault();
		var $btn = $(this);
		var urlSource = $btn.closest('.form-group').find('input').val() || '';
		var tipo = $btn.attr('data-tipo');

		var fnDone = function(){};
		if(tipo=="habilidad"){ fnDone = done_Habilidades; }
		else if(tipo=="nivel"){ fnDone = done_Niveles; }

		if(urlSource.trim()!='') {
			esFuenteExterna = true;
			$btn.find('i').removeClass('fa-cloud-download').addClass('fa-spinner fa-pulse');
			$btn.attr('disabled','disabled');

			$.ajax({
				url: urlSource,
				type: 'GET',
				dataType: 'json',
				data: null,
			}).done(fnDone).fail(function(xhr, textStatus, errorThrown) {
				mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')).'.\n'.ucfirst(JrTexto::_('no valid data from external source')); ?>', 'error');
				console.log(errorThrown);
			}).always(function() {
				$btn.removeAttr('disabled');
				$btn.find('i').removeClass('fa-spinner fa-pulse').addClass('fa-cloud-download');
			});
		}else{
			alert('You must write a valid URL.');
		}
	});

	$('#tblEscalas').on('change','select.form-control',function(){
		var i=$(this).val();

		obj=$(this).closest('tr[data-fila]').not($('.tr_clone')).next('tr[data-fila]').find('select.form-control');
		calificacionmin2('',i,obj);
		cargartxtAlfanumerico();
	});

	$('.table').on('blur','.txtNombreEscala',function(){	    	
		cargartxtAlfanumerico();
	});

	$('#tiene_certificado').change(function(e) {
		showFile_certificado();
	});

	$('#file_certificado').change(function(e) {
		var filename = $(this).val().split('\\').pop() || '';
		leerInputFile(this);
		$('#nombre_certificado').val(filename);
	});

	$('.btnsaveexamen').click(function(){
		guardarexamen(this);
	});

	<?php if(!empty($this->examen)) { ?>
	$('.btneliminarexamen').removeClass('hidden');
	<?php } ?>

	$('.btneliminarexamen').click(function(){
		var $btn = $(this);
		try{
			$.confirm({
	            title: '<?php echo JrTexto::_('Remove assessment');?>',
	            content: '<?php echo JrTexto::_('Are you sure you want to delete this').' '.JrTexto::_('assessment');?>?',
	            confirmButton: '<?php echo JrTexto::_('Accept');?>',
	            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
	            confirmButtonClass: 'btn-success',
	            cancelButtonClass: 'btn-default',
	            closeIcon: true,
	            confirm: function(){
	                var idexamen=$btn.attr('data-idexamen')||-1;
					var res = xajax__('', 'examenes', 'eliminar', parseInt(idexamen));
					$('.btn, a, button').attr('disabled','disabled');
					redir(_sysUrlBase_+'/examenes/');
	            },
	        });
		}catch(error){

		}
	});

	$('.showdemo').click(function(e) {
		e.preventDefault();
		var video = $(this).attr('data-videodemo');
		var ruta_video = _sysUrlStatic_+'/sysplantillas/examen/'+video;
		var name_tmpl = 'settings';
		var nombre_comun = '<?php echo JrTexto::_("Settings");?>';
		var idDemo = 'demo_'+name_tmpl;
		if( $('#demo_'+name_tmpl).length==0){
			var new_modal_video = $('#modalclone').clone();
			new_modal_video.removeAttr('id').attr('id', idDemo).addClass('video_demo');
			new_modal_video.find('.modal-header #modaltitle').text('Video Demo: '+nombre_comun);
			new_modal_video.find('.modal-body').html('<div class="col-xs-12"><div class="embed-responsive embed-responsive-16by9 mascara_video"><video controls="true" class="valvideo embed-responsive-item vid" src="'+ruta_video+'"></video></div></div>');
			$('body').append(new_modal_video);
		}

		$('#'+idDemo).modal();
		$('#'+idDemo).trigger('resize');
	});

    //funciones de inicio
    verengrupo();
    vertiempopor();
    vercalificacionpor();
    vercalificacionen();
    vercalificacion();
    preCarga_Skills();
    showFile_certificado();
    showexamen('setting');
    
    if(tieneIdCurso){ $('.get-external-source').trigger('click'); }
});
</script>