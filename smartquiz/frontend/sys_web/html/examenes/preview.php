<?php
$examen = $this->examen;
$preguntas=@$this->preguntas;
$rutabase = $this->documento->getUrlBase();
$arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
$srcPortada = (!empty($examen['portada']!=''))? str_replace('__xRUTABASEx__', $rutabase, $examen['portada']) : '';
$tipoEvaluacion = '';
if($examen['calificacion_en']=='N') { 
    $tipoEvaluacion = ' pts.'; 
    $calificacion_en = ucfirst(JrTexto::_("numeric"));
} else if($examen['calificacion_en']=='P') { 
    $tipoEvaluacion = '%'; 
    $calificacion_en = ucfirst(JrTexto::_("percentage"));
} else {
    $calificacion_en = ucfirst(JrTexto::_("alfabetic"));
}
$calif_minima = $examen["calificacion_min"];
$calif_total = $examen["calificacion_total"];
if($examen['calificacion_en']=='A'){
    $calif_minima = 51;
    $jsonEscalas = json_decode($examen["calificacion_total"], true);
    $calif_total = '<ul>';
    if(!empty($jsonEscalas) && is_array($jsonEscalas)){
        foreach ($jsonEscalas as $index=>$escala) {
            $calif_total .= '<li>'.ucfirst($escala["nombre"]).' ('.$escala["max"].' - '.$escala["min"].' pts)</li>';
        }
    }
    $calif_total .= '</ul>';
}
?>
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_verdad_falso.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_ordenar.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_fichas.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_imagen_puntos.css">
<link rel="stylesheet" href="<?php echo $this->documento->getUrlTema()?>/css/actividad_completar.css">
<style type="text/css">
    #examen_preview{
        margin:  0;
    }
    #examen_preview .encabezado{
        border-radius: 7px;
        box-shadow: 0px 7px 6px -5px #aaa;
        margin-bottom: 3ex;
    }
    #examen_preview .pnlpresentacion {
        padding: 0 20px;
    }
    #examen_preview #portada{
        border-radius: 50%;
        min-height: 18.277em;
    }
    
    #examen_preview .title_desc-zone{
        padding: 6% 0;
    }

    #examen_preview .portada-zone,.title_desc-zone{
        display: inline-block;
        padding: 1% 0;
    }

    #examen_preview .panel{
        margin-bottom: 0;
    }

    .pnlitem , .pnlitem .tmpl-ejerc{
        display: none;
    }
    .pnlitem.active,.pnlitem .tmpl-media, .pnlitem .tmpl-ejerc.active{
        display: block;        
    }   
    .panel-body{
    	min-height: 390px;
        position: relative;
    }
    #pregunta-timer{
        position: absolute;
        right: 12px;
        z-index: 100;
    }
    #pregunta-timer .timer{
        background: #7ac1a1;
        border-radius: 50%;
        box-shadow: 0px 1px 10px -2px #444;
        display: inline-block;
        height: 80px;
        width: 80px;
    }
    #pregunta-timer .time-elapsed{
        color:  #fff;
        font-weight: bolder;
        margin-top: -0.7em;
        position: absolute;
        text-align: center;
        top: 50%;
        width: 100%;
    }

    #examen_preview .pnlpreguntas .tmpl-ejerc.tiempo-acabo:before {
        background: rgba(255,255,255,0);
        content: ' ';
        height: 100%;
        position: absolute;
        width: 100%;
        z-index: 10;
    }
</style>
<div class="row" id="examen_preview">
    <!--div class="col-xs-12">
        <a href="javascript:history.back();" class="btn btn-default atras"><i class="fa fa-arrow-left"></i> <?php echo ucfirst(JrTexto::_("Go back")) ?></a>
    </div-->
    <?php if(!empty(@$examen["idexamen"])){ ?>
    <div class="col-xs-12">
        <div class="input-group" style="margin: 0;">
            <div class="input-group-addon"><?php echo ucfirst(JrTexto::_("Link to solve the exam"))?></div>
            <input type="url" class="form-control" id="fuente_externa" name="fuente_externa" placeholder="<?php echo ucfirst(JrTexto::_("e.g."))?>: https://www.myweb.com/skills/list" value="<?php echo $this->documento->getUrlBase().'/examenes/resolver/?idexamen='.@$examen["idexamen"]; ?>" readonly="readonly" style="background-color: #fff;">
        </div>
        <br>
    </div>
    <?php } ?>
    <div class="col-xs-12">
        <div class="panel">
            <div class="panel-heading bg-blue">
                <div class="col-md-6">
                    <div><ol class="breadcrumb" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
                        <li><a href="<?php echo $this->documento->getUrlBase() ?>/examenes/" style="color:#fff"><?php echo JrTexto::_("Assessment"); ?></a></li>
                        <li class="active"  style="color:#ccc"><?php echo JrTexto::_("Preview"); ?></li>
                    </ol>
                </div>
            </div>
            <div class="col-md-6">
                <?php if($this->showBtnPrint){ ?>
                <a href="#" class="btn btn-xs btn-default pull-right print" data-idexamen="<?php echo $examen['idexamen']; ?>"><i class="fa fa-print"></i> <?php echo JrTexto::_("Print"); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div id="barra-progreso">
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        <span>0</span>%
                    </div>
                </div>
            </div>
            <div id="pregunta-timer" style="display: none;">
                <div class="timer"></div>
                <div class="time-elapsed"><?php echo ($examen['tiempo_por']=='E')? $examen['tiempo_total']:'00:00:00' ?></div>
            </div>
            <div class="pnlpresentacion  pnlitem">
                <?php if(!empty($examen)){?>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center bg-blue encabezado">
                    <?php $col = 12;
                    if($srcPortada!='') { $col = 8; ?>
                    <div class="col-xs-12 col-sm-3 portada-zone">
                        <img src="<?php echo $srcPortada; ?>" alt="img-portada" id="portada" class="img-responsive">
                    </div>
                    <?php } ?>
                    <div class="col-xs-12 col-sm-<?php echo $col; ?> title_desc-zone">
                        <h2><?php echo ucfirst($examen["titulo"]); ?></h2>
                        <small><?php echo ucfirst($examen["descripcion"]); ?></small>
                    </div>
                </div>
                <?php } ?>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="table-key-value">
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("Scored by")); ?></div>
                            <div class="value"><?php echo ($examen["calificacion_por"]=='Q'?ucfirst(JrTexto::_('Question')):ucfirst(JrTexto::_('Assessment'))); ?></div>
                        </div>
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("type of score")); ?></div>
                            <div class="value"><?php echo $calificacion_en; ?></div>
                        </div>
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("max. score")); ?></div>
                            <div class="value"><?php echo $calif_total.$tipoEvaluacion; ?></div>
                        </div>
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("min. score to pass")); ?></div>
                            <div class="value"><?php echo $calif_minima.$tipoEvaluacion; ?></div>
                        </div>
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("number of attempts")); ?></div>
                            <div class="value"><?php echo ($examen['nintento']==$this->max_intento)?JrTexto::_("Unlimited"):$examen['nintento']; ?></div>
                        </div>
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("Choose")); ?></div>
                            <div class="value"><?php echo ucfirst($examen["calificacion"]=='M'?JrTexto::_("Best try"):JrTexto::_("Last try")); ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="table-key-value">
                        <div class="table-kv-row">
                            <div class="key"><?php echo ucfirst(JrTexto::_("objective").'s').' / '. ucfirst(JrTexto::_("skills")); ?></div>
                            <div class="value">
                                <ul class="list-unstyled_">
                                    <?php $habilidades_todas = json_decode(@$examen['habilidades_todas'], true);
                                    if(!empty(@$habilidades_todas)){
                                        foreach (@$habilidades_todas as $hab) {
                                            echo '<li>'.ucfirst(JrTexto::_(  $hab['skill_name'] )).'</li>';
                                        }} ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-md-12 text-center">
                        <a href="#" class="btn btn-lg btn-primary startexam"><?php echo JrTexto::_("Start Assessment") ?> <i class="fa fa-arrow-right"></i></a>
                    </div>                    	
                </div>
                <?php if(!empty($preguntas))
                    foreach ($preguntas as $pregunta){ $codeidgui=uniqid(); ?>
                <div class="col-xs-12 col-sm-12 pnlpreguntas pnlitem" style="font-family: '<?php echo $examen['fuente']; ?>'; font-size: <?php echo $examen['fuentesize'].'px'; ?>;">
                    <?php 
                    $nomedia=12;
                    if($pregunta["F"]){
                        $preg=$pregunta["F"][0];
                        $tpltipo=$preg["template"];
                        $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc'; 
                        $nomedia=6;
                        ?>
                    <div class="col-xs-12 col-sm-6 padding-0 tmpl tmpl-<?php echo $tpl; ?>" data-tipo="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>">
                        <?php if($tpltipo=='image'){ ?>
                        <div class="btns-img">
                            <button class="btn btn-info full-size" title="<?php echo ucfirst(JrTexto::_('Full size')); ?>"><i class="fa fa-external-link-square fa-2x"></i></button>
                        </div>
                        <?php } 
                        echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                    </div>
                    <?php } 
                    if($pregunta["P"]){
                        foreach ($pregunta["P"] as $preg) {
                            $tpltipo=$preg["template"];
                            $tpl=($tpltipo=='image'||$tpltipo=='audio'||$tpltipo=='video')?'media':'ejerc';?>
                    <div class="col-xs-12 col-sm-<?php echo $nomedia; ?> col-md-<?php echo $nomedia; ?> tmpl tmpl-<?php echo $tpl; ?>" data-tmpl="<?php echo $tpltipo; ?>" data-cls="<?php echo $tpltipo; ?>_question" data-idpregunta="<?php echo $preg["idpregunta"]; ?>" data-habilidades="<?php echo $preg["habilidades"]; ?>" data-tiempo="<?php echo $preg["tiempo"]; ?>" data-puntaje="<?php echo $preg["puntaje"]; ?>">
                        <?php echo @str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),@$preg["ejercicio"]); ?>
                    </div>
                    <?php } } ?>
                </div>                     
                <?php } ?>  
                <div class="pnlresultado pnlitem">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <h3><strong><?php echo ucfirst(JrTexto::_("Result")) ?></strong></h3>
                    </div>                	
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center" id="infototalcalificacion">

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <hr>
                        <h3><strong><?php echo ucfirst(JrTexto::_("results by skill")); ?></strong></h3>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12" id="resultskill">
                        <?php if(!empty($this->habilidades)){ $i=-1;
                            foreach ($this->habilidades as $hab) {  $i++;
                                if($i>count($arrColoresHab)){ $i=0; } ?>
                            <div class="col-xs-12 col-sm-6 padding-0 skill sk_<?php echo $hab["skill_id"] ?>">
                                <div class="col-xs-3 col-sm-3 col-md-4 circulo" data-idskill="<?php echo $hab["skill_id"] ?>"  data-value="0" data-texto=" " data-ccolorout="<?php echo $arrColoresHab[$i]; ?>"></div>
                                <div class="col-xs-9 col-sm-9 col-md-8 text-left bolder nombre-skill"><br><?php echo JrTexto::_($hab["skill_name"]);?></div>
                            </div>
                            <?php } 
                        } else{
                            echo "<h4 class='text-center'>No se han registrado Habilidades/Objetivos</h4>";
                        } ?>
                        </div> 
                    </div> 
                </div> 
            </div>
            <div class="panel-footer bg-blue text-center" id="controlfooter"  style="min-height: 57px;">
                <div class="btn-group btn-group-left" style="display: none;">
                    <a href="#" class="btn btn-primary irprev hidden"><i class="fa fa-arrow-left"></i></a>
                    <a href="#" class="btn btn-primary irinfo">-</a>
                    <a href="#" class="btn btn-primary irnext"><i class="fa fa-arrow-right"></i></a>
                </div>
                <input type="hidden" id="calificacion_por" value="<?php echo @$examen["calificacion_por"]; ?>" >
                <input type="hidden" id="calificacion_en" value="<?php echo @$examen["calificacion_en"]; ?>" >
                <textarea class="hidden" type="hidden" id="calificacion_total"><?php echo @$examen["calificacion_total"]; ?></textarea>
                <input type="hidden" id="calificacion_min" value="<?php echo @$examen["calificacion_min"]; ?>">
                <input type="hidden" id="calificacion_pregunta" value="0">
                <input type="hidden" id="calificacion_acumulada" value="0">
            </div>
        </div>
    </div>
</div>

<section id="msjes_idioma_php" class="hidden" style="display: none !important; ">
    <input type="hidden" id="you_failed_exam" value='<?php echo ucfirst(JrTexto::_('You failed the assessment')); ?>'>
    <input type="hidden" id="you_passed_exam" value='<?php echo ucfirst(JrTexto::_('You passed the assessment')); ?>'>
    <input type="hidden" id="your_score_below" value='<?php echo ucfirst(JrTexto::_('Your score is below the preset scales')); ?>'>
    <input type="hidden" id="out_of" value='<?php echo JrTexto::_('out of'); ?>'>
    <input type="hidden" id="your_result_in_scale" value='<?php echo ucfirst(JrTexto::_('your result is located on the scale')); ?>'>
</section>

<script type="text/javascript">
var _TIEMPO_POR= '<?php echo $examen['tiempo_por'] ?>';

var infopregunta=function(){
    var total= $('.panel-body .tmpl-ejerc').length;
    var indice=$('.panel-body .tmpl-ejerc.active .titulo_descripcion .titulo').attr('data-orden')||0;
    if(indice>0) $('#controlfooter a.irinfo').html(indice+' / '+total);
};

var initCircleTimer = function(selector='', seconds, $ejercActivo=''){
    var timeMilisegundos = seconds*1000;
    $ejercAddClass = ($ejercActivo!='')? $ejercActivo : $('.tmpl.tmpl-ejerc');
    if(selector!=''){
        $(selector).circletimer({
            onComplete: function() {
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>', '<?php echo JrTexto::_('Time is over');?>', 'warning');
                $ejercAddClass.addClass('tiempo-acabo').removeClass('tiempo-inicio');
            },
            onUpdate: function(elapsed) {
                var seg_transcurrido = Math.round(elapsed/1000);
                var new_time = seconds-seg_transcurrido;
                if(new_time==0) miliseconds = new_time;
                var h = Math.floor(new_time / 3600);
                var m = Math.floor(new_time % 3600 / 60);
                var s = Math.floor(new_time % 3600 % 60);

                var hh = (h<10?"0":"")+h;
                var mm = (m<10?"0":"")+m;
                var ss = (s<10?"0":"")+s;
                $('#pregunta-timer .time-elapsed').html(hh+':'+mm+':'+ss);
                $ejercAddClass.attr('data-tiemporestante', hh+':'+mm+':'+ss);
            },
            timeout: timeMilisegundos
        });
    } else {
        alert('Impossible initialize time!!');
    }
};

var setTiempo = function(){
    var tiempo = '';
    var $ejercActivo = $('.panel-body div.pnlitem.active div.tmpl-ejerc.active');
    if(_TIEMPO_POR=='E'){
        tiempo = $('#pregunta-timer .time-elapsed').text();
        $ejercActivo = $('.panel-body .tmpl.tmpl-ejerc');
    }
    if(_TIEMPO_POR=='Q'){
        if($('#pregunta-timer .timer svg').length>0) $('#pregunta-timer .timer').circletimer("pause");
        $ejercActivo.prev().addClass('tiempo-pausa').removeClass('tiempo-inicio').removeClass('tiempo-acabo');
        tiempo = ($ejercActivo.attr('data-tiemporestante')!=undefined)? $ejercActivo.attr('data-tiemporestante') : $ejercActivo.attr('data-tiempo');
    }

    if($ejercActivo.length>0 && tiempo!=''){
        var arrTime = tiempo.split(':');
        var seconds = (+arrTime[0])*60*60 + (+arrTime[1])*60 + (+arrTime[2]);
        var miliseconds = seconds*1000;
        $('#pregunta-timer .time-elapsed').html(tiempo);
        $('#pregunta-timer').show();
        if(_TIEMPO_POR=='E' && $('#pregunta-timer .timer svg').length==0) initCircleTimer('#pregunta-timer .timer', seconds);
        if(_TIEMPO_POR=='Q') initCircleTimer('#pregunta-timer .timer', seconds, $ejercActivo);                
    } else {
        $('#pregunta-timer').hide();
        $('#pregunta-timer .time-elapsed').html('00:00:00');
    }
};

var actualizarBarraProgreso = function(){
    var $listPregs= $('.panel-body .tmpl-ejerc');
    var $ejercActivo = $('.panel-body .tmpl-ejerc.active');
    var npreguntas= $listPregs.length;
    var indice = $listPregs.index($ejercActivo);
    if(indice>=0){
        var avance = (indice*100)/npreguntas;
        avance = Math.floor(avance);
    } else {
        var avance = 100;
    }
    $('#barra-progreso .progress-bar').css('width', avance+'%');
    $('#barra-progreso .progress-bar').attr('aria-valuenow', avance);
    $('#barra-progreso .progress-bar span').text(avance);
};

var numerarpreguntas=function(){
    $i=0;
    $('b.npregunta').remove().siblings('b').remove();
    $('.panel-body').find('.tmpl-ejerc').each(function(){
        var obj= $(this).find('.titulo_descripcion .titulo');
        var tmpl = $(this).data('tmpl');
        var instruccion = getInstruccionTmpl(tmpl);
        $(this).find('.titulo_descripcion>b').remove();
        obj.find('.plantilla').removeClass('editando');
        $i++;
        //if(obj.text()!=''&&obj!=undefined)
        obj.before('<b class="npregunta">'+($i)+'. '+instruccion+'</b>');
        obj.attr('data-orden',$i)
    });
};

var iniciar=function(){
    $('.panel-body div.pnlitem:first').addClass('active').nextAll().removeClass('active');
    $('.panel-body div.pnlitem div.tmpl-ejerc:first').addClass('active').nextAll().removeClass('active');
    $('.plantilla-completar').examMultiplantilla();
    $('.plantilla-verdad_falso').examTrueFalse();
    $('.plantilla-ordenar.ord_simple').examOrdenSimple();
    $('.plantilla-ordenar.ord_parrafo').examOrderParagraph();
    $('.plantilla-fichas').examJoin();
    $('.plantilla-img_puntos').examTagImage();
    infopregunta();
    numerarpreguntas();
};

var calcularpuntaje=function(){
    var  cpor=$('#calificacion_por').val()||'E';
    var  cen=$('#calificacion_en').val()||'P';
    var  ctotal=$('#calificacion_total').val()||100;
    var  cmin=$('#calificacion_min').val()||51;
    var npreguntas= $('.panel-body .tmpl-ejerc').length;
    var pxpregunta=0;
    var pacumuladatotal=0;
    var habilidades={};
    var datah={};
    var arrEscala = [];
    if(cpor=='E' && cen=='A'){
        var escalaAlfab = $.parseJSON(ctotal);
        ctotal = 100;
        cmin = 51; /* el minimo apra aprobar Alfanumerico */
    }

    if(cpor=='Q' && cen=='N'){
        ctotal = 0;
        $('.panel-body').find('.tmpl-ejerc').each(function(){
            var plantilla= $(this);
            var puntaje = plantilla.attr('data-puntaje')||0;
            ctotal = parseFloat(ctotal) + parseFloat(puntaje); 
        });
    } else {
        pxpregunta=ctotal/npreguntas;
    }
    
    
    $('.panel-body').find('.tmpl-ejerc').each(function(){
        var plantilla= $(this);                
        if(cpor=='Q' && cen=='N'){            
            var puntaje = plantilla.attr('data-puntaje')||0;
            pxpregunta = puntaje;
        }
        var tmpl=plantilla.attr('data-tmpl')||'';
        if(tmpl!=''){
            var totalitems=0;
            var itemsgood=0;
            var itemsbad=0;
            var tpl_skill=$(this).attr('data-habilidades')||'';
            if(tmpl=='tag_image'){
                totalitems=plantilla.find('.playing .dot-container').length;
                itemsbad=plantilla.find('.dot-tag[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.dot-tag[data-corregido="good"]').length;
            }else if(tmpl=='options'||tmpl=='click_drag'||tmpl=='select_box'||tmpl=='gap_fill'){
                totalitems=plantilla.find('.panelEjercicio input').length;
                itemsbad=plantilla.find('.panelEjercicio .inp-corregido[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.panelEjercicio .inp-corregido[data-corregido="good"]').length;
            }else if(tmpl=='true_false'){
                totalitems=plantilla.find('.ejercicio .premise').length;
                itemsbad=plantilla.find('.ejercicio .premise.bad').length;
                itemsgood=plantilla.find('.ejercicio .premise.good').length;
            }else if(tmpl=='order_paragraph'){
                totalitems=plantilla.find('.ejercicio .drag-parr div').length;
                itemsbad=plantilla.find('.drop-parr .filled[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.drop-parr .filled[data-corregido="good"]').length;
            }else if(tmpl=='order_simple'){
                totalitems=plantilla.find('.element .texto .drag>div').length;
                itemsbad=plantilla.find('.drop .parte[data-corregido="bad"]').length;
                itemsgood=plantilla.find('.drop .parte[data-corregido="good"]').length;
            }else if(tmpl=='join'){
                totalitems=plantilla.find('.ejercicio .partes-2 .ficha').length;
                itemsbad=plantilla.find('.ejercicio .partes-2 .ficha.bad').length;
                itemsgood=plantilla.find('.ejercicio .partes-2 .ficha.good').length;
            }else if(tmpl=='speach'){               
                totalitems=parseInt(plantilla.find('.speachtexttotal').attr('total-palabras')||0);
                plantilla.attr('data-items',totalitems);
                itemsgood=parseInt(plantilla.find('.speachtexttotal').attr('total-ok')||0);
                itemsbad=totalitems-itemsgood;
            }else if(tmpl=='nwrite'){         
                console.log(plantilla)       
                totalitems=parseInt(plantilla.find('.respuestaAlumno').attr('total-palabras')||0);
                plantilla.attr('data-items',totalitems);
                itemsgood=parseInt(plantilla.find('.respuestaAlumno').attr('total-acertado')||0);
                itemsbad=totalitems-itemsgood;
            }else if(tmpl=='nspeach'){               
                totalitems=1;
                plantilla.attr('data-items',totalitems);
                itemsgood=1;
                itemsbad=0;
            }
            var puntajeacumulado=0;                 
            if(plantilla.attr('data-items')==undefined){              
                plantilla.attr('data-items',totalitems);                        
            }else{                
                totalitems=plantilla.attr('data-items')||0;
            }            
            if(pxpregunta>0&&totalitems>0){
                puntajeacumulado=(pxpregunta*itemsgood)/totalitems;
                pacumuladatotal=pacumuladatotal+puntajeacumulado;
            }
            var skills=tpl_skill.split('|');
            $.each(skills,function(key,val){
                if(val!=''){
                    if(datah[val]==''||datah[val]==undefined){
                        datah[val]={'n':1,'p':parseFloat(puntajeacumulado),'pt':parseFloat(pxpregunta)};
                    }else{
                        datah[val].n++;
                        datah[val].p=parseFloat(datah[val].p)+parseFloat(puntajeacumulado);
                        datah[val].pt=parseFloat(datah[val].pt)+parseFloat(pxpregunta);
                    }
                    habilidades[val]=datah[val];
                }                       
            });           
            plantilla.attr('data-itemsbad',itemsbad);
            plantilla.attr('data-itemsgood',parseInt(itemsgood));
            plantilla.attr('data-score',puntajeacumulado);
        }
    });
    $('#calificacion_pregunta').val(pxpregunta);
    $('#calificacion_acumulada').val(pacumuladatotal);
    var sb='';
    var html='';
    if(pacumuladatotal>cmin) {
        html='<div class="color-green2"><h2>'+MSJES_PHP.you_passed_exam+'</h2>';
    } else {
        html='<div class="color-red"><h2>'+MSJES_PHP.you_failed_exam+'</h2>';
    }

    if(cen=='P') html+='<h3> '+(pacumuladatotal.toFixed(2))+'% de '+ctotal+'% </h3>';
    else if(cen=='A'){
        var ptosAcum = pacumuladatotal.toFixed(2);
        if(ptosAcum>100) ptosAcum = 100;
        var obj = {};
        if(typeof escalaAlfab!="number"){
            $.each(escalaAlfab, function(i, escala) {
                if( ptosAcum>parseFloat(escala.min) && ptosAcum<=parseFloat(escala.max) ){
                    obj = {
                        'nombre' : escala.nombre,
                        'puntaje_min' : escala.min,
                        'ptosAcum' : ptosAcum,
                    };
                }
            });
        }
        if($.isEmptyObject(obj)){
            html += '<h3> '+MSJES_PHP.your_score_below+'. <br>'+ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h3>';
        } else {
            html += '<h3> '+MSJES_PHP.your_result_in_scale+': "<b>'+obj.nombre+'</b>". </h3><h4>'+obj.ptosAcum+'pts '+MSJES_PHP.out_of+' '+ctotal+' </h4>';
        }
    }
    else html+='<h3> '+(pacumuladatotal.toFixed(2))+' de '+ctotal+' puntos </h3>';
    html+='</div>';
    $('#infototalcalificacion').html(html);
    $('#resultskill .skill').find('.circulo').html('');
    $('#resultskill .skill').hide();
    $.each(habilidades,function(key,val){
        var $skillobj=$('#resultskill .skill.sk_'+key);
        var $circulo_skill = $skillobj.find('.circulo');
        if($skillobj.length>0){
            $skillobj.show();
            var p=val.p||0;
            var pt=parseFloat(val.pt)||0;
            $circulo_skill.attr('data-acumulado',p);
            $circulo_skill.attr('data-total',pt);
            if(p>0&&pt>0){
                var val=(p*100)/pt;
                $circulo_skill.attr('data-value',val.toFixed(0));
            }else{
                $circulo_skill.attr('data-value',0);
            }

        } 
        $circulo_skill.graficocircle();                 
    }); 
};

$(document).ready(function(){
    cargarMensajesPHP('#msjes_idioma_php');

    $('.startexam').click(function(){
    	$('#controlfooter .irnext').trigger('click');
    });

    $('.btns-img .full-size').click(function(e) {
        e.preventDefault();
        var imgSrc = $(this).closest('.tmpl').find('img').attr('src');
        var idPreg = $(this).closest('.tmpl').data('idpregunta');
        var idModal = 'imagen_'+idPreg;
        if( $('#'+idModal).length==0 ){
            var $modal = $('#modalclone').clone();
            $modal.attr('id',idModal);
            $modal.find('#modaltitle').text('<?php echo ucfirst(JrTexto::_('Image')); ?>');
            console.log(imgSrc);
            $modal.find('#modalcontent').html('<img src="'+imgSrc+'" class="center-block">');
            $('body').append($modal);
        }
        $('#'+idModal).modal();
    });

    $('#controlfooter')
    .on('click','.irprev',function(ev){
        ev.preventDefault();
        $('audio, video').each(function(){ this.pause() });
        var divactive=$('.panel-body div.pnlitem.active');
        if(!divactive.hasClass('pnlpresentacion')){
            var subitems=divactive.find('.tmpl-ejerc');
            if(subitems.length>0){
                var subitemsactive=divactive.find('.tmpl-ejerc.active');
                if(subitemsactive.length>0){
                	var subi=subitemsactive.removeClass('active').prev();
                	if(subi.length>0&&subi.index()>0){
                       subi.addClass('active'); 
                    }else { 
                        divactive.removeClass('active').prev().addClass('active').find('.tmpl-ejerc:last').addClass('active').prevAll().removeClass('active');
                    }
                }else{
                    divactive.find('.tmpl-ejerc:last').addClass('active').prevAll().removeClass('active');
                }
            }else{
                divactive.removeClass('active').prev().addClass('active').find('.tmpl-ejerc:last').addClass('active').prevAll().removeClass('active');
            }
        }
        infopregunta();
        setTiempo();
    })
    .on('click','.irnext',function(ev){
        ev.preventDefault();
        $('audio, video').each(function(){ this.pause() });
        var divactive=$('.panel-body div.pnlitem.active');
        if(!divactive.hasClass('pnlresultado')){
            var subitems=divactive.find('.tmpl-ejerc');           
            if(subitems.length>0){
                var subitemsactive=divactive.find('.tmpl-ejerc.active');                
                if(subitemsactive.length>0){
                    var subi=subitemsactive.removeClass('active').next();                   
                    if(subi.length==0){
                        var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');
                        tpl.addClass('active').nextAll().removeClass('active');                        
                        var plt=tpl.children('.plantilla');
                        if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                        else if(plt.hasClass('plantilla-nlsw'))  setTimeout(function(){edithtml_all({id:plt.find('.respuestaAlumno').children('textarea').attr('id')})},550);
                    }else{                        
                        subi.addClass('active');
                        var plt=subi.children('.plantilla');
                        if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                        else if(plt.hasClass('plantilla-nlsw'))  setTimeout(function(){edithtml_all({id:plt.find('.respuestaAlumno').children('textarea').attr('id')})},550);

                    }
                }else{
                    var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');
                    tpl.addClass('active');
                     var plt=tpl.children('.plantilla');
                    if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                    else if(plt.hasClass('plantilla-nlsw'))  setTimeout(function(){edithtml_all({id:plt.find('.respuestaAlumno').children('textarea').attr('id')})},550);
                }
            }else{    
                var tpl=divactive.removeClass('active').next().addClass('active').find('.tmpl-ejerc:first');      
                tpl.addClass('active');
                var plt=tpl.children('.plantilla');
                if(plt.hasClass('plantilla-speach')){ mostrarondaSpeach(plt.find('.pnl-speach.docente')); }
                else if(plt.hasClass('plantilla-nlsw'))  setTimeout(function(){edithtml_all({id:plt.find('.respuestaAlumno').children('textarea').attr('id')})},550);
            } 
            calcularpuntaje();
            if($('.panel-body div.pnlitem.active').hasClass('pnlresultado')) {
                //objtimer.removeClass('active');
                //$('#curtimerexamen').trigger('oncropausar');
                $('#pregunta-timer .timer').circletimer("pause");
                //guardarResultado();
            }
        }else{
            calcularpuntaje();
        }

        if($('.pnlpreguntas').hasClass('active')) $('.panel-footer .btn-group').show();
        else $('.panel-footer .btn-group').hide();

        infopregunta();
        actualizarBarraProgreso();
        setTiempo();
    });

    showexamen('preview');      
    iniciar();

    $('.panel .btn.print').click(function(e){
        var new_modal =  $('#modalclone').clone();
        var now = Date.now();
        var idexamen = $(this).data('idexamen');
        var ruta_plantilla = _sysUrlBase_ +'/examenes/imprimir/?idexamen='+idexamen;
        new_modal.removeAttr('id').attr('id', 'print_'+now).addClass('imprimir');
        new_modal.find('#modaltitle').text('<?php echo JrTexto::_("Print view"); ?>');
        $.get(ruta_plantilla, function(data) {
            new_modal.find('#modalcontent').html(data);
            new_modal.find('#modalfooter').find('.btn.cerrarmodal').addClass('pull-left');
            new_modal.find('#modalfooter').append('<a href="#" class="btn btn-primary pull-right btn-imprimir"><i class="fa fa-print"></i> <?php echo JrTexto::_("Print"); ?></a>');
        });
        $('body').append(new_modal);
        $('#print_'+now).modal({backdrop: 'static', keyboard: false});
    });

    $('body').on('hidden.bs.modal', '.modal.imprimir', function (e) {        
        $('.modal.imprimir').remove();
    }).on('click', '.btn-imprimir', function(e) {
        e.preventDefault();
        
       // $('body').wrap('<div id="body-content" class="hidden"></div>');
       $('#body-content').addClass('hidden-print');
       $('div.container').hide();
       $('.modal.in').hide();
       var contenido_impresion = $('body .modal.in .modal-body').clone();
       $('body').append('<div id="contenido_impresion">'+contenido_impresion.html()+'</div>');

        /*var elem = document.getElementById("modalcontent");
        var domClone = elem.cloneNode(true);
    
        var $printSection = document.getElementById("printSection");
        
        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            document.body.appendChild($printSection);
        }
        
        $printSection.innerHTML = "";
        
        $printSection.appendChild(domClone);*/
        window.print();
        $('#contenido_impresion').remove();
        $('div.container').show();
        $('.modal.in').show();
    });

    $(".panel-body").on("mousedown", ".pnlitem.active .tmpl-ejerc.active",function(e) {
        if( !$(this).hasClass('tiempo-acabo') && !$(this).hasClass('tiempo-inicio') ){
            $('#pregunta-timer .timer').circletimer("start");
            if(_TIEMPO_POR=='E') $('.panel-body .tmpl.tmpl-ejerc').addClass('tiempo-inicio');
            else if(_TIEMPO_POR=='Q') $(this).addClass('tiempo-inicio').removeClass('tiempo-pausa');
        }
    });

    if(_TIEMPO_POR=='E') setTiempo();
});
</script>