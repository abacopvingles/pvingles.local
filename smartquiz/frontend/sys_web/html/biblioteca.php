<?php $type=!empty($_GET["type"])?$_GET["type"]:'all'; ?>
<style>
  #cabecera-biblioteca{ background-color: rgb(68, 102, 187); padding: .8em 0; margin: 0 -30px; }
  .paginationjs{ display: inline-block; }
  .modal-body{ padding-bottom: 0; padding-top: 0; }
</style>

<div class="container-fluid">
  <div class="row" id="cabecera-biblioteca">
    <div class="col-md-6 col-sm-6 col-xs-12 text-center">   
      <div class="btn-group">
      <!--
        <span type="button" class="btn btn-default disabled"><?php echo JrTexto::_('Upload file') ?></span>
        -->
        <?php if($type=='video'||$type=='all'){?> 
        <div class="btn btn-lg btn-file btn-file-2 btn btn-primary istooltip" title="<?php echo JrTexto::_('selecciona un archivo desde su computadora') ?>" data-placement="bottom">
          <i class="fa fa-video-camera"></i> 
          <?php echo JrTexto::_('upload') ?> video
          <input type="file" id="filearchivo" name="filearchivo" data-tipo="video" data-url="" class="image-input btnsavefile" accept="video/*">
        </div>
        <?php }if($type=='audio'||$type=='all'){?> 
        <div class="btn btn-lg btn-file btn-file-2 btn btn-primary istooltip" title="<?php echo JrTexto::_('selecciona un archivo desde su computadora') ?>" data-placement="bottom">
          <i class="fa fa-music"></i> 
          <?php echo JrTexto::_('upload') ?> audio
          <input type="file" id="filearchivo" name="filearchivo" data-tipo="audio" data-url="" class="image-input btnsavefile" accept="audio/*">
        </div>
        <?php }if($type=='image'||$type=='all'){?>
        <div class="btn btn-lg btn-file btn-file-2 btn btn-primary istooltip" title="<?php echo JrTexto::_('selecciona un archivo desde su computadora') ?>" data-placement="bottom">
          <i class="fa fa-photo"></i> 
          <?php echo JrTexto::_('upload').' '.JrTexto::_('image') ?> 
          <input type="file" id="filearchivo" name="filearchivo" data-tipo="image" data-url="" class="image-input btnsavefile" accept="image/*">
        </div>
        <?php } if($type=='pdf'||$type=='all'){?>
        <div class="btn btn-lg btn-file btn-file-2 btn btn-primary istooltip" title="<?php echo JrTexto::_('selecciona un archivo desde su computadora') ?>" data-placement="bottom">
          <i class="fa fa-file-pdf-o"></i> 
          <?php echo JrTexto::_('upload').' '.JrTexto::_('file') ?> 
          <input type="file" id="filearchivo" name="filearchivo" data-tipo="pdf" data-url="" class="image-input btnsavefile" accept="application/pdf">
        </div>
        <?php } if($type=='ppt'||$type=='all'){?>
        <div class="btn btn-lg btn-file btn-file-2 btn btn-primary istooltip" title="<?php echo JrTexto::_('selecciona un archivo desde su computadora') ?>" data-placement="bottom">
          <i class="fa fa-file-powerpoint-o"></i> 
          <?php echo JrTexto::_('upload').' '.JrTexto::_('file') ?> 
          <input type="file" id="filearchivo" name="filearchivo" data-tipo="ppt" data-url="" class="image-input btnsavefile" accept="application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.openxmlformats-officedocument.presentationml.presentation">
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 text-center">     
      <div class="form-group col-sm-12" style="margin:0">   

          <div class="input-group " style="margin:0">
            <input type="hidden" id="tipobiblioteca" value="<?php echo $type ?>">
            <input type="text" id="buscarbiblioteca" class="form-control" placeholder="<?php echo ucfirst(JrTexto::_('search')); ?>...">
            <span class="btn input-group-addon btnsearch"><i class="fa fa-search"></i></span>
          </div>
      </div>
    </div>   
  </div>

  <div id="listado" class="row addfile"></div>
  <div id="pag-listado" class="row text-center"></div>


<?php /*
  <div class="row addfile">
  <?php 
  if(!empty($this->datos)) #echo "<pre>"; print_r($this->datos); echo "</pre>";
    foreach ($this->datos as $i => $data){
      $nombre = explode('\\', $data);
      $nombre = $nombre[count($nombre)-1];
      ?>
    <div class="col-md-3 col-sm-4 col-xs-6 text-center item-biblioteca" data-filelink="<?php echo $data;?>" data-name="<?php echo $nombre;?>" title="<?php echo ucfirst(JrTexto::_('select')); ?>">
      <?php if($this->type=='image'){ ?>      
        <span class="file" data-tipo="image">
          <img data-mce-object="image" class="data-mce-image img-thumbnail img-responsive btnselected" src="<?php echo $this->documento->getUrlStatic() .'/media/'. $data;?>" alt="<?php echo $data; ?>" style="max-height: 70px;">
        </span> 
      <?php } if($this->type=='video'){ ?>
        <span class="file" data-tipo="video">
         <i class="iconfilebibli fa fa-file-video-o btnselected"></i>         
        </span>       
      <?php } if($this->type=='audio'){ ?>    
        <span class="file" data-tipo="audio">
          <i class="iconfilebibli fa fa-file-audio-o btnselected"></i>          
        </span>
        
      <?php } if($this->type=='pdf'){ ?>    
        <span class="file" data-tipo="pdf">
          <i class="iconfilebibli fa fa-file-pdf-o btnselected"></i>
        </span>
        
      <?php } if($this->type=='ppt'){ ?>    
        <span class="file" data-tipo="ppt">
          <i class="iconfilebibli fa fa-file-powerpoint-o btnselected"></i>
        </span>
        
      <?php } ?>
        <div class="text-center "><?php echo $nombre;?></div>
        <div>
          <span class="btn btn-xs btn istooltip btnpreviewbiblioteca" title="<?php echo JrTexto::_('Preview'); ?>"><i class="fa fa-eye" ></i></span>
          <span class="btn btn-xs istooltip btnselected" title="<?php echo JrTexto::_('Select'); ?>"><i class="fa fa-hand-o-down" ></i></span>
          <span class="btn btn-xs btnremove istooltip" title="<?php echo JrTexto::_('Remove'); ?>" data-id="<?php echo $data;?>" ><i class="fa fa-trash-o" ></i></span>
        </div>
     </div>
     <?php } ?>
  </div>
*/ ?>

</div>
<div id="modalbibliotecapreview" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close btnclosemodalbibliotecapreview" >&times;</button>
        <h4 class="modal-title" id="headermodalbiblioteca"></h4>
      </div>
      <div class="modal-body" >
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 text-center" id="contentmodalbiblioteca">
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btnclosemodalbibliotecapreview" ><?php echo JrTexto::_('Close'); ?></button>
      </div>
    </div>
  </div>
</div>
<div id="divprecargafile" style="display: none; position: relative; background: #fff;">
  <div style="position: absolute; bottom:0px; width:100%">
  <div class="text-center"><img width="30px" src="<?php echo $this->documento->getUrlStatic() ?>/sysplantillas/default.gif"></div>
  <div class="progress" style="margin-bottom: 0px;">  
    <div class="progress-bar progress-bar-striped progress-bar-animated focus active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:70%">
      <?php echo JrTexto::_('loading').'...'; ?><span>70%</span>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
var $paginacion= $('#pag-listado') ;
var $contenedor= $('#listado') ;
var elemsXpag = 8;
var template = function(data) {
    var html = '';

    $.each(data, function(i, v) {
      var tipo_link = '/'+v.tipo+'/'+v.link;
      var srcLink = _sysUrlStatic_+'/media'+tipo_link;

      html+='<div class="col-md-3 col-sm-4 col-xs-6 text-center item-biblioteca" data-filelink="'+tipo_link+'" data-name="'+v.nombre+'" title="'+v.nombre+'">';
      
      html+='<span class="file" data-tipo="'+v.tipo+'">';
      switch(v.tipo){
        case "image" : html+='<img data-mce-object="image" class="data-mce-image img-thumbnail img-responsive btnselected" src="'+srcLink+'" alt="'+v.nombre+'" style="max-height: 70px;">'; break;
        case "video" : html+='<i class="iconfilebibli fa fa-file-video-o btnselected"></i>'; break;
        case "audio" : html+='<i class="iconfilebibli fa fa-file-audio-o btnselected"></i>'; break;
        case "pdf" : html+='<i class="iconfilebibli fa fa-file-pdf-o btnselected"></i>'; break;
        case "ppt" : html+='<i class="iconfilebibli fa fa-file-powerpoint-o btnselected"></i>'; break;
        default : html+='<i class="iconfilebibli fa fa-file-o btnselected"></i>'; break;
      }
      html+='</span>';

      html+='<div class="text-center ">'+v.nombre+'</div>';
      html+='<div>';
      html+='<span class="btn btn-xs btn istooltip btnpreviewbiblioteca" title="<?php echo JrTexto::_('Preview'); ?>"><i class="fa fa-eye" ></i></span>';
      html+='<span class="btn btn-xs istooltip btnselected" title="<?php echo JrTexto::_('Select'); ?>"><i class="fa fa-hand-o-down" ></i></span>';
      html+='<span class="btn btn-xs btnremove istooltip" title="<?php echo JrTexto::_('Remove'); ?>" data-id="'+v.idbiblioteca+'" ><i class="fa fa-trash-o" ></i></span>';
      html+='</div>';
      html+='</div>';

    });

    return html;
};
var optPag = {
  dataSource: function(done) {
    $.ajax({
      url: _sysUrlBase_+'/biblioteca/listadojson',
      type: 'GET',
      dataType: 'json',
      data: {
        type : $('#tipobiblioteca').val(),
        texto : $('#buscarbiblioteca').val(),
      },
      beforeSend: function(){
        $contenedor.html('<h3 class="text-center color-blue"><i class="fa fa-cog fa-spin fa-3x"></i><br><?php echo JrTexto::_("Loading"); ?></h3>');
      }
    }).done(function(resp) {
      if(resp.code=="ok" && resp.data.length>0){
        done(resp.data);
      }else{
        $contenedor.html('<h3 class="text-center"><i class="fa fa-minus-circle fa-3x"></i><br><?php echo JrTexto::_("There is no data to display"); ?></h3>');
      }
    }).fail(function(err) {
      console.log("error", err);
    });
  },
  className: 'paginationjs-theme-blue paginationjs-big',
  pageSize: elemsXpag,
  showGoInput: true,
  showGoButton: true,
  callback: function(data, pagination) {
    var html = template(data);
    $contenedor.html(html);
  }
};

var refreshPagination = function () {
  if( $paginacion.find('.paginationjs').length>0 ){ $paginacion.pagination('destroy'); }
  $paginacion.pagination(optPag);
};

$(document).ready(function(){

  refreshPagination();

  $('.istooltip').tooltip();

  $('.btnclosemodalbibliotecapreview').click(function(e){
    e.preventDefault();    
    $('#modalbibliotecapreview').modal('hide');
    e.stopPropagation();
  });

  $('.addfile').on('click','.btnpreviewbiblioteca',function(){
    var info=$(this).parent().parent();
    var link=$(info).data('filelink');
    var name=$(info).data('name');
    var tipo=$(info).children('.file').data('tipo');
    var rutamedia=_sysUrlStatic_+'/media';
    $('#headermodalbiblioteca').html(tipo+': '+name);
    var html_="";
    if(tipo=='image'){
      html_="<img src='"+rutamedia+link+"' class='img-thumbnail img-responsive' >";
    }else if(tipo=='audio'){
      html_='<audio src="'+rutamedia+link+'" class="embed-responsive-item" controls></audio>';
    }else if(tipo=='video'){
      html_="<div class='embed-responsive embed-responsive-16by9'>"+
      '<video src="'+rutamedia+link+'" class="embed-responsive-item" controls></video>'+
      '</div>';
    } else if(tipo=='pdf'){
      html_="<div class='embed-responsive embed-responsive-4by3'>"+
      '<iframe src="'+rutamedia+link+'" frameborder="0" class="pdf-viewer"></iframe>'+
      '</div>';
    } else if(tipo=='ppt'){
      html_="<div class='embed-responsive embed-responsive-4by3'>"+
      '<iframe src="http://docs.google.com/gview?url='+rutamedia+link+'&embedded=true" class="ppt-viewer" frameborder="0"></iframe>'+
      '</div>';
    }
    $('#contentmodalbiblioteca').html(html_);
    $('#modalbibliotecapreview').modal('show');
  });

  var subirmedia=function(tipo,file){
    var formData = new FormData();
    formData.append("tipo",tipo);
    formData.append("filearchivo",file[0].files[0]);
    dataurl=file.attr('data-url');
    $.ajax({
      url: '<?php echo $this->documento->getUrlBase(); ?>/biblioteca/cargarmedia',
      type: "POST",
      data:  formData,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      processData:false,
      xhr:function(){
        var xhr = new window.XMLHttpRequest();
        /*Upload progress*/
        xhr.upload.addEventListener("progress", function(evt){

          if (evt.lengthComputable) {
            var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
            $('#divprecargafile .progress-bar').width(percentComplete+'%');
            $('#divprecargafile .progress-bar span').text(percentComplete+'%');
          }
        }, false);
        /*Download progress*/
        xhr.addEventListener("progress", function(evt){
          if (evt.lengthComputable) {
            var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
            /*Do something with download progress*/
            console.log(percentComplete);
          }
        }, false);
        return xhr;
      },
      beforeSend: function(XMLHttpRequest){
        div=$('#divprecargafile');
        $('#divprecargafile').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
        $('#divprecargafile .progress-bar').width('0%');
        $('#divprecargafile .progress-bar span').text('0%'); 
        $('#divprecargafile').show('fast'); 
      },      
      success: function(data){
        if(data.code==='ok'){
          $('#divprecargafile .progress-bar').width('100%');
          $('#divprecargafile .progress-bar').html('Complete <span>100%</span>');
          $('#divprecargafile').addClass('progress-bar-success').removeClass('progress-bar-animated');
          html_='';
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
          /*html_+='<div class="col-md-3 col-sm-4 col-xs-6 text-center item-biblioteca"'+
          'data-filelink="\\'+data.namelink+'" data-name="'+data.nombre+'" title="'+data.nombre+'">';
          if(tipo=='image'){
            html_+='<span class="file"  data-tipo="image">'+
            '<img data-mce-object="image" class="data-mce-image img-thumbnail img-responsive btnselected" src="'+_sysUrlStatic_+'/media\\'+data.namelink+'" alt="'+data.nombre+'" style="max-height: 80px;">'+
            '</span>';
          }else if(tipo=='audio'){
            html_+='<span class="file" data-tipo="audio">'+
            '<i class="iconfilebibli fa fa-file-audio-o btnselected"></i>'+
            '</span>';              
          }else if(tipo=='video'){ 
            html_+='<span class="file"  data-tipo="video">'+
            '<i class="iconfilebibli fa fa-file-video-o btnselected"></i>'+
            '</span>';                 
          }else if(tipo=='pdf'){ 
            html_+='<span class="file"  data-tipo="pdf">'+
            '<i class="iconfilebibli fa fa-file-pdf-o btnselected"></i>'+
            '</span>';                         
          }else if(tipo=='ppt'){ 
            html_+='<span class="file"  data-tipo="ppt">'+
            '<i class="iconfilebibli fa fa-file-powerpoint-o btnselected"></i>'+
            '</span>';                         
          }
          html_+='<div class="text-center">'+data.nombre+'</div><div>'+
          '<span class="btn btn-xs tooltip btnpreviewbiblioteca" title="<?php echo JrTexto::_('Preview'); ?>">'+
          '<i class="fa fa-eye" ></i></span>'+
          '<span class="btn btn-xs btnselected tooltip" title="<?php echo JrTexto::_('Selected'); ?>">'+
          '<i class="fa fa-hand-o-down" ></i></span>'+
          '<span class="btn btn-xs btnremove tooltip" title="<?php echo JrTexto::_('Remove'); ?>" data-id="'+data.namelink+'" >'+
          '<i class="fa fa-trash-o" ></i></span></div>'+
          '</div></div>';

          $('.addfile').prepend(html_);*/
          refreshPagination();

          $('#divprecargafile').hide('fast');
        }else{
          $('#divprecargafile').addClass('progress-bar-warning progress-bar-animated');
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          return false;
        }
      },
      error: function(e){
        $('#divprecargafile').addClass('progress-bar-danger progress-bar-animated');
        $('#divprecargafile .progress-bar').html('Error <span>-1%</span>'); 
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
        return false;
      },
      complete: function(xhr){
        $('#divprecargafile .progress-bar').html('Complete <span>100%</span>'); 
        $('#divprecargafile').addClass('progress-bar-success progress-bar-animated').hide('slow');
      }
    });
  };

  $('.btnsearch').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    
    refreshPagination();
    return false;

    /*var texto=$('#buscarbiblioteca').val();
    var tipo=$('#tipobiblioteca').val();
    var formData = new FormData();
    formData.append("type",tipo);
    formData.append("texto",texto);
    $.ajax({
      url: '<?php echo $this->documento->getUrlBase(); ?>/biblioteca/listadojson',
      type: "POST",
      data:  formData,
      contentType: false,
      dataType :'json',
      cache: false,
      processData:false,
      success: function(data)
      {
        if(data.code==='ok'){
          if(data.data!=undefined){
            obj=data.data;
            var html_='';
            $.each(obj,function(index,data){
              tipo=data.tipo;
              html_+='<div class="col-md-3 col-sm-4 col-xs-6 text-center item-biblioteca"'+
              'data-filelink="'+data.link+'" data-name="'+data.nombre+'">';
              var rutamedia=_sysUrlStatic_+'/media'+tipo+'/'+data.link;
              console.log(rutamedia);
              if(tipo=='image'){
                html_+='<span class="file"  data-tipo="image">'+
                '<img data-mce-object="image" class="data-mce-image img-thumbnail img-responsive btnselected" src="'+rutamedia+'" alt="'+data.nombre+'" style="max-height: 80px;">'+
                '</span>';

              }else if(tipo=='audio'){
                html_+='<span class="file" data-tipo="audio">'+
                '<i class="iconfilebibli fa fa-file-audio-o btnselected"></i>'+
                '</span>'; 
              }else if(tipo=='video'){ 
                html_+='<span class="file"  data-tipo="video">'+
                '<i class="iconfilebibli fa fa-file-video-o btnselected"></i>'+
                '</span>';                         
              }else if(tipo=='pdf'){ 
                html_+='<span class="file"  data-tipo="pdf">'+
                '<i class="iconfilebibli fa fa-file-pdf-o btnselected"></i>'+
                '</span>';                         
              }else if(tipo=='ppt'){ 
                html_+='<span class="file"  data-tipo="ppt">'+
                '<i class="iconfilebibli fa fa-file-powerpoint-o btnselected"></i>'+
                '</span>';                         
              }
              html_+='<div class="text-center">'+data.nombre+'</div><div>'+
              '<span class="btn btn-xs tooltip btnpreviewbiblioteca" title="<?php echo JrTexto::_('Preview'); ?>">'+
              '<i class="fa fa-eye" ></i></span>'+
              '<span class="btn btn-xs btnselected tooltip" title="<?php echo JrTexto::_('Selected'); ?>">'+
              '<i class="fa fa-hand-o-down" ></i></span>'+
              '<span class="btn btn-xs btnremove tooltip" title="<?php echo JrTexto::_('Remove'); ?>" data-id="'+data.idbiblioteca+'" >'+
              '<i class="fa fa-trash-o" ></i></span></div>'+
              '</div></div>';

            });
            $('.addfile').html(html_); 
          }
          return false;
        }else{
          mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
          return false;
        }
      },
      error: function(e) 
      {
        mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
        return false;
      }
    });*/
  });

  $('#buscarbiblioteca').keypress(function(e) {
    if(e.keyCode == 13){ $('.btnsearch').trigger('click'); }
  });

  $('.btnsavefile').change(function(e){
    var file=$(this);       
    if(file.val()=='') return false;
    var tipo= file.attr('data-tipo');
    subirmedia(tipo,file);
    e.preventDefault();
    e.stopPropagation();
  });

  $('.addfile').on('click','.btnremove',function(){
    var obj=$(this);
    var id=$(this).attr('data-id');
    var $file=$(this).closest('.item-biblioteca');
    var link=$file.data('filelink');
    var tipo=$file.data('tipo');
    $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){
        var res = xajax__('', 'biblioteca', 'eliminar', id,link,tipo);
        /*if(res){*/
          //$file.remove();
          refreshPagination();
        /*}*/
        }
      });
  });

  $('div.addfile').on('click','.btnselected',function(){
    var ruta=$(this).parent().parent();
    var file=$(ruta).children('span.file');
    var link=ruta.data('filelink');
    var nombre=ruta.data('name');
    var tiposearch='<?php echo @$_GET["type"];?>';
    var fcall='<?php echo !empty($_GET["fcall"])?$_GET["fcall"]:-1;?>';
    var tipo=file.data('tipo');
    var rutamedia=_sysUrlStatic_+'/media'+link; /* /'+tipo+'/'; */
    var robj='<?php echo @$_GET['robj'];?>';
    var pnlidgui='<?php echo @$_GET['pnlidgui'];?>';
    /*sin tymice e indica que objeto reemplazara*/
    if(robj==='afile'){
      var adonde='<?php echo @$_GET['donde'];?>';
      if(tipo=='video'){       
        $(tipo+adonde).attr('src',rutamedia);
        $(tipo+adonde).siblings('img').hide();
        $(tipo+adonde).show('fast');
      }else if(tipo=='audio'){
        $(adonde).attr("data-audio",link).show('fast');        
        $(adonde).attr("src",rutamedia).show('fast');        
        $('#audio'+adonde.substr(1)).attr("src",rutamedia);
        $(adonde).attr('data-nombre-file',nombre);
        $(adonde).siblings('img').hide();
      }else if(tipo=='image'){

        $('img'+adonde).attr('src',rutamedia);
        $('img'+adonde).attr('data-nombre-file',nombre);
        $('img'+adonde).siblings('img').hide();
        $('img'+adonde).show('fast');
      }else if(tipo=='pdf'){
        $(adonde).attr('src',rutamedia);
        $(adonde).attr('data-nombre-file',nombre);
        $(adonde).siblings('img').hide(); 
        $(adonde).siblings('div.mask').hide(); /*para 'PDFs' el sibling es un 'div.mask'*/
        $(adonde).show('fast');
      }else if(tipo=='ppt'){
        $(adonde).attr('src',rutamedia);
        $(adonde).siblings('img').hide(); 
        $(adonde).siblings('div.mask').hide(); /*para 'PPTs' el sibling es un 'div.mask'*/
        $(adonde).show('fast');
      }
      if(fcall!='-1'&&fcall!='undefined'){
        <?php echo !empty($_GET["fcall"])?($_GET["fcall"].'();'):''; ?>
      }
      $(this).closest('.modal').find('.cerrarmodal').trigger('click');
    }else if(robj=='returndata'){
        if(fcall!='-1'&&fcall!='undefined'){
          var datatmp={nombre:nombre,ruta:rutamedia,tipo:tipo,link:link,pnlidgui:pnlidgui};
          <?php echo !empty($_GET["fcall"])?($_GET["fcall"].'(datatmp);'):''; ?>
        } 
       $(this).closest('.modal').find('.cerrarmodal').trigger('click');
    }else{   

      var acteditor=tinyMCE.activeEditor;
      var _obj=robj||-1;

      if(_obj!=undefined&&_obj!=-1){
        obj_='_mce_modalobjectenedicion_';
        tipe=_obj;         
        var acteditor=$(acteditor.getBody());
        var actobj=$(acteditor).find('.'+obj_);

        if(tipo=='audio'||tipo=='video'){  
          if(tipe=='image'||tipe=='input'){          
            $(actobj).attr('data-audio',ruta.data('filelink'));
          }else if(tipe=='audio'||tipe=='video'){
            $(actobj).attr('data-src',rutamedia);
            $(actobj).attr('alt',nombre);
            $(actobj).attr('data-mce-object',tipo);
            if(tipe!=tipo)
              $(actobj).removeClass('mce-object-'+tipe).addClass('mce-object-'+tipo);
          }
          $(actobj).removeClass(obj_);          
        }else if(tipo=='image'){
          if(tipe=='image'){
            $(actobj).attr('src',rutamedia);
            $(actobj).attr('alt',nombre);
            $(actobj).removeClass(obj_);
          }else{
            img=file.clone();
            img.children('img').removeAttr('style');
            html=img.html();
            acteditor.selection.setContent(html);
            acteditor.nodeChanged();
          }
        }                 
        $(this).closest('.modal.biblioteca-'+tipo).find('.cerrarmodal').trigger('click');
      }else if(ruta!=undefined){               
        if(tipo==='image'){
          img=file.clone();
          img.children('img').removeAttr('style');
          html=img.html();
        }else{
          controls='';
          html='';
          if(tipo=='audio'||tipo=='video') 
            html='<img '+controls+' data-mce-object="'+tipo+'" controls="true" class="mce-object-'+tipo+'" data-src="'+rutamedia+'" alt="'+nombre+'">';
        }
        acteditor.selection.setContent(html);
        acteditor.nodeChanged();
        $(this).closest('.modal.biblioteca-'+tipo).find('.cerrarmodal').trigger('click');
      }
    }
  });

});
</script>