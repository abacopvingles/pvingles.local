<div class="row">
    <?php if($this->esSuperAdmin) { ?>
    <div class="col-xs-12 col-sm-4 form-group select-ctrl-wrapper select-azul" id="filtroEmpresa">
        <select name="opcEmpresa" id="opcEmpresa" class="form-control select-ctrl">
            <option value="">- <?php echo ucfirst(JrTexto::_("Select project")); ?> -</option>
            <?php foreach ($this->empresas as $e) { ?>
            <option value="<?php echo $e['slug'] ?>"><?php echo $e['nombre'] ?></option>
            <?php } ?>
        </select>
    </div>
    <?php } ?>

    <div class="col-xs-12 col-sm-4 form-group select-ctrl-wrapper select-azul">
        <select name="opcAlumno" id="opcAlumno" class="form-control select-ctrl">
            <option value="">- <?php echo ucfirst(JrTexto::_('Select student')); ?> -</option>
        </select>
    </div>
    <div class="col-xs-12 col-sm-4 form-group select-ctrl-wrapper select-azul">
        <select name="opcExamen" id="opcExamen" class="form-control select-ctrl">
            <option value="">- <?php echo ucfirst(JrTexto::_('Select exam')); ?> -</option>
        </select>
    </div>

    <div class="col-xs-12 text-center">
        <button class="btn btn-info btn-filtrar"><?php echo JrTexto::_("Filter") ?></button>
    </div>

    <div class="col-md-12">
        <iframe src="" id="ifrReporte" style="border:0; height: calc(100vh - 90px); width: 100%;"></iframe>
    </div>
</div>

<section id="msjes_idioma" class="hidden" style="display: none !important; ">
    <input type="hidden" id="attention" value='<?php echo JrTexto::_('Attention') ?>'>
    <input type="hidden" id="select_student" value='<?php echo JrTexto::_('Select student') ?>'>
    <input type="hidden" id="select_exam" value='<?php echo JrTexto::_('Select exam') ?>'>
</section>