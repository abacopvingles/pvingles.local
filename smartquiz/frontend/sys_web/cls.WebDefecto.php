<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{	
	private $oNegNiveles;
	private $oNegResources;
    protected $oNegMetodologia;

	public function __construct()
	{
		parent::__construct();
		/*$this->oNegNiveles = new NegNiveles;*/
		$this->oNegResources = new NegResources;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			return $aplicacion->redir('examenes');

			$this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
			$this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
			$this->documento->script('circle', '/libs/graficos/progressbar/');
			
			$this->usuarioAct = NegSesion::getUsuario();
			//$this->niveles=$this->oNegNiveles->buscarNiveles(array('tipo'=>'N'));			
			$this->documento->setTitulo(JrTexto::_('Inicio'), true);
			$rol=$this->usuarioAct["rol"];


			/*header('Location: ' . $this->documento->getUrlBase().'/usuario');
			exit(0);*/
			
			/*if(strtolower($rol)=="alumno"){
				$this->documento->script('inicio', '/js/alumno/');
				$this->esquema = 'alumno/inicio';
				$this->documento->plantilla = 'alumno/general';
				header('Location: ' . $this->documento->getUrlBase().'/cursos');
			}else{

	            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	            $usuarioAct = NegSesion::getUsuario();
	           	$this->iddoc=$usuarioAct["dni"];
	           	$this->recursos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'tipo'=>'P','idpersonal'=>$this->iddoc));

	           	$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
				$this->esquema = 'docente/inicio';
				$this->documento->plantilla = 'inicio';
			}*/
			$this->esquema = 'inicio_kfactory';
			$this->documento->plantilla = 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}