<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
class WebExamen_alumno extends JrWeb
{
	private $oNegExamen_alumno;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegExamen_alumno = new NegExamen_alumno;
	}

	public function defecto(){
		global $aplicacion;
		return $aplicacion->redir(JrAplicacion::getJrUrl(array('examenes', 'publicados')), false);
		#return $this->listado();
	}
	/*
	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegExamen_alumno->idexaalumno = @$_GET['id'];
			$this->datos = $this->oNegExamen_alumno->dataExamen_alumno;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Examen_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'examen_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	*/
	public function verjson()
    {
        try{
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            $this->documento->plantilla = 'returnjson';
            $filtros=array();
            //$filtros["titulo"] = !empty($_POST["txtBuscar"])?$_POST["txtBuscar"]:'';
            $filtros["idexaalumno"] = !empty($_REQUEST["idexaalumno"])?$_REQUEST["idexaalumno"]:0;
            $filtros["idexamen"] = !empty($_REQUEST["idexamen"])?$_REQUEST["idexamen"]:0;
            $filtros["idalumno"] = !empty($_REQUEST["idalumno"])?$_REQUEST["idalumno"]:0;
            $examenAlumno = $this->oNegExamen_alumno->buscar($filtros);
            $data = array('code'=>'ok','data'=>$examenAlumno);
            echo json_encode($data); exit(0);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }


    public function guardar()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario();
			if(!empty($idexamen)&&!empty($idexaalumno)){			
				$res=$this->oNegExamen_alumno->idexaalumno = $idexaalumno;
				$accion="editar";
			}
			$preguntas=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$preguntas));
			$resultado=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$resultado));
			$this->oNegExamen_alumno->__set('idexamen',@$idexamen);
			$this->oNegExamen_alumno->__set('idalumno',@$usuarioAct["dni"]);
			$this->oNegExamen_alumno->__set('preguntas',@$preguntas); //html
			$this->oNegExamen_alumno->__set('resultado',@$resultado); //html
			$this->oNegExamen_alumno->__set('puntajehabilidad',@$puntajehabilidad);
			$this->oNegExamen_alumno->__set('puntaje',@$puntaje);
			$this->oNegExamen_alumno->__set('resultadojson',@$resultadojson);
			$this->oNegExamen_alumno->__set('tiempoduracion',@$tiempoduracion);
			$this->oNegExamen_alumno->__set('intento',@$intento);
			$this->oNegExamen_alumno->__set('intento',@$intento);
			$this->oNegExamen_alumno->__set('preguntasoffline',@$preguntasoffline);
		    if(@$accion=="editar"){
				$res=$this->oNegExamen_alumno->editar();
			}else{
				$res=$this->oNegExamen_alumno->agregar();
		    }
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 	
	}

	// ========================== Funciones ajax ========================== //
	public function jxBuscar()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			
			if(isset($_REQUEST["idpregunta"])&&@$_REQUEST["idpregunta"]!='')$filtros["idpregunta"]=$_REQUEST["idpregunta"];
			if(isset($_REQUEST["idexamen"])&&@$_REQUEST["idexamen"]!='')$filtros["idexamen"]=$_REQUEST["idexamen"];
			if(isset($_REQUEST["pregunta"])&&@$_REQUEST["pregunta"]!='')$filtros["pregunta"]=$_REQUEST["pregunta"];
			if(isset($_REQUEST["descripcion"])&&@$_REQUEST["descripcion"]!='')$filtros["descripcion"]=$_REQUEST["descripcion"];
			if(isset($_REQUEST["ejercicio"])&&@$_REQUEST["ejercicio"]!='')$filtros["ejercicio"]=$_REQUEST["ejercicio"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["tiempo"])&&@$_REQUEST["tiempo"]!='')$filtros["tiempo"]=$_REQUEST["tiempo"];
			if(isset($_REQUEST["puntaje"])&&@$_REQUEST["puntaje"]!='')$filtros["puntaje"]=$_REQUEST["puntaje"];
			if(isset($_REQUEST["template"])&&@$_REQUEST["template"]!='')$filtros["template"]=$_REQUEST["template"];
			if(isset($_REQUEST["habilidad"])&&@$_REQUEST["habilidad"]!='')$filtros["habilidad"]=$_REQUEST["habilidad"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["idcontenedor"])&&@$_REQUEST["idcontenedor"]!='')$filtros["idcontenedor"]=$_REQUEST["idcontenedor"];
			if(isset($_REQUEST["dificultad"])&&@$_REQUEST["dificultad"]!='')$filtros["dificultad"]=$_REQUEST["dificultad"];
			if(isset($_REQUEST["idpregunta_origen"])&&@$_REQUEST["idpregunta_origen"]!='')$filtros["idpregunta_origen"]=$_REQUEST["idpregunta_origen"];

			$this->oNegExamen_alumno->setLimite(0,99999);
			$data=$this->oNegExamen_alumno->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$data));
		 	exit(0);
		} catch (Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e->getMessage()));
            exit(0);
		}
	}

	public function getResultadoXAlumno()
	{
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;	
			$idExamen = @$_REQUEST["idexamen"];

            $usuarioAct = NegSesion::getUsuario();
			$identifAlum = $usuarioAct["identificador"];
			$idProyecto = $usuarioAct["proyecto"]["idproyecto"];

			$resultadoXAlumno = $this->oNegExamen_alumno->getResultadoXAlumno(array(
				"idexamen"=>$idExamen,
				"idproyecto"=>$idProyecto,
				"estado"=>1,
				"identificador"=>$identifAlum
			));
			$resultadoXAlumno["otros"] = json_decode($usuarioAct["otrosParams"], true);
			
			echo json_encode(array('code'=>"ok",'msj'=>'Success','data'=>$resultadoXAlumno));
			exit(0);
		} catch (Exception $e) {	
			echo json_encode(array('code'=>"Error",'msj'=>$e->getMessage(),'data'=>[]));
			exit(0);
		}
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveExamen_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdexaalumno'])) {
					$this->oNegExamen_alumno->idexaalumno = $frm['pkIdexaalumno'];
				}
				
				    $this->oNegExamen_alumno->__set('idexamen',@$frm["txtIdexamen"]);
					$this->oNegExamen_alumno->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegExamen_alumno->__set('preguntas',@$frm["txtPreguntas"]);
					$this->oNegExamen_alumno->__set('resultado',@$frm["txtResultado"]);
					$this->oNegExamen_alumno->__set('puntajehabilidad',@$frm["txtPuntajehabilidad"]);
					$this->oNegExamen_alumno->__set('puntaje',@$frm["txtPuntaje"]);
					$this->oNegExamen_alumno->__set('resultadojson',@$frm["txtResultadojson"]);
					$this->oNegExamen_alumno->__set('tiempoduracion',@$frm["txtTiempoduracion"]);
					$this->oNegExamen_alumno->__set('fecha',@$frm["txtFecha"]);
					$this->oNegExamen_alumno->__set('intento',@$frm["txtIntento"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegExamen_alumno->agregar();
					}else{
									    $res=$this->oNegExamen_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegExamen_alumno->idexaalumno);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	  
}