<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $adonde=empty($_GET["adonde"])?exit(0):$_GET["adonde"];
    $idgui = uniqid();
?>
<div class="form-group">
    <label for="texto"><?php echo JrTexto::_('Add dialog'); ?></label>
    <textarea class="form-control" id="txt<?php echo $idgui ?>" name="txt<?php echo $idgui ?>" style="height:200px;">
          <h1><?php echo JrTexto::_('Dialogo of personajes'); ?></h1><br>
          <b><?php echo JrTexto::_('Abel'); ?></b>  :<?php echo JrTexto::_('Que tal esta plantilla'); ?><br>
          <b><?php echo JrTexto::_('Evelio'); ?></b> :<?php echo JrTexto::_('mmmmmmmm'); ?><br>          
          
    </textarea>
   
</div>
<button class="btn btn-primary saveguardar<?php echo $idgui; ?>"><i class="fa fa-save"></i></button>

<script type="text/javascript">
$(document).ready(function(){
    $('.saveguardar<?php echo $idgui; ?>').click(function(){
      tinyMCE.triggerSave();
      $('<?php echo $adonde;?>').html($('#txt<?php echo $idgui; ?>').val());
      $('#addinfotxt').modal('hide');
    });
  tinymce.init({
    menubar:false,
    relative_urls : false,
    //statusbar: false,
    cleanup : false,
    verify_html : false,
    selector: '#txt<?php echo $idgui ?>',
    height: 400,
    toolbar: 'undo redo | styleselect | table | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent '
  });
});
</script>