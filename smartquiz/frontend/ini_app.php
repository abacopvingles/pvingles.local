<?php
/**
 * @autor       Abel Chingo Tello
 * @fecha       08/09/2016
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */

date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
define('_BOOT_', true);
define('SD', DIRECTORY_SEPARATOR);
define('_mitema_','tema1');
define('_sitio_','frontend');
define('RUTA_BASE',			dirname(dirname(__FILE__)) . SD);
define('RUTA_LIBS', 		RUTA_BASE . 'sys_lib' . SD);
define('RUTA_INC',			RUTA_BASE . 'sys_inc' . SD);
define('RUTA_PLANTILLAS', 	RUTA_BASE . _sitio_. SD . 'plantillas' . SD);
define('RUTA_SITIO',		RUTA_BASE . _sitio_. SD);
define('IS_LOGIN', true);
define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
define('URL_BASE',_http_.$_SERVER["HTTP_HOST"].'/pvingles.local/smartquiz');
define('sysVERSION', 'xsfri2.0');
require_once(RUTA_LIBS . 'cls.JrCargador.php');
require_once(RUTA_LIBS . 'jrAdwen' . SD . 'jrFram.php');
require_once(RUTA_LIBS . 'jrAdwen'.SD.'documento'.SD.'cls.JrDocumento.'.'php');
try {
	JrCargador::clase('sys_inc::ConfigSitio', RUTA_BASE, 'sys_inc::');
	JrCargador::clase('sys_inc::Sitio', RUTA_BASE, 'sys_inc::');
	$aplicacion = Sitio::getInstancia();
} catch(Exception $e){
	
	exit('<h1>Imposible iniciar la aplicacion: '.$e->getMessage().'</h1>');
}