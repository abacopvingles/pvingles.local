<?php 
$uniqid = uniqid();
if(empty($this->usuarioAct['imagen'])){
    $fotoSrc = $this->documento->getUrlStatic().'/img/sistema/user_avatar.jpg';
}else{
    $fotoSrc = $this->raiz.'/fotos/'.$this->usuarioAct['idusuario'].'.JPG?'.$uniqid;
}

$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(_http_, '', $this->documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
/*$ruta_sin_UrlBase = @explode('/', $ruta_sin_UrlBase)[1];*/
?>
<style type="text/css">
    .text-border{
        text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff;
    }
</style>
<header>
    <div class="container-fluid">
        <div class="row">
            <!--div class="col-xs-4 col-sm-2" id="foto_usuario">
                <div class="nombre_completo text-center"><?php echo $this->usuarioAct['nombre_full'] ?></div>
                <img src="<?php echo $fotoSrc; ?>" class="img-responsive center-block" style="width: 120px; height: 120px;">
            </div-->
            <div class="col-xs-7 col-sm-6"  id="logo_left">
                <a href="<?php echo '#';//$this->documento->getUrlBase(); ?>" class="" id="logo">
                    <!--img src="<?php echo $this->documento->getUrlStatic() ?>/img/sistema/logo_edukt_blanco.png" class="hvr-wobble-skew img-responsive" alt="logo"-->
                    <span style="font-size: 3em; " class="text-border color-green2">smart</span>
                    <span style="font-size: 4em; " class="text-border color-blue">Quiz</span>
                </a>
            </div>
            <div class="col-xs-5 col-sm-6 text-right"  id="part_right">
                <?php foreach ($this->video as $v) {
                    if($v['link']==$ruta_sin_UrlBase && !empty($v['ruta_video'])){?>
                <button data-video="<?php echo $this->urlBaseVideo.$v['ruta_video'];?>" data-nombre="<?php echo str_replace(' ', '_', $v['nombre']);?>" class="btn btn-primary video_manual"><i class="fa fa-video-camera"></i></button>
                <?php } } ?>
            </div>
            <div class="navbar-header navbar-static-top"  id="nav_button"> 
                <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button"> 
                    <span class="sr-only">Toggle navigation</span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                    <span class="icon-bar"></span> 
                </button>
            </div>
            <nav class="collapse navbar-collapse" id="bs-navbar">
                <ul class="nav navbar-nav navbar-right menutop1">
                    <li>
                        <a class="hvr-buzz-out" href="#" id="getting-started">
                            <i class="fa fa-th-large"></i>
                        </a> 
                    </li>
                </ul> 
            </nav>
        </div>

    </div>
</header>
<script type="text/javascript">
$(document).ready(function() {
    $('header').on('click', '.btn.video_manual', function(e) {
        e.preventDefault();
        var nombre = $(this).data('nombre');
        var nombre_con_espacios = nombre.replace('_', ' ');
        var ruta_video = $(this).data('video');
        var idModal = 'mdl-VideoTutorial_'+nombre;
        var $mdlVideo = $('#modalclone').clone();
        $mdlVideo.attr('id', idModal).addClass('modal_tutorial');
        $mdlVideo.find('#modaltitle').text('Video Tutorial - '+nombre_con_espacios);
        $mdlVideo.find('#modalfooter').css('text-align', 'center');
        $mdlVideo.find('#modalfooter .btn.cerrarmodal').addClass('btn-danger').removeClass('btn-default');
        $mdlVideo.find('#modalcontent').html(
            $('<video controls>')
                .attr('src', ruta_video)
                .css('width', '100%')
        );
        $('body').append($mdlVideo);
        $('#'+idModal).modal();
    });

    $('body').on('hidden.bs.modal', '.modal.modal_tutorial', function (e) {
        $(this).remove();
    });
});
</script>