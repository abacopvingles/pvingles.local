<?php
/**
 * @autor		: Abel Chingo Tello . ACHT
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegConfiguracion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRol', RUTA_BASE, 'sys_negocio');
class Top extends JrModulo
{
	protected $oNegConfig;
	protected $oNegRol;
	public function __construct()
	{
		parent::__construct();		
		$this->documento = JrInstancia::getDocumento();
		$this->oNegConfig = NegConfiguracion::getInstancia();
		$this->oNegRol =new NegRol();
		$this->modulo = 'top';
	}
	
	public function mostrar($param=null)
	{
		try {
			$this->usuarioAct = NegSesion::getUsuario();
			$this->esquema = 'top';
			$this->raiz = _http_.$_SERVER["SERVER_NAME"];
			$this->urlBaseVideo = $this->raiz.'/videomanual/';
			$this->video = array(
				array('nombre' => ucfirst(JrTexto::_('area')),'link'=>'/cargos/?nivel=1', 'ruta_video'=>'subarea.mp4'),
				array('nombre' => ' Sub-'.ucfirst(JrTexto::_('area')), 'link'=>'/cargos/?nivel=2', 'ruta_video'=>'subarea.mp4'),
				array('nombre' => ucfirst(JrTexto::_('Obligatory courses')), 'link'=>'/curso_obligado', 'ruta_video'=>'cursos-obligatorios.mp4'),
				array('nombre' => ucfirst(JrTexto::_('administrative')), 'link'=>'/usuario', 'ruta_video'=>'registro-admin.mp4'),
				array('nombre' => ucfirst(JrTexto::_('student')), 'link'=>'/alumno', 'ruta_video'=>'registrar-alumno.mp4'),
				array('nombre' => ucfirst(JrTexto::_('Reports')), 'link'=>'/reportes', 'ruta_video'=>'report-admin_1.mp4'),
			);

			return $this->getEsquema();
		} catch(Exception $e) {
			return $e->getMessage();
		}
	}
}