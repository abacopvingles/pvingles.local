<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		29-09-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatRol_personal extends DatBase
{
	public function __construct()
	{
		try {
			#parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT COUNT(*) FROM rol_personal";
			
			$cond = array();		
			
			if(isset($filtros["iddetalle"])) {
					$cond[] = "iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			parent::conectar();
			$sql = "SELECT RP.*, R.rol FROM rol_personal RP "
					. " INNER JOIN rol R ON R.idrol=RP.idrol ";			
			
			$cond = array();		
					
			
			if(isset($filtros["iddetalle"])) {
					$cond[] = "RP.iddetalle = " . $this->oBD->escapar($filtros["iddetalle"]);
			}
			if(isset($filtros["idrol"])) {
					$cond[] = "RP.idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "RP.idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY RP.idrol ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}
	
	public function insertar($idrol,$idpersonal)
	{
		try {
			parent::conectar();
			
			$this->iniciarTransaccion('dat_rol_personal_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(iddetalle) FROM rol_personal");
			++$id;
			
			$estados = array('iddetalle' => $id
							
							,'idrol'=>$idrol
							,'idpersonal'=>$idpersonal							
							);
			
			$this->oBD->insert('rol_personal', $estados);			
			$this->terminarTransaccion('dat_rol_personal_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_rol_personal_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idrol,$idpersonal)
	{
		try {
			parent::conectar();
			$this->iniciarTransaccion('dat_rol_personal_update');
			$estados = array('idrol'=>$idrol
							,'idpersonal'=>$idpersonal								
							);
			
			$this->oBD->update('rol_personal ', $estados, array('iddetalle' => $id));
		    $this->terminarTransaccion('dat_rol_personal_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			parent::conectar();
			$sql = "SELECT  RP.*, R.rol FROM rol_personal RP "
					. " INNER JOIN rol R ON R.idrol=RP.idrol "
					. " WHERE iddetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			parent::conectar();
			return $this->oBD->delete('rol_personal', array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			parent::conectar();
			$this->oBD->update('rol_personal', array($propiedad => $valor), array('iddetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Rol_personal").": " . $e->getMessage());
		}
	}
   
		
}