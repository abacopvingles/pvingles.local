var altoDotContainer = anchoDotContainer = 40; /* .dot-container width and height = 40px */
var ANS_TAG = {};
var HELP = {};

var pausarTiempoImgTagged = function(){
    var $tmplActiva = getTmplActiva();
    var fichasTotal = $tmplActiva.find('.tpl_plantilla .dot');
    var fichasCorregidas = $tmplActiva.find('.tpl_plantilla .dot-tag.corregido');

    if( fichasTotal.length === fichasCorregidas.length ){
        $('#panel-tiempo .info-show').trigger('oncropausar');
        $tmplActiva.find('.plantilla').addClass('tiempo-pausado');
    }
};

var calcularPosicionPunto = function(e, IDGUI){
    var widthImg= $('.img-dots'+IDGUI)[0].getBoundingClientRect().width,
        heightImg= $('.img-dots'+IDGUI)[0].getBoundingClientRect().height,
        offset = $('#tmp_'+IDGUI+' .mask-dots.start-tagging').offset();        
    var posX = e.pageX - offset.left - (altoDotContainer/2),
        posY = e.pageY - offset.top  - (anchoDotContainer/2);
    var posicPorcentaje = {
        'x' : (posX*100)/widthImg ,
        'y' : (posY*100)/heightImg ,
    };
    return posicPorcentaje;
};

var crearPunto = function(e, idCloneFrom, idCloneTo, IDGUI){
    var now = Date.now();
    var posicion = calcularPosicionPunto(e, IDGUI);
    var newPunto = $(idCloneFrom).clone();
    newPunto.removeClass('hidden').attr('id', 'dot_'+now).removeAttr('data-id');
    newPunto.css({
        left: posicion.x+'%',
        top: posicion.y+'%',
    });
    var cant = $(idCloneTo).children().length;
    newPunto.find('.dot-tag').html(cant+1);
    $(idCloneTo).append(newPunto);
    return { 'cant':(cant+1), 'now': now};
};


var crearInput = function(nroPunto, now, placeholder, IDGUI){
    var htmlInput='<div class="input-group" id="edit_tag_'+now+'">'+
                    '<span class="input-group-addon">'+nroPunto+'</span>'+
                    '<input type="text" class="form-control" placeholder="'+placeholder+'">'+
                '</div>';
    $('#tmp_'+IDGUI+'   .tag-alts-edit').append(htmlInput);
};

var renameTags = function($containerDots, $containerInputs){
    var i = 0;
    $containerDots.find('.dot-container').each(function() {
        i++;
        var indexId = $(this).attr('id').split('_')[1];
        $(this).find('.dot-tag').text(i);
        $containerInputs.find('#edit_tag_'+indexId+' span.input-group-addon').text(i);
    });
};

var eliminarTag = function( $tag, $containerInputs ){
    var $containerDots = $tag.parent();
    var id = $tag.attr('id');
    if(id!='' && id!=undefined) {
        var index = id.split('_')[1];
        $tag.remove();
        $containerInputs.find('#edit_tag_'+index).remove();
        renameTags($containerDots, $containerInputs);
    }
};

var agregarRandom = function(html, $aDonde, index){
    var $child = $aDonde.children();
    if( $child.length == 0 ){
        $aDonde.html(html);
    } else {
        var posic = Math.floor(Math.random()*$child.length);
        while( posic == index ){
            posic = Math.floor(Math.random()*$child.length);
        }
        var aleat = ($child.length == 1) ? 2 : Math.floor(Math.random()*10);
        if(aleat%2 == 0) { $child.eq(posic).before(html); }
        else { $child.eq(posic).after(html); }
    }
};

var generarTags = function($containerInputs, $containerTags){
    $containerTags.html('');
    $containerInputs.find('.input-group').each(function() {
        var index = $containerInputs.find('.input-group').index( $(this) );
        var id = $(this).attr('id');
        var indexId = id.split('_')[id.split('_').length-1];
        var value = $(this).find('input').val();
        var number = $(this).find('span').text();
        $(this).find('input').attr('value', value);
        var newTag = '<div class="tag" id="tag_'+indexId+'" data-number="'+number+'">'+
                        '<div class="arrow"></div>'+ value +
                    '</div>';

        agregarRandom(newTag, $containerTags, (index+1));

    });
};

var playEjercicio = function(IDGUI, $tagAlts){
    $tagAlts.find('.tag').each(function() {
        var id=$(this).attr('id');
        var indexID = id.split('_')[id.split('_').length-1];
        var numero = $(this).attr('data-number');
        ANS_TAG[IDGUI][numero] = indexID;
        if( !$('.btnsaveActividad').is(':visible') ) {
            $(this).removeAttr('data-number');
        }
    });
};

var evaluarTag = function($selectedTag, $dotContainerActive, ayuda, IDGUI){
    var indexId_selecc = $selectedTag.attr('id').split('_').pop();
    var number_selecc = $dotContainerActive.attr('id').split('_').pop();
    if( indexId_selecc == number_selecc ){
        $dotContainerActive.find('.dot-tag').removeClass('bad');
        $dotContainerActive.find('.dot-tag').addClass('corregido good');
        var textoTag = $dotContainerActive.find('.dot-tag').text().trim();
        if(textoTag.length>0){ pronunciar(textoTag,false,'EN');} /*pronunciacion.js*/
        $selectedTag.addClass('_success_');
        $dotContainerActive.children('.dot').addClass('_success_');
    } else {        
        $dotContainerActive.find('.dot-tag').removeClass('good');
        $dotContainerActive.find('.dot-tag').addClass('corregido bad');
        $dotContainerActive.children('.dot').removeClass('_success_');
        if( esDBYself($dotContainerActive) ) restarPuntaje();
    }

    if(!ayuda){
        $selectedTag.hide();
    }
    pausarTiempoImgTagged();
};

var initImageTagged = function(IDGUI, ayuda){
    ANS_TAG[IDGUI] = {};
    HELP[IDGUI] = ayuda;    
    if( $('#tmp_'+IDGUI+' .botones-edicion').length>0 || $('#tmp_'+IDGUI+' .mask-dots').hasClass('playing') ){
        playEjercicio(IDGUI, $('#tmp_'+IDGUI+' .tag-alts'));
    }
};

var resetImgsTagged = function($tplPlantilla, IDGUI){
    $tplPlantilla.find('.dot-container .dot-tag').each(function() {
        $(this).removeClass('corregido').removeClass('good').removeClass('bad');
        $(this).text('');
        $(this).siblings('._success_').removeClass('_success_');
    });
    $tplPlantilla.parents('.plantilla').find('.tag-alts .tag').show().removeClass('_success_');     
};