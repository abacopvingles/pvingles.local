<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		04-01-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatPersona_experiencialaboral extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_experiencialaboral";
			
			$cond = array();		
			
			if(isset($filtros["idexperiencia"])) {
					$cond[] = "idexperiencia = " . $this->oBD->escapar($filtros["idexperiencia"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["empresa"])) {
					$cond[] = "empresa = " . $this->oBD->escapar($filtros["empresa"]);
			}
			if(isset($filtros["rubro"])) {
					$cond[] = "rubro = " . $this->oBD->escapar($filtros["rubro"]);
			}
			if(isset($filtros["area"])) {
					$cond[] = "area = " . $this->oBD->escapar($filtros["area"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["funciones"])) {
					$cond[] = "funciones = " . $this->oBD->escapar($filtros["funciones"]);
			}
			if(isset($filtros["fechade"])) {
					$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if(isset($filtros["fechahasta"])) {
					$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if(isset($filtros["actualmente"])) {
					$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT pe.* , (SELECT nombre from general g WHERE pe.area=g.codigo AND tipo_tabla='areaempresa') as strareaempresa FROM persona_experiencialaboral pe left join personal pl on pe.idpersona=pl.idpersona";			
			
			$cond = array();		
					
			
			if(isset($filtros["idexperiencia"])) {
					$cond[] = "idexperiencia = " . $this->oBD->escapar($filtros["idexperiencia"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "(pe.idpersona = " . $this->oBD->escapar($filtros["idpersona"])." OR md5(pe.idpersona)=".$this->oBD->escapar($filtros["idpersona"]).")";
			}
			if(isset($filtros["empresa"])) {
					$cond[] = "empresa = " . $this->oBD->escapar($filtros["empresa"]);
			}
			if(isset($filtros["rubro"])) {
					$cond[] = "rubro = " . $this->oBD->escapar($filtros["rubro"]);
			}
			if(isset($filtros["area"])) {
					$cond[] = "area = " . $this->oBD->escapar($filtros["area"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["funciones"])) {
					$cond[] = "funciones = " . $this->oBD->escapar($filtros["funciones"]);
			}
			if(isset($filtros["fechade"])) {
					$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if(isset($filtros["fechahasta"])) {
					$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if(isset($filtros["actualmente"])) {
					$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$empresa,$rubro,$area,$cargo,$funciones,$fechade,$fechahasta,$actualmente,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_experiencialaboral_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexperiencia) FROM persona_experiencialaboral");
			++$id;
			
			$estados = array('idexperiencia' => $id							
							,'idpersona'=>$idpersona
							,'empresa'=>$empresa
							,'rubro'=>$rubro
							,'area'=>$area
							,'cargo'=>$cargo
							,'funciones'=>$funciones
							,'fechade'=>$fechade
							,'fechahasta'=>$fechahasta
							,'actualmente'=>$actualmente
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('persona_experiencialaboral', $estados);			
			$this->terminarTransaccion('dat_persona_experiencialaboral_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_experiencialaboral_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$empresa,$rubro,$area,$cargo,$funciones,$fechade,$fechahasta,$actualmente,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_experiencialaboral_update');
			$estados = array('idpersona'=>$idpersona
							,'empresa'=>$empresa
							,'rubro'=>$rubro
							,'area'=>$area
							,'cargo'=>$cargo
							,'funciones'=>$funciones
							,'fechade'=>$fechade
							,'fechahasta'=>$fechahasta
							,'actualmente'=>$actualmente
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('persona_experiencialaboral ', $estados, array('idexperiencia' => $id));
		    $this->terminarTransaccion('dat_persona_experiencialaboral_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT pe.* , (SELECT nombre from general g WHERE pe.area=g.codigo AND tipo_tabla='areaempresa') as strareaempresa FROM persona_experiencialaboral pe left join personal pl on pe.idpersona=pl.idpersona WHERE pe.idexperiencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_experiencialaboral', array('idexperiencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_experiencialaboral', array($propiedad => $valor), array('idexperiencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_experiencialaboral").": " . $e->getMessage());
		}
	}
   
		
}