<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatMin_capacidades extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM min_capacidades";
			
			$cond = array();		
			
			if(isset($filtros["idcapacidad"])) {
					$cond[] = "idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
			}
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM min_capacidades";			
			
			$cond = array();		
					
			
			if(isset($filtros["idcapacidad"])) {
					$cond[] = "idcapacidad = " . $this->oBD->escapar($filtros["idcapacidad"]);
			}
			if(isset($filtros["idcompetencia"])) {
					$cond[] = "idcompetencia = " . $this->oBD->escapar($filtros["idcompetencia"]);
			}
			if(isset($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			if(isset($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcompetencia,$titulo,$descripcion,$habilidades)
	{
		try {
			
			$this->iniciarTransaccion('dat_min_capacidades_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcapacidad) FROM min_capacidades");
			++$id;
			
			$estados = array('idcapacidad' => $id
							
							,'idcompetencia'=>$idcompetencia
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'habilidades'=>$habilidades							
							);
			
			$this->oBD->insert('min_capacidades', $estados);			
			$this->terminarTransaccion('dat_min_capacidades_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_min_capacidades_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcompetencia,$titulo,$descripcion,$habilidades)
	{
		try {
			$this->iniciarTransaccion('dat_min_capacidades_update');
			$estados = array('idcompetencia'=>$idcompetencia
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'habilidades'=>$habilidades								
							);
			
			$this->oBD->update('min_capacidades ', $estados, array('idcapacidad' => $id));
		    $this->terminarTransaccion('dat_min_capacidades_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM min_capacidades  "
					. " WHERE idcapacidad = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('min_capacidades', array('idcapacidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('min_capacidades', array($propiedad => $valor), array('idcapacidad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Min_capacidades").": " . $e->getMessage());
		}
	}
   
		
}