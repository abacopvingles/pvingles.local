<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		08-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatConfiguracion_docente extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM configuracion_docente";
			
			$cond = array();		
			
			if(!empty($filtros["idconfigdocente"])) {
					$cond[] = "idconfigdocente = " . $this->oBD->escapar($filtros["idconfigdocente"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["tiempototal"])) {
					$cond[] = "tiempototal = " . $this->oBD->escapar($filtros["tiempototal"]);
			}
			if(!empty($filtros["tiempoactividades"])) {
					$cond[] = "tiempoactividades = " . $this->oBD->escapar($filtros["tiempoactividades"]);
			}
			if(!empty($filtros["tiempoteacherresrc"])) {
					$cond[] = "tiempoteacherresrc = " . $this->oBD->escapar($filtros["tiempoteacherresrc"]);
			}
			if(!empty($filtros["tiempogames"])) {
					$cond[] = "tiempogames = " . $this->oBD->escapar($filtros["tiempogames"]);
			}
			if(!empty($filtros["tiempoexamenes"])) {
					$cond[] = "tiempoexamenes = " . $this->oBD->escapar($filtros["tiempoexamenes"]);
			}
			if(!empty($filtros["escalas"])) {
					$cond[] = "escalas = " . $this->oBD->escapar($filtros["escalas"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM configuracion_docente";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idconfigdocente"])) {
					$cond[] = "idconfigdocente = " . $this->oBD->escapar($filtros["idconfigdocente"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idrol"])) {
					$cond[] = "idrol = " . $this->oBD->escapar($filtros["idrol"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["tiempototal"])) {
					$cond[] = "tiempototal = " . $this->oBD->escapar($filtros["tiempototal"]);
			}
			if(!empty($filtros["tiempoactividades"])) {
					$cond[] = "tiempoactividades = " . $this->oBD->escapar($filtros["tiempoactividades"]);
			}
			if(!empty($filtros["tiempoteacherresrc"])) {
					$cond[] = "tiempoteacherresrc = " . $this->oBD->escapar($filtros["tiempoteacherresrc"]);
			}
			if(!empty($filtros["tiempogames"])) {
					$cond[] = "tiempogames = " . $this->oBD->escapar($filtros["tiempogames"]);
			}
			if(!empty($filtros["tiempoexamenes"])) {
					$cond[] = "tiempoexamenes = " . $this->oBD->escapar($filtros["tiempoexamenes"]);
			}
			if(!empty($filtros["escalas"])) {
					$cond[] = "escalas = " . $this->oBD->escapar($filtros["escalas"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM configuracion_docente  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	
	public function insertar($iddocente,$idrol,$idnivel,$idunidad,$idactividad,$tiempototal,$tiempoactividades,$tiempoteacherresrc,$tiempogames,$tiempoexamenes,$escalas)
	{
		try {
			
			$this->iniciarTransaccion('dat_configuracion_docente_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idconfigdocente) FROM configuracion_docente");
			++$id;
			
			$estados = array('idconfigdocente' => $id
							,'iddocente'=>$iddocente
							,'idrol'=>$idrol
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'tiempototal'=>$tiempototal
							,'tiempoactividades'=>$tiempoactividades
							,'tiempoteacherresrc'=>$tiempoteacherresrc
							,'tiempogames'=>$tiempogames
							,'tiempoexamenes'=>$tiempoexamenes							
							,'escalas'=>$escalas							
							);
			
			$this->oBD->insert('configuracion_docente', $estados);			
			$this->terminarTransaccion('dat_configuracion_docente_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_configuracion_docente_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $iddocente,$idrol,$idnivel,$idunidad,$idactividad,$tiempototal,$tiempoactividades,$tiempoteacherresrc,$tiempogames,$tiempoexamenes,$escalas)
	{
		try {
			$this->iniciarTransaccion('dat_configuracion_docente_update');
			$estados = array('iddocente'=>$iddocente
							,'idrol'=>$idrol
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'tiempototal'=>$tiempototal
							,'tiempoactividades'=>$tiempoactividades
							,'tiempoteacherresrc'=>$tiempoteacherresrc
							,'tiempogames'=>$tiempogames
							,'tiempoexamenes'=>$tiempoexamenes								
							,'escalas'=>$escalas								
							);
			
			$this->oBD->update('configuracion_docente ', $estados, array('idconfigdocente' => $id));
		    $this->terminarTransaccion('dat_configuracion_docente_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM configuracion_docente  "
					. " WHERE idconfigdocente = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('configuracion_docente', array('idconfigdocente' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('configuracion_docente', array($propiedad => $valor), array('idconfigdocente' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Configuracion_docente").": " . $e->getMessage());
		}
	}
   
		
}