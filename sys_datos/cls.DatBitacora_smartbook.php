<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-11-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBitacora_smartbook extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";
			
			$cond = array();		
			
			if(isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if(isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if(isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if(isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if(isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if(isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bitacora_smartbook BS RIGHT JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";			
			
			$cond = array();		
					
			
			if(isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if(isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if(isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if(isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if(isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if(isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql .= " ORDER BY fechahora DESC";
			//echo $sql .'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	
	public function getProgresoPromedio($filtros=null)
	{
		try {
			$sql = "SELECT AVG(progreso) FROM bitacora_smartbook";
			
			$cond = array();		
			
			if(isset($filtros["idbitacora"])) {
				$cond[] = "idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if(isset($filtros["pestania"])) {
				$cond[] = "pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if(isset($filtros["total_pestanias"])) {
				$cond[] = "total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if(isset($filtros["fechahora"])) {
				$cond[] = "fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if(isset($filtros["progreso"])) {
				$cond[] = "progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if(isset($filtros["otros_datos"])) {
				$cond[] = "otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}

	public function getSumaProgresos($filtros=null)
	{
		try {
			$sql = "SELECT SUM(progreso) FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";
			
			$cond = array();
			
			if(isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=".$this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if(isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if(isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if(isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if(isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if(isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql.'<br>';
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	
	public function getSumaProgresos2($filtros=null)
	{
		try {
			$sql = "SELECT SUM(progreso) as sumprogreso ,total_pestanias FROM bitacora_smartbook BS JOIN bitacora_alumno_smartbook BAS ON BS.idbitacora_alum_smartbook = BAS.idbitacora_smartbook ";
			
			$cond = array();
			
			if(isset($filtros["idbitacora"])) {
				$cond[] = "BS.idbitacora = " . $this->oBD->escapar($filtros["idbitacora"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "BAS.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
				$cond[] = "BAS.idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
				//$cond[] = "BAS.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
				$cond[] = "BAS.idusuario=".$this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idbitacora_alum_smartbook"])) {
				$cond[] = "BS.idbitacora_alum_smartbook = " . $this->oBD->escapar($filtros["idbitacora_alum_smartbook"]);
			}
			if(isset($filtros["pestania"])) {
				$cond[] = "BS.pestania = " . $this->oBD->escapar($filtros["pestania"]);
			}
			if(isset($filtros["total_pestanias"])) {
				$cond[] = "BS.total_pestanias = " . $this->oBD->escapar($filtros["total_pestanias"]);
			}
			if(isset($filtros["fechahora"])) {
				$cond[] = "BS.fechahora = " . $this->oBD->escapar($filtros["fechahora"]);
			}
			if(isset($filtros["progreso"])) {
				$cond[] = "BS.progreso = " . $this->oBD->escapar($filtros["progreso"]);
			}
			if(isset($filtros["otros_datos"])) {
				$cond[] = "BS.otros_datos = " . $this->oBD->escapar($filtros["otros_datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			$sql.=" GROUP BY total_pestanias ";
			//echo $sql.'<br>';
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	
	public function insertar($idcurso,$idsesion,$idsesionB,$idusuario,$idbitacora_alum_smartbook,$pestania,$total_pestanias,$fechahora,$progreso,$otros_datos)
	{
		try {
			
			$this->iniciarTransaccion('dat_bitacora_smartbook_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idbitacora) FROM bitacora_smartbook");
			++$id;
			
			$estados = array('idbitacora' => $id							
							,'idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							, 'idsesionB' => $idsesionB
							,'idusuario'=>$idusuario
							,'idbitacora_alum_smartbook'=>$idbitacora_alum_smartbook
							,'pestania'=>$pestania
							,'total_pestanias'=>$total_pestanias
							,'fechahora'=>$fechahora
							,'progreso'=>$progreso
							);
			if(!empty($otros_datos)){
				$estados["otros_datos"] = $otros_datos;
			}
			
			$this->oBD->insert('bitacora_smartbook', $estados);			
			$this->terminarTransaccion('dat_bitacora_smartbook_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bitacora_smartbook_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idsesion,$idsesionB,$idusuario,$idbitacora_alum_smartbook,$pestania,$total_pestanias,$fechahora,$progreso,$otros_datos)
	{
		try {
			if(empty($pestania)) return $id;
			$this->iniciarTransaccion('dat_bitacora_smartbook_update');
			$estados = array(
							'idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idsesionB' => $idsesionB
							,'idusuario'=>$idusuario
							,'idbitacora_alum_smartbook'=>$idbitacora_alum_smartbook
							,'pestania'=>$pestania
							,'total_pestanias'=>$total_pestanias
							,'fechahora'=>$fechahora
							,'progreso'=>$progreso
							);
			if(!empty($otros_datos)){
				$estados["otros_datos"] = $otros_datos;
			}

			$this->oBD->update('bitacora_smartbook ', $estados, array('idbitacora' => $id));
		    $this->terminarTransaccion('dat_bitacora_smartbook_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bitacora_smartbook  "
					. " WHERE idbitacora = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_smartbook', array('idbitacora' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bitacora_smartbook', array($propiedad => $valor), array('idbitacora' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_smartbook").": " . $e->getMessage());
		}
	}   
		
}