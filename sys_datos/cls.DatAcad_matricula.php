<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_matricula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_matricula";
			
			$cond = array();		
			
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
					$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT m.*,gd.idgrupoaula,ga.idproyecto,ac.`idcurso`,ac.`nombre`,ac.`imagen`,ac.`descripcion`,ac.`estado` AS estado_curso,ac.`fecharegistro`, ac.`idusuario`, ac.`vinculosaprendizajes`, ac.`materialesyrecursos`, ac.`color`, ac.`objetivos`, ac.`certificacion`, ac.`costo`, ac.`silabo`,ac.`e_ini`,ac.`pdf`,ac.`abreviado`,ac.`e_fin`,ac.`aniopublicacion`,ac.`autor`,ac.`txtjson`, ga.nombre AS strcurso, (SELECT CONCAT(do.ape_paterno,' ',do.ape_materno,', ',do.nombre) FROM personal do WHERE do.dni=gd.iddocente OR do.idpersona=gd.iddocente limit 1) AS strdocente, (SELECT nombre FROM local l WHERE l.idlocal=gd.idlocal ) AS strlocal, CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) as stralumno,dni,email as alumno_email, pe.usuario,pe.foto,pe.telefono,pe.tipodoc,pe.sexo,pe.estado_civil, pe.esdemo, (SELECT CONCAT(numero,' ', (SELECT nombre from general g3 WHERE am.tipo=g3.codigo AND tipo_tabla='tipoambiente')) FROM ambiente am WHERE gd.idambiente=am.idambiente) AS strambiente FROM acad_matricula m LEFT JOIN acad_grupoauladetalle gd ON m.idgrupoauladetalle=gd.idgrupoauladetalle INNER JOIN acad_grupoaula ga ON gd.idgrupoaula=ga.idgrupoaula INNER JOIN acad_curso ac ON ac.idcurso=gd.idcurso INNER JOIN personal pe ON m.idalumno=pe.idpersona";
			
			$cond = array();		
			if(isset($filtros["idmatricula"])) $cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			if(isset($filtros["idgrupoauladetalle"]))  $cond[] = "m.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			
			if(isset($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			
			if(isset($filtros["fecha_registro"])) {
				$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["idgrupoaula"])) {
				$cond[] = "gd.idgrupoaula = " . $this->oBD->escapar($filtros["idgrupoaula"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "m.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "m.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "gd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "ga.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["iddocente"])){
				$cond[] = "gd.iddocente = ".$this->oBD->escapar($filtros["iddocente"]);
			}

			if(isset($filtros["texto"])) {
				$cond[] = " ( idalumno " . $this->oBD->like($filtros["texto"])." OR CONCAT(pe.ape_paterno,' ',pe.ape_materno,', ',pe.nombre) ".$this->oBD->like($filtros["texto"]).") ";
			}

			if(isset($filtros["fecha_matricula"])) {
				$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if(isset($filtro["orderby"])){
				if($filtro["orderby"]=='nombre')
					$sql .=" ORDER By stralumno ASC ";
			}else
				$sql .= " ORDER BY idmatricula Desc";
			
		//echo $sql;
		// var_dump($sql);
			return $this->oBD->consultarSQL($sql);			
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
			public function cursosAlumno($filtros=null)
	{
		try {
			$sql = "SELECT 
				M.idmatricula, 
				M.idalumno,
				M.fecha_registro, 
				M.fecha_matricula, 
				GAD.idgrupoauladetalle, 
				GAD.iddocente, 
				GAD.fecha_inicio, 
				GAD.fecha_final, 
				C.* , (SELECT CONCAT(P.ape_paterno,' ',P.ape_materno,' ',P.nombre) FROM personal P WHERE P.idpersona=GAD.iddocente  ) AS docente_nombre
			FROM acad_matricula M 
			JOIN acad_grupoauladetalle GAD ON M.idgrupoauladetalle=GAD.idgrupoauladetalle 
			JOIN acad_curso C ON GAD.idcurso=C.idcurso
			JOIN proyecto_cursos PC ON C.idcurso=PC.idcurso ";	
			
			$cond = array();
			if(isset($filtros["idmatricula"])) {
					$cond[] = "M.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "M.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "M.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "M.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "M.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "M.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
					$cond[] = "M.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "GAD.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "PC.idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}		
			$cond[] = " C.estado = 1 ";
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY C.nombre ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function horarioAlumno($filtros=null)
	{
		try {			
			$sql="SELECT a.fecha_inicio as fecha_inicioG, a.fecha_final as fecha_finalG,h.*,mat.*,c.nombre FROM acad_matricula mat left join acad_grupoauladetalle a on a.idgrupoauladetalle=mat.idgrupoauladetalle left join acad_horariogrupodetalle h on h.idgrupoauladetalle=a.idgrupoauladetalle left join acad_curso c on c.idcurso=a.idcurso";
			
			$cond = array();		

			if(isset($filtros["idmatricula"])) {
				$cond[] = "mat.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "mat.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
				$cond[] = "mat.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
				$cond[] = "mat.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "mat.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "mat.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
				$cond[] = "mat.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "a.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function horarioDocente($filtros=null)
	{
		try {			
			$sql="SELECT DISTINCT a.fecha_inicio as fecha_inicioG, a.fecha_final as fecha_finalG,h.*,c.nombre FROM acad_matricula mat left join acad_grupoauladetalle a on a.idgrupoauladetalle=mat.idgrupoauladetalle left join acad_horariogrupodetalle h on h.idgrupoauladetalle=a.idgrupoauladetalle left join acad_curso c on c.idcurso=a.idcurso";
			
			$cond = array();		

			if(isset($filtros["idmatricula"])) {
				$cond[] = "mat.idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
				$cond[] = "mat.idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
				$cond[] = "mat.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
				$cond[] = "mat.fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "mat.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
				$cond[] = "mat.idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
				$cond[] = "mat.fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(isset($filtros["idcurso"])) {
				$cond[] = "a.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["iddocente"])) {
				$cond[] = "a.iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	

			#echo $sql; exit(0);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	
	public function insertar($idgrupoauladetalle,$idalumno,$fecha_registro,$estado,$idusuario,$fecha_matricula)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_matricula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
			++$id;
			
			$estados = array('idmatricula' => $id
							
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha_registro'=>$fecha_registro
							,'estado'=>$estado
							,'idusuario'=>$idusuario
							,'fecha_matricula'=>$fecha_matricula							
							);
			
			$this->oBD->insert('acad_matricula', $estados);			
			$this->terminarTransaccion('dat_acad_matricula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_matricula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle,$idalumno,$fecha_registro,$estado,$idusuario,$fecha_matricula)
	{
		try {
			$this->iniciarTransaccion('dat_acad_matricula_update');
			$estados = array('idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha_registro'=>$fecha_registro
							,'estado'=>$estado
							,'idusuario'=>$idusuario
							,'fecha_matricula'=>$fecha_matricula								
							);
			
			$this->oBD->update('acad_matricula ', $estados, array('idmatricula' => $id));
		    $this->terminarTransaccion('dat_acad_matricula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_matricula  "
					. " WHERE idmatricula = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_matricula', array('idmatricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			
			$this->oBD->update('acad_matricula', array($propiedad => $valor), array('idmatricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
}