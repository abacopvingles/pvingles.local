<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-02-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatBitacora_alumno_smartbook extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bitacora_alumno_smartbook";
			
			$cond = array();		
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bitacora_alumno_smartbook";			
			
			$cond = array();		
					
			
			if(isset($filtros["idbitacora_smartbook"])) {
					$cond[] = "idbitacora_smartbook = " . $this->oBD->escapar($filtros["idbitacora_smartbook"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["idsesion"])) {
					$cond[] = "idsesion = " . $this->oBD->escapar($filtros["idsesion"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$idsesion,$idusuario,$estado,$regfecha)
	{
		try {
			
			$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idbitacora_smartbook) FROM bitacora_alumno_smartbook");
			++$id;
			
			$estados = array('idbitacora_smartbook' => $id
							
							,'idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idusuario'=>$idusuario
							,'estado'=>$estado
							,'regfecha'=>$regfecha							
							);
			
			$this->oBD->insert('bitacora_alumno_smartbook', $estados);			
			$this->terminarTransaccion('dat_bitacora_alumno_smartbook_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bitacora_alumno_smartbook_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idcurso,$idsesion,$idusuario,$estado,$regfecha)
	{
		try {
			$this->iniciarTransaccion('dat_bitacora_alumno_smartbook_update');
			$estados = array('idcurso'=>$idcurso
							,'idsesion'=>$idsesion
							,'idusuario'=>$idusuario
							,'estado'=>$estado
							,'regfecha'=>$regfecha								
							);
			
			$this->oBD->update('bitacora_alumno_smartbook ', $estados, array('idbitacora_smartbook' => $id));
		    $this->terminarTransaccion('dat_bitacora_alumno_smartbook_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bitacora_alumno_smartbook  "
					. " WHERE idbitacora_smartbook = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bitacora_alumno_smartbook', array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bitacora_alumno_smartbook', array($propiedad => $valor), array('idbitacora_smartbook' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bitacora_alumno_smartbook").": " . $e->getMessage());
		}
	}
   
		
}