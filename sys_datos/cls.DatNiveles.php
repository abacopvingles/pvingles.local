<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-10-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatNiveles extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM niveles";
			
			$cond = array();	
			
			if(isset($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM niveles";			
			
			$cond = array();		
					
			
			if(isset($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			
						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			if(!empty($filtros['sysordenar']))
				$sql.=' ORDER BY '.$filtros['sysordenar'];
			else
				$sql.=' ORDER BY orden asc ,idnivel ASC';
			
			//$sql .= " ORDER BY fecha_creado ASC";
			// echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM niveles  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}

	public function maxorden($tipo,$idpadre){
		$cond='';
		if(!empty($idpadre))$cond=" AND idpadre='".$idpadre."' ";
		$sql="SELECT MAX(orden) FROM niveles WHERE tipo='".$tipo."'".$cond;
		return $this->oBD->consultarEscalarSQL($sql);
	}
	
	
	public function insertar($nombre,$tipo,$idpadre,$idpersonal,$estado,$orden,$imagen,$descripcion='')
	{
		try {			
			$this->iniciarTransaccion('dat_niveles_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idnivel) FROM niveles");
			++$id;
			if (!$estado) $estado=0;
			$estados = array('idnivel' => $id							
							,'nombre'=>$nombre
							,'tipo'=>$tipo
							,'idpadre'=>$idpadre
							,'idpersonal'=>$idpersonal
							,'estado'=>$estado
							,'orden'=>$orden
							,'imagen'=>$imagen
							#,'descripcion'=>$descripcion
							);
			
			$this->oBD->insert('niveles', $estados);			
			$this->terminarTransaccion('dat_niveles_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_niveles_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$tipo,$idpadre,$idpersonal,$estado,$orden,$imagen,$descripcion)
	{
		try {
			$this->iniciarTransaccion('dat_niveles_update');
			$estados = array('nombre'=>$nombre
							,'tipo'=>$tipo
							,'idpadre'=>$idpadre
							,'idpersonal'=>$idpersonal
							,'estado'=>$estado
							,'orden'=>$orden
							,'imagen'=>$imagen
							#,'descripcion'=>$descripcion								
							);
			
			$this->oBD->update('niveles ', $estados, array('idnivel' => $id));
		    $this->terminarTransaccion('dat_niveles_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM niveles  WHERE idnivel = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try{			
			return $this->oBD->delete('niveles', array('idnivel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('niveles', array($propiedad => $valor), array('idnivel' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}

	public function setCampo($filtros, $propiedad, $valor)
	{//02.01.13
		try {
			$cond = array();
			if(!empty($filtros["idnivel"])) {
				$cond['idnivel']=$this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["tipo"])) {
				$cond["tipo"] = $filtros["tipo"];
			}
			if(!empty($filtros["idpadre"])) {
				$cond['idpadre'] = $this->oBD->escapar($filtros["idpadre"]);
			}
			if(!empty($filtros["orden"])) {
				$cond['orden'] = $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($cond))
				return $this->oBD->update('niveles', array($propiedad => $valor),$cond);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
}