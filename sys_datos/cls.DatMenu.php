<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatMenu extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM menu";
			
			$cond = array();		
			
			if(!empty($filtros["idmenu"])) {
					$cond[] = "idmenu = " . $this->oBD->escapar($filtros["idmenu"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM menu";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idmenu"])) {
					$cond[] = "idmenu = " . $this->oBD->escapar($filtros["idmenu"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM menu  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre)
	{
		try {
			
			$this->iniciarTransaccion('dat_menu_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmenu) FROM menu");
			++$id;
			
			$estados = array('idmenu' => $id
							
							,'nombre'=>$nombre							
							);
			
			$this->oBD->insert('menu', $estados);			
			$this->terminarTransaccion('dat_menu_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_menu_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre)
	{
		try {
			$this->iniciarTransaccion('dat_menu_update');
			$estados = array('nombre'=>$nombre								
							);
			
			$this->oBD->update('menu ', $estados, array('idmenu' => $id));
		    $this->terminarTransaccion('dat_menu_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM menu  "
					. " WHERE idmenu = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('menu', array('idmenu' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('menu', array($propiedad => $valor), array('idmenu' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Menu").": " . $e->getMessage());
		}
	}
   
		
}