<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		22-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatActividades extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM actividades";
			
			$cond = array();		
			
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["unidad"])) {
					$cond[] = "unidad = " . $this->oBD->escapar($filtros["unidad"]);
			}
			if(!empty($filtros["sesion"])) {
					$cond[] = "sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if(!empty($filtros["metodologia"])) {
					$cond[] = "metodologia = " . $this->oBD->escapar($filtros["metodologia"]);
			}
			
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->like($filtros["titulo"]);
			}

			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			
					
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM actividades";			
			
			$cond = array();			
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["unidad"])) {
					$cond[] = "unidad = " . $this->oBD->escapar($filtros["unidad"]);
			}
			if(!empty($filtros["sesion"])) {
					$cond[] = "sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if(!empty($filtros["metodologia"])) {
					$cond[] = "metodologia = " . $this->oBD->escapar($filtros["metodologia"]);
			}
			
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->like($filtros["titulo"]);
			}			
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}

	public function buscarActividad($filtros=null)
	{
		try {
			$sql = "SELECT ac.idactividad,nivel,unidad,sesion,metodologia,titulo,nombre,ac.descripcion, imagen,ac.idpersonal FROM actividades ac INNER JOIN niveles ni ON idnivel=sesion ";			
			
			$cond = array();			
			if(!empty($filtros["idactividad"])) {
					$cond[] = "ac.idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["nivel"])) {
					$cond[] = "nivel = " . $this->oBD->escapar($filtros["nivel"]);
			}
			if(!empty($filtros["unidad"])) {
					$cond[] = "unidad = " . $this->oBD->escapar($filtros["unidad"]);
			}
			if(!empty($filtros["sesion"])) {
					$cond[] = "sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if(!empty($filtros["metodologia"])) {
					$cond[] = "metodologia = " . $this->oBD->escapar($filtros["metodologia"]);
			}
			
			if(!empty($filtros["texto"])) {
					$cond[] = "concat(titulo,' ', nombre,' ',ac.descripcion )" . $this->oBD->like($filtros["texto"]);
			}			
			if(!empty($filtros["estado"])) {
					$cond[] = "ac.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
						
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			//echo $sql;
			
			//$sql .= " ORDER BY fecha_creado ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
	public function insertar($nivel,$unidad,$sesion,$metodologia,$habilidad,$titulo,$descripcion,$estado,$url,$idioma,$iduser)
	{
		try {
			
			$this->iniciarTransaccion('dat_actividades_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idactividad) FROM actividades");
			++$id;
			
			$estados = array('idactividad' => $id							
							,'nivel'=>$nivel
							,'unidad'=>$unidad
							,'sesion'=>$sesion
							,'metodologia'=>$metodologia
							,'habilidad'=>$habilidad						
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'url'=>$url
							,'idioma'=>$idioma
							,'idpersonal'=>$iduser						
							);
			
			$this->oBD->insert('actividades', $estados);			
			$this->terminarTransaccion('dat_actividades_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_actividades_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nivel,$unidad,$sesion,$metodologia,$habilidad,$titulo,$descripcion,$estado,$url,$idioma,$iduser)
	{
		try {
			$this->iniciarTransaccion('dat_actividades_update');
			$estados = array('nivel'=>$nivel
							,'unidad'=>$unidad
							,'sesion'=>$sesion
							,'metodologia'=>$metodologia
							,'habilidad'=>$habilidad							
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'estado'=>$estado
							,'url'=>$url
							,'idioma'=>$idioma
							,'idpersonal'=>$iduser								
							);
			
			$this->oBD->update('actividades ', $estados, array('idactividad' => $id));
		    $this->terminarTransaccion('dat_actividades_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM actividades  "
					. " WHERE idactividad = " . $this->oBD->escapar($id);
			//echo $sql;
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividades', array('idactividad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('actividades', array($propiedad => $valor), array('idactividad' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividades").": " . $e->getMessage());
		}
	}
}