<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		25-10-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatLocal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM local";
			
			$cond = array();		
			
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function infolocal($filtros = null){
		try{
			$sql = "SELECT l.idlocal,l.nombre, u.descripcion AS ugel, (select mdre.descripcion from min_dre mdre where u.iddepartamento = mdre.ubigeo) as dre, (select ubi.ciudad from ubigeo ubi where ubi.id_ubigeo = u.iddepartamento) AS departamento FROM local l INNER JOIN ugel u ON u.idugel = l.idugel";
			$cond = array();
			
			if(isset($filtros["idlocal"])) {
				$cond[] = "l.idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			// $sql .= " ORDER BY l.nombre ASC";
			
			return $this->oBD->consultarSQL($sql);
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT l.*, u.descripcion as ugel, (SELECT nombre from general g WHERE l.tipo=g.codigo AND tipo_tabla='tipolocal') as strtipolocal FROM local l LEFT JOIN ugel u on u.idugel=l.idugel ";			
			
			$cond = array();		
					
			
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "l.nombre " . $this->oBD->like($filtros["nombre"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "l.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = "l.nombre  " . $this->oBD->like($filtros["texto"]);
			}
			if(isset($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "l.idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY l.nombre ASC";
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto)
	{
		try {
			
			$this->iniciarTransaccion('dat_local_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idlocal) FROM local");
			++$id;
			
			$estados = array('idlocal' => $id							
							,'nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->insert('local', $estados);			
			$this->terminarTransaccion('dat_local_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_local_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function importar($id,$nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto){
		try{
			if(empty($id)) return $this->insertar($nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto);
			else{
				$this->iniciarTransaccion('dat_local_insert2');
				$estados = array('idlocal' => $id							
							,'nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel
							,'idproyecto'=>$idproyecto
							);
				$this->oBD->insert('local', $estados);
				$this->terminarTransaccion('dat_local_insert2');
				return $id;
			}
		}catch(Exception $e){
			$this->cancelarTransaccion('dat_local_insert2');
			return -1;
		}
	}



	public function actualizar($id, $nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_local_update');
			$estados = array('nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel
							,'idproyecto'=>$idproyecto								
							);
			
			$this->oBD->update('local ', $estados, array('idlocal' => $id));
		    $this->terminarTransaccion('dat_local_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT l.*, u.descripcion as ugel, g.nombre as tipo_local  FROM local l LEFT JOIN ugel u on u.idugel=l.idugel LEFT JOIN general g ON l.tipo=g.codigo WHERE l.idlocal = " . $this->oBD->escapar($id)." AND g.tipo_tabla = 'tipolocal'";			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('local', array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('local', array($propiedad => $valor), array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
   
		
}