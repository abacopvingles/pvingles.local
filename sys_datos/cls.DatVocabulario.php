<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		23-12-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatVocabulario extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM vocabulario";
			
			$cond = array();		
			
			if(!empty($filtros["idvocabulario"])) {
					$cond[] = "idvocabulario = " . $this->oBD->escapar($filtros["idvocabulario"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])){
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM vocabulario";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idvocabulario"])) {
					$cond[] = "idvocabulario = " . $this->oBD->escapar($filtros["idvocabulario"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY orden ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}
	
	public function insertar($idnivel,$idunidad,$idactividad,$idpersonal,$texto,$orden)
	{
		try {			
			$this->iniciarTransaccion('dat_vocabulario_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idvocabulario) FROM vocabulario");
			++$id;			
			$estados = array('idvocabulario' => $id							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'texto'=>$texto
							,'orden'=>$orden							
							);
			
			$this->oBD->insert('vocabulario', $estados);			
			$this->terminarTransaccion('dat_vocabulario_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_vocabulario_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$idpersonal,$texto,$orden)
	{
		try {
			$this->iniciarTransaccion('dat_vocabulario_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'idpersonal'=>$idpersonal
							,'texto'=>$texto
							,'orden'=>$orden								
							);
			
			$this->oBD->update('vocabulario ', $estados, array('idvocabulario' => $id));
		    $this->terminarTransaccion('dat_vocabulario_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT * FROM vocabulario WHERE idvocabulario = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('vocabulario', array('idvocabulario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('vocabulario', array($propiedad => $valor), array('idvocabulario' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Vocabulario").": " . $e->getMessage());
		}
	}   
}