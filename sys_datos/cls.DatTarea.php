<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea";
			
			$cond = array();		
			
			if(!empty($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($filtros["asignacion"])) {
				$cond[] = "asignacion = " . $this->oBD->escapar($filtros["asignacion"]);
			}
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["foto"])) {
				$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["habilidades"])) {
				$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if(!empty($filtros["habilidad_destacada"])) {
				$cond[] = "habilidad_destacada = " . $this->oBD->escapar($filtros["habilidad_destacada"]);
			}
			if(!empty($filtros["puntajemaximo"])) {
				$cond[] = "puntajemaximo = " . $this->oBD->escapar($filtros["puntajemaximo"]);
			}
			if(!empty($filtros["puntajeminimo"])) {
				$cond[] = "puntajeminimo = " . $this->oBD->escapar($filtros["puntajeminimo"]);
			}
			if(!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["eliminado"])) {
				$cond[] = "eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT DISTINCT(idtarea), `idactividad`, `idcursodetalle`, `iddocente`, `idproyecto`, `asignacion`, `nombre`, `descripcion`, `foto`, `habilidades`, `habilidad_destacada`, `puntajemaximo`, `puntajeminimo`, `estado`, `eliminado` FROM tarea";			
			
			$cond = $cond_OR = array();
			$flag = true;
					
			if(!empty($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["iddocente"])) {
				$condicion = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
				if(!empty($filtros["or_asignacion"])) {
					$condicion .= " OR asignacion = " . $this->oBD->escapar($filtros["or_asignacion"]);
					$flag = false;
				}
				$cond[] = ' ( '.$condicion.' ) ';
			}
			if(!empty($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($filtros["asignacion"])) {
				$cond[] = "asignacion = " . $this->oBD->escapar($filtros["asignacion"]);
			}
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["foto"])) {
				$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["habilidades"])) {
				$cond_hab = array();
				foreach ($filtros["habilidades"] as $idHabilildad) {
					$cond_hab[] = "habilidades LIKE '%\"" . $idHabilildad ."\"%'";
				}
				$cond[] = " (" . implode(' AND ', $cond_hab) . ") ";
			}
			if(!empty($filtros["habilidad_destacada"])) {
				$cond[] = "habilidad_destacada = " . $this->oBD->escapar($filtros["habilidad_destacada"]);
			}
			if(!empty($filtros["puntajemaximo"])) {
				$cond[] = "puntajemaximo = " . $this->oBD->escapar($filtros["puntajemaximo"]);
			}
			if(!empty($filtros["puntajeminimo"])) {
				$cond[] = "puntajeminimo = " . $this->oBD->escapar($filtros["puntajeminimo"]);
			}
			if(!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["eliminado"])) {
				$cond[] = "eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}

			if(isset($filtros["allrecursosnull"])) {
				$cond[] = " (idactividad IN (" . $filtros["allrecursosnull"].") OR idactividad IS NULL)";
				
			}

			if(isset($filtros["allrecursos"])) {
				
				$cond[] = " idactividad IN (" . $filtros["allrecursos"].") ";
			}

			// if($filtros["vacio"]==null) {
				// $cond[] = "idactividad = " . $filtros["vacio"];
			// }

			if(isset($filtros["idcurso"])) {
				$cond[] = "idcursodetalle IN (
					SELECT idcursodetalle FROM acad_cursodetalle 
					WHERE idcurso = ". $this->oBD->escapar($filtros["idcurso"])." 
				)" ;
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
				if(!empty($filtros["or_asignacion"]) && $flag===true) {
					$sql .= " OR asignacion = " . $this->oBD->escapar($filtros["or_asignacion"]);
				}
			}
			
			$sql .= " ORDER BY idtarea ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	
	public function insertar($idnivel,$idunidad,$idactividad,$iddocente,$idcursodetalle,$idproyecto,$asignacion,$nombre,$descripcion,$foto,$habilidades,$habilidad_destacada,$puntajemaximo,$puntajeminimo,$estado,$eliminado)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea) FROM tarea");
			++$id;
			
			$estados = array('idtarea' => $id
							
							,'idcursodetalle'=>$idcursodetalle
							,'idproyecto'=>$idproyecto
							,'iddocente'=>$iddocente
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'habilidades'=>$habilidades
							,'puntajemaximo'=>$puntajemaximo
							,'puntajeminimo'=>$puntajeminimo
							,'estado'=>$estado
							,'eliminado'=>$eliminado
							);
			if(!empty($asignacion)){
				$estados['asignacion'] = $asignacion;
			}
			if(!empty($habilidad_destacada)){
				$estados['habilidad_destacada'] = $habilidad_destacada;
			}
			if(!empty($idnivel)){
				$estados['idnivel'] = $idnivel;
			}
			if(!empty($idunidad)){
				$estados['idunidad'] = $idunidad;
			}
			if(!empty($idactividad)){
				$estados['idactividad'] = $idactividad;
			}

			$this->oBD->insert('tarea', $estados);			
			$this->terminarTransaccion('dat_tarea_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$iddocente,$idcursodetalle,$idproyecto,$asignacion,$nombre,$descripcion,$foto,$habilidades,$habilidad_destacada,$puntajemaximo,$puntajeminimo,$estado,$eliminado)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_update');
			$estados = array('idcursodetalle'=>$idcursodetalle
							,'idproyecto'=>$idproyecto
							,'iddocente'=>$iddocente
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'habilidades'=>$habilidades
							,'puntajemaximo'=>$puntajemaximo
							,'puntajeminimo'=>$puntajeminimo
							,'estado'=>$estado
							,'eliminado'=>$eliminado
							);
			if(!empty($asignacion)){
				$estados['asignacion'] = $asignacion;
			}
			if(!empty($habilidad_destacada)){
				$estados['habilidad_destacada'] = $habilidad_destacada;
			}
			if(!empty($idnivel)){
				$estados['idnivel'] = $idnivel;
			}
			if(!empty($idunidad)){
				$estados['idunidad'] = $idunidad;
			}
			if(!empty($idactividad)){
				$estados['idactividad'] = $idactividad;
			}
			
			$this->oBD->update('tarea ', $estados, array('idtarea' => $id));
		    $this->terminarTransaccion('dat_tarea_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea  "
					. " WHERE idtarea = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea', array('idtarea' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea', array($propiedad => $valor), array('idtarea' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
   
	public function xTarea($filtros=null)
	{
		try {
			$sql = "SELECT ta.*, concat(ta.fechaentrega,' ',ta.horaentrega) as fecha,t.*,tal.* FROM tarea t left join tarea_asignacion ta on t.idtarea=ta.idtarea left join tarea_asignacion_alumno tal on ta.idtarea_asignacion=tal.idtarea_asignacion";		
			
			$cond = array();		
					
			if(!empty($filtros["idtarea"])) {
				$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idnivel"])) {
				$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
				$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
				$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["iddocente"])) {
				$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["idcursodetalle"])) {
				$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(!empty($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(!empty($filtros["asignacion"])) {
				$cond[] = "asignacion = " . $this->oBD->escapar($filtros["asignacion"]);
			}
			if(!empty($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}	
	public function actualizartabla($tabla,$campo){
		var_dump($tabla,$campo);
		$dt=$this->oBD->consultarSQL("SELECT * FROM tarea ORDER BY idtarea DESC");
		foreach($dt as $per){
			if (empty($per['idactividad'])) {
				// echo $per['idcursodetalle']." -- ";
				$dt1=$this->oBD->consultarSQL("SELECT * FROM acad_cursodetalle WHERE idcursodetalle = ".$per['idcursodetalle']);
				foreach ($dt1 as $per1) {
					// echo $per1['idrecurso']." - ";
					$x=$this->oBD->update($tabla, array($campo => $per1['idrecurso']), array('idcursodetalle' =>$per["idcursodetalle"]));	
					echo $x."- ".$per["idcursodetalle"]." - ".$campo;				
				}
			}
			// $x=$this->oBD->update($tabla, array($campo => $per["idpersona"]), array($campo =>$per["dni"]));
			
		}
	}
}