<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatExamen_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM examen_alumno";
			
			$cond = array();		
			
			if(!empty($filtros["idexaalumno"])) {
					$cond[] = "idexaalumno = " . $this->oBD->escapar($filtros["idexaalumno"]);
			}
			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
						
			if(!empty($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			
			if(!empty($filtros["tiempoduracion"])) {
					$cond[] = "tiempoduracion = " . $this->oBD->escapar($filtros["tiempoduracion"]);
			}
			if(!empty($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["intento"])) {
					$cond[] = "intento = " . $this->oBD->escapar($filtros["intento"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM examen_alumno";			
			
			$cond = array();		
					
			if(!empty($filtros["idexaalumno"])) {
					$cond[] = "idexaalumno = " . $this->oBD->escapar($filtros["idexaalumno"]);
			}
			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
						
			if(!empty($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			
			if(!empty($filtros["tiempoduracion"])) {
					$cond[] = "tiempoduracion = " . $this->oBD->escapar($filtros["tiempoduracion"]);
			}
			if(!empty($filtros["fecha"])) {
					$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["intento"])) {
					$cond[] = "intento = " . $this->oBD->escapar($filtros["intento"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idexamen,$idalumno,$preguntas,$resultado,$puntajehabilidad,$puntaje,$resultadojson,$tiempoduracion,$intento)
	{
		try {
			
			$this->iniciarTransaccion('dat_examen_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexaalumno) FROM examen_alumno");
			++$id;
			
			$estados = array('idexaalumno' => $id
							
							,'idexamen'=>$idexamen
							,'idalumno'=>$idalumno
							,'preguntas'=>$preguntas
							,'resultado'=>$resultado
							,'puntajehabilidad'=>$puntajehabilidad
							,'puntaje'=>$puntaje
							,'resultadojson'=>$resultadojson
							,'tiempoduracion'=>$tiempoduracion
							,'fecha'=>date('Y/m/d')
							,'intento'=>$intento							
							);
			
			$this->oBD->insert('examen_alumno', $estados);			
			$this->terminarTransaccion('dat_examen_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examen_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idexamen,$idalumno,$preguntas,$resultado,$puntajehabilidad,$puntaje,$resultadojson,$tiempoduracion,$intento)
	{
		try {
			$this->iniciarTransaccion('dat_examen_alumno_update');
			$estados = array('idexamen'=>$idexamen
							,'idalumno'=>$idalumno
							,'preguntas'=>$preguntas
							,'resultado'=>$resultado
							,'puntajehabilidad'=>$puntajehabilidad
							,'puntaje'=>$puntaje
							,'resultadojson'=>$resultadojson
							,'tiempoduracion'=>$tiempoduracion
							,'fecha'=>date('Y/m/d')
							,'intento'=>$intento								
							);
			
			$this->oBD->update('examen_alumno ', $estados, array('idexaalumno' => $id));
		    $this->terminarTransaccion('dat_examen_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM examen_alumno  "
					. " WHERE idexaalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('examen_alumno', array('idexaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('examen_alumno', array($propiedad => $valor), array('idexaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examen_alumno").": " . $e->getMessage());
		}
	}
   
		
}