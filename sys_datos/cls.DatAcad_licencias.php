<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-12-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_licencias extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_licencias";
			
			$cond = array();		
			
			if(isset($filtros["idlicencia"])) {
					$cond[] = "idlicencia = " . $this->oBD->escapar($filtros["idlicencia"]);
			}
			if(isset($filtros["idempresa"])) {
					$cond[] = "idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["costoxrol"])) {
					$cond[] = "costoxrol = " . $this->oBD->escapar($filtros["costoxrol"]);
			}
			if(isset($filtros["costoxdescarga"])) {
					$cond[] = "costoxdescarga = " . $this->oBD->escapar($filtros["costoxdescarga"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT al.*, be.nombre as empresa FROM acad_licencias al INNER JOIN bolsa_empresas be ON be.idempresa=al.idempresa";			
			
			$cond = array();					
			
			if(isset($filtros["idlicencia"])) {
					$cond[] = "idlicencia = " . $this->oBD->escapar($filtros["idlicencia"]);
			}
			if(!empty($filtros["idempresa"])) {
					$cond[] = "al.idempresa = " . $this->oBD->escapar($filtros["idempresa"]);
			}
			if(isset($filtros["texto"])) {
				$cond[] = "be.nombre = " . $this->oBD->like($filtros["texto"]);
			}

			if(isset($filtros["costoxrol"])) {
					$cond[] = "costoxrol = " . $this->oBD->escapar($filtros["costoxrol"]);
			}
			if(isset($filtros["costoxdescarga"])) {
					$cond[] = "costoxdescarga = " . $this->oBD->escapar($filtros["costoxdescarga"]);
			}
			if(isset($filtros["fecha_inicio"])) {
					$cond[] = "fecha_inicio = " . $this->oBD->escapar($filtros["fecha_inicio"]);
			}
			if(isset($filtros["fecha_final"])) {
					$cond[] = "fecha_final = " . $this->oBD->escapar($filtros["fecha_final"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY be.nombre ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idempresa,$costoxrol,$costoxdescarga,$fecha_inicio,$fecha_final,$estado)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_licencias_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idlicencia) FROM acad_licencias");
			++$id;
			
			$estados = array('idlicencia' => $id
							,'idempresa'=>$idempresa
							,'costoxrol'=>$costoxrol
							,'costoxdescarga'=>$costoxdescarga
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'estado'=>$estado							
							);
			
			$this->oBD->insert('acad_licencias', $estados);			
			$this->terminarTransaccion('dat_acad_licencias_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_licencias_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idempresa,$costoxrol,$costoxdescarga,$fecha_inicio,$fecha_final,$estado)
	{
		try {
			$this->iniciarTransaccion('dat_acad_licencias_update');
			$estados = array('idempresa'=>$idempresa
							,'costoxrol'=>$costoxrol
							,'costoxdescarga'=>$costoxdescarga
							,'fecha_inicio'=>$fecha_inicio
							,'fecha_final'=>$fecha_final
							,'estado'=>$estado								
							);
			
			$this->oBD->update('acad_licencias ', $estados, array('idlicencia' => $id));
		    $this->terminarTransaccion('dat_acad_licencias_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_licencias  "
					. " WHERE idlicencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_licencias', array('idlicencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_licencias', array($propiedad => $valor), array('idlicencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_licencias").": " . $e->getMessage());
		}
	}
   
		
}