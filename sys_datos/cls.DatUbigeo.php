<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatUbigeo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM ubigeo";			
			$cond = array();			
			if(isset($filtros["id_ubigeo"])) {$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);}
			if(isset($filtros["pais"])) {	$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);}
			if(isset($filtros["departamento"])) {$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);}
			if(isset($filtros["provincia"])) {	$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);	}
			if(isset($filtros["distrito"])) {$cond[] = "distrito = " . $this->oBD->escapar($filtros["distrito"]);}
			if(isset($filtros["ciudad"])) {	$cond[] = "ciudad  " . $this->oBD->like($filtros["ciudad"]);}			

			if(!empty($cond)) {	$sql .= " WHERE " . implode(' AND ', $cond);}			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {			
			$sql = "SELECT * FROM ubigeo";
			$cond = array();			
			if(isset($filtros["id_ubigeo"])) {	$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);	}
			if(isset($filtros["pais"])){
				$pais=$filtros["pais"];
				if($pais=='all'){
					$cond[]="(departamento='00' AND provincia='00' AND distrito='00')";
				}else{
					if(isset($filtros["departamento"])){
						$depa=$filtros["departamento"];
						if($depa=='all'){
							$cond[]="(departamento<>'00' AND provincia='00' AND distrito='00' AND pais='".$pais."')";	
						}else{
							if(isset($filtros["provincia"])){
								$pro=$filtros["provincia"];
								if($pro=='all'){
									$cond[]="(departamento='".$depa."' AND provincia<>'00' AND distrito='00' AND pais='".$pais."')";	
								}else{
									if(isset($filtros["distrito"])){
									  $dis=$filtros["distrito"];
									  if($dis=='all'){
									  	 $cond[]="(departamento='".$depa."' AND provincia='".$pro."' AND pais='".$pais."')";
									  }else{
									  	$cond[]="(departamento='".$depa."' AND provincia='".$pro."' AND distrito='".$dis."' AND pais='".$pais."')";
									  }
									}else
									$cond[]="(departamento='".$depa."' AND provincia='".$pro."' AND distrito<>'00' AND pais='".$pais."')";
								}
							}else
							$cond[]="(departamento='".$depa."' AND provincia<>'00' AND distrito='00' AND pais='".$pais."')";	
						}						
					}else
					$cond[]="(departamento<>'00' AND provincia='00' AND distrito='00' AND pais='".$pais."')";
				}
			}
			
			//if(isset($filtros["provincia"])) {	$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);	}
			//if(isset($filtros["distrito"])) {	$cond[] = "distrito =" . $this->oBD->escapar($filtros["distrito"]);		}
			if(isset($filtros["ciudad"])) {		$cond[] = "ciudad " . $this->oBD->like($filtros["ciudad"]);	}
			if(!empty($cond)) {		$sql .= " WHERE " . implode(' AND ', $cond); }
			$sql .=" ORDER BY ciudad ASC ";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM ubigeo";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function getciudad($filtros=null){
		try {
			$sql = "SELECT  id_ubigeo,ciudad  FROM ubigeo";

			if(isset($filtros["allpaises"])){
				$sql="select * from ubigeo WHERE departamento='00' and provincia='00' and distrito='00'";
			}elseif(isset($filtros["alldepartamentos"])){
				if(isset($filtros["pais"])) $cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
				
				$cond[] = "provincia = '00' AND departamento<>'00'";
				$cond[] = "distrito = '00'";					
			}elseif(isset($filtros["allprovincias"])){
				if(isset($filtros["pais"])) $cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
				$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);
				if(!isset($filtros["departamento"])) return null;
				$cond[] = "distrito = '00' and provincia<>'00' ";
			}elseif(isset($filtros["alldistrito"])){
				if(isset($filtros["pais"])) return null;
				$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
				if(isset($filtros["departamento"])) return null;
				$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);
				if(isset($filtros["provincia"])) return null;
				$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);
				$cond[] = "distrito<>'00'";					
			}else{
				if(isset($filtros["pais"])) return null;
				$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
				if(isset($filtros["departamento"])) return null;
				$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);
				if(isset($filtros["provincia"])) return null;
				$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);
				if(isset($filtros["distrito"])) return null;
				$cond[] = "distrito=" . $this->oBD->escapar($filtros["distrito"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}		
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($pais,$departamento,$provincia,$distrito,$ciudad)
	{
		try {
			
			$this->iniciarTransaccion('dat_ubigeo_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_ubigeo) FROM ubigeo");
			++$id;
			
			$estados = array('id_ubigeo' => $id							
							,'pais'=>$pais
							,'departamento'=>$departamento
							,'provincia'=>$provincia
							,'distrito'=>$distrito
							,'ciudad'=>$ciudad							
							);
			
			$this->oBD->insert('ubigeo', $estados);			
			$this->terminarTransaccion('dat_ubigeo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_ubigeo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $pais,$departamento,$provincia,$distrito,$ciudad)
	{
		try {
			$this->iniciarTransaccion('dat_ubigeo_update');
			$estados = array('pais'=>$pais
							,'departamento'=>$departamento
							,'provincia'=>$provincia
							,'distrito'=>$distrito
							,'ciudad'=>$ciudad								
							);
			
			$this->oBD->update('ubigeo ', $estados, array('id_ubigeo' => $id));
		    $this->terminarTransaccion('dat_ubigeo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM ubigeo  "
					. " WHERE id_ubigeo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('ubigeo', array('id_ubigeo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('ubigeo', array($propiedad => $valor), array('id_ubigeo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}	
}