<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatActividad_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM actividad_alumno";
			
			$cond = array();		
			
			if(!empty($filtros["idactalumno"])) {
				$cond[] = "idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}
			if(!empty($filtros["iddetalleactividad"])) {
				$cond[] = "iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}
			if(!empty($filtros["idalumno"])) {
				$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["fecha"])) {
				$cond[] = "fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["porcentajeprogreso"])) {
				$cond[] = "porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}	
			if(!empty($filtros["habilidades"])) {
				$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}	
			if(!empty($filtros["estado"])) {
				$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}	
			if(!empty($filtros["html_solucion"])) {
				$cond[] = "html_solucion = " . $this->oBD->escapar($filtros["html_solucion"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT AA.*,AD.titulo ,AD.tipo, AD.idactividad, A.sesion FROM actividad_alumno AA JOIN actividad_detalle AD ON AA.iddetalleactividad = AD.iddetalle JOIN actividades A ON A.idactividad = AD.idactividad ";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idactalumno"])) {
				$cond[] = "AA.idactalumno = " . $this->oBD->escapar($filtros["idactalumno"]);
			}
			if(!empty($filtros["iddetalleactividad"])) {
				$cond[] = "AA.iddetalleactividad = " . $this->oBD->escapar($filtros["iddetalleactividad"]);
			}
			if(!empty($filtros["idalumno"])) {
				$cond[] = "AA.idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["fecha"])) {
				$cond[] = "AA.fecha = " . $this->oBD->escapar($filtros["fecha"]);
			}
			if(!empty($filtros["porcentajeprogreso"])) {
				$cond[] = "AA.porcentajeprogreso = " . $this->oBD->escapar($filtros["porcentajeprogreso"]);
			}	
			if(!empty($filtros["habilidades"])) {
				$cond[] = "AA.habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}	
			if(!empty($filtros["estado"])) {
				$cond[] = "AA.estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["html_solucion"])) {
				$cond[] = "AA.html_solucion = " . $this->oBD->escapar($filtros["html_solucion"]);
			}
			if(!empty($filtros["idactividad"])) {
				$cond[] = "AD.idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["sesion"])) {
				$cond[] = "A.sesion = " . $this->oBD->escapar($filtros["sesion"]);
			}
			if(!empty($filtros["insesion"])) {
				$cond[] = "A.sesion IN ('".$filtros["insesion"]."')";
			}

			if(!empty($filtros["solooffline"])) {
				$cond[] = " AA.tipofile!='' ";
			}


			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql."\n";
			//echo $sql;
			//exit();
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	
	public function insertar($iddetalleactividad,$idalumno,$fecha,$porcentajeprogreso,$habilidades,$estado,$html_solucion,$tipofile,$file)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_insert');
			
			// $id = $this->oBD->consultarEscalarSQL("SELECT MAX(idactalumno) FROM actividad_alumno");
			// ++$id;
			
			$estados = array(
							// 'idactalumno'=>$id,
							'iddetalleactividad'=>$iddetalleactividad
							,'idalumno'=>$idalumno
							/*,'fecha'=>$fecha*/
							,'porcentajeprogreso'=>$porcentajeprogreso
							,'habilidades'=>$habilidades
							,'estado'=>$estado
							,'html_solucion'=>$html_solucion
							,'tipofile'=>$tipofile
							,'file'=>$file
							);
			$this->oBD->insert('actividad_alumno', $estados);
			$this->terminarTransaccion('dat_actividad_alumno_insert');
			return 0;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_actividad_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $iddetalleactividad,$idalumno,$fecha,$porcentajeprogreso,$habilidades,$estado,$html_solucion,$tipofile,$file)
	{
		try {
			$this->iniciarTransaccion('dat_actividad_alumno_update');
			$estados = array('iddetalleactividad'=>$iddetalleactividad
							,'idalumno'=>$idalumno
							/*,'fecha'=>$fecha*/
							,'porcentajeprogreso'=>$porcentajeprogreso
							,'habilidades'=>$habilidades
							,'estado'=>$estado
							,'html_solucion'=>$html_solucion
							,'tipofile'=>$tipofile
							,'file'=>$file
							);
			
			$this->oBD->update('actividad_alumno ', $estados, array('idactalumno' => $id));
		    $this->terminarTransaccion('dat_actividad_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno  "
					. " WHERE idactalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('actividad_alumno', array('idactalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('actividad_alumno', array($propiedad => $valor), array('' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
   
	public function ultimaActividad($idalumno)
	{
		try {
			$sql = "SELECT  *  FROM actividad_alumno WHERE idactalumno = (SELECT MAX(idactalumno) FROM actividad_alumno WHERE idalumno=".$this->oBD->escapar($idalumno)."  AND iddetalleactividad<>0 )";
			
			$res = $this->oBD->consultarSQL($sql);
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Actividad_alumno").": " . $e->getMessage());
		}
	}
	public function countprogreso($filtros = null){
		try {
			$sql = "SELECT pe.idpersona, 
	(SELECT cd.idcurso FROM actividad_alumno aa inner join actividad_detalle ad on ad.iddetalle = aa.iddetalleactividad inner join actividades a on a.idactividad = ad.idactividad inner join acad_cursodetalle cd on cd.idrecurso = a.unidad inner join acad_curso c on c.idcurso = cd.idcurso
	WHERE (aa.habilidades LIKE '%4%' OR aa.habilidades LIKE '%5%' OR aa.habilidades LIKE '%6%' OR aa.habilidades LIKE '%7%') AND aa.idalumno = pe.idpersona AND c.estado = 1 limit 1) AS idcurso,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.idalumno = pe.idpersona _fecha) AS total_L,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.idalumno = pe.idpersona _fecha) AS total_R,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.idalumno = pe.idpersona _fecha) AS total_W,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.idalumno = pe.idpersona _fecha) AS total_S, 
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%4%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_L_T,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%5%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_R_T,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%6%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_W_T,
	(SELECT COUNT(aa.idactalumno) FROM actividad_alumno aa WHERE aa.habilidades LIKE '%7%' AND aa.estado = 'T' AND aa.idalumno = pe.idpersona _fecha) AS total_S_T
	FROM personal pe";
			
			$cond = array();

			//AND aa.fecha <= '2018-10-20 18:28:44'
			
			if(isset($filtros['fecha'])){
				$sql = str_replace('_fecha', "AND aa.fecha <= ".$this->oBD->escapar($filtros['fecha']) , $sql);
			}else{
				$sql = str_replace('_fecha', '' , $sql);
			}

			if(isset($filtros['idpersona'])){
				$cond[] = "pe.idpersona = ".$this->oBD->escapar($filtros['idpersona']);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	

			// echo $sql."<br><br>";
			
			return $this->oBD->consultarSQL($sql);
		} catch (Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}


	}
	public function actividadestotal($filtros = null){
		try{
			
			$sql="SELECT count(*) AS total FROM actividad_detalle ad INNER JOIN (SELECT a.idactividad FROM actividades a INNER JOIN acad_cursodetalle cd ON cd.idrecurso = a.sesion WHERE @curso a.metodologia >= 2  and cd.tiporecurso = 'L' _xunidad ) t ON ad.idactividad = t.idactividad";

			$cond = array();			
			
			if(isset($filtros['idcurso'])){
				$sql = str_replace('@curso', "cd.idcurso = ".$this->oBD->escapar($filtros['idcurso'])." AND" , $sql);
			}else{
				$sql = str_replace('@curso', '' , $sql);
			}
			if(isset($filtros['xunidad'])){
				if(isset($filtros['idrecurso']) && isset($filtros['idcurso'])){
					$sql = str_replace('_xunidad', "AND cd.idpadre = (SELECT acd.idcursodetalle FROM acad_cursodetalle acd WHERE acd.idrecurso = ".$this->oBD->escapar($filtros['idrecurso'])." and acd.idcurso = ".$this->oBD->escapar($filtros['idcurso'])." and acd.tiporecurso = 'U' LIMIT 1)", $sql);
				}else{
					$sql = str_replace('_xunidad',' ', $sql);
				}//end if idrecurso and idcurso
			}else{
				$sql = str_replace('_xunidad',' ', $sql);
			}

			if(isset($filtros['listen'])){
				$cond[] = "ad.idhabilidad LIKE '%4%'";
			}
			if(isset($filtros['read'])){
				$cond[] = "ad.idhabilidad LIKE '%5%'";
			}
			if(isset($filtros['write'])){
				$cond[] = "ad.idhabilidad LIKE '%6%'";
			}
			if(isset($filtros['speak'])){
				$cond[] = "ad.idhabilidad LIKE '%7%'";
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}	
			
			
			//echo $sql."<br><br>";

			
			return $this->oBD->consultarSQL($sql);
		}catch (Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}
}