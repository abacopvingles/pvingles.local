<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		17-04-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatLibre_tema extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM libre_tema";
			
			$cond = array();		
			
			if(isset($filtros["idtema"])) {
				$cond[] = "idtema = " . $this->oBD->escapar($filtros["idtema"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
				$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["idtipo"])) {
				$cond[] = "idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}

	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT TE.*, TI.nombre AS tipo_nombre, TI.tipo_contenido FROM libre_tema TE JOIN libre_tipo TI ON TE.idtipo=TI.idtipo ";			
			
			$cond = array();		
			
			if(isset($filtros["idtema"])) {
				$cond[] = "TE.idtema = " . $this->oBD->escapar($filtros["idtema"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "TE.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
				$cond[] = "TE.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["idtipo"])) {
				$cond[] = "TE.idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if(isset($filtros["tipo_contenido"])) {
				$cond[] = "TI.tipo_contenido = " . $this->oBD->escapar($filtros["tipo_contenido"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY idtema ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
	public function buscarvocabulario($filtros=null)
	{
		try {
			$sql = "SELECT TE.*, TI.nombre AS tipo_nombre, TI.tipo_contenido FROM libre_tema TE JOIN libre_tipo TI ON TE.idtipo=TI.idtipo ";			
			
			$cond = array();		
			
			if(isset($filtros["idtema"])) {
				$cond[] = "TE.idtema = " . $this->oBD->escapar($filtros["idtema"]);
			}
			if(isset($filtros["nombre"])) {
				$cond[] = "TE.nombre LIKE '" . $filtros["nombre"]."%'";
				// $cond[] = "TE.nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["descripcion"])) {
				$cond[] = "TE.descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(isset($filtros["idtipo"])) {
				$cond[] = "TE.idtipo = " . $this->oBD->escapar($filtros["idtipo"]);
			}
			if(isset($filtros["tipo_contenido"])) {
				$cond[] = "TI.tipo_contenido = " . $this->oBD->escapar($filtros["tipo_contenido"]);
			}
			if(isset($filtros["idcurso"])  || $filtros['idcurso']==0 ) {
				$cond[] = "TE.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY idtema ASC";
			// var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
	public function insertar($nombre,$descripcion,$idtipo)
	{
		try {
			
			$this->iniciarTransaccion('dat_libre_tema_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtema) FROM libre_tema");
			++$id;
			
			$estados = array('idtema' => $id
							
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'idtipo'=>$idtipo							
							);
			
			$this->oBD->insert('libre_tema', $estados);			
			$this->terminarTransaccion('dat_libre_tema_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_libre_tema_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
	
	public function actualizar($id, $nombre,$descripcion,$idtipo,$idcurso)
	{
		try {
			// var_dump($id, $nombre,$descripcion,$idtipo,$idcurso);
			$this->iniciarTransaccion('dat_libre_tema_update');
			$estados = array('nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'idtipo'=>$idtipo
							,'origen'=>null
							,'idcurso'=>$idcurso								
							);
			
			$this->oBD->update('libre_tema ', $estados, array('idtema' => $id));
		    $this->terminarTransaccion('dat_libre_tema_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.nombre AS _nombre  FROM libre_tema tb1 LEFT JOIN libre_tipo tb2 ON tb1.idtipo=tb2.idtipo  "
					. " WHERE tb1.idtema = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('libre_tema', array('idtema' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('libre_tema', array($propiedad => $valor), array('idtema' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Libre_tema").": " . $e->getMessage());
		}
	}
   
		
}