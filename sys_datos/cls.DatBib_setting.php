<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_setting extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_setting";
			
			$cond = array();		
			
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}
			if(!empty($filtros["cantidad_descarga"])) {
					$cond[] = "cantidad_descarga = " . $this->oBD->escapar($filtros["cantidad_descarga"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_setting";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_setting"])) {
					$cond[] = "id_setting = " . $this->oBD->escapar($filtros["id_setting"]);
			}
			if(!empty($filtros["cantidad_descarga"])) {
					$cond[] = "cantidad_descarga = " . $this->oBD->escapar($filtros["cantidad_descarga"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_setting  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	
	public function insertar($cantidad_descarga)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_setting_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_setting) FROM bib_setting");
			++$id;
			
			$estados = array('id_setting' => $id
							
							,'cantidad_descarga'=>$cantidad_descarga							
							);
			
			$this->oBD->insert('bib_setting', $estados);			
			$this->terminarTransaccion('dat_bib_setting_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_setting_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $cantidad_descarga)
	{
		try {
			$this->iniciarTransaccion('dat_bib_setting_update');
			$estados = array('cantidad_descarga'=>$cantidad_descarga								
							);
			
			$this->oBD->update('bib_setting ', $estados, array('id_setting' => $id));
		    $this->terminarTransaccion('dat_bib_setting_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_setting  "
					. " WHERE id_setting = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_setting', array('id_setting' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_setting', array($propiedad => $valor), array('id_setting' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_setting").": " . $e->getMessage());
		}
	}
   
		
}