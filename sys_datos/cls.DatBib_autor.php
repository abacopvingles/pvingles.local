<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_autor extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_autor";
			
			$cond = array();		
			
			if(!empty($filtros["id_autor"])) {
					$cond[] = "id_autor = " . $this->oBD->escapar($filtros["id_autor"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ap_paterno"])) {
					$cond[] = "ap_paterno = " . $this->oBD->escapar($filtros["ap_paterno"]);
			}
			if(!empty($filtros["ap_materno"])) {
					$cond[] = "ap_materno = " . $this->oBD->escapar($filtros["ap_materno"]);
			}
			if(!empty($filtros["id_pais"])) {
					$cond[] = "id_pais = " . $this->oBD->escapar($filtros["id_pais"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_autor";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_autor"])) {
					$cond[] = "id_autor = " . $this->oBD->escapar($filtros["id_autor"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ap_paterno"])) {
					$cond[] = "ap_paterno = " . $this->oBD->escapar($filtros["ap_paterno"]);
			}
			if(!empty($filtros["ap_materno"])) {
					$cond[] = "ap_materno = " . $this->oBD->escapar($filtros["ap_materno"]);
			}
			if(!empty($filtros["id_pais"])) {
					$cond[] = "id_pais = " . $this->oBD->escapar($filtros["id_pais"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_autor  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$ap_paterno,$ap_materno,$id_pais)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_autor_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_autor) FROM bib_autor");
			++$id;
			
			$estados = array('id_autor' => $id
							
							,'nombre'=>$nombre
							,'ap_paterno'=>$ap_paterno
							,'ap_materno'=>$ap_materno
							,'id_pais'=>$id_pais							
							);
			
			$this->oBD->insert('bib_autor', $estados);			
			$this->terminarTransaccion('dat_bib_autor_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_autor_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$ap_paterno,$ap_materno,$id_pais)
	{
		try {
			$this->iniciarTransaccion('dat_bib_autor_update');
			$estados = array('nombre'=>$nombre
							,'ap_paterno'=>$ap_paterno
							,'ap_materno'=>$ap_materno
							,'id_pais'=>$id_pais								
							);
			
			$this->oBD->update('bib_autor ', $estados, array('id_autor' => $id));
		    $this->terminarTransaccion('dat_bib_autor_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_autor  "
					. " WHERE id_autor = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_autor', array('id_autor' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_autor', array($propiedad => $valor), array('id_autor' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_autor").": " . $e->getMessage());
		}
	}
   
		
}