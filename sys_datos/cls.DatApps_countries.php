<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatApps_countries extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM apps_countries";
			
			$cond = array();		
			
			if(!empty($filtros["id_pais"])) {
					$cond[] = "id_pais = " . $this->oBD->escapar($filtros["id_pais"]);
			}
			if(!empty($filtros["country_code"])) {
					$cond[] = "country_code = " . $this->oBD->escapar($filtros["country_code"]);
			}
			if(!empty($filtros["country_name"])) {
					$cond[] = "country_name = " . $this->oBD->escapar($filtros["country_name"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM apps_countries";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_pais"])) {
					$cond[] = "id_pais = " . $this->oBD->escapar($filtros["id_pais"]);
			}
			if(!empty($filtros["country_code"])) {
					$cond[] = "country_code = " . $this->oBD->escapar($filtros["country_code"]);
			}
			if(!empty($filtros["country_name"])) {
					$cond[] = "country_name = " . $this->oBD->escapar($filtros["country_name"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "select * from apps_countries ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	
	public function insertar($country_code,$country_name)
	{
		try {
			
			$this->iniciarTransaccion('dat_apps_countries_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_pais) FROM apps_countries");
			++$id;
			
			$estados = array('id_pais' => $id
							
							,'country_code'=>$country_code
							,'country_name'=>$country_name							
							);
			
			$this->oBD->insert('apps_countries', $estados);			
			$this->terminarTransaccion('dat_apps_countries_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_apps_countries_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $country_code,$country_name)
	{
		try {
			$this->iniciarTransaccion('dat_apps_countries_update');
			$estados = array('country_code'=>$country_code
							,'country_name'=>$country_name								
							);
			
			$this->oBD->update('apps_countries ', $estados, array('id_pais' => $id));
		    $this->terminarTransaccion('dat_apps_countries_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM apps_countries  "
					. " WHERE id_pais = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('apps_countries', array('id_pais' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('apps_countries', array($propiedad => $valor), array('id_pais' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Apps_countries").": " . $e->getMessage());
		}
	}
   
		
}