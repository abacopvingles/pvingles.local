<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-09-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatPersona_setting extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_setting";
			
			$cond = array();		
			
			if(isset($filtros["idpersonasetting"])) {
					$cond[] = "idpersonasetting = " . $this->oBD->escapar($filtros["idpersonasetting"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["datos"])) {
					$cond[] = "datos = " . $this->oBD->escapar($filtros["datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM persona_setting";			
			
			$cond = array();		
					
			
			if(isset($filtros["idpersonasetting"])) {
					$cond[] = "idpersonasetting = " . $this->oBD->escapar($filtros["idpersonasetting"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["datos"])) {
					$cond[] = "datos = " . $this->oBD->escapar($filtros["datos"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idproyecto,$idpersona,$datos)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_setting_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idpersonasetting) FROM persona_setting");
			++$id;
			
			$estados = array('idpersonasetting' => $id
							
							,'idproyecto'=>$idproyecto
							,'idpersona'=>$idpersona
							,'datos'=>$datos							
							);
			
			$this->oBD->insert('persona_setting', $estados);			
			$this->terminarTransaccion('dat_persona_setting_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_setting_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idproyecto,$idpersona,$datos)
	{
		try {
			$this->iniciarTransaccion('dat_persona_setting_update');
			$estados = array('idproyecto'=>$idproyecto
							,'idpersona'=>$idpersona
							,'datos'=>$datos								
							);
			
			$this->oBD->update('persona_setting ', $estados, array('idpersonasetting' => $id));
		    $this->terminarTransaccion('dat_persona_setting_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM persona_setting  "
					. " WHERE idpersonasetting = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_setting', array('idpersonasetting' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_setting', array($propiedad => $valor), array('idpersonasetting' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_setting").": " . $e->getMessage());
		}
	}
   
		
}