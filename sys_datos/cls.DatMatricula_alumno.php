<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-04-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatMatricula_alumno extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM matricula_alumno";
			
			$cond = array();		
			
			if(!empty($filtros["idmatriculaalumno"])) {
					$cond[] = "idmatriculaalumno = " . $this->oBD->escapar($filtros["idmatriculaalumno"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = "fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["fechacaduca"])) {
					$cond[] = "fechacaduca = " . $this->oBD->escapar($filtros["fechacaduca"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM matricula_alumno";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idmatriculaalumno"])) {
					$cond[] = "idmatriculaalumno = " . $this->oBD->escapar($filtros["idmatriculaalumno"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = "codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = "fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["fechacaduca"])) {
					$cond[] = "fechacaduca = " . $this->oBD->escapar($filtros["fechacaduca"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM matricula_alumno  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	
	public function insertar($idalumno,$codigo,$tipo,$orden,$fechamatricula,$fechacaduca,$regusuario,$fecharegistro)
	{
		try {
			
			$this->iniciarTransaccion('dat_matricula_alumno_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatriculaalumno) FROM matricula_alumno");
			++$id;
			
			$estados = array('idmatriculaalumno' => $id
							
							,'idalumno'=>$idalumno
							,'codigo'=>$codigo
							,'tipo'=>$tipo
							,'orden'=>$orden
							,'fechamatricula'=>$fechamatricula
							,'fechacaduca'=>$fechacaduca
							,'regusuario'=>$regusuario
							,'fecharegistro'=>$fecharegistro							
							);
			
			$this->oBD->insert('matricula_alumno', $estados);			
			$this->terminarTransaccion('dat_matricula_alumno_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_matricula_alumno_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idalumno,$codigo,$tipo,$orden,$fechamatricula,$fechacaduca,$regusuario,$fecharegistro)
	{
		try {
			$this->iniciarTransaccion('dat_matricula_alumno_update');
			$estados = array('idalumno'=>$idalumno
							,'codigo'=>$codigo
							,'tipo'=>$tipo
							,'orden'=>$orden
							,'fechamatricula'=>$fechamatricula
							,'fechacaduca'=>$fechacaduca
							,'regusuario'=>$regusuario
							,'fecharegistro'=>$fecharegistro								
							);
			
			$this->oBD->update('matricula_alumno ', $estados, array('idmatriculaalumno' => $id));
		    $this->terminarTransaccion('dat_matricula_alumno_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM matricula_alumno  "
					. " WHERE idmatriculaalumno = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('matricula_alumno', array('idmatriculaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('matricula_alumno', array($propiedad => $valor), array('idmatriculaalumno' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Matricula_alumno").": " . $e->getMessage());
		}
	}
   

    public function auto_mejora($filtros=null)
	{
		try {
			$sql = "SELECT  DMA.*, AD.idhabilidad, AD.idactividad, ( SELECT  metodologia  FROM actividades A  WHERE A.idactividad=AD.idactividad ) AS metodologia, MA.idalumno  FROM matricula_alumno MA INNER JOIN detalle_matricula_alumno DMA ON DMA.idmatricula=MA.idmatriculaalumno INNER JOIN actividad_detalle AD     ON AD.iddetalle=DMA.idejercicio ";

			$cond = array();
			$alias_table=' MA';
			if(!empty($filtros["idmatriculaalumno"])) {
					$cond[] = $alias_table.".idmatriculaalumno = " . $this->oBD->escapar($filtros["idmatriculaalumno"]);
			}
			if(!empty($filtros["idalumno"])) {
					$cond[] = $alias_table.".idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(!empty($filtros["codigo"])) {
					$cond[] = $alias_table.".codigo = " . $this->oBD->escapar($filtros["codigo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = $alias_table.".tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["orden"])) {
					$cond[] = $alias_table.".orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(!empty($filtros["fechamatricula"])) {
					$cond[] = $alias_table.".fechamatricula = " . $this->oBD->escapar($filtros["fechamatricula"]);
			}
			if(!empty($filtros["fechacaduca"])) {
					$cond[] = $alias_table.".fechacaduca = " . $this->oBD->escapar($filtros["fechacaduca"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = $alias_table.".regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["fecharegistro"])) {
					$cond[] = $alias_table.".fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$sql .= " ORDER BY DMA.orden ASC";
			$res = $this->oBD->consultarSQL($sql);		
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Detalle_matricula_alumno").": " . $e->getMessage());
		}
	}
		
}