<?php
	require_once("class/class_evaluacion.php");
	require_once("class/class_ajustes.php");
	require_once("class/class_mantenimiento.php");
	require_once("class/class_respuestas.php");
	require_once("class/class_reporte.php");

	session_start();
	$id_encuesta = $_GET["id_encuesta"];
	$id_usuario = $_GET["id_usuario"];
	$acc = $_GET["accion"];

	$obj = new Ajuste();
	$obj1 = new Evaluacion();
	$obj2 = new Mantenimiento();
	$obj3 = new Respuestas();
	$obj4 = new Reporte();

	$ajuste = $obj->listar_idencuesta($id_encuesta);
	$encuesta = $obj1->listar_evaluacion($id_encuesta);
	$reporte= $obj4->listar_resultado_por_usuario($id_encuesta,$id_usuario);
	
	$x=json_decode($reporte[0]["cadena_respuestas"],true);
	//var_dump($x);

	
	
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
    <!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">
</head>

<style>
	::-webkit-input-placeholder {
   font-style: italic;
	}
	:-moz-placeholder {
	   font-style: italic;  
	}
	::-moz-placeholder {
	   font-style: italic;  
	}
	:-ms-input-placeholder {  
	   font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
	    background-color: #fff;
	}
</style>

<body>

<div class = 'container-fluid'>
		
		<div class = 'row'>
		    <div class = 'col-lg-12'>
			    <?php
			       require_once("menu.php");
			    ?>
		    </div>
		</div>

		<div class = 'row' style="margin-top: 60px;">

			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>

			<!--<form action = '#' method="post">-->
			<?php $cant_preg = count($encuesta);?>

				<input type = 'hidden' name = 'tipo_encuesta' value = '<?php echo $ajuste[0]["tipo"]; ?>'>
				<input type = 'hidden' name = 'id_encuesta' class = 'id_encuesta' value = '<?php echo $_GET["id_encuesta"];?>'>
				<input type = 'hidden' name = 'id_usuario' class = 'id_usuario' value = '<?php echo $_SESSION["id_usuario"];?>'>
				<input type = 'hidden' name = 'cant_preguntas' class = 'cant_preguntas' value = ''>

				<div class = 'panel panel-primary'>
					<div class = 'panel-heading'>
						Detalle Encuesta
					</div>
					<div class = 'panel-body'>

					<?php
					if($ajuste[0]["tipo"]=="rubrica")
					{
						?>
						<div class = 'row'>
							<div class = 'col-lg-12 text-center'>
								<h4>RESULTADO GENERAL</h4>
							</div>
						</div>
						<div class = 'row'>
						<div class = 'col-lg-2'></div>
						<div class = 'col-lg-8'>
							<table class = 'table table-striped table-condensed table-hover display'>
								<thead>
									<tr style = 'background-color:#428bca; color: #fff;'>
										<th>Puntaje máximo</th>
										<th>Puntaje Obtenido</th>
										<th>Porcentaje Obtenido</th>
										<th>Escala</th>
									</tr>
								</thead>
							<tbody>
							<?php
							foreach($x as $valor)
							{
								$puntaje_maximo = $valor["puntaje_maximo"];
								$puntaje_obtenido = $valor["puntaje_obtenido"];
								$porcentaje_obtenido = round(($puntaje_obtenido*100)/$puntaje_maximo);
								echo "<tr>
									<td>".$puntaje_maximo."</td>
									<td>".$puntaje_obtenido."</td>
									<td>".$porcentaje_obtenido."%</td>";
									foreach($ajuste as $punt)
									{
										$cadena_puntuacion = json_decode($punt["cadena_puntuacion"],true);
										foreach($cadena_puntuacion as $key=>$value)
										{
											if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
											{
												$nombre_escala = $key;
											}
										}
									}
									echo "<td>".$nombre_escala."</td>";
								echo "
								</tr>";
								
								$dimension = $valor["dimensiones"];
								
							}

							?>
							</tbody>
						</table>
						</div>
						<div class = 'col-lg-2'></div>
						</div>
					<div class = 'row'>
						<div class = 'col-lg-12 text-center'>
							<h4>RESULTADO POR DIMENSIONES</h4>
						</div>
					
					<div class = 'col-lg-12'>
						<?php
							$cont_d=0;
							$cont_est1=0;
							foreach($dimension as $key=>$value)
							{
								$cont_d++;
								$dim1 = $obj2->listar_id_dimension($key);
								$nombre_dim = $dim1[0]["nombre"];
								
								$puntaje_maximo = $value["puntaje_maximo"];
								$puntaje_obtenido = $value["puntaje_obtenido"];
								$porcentaje_obtenido = round(($puntaje_obtenido*100)/$puntaje_maximo);
								
								$estandar = $value["estandar"];

						?>
							<div class = 'row'>
							<div class = 'col-lg-2'></div>
							<div class = 'col-lg-8'>
							<?php
							echo "<div style = 'padding:5px;background: #f39b1e; color: #fff;font-weight:bold;'>Dimensión ".$cont_d.": ".$nombre_dim."</div>";
							?>
							<table class = 'table table-striped table-condensed table-hover display'>
								<thead>
									<tr style = 'background-color:#428bca; color: #fff;'>
										<th>Puntaje máximo</th>
										<th>Puntaje Obtenido</th>
										<th>Porcentaje Obtenido</th>
										<th>Escala</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $puntaje_maximo;?></td>
										<td><?php echo $puntaje_obtenido;?></td>
										<td><?php echo $porcentaje_obtenido."%";?></td>
										<?php
										foreach($ajuste as $punt)
										{
											$cadena_puntuacion = json_decode($punt["cadena_puntuacion"],true);
											foreach($cadena_puntuacion as $key=>$value)
											{
												if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
												{
													$nombre_escala = $key;
												}
											}
										}
										?>
										<td><?php echo $nombre_escala;?></td>
									</tr>
								</tbody>
							</table>
							</div>
							<div class = 'col-lg-2'></div>
							</div>
						<?php
							
							$let = 64+$cant_estandar;
						?>
						
						<?

							foreach($estandar as $key1=>$value1)
							{
								$est1= $obj2->listar_id_estandar($key1);
								$nombre_est = $est1[0]["nombre"];
								echo "<div class = 'row'><div class = 'col-lg-2'></div>";
								echo "<div class = 'col-lg-8'>";
									$cont_est1++;
									$let1 = 64+$cont_est1;
									
									$letra1 = strtoupper(chr($let1));
									
									echo "<div style = 'padding:5px;background: #e8e8e7; color: #40403f;font-weight:bold;'>ESTÁNDAR ".$letra1.": ".$nombre_est."</div>";

									$puntaje_max_est = $value1["puntaje_maximo"];
									$puntaje_obt_est = $value1["puntaje_obtenido"];
									$porcentaje_obtenido = round(($puntaje_obt_est*100)/$puntaje_max_est);
									?>
									<table class = 'table table-striped table-condensed table-hover display'>
										<thead>
											<tr style = 'background-color:#428bca; color: #fff;'>
												<th>Puntaje máximo</th>
												<th>Puntaje Obtenido</th>
												<th>Porcentaje Obtenido</th>
												<th>Escala</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><?php echo $puntaje_max_est;?></td>
												<td><?php echo $puntaje_obt_est;?></td>
												<td><?php echo $porcentaje_obtenido."%";?></td>
												<?php
												foreach($ajuste as $punt)
												{
													$cadena_puntuacion = json_decode($punt["cadena_puntuacion"],true);
													foreach($cadena_puntuacion as $key=>$value)
													{
														if($value["rango_inicial"]<=$porcentaje_obtenido && $porcentaje_obtenido <= $value["rango_final"])
														{
															$nombre_escala_est = $key;
														}
													}
												}
												?>
												<td><?php echo $nombre_escala_est;?></td>
											</tr>
										</tbody>
									</table>
									<?php
								echo "</div>";
								echo "<div class = 'col-lg-2'></div>
								</div>";
							}
							echo "<hr>";
						}
							?>
						
						</div>
					</div>
						<?php
					}
					else
					{
						?>
						<div class = 'row'>
							<div class = 'col-lg-12 text-center'>
								<h4>RESULTADO GENERAL</h4>
							</div>
						</div>
						<div class = 'row'>
						<div class = 'col-lg-2'></div>
						<div class = 'col-lg-8'>
							<table class = 'table table-striped table-condensed table-hover display'>
								<thead>
									<tr style = 'background-color:#428bca; color: #fff;'>
										<th>Puntaje máximo</th>
										<th>Puntaje Obtenido</th>
										<th>Porcentaje Obtenido</th>
										<th>Escala</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>53</td>
										<td>17</td>
										<td>32</td>
										<td>Bu00e1sico</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class = 'col-lg-2'></div>
						</div>
						<?php
					} //fin personalizado
					?>
						<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_encuesta"];?>'>

						<div class = 'contenedor_preguntas'>

							<!--<div class = 'muestra'>-->		
						<?php
									
						if(!empty($encuesta) && $ajuste[0]["tipo"]=="rubrica")
						{

						?>

						<div class = 'table-responsive'>
							<table class='table table-striped' id='tabla_dimensiones' cellpadding="4">
							<?php
							if(!empty($encuesta) && $ajuste[0]["tipo"]=="rubrica")
							{

								$cont=0;
								$cant_dimension=0;
								$cant_estandar = 0;
								$cad_encuesta = json_decode($encuesta[0]["cadena_encuesta"],true);
								$cant_niveles = $encuesta[0]["cant_niveles"];
								$colspan = $cant_niveles+2;

								echo "<tr class = 'cabecera'>
										<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'><h4>DIMENSIONES</h4></td>
										<td width = '10%' colspan = '".$cant_niveles."' style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'><dt>NIVELES</dt></td>
									</tr>
									<tr class = 'por_nivel'>";
									for($i=1;$i<=$cant_niveles;$i++)
									{
										echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$i."</h5></td>";
									}
									echo "</tr>";

									foreach($cad_encuesta as $key=>$value)
									{
										$dimension = $obj2->listar_id_dimension($key);
										$cant_dimension++;
										$let = 64+$cant_estandar;

										echo "<tr id_dimension='".$key."' style = 'padding: 10px;' class = 'fila_dimension'>
												<td colspan = '$colspan' class = 'td_dimension' style = 'background:#fb8c00;color:#fff;'><dt>DIMENSIÓN ".$cant_dimension.":".strtoupper($dimension[0]["nombre"])."</dt>
												</td>
											</tr>";

										foreach($value as $prim=>$sec)
										{
											$estandar = $obj2->listar_id_estandar($prim);
													
											$let++;
											$cant_estandar++;
											$letra = strtoupper(chr($let));
													
											echo "<tr id_estandar = '".$estandar[0]["id_estandar"]."' class = 'fila_estandar d_".$estandar[0]["id_dimension"]."' >
													<td class = 'td_estandar' colspan = '$colspan' style = 'background:#e8e8e7;'>
														<dt>".$letra.") ".$estandar[0]["nombre"]."</dt></td>
												</tr>";

												foreach($sec as $prim1)
												{
													$pregunta = $obj2->listar_id_pregunta($prim1);
													$cont++;
													echo "<tr id_pregunta = '".$pregunta[0]["id_pregunta"]."' class = 'fila_pregunta e_".$estandar[0]["id_estandar"]."'>
															<input type = 'hidden' name = 'pregunta_".$cont."' value = '".$pregunta[0]["id_pregunta"]."'>
															<td style = 'background: #e0e8ef;'>".$cont.".-</td>
															<td style = 'background: #fcfce0;'>".$pregunta[0]["nombre"]."</td>";

													for($i=1;$i<=$cant_niveles;$i++)
													{
														echo "<td class = 'text-center option_radio' orden_pregunta = '".$cont."' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center item_".$cont."'><input type = 'radio' name = 'item_".$cont."' value = '".$i."'></td>";
													}
													echo "</tr>";
													}
												}
											}
										}
										?>
							</table>
						</div>

						<?php
						}
						?>
						<!--</div>-->
						<?php

						/***************************PERSONALIZADO*****************************/
						/*$verificar_dim = $obj3->verificar_dimension($id_encuesta);
						if(!empty($verificar_dim))
						{
							$obj3->insertar_dim_temporal($id_encuesta);
						} 
						else
						{
							echo "ya está insertado";
						}*/
							$cont1=0;
							echo "<input type = 'hidden' name = 'cant_item' class = 'cant_item'>";
							if(!empty($encuesta) && $ajuste[0]["tipo"]=="personalizado")
							{
								for($i=0;$i<sizeof($encuesta);$i++)
								{
									$cont1++;
									$tipo_pregunta = $encuesta[$i]["tipo_pregunta"];
									

									?>
									<div class = 'muestra' tipo_pregunta = <?php echo $tipo_pregunta;?> cant_niveles = <?php echo $encuesta[$i]["cant_niveles"];?>>
										<div class = 'pregunta_padre'>
										<?php
										echo "<input type = 'hidden' class = 'tipo_pregunta' name = 'tipo_pregunta_".$cont1."' value = '".$tipo_pregunta."'>

										<input type = 'hidden' class = 'cant_dim_item_".$cont1."' name = 'cant_dim_item_".$cont1."'>";
										?>
				    						<div class = 'row'>
				    							<?php
				    							if($tipo_pregunta=="ponderacion_estandar" || $tipo_pregunta=="si_no")
				    							{
				    								$titulo = $encuesta[$i]["titulo"];
				    							
				    							?>
					    							<div class = 'col-lg-11 titulo_descripcion'>
					    								<h4><?php echo $cont1.".- ".$encuesta[$i]["titulo"];?></h4>
					    								<h3><?php echo $encuesta[$i]["descripcion"];?></h3>
					    							</div>
												
												<?php
												}
												else if($tipo_pregunta=="alternativas" || $tipo_pregunta=="autocompletar")
												{
												?>
													<div class = 'col-lg-11 titulo_descripcion'>
														<h4><?php echo $cont1;?>.- </h4>
													</div>
												</div>
												<div class = 'row'>
													<div class = 'col-lg-11'>
														<div class = 'escriba_titulo' style = 'padding: 10px;background:#fb8c00;color:#fff;'>
							    							<?php echo "DIMENSIÓN: <span>".$encuesta[$i]["titulo"]."</span>";?>
							    						</div>
							    						<div class = 'escriba_descripcion' style = 'background:#e8e8e7;padding:5px;'>
							    							<?php echo "ESTÁNDAR:  <span>".$encuesta[$i]["descripcion"]."</span>";?>
							    						</div>
							    						<?php
							    							if($tipo_pregunta=="alternativas")
							    							{
							    								$y=json_decode($encuesta[$i]["cadena_encuesta"],true);
							   									$pregunta = $y["pregunta"];
							   								}
							   								else
							   								{
						    									$pregunta = $encuesta[$i]["cadena_encuesta"];
						    								}
						    							?>
						    								<div class = 'escriba_pregunta' style = 'background:#fcfce0;padding:5px;'><dt>		<?php echo $pregunta;?></dt>
							    							</div>
							    						<br>
							    					</div>
						    					</div>
												<?php	
												}
												?>
											
											<div class = 'row'>
												<div class = 'col-lg-11 ejercicio'>
													<?php 
													if($tipo_pregunta=="ponderacion_estandar" || $tipo_pregunta=="si_no")
													{
														$cant_niveles = $encuesta[$i]["cant_niveles"];

														$colspan = $cant_niveles+2;
														if($tipo_pregunta == "ponderacion_estandar")
														{	$tit="NIVELES";$opc=$cant_niveles;}
														else 
														{	$tit="OPCION";$opc=array("SI","NO");}
													?>
													<table class = 'table table-bordered sortableTable responsive-table'>
														<tr class = 'cabecera'>
															<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'>
																<h4>DIMENSIONES</h4>
															</td>
															<td width = '10%' colspan = <?php echo $cant_niveles; ?> style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'>
																<dt><?php echo $tit;?></dt>
															</td>
														</tr>
														<tr class = 'por_nivel'>
														<?php
															if($tipo_pregunta == "ponderacion_estandar")
															{
																for($a=1;$a<=$opc;$a++)
																{
																	echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$a."</h5></td>";
																}
															}
															else
															{
																for($a=0;$a<sizeof($opc);$a++)
																{
																	echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$opc[$a]."</h5></td>";
																}
															}
															?>
														</tr>

														<?php
															$cant_est=0;
															$cont_est=64+$cant_estandar;
															$cont_pregunta = 0;
															$x=json_decode($encuesta[$i]["cadena_encuesta"],true);
															$cant_dimension=0;

															foreach($x as $dimension)
															{
																$cant_dimension++;

																$nombre = $dimension["nombre"];

																echo "<tr style = 'padding: 10px;' class = 'fila_dimension' clase_dimension = '".$cant_dimension."'>
																		<td colspan = '$colspan' class = 'td_dimension' style = 'background:#fb8c00;color:#fff;'>
																			<dt>
																			<input type = 'hidden' name = 'dim_".$cant_dimension."' value = '".$nombre."' style = 'color:#000;'>
																			DIMENSIÓN ".$cant_dimension.":
																				<div style = 'display:inline;' class = 'reemplazo_dimension' nombre_dim = '".$nombre."'>".strtoupper($nombre)."
																				</div>
																			</dt>
																			<input type = 'hidden' name = 'item_".$cont1."_cant_est_dim_".$cant_dimension."' class = 'cant_est_dim_".$cant_dimension."' style = 'color:#000;'>
																		</td>

																	</tr>";

																$est = $dimension["estandar"];

																foreach($est as $estandar)
																{
																	$cant_est++;
																	$cont_est++;
																	$letra = strtoupper(chr($cont_est));
																	echo "<tr class = 'fila_estandar d_".$cant_dimension."' clase_estandar='".$cant_est."'>
																			<td colspan = '$colspan' style = 'background:#e8e8e7;'>
																				<dt>".$letra.") 
																				<div style = 'display:inline;' class = 'reemplazo_estandar' nombre_est = '".$$estandar["nombre"]."'>".$estandar["nombre"]."
																				</div>
																				</dt>
																				<input type = 'hidden' class = 'cant_preg_est_".$cant_est."' name = 'item_".$cont1."_cant_preg_est_".$cant_est."'>
																			</td>
																		</tr>";

																		$preg=$estandar["pregunta"];
																		//var_dump($preg);
																	for($c=0;$c<sizeof($preg);$c++)
																	{
																		$cont_pregunta++;
																		echo "<tr class = 'fila_pregunta d_".$cant_dimension." e_".$cant_est."' orden_item = '".$cont1."' orden_pregunta=".$cont_pregunta.">
																				<td width = '5%' style = 'background: #e0e8ef;'>".$cont_pregunta.".-
																				</td>
																				<td style = 'background: #fcfce0;'>

																					<input type = 'hidden' name = 'item_".$cont1."_cant_est_dim_".$cant_dimension."' class = 'item_".$cont1."_cant_est_dim_".$cant_dimension."_fila_pregunta_".$cont_pregunta."' style = 'color:#000;'>

																					<div style = 'display:inline;' class = 'reemplazo_pregunta' nombre_preg = '".$preg[$c]."'>"
																							.$preg[$c]."
																					</div>
																				</td>
																			";
																			for($d=1;$d<=$cant_niveles;$d++)
																			{
																				if($tipo_pregunta == "ponderacion_estandar")
																				{
																					$num=$d;
																				}
																				if($tipo_pregunta == "si_no")
																				{
																					$num=$d;
																					if($d>1)$num=0;
																				}	
																					echo "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center'>
																						<input type = 'radio' name = 'item_".$cont1."_preg_".$cont_pregunta."' value = ".$num."></td>";
																				
																				
																			}
																		
																		echo "</tr>";
																	}
																}
																			
															}
															?>

														</table>
														<?php
														}
														if($tipo_pregunta == "alternativas")
														{
															$x=json_decode($encuesta[$i]["cadena_encuesta"],true);
															
															echo "<ul style = 'list-style-type: none;'>
															";

															foreach($x["alternativas"] as $key=>$value)
															{
																if($key==$x["correcta"]){	$atributo = "true";	} 
																else {$atributo = "false";}

																echo "<li correcta = '".$atributo."' class = 'marcacion_alternativa' style = 'cursor:pointer;'>
																		<label style = 'font-weight:normal;'>".$key.") </label> 
																		<span>".$value."</span>
																	</li>";
															}

															echo "</ul>";
														}

														if($tipo_pregunta == "autocompletar")
														{
															echo "<textarea class = 'form-control desc_aut' rows='5'></textarea>";
														}
														?>
															
													</div>
													<div class = 'col-lg-1'></div>
												</div>
												</div><!--PREGUNTA PADRE-->
											</div>
										</div>
										<br>
										<?php
										}
									}
									
								?>

						</div>
						<br>
						<?php 
						if($accion=="ver_detalle")
						{
						?>
						<div class = 'row'>
							<div class = 'col-lg-8'>
							</div>
							<div class = 'col-lg-4 text-right'>
								<button type="submit" class = 'btn btn-primary enviar_rubrica' tipo = '<?php echo $ajuste[0]["tipo"]; ?>'><i class = 'icon-save'></i> Enviar Rúbrica</button>
							</div>
						</div>
						<?php	
						}
						?>
						
					</div>
				</div>
				<!--</form>-->
			</div>
			<div class = 'col-lg-1'></div>
		</div>
	</div>


</body>

</html>