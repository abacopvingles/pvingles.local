<?php
	session_start();
	require_once("class/class_ajustes.php");
	require_once("class/class_mantenimiento.php");
	require_once("class/class_evaluacion.php");

	if(!empty($_GET["id_encuesta"]))
	{
		$ses_idencuesta = $_GET["id_encuesta"];
	}
	else
	{
		$ses_idencuesta = $_SESSION["id_encuesta"];
	}

	
	$obj = new Ajuste();
	$obj1 = new Evaluacion();
	$obj2 = new Mantenimiento();
	$ajuste = $obj->listar_idencuesta($ses_idencuesta);
	$encuesta = $obj1->listar_evaluacion($ses_idencuesta);
	$cant_preg = ($encuesta=="")?0:count($encuesta);
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
    <!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">
</head>

<style>
	::-webkit-input-placeholder {
   font-style: italic;
	}
	:-moz-placeholder {
	   font-style: italic;  
	}
	::-moz-placeholder {
	   font-style: italic;  
	}
	:-ms-input-placeholder {  
	   font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
	    background-color: #fff;
	}
</style>

<body>

<div class = 'aside' style = 'width: 25%;min-height: 600px; position: fixed; display: none; top: 60px;z-index: 9999; right: 0; background-color: #fff;border: 1px solid #ddd'>
	<?php
		if($ajuste[0]["tipo"]=="rubrica")
		{
			require_once("menu_lateral_rubrica.php"); 
		} 
		else
		{
			require_once("menu_lateral.php"); 
		}
	?>
	</div>

	<div class = 'container-fluid'>
		
		<div class = 'row'>
		    <div class = 'col-lg-12'>
			    <?php
			        require_once("menu.php");
			    ?>
		    </div>
		</div>

		<div class = 'row' style="margin-top: 60px;">

			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>


				<input type = 'hidden' class = 'cant_preguntas' value = '<?php echo $cant_preg;?>'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-heading'>
						Preguntas
					</div>
					<div class = 'panel-body'>
						<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_encuesta"];?>'>

						<div class = 'contenedor_preguntas'>

							<!--<div class = 'muestra'>-->		
						<?php

						if($ajuste[0]["tipo"]=="rubrica")
						{ 
							//echo "ses id_encuesta: ".$ses_idencuesta;
						?>
						<div class = 'row'>
							<div class = 'col-lg-11'>
								<?php

									if(!empty($encuesta))
									{
										$titulo_enc=$encuesta[0]["titulo"];
										$descripcion_enc=$encuesta[0]["descripcion"];

										if(strlen($titulo_enc)==0){ $titulo_enc="Escriba aquí el título"; }
										if(strlen($descripcion_enc)==0){ $descripcion_enc="Escriba aquí la descripción"; }
										
									}
									else
									{
										$titulo_enc="Escriba aquí el título";
										$descripcion_enc="Escriba aquí la descripción";
									}

								?>
								<div class = 'escriba_titulo' nombre=''><?php echo $titulo_enc;?></div>
								<div class = 'escriba_descripcion' nombre = '' style = 'margin-top:20px;'><?php echo $descripcion_enc;?></div>
							</div>
						</div>
						<?php
						}
										
						if($ajuste[0]["tipo"]=="rubrica")
						{
						?>
						<div class = 'form-horizontal'>
							<div class = 'form-group' style="margin-top: 20px;">
								<label class = 'control-label col-lg-4'>Niveles</label>
								<div class = 'col-lg-4'>
									<select class = 'form-control cant_niveles'>
									<?php
										if(!empty($encuesta)){ $cant_niveles = $encuesta[0]["cant_niveles"];}
										for($i=2;$i<6;$i++)
										{
											echo "<option value = '".$i."'"; if(!empty($encuesta) && $cant_niveles==$i){ echo "selected";} echo ">".$i."</option>";
										}
									?>
									</select>
								</div>
								<div class = 'col-lg-4'></div>
							</div>
						</div>
						<?php
						}
						?>

						<div class = 'table-responsive'>
							<table class='table table-striped' id='tabla_dimensiones' cellpadding="4">
							<?php
							if(!empty($encuesta) && $ajuste[0]["tipo"]=="rubrica")
							{
								$cont=0;
								$cant_dimension=0;
								$cant_estandar = 0;
								$cad_encuesta = json_decode($encuesta[0]["cadena_encuesta"],true);
								$colspan = $cant_niveles+2;

								echo "<tr class = 'cabecera'>
										<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'><h4>DIMENSIONES</h4></td>
										<td width = '10%' colspan = '".$cant_niveles."' style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'><dt>NIVELES</dt></td>
									</tr>
									<tr class = 'por_nivel'>";
									for($i=1;$i<=$cant_niveles;$i++)
									{
										echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$i."</h5></td>";
									}
									echo "</tr>";

									foreach($cad_encuesta as $key=>$value)
									{
										$dimension = $obj2->listar_id_dimension($key);
										$cant_dimension++;
										$let = 64+$cant_estandar;

										echo "<tr id_dimension='".$key."' style = 'padding: 10px;' class = 'fila_dimension'>
												<td colspan = '$colspan' class = 'td_dimension' style = 'background:#fb8c00;color:#fff;'><dt>DIMENSIÓN ".$cant_dimension.":".strtoupper($dimension[0]["nombre"])."</dt></td>
											</tr>";

										foreach($value as $prim=>$sec)
										{
											$estandar = $obj2->listar_id_estandar($prim);
													
											$let++;
											$cant_estandar++;
											$letra = strtoupper(chr($let));
													
											echo "<tr id_estandar = '".$estandar[0]["id_estandar"]."' class = 'fila_estandar d_".$estandar[0]["id_dimension"]."'>
															<td colspan = '$colspan' style = 'background:#e8e8e7;'><dt>".$letra.") ".$estandar[0]["nombre"]."</dt></td>
												</tr>";

												foreach($sec as $prim1)
												{
													$pregunta = $obj->listar_id_pregunta($prim1);
													$cont++;
													echo "<tr id_pregunta = '".$pregunta[0]["id_pregunta"]."' class = 'fila_pregunta e_".$valor["id_estandar"]."'>
															<td width = '5%' style = 'background: #e0e8ef;'>".$cont.".-</td>
															<td style = 'background: #fcfce0;'>".$pregunta[0]["nombre"]."</td>";
													for($i=1;$i<=$cant_niveles;$i++)
													{
														echo "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
													}
													echo "</tr>";
													}
												}
											}
										}
										?>
							</table>
						</div>

						<!--</div>-->
						<?php

						/***************************PERSONALIZADO*****************************/
							$cont1=0;
							if(!empty($encuesta) && $ajuste[0]["tipo"]=="personalizado")
							{
								for($i=0;$i<sizeof($encuesta);$i++)
								{
									$cont1++;
									$tipo_pregunta = $encuesta[$i]["tipo_pregunta"];
									?>
									<div class = 'muestra'>
										<div class = 'pregunta_padre'>
				    						<div class = 'row'>
				    							
				    							<?php 
				    								if($tipo_pregunta=="ponderacion_estandar" || $tipo_pregunta=="si_no")
				    								{
				    									$titulo = $encuesta[$i]["titulo"];
					    							?>
						    							<div class = 'col-lg-11 titulo_descripcion'>
						    								<?php if($titulo==""){ $titulo = "Pregunta";}?>
						    								<h4><?php echo $cont1.".- ".$titulo;?></h4>
						    								<h3><?php echo $encuesta[$i]["descripcion"];?></h3>
						    							</div>
					    							<?php
				    								}
				    								else if($tipo_pregunta=="alternativas" || $tipo_pregunta=="autocompletar")
				    								{
				    									?>
					    								<div class = 'col-lg-11 titulo_descripcion'>
															
																<h4><?php echo $cont1;?>.- </h4>
														
					    								</div>

				    									<?php
				    								}	
				    							?>
				    								
				    							
												<div class = 'col-lg-1 text-right'>
													<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>
												</div>


											</div>
											<?php
												if($tipo_pregunta=="alternativas" || $tipo_pregunta=="autocompletar")
												{
												?>
												<div class = 'row'>
													<div class = 'col-lg-11'>
														<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>
						    								<?php echo "DIMENSIÓN: ".$encuesta[$i]["titulo"];?>
						    							</div>
						    							<div style = 'background:#e8e8e7;padding:5px;'>
						    								<?php echo "ESTÁNDAR: ".$encuesta[$i]["descripcion"];?>
						    							</div>
						    							<?php
						    								if($tipo_pregunta=="alternativas")
						    								{
						    									$y=json_decode($encuesta[$i]["cadena_encuesta"],true);
						    									$pregunta = $y["pregunta"];
						    								}
						    								else
						    								{
						    									$pregunta = $encuesta[$i]["cadena_encuesta"];
						    								}
						    							?>
						    								<div style = 'background:#fcfce0;padding:5px;'><dt><?php echo $y["pregunta"];?></dt>
						    								</div>
						    							<br>
						    						</div>
					    						</div>
												<?php
												}
												?>
											<div class = 'row'>
												<div class = 'col-lg-11 ejercicio'>
													<?php 
													if($tipo_pregunta=="ponderacion_estandar" || $tipo_pregunta=="si_no")
													{
														$cant_niveles = $encuesta[$i]["cant_niveles"];

														$colspan = $cant_niveles+2;
														if($tipo_pregunta == "ponderacion_estandar")
														{$tit="NIVELES";$opc=$cant_niveles;}
														else {$tit="OPCION";$opc=array("SI","NO");}
													?>
													<table class = 'table table-bordered sortableTable responsive-table'>
														<tr class = 'cabecera'>
															<td width = '90%' colspan = '2' rowspan = '2' class = 'text-center' style = 'background:#2196f3;color:#fff;border-right:2px solid #fff;'>
																<h4>DIMENSIONES</h4>
															</td>
															<td width = '10%' colspan = <?php echo $cant_niveles; ?> style = 'background:#2196f3;color:#fff;' class = 'fila_nivel text-center'>
																<dt><?php echo $tit;?></dt>
															</td>
														</tr>
														<tr class = 'por_nivel'>
														<?php
															if($tipo_pregunta == "ponderacion_estandar")
															{
																for($a=1;$a<=$opc;$a++)
																{
																	echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$a."</h5></td>";
																}
															}
															else
															{
																for($a=0;$a<sizeof($opc);$a++)
																{
																	echo "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>".$opc[$a]."</h5></td>";
																}
															}
															?>
														</tr>

														<?php
															$cont_est=64+$cant_estandar;
															$cont_pregunta = 0;
															$x=json_decode($encuesta[$i]["cadena_encuesta"],true);
															$cant_dimension=0;
															foreach($x as $dimension)
															{
																$cant_dimension++;

																$nombre = $dimension["nombre"];

																echo "<tr style = 'padding: 10px;'>
																		<td colspan = '$colspan' class = 'td_dimension' style = 'background:#fb8c00;color:#fff;'><dt>DIMENSIÓN ".$cant_dimension.":".strtoupper($nombre)."</dt>
																		</td>
																	</tr>";

																$est = $dimension["estandar"];
																if(!empty($est))
																{
																	foreach($est as $estandar)
																	{
																		$cont_est++;
																		$letra = strtoupper(chr($cont_est));
																		echo "<tr>
																				<td colspan = '$colspan' style = 'background:#e8e8e7;'>
																					<dt>".$letra.") ".$estandar["nombre"]."</dt>
																				</td>
																			</tr>";

																			$preg=$estandar["pregunta"];
																			//var_dump($preg);
																		if(!empty($preg))
																		{
																			for($c=0;$c<sizeof($preg);$c++)
																			{
																				$cont_pregunta++;
																				echo "<tr>
																						<td width = '5%' style = 'background: #e0e8ef;'>".$cont_pregunta.".-
																						</td>
																						<td style = 'background: #fcfce0;'>".$preg[$c]."
																						</td>
																					";
																					for($d=1;$d<=$cant_niveles;$d++)
																					{
																						echo "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
																					}
																				
																				echo "</tr>";
																			}
																		}
																		
																	}
																}
																
																			
															}
															?>

														</table>
														<?php
														}
														if($tipo_pregunta == "alternativas")
														{
															$x=json_decode($encuesta[$i]["cadena_encuesta"],true);
															
															echo "<ul style = 'list-style-type: none;'>
															";

															foreach($x["alternativas"] as $key=>$value)
															{
																if($key==$x["correcta"]){	$atributo = "true";	} 
																else {$atributo = "false";}

																echo "<li correcta = '".$atributo."'>
																		<label style = 'font-weight:normal;'>".$key.") </label> 
																		<span>".$value."</span>
																	</li>";
															}

															echo "</ul>";
														}

														if($tipo_pregunta == "autocompletar")
														{
															echo "<textarea class = 'form-control' rows='5' readonly></textarea>";
														}
														?>
															
													</div>
													<div class = 'col-lg-1'></div>
												</div>
											</div>
										</div>
										<br>
										<?php
										}
									}
									
								?>

							


							<div class = 'sector'>

								<div class = 'row'>
									<div class = 'col-lg-11 bloque_preguntas'>
										<center>							
										<br><br><br>
											<button type="button" class="btn btn-default btn-circle btn-lg">
												<i class="icon-pencil" style = 'font-size: 24px;'></i>
	                            			</button>
	                            		</center>
									</div>
									<div class = 'col-lg-1'></div>

								</div>

							</div>



						</div>
						
						<?php
						if($ajuste[0]["tipo"]=="rubrica")
						{
							?>
							<br>
							<div class = 'row'>
								<div class = 'col-lg-8'>
								</div>
								<div class = 'col-lg-4 text-right'>
									<button class = 'btn btn-primary guardar_examen'><i class = 'icon-save'></i> Guardar</button>
								</div>
							</div>
							<?php
						}
						?>
						
					</div>
				</div>
			</div>
			<div class = 'col-lg-1'></div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
               	<div id = 'ponderacion_estandar'>
               	</div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
               	<div id = 'alternativas'>
               	</div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
               	<div id = 'si_no'>
               	</div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
               	<div id = 'autocompletar'>
               	</div>
	        </div>
	    </div>
	</div>

	<script>

		$(".bloque_preguntas").click(function(){
			$(".aside").show();
		});

		$(".pond_estandar").click(function(){

			$.ajax({
	                url: "ponderacion_estandar.php",
	                type: "POST",
	                //data: { id_registro: $(this).attr("id_modelo")},
	                success: function(result){
	                     $("#ponderacion_estandar").html(result);
	                    }
	                });
			$(".aside").hide();
		});

		$(".alternativas").click(function(){

			$.ajax({
	                url: "alternativas.php",
	                type: "POST",
	                success: function(result){
	                     $("#alternativas").html(result);
	                    }
	                });
			$(".aside").hide();
		});

		$(".si_no").click(function(){

			$.ajax({
	                url: "si_no.php",
	                type: "POST",
	                success: function(result){
	                     $("#si_no").html(result);
	                    }
	                });
			$(".aside").hide();
		});

		$(".autocompletar").click(function(){

			$.ajax({
	                url: "autocompletar.php",
	                type: "POST",
	                success: function(result){
	                     $("#autocompletar").html(result);
	                    }
	                });
			$(".aside").hide();
		});


		var con_cambio = function(obj,input_type,nombre)
		{
			if(input_type=="titulo")
			{
				$(obj).replaceWith("<input type = 'text' class = 'form-control titulo_rubrica' value = '"+nombre+"' placeholder = 'Escriba aquí el título'>");
				$(".titulo_rubrica").focus();


			}
			else if(input_type=="descripcion")
			{
				$(obj).replaceWith("<textarea class = 'form-control descripcion_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí la descripción'>"+nombre+"</textarea>");
				$(".descripcion_rubrica").focus();

			}
			else
			{
				$(obj).replaceWith("<textarea class = 'form-control pregunta_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí la pregunta'>"+nombre+"</textarea>");
				$(".pregunta_rubrica").focus();
			}
		}

		var sin_cambio = function(obj,input,input_type)
		{
			if(input_type=="titulo"){ $(obj).replaceWith(input);}
			if(input_type=="descripcion"){ $(obj).replaceWith(input); }
			if(input_type=="pregunta"){ $(obj).replaceWith(input); }
			
		}

		$("body").on("click",".escriba_titulo",function(){
			var nombre = $(this).attr("nombre");
			if(nombre.length==0){nombre="";}
			con_cambio($(this),"titulo",nombre);
		});

		$("body").on("click",".escriba_descripcion",function(){
			var nombre = $(this).attr("nombre");
			if(nombre.length==0){nombre="";}
			con_cambio($(this),"descripcion",nombre);
		});

		$("body").on("click",".escriba_pregunta",function(){
			var nombre = $(this).attr("nombre");
			if(nombre.length==0){nombre="";}
			con_cambio($(this),"pregunta",nombre);
		});

		$("body").on("blur",".titulo_rubrica",function(){
			var nombre = $(this).val(); nombre1=nombre;
			if(nombre.length==0)
			{
				nombre = "Escriba aquí el título";
				nombre1 = "";
			}

			var input1 = "<div class = 'escriba_titulo' nombre = '"+nombre1+"'>"+nombre+"</div>";
			sin_cambio($(this),input1,"titulo");
		});

		$("body").on("blur",".descripcion_rubrica",function(){
			var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;
			if(nombre_desc.length==0)
			{
				nombre_desc = "Escriba aquí la descripción";
				nombre1_desc = "";
			}
			var input2 = "<div class = 'escriba_descripcion' nombre = '"+nombre1_desc+"' style = 'margin-top:20px;'>"+nombre_desc+"</div>";
			sin_cambio($(this),input2,"descripcion");
		});

		$("body").on("blur",".pregunta_rubrica",function(){

			var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;

			if(nombre_desc.length==0)
			{
				nombre_desc = "Escriba aquí la pregunta";
				nombre1_desc = "";
			}
			var input2 = "<div class = 'escriba_pregunta' nombre = '"+nombre1_desc+"' style = 'margin-top:20px;'>"+nombre_desc+"</div>";
			sin_cambio($(this),input2,"pregunta");
		});


		$(".cant_niveles").change(function(){
			
			var cant_niveles = $(this).val();
			$(".fila_nivel").attr("colspan",cant_niveles);
			var columnas = '';
			var col_radio = '';
			var colspan = parseInt(cant_niveles)+parseInt(2);

			for(var i=1;i<=cant_niveles;i++)
			{
				columnas += "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>"+i+"</h5></td>";
				col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
			}
			$(".por_nivel").html(columnas);
			$(".option_radio").remove();
			$(".fila_pregunta").append(col_radio);
			$(".td_dimension").attr("colspan",colspan);
			$(".td_estandar").attr("colspan",colspan);

		})

		$(".guardar_examen").click(function(){
			var titulo = $(".escriba_titulo").attr("nombre");
			var descripcion = $(".escriba_descripcion").attr("nombre");
			var cant_nivel = $(".cant_niveles").val();
			var id_encuesta = $("#id_encuesta").val();
			var obj = {};

			$(".fila_dimension").each(function(){
				var id_dimension = $(this).attr("id_dimension");
				obj[id_dimension] = {};
				
				$(".fila_estandar.d_"+id_dimension).each(function(){
					var id_estandar = $(this).attr("id_estandar");
					obj[id_dimension][id_estandar] = [];	
					$(".fila_pregunta.e_"+id_estandar).each(function(){
						var id_pregunta = $(this).attr("id_pregunta");
						obj[id_dimension][id_estandar].push(id_pregunta);
					});
				});
			})

			$.ajax({
	    		url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					titulo: titulo,
					descripcion: descripcion,
					cant_nivel: cant_nivel,
					obj: obj,
					id_encuesta: id_encuesta,
					valor: "guardar_examen"
					}
	    	})

	    	.done(function(res){
	    		new PNotify({
					title: 'Mensaje',
					text: 'Rúbrica guardada',
					delay: 2000

				});
	    	})

	    	.fail(function(error){
            console.log("error-no-guarda-examen");
        })
		});
	

	/******--------*******/

	$("body").on("click",".guardar_ponderacion",function(){
		
		var titulo = $(".escriba_titulo").attr("nombre");
		var descripcion = $(".escriba_descripcion").attr("nombre");
		var id_encuesta = $("#id_encuesta").val();
		var tipo_pregunta = "ponderacion_estandar";
		var cant_niveles = $(".cant_niveles").val();
		var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);
		
		var arr = [];

		$(".fila_dimension").each(function(){
			var nombre_dim = $(this).find(".reemplazo_dimension").text();
			var attr_dim = $(this).attr("clase_dimension");
			var arr_pregunta = {};
			arr_pregunta["nombre"]=nombre_dim;
			arr_pregunta["estandar"]=[];
			//console.log(arr_pregunta);	

			$(".fila_estandar.d_"+attr_dim).each(function(){
				var nombre_est = $(this).find(".reemplazo_estandar").text();
				var attr_est = $(this).attr("clase_estandar");
				var elem = {};
				elem["nombre"]=nombre_est;
				elem["pregunta"]=[];


				$(".fila_pregunta.d_"+attr_dim+".e_"+attr_est).each(function(){
					var nombre_preg = $(this).find(".reemplazo_pregunta").text();
					elem["pregunta"].push(nombre_preg);
					//console.log(nombre_preg);
				});

				arr_pregunta["estandar"].push(elem);
			});
			arr.push(arr_pregunta);
		});


		$.ajax({
	    	url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_encuesta: id_encuesta,
				titulo: titulo,
				descripcion: descripcion,
				cant_niveles: cant_niveles,
				tipo_pregunta: tipo_pregunta,
				arr: JSON.stringify(arr),
				valor: "grabar_ponderacion"
			}
		})

		.done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		var clon = $(".clonado").clone();

	    		if(titulo.length==0){ titulo = "Pregunta"} if(descripcion.length==0){ descripcion = ""}
	    	
	    		$(".modal-body").html("");
	    		clon.find(".clonado").removeClass("clonado");
	    		clon.find(".fila_dimension").removeClass("fila_dimension");
	    		clon.find(".fila_estandar").removeClass("fila_estandar");
	    		clon.find(".fila_pregunta").removeClass("fila_pregunta");
	    		clon.find(".no_imprimir").remove();
	    		$(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
	    								"<div class = 'row'>"+
	    									"<div class = 'col-lg-11 titulo_descripcion'>"+
	    										"<h4>"+cant_preguntas+ ".- "+titulo+"</h4><h3>"+descripcion+"</h3>"+
	    									"</div>"+
											"<div class = 'col-lg-1 text-right'>"+
												"<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
											"</div>"+
										"</div>"+
										"<div class = 'row'>"+
											"<div class = 'col-lg-11 ejercicio'>"+
												"<table class = 'table table-bordered sortableTable responsive-table'>"+
													clon.html()+
												"</table>"+
											"</div>"+
											"<div class = 'col-lg-1'></div>"+
										"</div>"+
									"</div></div><br>");

	    		$(".cant_preguntas").val(cant_preguntas);

	    		new PNotify({
					title: 'Mensaje',
					text: 'Esta pregunta se ha guardado'
				});
	    	}
	    })

	    .fail(function(error){
            console.log("error-no-guarda-ponderacion");
        })
	});

	$("body").on("click",".guardar_alternativa",function(){
		var id_encuesta = $("#id_encuesta").val();
		var titulo = $(".escriba_titulo").attr("nombre");
		var descripcion = $(".escriba_descripcion").attr("nombre");
		var pregunta = $(".escriba_pregunta").attr("nombre");
		var tipo_pregunta = "alternativas";
		var correcta = $("input[name='alternativa']:checked").val();
		var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);
		var bloque = {};

		bloque["pregunta"]=pregunta;
		bloque["alternativas"]={}

		$(".fila_alternativa").each(function(i,element){
			var letra = $(this).find(".control-label").attr("letra");
			var alternativa = $(this).find(".texto_alternativa").val();
			bloque["alternativas"][letra] = alternativa;
		});
		bloque["correcta"]=correcta;

		$.ajax({
	    	url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_encuesta: id_encuesta,
				titulo: titulo,
				descripcion: descripcion,
				pregunta: pregunta,
				tipo_pregunta: tipo_pregunta,
				bloque: bloque,
				valor: "grabar_alternativa"
			}
		})

		.done(function(res){
			//if(titulo.length==0){ titulo = "Pregunta"} if(descripcion.length==0){ descripcion = ""}
			$(".modal-body").html("");
			var html = "<ul style = 'list-style-type: none;'>";

			$.each(bloque["alternativas"],function(key,value){
	    		if(key==bloque["correcta"]) { var atributo = "true"} else { var atributo = "false"}
	    		html += "<li correcta = "+atributo+">"+
	    					"<label style = 'font-weight:normal;'>"+key+")&nbsp;</label>"+
	    						"<span>"+value+"</span>"+
	    				"</li>";
	    	});
	    	html += "</ul>";

	    	
	    	$(".cant_preguntas").val(cant_preguntas);
			$(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
									"<div class = 'row'>"+
										"<div class = 'col-lg-11 titulo_descripcion'>"+
											"<h4>"+cant_preguntas+".-</h4>"+
										"</div>"+
										"<div class = 'col-lg-1 text-right'>"+
											"<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
										"</div>"+
									"</div>"+
	    							"<div class = 'row'>"+
	    								"<div class = 'col-lg-11'>"+
	    									"<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>DIMENSIÓN: "+titulo+"</div>"+
	    									"<div style = 'background:#e8e8e7;padding:10px;'>ESTÁNDAR: "+descripcion+"</div>"+
	    									"<div style = 'background:#fcfce0;padding:10px;'><dt>"+pregunta+"</dt></div>"+
	    								"</div>"+
										
									"</div><br>"+
									"<div class = 'row'>"+
										"<div class = 'col-lg-11 ejercicio'>"+
											html+
										"</div>"+
										"<div class = 'col-lg-1'></div>"+
									"</div>"+
								"</div></div><br>");

			new PNotify({
					title: 'Mensaje',
					text: 'Esta pregunta se ha guardado'
				});
			
		})

		.fail(function(error){
            console.log("error-no-guarda-alternativas");
		})
	});


	$("body").on("click",".guardar_si_no",function(){
		
		var titulo = $(".escriba_titulo").attr("nombre");
		var descripcion = $(".escriba_descripcion").attr("nombre");
		var id_encuesta = $("#id_encuesta").val();
		var tipo_pregunta = "si_no";
		var cant_niveles = $(".cant_niveles").val();
		var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);
		
		var arr = [];

		$(".fila_dimension").each(function(){
			var nombre_dim = $(this).find(".reemplazo_dimension").text();
			var attr_dim = $(this).attr("clase_dimension");
			var arr_pregunta = {};
			arr_pregunta["nombre"]=nombre_dim;
			arr_pregunta["estandar"]=[];
			//console.log(arr_pregunta);	

			$(".fila_estandar.d_"+attr_dim).each(function(){
				var nombre_est = $(this).find(".reemplazo_estandar").text();
				var attr_est = $(this).attr("clase_estandar");
				var elem = {};
				elem["nombre"]=nombre_est;
				elem["pregunta"]=[];

				console.log(nombre_est);	

				$(".fila_pregunta.d_"+attr_dim+".e_"+attr_est).each(function(){
					var nombre_preg = $(this).find(".reemplazo_pregunta").text();
					elem["pregunta"].push(nombre_preg);
					//console.log(nombre_preg);
				});

				arr_pregunta["estandar"].push(elem);
			});
			arr.push(arr_pregunta);
		});
		//console.log(arr);

		$.ajax({
	    	url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_encuesta: id_encuesta,
				titulo: titulo,
				descripcion: descripcion,
				cant_niveles: cant_niveles,
				tipo_pregunta: tipo_pregunta,
				arr: JSON.stringify(arr),
				valor: "grabar_si_no"
			}
		})

		.done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		var clon = $(".clonado").clone();

	    		if(titulo.length==0){ titulo = "Pregunta"} if(descripcion.length==0){ descripcion = ""}
	    	
	    		$(".modal-body").html("");
	    		clon.find(".clonado").removeClass("clonado");
	    		clon.find(".fila_dimension").removeClass("fila_dimension");
	    		clon.find(".fila_estandar").removeClass("fila_estandar");
	    		clon.find(".fila_pregunta").removeClass("fila_pregunta");
	    		clon.find(".no_imprimir").remove();
	    		
	    		$(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
	    								"<div class = 'row'>"+
	    									"<div class = 'col-lg-11 titulo_descripcion'>"+
	    										"<h4>"+	cant_preguntas+" "+titulo+"</h4><h3>"+descripcion+"</h3>"+
	    									"</div>"+
											"<div class = 'col-lg-1 text-right'>"+
												"<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
											"</div>"+
										"</div>"+
										"<div class = 'row'>"+
											"<div class = 'col-lg-11 ejercicio'>"+
												"<table class = 'table table-bordered sortableTable responsive-table'>"+
													clon.html()+
												"</table>"+
											"</div>"+
											"<div class = 'col-lg-1'></div>"+
										"</div>"+
									"</div></div><br>");

	    		$(".cant_preguntas").val(cant_preguntas);

	    		new PNotify({
					title: 'Mensaje',
					text: 'Esta pregunta se ha guardado'
				});
	    	}
	    })

	    .fail(function(error){
            console.log("error-no-guarda-ponderacion");
        })
	});

	$("body").on("click",".guardar_autocompletar",function(){

		var id_encuesta = $("#id_encuesta").val();
		var titulo = $(".escriba_titulo").attr("nombre");
		var descripcion = $(".escriba_descripcion").attr("nombre");
		var pregunta = $(".escriba_pregunta").attr("nombre");

		var tipo_pregunta = "autocompletar";

		var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);

		$.ajax({
	    	url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_encuesta: id_encuesta,
				titulo: titulo,
				descripcion: descripcion,
				pregunta: pregunta,
				tipo_pregunta: tipo_pregunta,
				valor: "grabar_autocompletar"
			}
		})

		.done(function(res){
			//if(titulo.length==0){ titulo = "Pregunta"} if(descripcion.length==0){ descripcion = ""}
			$(".modal-body").html("");
			
	    	$(".cant_preguntas").val(cant_preguntas);
			$(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
	    							"<div class = 'row'>"+
										"<div class = 'col-lg-11 titulo_descripcion'>"+
											"<h4>"+cant_preguntas+".-</h4>"+
										"</div>"+
									"</div>"+
	    							"<div class = 'row'>"+
	    								"<div class = 'col-lg-11'>"+
	    									"<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>DIMENSIÓN: "+titulo+"</div>"+
	    									"<div style = 'background:#e8e8e7;font-weight:bold;padding:10px;'>ESTÁNDAR: "+descripcion+"</div>"+
	    									"<div style = 'background:#fcfce0;padding:10px;'><dt>"+pregunta+"</dt></div>"+
	    								"</div>"+
										"<div class = 'col-lg-1 text-right'>"+
											"<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
										"</div>"+
									"</div><br>"+
									"<div class = 'row'>"+
										"<div class = 'col-lg-11 ejercicio'>"+
											"<textarea class = 'form-control' rows='5' readonly></textarea>"+
										"</div>"+
										"<div class = 'col-lg-1'></div>"+
									"</div>"+
								"</div></div><br>");

			new PNotify({
					title: 'Mensaje',
					text: 'Esta pregunta se ha guardado'
				});
			
		})

		.fail(function(error){
            console.log("error-no-guarda-autocompletar");
		})
	});
	 
	</script>

	

</body>

</html>
