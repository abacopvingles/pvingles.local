<?php
	session_start();
	require_once("class/class_reporte.php");
	require_once("class/class_usuario.php");
	require_once("class/class_ajustes.php");

	$id_encuesta = $_POST["id_encuesta"];

	$obj=new Reporte();
	$obj1=new Ajuste();
	$obj2=new Usuario();

	$res=$obj->listar_resultado($id_encuesta);
	$res1=$obj1->listar_idencuesta($id_encuesta);
	$objetivo = $res1[0]["objetivo"];
?>

<div class = 'modal-header'>
	LISTA DE USUARIOS
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>
</div>

<div class = 'modal-body'>
	<div class = 'table-responsive'>
		<table class = 'table table-striped table-condensed table-hover display' >
			<thead>
				<tr style = 'background-color:#428bca; color: #fff;'>
					<th>N°</th>
					<th>Usuario</th>
					<th>Puntaje Obtenido</th>
					<th>Porcentaje Obtenido</th>
					<th>Escala</th>
				</tr>
			</thead>
		<tbody>
			<?php 
			$cont=0;
			for($i=0;$i<sizeof($res);$i++)
			{
				$id_usuario = $res[$i]["id_usuario"];

				$cont++;
				if($objetivo=="alumno")
				{
					$res2=$obj2->listar_alumno($id_usuario);
				}
				else
				{
					$res2=$obj2->listar_usuario($id_usuario);
				}

				$nombre_usuario = $res2[$i]["nombre"];
				$resultado = json_decode($res[$i]["resultado"],true);
				?>
				<tr>
					<td><?php echo $cont;?></td>
					<td><?php echo "<a target= '_blank' href = 'detalle_encuesta.php?id_encuesta=$id_encuesta&id_usuario=$id_usuario&accion=ver_detalle'>".$nombre_usuario."</a>";?></td>
					<td><?php echo $resultado["puntaje_obtenido_encuesta"];?></td>
					<td><?php echo $resultado["porcentaje_obtenido"]."%";?></td>
					<td><?php echo "<a target= '_blank' href = 'detalle_encuesta.php?id_encuesta=$id_encuesta&id_usuario=$id_usuario&accion=ver_detalle'>".$resultado["escala_obtenida"]."</a>";?></td>
				</tr>
				<?php
			}
			?>
		</tbody>
		</table>
</div>

<div class = 'modal-footer'>
</div>