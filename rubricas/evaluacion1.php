<?php
//if (session_status() == PHP_SESSION_NONE) {
	@session_start();
//}
require_once("class/class_ajustes.php");
require_once("class/class_mantenimiento.php");
require_once("class/class_evaluacion.php");

if(!empty($_GET["id_rubrica"]))
{
	$ses_idrubrica = $_GET["id_rubrica"];
}
else
{
	$ses_idrubrica = $_SESSION["id_rubrica"];
}


$obj = new Ajuste();
$obj1 = new Evaluacion();
$obj2 = new Mantenimiento();
$ajuste = $obj->listar_idencuesta($ses_idrubrica);
$encuesta = $obj1->listar_evaluacion($ses_idrubrica); 
$cant_preg = ($encuesta=="")?0:count($encuesta);
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- PAGE LEVEL TABLA STYLES -->
	<link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<script src ="plugins/dataTables/Spanish.js"></script>
	<!-- END PAGE LEVEL  STYLES -->

	<!-- PAGE LEVEL STYLES -->
	<link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src="js/pnotify.custom.min.js"></script>
	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

</head>

<style>
	::-webkit-input-placeholder {
		font-style: italic;
	}
	:-moz-placeholder {
		font-style: italic;  
	}
	::-moz-placeholder {
		font-style: italic;  
	}
	:-ms-input-placeholder {  
		font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
		background-color: #fff;
	}

	.poner_imagen{
		padding-left:5px;
		cursor: pointer;
		position: relative;
	}

	.img_check{
		position:absolute;
		bottom:0px;
		left:1em;
		right:0px;
		top: 0px;
		display: none;
		height: 100%
	}

	.navbar-top-links li a {
		padding: 10px 2px;
	}

}
</style>

<body>

	<div class = 'container-fluid'>

		<div class = 'row'>
			<div class = 'col-lg-12'>
				<?php require_once("menu.php"); ?>
			</div>
		</div>

		<div class = 'row' style="margin-top: 60px;">

			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>

				<input type = 'hidden' class = 'cant_preguntas' value = '<?php echo $cant_preg;?>'>
					
				<div class="form-group link_llenado" id="link_llenado">
					<div class="input-group">
						<div class="input-group-addon">Enlace para responder la rubrica:</div>
						<input type="text" name="txtLinkLlenado" id="txtLinkLlenado" class="form-control" value="<?php echo $_SERVER['HTTP_HOST']."/tacna/control/monitoreo/ingles/dar_examen.php?id_rubrica=".$_SESSION['id_rubrica'] ?>" readonly>
						<div class="input-group-addon"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> </div>
					</div>
				</div>
				<input type = 'hidden' id='id_rubrica' value = '<?php echo $ses_idrubrica;?>'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-heading text-center'>
						<h3>Rúbrica: "<?php echo $ajuste[0]["titulo"];?>"</h3>
						<h4>Creación de Dimensiones</h4>
					</div>
					<div class = 'panel-body'>
						<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>

						<div class = 'row bloque_add_dimension'>
							<div class = 'col-lg-12 text-center'>
								<button class = 'btn btn-default agregar_dimension'><i class = 'icon-plus'></i> Agregar Dimensión</button>
							</div>
						</div>

					</div>
				</div>
			<div class = 'col-lg-1'></div>
		</div>
	</div>


	<div class="modal fade" id="dimensiones_bd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close btncloseaddinfotxt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 id="titulo-modal">Dimensiones de Base de Datos</h4>
				</div>
				<div class="modal-body" id="cuerpo-modal">
					<img src="./images/loader.gif" alt="Cargando" class="center-block">
				</div>
				<div class="modal-footer" id="pie-modal">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary pull-right elegir_dimension">Elegir dimensión</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ModalPuntuacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id = 'puntuacion'>
				</div>
			</div>
		</div>
	</div>

    <script>
        $(document).ready(function() {
            $(".bloque_preguntas").click(function(){
                $(".aside").show();
            });

            /**********************************************/
    
	$("body").on("click",".agregar_dimension",function(){
		var colspan = parseInt($(".cant_niveles").val())+2;
		var cant_fila_dimension = parseInt($(".fila_dimension").length)+1;

		$(".bloque_add_dimension").before("<div class = 'row fila_dimension' style='background:#fb8c00;color:#fff;padding: 10px;'>"+
									"<input type = 'hidden' class = 'tipo_plantilla'>"+
									"<div class = 'col-lg-2' style='margin-top:10px'>"+
										"<h4>DIMENSIÓN "+cant_fila_dimension+" :</h4>"+
									"</div>"+
									"<div class = 'col-lg-7' style='margin-top:10px'>"+
										"<input type='text' style='display:inline;' class='form-control nueva_dimension' placeholder='Escriba aquí la dimensión'>"+
									"</div>"+
										
									"<div class = 'col-lg-3 col-md-3 col-sm-3 col-xs-3' id = 'area_botones'>"+
										'<ul class="nav navbar-top-links navbar-right">'+                
                    						'<li class="dropdown" style = "padding-right:0px;">'+
                        						'<a class="dropdown-toggle btn btn-circle btn-primary btn-lg" title = "Elegir Plantilla" data-toggle="dropdown" href="#" style = "background:#337ab7;border:none;">'+
                            						'<i class = "icon-list"></i>'+
                        						'</a>'+
                        						'<ul class="dropdown-menu dropdown-messages">'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='PE'>"+
																	"<img src = 'images/pond_estandar.png' class = 'img-responsive' title = 'Ponderación Estándar'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Ponderación Estándar'+
		                                    				'</div>'+
                                						'</a>'+
                            						'</li>'+
                            						'<li class="divider"></li>'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='SN'>"+
																	"<img src = 'images/si_no.png' class = 'img-responsive' title = 'SI-NO'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">SI-NO'+
		                                    				'</div>'+
                                						'</a>'+
                            						'</li>'+
                            						'<li class="divider"></li>'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='A'>"+
																	"<img src = 'images/alternativas.png' class = 'img-responsive' title = 'Alternativas'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Alternativas'+
		                                    				'</div>'+
                                						'</a>'+
					                            	'</li>'+
					                            	'<li class="divider"></li>'+
					                            	'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='C'>"+
																	"<img src = 'images/autocompletar.png' class = 'img-responsive' title = 'Completar'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Completar'+
		                                    				'</div>'+
                                						'</a>'+
					                            	'</li>'+
					                            	
						                        '</ul>'+
						                    '</li>'+
									
										"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-success btn-lg' title = 'Elegir de BD'>"+
											"<i class='icon-hand-up' style = 'font-size:20px;'></i>"+
                            			"</button></li>"+

                            			"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-primary btn-lg guardar_dimension' title = 'Guardar'>"+
											"<i class='icon-save' style = 'font-size:20px;'></i>"+
                            			"</button></li>"+
									
										"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-default btn-lg cancelar_add_dimension' title = 'Cancelar'>"+
											"<i class='icon-remove' style = 'font-size:20px;'></i>"+
                            			"</button></li></ul>"+
									"</div>"+
								"</div>"+
								"<br>");
	});

	$("body").on("click",".poner_imagen",function(){
		$(".img_check").css("display","none");
		$(this).find(".img_check").fadeIn("1500");
		var tipo_plantilla = $(this).attr("tipo_plantilla");
		$(".tipo_plantilla").val(tipo_plantilla);
	})

	$("body").on("click",".guardar_dimension",function(){
		var nueva_dimension = $(this).parents(".fila_dimension").find(".nueva_dimension").val();
		var tipo_plantilla = $(".tipo_plantilla").val();
		var id_rubrica = $("#id_rubrica").val();
		var cant_fila_dimension = parseInt($(".fila_dimension").length);
		var $this = $(this);

		var cant_niveles = $(".cant_niveles").val();
		if(!cant_niveles){ cant_niveles=2;}
		var colspan = parseInt(cant_niveles)+2;
		$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_rubrica': id_rubrica,
                'nombre': nueva_dimension,
                'tipo_pregunta': tipo_plantilla,
                'valor': "guardar_dimension"
            },
        }).done(function(resp) {
            if(resp.codigo=='OK'){
            	new PNotify({
					title: 'Mensaje',
					text: 'Dimensión guardada',
					delay: 1500
				});
            	
            	if(tipo_plantilla=="PE"){
            		$this.parents(".fila_dimension")
            				.before("<div class = 'row'>"+
										"<div class = 'col-lg-12'>"+
											"<div class = 'table-responsive' style = 'overflow:initial;'>"+
												'<table class="table table-striped clonado" id="tabla_creacion_dimensiones" cellpadding="4">'+
													'<tbody>'+
														'<tr class="cabecera fila_add_dimension">'+
															'<td width="90%" colspan="2" rowspan="2" class="text-center" style="background:#2196f3; color:#fff; border-right:2px solid #fff;"><h4>PONDERACIÓN ESTÁNDAR</h4></td>'+
															'<td width="10%" colspan="2" style="background:#2196f3;color:#fff;" class="fila_nivel text-center"><dt>NIVELES</dt></td>'+
														'</tr>'+
														'<tr class="por_nivel">'+
															'<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">'+
																'<h5>1</h5>'+
															'</td>'+
															'<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">'+
																'<h5>2</h5>'+
															'</td>'+
														'</tr>'+
														'<tr class="fila_dimension d_'+resp.id_ultimo+'" style="background:#fb8c00;color:#fff;" clase_dimension="'+resp.id_ultimo+'" data-iddimension="'+resp.id_ultimo+'">'+
															'<td colspan="'+colspan+'" class="td_dimension" style = "padding:10px;">'+
																'<div class="col-xs-8" style = "padding-top:5px;">'+
																	'<dt class = "let_dimension text-left" style = "display:inline;font-size:17px;">DIMENSIÓN '+cant_fila_dimension+': '+
																	'</dt>'+
																	'<dt class="reemplazo_dimension cambio_texto" style = "display:inline;font-size:17px;">'+nueva_dimension+
																	'</dt>'+
																'</div>'+

																'<div class="col-xs-4 text-right">'+


								'<ul class="nav navbar-top-links navbar-right">'+   
										"<li style = 'padding-right:0px;'>"+
											'NIVELES: '+
											'<select class = "cant_niveles" style = "color: #000;padding:5px;border-radius:6px;">'+
												'<option value = "2">2</option>'+
												'<option value = "3">3</option>'+
												'<option value = "4">4</option>'+
												'<option value = "5">5</option>'+
											'</select>&nbsp;&nbsp;'+
                            			"</li>"+             
                    						'<li class="dropdown" style = "padding-right:0px;">'+
                        						'<a class="dropdown-toggle btn btn-circle btn-info" title = "Elegir Plantilla" data-toggle="dropdown" href="#" style = "background-color: #31b0d5;">'+
                            						'<i class = "icon-list"></i>'+
                        						'</a>'+
                        						'<ul class="dropdown-menu dropdown-messages">'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='PE'>"+
																	"<img src = 'images/pond_estandar.png' class = 'img-responsive' title = 'Ponderación Estándar'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Ponderación Estándar'+
		                                    				'</div>'+
                                						'</a>'+
                            						'</li>'+
                            						'<li class="divider"></li>'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='SN'>"+
																	"<img src = 'images/si_no.png' class = 'img-responsive' title = 'SI-NO'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">SI-NO'+
		                                    				'</div>'+
                                						'</a>'+
                            						'</li>'+
                            						'<li class="divider"></li>'+
                            						'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='A'>"+
																	"<img src = 'images/alternativas.png' class = 'img-responsive' title = 'Alternativas'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Alternativas'+
		                                    				'</div>'+
                                						'</a>'+
					                            	'</li>'+
					                            	'<li class="divider"></li>'+
					                            	'<li>'+
                                						'<a href="#" class = "row">'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">'+
			                                       				"<div class = 'poner_imagen' tipo_plantilla='C'>"+
																	"<img src = 'images/autocompletar.png' class = 'img-responsive' title = 'Completar'>"+
																	"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
																"</div>"+
		                                    				'</div>'+
		                                    				'<div class = "col-lg-6 col-md-6 col-sm-6 col-xs-6">Completar'+
		                                    				'</div>'+
                                						'</a>'+
					                            	'</li>'+
					                            	
						                        '</ul>'+
						                    '</li>'+
									
										

                            			"<li style = 'padding-right:0px;'>"+
                            				'<button class="btn btn-primary btn-circle agregar_estandar istooltip" data-placement="bottom" title="Agregar Estándar">'+
												'<i class="icon-plus"></i>'+
											'</button>'+
                            			"</li>"+
									
										"<li style = 'padding-right:0px;'>"+
											'<button class="btn btn-success btn-circle guardar_puntuacion istooltip" id_dimension="'+resp.id_ultimo+'" data-toggle="modal" data-target="#ModalPuntuacion" data-placement="bottom" title="Establecer escalas de evaluación">'+
												'<i class="icon-th-large"></i>'+
											'</button>'+
                            			"</li>"+

                            			"<li style = 'padding-right:0px;'>"+
											'<button class="btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip" data-placement="bottom" title="Eliminar dimensión">'+
												'<i class="icon-trash"></i>'+
											'</button>'+
                            			"</li>"+


                            		"</ul>"+

		
																'</div>'+
															'</td>'+
														'</tr>'+
													'</tbody>'+
												'</table>'+
											"</div>"+
										"</div>"+
								   "</div>");
            		$this.parents(".fila_dimension").remove();
            	}
            	/*$("#area_botones").html("<div class = 'margin-top:10px'>"+
            								"Botones"+
            							"</div>");*/
            }
            else { console.log('Ocurrió un problema al actualizar error:'+resp.msg) }
            console.log("success");
        }).fail(function(err) {
            console.log('Algo salió mal');
            console.log(err.responseText);
        });
	});

	$("body").on("change",".cant_niveles",function(){
		var cant_niveles = $(this).val();
		var colspan = parseInt(cant_niveles)+parseInt(2);
		var id_dimension = $(this).parents(".fila_dimension").attr("clase_dimension");
		$(".fila_nivel").attr("colspan",cant_niveles);
		var columnas = '';
		var col_radio = '';
		var arreglo2 = {"niveles":cant_niveles};

		for(var i=1;i<=cant_niveles;i++)
		{
			columnas += "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>"+i+"</h5></td>";
			col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
		}
		$(this).parents("#tabla_creacion_dimensiones").find(".por_nivel").html(columnas);
		$(this).parents("#tabla_creacion_dimensiones").find(".td_dimension").attr("colspan",colspan);
		$(this).parents("#tabla_creacion_dimensiones").find(".option_radio").remove();
		$(this).parents("#tabla_creacion_dimensiones").find(".fila_pregunta").append(col_radio);
		$(this).parents("#tabla_creacion_dimensiones").find(".td_dimension").attr("colspan",colspan);
		$(this).parents("#tabla_creacion_dimensiones").find(".td_estandar").attr("colspan",colspan);

		$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_dimension': id_dimension,
                'niveles':arreglo2,
                'valor': "actualizar_niveles"
            },
        }).done(function(resp) {
            if(resp.codigo=='OK'){
            	//console.log("ya esta");
			}
		}).fail(function(err) {
            console.log('Error no actualiza niveles');
            console.log(err.responseText);
        });
	})

	$("body").on("click",".agregar_estandar",function(){
		var id_dimension = $(this).parents(".fila_dimension").attr("clase_dimension");
		var cant_fila_estandar = parseInt($(this).parents("#tabla_creacion_dimensiones").find("tr.fila_estandar").length);
		var cant_niveles=$(this).parents("#tabla_creacion_dimensiones").find(".cant_niveles").val();
		var letra1 = cant_fila_estandar+97;
		var letra = String.fromCharCode(letra1).toUpperCase();
		var colspan = parseInt(cant_niveles)+parseInt(2);

		$(this).parents("#tabla_creacion_dimensiones")
			.append('<tr style="background:#e8e8e7;" class="fila_estandar editando d_'+id_dimension+'" clase_dimension="'+id_dimension+'">'+
					'<td class="td_estandar" colspan="'+colspan+'">'+
						'<div class="col-xs-10" style = "padding-top:5px;">'+
							'<dt class = "let_estandar text-right" style = "display:inline;padding-top:5px;">'+
							letra+') '+
							'</dt>'+
							'<div class = "new_estandar" style="display:inline;margin-top:5px;">'+
								'<input type="text" class="form-control nuevo_estandar" placeholder="Escriba aquí el estándar" style = "width: 80%;display:inline;">'+
							'</div>'+
						'</div>'+
						'<div class="col-xs-2 text-right btn_estandar">'+
							'<button class="btn btn-circle btn-primary save_estandar">'+
								'<i class="icon-save"></i>'+
							'</button>'+
							'<button class="btn btn-circle btn-default cancelar_add_estandar">'+
								'<i class="icon-remove"></i>'+
							'</button>'+
						'</div>'+
					'</td>'+
				'</tr>');
	});

	$("body").on("click",".save_estandar",function(){
		var nombre = $(".nuevo_estandar").val();
		var id_dimension = $(this).parents(".fila_estandar").attr("clase_dimension");
		var $this = $(this);

		$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_dimension': id_dimension,
                'nombre':nombre,
                'valor': "guardar_estandar"
            },
        }).done(function(resp) {
            if(resp.codigo=='OK'){

            	new PNotify({
					title: 'Mensaje',
					text: 'Estándar guardado',
					delay: 1500
				});
				$this.parents(".fila_estandar").attr("clase_estandar",resp.id_ultimo);
				$this.parents(".fila_estandar").addClass("e_"+resp.id_ultimo);
				$this.parents(".fila_estandar").find(".btn_estandar").html('<button class="btn btn-success btn-circle agregar_pregunta no_imprimir" data-mytooltip="tooltip1" data-placement="bottom" title="Agregar Pregunta"><i class="icon-plus"></i></button>'+
					'<button class="btn btn-danger btn-circle eliminar_estandar no_imprimir" data-mytooltip="tooltip1" data-placement="bottom" title="Eliminar estándar"><i class="icon-trash"></i></button>');
				$(".new_estandar").replaceWith('<div class="reemplazo_estandar cambio_texto" style = "padding-top:5px;display:inline;">'+
													nombre+
												'</div>');
				
			}
		}).fail(function(err) {
            console.log('Error no guarda estándar');
            console.log(err.responseText);
        });
	});	


	$("body").on("click",".agregar_pregunta",function(){
		var id_dimension = $(this).parents(".fila_estandar").attr("clase_dimension");
		var id_estandar = $(this).parents(".fila_estandar").attr("clase_estandar");
		var colspan = $(this).parents("#tabla_creacion_dimensiones").find(".cant_niveles").val();
		var cant_fila_pregunta = parseInt($("#tabla_creacion_dimensiones tr.fila_pregunta").length)+1;

		$(this).parents("#tabla_creacion_dimensiones").find("tr.e_"+id_estandar).last()
			.after('<tr style="background:#e8e8e7;" class="fila_pregunta editando" clase_dimension="'+id_dimension+'" clase_estandar="'+id_estandar+'">'+
					'<td width="5%" style="background: #e0e8ef; text-align: right;">'+
						'<div class="let_pregunta" style="display:inline;">'+
							cant_fila_pregunta+'.- '+
						'</div>'+
					'</td>'+
					'<td style="background: #fcfce0;">'+
						'<div class="new_pregunta" style="display:inline;">'+
							'<input type="text" style="width:90%;display:inline;" class="form-control nueva_pregunta" placeholder="Escriba aquí la pregunta">'+
						'</div>'+
					'</td>'+
					'<td colspan="'+colspan+'" class="niveles">'+
						'<button class="btn btn-circle btn-primary save_pregunta">'+
							'<i class="icon-save"></i>'+
						'</button>'+
						'<button class="btn btn-circle btn-default cancelar_add_pregunta">'+
							'<i class="icon-remove"></i>'+
						'</button>'+
					'</td>'+
				'</tr>');
	});	

	$("body").on("click",".save_pregunta",function(){
		var id_estandar = $(this).parents(".fila_pregunta").attr("clase_estandar");
		var nombre = $(".nueva_pregunta").val();
		var cant_niveles=$(this).parents("#tabla_creacion_dimensiones").find(".cant_niveles").val();
		var $this = $(this);
		var col_radio = '';
		for(var i=0;i<cant_niveles;i++)
		{
			col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
		}

		$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_estandar': id_estandar,
                'nombre':nombre,
                'otros_datos': '',
                'valor': "guardar_pregunta"
            }
        }).done(function(resp) {
            if(resp.codigo=='OK'){

				$this.parents(".fila_pregunta").attr("clase_pregunta",resp.id_ultimo);
				$this.parents(".fila_pregunta").addClass("e_"+id_estandar);
				$this.parents(".niveles").replaceWith(col_radio);

				$(".new_pregunta").replaceWith('<div class="col-xs-9 reemplazo_pregunta cambio_texto" style = "padding-top:5px;">'+
													nombre+
												'</div>');
				}
		}).fail(function(err) {
            console.log('Error no guarda pregunta');
            console.log(err.responseText);
        });
	});	

	$("body").on("click",".cancelar_add_dimension",function(){
		$(this).parents(".fila_dimension").remove();
	});

	$("body").on("click",".cancelar_add_estandar",function(){
		$(this).parents(".fila_estandar").remove();
	});	

	$("body").on("click",".cancelar_add_pregunta",function(){
		$(this).parents(".fila_pregunta").remove();
	});



	








            /*************************************************/

            /***********MODAL PUNTUACION***************/

            $(".guardar_puntuacion").click(function(){

                var id_dimension = $(this).attr("id_dimension");

                $.ajax({
                    url: "guardar_puntuacion.php",
                    type: "POST",
                    data: {
                        id_dimension: id_dimension
                    },
                    success: function(result){
                        $("#puntuacion").html(result);
                    }
                });

            });

            /****************************************/



            $(".pond_estandar").click(function(){

                $.ajax({
                    url: "ponderacion_estandar.php",
                    type: "POST",
                    success: function(result){
                        $("#ponderacion_estandar").html(result);
                        $('#ponderacion_estandar .modal-header h4 strong').text('PONDERACIÓN ESTANDAR');
                        $('#ponderacion_estandar #tipo_pregunta').attr('value', 'PE');
                    }
                });
                $(".aside").hide();
            });

            $(".alternativas").click(function(){

                $.ajax({
                    url: "alternativas.php",
                    type: "POST",
                    success: function(result){
                        $("#alternativas").html(result);
                    }
                });
                $(".aside").hide();
            });

            $(".si_no").click(function(){

                $.ajax({
                    url: "ponderacion_estandar.php",
                    type: "POST",
                    success: function(result){
                        $("#si_no").html(result);
                        $('#si_no .modal-header h4 strong').text('SI - NO');
                        $('#si_no .cantidad_de_niveles').hide().addClass('hidden'); 
                        $('#si_no #tipo_pregunta').attr('value', 'SN');
                    }
                });
                $(".aside").hide();
            });

            $(".autocompletar").click(function(){

                $.ajax({
                    url: "autocompletar.php",
                    type: "POST",
                    success: function(result){
                        $("#autocompletar").html(result);
                    }
                });
                $(".aside").hide();
            });



            var con_cambio2 = function(obj,input_type,nombre)
            {
                if(input_type=="titulo")
                {
                    $(obj).replaceWith("<input type = 'text' class = 'form-control titulo_rubrica' value = '"+nombre+"' placeholder = 'Escriba aquí la dimensión'>");
                    $(".titulo_rubrica").focus();


                }
                else if(input_type=="descripcion")
                {
                    $(obj).replaceWith("<textarea class = 'form-control descripcion_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí el estandar'>"+nombre+"</textarea>");
                    $(".descripcion_rubrica").focus();

                }
                else
                {
                    $(obj).replaceWith("<textarea class = 'form-control pregunta_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí la pregunta'>"+nombre+"</textarea>");
                    $(".pregunta_rubrica").focus();
                }
            };

            var sin_cambio2 = function(obj,input,input_type)
            {
                if(input_type=="titulo"){ $(obj).replaceWith(input);}
                if(input_type=="descripcion"){ $(obj).replaceWith(input); }
                if(input_type=="pregunta"){ $(obj).replaceWith(input); }
            };

            $("body").on("click",".escriba_titulo",function(){
                var nombre = $(this).attr("nombre");
                if(nombre.length==0){nombre="";}
                con_cambio2($(this),"titulo",nombre);
            });

            $("body").on("click",".escriba_descripcion",function(){
                var nombre = $(this).attr("nombre");
                if(nombre.length==0){nombre="";}
                con_cambio2($(this),"descripcion",nombre);
            });

            $("body").on("click",".escriba_pregunta",function(){
                var nombre = $(this).attr("nombre");
                if(nombre.length==0){nombre="";}
                con_cambio2($(this),"pregunta",nombre);
            });

            $("body").on("blur",".titulo_rubrica",function(){
                var nombre = $(this).val(); nombre1=nombre;
                if(nombre.length==0)
                {
                    nombre = "Escriba aquí la Dimensión";
                    nombre1 = "";
                }

                var input1 = "<div class = 'escriba_titulo' nombre = '"+nombre1+"'>"+nombre+"</div>";
                sin_cambio2($(this),input1,"titulo");
            });

            $("body").on("blur",".descripcion_rubrica",function(){
                var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;
                if(nombre_desc.length==0)
                {
                    nombre_desc = "Escriba aquí el Estándar";
                    nombre1_desc = "";
                }
                var input2 = "<div class = 'escriba_descripcion' nombre = '"+nombre1_desc+"' style = 'margin-top:20px;'>"+nombre_desc+"</div>";
                sin_cambio2($(this),input2,"descripcion");
            });

            $("body").on("blur",".pregunta_rubrica",function(){
                var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;

                if(nombre_desc.length==0){
                    nombre_desc = "Escriba aquí la pregunta";
                    nombre1_desc = "";
                }
                var input2 = "<div class = 'escriba_pregunta' nombre = '"+nombre1_desc+"' style = 'margin-top:20px;'>"+nombre_desc+"</div>";
                sin_cambio2($(this),input2,"pregunta");
            });

            $("#ponderacion_estandar").on('change', '.cant_niveles', function(e){
                var cant_niveles = $(this).val();
                $(".fila_nivel").attr("colspan",cant_niveles);
                var columnas = '';
                var col_radio = '';
                var colspan = parseInt(cant_niveles)+parseInt(2);
                var niveles = JSON.stringify({'niveles':cant_niveles});

                for(var i=1;i<=cant_niveles;i++) {
                    columnas += "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>"+i+"</h5></td>";
                    col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
                }
                $(".por_nivel").html(columnas);
                $(".option_radio").remove();
                $(".fila_pregunta").append(col_radio);
                $(".td_dimension").attr("colspan",colspan);
                $(".td_estandar").attr("colspan",colspan);

                /******actualizando niveles*******/
                var arrIdDimensiones=[];
                $('#ponderacion_estandar tr.fila_dimension').each(function(index, el) {
                    arrIdDimensiones.push($(this).attr('data-iddimension'));
                });
                if(arrIdDimensiones.length>0){
                    $.ajax({
                        url: 'grabar_ajax.php',
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            'otros_datos': niveles,
                            'arrIdDimensiones': JSON.stringify(arrIdDimensiones),
                            'valor': 'actualizar_niveles_dimension',
                        },
                    }).done(function(resp) {
                        if(resp.code=='OK'){ console.log('Niveles actualizados.'); }
                        else { console.log('Ocurrió un problema al actualizar niveles:'+resp.msg) }
                        console.log("success");
                    }).fail(function(err) {
                        console.log('Algo salió mal');
                        console.log(err.responseText);
                    });
                }
            })

            $(".guardar_examen").click(function(){
                var titulo = $(".escriba_titulo").attr("nombre");
                var descripcion = $(".escriba_descripcion").attr("nombre");
                var cant_nivel = $(".cant_niveles").val();
                var id_encuesta = $("#id_encuesta").val();
                var obj = {};

                $(".fila_dimension").each(function(){
                    var id_dimension = $(this).attr("id_dimension");
                    obj[id_dimension] = {};

                    $(".fila_estandar.d_"+id_dimension).each(function(){
                        var id_estandar = $(this).attr("id_estandar");
                        obj[id_dimension][id_estandar] =[];    
                        $(".fila_pregunta.e_"+id_estandar).each(function(){
                            var id_pregunta = $(this).attr("id_pregunta");
                            obj[id_dimension][id_estandar].push(id_pregunta);
                        });
                    });
                })

                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        titulo: titulo,
                        descripcion: descripcion,
                        cant_nivel: cant_nivel,
                        obj: obj,
                        id_encuesta: id_encuesta,
                        valor: "guardar_examen"
                    }
                }).done(function(res){
                    new PNotify({
                        title: 'Mensaje',
                        text: 'Rúbrica guardada',
                        delay: 2000

                    });
                }).fail(function(error){
                    console.log("error-no-guarda-examen");
                })
            });


            /******--------*******/

            $("#ponderacion_estandar, #si_no").on("click",".guardar_ponderacion",function(){
                /*var tblPoneracion = $('#ponderacion_estandar #tabla_creacion_dimensiones').clone();
                tblPoneracion.find('.no_imprimir').remove();
                $('.sector').before(tblPoneracion); */
                $('.sector').before('<img src="./images/loader.gif" class="center-block" alt="Cargando">');
                setTimeout (function(){
                    location.href='evaluacion.php';
                }, 500);
            });

            var agregarNuevaDimension = function($tr_dimension, objDimension){
                $tr_dimension.attr("clase_dimension",objDimension.id_dimension);
                $tr_dimension.attr("data-iddimension", objDimension.id_dimension);
                $tr_dimension.find(".new_dimension").replaceWith("<div class='col-xs-8 reemplazo_dimension cambio_texto'>"+objDimension.nombre+"</div>");

                $tr_dimension.addClass("d_"+objDimension.id_dimension);
                $tr_dimension.find(".btn.elegir_dimension").replaceWith("<button class='btn btn-success btn-circle guardar_puntuacion istooltip' id_dimension="+objDimension.id_dimension+" data-toggle='modal' data-target='#ModalPuntuacion' data-placement='bottom' title='Establecer escalas de evaluación'><i class='icon-th-large'></i></button>");
                $tr_dimension.find(".btn.cancelar_add_dimension").replaceWith("<button class = 'btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip' data-placement='bottom' title='Eliminar dimensión'><i class = 'icon-trash'></i></button>");
                $tr_dimension.find(".btn.save_dimension").replaceWith("<button class = 'btn btn-primary btn-circle agregar_estandar no_imprimir istooltip' data-placement='bottom' title='Agregar Estándar'><i class = 'icon-plus'></i></button>");

                $tr_dimension.removeClass('editando');

                $("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
            };

            var agregarNuevoEstandar =  function($tr_estandar, objEstandar){
                var let_estandar = $tr_estandar.find(".let_estandar").text();
                $tr_estandar.attr("clase_estandar",objEstandar.id_estandar);
                $tr_estandar.attr("data-idestandar",objEstandar.id_estandar);
                $tr_estandar.find(".new_estandar").replaceWith("<div class = 'col-xs-9 reemplazo_estandar cambio_texto'>"+objEstandar.nombre+"</div>");
                $tr_estandar.find("td").css("padding","5px");
                $tr_estandar.addClass("e_"+objEstandar.id_estandar);
                $tr_estandar.find(".let_estandar").text(let_estandar);
                $tr_estandar.find(".btn.cancelar_add_estandar").remove();
                $tr_estandar.find(".btn.save_estandar").replaceWith("<button class = 'btn btn-success btn-circle agregar_pregunta no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Agregar Pregunta'><i class = 'icon-plus'></i>"+
                    "</button><button class = 'btn btn-danger btn-circle eliminar_estandar no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Eliminar estándar'><i class = 'icon-trash'></i></button>");
                $tr_estandar.removeClass('editando');
                $("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
            };

            var agregarNuevaPregunta = function ($tr_pregunta, objPregunta) {
                var cant_niveles = $('#ponderacion_estandar').find(".cant_niveles").val();
                var columnas = '';
                $tr_pregunta.attr("clase_pregunta",objPregunta.id_pregunta);
                $tr_pregunta.attr("data-idpregunta",objPregunta.id_pregunta);
                $tr_pregunta.find(".new_pregunta").replaceWith("<div class='reemplazo_pregunta cambio_texto' style = 'display:inline;'>"+objPregunta.nombre+"</div>"+
                    "<div style='display: inline; float: right'><button class = 'btn btn-danger btn-circle eliminar_pregunta no_imprimir istooltip' data-placement='bottom' title='Eliminar pregunta'><i class = 'icon-trash'></i></button></div>");
                $tr_pregunta.find(".cancelar_add_pregunta").remove();
                $tr_pregunta.find("td").css("padding","5px");
                $tr_pregunta.addClass("p_"+objPregunta.id_pregunta);

                for(var i=1;i<=cant_niveles;i++) {
                    columnas += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
                }
                $tr_pregunta.find('.save_pregunta').closest("td").remove().append(columnas);
                $tr_pregunta.append(columnas);

                $tr_pregunta.removeClass('editando');
                $("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                $("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
            };

            var con_cambio = function(obj,valor_nuevo,estilo){
                //$(obj).closest('.cambio_texto').addClass(estilo);
                $(obj).closest('.cambio_texto').attr("valor_estatico",valor_nuevo);
                $(obj).closest('.cambio_texto').html(valor_nuevo);
            };

            var sin_cambio = function(obj,valor_estatico,estilo){
                //obj.closest('.cambio_texto').addClass(estilo);
                obj.closest('.cambio_texto').html(valor_estatico);
            };

            $("#ponderacion_estandar, #si_no").on("click",".fila_dimension .save_dimension",function(e){
                var $this= $(this);
                var nombre = $(".nueva_dimension").val().trim();
                var let_dimension = $(this).parents("tr").find(".let_dimension").text();
                var cant_fila_dimension = parseInt($("#tabla_creacion_dimensiones tr.fila_dimension").length);
                var niveles={
                    'niveles': $(".cant_niveles").val(),
                };
                if(nombre.length==0){
                    $(".nueva_dimension").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nueva_dimension' style = 'display:inline;' placeholder = 'Ingrese una dimensión'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
                    $(".nueva_dimension").addClass("borde_rojo");
                } else {
                    $.ajax({
                        url: "grabar_ajax.php",
                        type: "POST",
                        dataType: "json",
                        data: {
                            'id_rubrica': $("#id_encuesta").val(),
                            'nombre_dim': nombre,
                            'tipo_pregunta': $('#tipo_pregunta').val(),
                            'escalas_evaluacion' : '',
                            'otros_datos': JSON.stringify(niveles),
                            'activo':1,
                            'valor': "grabar_dimension"
                        },
                        beforeSend: function(){
                            $(".nueva_dimension").attr('disabled','disabled');
                            $(".nueva_dimension").closest('tr').find('.btn').attr('disabled','disabled');
                        }
                    }).done(function(res){
                        if(res.code="OK"){
                            var $fila_dimension = $this.closest('tr.fila_dimension.editando');
                            var obj = {
                                'id_dimension': res.data,
                                'nombre': nombre
                            };
                            agregarNuevaDimension($fila_dimension, obj);

                            $(".agregar_dimension").removeAttr("disabled");

                            /*$("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                            $("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                            $("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                            $("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");*/
                        } else {
                            new PNotify({
                                title: 'Error',
                                text: res.msg,
                                type: 'error',
                            });
                        }
                    }).fail(function(err){
                        new PNotify({
                            title: 'Error',
                            text: 'Algo salió mal',
                            type: 'error',
                        });
                        console.log(err.responseText);
                    });
                }
            }).on("click", ".fila_dimension .eliminar_dimension", function(e){
                var $this = $(this);
                var id_dimension = $(this).closest('tr.fila_dimension').attr('data-iddimension');
                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id_dimension': id_dimension,
                        'valor': "eliminar_fisica_dimension",
                    },
                    beforeSend: function () {
                        $this.closest('tr.fila_dimension').find('.btn').attr('disabled','disabled');
                    },
                }).done(function(res){
                    if(res.code="OK"){
                        var classDim = 'd_'+$this.closest('tr.fila_dimension').attr('clase_dimension');
                        $this.closest('tr').siblings('tr.'+classDim).remove();
                        $this.closest('tr.fila_dimension').remove();
                    }else {
                        new PNotify({
                            title: 'Error',
                            text: res.msg,
                            type: 'error',
                        });
                    }
                }).fail(function(err){
                    new PNotify({
                        title: 'Error',
                        text: 'Algo salió mal',
                        type: 'error',
                    });
                    console.log(err.responseText);
                });
            }).on('click', '.fila_dimension .elegir_dimension', function(e) {
                e.preventDefault();
                $.get('./mantenimiento.php', function(data) {
                    $('#dimensiones_bd .modal-body').html(data);
                });
            }).on("click",".fila_estandar .save_estandar",function(e){
                var nombre = $(".nuevo_estandar").val();
                var let_estandar = $(this).parents("tr").find(".let_estandar").text();
                var cant_fila_estandar = parseInt($("#tabla_creacion_dimensiones tr.fila_estandar").length);
                var $this=$(this);
                if(nombre.length==0) {
                    $(".nuevo_estandar").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nuevo_estandar' placeholder = 'Ingrese un estándar'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
                    $(".nuevo_estandar").addClass("borde_rojo");
                } else {
                    $.ajax({
                        url: 'grabar_ajax.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id_dimension': $this.closest('.fila_estandar').attr('clase_dimension'),
                            'nombre_est': nombre,
                            'activo': 1,
                            'valor': "grabar_estandar"
                        },
                        beforeSend: function(){
                            $(".nuevo_estandar").attr('disabled','disabled');
                            $(".nuevo_estandar").closest('tr').find('.btn').attr('disabled','disabled');
                        }
                    }).done(function(resp) {
                        if(resp.code=='OK'){
                            var $fila_estandar = $this.closest('tr.fila_estandar.editando');
                            var obj = {
                                'id_estandar': resp.data,
                                'nombre': nombre,
                            };
                            agregarNuevoEstandar($fila_estandar, obj);
                        } else {
                            new PNotify({
                                title: 'Error',
                                text: resp.msg,
                                type: 'error',
                            });
                        }
                    }).fail(function(err){
                        new PNotify({
                            title: 'Error',
                            text: 'Algo salió mal',
                            type: 'error',
                        });
                        console.log(err.responseText);
                    });
                    
                }
            }).on("click",".fila_pregunta .save_pregunta",function(e){
                var nombre = $(".nueva_pregunta").val();
                var let_pregunta = $(this).parents("tr").find(".let_pregunta").text();
                var cant_fila_pregunta = parseInt($("#tabla_creacion_dimensiones tr.fila_pregunta").length);
                var $this=$(this);
                var cant_niveles = $(".modal-body .cant_niveles").val();
                var columnas = '';
                if(nombre.length==0) {
                    $(".nueva_pregunta").replaceWith("<div style = 'margin-bottom: 0px;display:inline;position:relative;'><input type = 'text' class='form-control nueva_pregunta' style = 'width:90%;display:inline;' placeholder = 'Ingrese una pregunta'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red' style = 'top:-10px;'></i></div>");
                    $(".nueva_pregunta").addClass("borde_rojo");
                } else {
                    $.ajax({
                        url: 'grabar_ajax.php',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            'id_estandar': $this.closest('tr.fila_pregunta').attr('clase_estandar'),
                            'nombre_preg': nombre,
                            'activo': 1,
                            'valor': 'grabar_pregunta',
                        },
                        beforeSend: function(){
                            $(".nueva_pregunta").attr('disabled','disabled');
                            $(".nueva_pregunta").closest('tr').find('.btn').attr('disabled','disabled');
                        }
                    }).done(function(resp) {
                        if(resp.code=='OK'){
                            var $fila_pregunta = $this.closest('tr');
                            var obj ={
                                'id_pregunta': resp.data,
                                'nombre': nombre,
                            };
                            agregarNuevaPregunta($fila_pregunta, obj);
                        }else {
                            new PNotify({
                                title: 'Error',
                                text: resp.msg,
                                type: 'error',
                            });
                        }
                    }).fail(function(err){
                        new PNotify({
                            title: 'Error',
                            text: 'Algo salió mal',
                            type: 'error',
                        });
                        console.log(err.responseText);
                    });
                }
            }).on('click', '.fila_estandar .eliminar_estandar', function(e) {
                e.preventDefault();
                var $this = $(this);
                var id_estandar = $(this).closest('tr.fila_estandar').attr('data-idestandar');
                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id_estandar': id_estandar,
                        'valor': "eliminar_fisica_estandar",
                    },
                    beforeSend: function () {
                        $this.closest('tr.fila_estandar').find('.btn').attr('disabled','disabled');
                    },
                }).done(function(res){
                    if(res.code="OK"){
                        var clase = 'e_'+$this.closest('tr.fila_estandar').attr('clase_estandar');
                        $this.closest('tr').siblings('tr.'+clase).remove();
                        $this.closest('tr.fila_estandar').remove();
                    }else {
                        new PNotify({
                            title: 'Error',
                            text: res.msg,
                            type: 'error',
                        });
                    }
                }).fail(function(err){
                    new PNotify({
                        title: 'Error',
                        text: 'Algo salió mal',
                        type: 'error',
                    });
                    console.log(err.responseText);
                });
            }).on('click', '.fila_pregunta .eliminar_pregunta', function(e) {
                e.preventDefault();
                var $this = $(this);
                var id_pregunta = $(this).closest('tr.fila_pregunta').attr('data-idpregunta');
                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id_pregunta': id_pregunta,
                        'valor': "eliminar_fisica_pregunta",
                    },
                    beforeSend: function () {
                        $this.closest('tr.fila_pregunta').find('.btn').attr('disabled','disabled');
                    },
                }).done(function(res){
                    if(res.code="OK"){
                        var clase = 'p_'+$this.closest('tr.fila_pregunta').attr('clase_pregunta');
                        $this.closest('tr').siblings('tr.'+clase).remove();
                        $this.closest('tr.fila_pregunta').remove();
                    }else {
                        new PNotify({
                            title: 'Error',
                            text: res.msg,
                            type: 'error',
                        });
                    }
                }).fail(function(err){
                    new PNotify({
                        title: 'Error',
                        text: 'Algo salió mal',
                        type: 'error',
                    });
                    console.log(err.responseText);
                });
            }).on("keypress","input.nueva_dimension",function(e){
                e.stopPropagation();
                if(e.which == 13) { 
                    var $tr_padre = $(this).parents("tr");
                    var hermano = $tr_padre.find(".save_dimension");
                    $(hermano).trigger("click"); 
                }
            }).on("keypress","input.nuevo_estandar",function(e){
                e.stopPropagation();
                if(e.which == 13) { 
                    var $tr_padre = $(this).parents("tr");
                    var hermano = $tr_padre.find(".save_estandar");
                    $(hermano).trigger("click"); 
                }
            }).on("keypress","input.nueva_pregunta",function(e){
                e.stopPropagation();
                if(e.which == 13) { 
                    var $tr_padre = $(this).parents("tr");
                    var hermano = $tr_padre.find(".save_pregunta");
                    $(hermano).trigger("click"); 
                }
            });


            $("#ponderacion_estandar, #si_no").on("click",".cambio_texto",function(){
                var valor_estatico = $(this).attr("valor_estatico") || $(this).text();
                if($(this).find('input.cambio').length==0){
                    //var valor_estatico = $(this).text();
                    $(this).attr("valor_estatico", valor_estatico);
                    $(this).html("<input type='text' class='form-control cambio' value='"+valor_estatico+"' >");
                    $(".cambio").focus();
                }
                //$(this).parents("#tblDimensiones").find(".cambio_texto").removeClass("cambio_texto");
            }).on("blur",".cambio_texto input.cambio",function(e){
                var $this = $(this);
                var $fila = $this.closest('tr');
                var valor_estatico = $this.closest('.cambio_texto').attr("valor_estatico");
                var valor_nuevo = $this.val().trim();
                var tecla_esc = $this.closest('.cambio_texto').attr("tecla_esc");
                var dataAjax = {
                    'nombre':valor_nuevo,
                    'valor': "",
                };
                if($this.closest("tr").hasClass('fila_dimension')){
                    var id = $this.closest("tr").attr("data-iddimension");
                    dataAjax['id_dimension'] = id;
                    dataAjax['valor'] = 'editar_dimension';
                } else if ($this.closest("tr").hasClass('fila_estandar')){
                    var id = $this.closest("tr").attr("data-idestandar");
                    dataAjax['id_estandar'] = id;
                    dataAjax['valor'] = 'editar_estandar';
                } else if($this.closest("tr").hasClass('fila_pregunta')){
                    var id = $this.closest("tr").attr("data-idpregunta");
                    dataAjax['id_pregunta'] = id;
                    dataAjax['valor'] = 'editar_pregunta';
                }

                if(valor_nuevo.length>0 && valor_estatico != valor_nuevo && tecla_esc == "no") {
                    $.ajax({
                        url: "grabar_ajax.php",
                        type: "POST",
                        dataType: "json",
                        data: dataAjax,
                        beforeSend: function(){
                            $this.attr('disabled','disabled');
                            $fila.find('.btn').attr('disabled','disabled');
                        }
                    }).done(function(res){
                        if(res.codigo=="OK") {
                            con_cambio($this, valor_nuevo, "cambio_texto");
                            new PNotify({
                                title: 'Listo',
                                text: 'Se actualizó el valor',
                                delay: 1500,
                                type:'success',
                            });
                        } else {
                            sin_cambio($this, valor_estatico, 'cambio_texto');
                        }
                    }).fail(function(error){
                        console.log("error-no-edita-dimension");
                        new PNotify({
                                title: 'Error',
                                text: 'Ocurrió un error. No se guardaron los cambios.',
                                delay: 3000,
                                type: 'error',

                            });
                        sin_cambio($this, valor_estatico, 'cambio_texto');
                    }).always(function(){
                        console.log($fila);
                        $fila.find('.btn').removeAttr('disabled');
                    });
                    
                } else if(valor_nuevo.length==0) {
                    $(".cambio").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><input type='text' class='form-control cambio' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></div>");
                    $(".cambio").addClass("borde_rojo");
                } else if(valor_estatico == valor_nuevo) {
                    con_cambio($this ,valor_nuevo, "cambio_texto");
                }
                $(this).closest('.cambio_texto').attr("tecla_esc","no");    
            }).on("keypress",".cambio_texto input.cambio",function(e){
                $(this).removeClass("borde_rojo");
                if(e.which == 13) {
                    $(this).closest('.cambio_texto').attr("tecla_esc","no");    
                    var padre = $(this).parents("tr");
                    var hermano = $(padre).find(".cambio");
                    $(hermano).trigger("blur"); 
                }
            }).on("keyup",".cambio_texto input.cambio",function(e){
                if(e.which == 27) { 
                    var $this = $(this);
                    $(this).closest('.cambio_texto').attr("tecla_esc","si");
                    var valor_estatico = $this.closest('.cambio_texto').attr("valor_estatico");
                    var valor_nuevo = $this.val();
                    sin_cambio($this, valor_estatico, "cambio_texto");
                }
            });


            $('#dimensiones_bd').on("click",".mostrar_estandares",function(){
                var $fila = $(this).closest('tr');
                $('#tblDimensiones tr.fila_agregada').remove();
                $('.btn.mostrar_estandares').find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
                if( $fila.hasClass('open') ){
                    $('#tblDimensiones tr.dimension').removeClass('open');
                    $(this).find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
                } else {
                    var cant_cols = $fila.find('td').length;
                    var id_dimension = $fila.attr("id_dimension");

                    $('#tblDimensiones tr.dimension').removeClass('open');
                    $(this).find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');

                    $fila.addClass('open');
                    $fila.after('<tr class="fila_agregada estandar">'+
                        '<td colspan="'+cant_cols+'"><div class="tabla_estandar">'+
                        '<img src="./images/loader.gif" alt="Cargando" class="center-block">'+
                        '</div></td>'+
                        '</tr>');
                    $.ajax({
                        url: "ver_estandares.php",
                        type: "POST",
                        data: { id_dimension: id_dimension},
                        success: function(result){
                            $(".fila_agregada.estandar .tabla_estandar").html(result);
                        }
                    });
                }
            }).on("click",".mostrar_preguntas",function(){
                var $fila_est = $(this).closest('tr');
                $('#tblEstandares tr.fila_agregada').remove();
                $('.btn.mostrar_preguntas').find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
                if( $fila_est.hasClass('open') ){
                    $('#tblEstandares tr.estandar').removeClass('open');
                } else {
                    var cant_cols= $fila_est.find("td").length;
                    var id_estandar = $fila_est.attr("id_estandar");

                    $('#tblEstandares tr.estandar').removeClass('open');
                    $(this).find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');

                    $fila_est.addClass('open');
                    $fila_est.after('<tr class="fila_agregada pregunta">'+
                        '<td colspan="'+cant_cols+'"><div class="tabla_pregunta">'+
                        '<img src="./images/loader.gif" alt="Cargando" class="center-block">'+
                        '</div></td>'+
                        '</tr>');
                    $.ajax({
                        url: "lista_preguntas.php",
                        type: "POST",
                        data: { id_estandar: id_estandar },
                        success: function(result){
                            $(".fila_agregada.pregunta .tabla_pregunta").html(result);
                        }
                    });
                }
            }).on('click', '.seleccionar_dim', function(e) {
                e.preventDefault();
                var id_dimension = $(this).closest('tr').attr('id_dimension');
                if($('#tipo_pregunta').val()=='PE'){var $modalPE = $('#ponderacion_estandar');}
                if($('#tipo_pregunta').val()=='SN'){var $modalPE = $('#si_no');}
                var niveles={
                    'niveles': $(".cant_niveles").val(),
                }; 
                $.ajax({
                    url: 'grabar_ajax.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'id_dimension' : id_dimension,
                        'tipo_pregunta': $('#tipo_pregunta').val(),
                        'otros_datos': JSON.stringify(niveles),
                        'id_rubrica' : $('#id_encuesta').val(),
                        'valor': 'grabar_dimension_bd',
                    },
                    beforeSend: function() {
                        $('#dimensiones_bd #tblDimensiones .btn.seleccionar_dim').attr('disabled', 'disabled');
                    }
                }).done(function(resp) {
                    if(resp.code="OK"){
                        $('#dimensiones_bd button[data-dismiss="modal"]').trigger('click');
                        var dim=resp.data;
                        var $fila_dimension = $modalPE.find('.fila_dimension.editando');
                        var colspan = $modalPE.find(".cant_niveles").val();

                        agregarNuevaDimension($fila_dimension, dim)
                        if(dim.estandar.length>0){
                            $.each(dim.estandar, function(key, est) {
                                var cant_fila_estandar = parseInt($modalPE.find(".fila_estandar.d_"+dim.id_dimension).length);
                                var letra = cant_fila_estandar+97;
                                $modalPE.find('tr.d_'+dim.id_dimension).last().after('<tr style="background:#e8e8e7;" class="fila_estandar editando d_'+dim.id_dimension+'" clase_dimension="'+dim.id_dimension+'">'+
                                        '<td class="td_estandar" colspan="'+(parseInt(colspan)+2)+'">'+
                                            '<div class="col-xs-1 let_estandar text-right" style="text-transform: uppercase;">'+String.fromCharCode(letra).toUpperCase()+')</div>'+
                                            '<div class="col-xs-9 new_estandar"></div>'+
                                            '<div class="col-xs-2 text-right">'+
                                                '<button class="btn btn-circle btn-primary save_estandar"><i class="icon-save"></i></button>'+
                                                '<button class="btn btn-circle btn-default cancelar_add_estandar"><i class="icon-remove"></i></button>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>');
                                var $fila_est=$('#tabla_creacion_dimensiones .fila_estandar.editando');
                                agregarNuevoEstandar($fila_est, est);

                                if(est.pregunta.length>0){
                                    
                                    $.each(est.pregunta, function(key, preg) {
                                        var nroPreg = parseInt($modalPE.find("tr.fila_pregunta").length)+1;
                                        $modalPE.find("tr.e_"+est.id_estandar).last().after("<tr style='background:#e8e8e7;' class='fila_pregunta editando d_"+dim.id_dimension+" e_"+est.id_estandar+"' clase_dimension='"+dim.id_dimension+"' clase_estandar='"+est.id_estandar+"'>"+
                                            "<td width='12%' style='background: #e0e8ef; text-align: right;'><div class='let_pregunta' style='display:inline;'>"+nroPreg+".- </div>"+
                                            "</td>"+
                                            "<td style = 'background: #fcfce0;'><div class = 'new_pregunta' style = 'display:inline;'></div>"+
                                            "</td>"+
                                            "<td colspan = '"+colspan+"' class = 'niveles'>"+
                                                "<button class = 'btn btn-circle btn-primary save_pregunta'><i class = 'icon-save'></i></button>"+
                                                "<button class = 'btn btn-circle btn-default cancelar_add_pregunta'><i class = 'icon-remove'></i></button>"+
                                            "</td>"+
                                        "</tr>");
                                        var $fila_preg=$('#tabla_creacion_dimensiones .fila_pregunta.editando');
                                        agregarNuevaPregunta($fila_preg, preg);
                                    });
                                }
                            });
                        }
                        $(".agregar_dimension").removeAttr("disabled");

                        /*
                        $("#tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                        $("#tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                        $("#tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                        $("#tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");*/
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: res.msg,
                            type: 'error',
                        });
                    }
                }).fail(function(err) {
                    new PNotify({
                        title: 'Error',
                        text: 'Algo salió mal',
                        type: 'error',
                    });
                    console.log(err.responseText);
                });
            }).on('hidden.bs.modal', function (e) {
                console.log('cerrando dimensiones_bd');
                $('#dimensiones_bd .modal-body').html('<img src="./images/loader.gif" alt="Cargando" class="center-block">');
            });


            $("body").on("click",".guardar_alternativa",function(){
                var correcta = $("input[name='alternativa']:checked").val();
                var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);
                var bloque = {
                    "alternativas":{},
                    "correcta": correcta,
                };

                $(".fila_alternativa").each(function(i,element){
                    var letra = $(this).find(".control-label").attr("letra");
                    var alternativa = $(this).find(".texto_alternativa").val();
                    bloque["alternativas"][letra] = alternativa;
                });

                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id_rubrica': $("#id_encuesta").val(),
                        'nombre_dim': $(".escriba_titulo").attr("nombre"),
                        'nombre_est': $(".escriba_descripcion").attr("nombre"),
                        'nombre_preg': $(".escriba_pregunta").attr("nombre"),
                        'tipo_pregunta': 'A',
                        'escalas_evaluacion': '',
                        'activo': 1,
                        'otros_datos_alt': JSON.stringify(bloque),
                        'otros_datos': JSON.stringify({'niveles':0}),
                        valor: "grabar_alternativa"
                    },
                    beforeSend: function(){
                        $('.sector').before('<img src="./images/loader.gif" class="center-block" alt="Cargando">');
                    }
                }).done(function(res){
                    $(".modal-body").html("");
                    /*
                    var html = "<ul style = 'list-style-type: none;'>";

                    $.each(bloque["alternativas"],function(key,value){
                        if(key==bloque["correcta"]) { var atributo = "true"} else { var atributo = "false"}
                            html += "<li correcta = "+atributo+">"+
                        "<label style = 'font-weight:normal;'>"+key+")&nbsp;</label>"+
                        "<span>"+value+"</span>"+
                        "</li>";
                    });
                    html += "</ul>";


                    $(".cant_preguntas").val(cant_preguntas);
                    $(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11 titulo_descripcion'>"+
                        "<h4>"+cant_preguntas+".-</h4>"+
                        "</div>"+
                        "<div class = 'col-lg-1 text-right'>"+
                        "<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
                        "</div>"+
                        "</div>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11'>"+
                        "<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>DIMENSIÓN: "+titulo+"</div>"+
                        "<div style = 'background:#e8e8e7;padding:10px;'>ESTÁNDAR: "+descripcion+"</div>"+
                        "<div style = 'background:#fcfce0;padding:10px;'><dt>"+pregunta+"</dt></div>"+
                        "</div>"+

                        "</div><br>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11 ejercicio'>"+
                        html+
                        "</div>"+
                        "<div class = 'col-lg-1'></div>"+
                        "</div>"+
                        "</div></div><br>");
                    */
                    setTimeout (function(){
                        location.href='evaluacion.php';
                    }, 500);
                    new PNotify({
                        title: 'Mensaje',
                        text: 'Esta pregunta se ha guardado'
                    });

                }).fail(function(error){
                    console.log("error-no-guarda-alternativas");
                });
            });


            $("body").on("click",".guardar_si_no",function(){

                var titulo = $(".escriba_titulo").attr("nombre");
                var descripcion = $(".escriba_descripcion").attr("nombre");
                var id_encuesta = $("#id_encuesta").val();
                var tipo_pregunta = "si_no";
                var cant_niveles = $(".cant_niveles").val();
                var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);

                var arr =[];

                $(".fila_dimension").each(function(){
                    var nombre_dim = $(this).find(".reemplazo_dimension").text();
                    var attr_dim = $(this).attr("clase_dimension");
                    var arr_pregunta = {};
                    arr_pregunta["nombre"]=nombre_dim;
                    arr_pregunta["estandar"]=[];

                    $(".fila_estandar.d_"+attr_dim).each(function(){
                        var nombre_est = $(this).find(".reemplazo_estandar").text();
                        var attr_est = $(this).attr("clase_estandar");
                        var elem = {};
                        elem["nombre"]=nombre_est;
                        elem["pregunta"]=[];

                        console.log(nombre_est);    

                        $(".fila_pregunta.d_"+attr_dim+".e_"+attr_est).each(function(){
                            var nombre_preg = $(this).find(".reemplazo_pregunta").text();
                            elem["pregunta"].push(nombre_preg);
                        });
                        arr.push(arr_pregunta);
                    });


                    $.ajax({
                        url: "grabar_ajax.php",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_encuesta: id_encuesta,
                            titulo: titulo,
                            descripcion: descripcion,
                            cant_niveles: cant_niveles,
                            tipo_pregunta: tipo_pregunta,
                            arr: JSON.stringify(arr),
                            valor: "grabar_si_no"
                        }
                    }).done(function(res){
                        if(res.codigo=="OK")
                        {
                            var clon = $(".clonado").clone();

                            if(titulo.length==0){ titulo = "Pregunta"} if(descripcion.length==0){ descripcion = ""}

                                $(".modal-body").html("");
                            clon.find(".clonado").removeClass("clonado");
                            clon.find(".fila_dimension").removeClass("fila_dimension");
                            clon.find(".fila_estandar").removeClass("fila_estandar");
                            clon.find(".fila_pregunta").removeClass("fila_pregunta");
                            clon.find(".no_imprimir").remove();

                            $(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
                                "<div class = 'row'>"+
                                "<div class = 'col-lg-11 titulo_descripcion'>"+
                                "<h4>"+ cant_preguntas+" "+titulo+"</h4><h3>"+descripcion+"</h3>"+
                                "</div>"+
                                "<div class = 'col-lg-1 text-right'>"+
                                "<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
                                "</div>"+
                                "</div>"+
                                "<div class = 'row'>"+
                                "<div class = 'col-lg-11 ejercicio'>"+
                                "<table class = 'table table-bordered sortableTable responsive-table'>"+
                                clon.html()+
                                "</table>"+
                                "</div>"+
                                "<div class = 'col-lg-1'></div>"+
                                "</div>"+
                                "</div></div><br>");

                            $(".cant_preguntas").val(cant_preguntas);

                            new PNotify({
                                title: 'Mensaje',
                                text: 'Esta pregunta se ha guardado'
                            });
                        }
                    }).fail(function(error){
                        console.log("error-no-guarda-ponderacion");
                    })
                });
            });

            $("body").on("click",".guardar_autocompletar",function(){
                var id_encuesta = $("#id_encuesta").val();
                var titulo = $(".escriba_titulo").attr("nombre");
                var descripcion = $(".escriba_descripcion").attr("nombre");
                var pregunta = $(".escriba_pregunta").attr("nombre");


                var cant_preguntas = parseInt($(".cant_preguntas").val())+parseInt(1);

                $.ajax({
                    url: "grabar_ajax.php",
                    type: "POST",
                    dataType: "json",
                    data: {
                        'id_rubrica':  $("#id_encuesta").val(),
                        'nombre_dim': $(".escriba_titulo").attr("nombre"),
                        'nombre_est':  $(".escriba_descripcion").attr("nombre"),
                        'nombre_preg': $(".escriba_pregunta").attr("nombre"),
                        'tipo_pregunta': 'C',
                        'escalas_evaluacion': '',
                        'activo': 1,
                        'otros_datos': JSON.stringify({'niveles':0}),
                        'valor': "grabar_autocompletar"
                    }, 
                    beforeSend: function () {
                        $('.sector').before('<img src="./images/loader.gif" class="center-block" alt="Cargando">');
                    }
                }).done(function(res){
                    $(".modal-body").html("");
                    /*
                    $(".cant_preguntas").val(cant_preguntas);
                    $(".sector").before("<div class = 'muestra'><div class = 'pregunta_padre'>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11 titulo_descripcion'>"+
                        "<h4>"+cant_preguntas+".-</h4>"+
                        "</div>"+
                        "</div>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11'>"+
                        "<div style = 'padding: 10px;background:#fb8c00;color:#fff;'>DIMENSIÓN: "+titulo+"</div>"+
                        "<div style = 'background:#e8e8e7;font-weight:bold;padding:10px;'>ESTÁNDAR: "+descripcion+"</div>"+
                        "<div style = 'background:#fcfce0;padding:10px;'><dt>"+pregunta+"</dt></div>"+
                        "</div>"+
                        "<div class = 'col-lg-1 text-right'>"+
                        "<button class='btn btn-danger eliminar_pregunta'><i class = 'icon-remove'></i></button>"+
                        "</div>"+
                        "</div><br>"+
                        "<div class = 'row'>"+
                        "<div class = 'col-lg-11 ejercicio'>"+
                        "<textarea class = 'form-control' rows='5' readonly></textarea>"+
                        "</div>"+
                        "<div class = 'col-lg-1'></div>"+
                        "</div>"+
                        "</div></div><br>");
                    */
                    setTimeout (function(){
                        location.href='evaluacion.php';
                    }, 500);
                    new PNotify({
                        title: 'Mensaje',
                        text: 'Esta pregunta se ha guardado'
                    });
                }).fail(function(error){
                    console.log("error-no-guarda-autocompletar");
                });
            });
        }); 



/***************MILUSKA*******************************/

$("body").on("change",".niveles",function(){

    var cant_niveles= $(".niveles").val();
    var divisor = 100/cant_niveles;
    var tabla = "<div class = 'row'>"+
                    "<div class = 'col-lg-4'><dt>Calificación</dt></div>"+
                    "<div class = 'col-lg-4'><dt>Rango Inicial</dt></div>"+
                    "<div class = 'col-lg-4'><dt>Rango Final</dt></div>"+
                "</div>";
    var cont = 0;
    var puntuacion_final = -1;

    for(var i=0;i<cant_niveles;i++){
        cont++;
        var valor_inicial = puntuacion_final+1;

        puntuacion_final = Math.floor(divisor*cont);

        tabla += "<div class = 'form-group fila' valor_inicial="+valor_inicial+" valor_final = "+puntuacion_final+" class = 'fila'>"+
                    "<div class = 'col-lg-4'><input type = 'text' class = 'form-control representacion' ></div>"+
                    "<div class = 'col-lg-4'><input type = 'text' class = 'form-control' disabled value = '"+valor_inicial+"%'></div>"+
                    "<div class = 'col-lg-4'><input type = 'text' class = 'form-control' disabled value = '"+puntuacion_final+"%'></div>"+
                "</div>";
    }

    $(".muestra_niveles").html(tabla);
})

$("body").on("click",".guardar_puntuacion1",function(){
    
        var id_dimension= $(".dim_numero").val();
        var niveles = $(".niveles").val();
        var arreglo = {};
        var arreglo2 = {"niveles":niveles};

        var i=0;
        $(".fila").each(function(){
            var calif = $(this).find(".representacion").val();
            arreglo[calif]={
                "rango_inicial": $(this).attr("valor_inicial"),
                "rango_final": $(this).attr("valor_final")
            };
            
        });
       // console.log(arreglo);
        $.ajax({
            url: "grabar_ajax.php",
            type: "POST",
            dataType: "json",
            data: {
                id_dimension: id_dimension,
                niveles:arreglo2,
                arreglo:arreglo,
                valor: "grabar_puntuacion1"
            }
        })
        .done(function(res){
            new PNotify({
                title: 'Mensaje',
                text: 'Puntuación actualizada',
                delay: 2000
            })
        })
        .fail(function(error){
            console.log("error-no-guarda-puntuacion");
        })
    })

    </script>



</body>

</html>
