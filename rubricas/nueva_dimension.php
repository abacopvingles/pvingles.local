<?php
	$id_rubrica = $_POST["id_rubrica"];
?>

<div class = 'modal-header'>
	NUEVA DIMENSIÓN
</div>

<div class = 'modal-body'>
<input type = 'hidden' id='id_rubrica' value="<?php echo $id_rubrica;?>">
	<form class = "form-horizontal" id="block-validate">
		<div class = 'form-group'>
			<label class = 'control-label col-lg-3'>
				Nombre
			</label>
			<div class = 'col-lg-7'>
				<input type = 'text' class = 'form-control' name = 'nombre' id = 'nombre'>
			</div>
			<div class = 'col-lg-2'></div>
		</div>
		<div class = 'row text-center' id='aviso'>

		</div>

		<hr>

		<div class = "text-right">
			<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cerrar</button>
			<button type="submit" class="btn btn1 submit1">Guardar</button>
		</div>

	</form>

</div>

<script>
	$(function () {

            $('#block-validate').validate({

		        rules: {
		            nombre:
		            {
		                required: true
		            } 
		        },
		        errorClass: 'help-block',
		        errorElement: 'span',
		        highlight: function (element, errorClass, validClass) {
		            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
		        },
		        unhighlight: function (element, errorClass, validClass) {
		            $(element).parents('.form-group').removeClass('has-error');
		        },
		        submitHandler: function(){
		     
		        	var nombre = $("#nombre").val();
		        	var id_rubrica = $("#id_rubrica").val();
		        	
	    				$.ajax({
							url: "grabar_ajax.php",
							type: "POST",
							dataType: "json",
							data: {
								nombre: nombre,
								id_rubrica: id_rubrica,
								valor: "comparar_nombre_dimension"
							}
						})

						.done(function(res){

							if(res.codigo=="no_registrar")
							{
								$("#aviso").addClass("alert1").append(res.mensaje);
			                    
			                    setTimeout (function(){
									$(".alert1").css("display","none");
									$("#nombre").focus();
			                    }, 2000);
							}
							else 
							{
								$.ajax({
									url: "grabar_ajax.php",
									type: "POST",
									dataType: "json",
									data: {
										nombre: nombre,
										id_rubrica: id_rubrica,
										valor: "crear_dimension"
									}
								})

								.done(function(a){
									if(a.codigo=="OK")
									{
										$("#aviso").css("display","block");
										$("#aviso").addClass("mensaje1").text(a.mensaje);	
										setTimeout (function(){
											$('.btn-close').trigger("click");
					                       location.href='mantenimiento.php?id_rubrica='+id_rubrica;
					                    }, 1100);
									}
								})

								.fail(function(error){
						                console.log("error-no-crea");
								})
							}
						})
	    			
		        }

		    });
	});
</script>