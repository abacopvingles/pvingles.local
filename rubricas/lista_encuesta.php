<?php
	@session_start();
	require_once("class/class_evaluacion.php");

	$obj = new Evaluacion();
	//
	//$encuesta = $obj->listar_evaluaciones_por_creador($_SESSION["s_id_usu"]);
	$encuesta = $obj->listar_evaluaciones_por_creador('72042592');

	//echo "<br><br><br><br><br><br><br><br><br>".$_SESSION['s_id_usu'];

?>

<!doctype html>

<head>	
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

  	<!-- PAGE LEVEL STYLES -->
    <link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src = 'js/jquery.cokidoo-textarea.js'></script>
</head>

<body>
	<div class = 'container-fluid'>
	<?php
		if(isset($_SESSION["s_id_usu"])){
	?>
		<div class = 'row'>
		    <div class = 'col-xs-12'>
			    <?php
			        require_once("menu.php");
			        $accion = "nuevo";
			    ?>
		    </div>
		</div>

		<div class = 'row' style="margin-top: 60px;">
			
			<div class = 'col-xs-1'></div>
			<div class = 'col-xs-10'>
				<div class = 'panel panel-info'>
					<div class = 'panel-heading'>
						<?php /*if($_SESSION["cargo"]=="Administrador")
						{
							?>
							<a class = 'btn btn-default' href = 'ajustes.php?accion=<?php echo $accion;?>'><h4><i class = 'icon-plus'></i> Agregar</h4></a>
							<?php
						}else{
							?>
							<h4>RÚBRICAS</h4>
							<?php
						}*/
						?>
						RÚBRICAS
					</div>
					<div class = 'panel-body'>

						<div class = 'row'>
						<?php
						

						if(!empty($encuesta))
						{
							foreach($encuesta as $enc)
							{
								$id_encuesta = $enc["id_rubrica"];

								if(!empty($enc["foto"])){
									$foto = $enc["foto"];									
								}
								else{
									$foto = "images/nofoto.jpg";
								}

							?>

							<div class = 'col-xs-2'>
								<a href = 'ajustes.php?id_rubrica=<?php echo $id_encuesta;?>' target='_blank'>
									<div class = 'exa-item'>
										<div class = 'titulo' style="height: 50px;"><?php echo $enc["titulo"];?></div>
										<div class = 'portada'>
											<img class="img-responsive" style = 'height: 116px;' width="100%" src=<?php echo $foto;?>>	
										</div>
									</div>
								</a>
							</div>
							<?php
							}
						}
						else
						{
							echo "No tiene rúbricas";
						}

						
						?>
						</div>
					</div>
				</div>
			</div>
				
		</div>
<?php
}
else
{
	echo "<div class = 'text-center'><br>No está logueado....<a href = 'index.php'>Regresar</a></div>";
}

?>
	</div>	
</body>
</html>