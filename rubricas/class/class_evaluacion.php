<?php
require_once("class.php");

class Evaluacion
{
	private $evaluacion = array();
	private $encuesta = array();
	private $encuesta1 = array();
	private $encuesta2 = array();
	private $encuesta3 = array();
	private $encuesta4 = array();
	private $dimension = array();

	

	public function listar_evaluacion($id_encuesta)
	{
		//$this->evaluacion = array();
		$sql = "SELECT * from rub_dimension where id_rubrica = ".$id_encuesta. " and activo = 1"; 
		$res = mysql_query($sql,Conectar::conex());
		while($reg = mysql_fetch_assoc($res))
		{
			$this->evaluacion[] = $reg;
		}		
		return $this->evaluacion;
	}

	

	
	public function listar_evaluaciones_por_creador($id_usuario)
	{
		$this->encuesta1=array();
		$sql = "SELECT *from rub_rubrica where id_usuario = '".$id_usuario."' and activo = 1"; 
		$res = mysql_query($sql,Conectar::conex());
		while($reg = mysql_fetch_assoc($res))
		{
			$this->encuesta1[] = $reg;
		}		
		return $this->encuesta1;
	}

	

	public function buscar_dimension($filtros)
	{
		try {
			$elementos = array();
			$sql = "SELECT * FROM rub_dimension";
			$cond = array();
			if(!empty($filtros["id_dimension"])){
				$cond[] = "id_dimension = " . $filtros["id_dimension"];
			}
			if(!empty($filtros["nombre"])){
				$cond[] = "nombre = '" . $filtros["nombre"] ."'";
			}
			if(!empty($filtros["tipo_pregunta"])){
				$cond[] = "tipo_pregunta = '" . $filtros["tipo_pregunta"] ."'";
			}
			if(!empty($filtros["escalas_evaluacion"])){
				$cond[] = "escalas_evaluacion = '" . $filtros["escalas_evaluacion"] ."'";
			}
			if(!empty($filtros["tipo"])){
				$cond[] = "tipo = '" . $filtros["tipo"] . "'";
			}
			if(!empty($filtros["id_rubrica"])){
				$cond[] = "id_rubrica = " . $filtros["id_rubrica"];
			}
			if(!empty($filtros["activo"])){
				$cond[] = "activo = " . $filtros["activo"];
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			return $elementos;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function grabar_dimension()
	{
		try {
			$filtros = array();
			$filtros['nombre'] = $_POST["nombre_dim"];
			$filtros['tipo_pregunta'] = $_POST["tipo_pregunta"];
			$filtros['escalas_evaluacion'] = $_POST["escalas_evaluacion"];
			$dimension = $this->buscar_dimension($filtros);
			$tipo = 'N';
			if(!empty($dimension)){ $tipo='R'; }
			
			$sql = "INSERT INTO rub_dimension(nombre, tipo_pregunta, escalas_evaluacion, tipo, otros_datos, id_rubrica, activo) VALUES ('". $_POST['nombre_dim'] ."', '". $_POST['tipo_pregunta'] ."', '". $_POST['escalas_evaluacion'] ."', '". $tipo ."', '".$_POST['otros_datos']."', ". $_POST['id_rubrica'] .", ". $_POST['activo'] .")";

			//echo $sql;
			mysql_query($sql,Conectar::conex());
			return mysql_insert_id();
			
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function actualizar_niveles_dimension($id_dimension)
	{
		try {			
			$sql = "UPDATE rub_dimension SET otros_datos='".$_POST['otros_datos']."' WHERE id_dimension=". $id_dimension;

			//echo $sql;
			mysql_query($sql,Conectar::conex());
			return mysql_insert_id();
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function editar_dimension()
	{
		try {
			$filtros = array();
			$filtros['nombre'] = $_POST["nombre"];
			$dimension = $this->buscar_dimension($filtros);
			$tipo = 'N';
			if(!empty($dimension)){ $tipo='R'; }

			$sql = "UPDATE rub_dimension set nombre='".$_POST["nombre"]."', tipo='".$tipo."' where id_dimension = '".$_POST["id_dimension"]."'"; 
			$res = mysql_query($sql,Conectar::conex());

		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function buscar_estandar($filtros)
	{
		try {
			$elementos = array();
			$sql = "SELECT * FROM rub_estandar";
			$cond = array();
			if(!empty($filtros["id_estandar"])){
				$cond[] = "id_estandar = " . $filtros["id_estandar"];
			}
			if(!empty($filtros["nombre"])){
				$cond[] = "nombre = '" . $filtros["nombre"] ."'";
			}
			if(!empty($filtros["duplicado_id_estandar"])){
				$cond[] = "duplicado_id_estandar = '" . $filtros["duplicado_id_estandar"] ."'";
			}
			if(!empty($filtros["id_dimension"])){
				$cond[] = "id_dimension = " . $filtros["id_dimension"];
			}
			if(!empty($filtros["tipo"])){
				$cond[] = "tipo = '" . $filtros["tipo"] . "'";
			}
			if(!empty($filtros["activo"])){
				$cond[] = "activo = " . $filtros["activo"];
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			return $elementos;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function grabar_estandar($id_dimension=null)
	{
		try {
			if(isset($_POST['id_dimension'])){ 
				$id_dimension = $_POST['id_dimension']; 
			}
			$filtros = array();
			$filtros['nombre'] = $_POST["nombre_est"];
			$estandar = $this->buscar_estandar($filtros);
			$tipo = 'N';
			if(!empty($estandar)){ $tipo='R'; }
			
			$sql = "INSERT INTO rub_estandar(nombre, id_dimension, tipo, activo) VALUES ('". $_POST['nombre_est'] ."', '".$id_dimension."', '". $tipo ."', ". $_POST['activo'] .")";

			//echo $sql;
			mysql_query($sql,Conectar::conex());
			return mysql_insert_id();
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function editar_estandar()
	{
		try {
			$filtros = array();
			$filtros['nombre'] = $_POST["nombre"];
			$estandar = $this->buscar_estandar($filtros);
			$tipo = 'N';
			if(!empty($estandar)){ $tipo='R'; }

			$sql = "UPDATE rub_estandar set nombre = '".$_POST["nombre"]."', tipo='".$tipo."' where id_estandar = ".$_POST["id_estandar"];
			$res = mysql_query($sql,Conectar::conex());
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function buscar_pregunta($filtros)
	{
		try {
			$elementos = array();
			$sql = "SELECT * FROM rub_estandar";
			$cond = array();
			if(!empty($filtros["id_pregunta"])){
				$cond[] = "id_pregunta = " . $filtros["id_pregunta"];
			}
			if(!empty($filtros["nombre"])){
				$cond[] = "nombre = '" . $filtros["nombre"] ."'";
			}
			if(!empty($filtros["id_estandar"])){
				$cond[] = "id_estandar = " . $filtros["id_estandar"];
			}
			if(!empty($filtros["tipo"])){
				$cond[] = "tipo = '" . $filtros["tipo"] . "'";
			}
			if(!empty($filtros["otros_datos"])){
				$cond[] = "tipo = '" . $filtros["otros_datos"] . "'";
			}
			if(!empty($filtros["activo"])){
				$cond[] = "activo = " . $filtros["activo"];
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			$res = mysql_query($sql,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			return $elementos;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function grabar_pregunta($id_estandar=null)
	{
		try {
			$otros_datos='';
			$filtros = array();
			if($id_estandar==null){ $id_estandar = $_POST['id_estandar']; }
			if(isset($_POST['otros_datos_alt']) && $_POST['otros_datos_alt']!=''){ $otros_datos = $_POST['otros_datos_alt']; }
			$filtros['nombre'] = $_POST["nombre_preg"];
			$pregunta = $this->buscar_pregunta($filtros);
			$tipo = 'N';
			if(!empty($pregunta)){ $tipo='R'; }
			
			$sql = "INSERT INTO rub_pregunta(nombre, id_estandar, tipo, activo, otros_datos) VALUES ('". $_POST['nombre_preg'] ."', '".$id_estandar."', '". $tipo ."', ". $_POST['activo'] .", '".$otros_datos."')";
			mysql_query($sql,Conectar::conex());
			return mysql_insert_id();
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function editar_pregunta()
	{
		try {
			$filtros = array();
			$filtros['nombre'] = $_POST["nombre"];
			$pregunta = $this->buscar_pregunta($filtros);
			$tipo = 'N';
			if(!empty($pregunta)){ $tipo='R'; }
			
			$sql = "UPDATE rub_pregunta SET nombre='".$_POST['nombre']."', tipo='".$tipo."' WHERE id_pregunta=".$_POST["id_pregunta"];

			//echo $sql;
			mysql_query($sql,Conectar::conex());
			return mysql_insert_id();
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function duplicar_dimension($id_dimension, $id_rubrica)
	{
		try {
			$elementos = array();
			$sql = "INSERT INTO rub_dimension (nombre, tipo_pregunta, escalas_evaluacion, tipo, otros_datos, id_rubrica, activo) SELECT nombre, tipo_pregunta, escalas_evaluacion, 'R', otros_datos, ".$id_rubrica.", 1 FROM dimension WHERE id_dimension=". $id_dimension;

			//echo $sql;
			mysql_query($sql,Conectar::conex());
			$last_id = mysql_insert_id();
			/* Obteniendo los 'id_estandar' nuevos y antiguos */
			$sql2 = "SELECT * FROM rub_dimension WHERE id_dimension=". $last_id;
			$res = mysql_query($sql2,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			return $elementos[0];
			
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function duplicar_estandar($id_dimension_duplicar, $id_dimension_nueva)
	{
		try {
			$elementos = array();
			$sql = "INSERT INTO rub_estandar (nombre, id_dimension, duplicado_id_estandar, activo, tipo) SELECT nombre, ".$id_dimension_nueva.", id_estandar , 1, 'R'  FROM estandar WHERE activo=1 AND id_dimension=". $id_dimension_duplicar;

			//echo $sql;
			mysql_query($sql,Conectar::conex());

			/* Obteniendo los 'id_estandar' nuevos y antiguos */
			$sql2 = "SELECT * FROM rub_estandar WHERE id_dimension=". $id_dimension_nueva;
			$res = mysql_query($sql2,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			//var_dump($elementos);
			return $elementos;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function duplicar_pregunta($id_estandar_duplicar, $id_estandar_nueva)
	{
		try {
			$elementos = array();
			$sql = "INSERT INTO rub_pregunta (nombre, otros_datos, id_estandar, activo, tipo) SELECT nombre, otros_datos, ".$id_estandar_nueva.",1,'R'   FROM pregunta WHERE id_estandar=". $id_estandar_duplicar;

			//echo $sql;
			mysql_query($sql,Conectar::conex());

			/* Obteniendo los 'id_estandar' */
			$sql2 = "SELECT * FROM rub_pregunta WHERE id_estandar=". $id_estandar_nueva;
			$res = mysql_query($sql2,Conectar::conex());
			while($reg = mysql_fetch_assoc($res)) {
				$elementos[] = $reg;
			}
			return $elementos;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	public function eliminar_dimension($id_dimension)
	{
		try {
			$sql = "DELETE FROM rub_dimension WHERE id_dimension=". $id_dimension;
			$resp = mysql_query($sql,Conectar::conex());
			if ($resp === false) {
				throw new Exception("ERROR has occured while executing the next SQL query: ". $sql);
			}
			return true;
		} catch (Exception $e) {
			throw new Exception("ERROR at '".__METHOD__."': " . $e->getMessage());
		}
	}

	/*public function listar_preguntas_pe_sn($id_rubrica)
	{
		$sql = "SELECT *from dimension where id_rubrica = ".$id_rubrica. " and tipo_pregunta = 'PE' OR tipo_pregunta = 'SN'";
		$res = mysql_query($sql,Conectar::conex());
		while($reg = mysql_fetch_assoc($res))
		{
			$this->encuesta4[] = $reg;
		}		
		return $this->encuesta4;
	}*/

	public function save_dimension()
	{
		$niveles = json_encode($_POST["niveles"]);
		$sql = "INSERT INTO rub_dimension(nombre,tipo_pregunta,tipo,id_rubrica,activo,otros_datos) VALUES ('".$_POST["nombre"]."','".$_POST["tipo_pregunta"]."','N','".$_POST["id_rubrica"]."',1,'".$niveles."')";
		$res = mysql_query($sql,Conectar::conex());
		$id_ultimo = mysql_insert_id();
		return $id_ultimo;
	}

	public function actualizar_niveles_dimension1()
	{
		$niveles = json_encode($_POST["niveles"]);
		$sql = "UPDATE rub_dimension set otros_datos = '".$niveles."' where id_dimension = ".$_POST["id_dimension"];
		$res = mysql_query($sql,Conectar::conex());
	}

	public function guardar_estandar()
	{
		$sql = "INSERT INTO rub_estandar(nombre,id_dimension,activo,tipo) VALUES ('".$_POST["nombre"]."','".$_POST["id_dimension"]."',1,'N')";
		$res = mysql_query($sql,Conectar::conex());
		$id_ultimo = mysql_insert_id();
		return $id_ultimo;
	}

	public function guardar_pregunta()
	{
		$sql = "INSERT INTO rub_pregunta(nombre,otros_datos,id_estandar,tipo) VALUES ('".$_POST["nombre"]."','".$_POST["otros_datos"]."','".$_POST["id_estandar"]."','N')";
		$res = mysql_query($sql,Conectar::conex());
		$id_ultimo = mysql_insert_id();
		return $id_ultimo;
	}

	public function eliminar_pregunta()
	{
		$sql = "DELETE from rub_pregunta where id_pregunta = ".$_POST["id_pregunta"];
		$res = mysql_query($sql,Conectar::conex());
	}

	public function elim_estandar()
	{
		$sql = "DELETE from rub_estandar where id_estandar = ".$_POST["id_estandar"];
		$res = mysql_query($sql,Conectar::conex());
	}

	public function elim_dimension()
	{
		$sql = "DELETE from rub_dimension where id_dimension = ".$_POST["id_dimension"];
		$res = mysql_query($sql,Conectar::conex());
	}

	public function act_plantilla()
	{
		$sql = "UPDATE rub_dimension set tipo_pregunta = '".$_POST["tipo_plantilla"]."' where id_dimension = ".$_POST["id_dimension"];
		echo "sql: $sql";
		$res = mysql_query($sql,Conectar::conex());
	}

	public function act_pregunta_alternativas()
	{
		$sql = "UPDATE rub_pregunta SET otros_datos = '".$_POST["otros_datos_alt"]."' WHERE id_pregunta = ".$_POST["id_pregunta"];
		$res = mysql_query($sql,Conectar::conex());
	}




}

?>