<?php
	require_once("class/class_ugel.php");
	$obj=new Ugel();

	$link = $_POST["link"];
?>

<link rel="stylesheet" href="plugins/tagsinput/jquery.tagsinput.css">

<style>
	.radio1{
		width: 1em;
		height: 1em;
	}

	.conestilo option
	{
		font-size:15px;
	}

	td{
		text-align: left;
	}
</style>

<div class = 'modal-header'>
	ENVIAR CORREO
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
</div>

<div class = 'modal-body'>
	<div class = 'form-horizontal' style="padding:10px;">
		<div class = 'row'>
			<div class = 'col-lg-12'>
				<div class = 'escriba_titulo' nombre>Asunto</div>
				<br>
				<div class = 'escriba_descripcion escriba_descripcion1' nombre>Mensaje</div>
			</div>
		</div>
		<br>
		<div class = 'form-group'>
			<div class = 'col-lg-2'></div>
			<div class = 'col-lg-10'>
				<span class="label label-info">Link:&nbsp;&nbsp;<?php echo $link;?></span>
			</div>
		</div>

		<input type = 'hidden' class = 'link' value = '<?php echo $link;?>'>
		
		<div class = 'form-group'>
			<label class = 'control-label col-lg-2' style="text-align: left;">
				Dirigido a:
			</label>
			<div class = 'col-lg-5'>
				<label class="radio-inline">
					<input type = 'radio' name = 'selec_personal' class = 'selec_personal' value = 'P' style = 'width:1em;height: 1em;'>
						Directores
				</label>

				<label class="radio-inline">
					<input type = 'radio' name = 'selec_personal' class = 'selec_personal' value = 'P' style = 'width:1em;height: 1em;'>
						Docentes
				</label>
			
				<label class="radio-inline">
					<input type = 'radio' name = 'selec_personal' class = 'selec_personal' value = 'A' style = 'width:1em;height: 1em;'>
					Alumnos
				</label>
			</div>
		</div>
		<div class = 'form-group'>
			<div class = 'col-lg-12 seleccion'>
				<div class = 'form-group'>
					<div class = 'col-lg-4'></div>
					<div class = 'col-lg-4'>
						<div class = 'caja_select' style="font-size: 1.3em;">
							<select name = 'ugel' class = 'conestilo ugel'>
								<option value = 'n'>Seleccione Ugel</option>
								<option value = '0'>Todas las Ugeles</option>
								<?php
								$ugel = $obj->listar_ugel();
								foreach($ugel as $ug){
								?>
									<option value = '<?php echo $ug["idugel"];?>'><?php echo $ug["descripcion"]?></option>
								<?php
								}
							?>
							</select>
						</div>
					</div>
					<div class = 'col-lg-4'></div>
				</div>
				<div class = 'form-group'>
					<div class = 'col-lg-4'></div>
					<div class = 'col-lg-4'>
						<input type = 'hidden' id='codlocal'>
						<textarea class = 'conestilo txt_colegio' style = 'font-weight: 600;' placeholder="Institución Educativa" disabled></textarea>
					</div>
					<div class = 'col-lg-4'>
						<label class="checkbox-inline chk_ie" style ='display: none;'>
                            <input type="checkbox" class = 'cambiar_ie'>Cambiar IE
                        </label>
					</div>
				</div>
				<div class='form-group div_listado' style="display:none;">
					<div class = 'col-lg-12 listado_ie text-center'>					
					</div>
				</div>
				<div class = 'form-group'>
					<div class = 'col-lg-4'></div>
					<div class = 'col-lg-4'>
						<div class = 'caja_select' style="font-size: 1.3em;">
							<select class = 'conestilo aula'>
								<option value = '0'>Seleccione Aula</option>
							</select>
						</div>
					</div>
					<div class = 'col-lg-4'></div>
				</div>
				<div class = 'form-group'>
					<div class = 'col-lg-4'></div>
					<div class = 'col-lg-4'>
						<div class = 'caja_select' style="font-size: 1.3em;">
							<select class = 'conestilo grupo'>
								<option value = '0'>Seleccione Grupo</option>
							</select>
						</div>
					</div>
					<div class = 'col-lg-4'></div>
				</div>
				<div class = 'form-group'>
					<label for="tags" class="control-label col-lg-4">Agregar correos</label>                   
					<div class="col-lg-8">
                        <input name="tags" id="tags" class="form-control" />
                    </div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class = 'modal-footer'>
	<div class = 'col-lg-12 text-right'>
		<a class = 'btn btn-primary enviar_correo1' data-dismiss = "modal">Enviar Correo</a>
	</div>
</div>


<script src="plugins/tagsinput/jquery.tagsinput.min.js"></script>

<script>

$(document).ready(function(){
	$('#tags').tagsInput();
})

	var func_colegio = function(id_ugel){

		if(id_ugel!=0 && id_ugel!="n"){
			$(".listado_ie").html("<img src = 'images/loader.gif' style = 'text-align:center;'>");
			$(".txt_colegio").css("display","none");
			$.ajax({
    			url: "grabar_ajax.php",
				type: "POST",
				dataType: "json",
				data: {
					"id_ugel": id_ugel,
					"edit": "sin_th",
					"valor": "mostrar_ie"
				},
    		}).done(function(res){
	    		if(res.codigo=="OK"){

	    			$(".listado_ie").html(res.tabla);
	    			 $('#example3').dataTable({
				        "language": dt_es
				        });
	    		}
	    	}).fail(function(error){
	            console.log("no-muestra");
	        })
		}
		else if(id_ugel==0){
			$(".listado_ie").html("Se enviarán a todas las instituciones educativas de todas las ugeles");
			$(".txt_colegio").css("display","none");
		}
		else if(id_ugel=="n"){
			$(".listado_ie").html("");
		}
	}

	$("body").on("change",".ugel",function(){
		var id_ugel = $(this).val();
		func_colegio(id_ugel);
		$(".div_listado").css("display","block");
	});	

	$("body").on("click",".cambiar_ie",function(){
		var id_ugel = $(".ugel").val();
		$(".div_listado").css("display","block");
		func_colegio(id_ugel);
	});	

	$("body").on("click",".selec_ie",function(){
		$(".div_listado").css("display","none");
		$(".txt_colegio").css("display","block");
		$(".chk_ie").css("display","block");
		$(".cambiar_ie").prop("checked",false);

		
		var colegio = $("input.selec_ie:checked").val();
		
		var arr_colegio = colegio.split(",");
		var id_colegio = arr_colegio[0];
		var nombre_colegio = arr_colegio[1];
		var option = '';

		$(".txt_colegio").text(nombre_colegio);
		$("#codlocal").val(id_colegio);

		$(".listado_ie").html("");

		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				"idlocal": id_colegio,
				"valor": "mostrar_ambiente"
			},
		}).done(function(res){
    		if(res.codigo=="OK"){
    			
			for(var i=0;i<(res.arr).length;i++){
				option+="<option value = '"+res.arr[i]["idambiente"]+"'>"+res.arr[i]["idambiente"]+"</option>";
			}
    			$(".aula").append(option);
    		}
    	}).fail(function(error){
            console.log(error);
        })
	});

	$("body").on("change",".aula",function(){
		var option="";
		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				"valor": "mostrar_programacion"
			}
		}).done(function(res){
    		if(res.codigo=="OK"){
    			
			for(var i=0;i<(res.arr).length;i++){
				option+="<option value = '"+res.arr[i]["idprogramacion"]+"'>"+res.arr[i]["campos"]+"</option>";
			}
    			$(".grupo").append(option);
    		}
    	}).fail(function(error){
            console.log(error);
        })
	});	

	$(".enviar_correo1").click(function(){
		var titulo = $(".escriba_titulo").text();
		var descripcion = $(".escriba_descripcion").text();
		var link = $(".link").val();
		var cadena = "";
		$(".tag").find("span").each(function(){
			cadena += $(this).text().trim()+";";
		});
		var tam_cadena = cadena.length-1;
		var cad = cadena.substr(0,tam_cadena);

		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				"titulo":titulo,
				"descripcion":descripcion,
				"cadena":cad,
				"link":link,
				"valor": "enviar_correo"
			}
		}).done(function(res){
    		if(res.codigo=="OK"){
    			new PNotify({
					title: 'Mensaje',
					text: 'El mensaje ha sido enviado a todos los destinatarios'
				});
    		}
    	}).fail(function(error){
            console.log(error);
        })
	});
</script>
