<?php
//if (session_status() == PHP_SESSION_NONE) {
	@session_start();
//}
require_once("class/class_ajustes.php");
require_once("class/class_mantenimiento.php");
require_once("class/class_evaluacion.php");

?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- PAGE LEVEL TABLA STYLES -->
	<link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<script src ="plugins/dataTables/Spanish.js"></script>
	<!-- END PAGE LEVEL  STYLES -->

	<!-- PAGE LEVEL STYLES -->
	<link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src="js/pnotify.custom.min.js"></script>
	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

</head>

<style>
	::-webkit-input-placeholder {
		font-style: italic;
	}
	:-moz-placeholder {
		font-style: italic;  
	}
	::-moz-placeholder {
		font-style: italic;  
	}
	:-ms-input-placeholder {  
		font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
		background-color: #fff;
	}

	.poner_imagen{
		padding-left:5px;
		cursor: pointer;
		position: relative;
	}

	.img_check{
		position:absolute;
		bottom:0px;
		left:1em;
		right:0px;
		top: 0px;
		display: none;
		height: 100%
	}

	.navbar-top-links li a {
		padding: 10px 2px;
	}



}
</style>

<body>

	<div class = 'container-fluid'>

<?php
	if($_SESSION["s_id_usu"]){
?>

		<div class = 'row'>
			<div class = 'col-xs-12'>
				<?php require_once("menu.php"); ?>
			</div>
		</div>

		<?php
	if(!empty($_GET["id_rubrica"]))
{
	$ses_idrubrica = $_GET["id_rubrica"];
	$obj = new Ajuste();
	$obj1 = new Evaluacion();
	$obj2 = new Mantenimiento();
	$ajuste = $obj->listar_idencuesta($ses_idrubrica);
	$encuesta = $obj1->listar_evaluacion($ses_idrubrica); 
	$cant_preg = ($encuesta=="")?0:count($encuesta);
	$tipo_rubrica = $ajuste[0]["tipo_rubrica"];
?>

		<div class = 'row' style="margin-top: 60px;">

			<div class = 'col-xs-1'></div>
			<div class = 'col-xs-10'>

				<input type = 'hidden' class = 'cant_preguntas' value = '<?php echo $cant_preg;?>'>
				<input type = 'hidden' id='tipo_rubrica' value = '<?php echo $tipo_rubrica;?>'>
					
				<div class="form-group link_llenado" id="link_llenado">
					<div class="input-group">
						<div class="input-group-addon">Enlace para responder la rúbrica:</div>
						<input type="text" name="txtLinkLlenado" id="txtLinkLlenado" class="form-control" value="<?php echo $_SERVER['HTTP_HOST']."/tacna/control/monitoreo/ingles/dar_examen.php?id_rubrica=".$_GET['id_rubrica'] ?>" readonly>
						<div class="input-group-addon enviar_correo" data-toggle="modal" data-target="#ModalCorreo" style = 'cursor:pointer'><span class="glyphicon glyphicon-envelope" aria-hidden="true" title="Enviar correo"></span></div>
					</div>
				</div>
				<input type = 'hidden' id='id_rubrica' value = '<?php echo $ses_idrubrica;?>'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-heading text-center'>
						<h3>Rúbrica: "<?php echo $ajuste[0]["titulo"];?>"</h3>
						<h4>Creación de 
							<?php 
								if($tipo_rubrica==1){
									echo "Preguntas";

								}else if($tipo_rubrica==2){
									echo "Estándares";
									$css = 'style="background:#fff;border-top: 2px solid #4e4e4e;border-bottom: 2px solid #4e4e4e;color:#4e4e4e;"';
									$tit='BLOQUE'+' '+$cant_fila_dimension;
									$mensaje = "Bloque de estándares guardado";
									$css_titulo = 'style = "display:none"';

								}else if($tipo_rubrica==3){
									echo "Dimensiones";
									$css = 'style="background:#fb8c00;color:#fff;"';
									$tit='DIMENSIÓN'+' '+$cant_fila_dimension+': ';
									$mensaje = "Dimensión guardada";
									$css_titulo = 'style = "display:inline;font-size:17px;"';
								}
							?>
						</h4>
					</div>
					<div class = 'panel-body'>
						<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>

						<div id = 'contenido_dimensiones'>

						<?php
						$cont=0;

						foreach($encuesta as $dim)
						{
							$cont++;

							if($tipo_rubrica==1){
								$css1 = 'style="background:#fff;border-top: 2px solid #4e4e4e;border-bottom: 2px solid #4e4e4e;color:#4e4e4e;"';
								$tit='BLOQUE'.' '.$cont;
								$css_titulo = 'style = "display:none"';
								$css_estandar = 'style = "display:none"';

								}else if($tipo_rubrica==2){
									$css1 = 'style="background:#fff;border-top: 2px solid #4e4e4e;border-bottom: 2px solid #4e4e4e;color:#4e4e4e;"';
									$tit='BLOQUE'.' '.$cont;
									$css_titulo = 'style = "display:none"';
									$css_estandar = 'style = "background:#e8e8e7"';

								}else if($tipo_rubrica==3){
									$css1 = 'style="background:#fb8c00;color:#fff;"';
									$tit='DIMENSIÓN'.' '.$cont.': ';
									$css_titulo = 'style = "display:inline;font-size:17px;"';
									$css_estandar = 'style = "background:#e8e8e7"';
								}

							$tipo_pregunta = $dim["tipo_pregunta"];
							$div_niveles = "";
							$cant_niv=json_decode($dim["otros_datos"],true);
							$cant_niveles=$cant_niv["niveles"];

							if($tipo_pregunta=="PE"){
								$div_niveles.='<li style="padding-right:0px;" class="resaltado_subt">NIVELES: 
										<select class="cant_niveles" style="color: #000;padding:5px;border-radius:6px;">';
										for($i=2;$i<=5;$i++){
											$selected = "";
											if($cant_niveles==$i){ $selected = "selected";}

											$div_niveles .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
										}
								$div_niveles.= '</select>&nbsp;&nbsp;
									</li>';
							}

							
							$array_imagenes=array("pond_estandar","si_no","alternativas","autocompletar");
							$array_title=array("Ponderación Estándar","SI-NO","Alternativas","Completar");
							$array_tpregunta = array("PE","SN","A","C");

							$menu="";
							//menu que va en todas las dimensiones
							$menu .= '<ul class="nav navbar-top-links navbar-right">'.$div_niveles;
								$menu .= '

									<li class="dropdown" style="padding-right:0px;">
										<a class="dropdown-toggle btn btn-circle btn-info" title="Elegir Plantilla" data-toggle="dropdown" href="#" style="background-color: #31b0d5;">
											<i class="icon-list"></i>
										</a>

										<ul class="dropdown-menu dropdown-messages">';

								for($i=0;$i<sizeof($array_tpregunta);$i++){

									$css='style="display: none;"';
									if($tipo_pregunta==$array_tpregunta[$i]){
										$css='style="display: block;"';
									}
									
									$menu.='	<li>
											<a class="row">
												<div class="col-xs-6 col-md-6 col-sm-6 col-xs-6">
													<div class="poner_imagen" tipo_plantilla="'.$array_tpregunta[$i].'">
														<img src="images/'.$array_imagenes[$i].'.png" class="img-responsive" title="'.$array_title[$i].'">
														<img src="images/check.png" class="img_check img-responsive" '.$css.'">
													</div>
												</div>

												<div class="poner_imagen col-xs-6 col-md-6 col-sm-6 col-xs-6">
													'.$array_title[$i].'
												</div>
											</a>
										</li>

										<li class="divider"></li>';
								}
											
								$menu.='</ul>
									</li>

									<li style="padding-right:0px;">
										<button class="btn btn-primary btn-circle agregar_estandar istooltip" data-placement="bottom" title="Agregar Estándar">
											<i class="icon-plus"></i>
										</button>
									</li>
									<li style="padding-right:0px;">
										<button class="btn btn-success btn-circle guardar_puntuacion istooltip" id_dimension='.$dim["id_dimension"].' data-toggle="modal" data-target="#ModalPuntuacion" data-placement="bottom" title="Establecer escalas de evaluación">
											<i class="icon-th-large"></i>
										</button>
									</li>
									<li style="padding-right:0px;">
										<button class="btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip" data-placement="bottom" title="Eliminar dimensión">
											<i class="icon-trash"></i>
										</button>
									</li>
								</ul>';
							
						?>

						<div class = 'row'>
							<div class = 'col-xs-12'>
							<input type="hidden" class="tipo_plantilla" value="<?php echo $tipo_pregunta;?>">
							<div class="table-responsive" style="overflow:initial;">
								<table class="table table-striped tabla_creacion_dimensiones" id="tabla_creacion_dimensiones_<?php echo $id_dimension?>" attr-id="<?php echo $id_dimension?>" cellpadding="4">
									<tbody>

							<?php
							
								$titulo_enc = "PONDERACIÓN ESTÁNDAR";
								$subtitulo = "NIVELES";
								$rowspan = "rowspan = '2'";

								if($tipo_pregunta=="SN"){ 
									$titulo_enc = "SI - NO";
									$cant_niveles = 2;
									$subtitulo = "OPCION";
								}
								else if($tipo_pregunta=="A"){ 
									$titulo_enc = "ALTERNATIVAS";
									$cant_niveles = 2;
									$rowspan = "";
								}	
								else if($tipo_pregunta=="C"){ 
									$titulo_enc = "COMPLETAR";
									$cant_niveles = 2;
									$rowspan = "";
								}

								$colspan = $cant_niveles+2;
								?>
									<tr class="cabecera fila_add_dimension">
									
									<?php
									if($tipo_pregunta == "PE" || $tipo_pregunta == "SN")
									{
									?>
										<td width="90%" colspan="2" <?php echo $rowspan;?> class="text-center td_modificable" style="background:#2196f3; color:#fff; border-right:2px solid #fff;">
											<h4 class="titulo_plantilla"><?php echo $titulo_enc;?></h4>
										</td>
										<td width="10%" colspan="<?php echo $cant_niveles;?>" style="background:#2196f3;color:#fff;" class="fila_nivel text-center"><dt>NIVELES</dt>
										</td>
									<?php
									}
									else if($tipo_pregunta == "A" || $tipo_pregunta == "C")
									{
									?>
										<td width="90%" colspan="<?php echo $colspan;?>" <?php echo $rowspan;?> class="text-center td_modificable" style="background:#2196f3; color:#fff; border-right:2px solid #fff;">
											<h4 class="titulo_plantilla"><?php echo $titulo_enc;?></h4>
										</td>
									<?php
									}
									?>
									</tr>
									<?php
									if($tipo_pregunta == "PE" || $tipo_pregunta == "SN")
									{
									?>
									<tr class="por_nivel">
									
									<?php

										if($tipo_pregunta == "PE"){
											for($i=1;$i<=$cant_niveles;$i++){
											?>
												<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">
													<h5><?php echo $i;?></h5>
												</td>
											<?php
											}
										}
										else{
											$array = array("SI","NO");

											for($i=0;$i<sizeof($array);$i++){
												?>
												<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">
													<h5><?php echo $array[$i];?></h5>
												</td>
												<?php
											}
										}
										
										?>
									</tr>
									<?php
									}
									?>
									<tr class="fila_dimension d_<?php echo $dim["id_dimension"];?>" <?php echo $css1;?> clase_dimension="<?php echo $dim["id_dimension"];?>" data-iddimension="<?php echo $dim["id_dimension"];?>">
										<td colspan="<?php echo $colspan;?>" class="td_dimension" style="padding:10px;">
											<div class="col-xs-6" style="padding-top:5px;">
												<dt class="let_dimension text-left" style="display:inline;font-size:17px;">				
													<?php echo $tit;?> 
												</dt>
												<dt class="reemplazo_dimension cambio_texto" <?php echo $css_titulo;?> valor_estatico="<?php echo $dim["nombre"]?>" tecla_esc="no">
													<?php echo $dim["nombre"]?>		
												</dt>
											</div>
											<div class="col-xs-6 text-right">
												<?php echo $menu;?>
											</div>
										</td>
									</tr>
									<?php
									
									$est1 = $obj2->listar_estandares($dim["id_dimension"]);
									$cont_est = 0;
									$cont_preg=0;

									if(!empty($est1)){
									foreach($est1 as $est){
										$cont_est++;
										$cont_letra = $cont_est+64;
										$letra = strtoupper(chr($cont_letra));
										?>
										<tr <?php echo $css_estandar;?> class="fila_estandar d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" data-idestandar="<?php echo $est["id_estandar"];?>">
											<td class="td_estandar" colspan="<?php echo $colspan;?>">
												<div class="col-xs-10" style="padding-top:5px;">
													<dt class="let_estandar text-right" style="display:inline;padding-top:5px;">
														<?php echo $letra;?>)  
													</dt>
													<div class="reemplazo_estandar cambio_texto" style="padding-top:5px;display:inline;" valor_estatico="<?php echo $est["nombre"];?>" tecla_esc="no">
														<?php echo $est["nombre"];?>
													</div>
												</div>
												<div class="col-xs-2 text-right btn_estandar">
													<button class="btn btn-success btn-circle agregar_pregunta no_imprimir" data-mytooltip="tooltip1" data-placement="bottom" title="Agregar Pregunta">
														<i class="icon-plus"></i>
													</button>
													<button class="btn btn-danger btn-circle eliminar_estandar no_imprimir" data-mytooltip="tooltip1" data-placement="bottom" title="Eliminar estándar">
														<i class="icon-trash"></i>
													</button>
												</div>
											</td>
										</tr>
										<?php
										
										$preg1 = $obj2->listar_preguntas($est["id_estandar"]);

										if(!empty($preg1)){
											foreach($preg1 as $preg){
												$cont_preg++;
												?>
												<tr style="background:#e8e8e7;" class="fila_pregunta d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>" data-idpregunta="<?php echo $preg["id_pregunta"];?>">
													<td width="5%" style="background: #e0e8ef; text-align: right;">
														<div class="let_pregunta" style="display:inline;">
															<?php echo $cont_preg;?>.- 
														</div>
													</td>
													<td width="75%" style="background: #fcfce0;" colspan="">
														<div class="col-xs-11 reemplazo_pregunta cambio_texto" style="padding-top:5px;" valor_estatico="<?php echo $preg["nombre"]?>" tecla_esc="no">
															<?php echo $preg["nombre"]?>
														</div>
														<div class="col-xs-1 pull-right text-muted">
															<em><button class="btn btn-danger btn-circle eliminar_pregunta"><i class="icon-trash"></i></button>
															</em>
														</div>
													</td>

													<?php 
													if($tipo_pregunta == "PE" || $tipo_pregunta == "SN")
													{
														for($i=0;$i<$cant_niveles;$i++)
														{
														?>
															<td class="text-center option_radio" style="background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;" onclick="return false;">
																<input type="radio">
															</td>
														<?php
														}
													}
													?>
												</tr>

												<?php

												if($tipo_pregunta=="A"){
												?>
												<tr class="grupo_alternativas d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>">
													<td colspan="3" style="background:rgba(91,192,222,0.08);">
														<table width="100%">
															<tbody>
																<tr>
																	<td>
																		<div class="form-horizontal">
																		<?php
																		if(!empty($preg["otros_datos"]))
																		{

																			$alternativas=json_decode($preg["otros_datos"],true);
																			$alt1=$alternativas["alternativas"];
																			$correcta = $alternativas["correcta"];
																			$a=true;
																			foreach($alt1 as $key=>$value){
																				?>
																				<div class="form-group fila_alternativa">
																					<label class="control-label col-xs-1" letra="<?php echo $key;?>"><?php echo $key;?>)</label>
																					<label class="alt_disabled control-label col-xs-6" style="font-weight:normal;text-align:left;"><?php echo $value;?></label>
																					<div class="col-xs-1">
																						<input type="radio" name="<?php echo "p_".$preg["id_pregunta"];?>_alternativa" value="<?php echo $key;?>" disabled="" <?php if($key==$correcta){echo "checked";}?>>
																					</div>
																					<?php if($a==true){
																					?>
																						<div class="col-xs-2 text-left">
																							<button class="btn btn-primary btn-circle agregar_alternativa">
																								<i class="icon-plus"></i>
																							</button>
																							<button class="btn btn-info btn-circle edit_alternativas">
																								<i class="icon-edit"></i>
																							</button>
																						</div>
																					<?php
																					$a=false;
																					}?>
																					
																				</div>
																				<?php
																			}
																		
																		}
																		else
																		{
																		?>
																		<div class = 'form-group fila_alternativa'>
																			<label class = 'control-label col-xs-1' letra = 'a'>
																				a)
																			</label>
																			<div class = 'col-xs-6'>
																				<input type = 'text' class = 'form-control texto_alternativa'>
																			</div>
																			<div class = 'col-xs-1'>
																				<input type = 'radio' name = 'p_<?php echo $preg["id_pregunta"];?>_alternativa' value = 'a'>
																			</div>
																			<div class = 'col-xs-2 text-left'>
																				<button class = 'btn btn-primary btn-circle agregar_alternativa'><i class = 'icon-plus'></i></button>
																				<button class = 'btn btn-success btn-circle save_alternativas' title = 'Guardar Alternativas'><i class = 'icon-save'></i></button>
																			</div>
																		</div>

																		<div class = 'form-group fila_alternativa'>
																			<label class = 'control-label col-xs-1' letra = 'b'>
																				b)
																			</label>
																			<div class = 'col-xs-6'>
																				<input type = 'text' class = 'form-control texto_alternativa'>
																			</div>
																			<div class = 'col-xs-1'>
																				<input type = 'radio' name = 'p_<?php echo $preg["id_pregunta"];?>_alternativa' value='b'>
																			</div>
																		</div>
																		<?php
																		}
																		?>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<?php
											}else if($tipo_pregunta=="C"){
												?>
												<tr class="completar d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>">
													<td colspan="3" style="background:rgba(91,192,222,0.08);">
														<table width="100%">
															<tbody>
																<tr>
																	<td>
																		<textarea class="form-control" style="width:90%" rows="6" disabled=""></textarea>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<?php
											}
											} //fin preg1
										} //fin empty preg1
										
									}
									//}
									
								}//FIN PONDERACIÓN EST-SINO
								
							?>

									</tbody>
								</table>
							</div>
						</div>
						</div>
						
							<?php
						}
						?>


						</div> <!--fin contenido dimensiones-->

						<div class = 'row bloque_add_dimension'>
							<div class = 'col-xs-12 text-center'>
								<button class = 'btn btn-default agregar_dimension'><i class = 'icon-plus'></i> Agregar 
								<?php

									if($tipo_rubrica==1){
									echo "Pregunta";
									}else if($tipo_rubrica==2){
										echo "Estándar";
									}else if($tipo_rubrica==3){
										echo "Dimension";
									}
								?>
								</button>
							</div>
						</div>

					</div>
				</div>
			<div class = 'col-xs-1'></div>
		</div>

		<?php
		}
		else{
			?>

				<div class = 'row' style="margin-top: 60px;">
					<div class = 'col-lg-1'></div>
					<div class = 'col-lg-10'>
						<div class = 'panel panel-primary'>
							<div class = 'panel-heading'>
								AVISO
							</div>
							<div class = 'panel-body'>
								No hay rúbrica creada
							</div>
						</div>
						
					</div>
					<div class = 'col-lg-1'></div>
				</div>

			<?php
		}

}
else
{
	echo "<div class = 'text-center'><br>No está logueado....<a href = 'index.php'>Regresar</a></div>";
}


?>
	</div>


	<div class="modal fade" id="dimensiones_bd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close btncloseaddinfotxt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 id="titulo-modal">Dimensiones de Base de Datos</h4>
				</div>
				<div class="modal-body" id="cuerpo-modal" style="max-height: 500px;overflow: auto;">
					<img src="./images/loader.gif" alt="Cargando" class="center-block">
				</div>
				<div class="modal-footer" id="pie-modal">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary pull-right elegir_dimension">Elegir dimensión</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ModalPuntuacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id = 'puntuacion'>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ModalCorreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop='static' data-keyboard='false'>
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div id = 'correo'>
				</div>
			</div>
		</div>
	</div>



	<script src="plugins/dataTables/jquery.dataTables.js"></script>
	<script src="plugins/dataTables/dataTables.bootstrap.js"></script>



<script>
$(document).ready(function() {

	

	var menu = '<ul class="nav navbar-top-links navbar-right">'+                
			'<li class="dropdown" style = "padding-right:0px;">'+
				'<a class="dropdown-toggle btn btn-circle btn-primary btn-lg" title = "Elegir Plantilla" data-toggle="dropdown" style = "background:#337ab7;border:none;">'+
					'<i class = "icon-list"></i>'+
				'</a>'+
				'<ul class="dropdown-menu dropdown-messages">'+
					'<li>'+
						'<a class = "row">'+
            				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
                   				"<div class = 'poner_imagen' tipo_plantilla='PE'>"+
									"<img src = 'images/pond_estandar.png' class = 'img-responsive' title = 'Ponderación Estándar'>"+
									"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
								"</div>"+
            				'</div>'+
            				'<div class = "poner_imagen col-xs-6 col-md-6 col-sm-6 col-xs-6">Ponderación Estándar'+
            				'</div>'+
						'</a>'+
					'</li>'+
					'<li class="divider"></li>'+
					'<li>'+
						'<a class = "row">'+
            				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
                   				"<div class = 'poner_imagen' tipo_plantilla='SN'>"+
									"<img src = 'images/si_no.png' class = 'img-responsive' title = 'SI-NO'>"+
									"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
								"</div>"+
            				'</div>'+
            				'<div class = "poner_imagen col-xs-6 col-md-6 col-sm-6 col-xs-6">SI-NO'+
            				'</div>'+
						'</a>'+
					'</li>'+
					'<li class="divider"></li>'+
					'<li>'+
						'<a class = "row">'+
            				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
                   				"<div class = 'poner_imagen' tipo_plantilla='A'>"+
									"<img src = 'images/alternativas.png' class = 'img-responsive' title = 'Alternativas'>"+
									"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
								"</div>"+
            				'</div>'+
            				'<div class = "poner_imagen col-xs-6 col-md-6 col-sm-6 col-xs-6">Alternativas'+
            				'</div>'+
						'</a>'+
                	'</li>'+
                	'<li class="divider"></li>'+
                	'<li>'+
						'<a class = "row">'+
            				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
                   				"<div class = 'poner_imagen' tipo_plantilla='C'>"+
									"<img src = 'images/autocompletar.png' class = 'img-responsive' title = 'Completar'>"+
									"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
								"</div>"+
            				'</div>'+
            				'<div class = "poner_imagen col-xs-6 col-md-6 col-sm-6 col-xs-6">Completar'+
            				'</div>'+
						'</a>'+
                	'</li>'+
                	
                '</ul>'+
            '</li>'+
	
		"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-success btn-lg elegir_dimension' title = 'Elegir de BD' data-toggle='modal' data-target='#dimensiones_bd'>"+
			"<i class='icon-hand-up' style = 'font-size:20px;'></i>"+
		"</button></li>"+

		"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-primary btn-lg guardar_dimension' title = 'Guardar'>"+
			"<i class='icon-save' style = 'font-size:20px;'></i>"+
		"</button></li>"+
	
		"<li style = 'padding-right:0px;'><button type='button' class='btn btn-circle btn-default btn-lg cancelar_add_dimension' title = 'Cancelar'>"+
			"<i class='icon-remove' style = 'font-size:20px;'></i>"+
		"</button></li></ul>";

	var menu1 = '<li class="dropdown" style = "padding-right:0px;">'+
			'<a class="dropdown-toggle btn btn-circle btn-info" title = "Elegir Plantilla" data-toggle="dropdown" href="#" style = "background-color: #31b0d5;">'+
				'<i class = "icon-list"></i>'+
			'</a>'+
			'<ul class="dropdown-menu dropdown-messages">'+
				'<li>'+
					'<a class = "row">'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
               				"<div class = 'poner_imagen' tipo_plantilla='PE'>"+
								"<img src = 'images/pond_estandar.png' class = 'img-responsive' title = 'Ponderación Estándar'>"+
								"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
							"</div>"+
        				'</div>'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">Ponderación Estándar'+
        				'</div>'+
					'</a>'+
				'</li>'+
				'<li class="divider"></li>'+
				'<li>'+
					'<a class = "row">'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
               				"<div class = 'poner_imagen' tipo_plantilla='SN'>"+
								"<img src = 'images/si_no.png' class = 'img-responsive' title = 'SI-NO'>"+
								"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
							"</div>"+
        				'</div>'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">SI-NO'+
        				'</div>'+
					'</a>'+
				'</li>'+
				'<li class="divider"></li>'+
				'<li>'+
					'<a class = "row">'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
               				"<div class = 'poner_imagen' tipo_plantilla='A'>"+
								"<img src = 'images/alternativas.png' class = 'img-responsive' title = 'Alternativas'>"+
								"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
							"</div>"+
        				'</div>'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">Alternativas'+
        				'</div>'+
					'</a>'+
            	'</li>'+
            	'<li class="divider"></li>'+
            	'<li>'+
					'<a class = "row">'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">'+
               				"<div class = 'poner_imagen' tipo_plantilla='C'>"+
								"<img src = 'images/autocompletar.png' class = 'img-responsive' title = 'Completar'>"+
								"<img src = 'images/check.png' class = 'img_check img-responsive'>"+
							"</div>"+
        				'</div>'+
        				'<div class = "col-xs-6 col-md-6 col-sm-6 col-xs-6">Completar'+
        				'</div>'+
					'</a>'+
            	'</li>'+
            	
            '</ul>'+
        '</li>';


    $("body").on("click",".guardar_puntuacion",function(){
    	var id_dimension = $(this).attr("id_dimension");
    	$("#puntuacion").html('<img src="./images/loader.gif" alt="Cargando" class="center-block">');
    	$.ajax({
            url: "guardar_puntuacion.php",
            type: "POST",
            data: {
                id_dimension: id_dimension
            },
            success: function(result){

                $("#puntuacion").html(result);
            }
        });
    });

    $("body").on("click",".enviar_correo",function(){

    	$("#correo").html('<img src="./images/loader.gif" alt="Cargando" class="center-block">');
    	var link = $("#txtLinkLlenado").val();
    	$.ajax({
            url: "enviar_correo.php",
            type: "POST",
            data:{"link":link},
            success: function(result){
                $("#correo").html(result);
            }
        });
    });

    $("body").on("change",".niveles",function(){

	    var cant_niveles= $(".niveles").val();
	    var divisor = 100/cant_niveles;
	    var tabla = "<div class = 'row'>"+
	                    "<div class = 'col-xs-4'><dt>Calificación</dt></div>"+
	                    "<div class = 'col-xs-4'><dt>Rango Inicial</dt></div>"+
	                    "<div class = 'col-xs-4'><dt>Rango Final</dt></div>"+
	                "</div>";
	    var cont = 0;
	    var puntuacion_final = -1;

	    for(var i=0;i<cant_niveles;i++){
	        cont++;
	        var valor_inicial = puntuacion_final+1;

	        puntuacion_final = Math.floor(divisor*cont);

	        tabla += "<div class = 'form-group fila' valor_inicial="+valor_inicial+" valor_final = "+puntuacion_final+" class = 'fila'>"+
	                    "<div class = 'col-xs-4'><input type = 'text' class = 'form-control representacion' ></div>"+
	                    "<div class = 'col-xs-4'><input type = 'text' class = 'form-control' disabled value = '"+valor_inicial+"%'></div>"+
	                    "<div class = 'col-xs-4'><input type = 'text' class = 'form-control' disabled value = '"+puntuacion_final+"%'></div>"+
	                "</div>";
	    }	    
	    $(".muestra_niveles").html(tabla);
	})

	$("body").on("click",".agregar_dimension",function(){
		var tipo_rubrica = $("#tipo_rubrica").val();
		var colspan = parseInt($(".cant_niveles").val())+2;
		var cant_fila_dimension = parseInt($(".fila_dimension").length)+1;
		var tit = "";
		var css = "";
		var nueva_dimension="";
		
		if(tipo_rubrica==1 || tipo_rubrica==2){
			tit = "BLOQUE"+" "+cant_fila_dimension;
			css = "style='background:#fff;border: 1px solid #434343;color:#4e4c4c;padding: 10px;'";
			nueva_dimension="<div class = 'col-xs-7 style='margin-top:10px'></div>";

		}else if(tipo_rubrica==3){
			tit = "DIMENSIÓN"+" "+cant_fila_dimension+": ";
			css = "style='background:#fb8c00;color:#fff;padding: 10px;'";
			nueva_dimension="<div class = 'col-xs-7 new_dimension' style='margin-top:10px'>"+
								"<input type='text' style='display:inline;' class='form-control nueva_dimension' placeholder='Escriba aquí la dimensión'>"+
							"</div>";
		}

			$(".bloque_add_dimension")
			.before("<div class = 'row fila_dimension editando' "+css+">"+
						"<input type = 'hidden' class = 'tipo_plantilla'>"+
						"<div class = 'col-xs-2' style='margin-top:10px'>"+
							"<h4>"+tit+"</h4>"+
						"</div>"+
						nueva_dimension+	
						"<div class = 'col-lg-3 col-md-3 col-sm-3 col-xs-3' id = 'area_botones'>"+
							menu+
						"</div>"+
					"</div>"
					);
		
		
	});

	$("body").on("click",".poner_imagen",function(){
		$(".img_check").css("display","none");
		$(this).find(".img_check").fadeIn("1500");
		var tipo_plantilla = $(this).attr("tipo_plantilla");
		$(".tipo_plantilla").val(tipo_plantilla);
		
		var id_dimension = $(this).parents(".fila_dimension").attr("clase_dimension");
		var colspan = 2;
		var cant_niveles = 2;
		if(($(padre).find("cant_niveles")).length>0){
			colspan = $(padre).find(".cant_niveles").val();
			cant_niveles = $(padre).find(".cant_niveles").val();
		}

		var padre = $(this).parents(".tabla_creacion_dimensiones");
	
		$(padre).find(".td_modificable").attr("colspan",2);

		if(tipo_plantilla=="PE"){

			$(padre).find(".titulo_plantilla").text("PONDERACIÓN ESTÁNDAR");
			$(padre).find(".resaltado_subt").css("display","inline-block");
			$(padre).find(".reemplazo_pregunta").parents("td").removeAttr("colspan");

			if(($(padre).find(".resaltado_subt")).length==0){
				$(padre).find(".navbar-top-links").before('<li style="padding-right:0px;" class="resaltado_subt">NIVELES: <select class="cant_niveles" style="color: #000;padding:5px;border-radius:6px;"><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select>&nbsp;&nbsp;</li>')
				$(padre).find(".resaltado_subt").css({"list-style": "none", "display": "inline"});
			} 

			if(($(padre).find(".fila_nivel")).length==0){
				
				$(padre).find(".fila_add_dimension")
				.append('<td width="10%" colspan="'+colspan+'" style="background: rgb(33, 150, 243); color: rgb(255, 255, 255);" class="fila_nivel text-center"><dt>NIVELES</dt></td>');
				$(padre).find(".fila_add_dimension")
				.after("<tr class = 'por_nivel'></tr>");
			}
			else{
				
				$(padre).find(".fila_nivel").show();
				$(padre).find(".por_nivel").show();
			}

			$(padre).find(".option_radio").remove();
			var columnas = '';
			var col_radio = '';
			for(var i=1;i<=cant_niveles;i++)
			{
				columnas += "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>"+i+"</h5></td>";
				col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
			}
			$(padre).find(".por_nivel").html(columnas);
			$(padre).find(".fila_pregunta").append(col_radio);
			$(padre).find(".td_modificable").attr("rowspan",2);
			//$(padre).find(".grupo_alternativas").css("display","none");
			if($(padre).find(".completar").length>0)
			{
				$(padre).find(".completar").each(function(){
					$(this).hide();
				})
			}

			if($(padre).find(".grupo_alternativas").length>0)
			{
				$(padre).find(".grupo_alternativas").each(function(){
					$(this).hide();
				})
			}
		}
		else if(tipo_plantilla=="SN"){
			$(padre).find(".titulo_plantilla").text("SI-NO");
			//$(padre).find(".cant_niveles").css("display","none");
			$(padre).find(".cant_niveles").val(2);
			$(padre).find(".resaltado_subt").css("display","none");
			$(padre).find(".fila_nivel").show();
			$(padre).find(".td_modificable").attr("rowspan",2);
			$(padre).find(".por_nivel").show();
			$(padre).find(".por_nivel").html('<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;"><h5>SI</h5></td><td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;"><h5>NO</h5></td>');
			$(padre).find(".reemplazo_pregunta").parents("td").removeAttr("colspan");
			$(padre).find("td_dimension").attr("colspan",4);
			$(padre).find("td_estandar").attr("colspan",4);
			$(padre).find(".option_radio").remove();
			//$(padre).find(".grupo_alternativas").css("display","none");
			if($(padre).find(".completar").length>0)
			{
				$(padre).find(".completar").each(function(){
					$(this).hide();
				})
			}

			if($(padre).find(".grupo_alternativas").length>0)
			{
				$(padre).find(".grupo_alternativas").each(function(){
					$(this).hide();
				})
			}

			var col_radio = '';
			for(var i=1;i<=2;i++)
			{
				col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
			}
			$(padre).find(".fila_pregunta").append(col_radio);
		}

		else if(tipo_plantilla=="A"){

			$(padre).find(".titulo_plantilla").text("ALTERNATIVAS");
			$(padre).find(".fila_nivel").css("display","none");
			$(padre).find(".por_nivel").css("display","none");
			$(padre).find(".td_modificable").attr("colspan",2);
			$(padre).find(".td_modificable").removeAttr("rowspan");
			$(padre).find(".td_dimension").attr("colspan",4);
			$(padre).find(".td_estandar").attr("colspan",4);
			$(padre).find(".cant_niveles").val(2);
			$(padre).find(".resaltado_subt").css("display","none");
			$(padre).find(".option_radio").remove();
			$(padre).find(".reemplazo_pregunta").parents("td").removeAttr("colspan");

			if($(padre).find(".completar").length>0)
			{
				$(padre).find(".completar").each(function(){
					$(this).hide();
				})
			}

			if($(padre).find(".grupo_alternativas").length>0)
			{
				$(padre).find(".grupo_alternativas").each(function(){
					$(this).show();
				})
			}

			else{
				$(padre).find(".fila_pregunta").each(function(){
					var id_estandar = $(this).attr("clase_estandar");
					var id_pregunta = $(this).attr("clase_pregunta");

					$(this).after("<tr class = 'grupo_alternativas d_"+id_dimension+" e_"+id_estandar+" p_"+id_pregunta+"' clase_pregunta = '"+id_pregunta+"'>"+
							"<td colspan = '3' style = 'background:rgba(91,192,222,0.08);'>"+
								"<table width='100%'>"+
									"<tr>"+
										"<td>"+
											"<div class = 'form-horizontal'>"+

												"<div class = 'form-group fila_alternativa'>"+
													"<label class = 'control-label col-xs-1' letra = 'a'>"+
														"a)"+
													"</label>"+
													"<div class = 'col-xs-6'>"+
														"<input type = 'text' class = 'form-control texto_alternativa'>"+
													"</div>"+
													"<div class = 'col-xs-1'>"+
														"<input type = 'radio' name = 'p_"+id_pregunta+"_alternativa' value = 'a'>"+
													"</div>"+
													"<div class = 'col-xs-2 text-left'>"+
														"<button class = 'btn btn-primary btn-circle agregar_alternativa'><i class = 'icon-plus'></i></button>"+
													
														"<button class = 'btn btn-success btn-circle save_alternativas' title = 'Guardar Alternativas'><i class = 'icon-save'></i></button>"+
													"</div>"+
												"</div>"+

												"<div class = 'form-group fila_alternativa'>"+
													"<label class = 'control-label col-xs-1' letra = 'b'>"+
														"b)"+
													"</label>"+
													"<div class = 'col-xs-6'>"+
														"<input type = 'text' class = 'form-control texto_alternativa'>"+
													"</div>"+
													"<div class = 'col-xs-1'>"+
														"<input type = 'radio' name = 'p_"+id_pregunta+"_alternativa' value='b'>"+
													"</div>"+
												"</div>"+

											"</div>"+
										"</td>"+
									"</tr>"+
								"</table>"+
							"</td>"+
						"</tr>");
					})
			}
		}

		else if(tipo_plantilla=="C"){

			$(padre).find(".titulo_plantilla").text("COMPLETAR");
			$(padre).find(".fila_nivel").css("display","none");
			$(padre).find(".por_nivel").css("display","none");
			$(padre).find(".td_modificable").attr("colspan",2);
			$(padre).find(".td_modificable").removeAttr("rowspan");
			$(padre).find(".td_dimension").attr("colspan",4);
			$(padre).find(".td_estandar").attr("colspan",4);
			$(padre).find(".cant_niveles").val(2);
			$(padre).find(".resaltado_subt").css("display","none");
			$(padre).find(".option_radio").remove();
			$(padre).find(".reemplazo_pregunta").parents("td").removeAttr("colspan");

			
			if($(padre).find(".grupo_alternativas").length>0)
			{
				$(padre).find(".grupo_alternativas").each(function(){
					$(this).hide();
				})
			}

			if($(padre).find(".completar").length>0)
			{
				$(padre).find(".completar").each(function(){
					$(this).show();
				})
			}
			else{
				$(padre).find(".fila_pregunta").each(function(){
					var id_estandar = $(this).attr("clase_estandar");
					var id_pregunta = $(this).attr("clase_pregunta");

					$(this).after("<tr class = 'completar d_"+id_dimension+" e_"+id_estandar+" p_"+id_pregunta+"' clase_pregunta = '"+id_pregunta+"'>"+
							"<td colspan = '3' style = 'background:rgba(91,192,222,0.08);'>"+
								"<table width='100%'>"+
									"<tr>"+
										"<td>"+
											"<textarea style='width:90%;' class = 'form-control' rows='6' disabled></textarea>"+
										"</td>"+
									"</tr>"+
								"</table>"+
							"</td>"+
						"</tr>");
					})
			}
		}

		var tipo_plantilla1 = $(this).parents(".row").find(".tipo_plantilla").val();

        
		$.ajax({
            url: "grabar_ajax.php",
            type: "POST",
            dataType: "json",
            data: {
                id_dimension: id_dimension,
                tipo_plantilla: tipo_plantilla1,
                valor: "actualizar_plantilla"
            }
        }).done(function(res){
           //console.log("exito");
        })
        .fail(function(error){
            console.log("error-no-guarda-plantilla");
        })
	})

	$("body").on("click",".guardar_dimension",function(){
		var tipo_rubrica = $("#tipo_rubrica").val();
		var nueva_dimension = $(this).parents(".fila_dimension").find(".nueva_dimension").val();
		var tipo_plantilla = $(this).parents(".row").find(".tipo_plantilla").val();
		var id_rubrica = $("#id_rubrica").val();
		var cant_fila_dimension = parseInt($(".fila_dimension").length);
		var $this = $(this);
		

		var arreglo2 = {"niveles":"2"};
		var a = false;
		var cant_niveles = $(".cant_niveles").val();
		if(!cant_niveles){ cant_niveles=2;}
		var colspan = parseInt(cant_niveles)+2;
		var css = "";
		var tit="";
		var mensaje = "";
		var css_titulo = "";
		var orden = "";
		var title = "";
		var tr_estandar="";

		$this.parents(".fila_dimension").find(".img_check").each(function(){

			var check = $(this).css("display");
			
			if(check == "block"){
				a=true;
			}else{
				
			}
		});

		if(a==true){
			if(tipo_rubrica==1 || tipo_rubrica==2){
				nueva_dimension = "nulo";
			}

			if(nueva_dimension.length>0){
				$.ajax({
	            url: 'grabar_ajax.php',
	            type: 'POST',
	            dataType: 'JSON',
	            data: {
	            	'id_rubrica': id_rubrica,
	                'nombre': nueva_dimension,
	                'tipo_pregunta': tipo_plantilla,
	                'niveles':arreglo2,
	                'valor': "guardar_dimension"
	            },
	        }).done(function(resp) {
	            if(resp.codigo=='OK'){
	            	
	            	var fecha = Date.now();
	            	
	            	/*
					AQUÍ VA EL DISEÑO AL GUARDAR LA DIMENSIÓN
	            	*/
	            	if(tipo_rubrica==1){
	            		css = 'style="background:#fff;border-top: 2px solid #4e4e4e;border-bottom: 2px solid #4e4e4e;color:#4e4e4e;"';
	            		tit='BLOQUE'+' '+cant_fila_dimension;
	            		mensaje = "Bloque de preguntas guardado";
	            		css_titulo = 'style = "display:none"';
	            		orden="agregar_pregunta";
	            		title = "title = 'Agregar Pregunta'";
	            		tr_estandar = "<tr class = 'fila_estandar'></tr>";

	            		
				    }
	            	else if(tipo_rubrica==2){
	            		css = 'style="background:#fff;border-top: 2px solid #4e4e4e;border-bottom: 2px solid #4e4e4e;color:#4e4e4e;"';
	            		tit='BLOQUE'+' '+cant_fila_dimension;
	            		mensaje = "Bloque de estándares guardado";
	            		css_titulo = 'style = "display:none"';
	            		orden="agregar_estandar";
	            		title="title = 'Agregar Estándar'";
	            		tr_estandar='';

	            	}else if(tipo_rubrica==3){
	            		css = 'style="background:#fb8c00;color:#fff;"';
	            		tit='DIMENSIÓN'+' '+cant_fila_dimension+': ';
	            		mensaje = "Dimensión guardada";
	            		css_titulo = 'style = "display:inline;font-size:17px;"';
	            		orden="agregar_estandar";
	            		title="title = 'Agregar Estándar'";
	            		tr_estandar='';
	            	}

	            	new PNotify({
						title: 'Mensaje',
						text: mensaje,
						delay: 1500
					});


	            	if(tipo_plantilla=="PE"){
	            		var titulo = "PONDERACIÓN ESTÁNDAR";
	            		var colspan1 = "colspan = 2";
	            		var rowspan = "rowspan = 2";
	            		var width = "90%";
	            		var fila_extra='<td width="10%" colspan="2" style = "background:#2196f3;color:#fff" class="fila_nivel text-center"><dt>NIVELES</dt></td>';
	            		var tr_adicional = '<tr class="por_nivel" style = "background:#2196f3;color:#fff">'+
												'<td class="text-center" style="border:1px solid #fff;">'+
													'<h5>1</h5>'+
												'</td>'+
												'<td class="text-center" style="border:1px solid #fff;">'+
													'<h5>2</h5>'+
												'</td>'+
											'</tr>'
											;
						var select = "<li style = 'padding-right:0px;' class = 'resaltado_subt'>"+
										'NIVELES: '+
										'<select class = "cant_niveles" style = "color: #000;padding:5px;border-radius:6px;">'+
											'<option value = "2">2</option>'+
											'<option value = "3">3</option>'+
											'<option value = "4">4</option>'+
											'<option value = "5">5</option>'+
										'</select>&nbsp;&nbsp;'+
                        			"</li>";
	            	
	            	}
	            	else if(tipo_plantilla=="SN"){
	            		var titulo = "SI-NO";
	            		var colspan1 = "colspan = 2";
	            		var rowspan = "rowspan = 2";
	            		var width = "90%";
	            		var fila_extra='<td width="10%" colspan="2"  style="background:#2196f3;color:#fff;border:1px solid #fff;" class="fila_nivel text-center"><dt>OPCIÓN</dt></td>';
	            		var tr_adicional = '<tr class="por_nivel">'+
												'<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">'+
													'<h5>SI</h5>'+
												'</td>'+
												'<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">'+
													'<h5>NO</h5>'+
												'</td>'+
											'</tr>';
						var select = "";

	            	}
	            	else if(tipo_plantilla=="A"){
	            		var titulo = "ALTERNATIVAS";
	            		var colspan1 = "colspan = 4";
	            		var rowspan = "";
	            		var width = "";
	            		var fila_extra = "";
	            		var tr_adicional = "";
	            		var select = "";
	            	}
	            	else if(tipo_plantilla=="C"){
	            		var titulo = "COMPLETAR";
	            		var colspan1 = "colspan = 4";
	            		var rowspan = "";
	            		var width = "";
	            		var fila_extra = "";
	            		var tr_adicional = "";
	            		var select = "";
	            	}

	        $this.parents(".fila_dimension")
	        .before("<div class = 'row'>"+
	        			"<div class = 'col-xs-12'>"+
	        				"<input type = 'hidden' class = 'tipo_plantilla' value = '"+tipo_plantilla+"'>"+
	        				"<div class = 'table-responsive' style = 'overflow:initial;'>"+
	        					'<table class="table table-striped tabla_creacion_dimensiones" id="tabla_creacion_dimensiones_'+fecha+'" attr-id = "'+fecha+'" cellpadding="4">'+
	        						'<tbody>'+
	        							'<tr class="cabecera fila_add_dimension">'+
	        								'<td '+width+' '+colspan1+' '+rowspan+' style="background:#2196f3;color:#fff;border-right:2px solid #fff;" class="text-center td_modificable"><h4 class = "titulo_plantilla">'+titulo+'</h4></td>'+
	        									fila_extra+
	        							'</tr>'+
	        							tr_adicional+
	        							'<tr class="fila_dimension d_'+resp.id_ultimo+'" '+css+' clase_dimension="'+resp.id_ultimo+'" data-iddimension="'+resp.id_ultimo+'">'+
	        								'<td colspan="'+colspan+'" class="td_dimension" style = "padding:10px;">'+
												'<div class="col-xs-6" style = "padding-top:5px;">'+
													'<dt class = "let_dimension text-left" style = "display:inline;font-size:17px;">'+tit+
													'</dt>'+
													'<dt class="reemplazo_dimension cambio_texto" '+css_titulo+'>'+nueva_dimension+
													'</dt>'+
												'</div>'+
												'<div class="col-xs-6 text-right">'+
													'<ul class="nav navbar-top-links navbar-right">'+ 
														"<li style = 'padding-right:0px;' class = 'resaltado_subt'>"+
															select+
														"</li>"+
															menu1+
														"<li style = 'padding-right:0px;'>"+
				                            				'<button class="btn btn-primary btn-circle '+orden+' istooltip" data-placement="bottom" '+title+'>'+
																'<i class="icon-plus"></i>'+
															'</button>'+
				                            			"</li>"+

				                            			"<li style = 'padding-right:0px;'>"+
															'<button class="btn btn-success btn-circle guardar_puntuacion istooltip" id_dimension="'+resp.id_ultimo+'" data-toggle="modal" data-target="#ModalPuntuacion" data-placement="bottom" title="Establecer escalas de evaluación">'+
																'<i class="icon-th-large"></i>'+
															'</button>'+
				                            			"</li>"+

				                            			"<li style = 'padding-right:0px;'>"+
															'<button class="btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip" data-placement="bottom" title="Eliminar dimensión">'+
																'<i class="icon-trash"></i>'+
															'</button>'+
				                            			"</li>"+
				                            		"</ul>"+
				                            	'</div>'+
				                            '</td>'+
				                       	'</tr>'+
				                       	tr_estandar+
				                    '</tbody>'+
				                '</table>'+
				            "</div>"+
				        "</div>"+
				    "</div>");

	        	$this.parents(".fila_dimension").remove();

	        	var $padre=$("table");

	        	//guardar_estandar_nulo_internamente
	            		var nombre_est="nulo";
	            		var id_dimension = resp.id_ultimo;
	            		
	            		$.ajax({
				            url: 'grabar_ajax.php',
				            type: 'POST',
				            dataType: 'JSON',
				            data: {
				            	'id_dimension': id_dimension,
				                'nombre':nombre_est,
				                'valor': "guardar_estandar"
				            }
				        }).done(function(res) {
				            if(resp.codigo=='OK'){
								$padre.find(".fila_estandar").attr("clase_estandar",res.id_ultimo);
								$padre.find(".fila_estandar").attr("data-idestandar",res.id_ultimo);
								$padre.find(".fila_estandar").addClass("e_"+res.id_ultimo);
							}
            			})
				        /////////////////////////////////////////////////////////////

	            }
	            else { console.log('Ocurrió un problema al actualizar error:'+resp.msg) }

		        }).fail(function(err) {
		            console.log('Algo salió mal');
		            console.log(err.responseText);
		        });
		}else{
			$(".nueva_dimension").focus();
			$(".nueva_dimension").css("background","#f2f3df");
		}
			
		}
		else
		{
			alert("No ha seleccionado un tipo de plantilla");
		}

		});

	$("body").on("change",".cant_niveles",function(){
		var cant_niveles = $(this).val();
		var colspan = parseInt(cant_niveles)+parseInt(2);

		var id_dimension = $(this).parents(".fila_dimension").attr("clase_dimension");
		
		var id_padre = $(this).parents(".tabla_creacion_dimensiones").attr("attr-id");
		
		var columnas = '';
		var col_radio = '';
		var arreglo2 = {"niveles":cant_niveles};
		var $this = $(this);

		for(var i=1;i<=cant_niveles;i++)
		{
			columnas += "<td class = 'text-center' style = 'background:#2196f3;color:#fff;border:1px solid #fff;'><h5>"+i+"</h5></td>";
			col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
		}
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".fila_nivel").attr("colspan",cant_niveles);
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".por_nivel").html(columnas);
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".td_dimension").attr("colspan",colspan);
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".option_radio").remove();
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".fila_pregunta").append(col_radio);
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".td_dimension").attr("colspan",colspan);
		$this.parents("#tabla_creacion_dimensiones_"+id_padre).find(".td_estandar").attr("colspan",colspan);

		$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_dimension': id_dimension,
                'niveles':arreglo2,
                'valor': "actualizar_niveles"
            },
        }).done(function(resp) {
            if(resp.codigo=='OK'){
            	//console.log("ya esta");
			}
		}).fail(function(err) {
            console.log('Error no actualiza niveles');
            console.log(err.responseText);
        });
	})

	$("body").on("click",".agregar_estandar",function(){
		var id_dimension = $(this).parents(".fila_dimension").attr("clase_dimension");
		var cant_fila_estandar = parseInt($(this).parents(".tabla_creacion_dimensiones").find("tr.fila_estandar").length);
		var cant_niveles=$(this).parents(".tabla_creacion_dimensiones").find(".cant_niveles").val();
		var letra1 = cant_fila_estandar+97;
		var letra = String.fromCharCode(letra1).toUpperCase();
		var colspan = parseInt(cant_niveles)+parseInt(2);
		if(!colspan){colspan=4;}
		var tipo_plantilla = $(this).parents(".row").find(".tipo_plantilla").val();
		
		$(this).parents(".tabla_creacion_dimensiones")
			.append('<tr style="background:#e8e8e7;" class="fila_estandar editando d_'+id_dimension+'" clase_dimension="'+id_dimension+'">'+
					'<td class="td_estandar" colspan="'+colspan+'">'+
						'<div class="col-xs-10" style = "padding-top:5px;">'+
							'<dt class = "let_estandar text-right" style = "display:inline;padding-top:5px;">'+
							letra+') '+
							'</dt>'+
							'<div class = "new_estandar" style="display:inline;margin-top:5px;">'+
								'<input type="text" class="form-control nuevo_estandar" placeholder="Escriba aquí el estándar" style = "width: 80%;display:inline;">'+
							'</div>'+
						'</div>'+
						'<div class="col-xs-2 text-right btn_estandar">'+
							'<button class="btn btn-circle btn-primary save_estandar">'+
								'<i class="icon-save"></i>'+
							'</button>'+
							'<button class="btn btn-circle btn-default cancelar_add_estandar">'+
								'<i class="icon-remove"></i>'+
							'</button>'+
						'</div>'+
					'</td>'+
				'</tr>');
	});

	$("body").on("click",".save_estandar",function(){
		var nombre = $(".nuevo_estandar").val();
		var id_dimension = $(this).parents(".fila_estandar").attr("clase_dimension");
		var $this = $(this);

		if(nombre.length>0){
			$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_dimension': id_dimension,
                'nombre':nombre,
                'valor': "guardar_estandar"
            },
        }).done(function(resp) {
            if(resp.codigo=='OK'){

            	new PNotify({
					title: 'Mensaje',
					text: 'Estándar guardado',
					delay: 1500
				});
				$this.parents(".fila_estandar").attr("clase_estandar",resp.id_ultimo);
				$this.parents(".fila_estandar").attr("data-idestandar",resp.id_ultimo);
				$this.parents(".fila_estandar").addClass("e_"+resp.id_ultimo);
				$this.parents(".fila_estandar").removeClass("editando");
				$this.parents(".fila_estandar").find(".btn_estandar").html('<button class="btn btn-success btn-circle agregar_pregunta no_imprimir"  data-mytooltip="tooltip1" data-placement="bottom" title="Agregar Pregunta"><i class="icon-plus"></i></button>'+
					'<button class="btn btn-danger btn-circle eliminar_estandar no_imprimir" data-mytooltip="tooltip1" data-placement="bottom" title="Eliminar estándar"><i class="icon-trash"></i></button>');
				$(".new_estandar").replaceWith('<div class="reemplazo_estandar cambio_texto" style = "padding-top:5px;display:inline;">'+
													nombre+
												'</div>');
				
			}
		}).fail(function(err) {
            console.log('Error no guarda estándar');
            console.log(err.responseText);
        });
		}else{
			$(".nuevo_estandar").focus();
			$(".nuevo_estandar").css("background","#fbe39a");
		}
		
	});	


	$("body").on("click",".agregar_pregunta",function(){

		var tipo_rubrica = $("#tipo_rubrica").val();
		var id_dimension = $(this).parents(".fila_estandar").attr("clase_dimension");
		
		if(tipo_rubrica==2||tipo_rubrica==3){
			var id_estandar = $(this).parents(".fila_estandar").attr("clase_estandar");
		}else{
			var id_estandar = $(this).parents("table").find(".fila_estandar").attr("clase_estandar");
		}
		
		var colspan = $(this).parents(".tabla_creacion_dimensiones").find(".cant_niveles").val();

		var cant_fila_pregunta = parseInt($(".tabla_creacion_dimensiones tr.fila_pregunta").length)+1;
		$(this).parents(".tabla_creacion_dimensiones").find(".td_modificable").attr("width","90%");
		var tipo_plantilla = $(this).parents(".row").find(".tipo_plantilla").val();
		var td_width = "5%";
		if(tipo_plantilla == "SN"){ colspan = "2";}
		
		if(tipo_plantilla == "A"){ td_width = "2%";}
		
		$(this).parents(".tabla_creacion_dimensiones").find("tr.e_"+id_estandar).last()
			.after('<tr style="background:#e8e8e7;" class="fila_pregunta editando" clase_dimension="'+id_dimension+'" clase_estandar="'+id_estandar+'">'+
					'<td width="'+td_width+'" style="background: #e0e8ef; text-align: right;">'+
						'<div class="let_pregunta" style="display:inline;">'+
							cant_fila_pregunta+'.- '+
						'</div>'+
					'</td>'+
					'<td width = "75%" style="background: #fcfce0;">'+
						'<div class="new_pregunta" style="display:inline;">'+
							'<input type="text" style="width:90%;display:inline;" class="form-control nueva_pregunta" placeholder="Escriba aquí la pregunta">'+
						'</div>'+
					'</td>'+
					'<td colspan="'+colspan+'" class="niveles">'+
						'<button class="btn btn-circle btn-primary save_pregunta">'+
							'<i class="icon-save"></i>'+
						'</button>'+
						'<button class="btn btn-circle btn-default cancelar_add_pregunta">'+
							'<i class="icon-remove"></i>'+
						'</button>'+
					'</td>'+
				'</tr>');

		var i=0;
		$(this).parents(".tabla_creacion_dimensiones").find("tr.fila_pregunta").each(function(){
			i++;

			$(this).find(".let_pregunta").text(i+".- ");
		});
	});	

	$("body").on("click",".save_pregunta",function(){
		var id_dimension = $(this).parents(".fila_pregunta").attr("clase_dimension");
		var id_estandar = $(this).parents(".fila_pregunta").attr("clase_estandar");
		var nombre = $(".nueva_pregunta").val();
		var cant_niveles=$(this).parents(".tabla_creacion_dimensiones").find(".cant_niveles").val();
		var $this = $(this);
		var col_radio = '';
		var tipo_plantilla = $(this).parents(".row").find(".tipo_plantilla").val();
		colspan = '';
		if(tipo_plantilla == "A"){ var colspan = 2;} 
		for(var i=0;i<cant_niveles;i++)
		{
			col_radio += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
		}

		if(nombre.length>0){
			$.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
            	'id_estandar': id_estandar,
                'nombre':nombre,
                'otros_datos': '',
                'valor': "guardar_pregunta"
            }
        }).done(function(resp) {
            if(resp.codigo=='OK'){

				$this.parents(".fila_pregunta").attr("clase_pregunta",resp.id_ultimo);
				$this.parents(".fila_pregunta").attr("data-idpregunta",resp.id_ultimo);
				$this.parents(".fila_pregunta").addClass("d_"+id_dimension);
				$this.parents(".fila_pregunta").addClass("e_"+id_estandar);
				$this.parents(".fila_pregunta").addClass("p_"+resp.id_ultimo);
				$this.parents(".fila_pregunta").removeClass("editando");
				$this.parents(".niveles").replaceWith(col_radio);
				$(".new_pregunta").parents("td").attr("colspan",colspan);
				$(".new_pregunta").replaceWith('<div class="col-xs-11 reemplazo_pregunta cambio_texto" style = "padding-top:5px;">'+
													nombre+
												'</div>'+
												'<div class = "col-xs-1 pull-right text-muted">'+
			                                        '<em><button class = "btn btn-danger btn-circle eliminar_pregunta">'+
			                                            '<i class = "icon-trash"></i>'+
			                                        '</button></em>'+
			                                    '</div>');

				if(tipo_plantilla=="A"){
					$(".p_"+resp.id_ultimo).after(
						"<tr class = 'grupo_alternativas d_"+id_dimension+" e_"+id_estandar+" p_"+resp.id_ultimo+"' clase_pregunta = '"+resp.id_ultimo+"'>"+
							"<td colspan = '3' style = 'background:rgba(91,192,222,0.08);'>"+
								"<table width='100%'>"+
									"<tr>"+
										"<td>"+
											"<div class = 'form-horizontal'>"+

												"<div class = 'form-group fila_alternativa'>"+
													"<label class = 'control-label col-xs-1' letra = 'a'>"+
														"a)"+
													"</label>"+
													"<div class = 'col-xs-6'>"+
														"<input type = 'text' class = 'form-control texto_alternativa'>"+
													"</div>"+
													"<div class = 'col-xs-1'>"+
														"<input type = 'radio' name = 'p_"+resp.id_ultimo+"_alternativa' value = 'a'>"+
													"</div>"+
													"<div class = 'col-xs-2 text-left'>"+
														"<button class = 'btn btn-primary btn-circle agregar_alternativa'><i class = 'icon-plus'></i></button>"+
														"<button class = 'btn btn-success btn-circle save_alternativas' title = 'Guardar Alternativas'><i class = 'icon-save'></i></button>"+
													"</div>"+
												"</div>"+

												"<div class = 'form-group fila_alternativa'>"+
													"<label class = 'control-label col-xs-1' letra = 'b'>"+
														"b)"+
													"</label>"+
													"<div class = 'col-xs-6'>"+
														"<input type = 'text' class = 'form-control texto_alternativa'>"+
													"</div>"+
													"<div class = 'col-xs-1'>"+
														"<input type = 'radio' name = 'p_"+resp.id_ultimo+"_alternativa' value='b'>"+
													"</div>"+
												"</div>"+

											"</div>"+
										"</td>"+
									"</tr>"+
								"</table>"+
							"</td>"+
						"</tr>")
				}
				else if(tipo_plantilla=="C"){
					$(".p_"+resp.id_ultimo).after(
						"<tr class = 'completar d_"+id_dimension+" e_"+id_estandar+" p_"+resp.id_ultimo+"' clase_pregunta = '"+resp.id_ultimo+"'>"+
							"<td colspan = '3' style = 'background:rgba(91,192,222,0.08);'>"+
								"<table width='100%'>"+
									"<tr>"+
										"<td>"+
											"<textarea class = 'form-control' style = 'width:90%' rows = '6' disabled></textarea>"+
										"</td>"+
									"</tr>"+
								"</table>"+
							"</td>"+
						"</tr>")
				}
			}
		}).fail(function(err) {
            console.log('Error no guarda pregunta');
            console.log(err.responseText);
        });
		}else{
			$(".nueva_pregunta").css("background","#fcd083");
			$(".nueva_pregunta").focus();
		}
		
	});	

	$("body").on("click",".cancelar_add_dimension",function(){
		$(this).parents(".fila_dimension").remove();
	});

	$("body").on("click",".cancelar_add_estandar",function(){
		$(this).parents(".fila_estandar").remove();
	});	

	$("body").on("click",".cancelar_add_pregunta",function(){
		$(this).parents(".fila_pregunta").remove();
	});

	$("body").on("click",".eliminar_pregunta",function(){ console.log("eliminando pregunta");
		var id_pregunta = $(this).parents("tr").attr("clase_pregunta");
		var padre = $(this).parents(".tabla_creacion_dimensiones");
		var $this = $(this);
		var padre=$(this).parents(".tabla_creacion_dimensiones");
		$.ajax({
	        url: 'grabar_ajax.php',
	        type: 'POST',
	        dataType: 'JSON',
	        data: {
	        	'id_pregunta': id_pregunta,
	            'valor': "elim_pregunta"
	        }
        }).done(function(resp) {
			 if(resp.codigo=='OK'){
			 	$this.parents(".tabla_creacion_dimensiones").find(".td_modificable").attr("width","180%");
			 	$this.parents(".fila_pregunta").remove();
			 	$(padre).find(".p_"+id_pregunta).each(function(){
			 		$(this).remove();
			 	})
			 	
			 	
			 	var i=0;
				$(padre).find("tr.fila_pregunta").each(function(){
					i++;
					$(this).find(".let_pregunta").text(i+".- ");
				});

			}
		}).fail(function(err) {
	        console.log('Error no elimina pregunta');
	        console.log(err.responseText);
	    });
	});

	$("body").on("click",".eliminar_estandar",function(){ console.log("eliminando estndar");
		var id_estandar = $(this).parents("tr").attr("clase_estandar");
		var $this = $(this);
		var padre=$(this).parents(".tabla_creacion_dimensiones");
		$.ajax({
	        url: 'grabar_ajax.php',
	        type: 'POST',
	        dataType: 'JSON',
	        data: {
	        	'id_estandar': id_estandar,
	            'valor': "elim_estandar"
	        }
        }).done(function(resp) {
			 if(resp.codigo=='OK'){
			 	$this.parents(".tabla_creacion_dimensiones").find(".td_modificable").attr("width","180%");
			 	$this.parents(".tabla_creacion_dimensiones").find(".e_"+id_estandar).remove();

			 	var cant_fila_estandar = parseInt($(this).parents(".tabla_creacion_dimensiones").find("tr.fila_estandar").length);
			 	
			 	var letra1 = cant_fila_estandar+96;
				var letra = String.fromCharCode(letra1).toUpperCase();

			 	var i=letra1;
				$(padre).find("tr.fila_estandar").each(function(){
					i++;
					var letra = String.fromCharCode(i).toUpperCase();
					console.log(letra);
					$(this).find(".let_estandar").text(letra+") ");
				});

				var j=0;
				$(padre).find("tr.fila_pregunta").each(function(){
					j++;
					$(this).find(".let_pregunta").text(j+".- ");
				});
			}
		}).fail(function(err) {
	        console.log('Error no elimina estandar');
	        console.log(err.responseText);
	    });
	});


	$("body").on("click",".eliminar_dimension",function(){
		var id_dimension = $(this).parents("tr").attr("clase_dimension");
		var $this = $(this);
		var padre=$(this).parents(".tabla_creacion_dimensiones");
		$.ajax({
	        url: 'grabar_ajax.php',
	        type: 'POST',
	        dataType: 'JSON',
	        data: {
	        	'id_dimension': id_dimension,
	            'valor': "elim_dimension"
	        }
        }).done(function(resp) {
			 if(resp.codigo=='OK'){
			 	$this.parents(".tabla_creacion_dimensiones").remove();
			}
		}).fail(function(err) {
	        console.log('Error no elimina dimension');
	        console.log(err.responseText);
	    });
	});





	////////
	$("body").on('click', '.fila_dimension .elegir_dimension', function(e) {
                e.preventDefault();
                $.get('./mantenimiento.php', function(data) {
                    $('#dimensiones_bd .modal-body').html(data);
                });
        })

	$('#dimensiones_bd').on("click",".mostrar_estandares",function(){
        var $fila = $(this).closest('tr');
        $('#tblDimensiones tr.fila_agregada').remove();
        $('.btn.mostrar_estandares').find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
        if( $fila.hasClass('open') ){
            $('#tblDimensiones tr.dimension').removeClass('open');
            $(this).find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
        } else {
            var cant_cols = $fila.find('td').length;
            var id_dimension = $fila.attr("id_dimension");

            $('#tblDimensiones tr.dimension').removeClass('open');
            $(this).find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');

            $fila.addClass('open');
            $fila.after('<tr class="fila_agregada estandar">'+
                '<td colspan="'+cant_cols+'"><div class="tabla_estandar">'+
                '<img src="./images/loader.gif" alt="Cargando" class="center-block">'+
                '</div></td>'+
                '</tr>');
            $.ajax({
                url: "ver_estandares.php",
                type: "POST",
                data: { id_dimension: id_dimension},
                success: function(result){
                    $(".fila_agregada.estandar .tabla_estandar").html(result);
                }
            });
        }
    }).on("click",".mostrar_preguntas",function(){
        var $fila_est = $(this).closest('tr');
        $('#tblEstandares tr.fila_agregada').remove();
        $('.btn.mostrar_preguntas').find('i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
        if( $fila_est.hasClass('open') ){
            $('#tblEstandares tr.estandar').removeClass('open');
        } else {
            var cant_cols= $fila_est.find("td").length;
            var id_estandar = $fila_est.attr("id_estandar");

            $('#tblEstandares tr.estandar').removeClass('open');
            $(this).find('i').removeClass('icon-chevron-down').addClass('icon-chevron-up');

            $fila_est.addClass('open');
            $fila_est.after('<tr class="fila_agregada pregunta">'+
                '<td colspan="'+cant_cols+'"><div class="tabla_pregunta">'+
                '<img src="./images/loader.gif" alt="Cargando" class="center-block">'+
                '</div></td>'+
                '</tr>');
            $.ajax({
                url: "lista_preguntas.php",
                type: "POST",
                data: { id_estandar: id_estandar },
                success: function(result){
                    $(".fila_agregada.pregunta .tabla_pregunta").html(result);
                }
            });
        }
    }).on('click', '.seleccionar_dim', function(e) {
        e.preventDefault();
        var id_dimension = $(this).closest('tr').attr('id_dimension');
        var $modalPE = $('body');
        
        $.ajax({
            url: 'grabar_ajax.php',
            type: 'POST',
            dataType: 'json',
            data: {
                'id_dimension' : id_dimension,
                'id_rubrica' : $('#id_encuesta').val(),
                'valor': 'grabar_dimension_bd',
            },
            beforeSend: function() {
                $('#dimensiones_bd #tblDimensiones .btn.seleccionar_dim').attr('disabled', 'disabled');
            }
        }).done(function(resp) {
            if(resp.code="OK"){
                $('#dimensiones_bd button[data-dismiss="modal"]').trigger('click');
                var dim=resp.data;
                var $fila_dimension = $modalPE.find('.fila_dimension.editando');
                var otros_datos = JSON.parse(dim.otros_datos);
                var colspan = otros_datos.niveles;
                var tipo_pregunta = dim.tipo_pregunta;
                var cant_fila_dimension = parseInt($(".fila_dimension").length);
                var fecha = Date.now();

                var texto1 = "<div class = 'row'>"+
										"<div class = 'col-xs-12'>"+
											"<div class = 'table-responsive' style = 'overflow:initial;'>"+
												'<table class="table table-striped tabla_creacion_dimensiones" id="tabla_creacion_dimensiones_'+fecha+'" attr-id= "'+fecha+'" cellpadding="4">'+
													'<tbody>'+
														'<tr class="cabecera fila_add_dimension">'+
															'<td width="90%" colspan="2" rowspan="2" class="text-center td_modificable" style="background:#2196f3; color:#fff; border-right:2px solid #fff;"><h4 class = "titulo_plantilla">PONDERACIÓN ESTÁNDAR</h4></td>'+
															'<td width="10%" colspan="'+colspan+'" style="background:#2196f3;color:#fff;" class="fila_nivel text-center"><dt>NIVELES</dt></td>'+
														'</tr>'+
														'<tr class="por_nivel">';
				for (var i = 1; i <= parseInt(colspan); i++) {
					texto1+='<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">'+
						'<h5>'+i+'</h5>'+
					'</td>';
				}
				texto1+='</tr>'+
						'<tr class="fila_dimension editando d_'+dim.id_dimension+'" style="background:#fb8c00;color:#fff;" clase_dimension="'+dim.id_dimension+'" data-iddimension="'+dim.id_dimension+'">'+
							'<td colspan="'+(parseInt(colspan)+2)+'" class="td_dimension" style = "padding:10px;">'+
								'<div class="col-xs-6" style = "padding-top:5px;">'+
									'<dt class = "let_dimension text-left" style = "display:inline;font-size:17px;">DIMENSIÓN '+cant_fila_dimension+': '+
									'</dt>'+
									'<dt class="reemplazo_dimension cambio_texto" style = "display:inline;font-size:17px;">'+dim.nombre+
									'</dt>'+
								'</div>'+

								'<div class="col-xs-6 text-right">'+
									'<ul class="nav navbar-top-links navbar-right">'+   

										"<li style = 'padding-right:0px;' class = 'resaltado_subt'>"+
											'NIVELES: '+
											'<select class = "cant_niveles" style = "color: #000;padding:5px;border-radius:6px;">'+
												'<option value = "2" '+((colspan=="2")?"selected":"")+'>2</option>'+
												'<option value = "3" '+((colspan=="3")?"selected":"")+'>3</option>'+
												'<option value = "4" '+((colspan=="4")?"selected":"")+'>4</option>'+
												'<option value = "5" '+((colspan=="5")?"selected":"")+'>5</option>'+
											'</select>&nbsp;&nbsp;'+
                            			"</li>"+             
                    						menu1+

                            			"<li style = 'padding-right:0px;'>"+
                            				'<button class="btn btn-primary btn-circle agregar_estandar istooltip" data-placement="bottom" title="Agregar Estándar">'+
												'<i class="icon-plus"></i>'+
											'</button>'+
                            			"</li>"+
									
										"<li style = 'padding-right:0px;'>"+
											'<button class="btn btn-success btn-circle guardar_puntuacion istooltip" id_dimension="'+dim.id_dimension+'" data-toggle="modal" data-target="#ModalPuntuacion" data-placement="bottom" title="Establecer escalas de evaluación">'+
												'<i class="icon-th-large"></i>'+
											'</button>'+
                            			"</li>"+

                            			"<li style = 'padding-right:0px;'>"+
											'<button class="btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip" data-placement="bottom" title="Eliminar dimensión">'+
												'<i class="icon-trash"></i>'+
											'</button>'+
                            			"</li>"+
                            		"</ul>"+
								'</div>'+
							'</td></tr>'+
						'</tbody></table>'+
				"</div></div></div>";

                //if(tipo_pregunta=="PE" || tipo_pregunta=="SN"){
                	$(".fila_dimension.editando").replaceWith(texto1);
                var idtabla = $(".fila_dimension.d_"+dim.id_dimension).closest(".tabla_creacion_dimensiones").attr("id");
                var $tabla = $("#"+idtabla);
					$(".fila_dimension.editando").removeClass("editando");
                //}

                //agregarNuevaDimension($fila_dimension, dim)
                if(dim.estandar.length>0){
                    $.each(dim.estandar, function(key, est) {
                        var cant_fila_estandar = parseInt($tabla.find(".fila_estandar.d_"+dim.id_dimension).length);
                        var letra = cant_fila_estandar+97;
                        console.log($tabla.find('tr.d_'+dim.id_dimension));
                        $tabla.find('tr.d_'+dim.id_dimension).last().after('<tr style="background:#e8e8e7;" class="fila_estandar editando d_'+dim.id_dimension+'" clase_dimension="'+dim.id_dimension+'">'+
                                '<td class="td_estandar" colspan="'+(parseInt(colspan)+2)+'">'+
                                    '<div class="col-xs-10" style="padding-top:5px;">'+
                                    	'<dt class = "let_estandar text-right" style="display:inline;padding-top:5px">'+String.fromCharCode(letra).toUpperCase()+') '+
                                    	'</dt>'+
                                    	'<div class="new_estandar"></div>'+
                                    '</div>'+
                                    '<div class="col-xs-2 text-right">'+
                                        '<button class="btn btn-circle btn-primary save_estandar"><i class="icon-save"></i></button>'+
                                        '<button class="btn btn-circle btn-default cancelar_add_estandar"><i class="icon-remove"></i></button>'+
                                    '</div>'+
                                '</td>'+
                            '</tr>');
                        var $fila_est=$tabla.find('.fila_estandar.editando');
                        agregarNuevoEstandar($fila_est, est);

                        if(est.pregunta.length>0){
                            
                            $.each(est.pregunta, function(key, preg) {
                                var nroPreg = parseInt($tabla.find("tr.fila_pregunta").length)+1;
                                $tabla.find("tr.e_"+est.id_estandar).last().after("<tr style='background:#e8e8e7;' class='fila_pregunta editando d_"+dim.id_dimension+" e_"+est.id_estandar+"' clase_dimension='"+dim.id_dimension+"' clase_estandar='"+est.id_estandar+"'>"+
                                    "<td width='5%' style='background: #e0e8ef; text-align: right;'>"+
                                    	"<div class='let_pregunta' style='display:inline;'>"+nroPreg+".- </div>"+
                                    "</td>"+
                                    "<td style = 'background: #fcfce0; white-space:inherit;'><div class = 'new_pregunta' style = 'display:inline;'></div>"+
                                    "</td>"+
                                    "<td colspan = '"+colspan+"' class = 'niveles'>"+
                                        "<button class = 'btn btn-circle btn-primary save_pregunta'><i class = 'icon-save'></i></button>"+
                                        "<button class = 'btn btn-circle btn-default cancelar_add_pregunta'><i class = 'icon-remove'></i></button>"+
                                    "</td>"+
                                "</tr>");
                                var $fila_preg=$tabla.find('.fila_pregunta.editando');
                                agregarNuevaPregunta($fila_preg, preg,colspan,dim,est);
                            });
                        }
                    });
                }
                $(".agregar_dimension").removeAttr("disabled");

                /*
                $(".tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
                $(".tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
                $(".tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
                $(".tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");*/
            } else {
                new PNotify({
                    title: 'Error',
                    text: res.msg,
                    type: 'error',
                });
            }
        }).fail(function(err) {
            new PNotify({
                title: 'Error',
                text: 'Algo salió mal',
                type: 'error',
            });
            console.log(err.responseText);
        });
    });

	 var agregarNuevaDimension = function($tr_dimension, objDimension){
        $tr_dimension.attr("clase_dimension",objDimension.id_dimension);
        $tr_dimension.attr("data-iddimension", objDimension.id_dimension);
        $tr_dimension.find(".new_dimension").replaceWith("<div class='col-xs-8 reemplazo_dimension cambio_texto'>"+objDimension.nombre+"</div>");

        $tr_dimension.addClass("d_"+objDimension.id_dimension);
        console.log(objDimension);
        $tr_dimension.find(".btn.elegir_dimension").replaceWith("<button class='btn btn-success btn-circle guardar_puntuacion istooltip' id_dimension='"+objDimension.id_dimension+"1234' data-toggle='modal' data-target='#ModalPuntuacion' data-placement='bottom' title='Establecer escalas de evaluación'><i class='icon-th-large'></i></button>");
        $tr_dimension.find(".btn.cancelar_add_dimension").replaceWith("<button class = 'btn btn-danger btn-circle eliminar_dimension no_imprimir istooltip' data-placement='bottom' title='Eliminar dimensión'><i class = 'icon-trash'></i></button>");
        $tr_dimension.find(".btn.save_dimension").replaceWith("<button class = 'btn btn-primary btn-circle agregar_estandar no_imprimir istooltip' data-placement='bottom' title='Agregar Estándar'><i class = 'icon-plus'></i></button>");

        $tr_dimension.removeClass('editando');

        $(".tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
    };

    var agregarNuevoEstandar =  function($tr_estandar, objEstandar){
        var let_estandar = $tr_estandar.find(".let_estandar").text();
        $tr_estandar.attr("clase_estandar",objEstandar.id_estandar);
        $tr_estandar.attr("data-idestandar",objEstandar.id_estandar);
        $tr_estandar.find(".new_estandar").replaceWith("<div class = 'reemplazo_estandar cambio_texto' style = 'padding-top:5px;display:inline;'>"+objEstandar.nombre+"</div>");
        $tr_estandar.find("td").css("padding","5px");
        $tr_estandar.addClass("e_"+objEstandar.id_estandar);
        $tr_estandar.find(".let_estandar").text(let_estandar);
        $tr_estandar.find(".btn.cancelar_add_estandar").remove();
        $tr_estandar.find(".btn.save_estandar").replaceWith("<button class = 'btn btn-success btn-circle agregar_pregunta no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Agregar Pregunta'><i class = 'icon-plus'></i>"+
            "</button><button class = 'btn btn-danger btn-circle eliminar_estandar no_imprimir' data-mytooltip='tooltip1' data-placement='bottom' title='Eliminar estándar'><i class = 'icon-trash'></i></button>");
        $tr_estandar.removeClass('editando');
        $(".tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
    };

    var agregarNuevaPregunta = function ($tr_pregunta, objPregunta,cant_niveles,objDimension,objEstandar) {
        var columnas = '';
        var tr_alternativas = '';
        $tr_pregunta.attr("clase_pregunta",objPregunta.id_pregunta);
        $tr_pregunta.attr("data-idpregunta",objPregunta.id_pregunta);
        $tr_pregunta.find(".new_pregunta").replaceWith("<div class='col-xs-11 reemplazo_pregunta cambio_texto' style = 'padding-top:5px;'>"+objPregunta.nombre+
        	"</div>"+
            '<div class="xs-1 pull-right text-muted"><em><button class="btn btn-danger btn-circle eliminar_pregunta"><i class="icon-trash"></i></button></em></div>');
        $tr_pregunta.find(".cancelar_add_pregunta").remove();
        $tr_pregunta.find("td").css("padding","5px");
        $tr_pregunta.addClass("p_"+objPregunta.id_pregunta);

        //if(objDimension.tipo_plantilla=="PE" || objDimension.tipo_plantilla=="SN"){
        	for(var i=1;i<=cant_niveles;i++) {
            columnas += "<td class = 'text-center option_radio' style = 'background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;' class = 'text-center' onclick='return false;'><input type = 'radio'></td>";
	        }
	        $tr_pregunta.find('.save_pregunta').parents("td").remove().append(columnas);
	        $tr_pregunta.append(columnas);
       /* }
        else if(objDimension.tipo_plantilla=="A"){
        	tr_alternativas += '';
        }*/
        

        $tr_pregunta.removeClass('editando');
        $(".tabla_creacion_dimensiones").find(".agregar_estandar").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_dimension").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".agregar_pregunta").removeAttr("disabled");
        $(".tabla_creacion_dimensiones").find(".eliminar_estandar").removeAttr("disabled");
    };

    var con_cambio = function(obj,valor_nuevo,estilo){
        //$(obj).closest('.cambio_texto').addClass(estilo);
        $(obj).closest('.cambio_texto').attr("valor_estatico",valor_nuevo);
        $(obj).closest('.cambio_texto').html(valor_nuevo);
    };

    var sin_cambio = function(obj,valor_estatico,estilo){
        //obj.closest('.cambio_texto').addClass(estilo);
        obj.closest('.cambio_texto').html(valor_estatico);
    };

    $("body").on("click",".cambio_texto",function(){
        var valor_estatico = $(this).attr("valor_estatico");

        if(valor_estatico===undefined){ valor_estatico=$(this).text();}
        if($(this).find('input.cambio').length==0){
            //var valor_estatico = $(this).text();
            $(this).attr("valor_estatico", valor_estatico);
            $(this).html("<input type='text' class='form-control cambio' value='"+valor_estatico+"' >");
            $(".cambio").focus();
        }

    }).on("blur",".cambio_texto input.cambio",function(e){
        var $this = $(this);
        var $fila = $this.closest('tr');
        var valor_estatico = $this.closest('.cambio_texto').attr("valor_estatico");
        var valor_nuevo = $this.val().trim();
        var tecla_esc = $this.closest('.cambio_texto').attr("tecla_esc");
        var dataAjax = {
            'nombre':valor_nuevo,
            'valor': "",
        };
        if($this.closest("tr").hasClass('fila_dimension')){
            var id = $this.closest("tr").attr("data-iddimension");
            dataAjax['id_dimension'] = id;
            dataAjax['valor'] = 'editar_dimension';
        } else if ($this.closest("tr").hasClass('fila_estandar')){
            var id = $this.closest("tr").attr("data-idestandar");
            dataAjax['id_estandar'] = id;
            dataAjax['valor'] = 'editar_estandar';
        } else if($this.closest("tr").hasClass('fila_pregunta')){
            var id = $this.closest("tr").attr("data-idpregunta");
            dataAjax['id_pregunta'] = id;
            dataAjax['valor'] = 'editar_pregunta';
        }

        if(valor_nuevo.length>0 && valor_estatico != valor_nuevo && tecla_esc == "no") {
            $.ajax({
                url: "grabar_ajax.php",
                type: "POST",
                dataType: "json",
                data: dataAjax,
                beforeSend: function(){
                    $this.attr('disabled','disabled');
                    $fila.find('.btn').attr('disabled','disabled');
                }
            }).done(function(res){
                if(res.codigo=="OK") {
                    con_cambio($this, valor_nuevo, "cambio_texto");
                    new PNotify({
                        title: 'Listo',
                        text: 'Se actualizó el valor',
                        delay: 1500,
                        type:'success',
                    });
                } else {
                    sin_cambio($this, valor_estatico, 'cambio_texto');
                }
            }).fail(function(error){
                console.log("error-no-edita-dimension");
                new PNotify({
                        title: 'Error',
                        text: 'Ocurrió un error. No se guardaron los cambios.',
                        delay: 3000,
                        type: 'error',

                    });
                sin_cambio($this, valor_estatico, 'cambio_texto');
            }).always(function(){
                console.log($fila);
                $fila.find('.btn').removeAttr('disabled');
            });
            
        } else if(valor_nuevo.length==0) {
            $(".cambio").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><input type='text' class='form-control cambio' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></div>");
            $(".cambio").addClass("borde_rojo");
        } else if(valor_estatico == valor_nuevo) {
            con_cambio($this ,valor_nuevo, "cambio_texto");
        }
        $(this).closest('.cambio_texto').attr("tecla_esc","no");    
    }).on("keypress",".cambio_texto input.cambio",function(e){
        $(this).removeClass("borde_rojo");
        if(e.which == 13) {
            $(this).closest('.cambio_texto').attr("tecla_esc","no");    
            var padre = $(this).parents("tr");
            var hermano = $(padre).find(".cambio");
            $(hermano).trigger("blur"); 
        }
    }).on("keyup",".cambio_texto input.cambio",function(e){
        if(e.which == 27) { 
            var $this = $(this);
            $(this).closest('.cambio_texto').attr("tecla_esc","si");
            var valor_estatico = $this.closest('.cambio_texto').attr("valor_estatico");
            var valor_nuevo = $this.val();
            sin_cambio($this, valor_estatico, "cambio_texto");
        }
    });

    $("body").on("click",".guardar_puntuacion1",function(){
    
        var id_dimension= $(".dim_numero").val();
        var niveles = $(".niveles").val();
        var arreglo = {};
        var arreglo2 = {"niveles":niveles};

        var i=0;
        $(".fila").each(function(){
            var calif = $(this).find(".representacion").val();
            arreglo[calif]={
                "rango_inicial": $(this).attr("valor_inicial"),
                "rango_final": $(this).attr("valor_final")
            };
            
        });
       // console.log(arreglo);
        $.ajax({
            url: "grabar_ajax.php",
            type: "POST",
            dataType: "json",
            data: {
                id_dimension: id_dimension,
                niveles:arreglo2,
                arreglo:arreglo,
                valor: "grabar_puntuacion1"
            }
        })
        .done(function(res){
            new PNotify({
                title: 'Mensaje',
                text: 'Puntuación actualizada',
                delay: 2000
            })
        })
        .fail(function(error){
            console.log("error-no-guarda-puntuacion");
        })
    })


    $("body").on("click",".agregar_alternativa",function(){
    	var id_pregunta = $(this).parents(".grupo_alternativas").attr("clase_pregunta");
		var letra = 97;
			$(this).parents(".form-horizontal").find("input[type='radio'][name=p_"+id_pregunta+"_alternativa]").each(function(){
				letra++;
			});
			
		var letra1 = String.fromCharCode(letra);
		$(this).parents(".form-horizontal")
								.append("<div class = 'form-group fila_alternativa'>"+
											"<label class = 'control-label col-xs-1' letra = '"+String.fromCharCode(letra)+"'>"+letra1+")</label>"+
											"<div class = 'col-xs-6'>"+
												"<input type = 'text' class = 'form-control texto_alternativa'>"+
											"</div>"+
											"<div class = 'col-xs-1'>"+
												"<input type = 'radio' name = 'p_"+id_pregunta+"_alternativa' value = '"+letra1+"'>"+
											"</div>"+
											"<div class = 'col-xs-1'>"+
												"<button class = 'btn btn-danger btn-circle eliminar_alternativa'><i class = 'icon-trash'></i></button>"+
											"</div>"+
										"</div>");
	})

	$("body").on("click",".eliminar_alternativa",function(){
		var padre = $(this).parents(".form-horizontal");
		var id_pregunta = $(this).parents(".grupo_alternativas").attr("clase_pregunta");

		$(this).parents(".form-group").remove();
		var letra = 96;

		$(padre).find("input[type='radio'][name=p_"+id_pregunta+"_alternativa]").each(function(){
			letra++;
			var letra1 = String.fromCharCode(letra);
			$(this).parents(".form-group").find(".control-label").text(letra1+") ");
			$(this).parents(".form-group").find(".control-label").attr("letra",letra1);
			console.log("hola");
		
		});
	})	

	

	$("body").on("click",".edit_alternativas",function(){
		var $this = $(this);
		$this.parents(".grupo_alternativas").find(".alt_disabled").each(function(){

			var nombre = $(this).text();
			$(this).replaceWith("<div class = 'col-xs-6'><input type = 'text' class = 'form-control texto_alternativa' value = '"+nombre+"'></div>");
		});

		$this.parents(".grupo_alternativas").find("input[type='radio']").each(function(){
			$(this).prop("disabled",false);
			
		});

		$(this).replaceWith("<button class = 'btn btn-success btn-circle save_alternativas'><i class = 'icon-save'></i></button>");
		
	})

	$("body").on("click",".save_alternativas",function(){
		
		var id_pregunta = $(this).parents(".grupo_alternativas").attr("clase_pregunta");
	    var correcta = $("input[name='p_"+id_pregunta+"_alternativa']:checked").val();
	    var $this = $(this);
	    var bloque = {
	        "alternativas":{},
	        "correcta": correcta,
	    };

	    $(this).parents(".form-horizontal").find(".fila_alternativa").each(function(i,element){
	        var letra = $(this).find(".control-label").attr("letra").trim();
	        var alternativa = $(this).find(".texto_alternativa").val().trim();
	        bloque["alternativas"][letra] = alternativa;
	    });

	    $.ajax({
	        url: "grabar_ajax.php",
	        type: "POST",
	        dataType: "json",
	        data: {
	            'id_pregunta': id_pregunta,
	            'otros_datos_alt': JSON.stringify(bloque),
	            valor: "grabar_alternativa1"
	        }
	    }).done(function(res){
	   		if(res.codigo=="OK"){
	   			$this.parents(".grupo_alternativas").find(".texto_alternativa").each(function(){
					var nombre = $(this).val();
					$(this).parents(".col-xs-6").replaceWith("<label class = 'alt_disabled control-label col-xs-6' style = 'font-weight:normal;text-align:left;'>"+nombre+"</label>");
				});

				$this.parents(".grupo_alternativas").find("input[type='radio']").each(function(){
					$(this).prop("disabled",true);
				});

				$this.replaceWith("<button class = 'btn btn-info btn-circle edit_alternativas'><i class = 'icon-edit'></i></button>");
			}
	    }).fail(function(error){
	        console.log("error-no-guarda-alternativas");
			});
	});

	/****************/
	var con_cambio1 = function(obj,input_type,nombre)
	{
		if(input_type=="titulo")
		{
			$(obj).replaceWith("<input type = 'text' class = 'form-control titulo_rubrica' value = '"+nombre+"' placeholder = 'Escriba aquí su nombre'>");
			$(".titulo_rubrica").focus();
		}
		else if(input_type=="descripcion")
		{
			$(obj).replaceWith("<textarea class = 'form-control descripcion_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí su empresa'>"+nombre+"</textarea>");
			$(".descripcion_rubrica").focus();

		}
		else
		{
			$(obj).replaceWith("<textarea class = 'form-control pregunta_rubrica' style = 'margin-top:20px;' placeholder = 'Escriba aquí la pregunta'>"+nombre+"</textarea>");
			$(".pregunta_rubrica").focus();
		}
	}

	var sin_cambio1 = function(obj,input,input_type)
	{
		if(input_type=="titulo"){ $(obj).replaceWith(input);}
		if(input_type=="descripcion"){ $(obj).replaceWith(input); }
		if(input_type=="pregunta"){ $(obj).replaceWith(input); }
		
	}

	$("body").on("click",".escriba_titulo",function(){
		var nombre = $(this).attr("nombre");
		if(nombre.length==0){nombre="";}
		con_cambio1($(this),"titulo",nombre);
	});

	$("body").on("click",".escriba_descripcion",function(){
		var nombre = $(this).attr("nombre");
		if(nombre.length==0){nombre="";}
		con_cambio1($(this),"descripcion",nombre);
	});

	$("body").on("blur",".titulo_rubrica",function(){
		var nombre = $(this).val(); nombre1=nombre;
		if(nombre.length==0)
		{
			nombre = "Escriba aquí el título";
			nombre1 = "";
		}

		var input1 = "<div class = 'escriba_titulo responsable' nombre = '"+nombre1+"'>"+nombre+"</div>";
		sin_cambio1($(this),input1,"titulo");
	});

	$("body").on("blur",".descripcion_rubrica",function(){
		var nombre_desc = $(this).val(); nombre1_desc=nombre_desc;
		if(nombre_desc.length==0)
		{
			nombre_desc = "Escriba aquí el contenido";
			nombre1_desc = "";
		}
		var input2 = "<div class = 'escriba_descripcion escriba_descripcion1' nombre = '"+nombre1_desc+"'>"+nombre_desc+"</div>";
		sin_cambio1($(this),input2,"descripcion");
	});


	/********************/


}); //fin ready

</script>


</body>

</html>
