<?php
	//if (session_status() == PHP_SESSION_NONE) {
		@session_start();
	//}
	require_once("class/class_ajustes.php");

	$ses_idencuesta = $_SESSION["id_rubrica"];
	$obj = new Ajuste();
?>

<style>
	::-webkit-input-placeholder {
   font-style: italic;
	}
	:-moz-placeholder {
	   font-style: italic;  
	}
	::-moz-placeholder {
	   font-style: italic;  
	}
	:-ms-input-placeholder {  
	   font-style: italic; 
	}

	
	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
	    background-color: #fff;
	}
</style>

<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

<div class = 'modal-header text-center'>
	<h4><strong>AUTOCOMPLETAR</strong>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>
</div>

<div class = 'modal-body'>

	<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>

	<div class = 'form-horizontal'>
		<div class = 'form-group'>
			<div class = 'col-lg-12'>
				<div class = 'escriba_titulo' nombre=''>Escriba aquí la dimensión</div>
				<div class = 'escriba_descripcion' nombre = '' style = 'margin-top:20px;'>Escriba aquí el estándar</div>
				<div class = 'escriba_pregunta' nombre = '' style = 'margin-top:20px;'>Escriba aquí la pregunta</div>
			</div>
		</div>
		<br>	
		<div class = 'form-group'>
			<div class = 'col-lg-1'></div>
			<div class = 'col-lg-10'>
				<textarea class = 'form-control' readonly rows = '5'></textarea>
			</div>
			<div class = 'col-lg-1'></div>
		</div>
	</div>
</div>

<div class = 'modal-footer'>
	<div class = 'row'>
		<div class = 'col-lg-4 text-left'>
			<button  class="btn btn-default" data-dismiss="modal">Cerrar sin guardar datos</button>
		</div>
		<div class = 'col-lg-4'></div>
		<div class = 'col-lg-4'>
			<button class = 'btn btn-primary guardar_autocompletar' data-dismiss="modal"><i class = 'icon-save'></i> Guardar Pregunta</button>
		</div>
	</div>
</div>

<script>
	
</script>