<?php
//if (session_status() == PHP_SESSION_NONE) {
	@session_start();
//}
require_once("class/class_ajustes.php");
require_once("class/class_mantenimiento.php");
require_once("class/class_evaluacion.php");

if(!empty($_GET["id_rubrica"]))
{
	$ses_idrubrica = $_GET["id_rubrica"];
}
else
{
	$ses_idrubrica = $_SESSION["id_rubrica"];
}


$obj = new Ajuste();
$obj1 = new Evaluacion();
$obj2 = new Mantenimiento();
$ajuste = $obj->listar_idencuesta($ses_idrubrica);
$encuesta = $obj1->listar_evaluacion($ses_idrubrica); 
$cant_preg = ($encuesta=="")?0:count($encuesta);
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
	<!-- PAGE LEVEL TABLA STYLES -->
	<link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<script src ="plugins/dataTables/Spanish.js"></script>
	<!-- END PAGE LEVEL  STYLES -->

	<!-- PAGE LEVEL STYLES -->
	<link rel="stylesheet" href="plugins/validationengine/css/validationEngine.jquery.css" />
	<!-- END PAGE LEVEL  STYLES -->

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">

	<script src="js/pnotify.custom.min.js"></script>
	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">
	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

</head>

<style>
	::-webkit-input-placeholder {
		font-style: italic;
	}
	:-moz-placeholder {
		font-style: italic;  
	}
	::-moz-placeholder {
		font-style: italic;  
	}
	:-ms-input-placeholder {  
		font-style: italic; 
	}

	table tr td
	{
		padding:5px;
	}

	input[type="radio"]{
		width: 2em;
		height: 2em;
	}

	.form-control[readonly] { 
		background-color: #fff;
	}

	.poner_imagen{
		padding-left:5px;
		cursor: pointer;
		position: relative;
	}

	.img_check{
		position:absolute;
		bottom:0px;
		left:1em;
		right:0px;
		top: 0px;
		display: none;
		height: 100%
	}

	.navbar-top-links li a {
		padding: 10px 2px;
	}



}
</style>

<body>

	<div class = 'container-fluid'>

		<div class = 'row'>
			<div class = 'col-xs-12'>
				<?php //require_once("menu.php"); ?>
			</div>
		</div>

		<div class = 'row'>

			<div class = 'col-xs-1'></div>
			<div class = 'col-xs-10'>
				<input type = 'hidden' id='id_colegio'>
				<input type = 'hidden' class = 'cant_preguntas' value = '<?php echo $cant_preg;?>'>
				<input type = 'hidden' id='id_rubrica' value = '<?php echo $ses_idrubrica;?>'>
				<div class = 'panel panel-primary' id='panel_prim'>
					<div class = 'panel-heading text-center'><h4>RÚBRICA</h4></div>
					<div class = 'panel-body'>
						<input type = 'hidden' class = 'tipo_encuesta' value = '<?php echo $ajuste[0]["tipo_encuestado"];?>'>
						<button style = 'display: none;' class = 'ventana_modal' data-toggle="modal" data-target="#ventana_modal" >Ver datos</button>

						<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>
						<input type = 'hidden' id = 'id_docente'>
						<br>
						<div class = 'row'>
							
							<div class = 'col-lg-8'>
								<div class = 'marco' style="padding: 12px;background:#fdf8ec;">
								<div class = 'escriba_titulo'>
									<?php echo '"'.$ajuste[0]["titulo"].'"';?>
								</div>
								<br>
								<div class = 'escriba_descripcion'>
									<?php echo $ajuste[0]["descripcion"];?>
								</div>
								<br>
								<div class = 'escriba_descripcion'>
									<?php echo "<dt>Autor: </dt>".$ajuste[0]["autor"];?>
								</div>
							
								<hr>

								<?php 
									if(!empty($ajuste[0]["opcion_publico"]) and $ajuste[0]["opcion_publico"] == "I"){
										?>
										<p><em>Ciudad: <span class = 'txt_ciudad'></span></em></p>
										<p><em>Ugel:<span class = 'txt_ugel'></span></em></p>
										<p><em>Colegio:<span class = 'txt_colegio'></span></em></p>
										<p><em>Responsable:<span class = 'txt_responsable'></span></em></p>
										<?php
										if($_GET["id_rubrica"]==32){
											?>
											<p><em>Docente:<span class = 'txt_docente'></span></em></p>
											<?php
										}
									}
									else if($ajuste[0]["opcion_publico"] == "E"){
										?>
										<p><em>Nombre:</em></p>
										<p><em>Empresa:</em></p>
										<?php
									}
								?>

								</div>
							</div>

							
						
							<div class = 'col-lg-4 text-center'>
								<?php
								if(!empty($ajuste[0]["foto"])){
									$ruta = $ajuste[0]["foto"];
								}
								else{
									$ruta = "images/nofoto.jpg";
								}

								?>
								<img src = '<?php echo $ruta;?>'  class = 'img-responsive img-thumbnails istooltip' width="170px" data-tipo="image" style = "display:inline;width:100%;" data-url=".img-portada" alt title data-original-title="<?php echo 'Rúbrica: "'.$ajuste[0]["titulo"].'"';?>">
							</div>
						</div>
						<br>
						<div id = 'contenido_dimensiones'>

						<?php
						$cont=0;
						$cont_preg1=0;
						foreach($encuesta as $dim)
						{

							$tipo_pregunta = $dim["tipo_pregunta"];
							$div_niveles = "";
							$cant_niv=json_decode($dim["otros_datos"],true);
							$cant_niveles=$cant_niv["niveles"];

							if($tipo_pregunta=="PE"){
								$div_niveles.='<li style="padding-right:0px;" class="resaltado_subt">NIVELES: 
										<select class="cant_niveles" style="color: #000;padding:5px;border-radius:6px;">';
										for($i=2;$i<=5;$i++){
											$selected = "";
											if($cant_niveles==$i){ $selected = "selected";}

											$div_niveles .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
										}
								$div_niveles.= '</select>&nbsp;&nbsp;
									</li>';
							}

							$cont++;
							$menu="";
							//menu que va en todas las dimensiones
							
						?>

						<div class = 'row'>
							<div class = 'col-xs-12'>
							<input type="hidden" class="tipo_plantilla" value="<?php echo $tipo_pregunta;?>">
							<div class="table-responsive" style="overflow:initial;">
								<table class="table table-striped tabla_creacion_dimensiones" id="tabla_creacion_dimensiones_<?php echo $id_dimension?>" attr-id="<?php echo $id_dimension?>" cellpadding="4">
									<tbody>

									<?php
							
								$titulo_enc = "PONDERACIÓN ESTÁNDAR";
								$subtitulo = "NIVELES";
								$rowspan = "rowspan = '2'";

								if($tipo_pregunta=="SN"){ 
										$titulo_enc = "SI - NO";
										$cant_niveles = 2;
										$subtitulo = "OPCION";
									}
								else if($tipo_pregunta=="A"){ 
										$titulo_enc = "ALTERNATIVAS";
										$cant_niveles = 2;
										$rowspan = "";
									}	
								else if($tipo_pregunta=="C"){ 
										$titulo_enc = "COMPLETAR";
										$cant_niveles = 2;
										$rowspan = "";
									}

								$colspan = $cant_niveles+2;

								if($tipo_pregunta == "PE" || $tipo_pregunta == "SN")
								{
									?>
									<tr class="cabecera fila_add_dimension">
										<td width="90%" colspan="2" <?php echo $rowspan;?> class="text-center td_modificable" style="background:#2196f3; color:#fff; border-right:2px solid #fff;">
											<h4 class="titulo_plantilla"><?php echo $titulo_enc;?></h4>
										</td>
										<td width="10%" colspan="<?php echo $cant_niveles;?>" style="background:#2196f3;color:#fff;" class="fila_nivel text-center"><dt>NIVELES</dt>
										</td>
									</tr>
									<tr class="por_nivel">
										<?php

										if($tipo_pregunta == "PE"){
											for($i=1;$i<=$cant_niveles;$i++){
											?>
												<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">
													<h5><?php echo $i;?></h5>
												</td>
											<?php
											}
										}
										else{
											$array = array("SI","NO");

											for($i=0;$i<sizeof($array);$i++){
												?>
												<td class="text-center" style="background:#2196f3;color:#fff;border:1px solid #fff;">
													<h5><?php echo $array[$i];?></h5>
												</td>
												<?php
											}
										}
										
										?>
									</tr>
									<tr class="fila_dimension d_<?php echo $dim["id_dimension"];?>" style="background:#fb8c00;color:#fff;" clase_dimension="<?php echo $dim["id_dimension"];?>" data-iddimension="<?php echo $dim["id_dimension"];?>">
										<td colspan="<?php echo $colspan;?>" class="td_dimension" style="padding:10px;">
											<div class="col-xs-12" style="padding-top:5px;">
												<dt class="let_dimension text-left" style="display:inline;font-size:17px;">DIMENSIÓN <?php echo $cont;?>: 
												</dt>
												<dt class="reemplazo_dimension cambio_texto" style="display:inline;font-size:17px;" valor_estatico="<?php echo $dim["nombre"]?>" tecla_esc="no">
													<?php echo $dim["nombre"]?>		
												</dt>
											</div>
											
										</td>
									</tr>
									<?php
									$est1 = $obj2->listar_estandares($dim["id_dimension"]);

									$cont_est = 0;
									$cont_preg=0;
									foreach($est1 as $est){
										$cont_est++;
										$cont_letra = $cont_est+64;
										$letra = strtoupper(chr($cont_letra));
										?>
										<tr style="background:#e8e8e7;" class="fila_estandar d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" data-idestandar="<?php echo $est["id_estandar"];?>">
											<td class="td_estandar" colspan="<?php echo $colspan;?>">
												<div class="col-xs-12" style="padding-top:5px;">
													<dt class="let_estandar text-right" style="display:inline;padding-top:5px;">
														<?php echo $letra;?> 
													</dt>
													<div class="reemplazo_estandar cambio_texto" style="padding-top:5px;display:inline;" valor_estatico="<?php echo $est["nombre"];?>" tecla_esc="no">
														<?php echo $est["nombre"];?>
													</div>
												</div>
												
											</td>
										</tr>
										<?php
										
										$preg1 = $obj2->listar_preguntas($est["id_estandar"]);

										foreach($preg1 as $preg){
											$cont_preg++;
											$cont_preg1++;
											?>
											<tr style="background:#e8e8e7;" class="fila_pregunta d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>" data-idpregunta="<?php echo $preg["id_pregunta"];?>">
												<td width="5%" style="background: #e0e8ef; text-align: right;">
													<div class="let_pregunta" style="display:inline;">
														<?php echo $cont_preg;?>.- 
													</div>
												</td>
												<td width="75%" style="background: #fcfce0;" colspan="">
													<div class="col-xs-12 reemplazo_pregunta cambio_texto" style="padding-top:5px;" valor_estatico="<?php echo $preg["nombre"]?>" tecla_esc="no">
														<?php echo $preg["nombre"]?>
													</div>
													
												</td>

												<?php

												if($tipo_pregunta == "PE"){$a=0;}
												if($tipo_pregunta == "SN"){$a=2;}
												
												for($i=0;$i<$cant_niveles;$i++)
												{
													if($tipo_pregunta == "PE"){$a++;}
													if($tipo_pregunta == "SN"){$a--;}
												?>
													<td class="text-center option_radio" style="background: rgba(33, 150, 243, 0.21);border: 1px solid #ddd;" >
														<input type="radio" name = 'p_<?php echo $cont_preg1;?>' rpta = '<?php if($a==1){ echo "SI";} else {echo "NO";} ?>' value = '<?php echo $a; ?>'>
													</td>
												<?php
												}
												?>
											</tr>
											<?php
										}
									}
								}//FIN PONDERACIÓN EST-SINO
								
								else if($tipo_pregunta == "A" || $tipo_pregunta == "C")
								{
									?>
									<tr class="cabecera fila_add_dimension">
										<td width="90%" colspan="<?php echo $colspan;?>" <?php echo $rowspan;?> class="text-center td_modificable" style="background:#2196f3; color:#fff; border-right:2px solid #fff;">
											<h4 class="titulo_plantilla"><?php echo $titulo_enc;?></h4>
										</td>
									</tr>
									<tr class="fila_dimension d_<?php echo $dim["id_dimension"];?>" style="background:#fb8c00;color:#fff;" clase_dimension="<?php echo $dim["id_dimension"];?>" data-iddimension="<?php echo $dim["id_dimension"];?>">
										<td colspan="<?php echo $colspan;?>" class="td_dimension" style="padding:10px;">
											<div class="col-xs-6" style="padding-top:5px;">
												<dt class="let_dimension text-left" style="display:inline;font-size:17px;">DIMENSIÓN <?php echo $cont;?>: 
												</dt>
												<dt class="reemplazo_dimension cambio_texto" style="display:inline;font-size:17px;" valor_estatico="<?php echo $dim["nombre"]?>" tecla_esc="no">
													<?php echo $dim["nombre"]?>		
												</dt>
											</div>
											<div class="col-xs-6 text-right">
												<?php echo $menu;?>
											</div>
										</td>
									</tr>

									<?php
									$est1 = $obj2->listar_estandares($dim["id_dimension"]);

									$cont_est = 0;
									$cont_preg=0;
									
									foreach($est1 as $est){
										$cont_est++;
										$cont_letra = $cont_est+64;
										$letra = strtoupper(chr($cont_letra));
										?>
										<tr style="background:#e8e8e7;" class="fila_estandar d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" data-idestandar="<?php echo $est["id_estandar"];?>">
											<td class="td_estandar" colspan="<?php echo $colspan;?>">
												<div class="col-xs-12" style="padding-top:5px;">
													<dt class="let_estandar text-right" style="display:inline;padding-top:5px;">
														<?php echo $letra.") ";?> 
													</dt>
													<div class="reemplazo_estandar cambio_texto" style="padding-top:5px;display:inline;" valor_estatico="<?php echo $est["nombre"];?>" tecla_esc="no">
														<?php echo $est["nombre"];?>
													</div>
												</div>
												
											</td>
										</tr>

										<?php
										
										$preg1 = $obj2->listar_preguntas($est["id_estandar"]);

										foreach($preg1 as $preg){
											$cont_preg++;
											$cont_preg1++;
											?>
											<tr style="background:#e8e8e7;" class="fila_pregunta d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_dimension="<?php echo $dim["id_dimension"];?>" clase_estandar="<?php echo $est["id_estandar"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>" data-idpregunta="<?php echo $preg["id_pregunta"];?>">
												<td width="5%" style="background: #e0e8ef; text-align: right;">
													<div class="let_pregunta" style="display:inline;">
														<?php echo $cont_preg;?>.- 
													</div>
												</td>
												<td width="75%" style="background: #fcfce0;" colspan="">
													<div class="col-xs-12 reemplazo_pregunta cambio_texto" style="padding-top:5px;" valor_estatico="<?php echo $preg["nombre"]?>" tecla_esc="no"><?php echo $preg["nombre"];?></div>
													
												</td>
											</tr>
											<?php 
											if($tipo_pregunta=="A"){
												?>
												<tr class="grupo_alternativas d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>">
													<td colspan="3" style="background:rgba(91,192,222,0.08);">
														<table width="100%">
															<tbody>
																<tr>
																	<td>
																		<div class="form-horizontal">
																		<?php
																		if(!empty($preg["otros_datos"]))
																		{
																			$alternativas=json_decode($preg["otros_datos"],true);
																			$alt1=$alternativas["alternativas"];
																			$correcta = $alternativas["correcta"];
																			foreach($alt1 as $key=>$value){
																				?>
																				<div class="form-group fila_alternativa">
																					<label class="control-label col-xs-1" letra="<?php echo $key;?>"><?php echo $key;?>)</label>
																					<label class="alt_disabled control-label col-xs-6" style="font-weight:normal;text-align:left;"><?php echo $value;?></label>
																					<div class="col-xs-1">
																						<input type="radio" name='p_<?php echo $cont_preg1;?>' value="<?php echo $key;?>"  <?php /*if($key==$correcta){echo "checked";}*/?>>
																					</div>
																					
																				</div>
																				<?php
																			}
																		
																		}
																		else
																		{
																		?>
																		<div class = 'form-group fila_alternativa'>
																			<label class = 'control-label col-xs-1' letra = 'a'>
																				a)
																			</label>
																			<div class = 'col-xs-6'>
																				<input type = 'text' class = 'form-control texto_alternativa'>
																			</div>
																			<div class = 'col-xs-1'>
																				<input type = 'radio' name = 'p_<?php echo $cont_preg1;?>' value = 'a'>
																			</div>
																			
																		</div>

																		<div class = 'form-group fila_alternativa'>
																			<label class = 'control-label col-xs-1' letra = 'b'>
																				b)
																			</label>
																			<div class = 'col-xs-6'>
																				<input type = 'text' class = 'form-control texto_alternativa'>
																			</div>
																			<div class = 'col-xs-1'>
																				<input type = 'radio' name = 'p_<?php echo $cont_preg1;?>' value='b'>
																			</div>
																		</div>
																		<?php
																		}
																		?>
																		</div>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<?php
											}else if($tipo_pregunta=="C"){
												?>
												<tr class="completar d_<?php echo $dim["id_dimension"];?> e_<?php echo $est["id_estandar"];?> p_<?php echo $preg["id_pregunta"];?>" clase_pregunta="<?php echo $preg["id_pregunta"];?>">
													<td colspan="3" style="background:rgba(91,192,222,0.08);">
														<table width="100%">
															<tbody>
																<tr>
																	<td>
																		<textarea class="form-control comp_pregunta" style="width:90%" rows="6" ></textarea>
																	</td>
																</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<?php
											}
											
										}//fin pregunta
									}//fin estandar
								}//fin tipo alternativas
								?>

									</tbody>
								</table>
							</div>
						</div>
						</div>
						
							<?php
						}
						?>


						</div> <!--fin contenido dimensiones-->

						<div class = 'row'>
						<div class = 'col-lg-12 text-right'>
							<a class = 'btn btn-primary enviar'>Enviar</a>
						</div>
					</div>

					</div>
				</div>
			<div class = 'col-xs-1'></div>
		</div>

		<div class = 'row aviso_enviado' style = 'display:none;'>
			<div class = 'col-lg-2'></div>
			<div class = 'col-lg-8'>
				<div class = 'panel panel-primary'>
					<div class = 'panel-header'>
					</div>
					<div class = 'panel-body text-center'>
						<p class = 'text-primary'>SUS RESPUESTAS HAN SIDO ENVIADAS</p>
						<h4>GRACIAS</h4>
					</div>
				</div>
			</div>
			<div class = 'col-lg-2'></div>
		</div>



	</div>

	<div class="modal" id="ventana_modal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div id = 'area_modal'>
               </div>
            </div>
        </div>
    </div>

<script>


	$(document).ready(function(){

		$(".ventana_modal").click(function(){
			var id_rubrica = $("#id_rubrica").val();
		$.ajax({
	        url: "form_datos.php",
	        type: "POST",
	        data:{"id_rubrica": id_rubrica},
	        success: function(result){
	            $("#area_modal").html(result);
	        }
	    });
	});

		if($(".tipo_encuesta").val()=="P")
		{
			$(".ventana_modal").trigger("click");
			$("#panel_prim").css("display","none");
		}

	

	$("body").on("click",".continuar",function(){
		var nombre_persona = $(".responsable").attr("nombre");
		var nombre_empresa = $(".escriba_descripcion").attr("nombre");

		if(nombre_persona.length>0){
			var ciudad = "TACNA";
			var id_ugel = $(".ugel").val();
			var nombre_ugel = $(".ugel option:selected").text();
			var colegio = $("input.selec_ie:checked").val();
			var arr_colegio = colegio.split(",");
			var id_colegio = arr_colegio[0];
			var nombre_colegio = arr_colegio[1];
			var id_docente = $(".docente").val();
			var nombre_docente = $(".docente option:selected").text();

			$(".nombre_persona").val(nombre_persona);
			$(".nombre_empresa").val(nombre_empresa);
			$("#id_docente").val(id_docente);

			$(".txt_ciudad").text(ciudad);
			$(".txt_ugel").text(nombre_ugel);
			$(".txt_colegio").text(nombre_colegio);
			$(".txt_responsable").text(nombre_persona);
			$("#id_colegio").val(id_colegio);
			$(".txt_docente").text(nombre_docente);

			$('.modal').modal().hide();
			$('.modal-open').css("overflow","scroll");
			$(".modal-backdrop").hide();
			$("#panel_prim").css("display","block");
		}
		else{
			$(this).parents(".modal").find(".escriba_titulo").css("color","red");
		}
		
	});

	$(".escriba_titulo").keyup(function(){
		$(".escriba_titulo").css("color","#807d7d");
	});


	$(".enviar").click(function(){
		var id_rubrica = $("#id_rubrica").val();
		var nombre = $(".txt_responsable").text();
		var id_colegio = $("#id_colegio").val();
		var institucion = $(".nombre_empresa").val();
		var id_docente = $("#id_docente").val();
		var a=0;
		var arreglo = {};
		new PNotify({
					title: 'Mensaje',
					text: 'Enviando respuestas',
					delay: 5000
				});

		$(".fila_pregunta").each(function(){
			a++;
			var id_dimension = $(this).attr("clase_dimension");
			var id_pregunta = $(this).attr("clase_pregunta");
			var tipo_pregunta = $(this).parents(".row").find(".tipo_plantilla").val();

			var puntaje = 0;
			var respuesta = "";
			var $this = $(this);
			//var elem = {}
			if(tipo_pregunta=="PE"){
				respuesta = $this.find("input[type=radio][name=p_"+a+"]:checked").val(); 

				puntaje = $this.find("input[type=radio][name=p_"+a+"]:checked").val();
			}
			else if(tipo_pregunta=="SN"){
				respuesta = $this.find("input[type=radio][name=p_"+a+"]:checked").attr("rpta"); 

				puntaje = $this.find("input[type=radio][name=p_"+a+"]:checked").val();
			}
			else if(tipo_pregunta=="A"){
				respuesta =$("input.p_"+a+":checked").val();
				//puntaje = $(this).find("input.alt_puntaje").val();
				puntaje = 1;
			}
			if(tipo_pregunta=="C"){
				respuesta=$this.parents(".row").find(".comp_pregunta").val();
			}

			arreglo[id_pregunta]={
				"respuesta":respuesta,
				"puntaje_obtenido":puntaje,
				"id_dimension":id_dimension
			};
			
		});
		//console.log(arreglo);
		//console.log(JSON.stringify(arreglo));
		
		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				"id_rubrica": id_rubrica,
				"nombre": nombre,
				"id_colegio": id_colegio,
				"institucion": institucion,
				"id_docente": id_docente,
				"arreglo": (arreglo),
				"valor": "grabar_examen"
			}
		}).done(function(res){
			
			if(res.codigo=="OK")
	    	{
				setTimeout (function(){
		            $("#panel_prim").css("display","none");
		            $(".aviso_enviado").css("display","block")
		        }, 1500);
		    }
		}).fail(function(error){
	        console.log("no-envia-respuestas");
	        console.log(error.responseText);
	    });
	})



	});
</script>


</body>

</html>
