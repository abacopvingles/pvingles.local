<?php
	require_once("class/class_mantenimiento.php");
	$obj = new Mantenimiento();
?>

<!doctype html>

<head>
	<meta charset="utf-8">
	<title>ENCUESTAS</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="js/jquery-3.1.1.min.js"></script>

	<link rel="stylesheet" href="plugins/Font-Awesome/css/font-awesome.css" />

	<!-- PAGE LEVEL TABLA STYLES -->
    <link href="plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <script src ="plugins/dataTables/Spanish.js"></script>
    <!-- END PAGE LEVEL  STYLES -->

    <script src="js/pnotify.custom.min.js"></script>
  	<link href="css/pnotify.custom.min.css" rel="stylesheet" type="text/css">

	<link rel="stylesheet" href="css/style_1.css">
	<link rel="stylesheet" href="css/hover.min.css">
</head>

<body>

<div class = 'container-fluid'>

	<div class = 'row'>
	    <div class = 'col-lg-12'>
		    <?php
		        require_once("menu.php");
		    ?>
	    </div>
	</div>

	<div class = 'row' style="margin-top: 60px;">
		<div class = 'col-lg-4'>
			<br><button class = 'btn btn-danger nueva_pregunta' data-toggle='modal' data-target='#myModal'>NUEVA PREGUNTA</button>
		</div>
		<div class = 'col-lg-8'>
			<h3>Preguntas</h3>
		</div>
	</div>
	<br>
	<div class = 'row'>
		<div class = 'col-lg-4'>
			<select class = 'form-control rubrica'>
				<option value = '0' class = ''>Seleccione Rúbrica</option>
				<?php
					$rubrica=$obj->listar_rubrica();
					foreach($rubrica as $valor)
					{
						echo "<option value = '".$valor["id_rubrica"]."'>".$valor["nombre"]."</option>";
					}
				?>
			</select>
		</div>
		<div class = 'col-lg-4'>
			<select class = 'form-control dimension'>
				<option value = '0'>Seleccione Dimensión</option>
			</select>
		</div>
		<div class = 'col-lg-4'>
			<select class = 'form-control estandar'>
				<option value = '0'>Seleccione Estándar</option>
			</select>
		</div>
	</div>
	<br>
	<div class = 'row'>
		<div class = 'col-lg-12 text-center'>
			<div class = 'table-responsive'>
				<table class = 'table table-striped table-condensed table-hover display' id='example1'>
					<thead>
						<tr style = 'background-color:#428bca; color: #fff;'>
							<th>N°</th>
							<th>Nombre Pregunta</th>
							<th>Tipo</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody class = 'llenado_preguntas'>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	           	<div id = 'nueva_pregunta'>
	           	</div>
		    </div>
	    </div>
	</div>

</div>

<script>
	$(".rubrica").change(function(){
		var id_rubrica = $(this).val();

		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_rubrica: id_rubrica,
				valor: "listar_dimensiones_rubrica"
			}
		})

		.done(function(res){
	    	if(res.codigo=="OK")
	    	{
	    		$(".dimension").html("<option value = '0'>Seleccione Dimensión</option>"+res.select);

	    	}
	    })

	    .fail(function(error){
	        console.log("error-no-lista-dimensiones");
	    })
	});

	$(".dimension").change(function(){
		var id_dimension = $(".dimension").val();

	    $.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
					id_dimension: id_dimension,
					valor: "listar_estandares_dimension"
				}
			})

		.done(function(res1){
			$(".estandar").html("<option value = '0'>Seleccione Estándar</option>"+res1.select);
		})

		.fail(function(error){
		    console.log("error-no-lista-estandares");
		})
	})

	$(".estandar").change(function(){
		var id_estandar = $(".estandar").val();

		$.ajax({
			url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_estandar: id_estandar,
				valor: "listar_preguntas_estandar"
			}
		})

		.done(function(res){
			$(".llenado_preguntas").html(res.tabla);
		})

		.fail(function(error){
		    console.log("error-no-lista-preguntas");
		})
	})

	$(".container-fluid").on("click",".nueva_pregunta",function(){
		var id_estandar = $(".estandar").val();
		$.ajax({
                url: "nueva_pregunta.php",
                type: "POST",
                data: { id_estandar: id_estandar},
                success: function(result){
                     $("#nueva_pregunta").html(result);
                    }
                });
	});
</script>
</body>

</html>