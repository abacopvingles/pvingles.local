<?php
	//if (session_status() == PHP_SESSION_NONE) {
      @session_start();
  	//}
	require_once("class/class_ajustes.php");

	$ses_idencuesta = $_SESSION["id_rubrica"];
	$obj = new Ajuste();
?>

<style>
	::-webkit-input-placeholder {
   font-style: italic;
	}
	:-moz-placeholder {
	   font-style: italic;  
	}
	::-moz-placeholder {
	   font-style: italic;  
	}
	:-ms-input-placeholder {  
	   font-style: italic; 
	}

	
	input[type="radio"]{
		width: 2em;
		height: 2em;
	}
</style>

<link rel="stylesheet" href="bootstrap/css/MoneAdmin.css">

<div class = 'modal-header text-center'>
	<h4><strong>ALTERNATIVAS</strong>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>
</div>

<div class = 'modal-body'>
	<input type = 'hidden' id = 'id_encuesta' value = '<?php echo $_SESSION["id_rubrica"];?>'>

	<div class = 'form-horizontal'>
		<div class = 'form-group'>
			<div class = 'col-xs-12'>
				<div class = 'escriba_titulo cambio_texto' nombre=''>Escriba aquí la dimensión</div>
				<div class = 'escriba_descripcion' nombre = '' style = 'margin-top:20px;'>Escriba aquí el estándar</div>
				<div class = 'escriba_pregunta' nombre = '' style = 'margin-top:20px;'>Escriba aquí la pregunta</div>
			</div>
		</div>
		<br>	
		<div class = 'form-group fila_alternativa'>
			<label class = 'control-label col-lg-1' letra = 'a'>
				a)
			</label>
			<div class = 'col-lg-6'>
				<input type = 'text' class = 'form-control texto_alternativa'>
			</div>
			<div class = 'col-lg-1'>
				<input type = 'radio' name = 'alternativa' value = 'a'>
			</div>
			<div class = 'col-lg-1 text-left'>
				<button class = 'btn btn-primary btn-circle agregar_alternativa'><i class = 'icon-plus'></i></button>
			</div>
		</div>
		<div class = 'form-group fila_alternativa'>
			<label class = 'control-label col-lg-1' letra = 'b'>
				b)
			</label>
			<div class = 'col-lg-6'>
				<input type = 'text' class = 'form-control texto_alternativa'>
			</div>
			<div class = 'col-lg-1'>
				<input type = 'radio' name = 'alternativa' value="b">
			</div>
		</div>
		<div id = 'llenado_alternativas'>
		</div>
	</div>
</div>

<div class = 'modal-footer'>
	<div class = 'row'>
		<div class = 'col-lg-4 text-left'>
			<button  class="btn btn-default" data-dismiss="modal">Cerrar sin guardar datos</button>
		</div>
		<div class = 'col-lg-4'></div>
		<div class = 'col-lg-4'>
			<button class = 'btn btn-primary guardar_alternativa' data-dismiss="modal"><i class = 'icon-save'></i> Guardar Pregunta</button>
		</div>
	</div>
</div>

<script>
	$(".agregar_alternativa").click(function(){
		var letra = 97;
			$(".modal-body").find("input[type='radio'][name=alternativa]").each(function(){
				letra++;
			});
		var letra1 = String.fromCharCode(letra);
		$("#llenado_alternativas").append("<div class = 'form-group fila_alternativa'>"+
											"<label class = 'control-label col-lg-1' letra = '"+String.fromCharCode(letra)+"'>"+letra1+")</label>"+
											"<div class = 'col-lg-6'>"+
												"<input type = 'text' class = 'form-control texto_alternativa'>"+
											"</div>"+
											"<div class = 'col-lg-1'>"+
												"<input type = 'radio' name = 'alternativa' value = '"+letra1+"'>"+
											"</div>"+
											"<div class = 'col-lg-1'>"+
												"<button class = 'btn btn-danger btn-circle eliminar_alternativa'><i class = 'icon-trash'></i></button>"+
											"</div>");
	})

	$(".modal-body").on("click",".eliminar_alternativa",function(){
		
		$(this).parents(".form-group").remove();
		var letra = 97;

			$(".modal-body").find("input[type='radio'][name=alternativa]").each(function(){
			
				var letra1 = String.fromCharCode(letra);

				$(this).parents(".form-group").find(".control-label").text(letra1+") ");
				$(this).parents(".form-group").find(".control-label").attr("letra",letra1);
				letra++;
			
			});
	})	
</script>