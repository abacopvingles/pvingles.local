<?php
/**
 * @autor		Sistec Peru
 */

class DatConfiguracion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function listar($autocargar = true)
	{//02.01.13
		try {
			$sql = "SELECT config, valor FROM sis_configuracion";
			$sql .= (true == $autocargar) ? " WHERE autocargar = 'si'" : "";
			
			return $this->oBD->consultarSQL($sql, 'ARRAY', 'ASOC', 'config');
		} catch(Exception $e) {
			throw new Exception("ERROR\nObtener configuración: " . $e->getMessage());
		}
	}
	
	public function get($config)
	{//03.16
		try {
			$sql = "SELECT valor FROM sis_configuracion WHERE config = " . $this->oBD->escapar($config);
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\nObtener configuración: " . $e->getMessage());
		}
	}
	
	public function set($config, $valor)
	{//02.01.13
		try {
			return $this->oBD->update('sis_configuracion', array('valor' => $valor), array('config' => $config));
		} catch(Exception $e) {
			throw new Exception("ERROR\nActualizar configuración: " . $e->getMessage());
		}
	}
}