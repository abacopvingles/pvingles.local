var compressor = require('node-minify');
compressor.minify({
  compressor: 'gcc',
  input: ['../static/tema/js/jquery.min.js','../static/tema/js/bootstrap.min.js','../static/libs/slick/slick.min.js','js/funciones.js','js/page.js'],
  output: '../static/tema/js/pageall.min.js',
  callback: function (err, min){}
});


compressor.minify({
  compressor: 'clean-css',
  input: ['../static/tema/css/bootstrap.min.css','../static/tema/css/font-awesome.min.css','../static/libs/slick/slick.css','../static/libs/slick/slick-theme.css',
          '../static/tema/css/animate.css','../static/tema/css/hover.css','../static/libs/pnotify/pnotify.min.css','css/preload.css','css/colores.css','css/general.css'],
  output: '../static/tema/css/pageall.min.css',
  options: {
    advanced: false, // set to false to disable advanced optimizations - selector & property merging, reduction, etc.
    aggressiveMerging: false // set to false to disable aggressive merging of properties.   
  },
  callback: function (err, min){}
});

/*compressor.minify({
  compressor: 'clean-css',
  input: ['css/preload.css/'],
  output: '../static/tema/css/preload.min.css',
  options: {
    advanced: false, // set to false to disable advanced optimizations - selector & property merging, reduction, etc.
    aggressiveMerging: false // set to false to disable aggressive merging of properties.   
  },
  callback: function (err, min){}
});*/
