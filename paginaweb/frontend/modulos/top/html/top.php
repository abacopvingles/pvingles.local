<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
@session_start();
$islogin=!empty($_SESSION["loginempresa"])?true:false;
?>
<style type="text/css">
html, body{
      font-family: 'Open Sans',Helvetica,Arial;
    font-style: normal;
    font-weight: 400;
}
html, html a{
  -webkit-font-smoothing: antialiased;
  text-shadow: 1px 1px 1px rgba(0,0,0,.004);
  text-decoration: none;
}
.body{
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
      font: 15px;
}
.submenu{
  position: absolute;
  width: 100%;
  left: 0px;
  background: rgba(255,255,255,0.95);  
}
.submenu .row{
  margin: 0px;
  padding: 0px;
}
.submenu a{
  font: 15px dinpro-regular;
}
div.search{ position: relative;}
div.search input{ 
    padding-left: 35px;    
    padding-left: 35px;
    border: 0px;
    border-bottom: 1px #fff solid;
    background: transparent;
    color:#fff;
}
div.search input::placeholder {  color:#fff;}
div.search input::focus{
    color: #495057;
    background-color: #03a3fca8;
}

div.search::before{
  content:"\f002";
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  text-decoration: inherit;
  padding-right: 0.5em;
  position: absolute;
  top: 7px;
  left: 10px;
  color: #b7bfc7;  
}
.header{
  background: url(<?php echo $this->documento->getUrlStatic() ?>/media/web/bg-tv);
  position: relative;
}
#menutop{
  width: 100%;  
  background: transparent;
  position: absolute;
  top: 0px;
  z-index: 999;
}
#navbarSupportedContent ul li { padding-left: 10px; padding-right:10px; }
#navbarSupportedContent ul li > .submenu{ display: none;  padding-top: 1em; border-top: 1px solid #aaa;  }
#navbarSupportedContent ul li:hover > .submenu ,#navbarSupportedContent ul li.active > .submenu{ display: block; }
.infoslider{
  position: absolute;
  width: 100%;
  top: calc(50% - 180px);
  color: #fff;
  padding: 1rem;
}
.infoslider .info1{
    font-size: 13.861px;
    line-height: 24px;
    color: #fff;
    font-family: dinpro-regular;
}
.infoslider .infocontent{
  max-width: 850px;
  margin:auto;
}
.info2{
    color: #fff;
    width: 336px;
    font-size: 2.5rem;
    font-family: dinpro-bold;
    line-height: 48px;
    letter-spacing: -1.19px;
}
.infoslider .info3{
  font-size: 22.43px;
  font-family: dinpro-regular;
  line-height: 32px;
  letter-spacing: -.32px;
  max-width: 680px;
  margin-top: 25px;
}
.btnunete{
    margin-top: 30px;
    background: transparent;
    box-shadow: inset 0px 0px 0px 2px #FFFFFF;
    border-radius: 25px;
    color:#fff;
    position: relative;
   transition: background 375ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,opacity 375ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 375ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;   
}

.btnunete:hover{
  color: rgb(0, 153, 255);
  background: #FFFFFF;
  box-shadow: inset 0px 0px 0px 2px #FFFFFF;
}
.menufijocolor{
  background-color: #fff !important;
  box-shadow: 0 1px 2px rgba(0,0,0,.1);
  position: fixed !important;
  top: 0px;
  z-index: 999;
}
.navbar-light .navbar-nav .nav-link {
      color: #454545!important;
      font: 16px dinpro-regular;
      text-decoration: none;
      vertical-align: baseline;
}
.tituloblue{
  color: rgba(35, 10, 178, 0.9);
  font-size: 1.5rem;
}
.showcategorias li{ list-style: none; }
.showcategorias a{
    /* font: 15px dinpro-regular; */
    font-size: 14px;
    font-family: dinpro-regular;
    color: #454545;
    text-decoration: none;
    cursor: pointer;
    display: block;
    margin-bottom: 13px;
    -webkit-line-clamp: 1;
    line-clamp: 1;
}
#showcursos > div{
  text-decoration: none;
    cursor: pointer;
    display: block;
   font-family: dinpro-regular;
    color: #454545;
    height: 20px;
    overflow: hidden;
    font-size: 14px;
}


</style>
<nav class="navbar navbar-expand-lg navbar-light" id="menutop">
  <a class="navbar-brand tituloblue" href="#"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/empresas/logo000.jpg" width="50px" height="50px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cursos
        </a>
        <div class="submenu">
          <div class="row">
            <div class="container">
              <div class="row">
                <div class="col-md-3 col-sm-12">
                  <h2 class="tituloblue text-center">Categorias</h2> 
                  <div>
                    <ul class="showcategorias"></ul>
                  </div>           
                </div>
                <div class="col-md-9 col-sm-12">
                  <h2 class="tituloblue text-center">Cursos</h2>
                  <div class="row" id="showcursos">
                    
                  </div>
                </div>                
              </div>
            </div>
          </div> 
        </div>
      </li>      
      <li class="nav-item">
          <div class="search">
            <input type="text" class="form-control" placeholder="Buscar Cursos...">
          </div>
      </li>         
    </ul>
    <ul class="navbar-nav pull-right">
      <li class="nav-item active">
        <a class="nav-link" href="javascript:void(0)">Entrar</a>
      </li>
      <li class="nav-item active ">
        <a class="btn btn-primary border-white" href="javascript:void(0)">Registrarse</a>
      </li>     
    </ul>  
  </div>
</nav>
<div class="header text-center" style="height: calc(100vh);">
  <div class="row">
    <div class="col-12 text-center infoslider">
      <div class="infocontent">
        <span class="info1">Impulsa tu crecimiento profesional</span><br>
        <span class="info2"><b>Tu talento al siguiente nivel</b></span><br>
        <span class="info3">Aprende nuevas habilidades de la mano de profesionales expertos en todo el mundo.<br> Sin horarios, sin límites. Completamente online.</span><br>
        <span><button class="btn btnunete">Unete Gratis</button></span><br>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  var oldscroll=0;
  $(document).ready(function(ev){
     var header = $("#menutop");
      $(window).scroll(function() {
          var scroll = $(window).scrollTop();
          if(scroll>100&oldscroll>scroll){header.addClass("menufijocolor")}
          else{
              header.removeClass("menufijocolor");
          }
          oldscroll=scroll
      });
  })
</script>