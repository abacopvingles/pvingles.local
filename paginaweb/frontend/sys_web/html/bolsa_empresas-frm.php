<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Bolsa_empresas"); ?></li>
  </ol>
</nav>
<?php } ?><div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="Idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Rason social');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="rason_social<?php echo $idgui; ?>" name="rason_social" required="required" class="form-control" value="<?php echo @$frm["rason_social"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Ruc');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="ruc<?php echo $idgui; ?>" name="ruc" required="required" class="form-control" value="<?php echo @$frm["ruc"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Logo');?> <span class="required"> (*) </span></label>              
                              <div style="position: relative;" class="frmchangeimage">
                <div class="toolbarmouse text-center">                  
                  <span class="btn btnaddfile"><input type="file" class="input-file-invisible" name="logo" id="logo<?php echo $idgui; ?>"><i class="fa fa-pencil"></i></span>
                </div>
                <div id="sysfilelogo<?php echo $idgui; ?>"><?php echo @$file; ?></div>                
              </div>               
                
            </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Direccion');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="direccion<?php echo $idgui; ?>" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Telefono');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="telefono<?php echo $idgui; ?>" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Representante');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="representante<?php echo $idgui; ?>" name="representante" required="required" class="form-control" value="<?php echo @$frm["representante"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Usuario');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="usuario<?php echo $idgui; ?>" name="usuario" required="required" class="form-control" value="<?php echo @$frm["usuario"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Clave');?> <span class="required"> (*) </span></label>              
               <input type="password" id="clave<?php echo $idgui; ?>" name="clave" class="form-control" required /> 
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Correo');?> <span class="required"> (*) </span></label>              
                <input type="email" id="correo<?php echo $idgui; ?>" name="correo" required="required" class="form-control" value="<?php echo @$frm["correo"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Estado');?> <span class="required"> (*) </span></label>              
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" id="estado<?php echo $idgui; ?>" name="estado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                              </div>
            
    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-saveBolsa_empresas" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_empresas'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'bolsa_empresas', 'saveBolsa_empresas', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
       <?php if(!empty($fcall)){ ?> if(typeof <?php echo $fcall?> == 'function'){
          <?php echo $fcall?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php } ?>  return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_empresas"))?>');
      }
     }
  }); 
  
$('#logo<?php echo $idgui; ?>').click(function(ev){  
    $(this).attr('accept','image/*');   
    //if(tipo=='pdf') $(this).attr('accept','application/pdf');
    //else $(this).attr('accept','application/*');
})

var inputlogo<?php echo $idgui; ?> = document.getElementById('logo<?php echo $idgui; ?>');  
inputlogo<?php echo $idgui; ?>.addEventListener('change', addfilelogo<?php echo $idgui; ?>, false);
function addfilelogo<?php echo $idgui; ?>(ev) {
  var file=ev.target.files[0];
  var tipo='imagen';
  var donde=$('#sysfilelogo<?php echo $idgui; ?>');
  var reader=new FileReader();      
  reader.onload=function(ev_){
    var filetmp=document.createElement('img');          
    filetmp.style.width='60%';
    filetmp.style.height='200px';
    filetmp.classList.add("img-responsive");
          filetmp.classList.add("center-block");
          filetmp.src=ev_.target.result;
          
    if(filetmp!='') donde.html('').append(filetmp);
  }
  reader.readAsDataURL(file);
}
  
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });
</script>