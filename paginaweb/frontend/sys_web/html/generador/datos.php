<?php 
$tb=ucfirst($this->tabla);
$campos=$this->campos;
$campopk=$this->pk;
$frm=$this->frm;
$inner='';
$nuevalista='';
$ifk=1;
$aliastb='tb1';
$camposfk='';
echo '<?php
'; ?>
<?php 
		$x='';
		$strcampos='';
		$filtros='';
		foreach ($campos as $campo) {
					$filtros.='
			if(isset($filtros["'.$campo.'"])) {
					$cond[] = "'.$campo.' = " . $this->oBD->escapar($filtros["'.$campo.'"]);
			}';
					if(@$frm["tipo_".$campo]=='fk'){
						if(!empty($frm["tipo2_".$campo])){
							$tb2=$frm["tipo2_".$campo];							
							if(!empty($frm["tipofkid_".$campo])&&!empty($frm["tipofkver_".$campo])){
								$ifk++;
								$aliastb2='tb'.$ifk;
								$fkid=$frm["tipofkid_".$campo];
								$fkver=$frm["tipofkver_".$campo];
								$inner.=' LEFT JOIN '.$tb2.' '.$aliastb2.' ON '.$aliastb.'.'.$campo.'='.$aliastb2.'.'.$fkid.' ';
								$camposfk.=','.$aliastb2.'.'.$fkver.' AS _'.$fkver.' ';
								$nuevalista.='';
							}
						}

					}

					if($campopk!=$campo){
							$strcampos.=",$".$campo;
							$x.= "
							,'".$campo."'=>$".$campo."";
					}
	}
		$strcampos=substr($strcampos, 1);
	?>
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		<?php echo date("d-m-Y") ?>  
  * @copyright	Copyright (C) <?php echo date("Y") ?>. Todos los derechos reservados.
 */ 
class Dat<?php echo  $tb ?> extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM <?php echo $this->tablaActiva; ?>";
			
			$cond = array();		
			<?php echo $filtros; ?>
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM <?php echo $this->tablaActiva; ?>";			
			
			$cond = array();		
					
			<?php echo $filtros; ?>
			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	
	
	public function insertar(<?php echo $strcampos ?>)
	{
		try {
			
			$this->iniciarTransaccion('dat_<?php echo $this->tabla; ?>_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(<?php echo $this->pk ?>) FROM <?php echo $this->tablaActiva; ?>");
			++$id;
			
			$estados = array('<?php echo $this->pk ?>' => $id
							<?php echo $x; ?>
							
							);
			
			$this->oBD->insert('<?php echo $this->tablaActiva; ?>', $estados);			
			$this->terminarTransaccion('dat_<?php echo $this->tabla; ?>_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_<?php echo $this->tabla; ?>_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	public function actualizar($id, <?php echo $strcampos ?>)
	{
		try {
			$this->iniciarTransaccion('dat_<?php echo $this->tabla; ?>_update');
			$estados = array(<?php echo substr(trim($x),1);?>								
							);
			
			$this->oBD->update('<?php echo $this->tablaActiva; ?> ', $estados, array('<?php echo $this->pk ?>' => $id));
		    $this->terminarTransaccion('dat_<?php echo $this->tabla; ?>_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT <?php echo (!empty($inner)?($aliastb.'.*'.$camposfk):' * ').' FROM '.$this->tablaActiva.' '.(!empty($inner)?($aliastb.$inner):''); ?> "
					. " WHERE <?php echo (!empty($inner)?($aliastb.'.'):'').$this->pk ?> = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('<?php echo $this->tablaActiva; ?>', array('<?php echo $this->pk ?>' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('<?php echo $this->tablaActiva; ?>', array($propiedad => $valor), array('<?php echo $this->pk ?>' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("<?php echo $tb; ?>").": " . $e->getMessage());
		}
	}
   
	<?php echo $nuevalista; ?>
	
}