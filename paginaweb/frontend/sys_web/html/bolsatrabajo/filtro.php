<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?>
<div class="container-fluid pnlbusquedas">
	<div class="row">
		<div class="container">
			<form method="post" id="frmbuscar-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left">
			<div class="row" style="padding: 1.5em 0px">								
				<div class="col-12 col-sm-12 col-md-12 form-group">
	              <label style="    font-size: 1.5em;  color: #fff; font-weight: bold;"><?php echo JrTexto::_('Busca nuevas ofertas laborales');?></label>              
	              <div class="input-group">
				      <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Puesto, empresa o palabra clave"))?>">
				      <div class="input-group-append"><button class="btn btnbuscar<?php echo $idgui; ?> btn-primary"><?php echo  ucfirst(JrTexto::_("Buscar"))?> <i class="fa fa-search"></i></button></div>				     
				    </div>
	            </div>
			</div>
		</form>
		</div>
	</div>
</div>
<div class="container" id="pnlresult<?php echo $idgui; ?>">
		
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var cargarresul<?php echo $idgui; ?>=function(){
			var tmpfrm=document.getElementById('frmbuscar-<?php echo $idgui;?>');	
			var formData = new FormData(tmpfrm);			
			formData.append('idioma',_sysIdioma_);
			formData.append('plt','blanco'); 
			var _sysajax={
				fromdata:formData,
				url:_sysUrlBase_+'/defecto/verlistado',
				type:'html',
				callback:function(html){
					$('#pnlresult<?php echo $idgui; ?>').html(html);
				}
			}
			sysajax(_sysajax);
			return false;	
		}
		$('#frmbuscar-<?php echo $idgui;?>').submit(function(ev){
			cargarresul<?php echo $idgui; ?>();
			ev.preventDefault();
			return false;
		});
		cargarresul<?php echo $idgui; ?>();
	});
</script>