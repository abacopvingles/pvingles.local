<?php defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pageall.min.css" rel="stylesheet">   
    <link href="<?php echo $documento->getUrlStatic()?>/libs/pnotify/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script>
            var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>';
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';
            var _sysIdioma_ = '<?php echo $documento->getIdioma()?>';
    </script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/pageall.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/pnotify/pnotify.min.js"></script>   
    <jrdoc:incluir tipo="cabecera" />
</head>
<body style="background-color:#ddd;" >
<jrdoc:incluir tipo="modulo" nombre="preload" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
    <jrdoc:incluir tipo="mensaje" />
    <jrdoc:incluir tipo="recurso" />
<jrdoc:incluir tipo="modulo" nombre="pie" posicion="man"/></jrdoc:incluir>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<jrdoc:incluir tipo="docsJs" />
</body>
</html>