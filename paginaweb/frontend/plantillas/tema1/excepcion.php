<?php defined('_BOOT_') or die(''); $verion=!empty(_version_)?_version_:'1.0';?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->    
    <script src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
             var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
             var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>   
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/libs.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/libs/pnotify/pnotify.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/pageall.min.js"></script>
</head>
<body >
<div class="">
    <!--jrdoc:incluir tipo="mensaje" /-->
    <jrdoc:incluir tipo="recurso" />    
</div>

</body>
</html>