-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-01-2018 a las 16:18:50
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bolsa_publicaciones`
--

CREATE TABLE `bolsa_publicaciones` (
  `idpublicacion` int(11) NOT NULL,
  `idempresa` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `sueldo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `nvacantes` smallint(4) NOT NULL,
  `disponibilidadeviaje` int(11) NOT NULL,
  `duracioncontrato` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `xtiempo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecharegistro` date NOT NULL,
  `fechapublicacion` datetime NOT NULL,
  `cambioderesidencia` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `bolsa_publicaciones`
--

INSERT INTO `bolsa_publicaciones` (`idpublicacion`, `idempresa`, `titulo`, `descripcion`, `sueldo`, `nvacantes`, `disponibilidadeviaje`, `duracioncontrato`, `xtiempo`, `fecharegistro`, `fechapublicacion`, `cambioderesidencia`, `mostrar`) VALUES
(1, 1, 'Necesito ing. de sistemas', 'Que desarrolle apps para celulares\r\nprogramar en múltiples lenguajes\r\nque sepa volar y nadar\r\n', '6000', 1, 1, '13M', 'TC', '2018-01-25', '2018-01-25 15:00:00', 1, 1),
(2, 1, 'Necesito ing. de sistemas', 'Que desarrolle apps para celulares\r\nprogramar en múltiples lenguajes\r\nque sepa volar y nadar\r\n', '6000', 1, 1, '13M', 'TC', '2018-01-25', '2018-01-25 10:00:00', 1, 1),
(3, 1, 'Necesito ing. de sistemas', 'Que desarrolle apps para celulares\r\nprogramar en múltiples lenguajes\r\nque sepa volar y nadar\r\n', '6000', 1, 1, '13M', 'TC', '2018-01-25', '2018-01-25 11:00:00', 1, 1),
(4, 1, 'sdasd', 'asdasdasd', '123123', 1, 1, '1A', 'TC', '2018-01-25', '2018-01-25 11:00:00', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bolsa_publicaciones`
--
ALTER TABLE `bolsa_publicaciones`
  ADD PRIMARY KEY (`idpublicacion`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
