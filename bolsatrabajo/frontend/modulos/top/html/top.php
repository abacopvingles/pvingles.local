<?php
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
@session_start();
$islogin=!empty($_SESSION["loginempresa"])?true:false;
?>
<?php if(!empty($this->plt)){?><div class="container-fluid pnlbusquedas2"><div class="row"><div class="col-12"><?php } ?>
<div class="container btnmenuactive no-print">	
	<div class="row">
		<div class="col "> 
		<nav class="text-right" style="padding: 1ex;">
      <a class="btn hvr-float active <?php echo !empty($this->plt)?'border-white btn-primary':'btn-success'; ?> " href="<?php echo $this->documento->getUrlBase(); ?>" style="float:left"><i class="fa fa-home"></i> <?php echo JrTexto::_('Publicaciones') ?></a>
        <!--a class="text-dark" href="#"><?php echo JrTexto::_('') ?></a-->
        <a class="btn hvr-float active <?php echo !empty($this->plt)?'border-white btn-warning':'border-warning btn-default'; ?>  btnpublicaraviso<?php echo $idgui;  ?>" data-modal="no" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/publicar"><?php echo JrTexto::_('Publicar Aviso') ?></a>
        <?php if(!$islogin){?>
        <a class="btn hvr-float <?php echo !empty($this->plt)?'border-white btn-success':'border-success btn-default'; ?> venloginopen border-success btn-default btninciarsesion<?php echo $idgui;?>" href="<?php echo $this->documento->getUrlBase(); ?>/sesion/formloginempresa"><?php echo JrTexto::_('Iniciar Sesion') ?></a>
        <a class="btn hvr-float <?php echo !empty($this->plt)?'border-white':''; ?> btn-primary btnregistrarme<?php echo $idgui;?>" data-title="<?php echo JrTexto::_('Registrame') ?>" data-modal="no" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/crearempresa"><?php echo JrTexto::_('Registrarme') ?></a>
        <?php } else { ?>
          <a class="btn hvr-float <?php echo !empty($this->plt)?'border-white btn-success':'border-success btn-default'; ?>  btnmispublicaciones<?php echo $idgui;?>" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/mispublicaciones"><?php echo JrTexto::_('Mis publicaciones') ?></a>
          <a class="btn hvr-float <?php echo !empty($this->plt)?'border-white btn-info':'border-info btn-default'; ?> btnmispublicaciones<?php echo $idgui;?>" href="<?php echo $this->documento->getUrlBase(); ?>/defecto/crearempresa"><?php echo JrTexto::_('Mi empresa') ?></a>
          <a class="btn hvr-float <?php echo !empty($this->plt)?'border-white ':''; ?>btn-primary  btnsalir<?php echo $idgui;?>" data-modal="no" href="javascript:void(0)"><?php echo JrTexto::_('Cerrar Sesion') ?></a>
        <?php } ?>
      </nav>
    </div>
	</div>	
</div>
<?php if(!empty($this->plt)){?></div></div></div><?php } ?>

<script type="text/javascript">  
  var _openmodal<?php echo $idgui;?>=function(info){
    var info=info||{};
    var ismodal=(info.ismodal=='no'||info.ismodal==false)?false:true;
    var url=info.url||_sysUrlBase_;  
    if(!ismodal){
      redir(url);
      return;
    }else{
      var titulo=info.titulo||'';      
      var tam=info.tam||'lg';
      var frmdata=info.frmdata||{};
      var claseid=info.claseid||'cls'+Date.now();
      frmdata.plt='modal';
      frmdata.idioma=_sysIdioma_;
      openModal(tam,titulo,url,true,claseid,{header:false,footer:false,borrarmodal:true,datasend:frmdata});
    }
    return false;
  }
  
  var _sysopenmodal_=function(info){
    _openmodal<?php echo $idgui;?>(info);
  }

  function veravisologin(){
    var info={url:_sysUrlBase_+'/defecto/avisologueate', ismodal:true, titulo:'' }
    _openmodal<?php echo $idgui;?>(info);
  }
  $(document).ready(function(ev){
    var islogin=<?php echo !empty($islogin)?'true':'false'; ?>;
    

    $('.btninciarsesion<?php echo $idgui;?>').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      var info={url:$(this).attr('href'), ismodal:true, titulo:$(this).attr('data-title'), tam:'md'}
      _openmodal<?php echo $idgui;?>(info);
    })
    $('.btnpublicaraviso<?php echo $idgui;?>').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      if(islogin==false) {
        veravisologin();
        return false;
      }
      var info={url:$(this).attr('href'), ismodal:false, titulo:$(this).attr('data-title') }
      _openmodal<?php echo $idgui;?>(info);

    })
    $('.btnregistrarme<?php echo $idgui;?>').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
     var info={url:$(this).attr('href'), ismodal:false, titulo:$(this).attr('data-title') }
      _openmodal<?php echo $idgui;?>(info);
    })

    $('.btnmispublicaciones<?php echo $idgui;?>').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
     var info={url:$(this).attr('href'), ismodal:false, titulo:$(this).attr('data-title') }
      _openmodal<?php echo $idgui;?>(info);
    })

    $('.btnsalir<?php echo $idgui;?>').on('click',function(ev){
      ev.preventDefault();
      ev.stopPropagation();
      var _sysajax={
          fromdata:'',
         // showmsjok:true,
          url:_sysUrlBase_+'/sesion/salirempresa',
          callback:function(rs){                   
              setTimeout(function(){redir(_sysUrlBase_)},500);
              return false;
          }
      }
      sysajax(_sysajax);     
    })  
  })
</script>
