<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		25-01-2018
 * @copyright	Copyright (C) 25-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBolsa_publicaciones', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBolsa_publicaciones 
{
	protected $idpublicacion;
	protected $idempresa;
	protected $titulo;
	protected $descripcion;
	protected $sueldo;
	protected $nvacantes;
	protected $disponibilidadeviaje;
	protected $duracioncontrato;
	protected $xtiempo;
	protected $fecharegistro;
	protected $fechapublicacion;
	protected $cambioderesidencia;
	protected $mostrar;
	
	protected $dataBolsa_publicaciones;
	protected $oDatBolsa_publicaciones;	

	public function __construct()
	{
		$this->oDatBolsa_publicaciones = new DatBolsa_publicaciones;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBolsa_publicaciones->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBolsa_publicaciones->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBolsa_publicaciones->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBolsa_publicaciones->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBolsa_publicaciones->get($this->idpublicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_publicaciones', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBolsa_publicaciones->iniciarTransaccion('neg_i_Bolsa_publicaciones');
			$this->idpublicacion = $this->oDatBolsa_publicaciones->insertar($this->idempresa,$this->titulo,$this->descripcion,$this->sueldo,$this->nvacantes,$this->disponibilidadeviaje,$this->duracioncontrato,$this->xtiempo,$this->fecharegistro,$this->fechapublicacion,$this->cambioderesidencia,$this->mostrar);
			$this->oDatBolsa_publicaciones->terminarTransaccion('neg_i_Bolsa_publicaciones');	
			return $this->idpublicacion;
		} catch(Exception $e) {	
		    $this->oDatBolsa_publicaciones->cancelarTransaccion('neg_i_Bolsa_publicaciones');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_publicaciones', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBolsa_publicaciones->actualizar($this->idpublicacion,$this->idempresa,$this->titulo,$this->descripcion,$this->sueldo,$this->nvacantes,$this->disponibilidadeviaje,$this->duracioncontrato,$this->xtiempo,$this->fecharegistro,$this->fechapublicacion,$this->cambioderesidencia,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatBolsa_publicaciones->cambiarvalorcampo($this->idpublicacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bolsa_publicaciones', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBolsa_publicaciones->eliminar($this->idpublicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpublicacion($pk){
		try {
			$this->dataBolsa_publicaciones = $this->oDatBolsa_publicaciones->get($pk);
			if(empty($this->dataBolsa_publicaciones)) {
				throw new Exception(JrTexto::_("Bolsa_publicaciones").' '.JrTexto::_("not registered"));
			}
			$this->idpublicacion = $this->dataBolsa_publicaciones["idpublicacion"];
			$this->idempresa = $this->dataBolsa_publicaciones["idempresa"];
			$this->titulo = $this->dataBolsa_publicaciones["titulo"];
			$this->descripcion = $this->dataBolsa_publicaciones["descripcion"];
			$this->sueldo = $this->dataBolsa_publicaciones["sueldo"];
			$this->nvacantes = $this->dataBolsa_publicaciones["nvacantes"];
			$this->disponibilidadeviaje = $this->dataBolsa_publicaciones["disponibilidadeviaje"];
			$this->duracioncontrato = $this->dataBolsa_publicaciones["duracioncontrato"];
			$this->xtiempo = $this->dataBolsa_publicaciones["xtiempo"];
			$this->fecharegistro = $this->dataBolsa_publicaciones["fecharegistro"];
			$this->fechapublicacion = $this->dataBolsa_publicaciones["fechapublicacion"];
			$this->cambioderesidencia = $this->dataBolsa_publicaciones["cambioderesidencia"];
			$this->mostrar = $this->dataBolsa_publicaciones["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bolsa_publicaciones', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBolsa_publicaciones = $this->oDatBolsa_publicaciones->get($pk);
			if(empty($this->dataBolsa_publicaciones)) {
				throw new Exception(JrTexto::_("Bolsa_publicaciones").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBolsa_publicaciones->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}