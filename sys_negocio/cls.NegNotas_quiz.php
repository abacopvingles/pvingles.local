<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-09-2018
 * @copyright	Copyright (C) 16-09-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_quiz', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class NegNotas_quiz 
{
	protected $idnota;
	protected $idcursodetalle;
	protected $idrecurso;
	protected $idalumno;
	protected $tipo;
	protected $nota;
	protected $notatexto;
	protected $regfecha;
	protected $idproyecto;
	protected $calificacion_en;
	protected $calificacion_total;
	protected $calificacion_min;
	protected $tiempo_total;
	protected $tiempo_realizado;
	protected $calificacion;
	protected $habilidades;
	protected $habilidad_puntaje;
	protected $intento;
	protected $idexamenalumnoquiz;
	protected $idexamenproyecto;
	protected $idexamenidalumno;
	protected $datos;
	protected $preguntas;
	protected $preguntasoffline;
	
	protected $dataNotas_quiz;
	protected $oDatNotas_quiz;
	protected $oNegAcad_cursodetalle;

	public function __construct()
	{
		$this->oDatNotas_quiz = new DatNotas_quiz;
		$this->oNegAcad_cursodetalle = new NegAcad_cursodetalle;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas_quiz->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function buscarNotas($filtros = null){
		try {
			$resultado = array();
			$this->setLimite(0,100000);
			$result = $this->oDatNotas_quiz->buscarNotas($filtros);
			if(!empty($result)){
				$letra = 'B';
				$isTri = false;
				$cantidad = 5;
				if(isset($filtros["trimestre"])){
					if($filtros["trimestre"] == true){
						$letra = 'T';
						$isTri = true;
						$cantidad = 4;
					}
				}
				foreach($result as $key => $value){
					$value['examen_'.$letra.'1'] = (!empty($value['examen_'.$letra.'1'])) ? intval(floor($value['examen_'.$letra.'1'] * 0.20)) : 0;
					$value['examen_'.$letra.'2'] = (!empty($value['examen_'.$letra.'2'])) ? intval(floor($value['examen_'.$letra.'2'] * 0.20)) : 0;
					$value['examen_'.$letra.'3'] = (!empty($value['examen_'.$letra.'3'])) ? intval(floor($value['examen_'.$letra.'3'] * 0.20)) : 0;
					if(!$isTri){
						$value['examen_'.$letra.'4'] = (!empty($value['examen_'.$letra.'4'])) ? intval(floor($value['examen_'.$letra.'4'] * 0.20)) : 0;
					}
					$value['examen_salida'] = (!empty($value['examen_salida'])) ? intval(floor($value['examen_salida'] * 0.20)) : 0;
					$prom = ($isTri == false) ? $value['examen_'.$letra.'1'] + $value['examen_'.$letra.'2'] + $value['examen_'.$letra.'3'] + $value['examen_'.$letra.'4'] + $value['examen_salida'] : $value['examen_'.$letra.'1'] + $value['examen_'.$letra.'2'] + $value['examen_'.$letra.'3'] + $value['examen_salida'];
					$value['promedio_'.$letra] = ($prom != 0) ? intval(floor($prom/$cantidad)) : 0; 
					$resultado[] = $value;
				}
			}

			return $resultado;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarNotasUnidad($filtros,$idcurso){
		try{
			$resultado = $this->oDatNotas_quiz->matriculacustom($filtros);
			$sesiones = $this->oNegAcad_cursodetalle->sesiones($idcurso,0);
			// // var_dump($sesiones[1]);
			if(!empty($resultado)){
				$indice = -1;
				foreach($sesiones as $key => $value){
					if($value['tiporecurso'] == 'U'){
						++$indice;
						$i = 0; $c = count($resultado);
						do{
							$resultado[$i]['notasunidad'][] = array('nombre'=>$value['nombre'], 'nota' => 0) ;

							++$i;
						}while($i < $c);					
						if(isset($value['hijo'])){
							foreach($value['hijo'] as $hijovalue){
								if($hijovalue['tiporecurso'] == 'E'){
									$i = 0; $c= count($resultado);
									do{
										$_nota = $this->oDatNotas_quiz->buscar(array('idrecurso'=>$hijovalue['idrecurso'],'idalumno'=>$resultado[$i]['idalumno'],'tipo'=>'E'));
										$resultado[$i]['notasunidad'][$indice]['nota']= (empty($_nota)) ? 0 : ($_nota[0]['nota'] * 0.20) ;
										++$i;
									}while($i < $c);
								}
							}//end foreach hijo
						}//end if isset hijo
					}//end if tiporecurso
				}//end foreach sesiones
			}//end if empty resultado
			return $resultado;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_quiz->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar2($filtros = array())
	{
		try {
			$datos=$this->oDatNotas_quiz->buscar($filtros);
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 
			$datos1=array();
			foreach ($datos as $dt){				
				$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
				$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
				$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $dt["nombre"] = $dataExam['data']['titulo'];
                        $dt["imagen"] = $dataExam['data']['portada'];
                    }
				$datos1[]=$dt;
			}
			return $datos1;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas_quiz->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_quiz->get($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			
			$this->oDatNotas_quiz->iniciarTransaccion('neg_i_Notas_quiz');
			$this->idnota = $this->oDatNotas_quiz->insertar($this->idcursodetalle,$this->idrecurso,$this->idalumno,$this->tipo,$this->nota,$this->notatexto,$this->regfecha,$this->idproyecto,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_total,$this->tiempo_realizado,$this->calificacion,$this->habilidades,$this->habilidad_puntaje,$this->intento,$this->idexamenalumnoquiz,$this->idexamenproyecto,$this->idexamenidalumno,$this->datos,$this->preguntas,$this->preguntasoffline);
			$this->oDatNotas_quiz->terminarTransaccion('neg_i_Notas_quiz');	
			return $this->idnota;
		} catch(Exception $e) {	
		    $this->oDatNotas_quiz->cancelarTransaccion('neg_i_Notas_quiz');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas_quiz->actualizar($this->idnota,$this->idcursodetalle,$this->idrecurso,$this->idalumno,$this->tipo,$this->nota,$this->notatexto,$this->regfecha,$this->idproyecto,$this->calificacion_en,$this->calificacion_total,$this->calificacion_min,$this->tiempo_total,$this->tiempo_realizado,$this->calificacion,$this->habilidades,$this->habilidad_puntaje,$this->intento,$this->idexamenalumnoquiz,$this->idexamenproyecto,$this->idexamenidalumno,$this->datos,$this->preguntas,$this->preguntasoffline);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_quiz', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_quiz->eliminar($this->idnota);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdnota($pk){
		try {
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if(empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz").' '.JrTexto::_("not registered"));
			}
			$this->idnota = $this->dataNotas_quiz["idnota"];
			$this->idcursodetalle = $this->dataNotas_quiz["idcursodetalle"];
			$this->idrecurso = $this->dataNotas_quiz["idrecurso"];
			$this->idalumno = $this->dataNotas_quiz["idalumno"];
			$this->tipo = $this->dataNotas_quiz["tipo"];
			$this->nota = $this->dataNotas_quiz["nota"];
			$this->notatexto = $this->dataNotas_quiz["notatexto"];
			$this->regfecha = $this->dataNotas_quiz["regfecha"];
			$this->idproyecto = $this->dataNotas_quiz["idproyecto"];
			$this->calificacion_en = $this->dataNotas_quiz["calificacion_en"];
			$this->calificacion_total = $this->dataNotas_quiz["calificacion_total"];
			$this->calificacion_min = $this->dataNotas_quiz["calificacion_min"];
			$this->tiempo_total = $this->dataNotas_quiz["tiempo_total"];
			$this->tiempo_realizado = $this->dataNotas_quiz["tiempo_realizado"];
			$this->calificacion = $this->dataNotas_quiz["calificacion"];
			$this->habilidades = $this->dataNotas_quiz["habilidades"];
			$this->habilidad_puntaje = $this->dataNotas_quiz["habilidad_puntaje"];
			$this->intento = $this->dataNotas_quiz["intento"];
			$this->idexamenalumnoquiz = $this->dataNotas_quiz["idexamenalumnoquiz"];
			$this->idexamenproyecto = $this->dataNotas_quiz["idexamenproyecto"];
			$this->idexamenidalumno = $this->dataNotas_quiz["idexamenidalumno"];
			$this->datos = $this->dataNotas_quiz['datos'];
			$this->preguntas = $this->dataNotas_quiz['preguntas'];
			$this->preguntasoffline = $this->dataNotas_quiz['preguntasoffline'];
			//falta campos
			return true;
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
			return false;
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas_quiz', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_quiz = $this->oDatNotas_quiz->get($pk);
			if(empty($this->dataNotas_quiz)) {
				throw new Exception(JrTexto::_("Notas_quiz").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas_quiz->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}