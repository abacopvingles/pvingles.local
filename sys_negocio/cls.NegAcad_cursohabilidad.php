<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-08-2018
 * @copyright	Copyright (C) 03-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursohabilidad', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_cursohabilidad 
{
	protected $idcursohabilidad;
	protected $texto;
	protected $tipo;
	protected $idcurso;
	protected $idcursodetalle;
	protected $idpadre;
	
	protected $dataAcad_cursohabilidad;
	protected $oDatAcad_cursohabilidad;	

	public function __construct()
	{
		$this->oDatAcad_cursohabilidad = new DatAcad_cursohabilidad;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_cursohabilidad->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursohabilidad->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_cursohabilidad->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_cursohabilidad->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursohabilidad->get($this->idcursohabilidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidad', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_cursohabilidad->iniciarTransaccion('neg_i_Acad_cursohabilidad');
			$this->idcursohabilidad = $this->oDatAcad_cursohabilidad->insertar($this->texto,$this->tipo,$this->idcurso,$this->idcursodetalle,$this->idpadre);
			$this->oDatAcad_cursohabilidad->terminarTransaccion('neg_i_Acad_cursohabilidad');	
			return $this->idcursohabilidad;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursohabilidad->cancelarTransaccion('neg_i_Acad_cursohabilidad');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidad', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_cursohabilidad->actualizar($this->idcursohabilidad,$this->texto,$this->tipo,$this->idcurso,$this->idcursodetalle,$this->idpadre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_cursohabilidad', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_cursohabilidad->eliminar($this->idcursohabilidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdcursohabilidad($pk){
		try {
			$this->dataAcad_cursohabilidad = $this->oDatAcad_cursohabilidad->get($pk);
			if(empty($this->dataAcad_cursohabilidad)) {
				throw new Exception(JrTexto::_("Acad_cursohabilidad").' '.JrTexto::_("not registered"));
			}
			$this->idcursohabilidad = $this->dataAcad_cursohabilidad["idcursohabilidad"];
			$this->texto = $this->dataAcad_cursohabilidad["texto"];
			$this->tipo = $this->dataAcad_cursohabilidad["tipo"];
			$this->idcurso = $this->dataAcad_cursohabilidad["idcurso"];
			$this->idcursodetalle = $this->dataAcad_cursohabilidad["idcursodetalle"];
			$this->idpadre = $this->dataAcad_cursohabilidad["idpadre"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_cursohabilidad', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_cursohabilidad = $this->oDatAcad_cursohabilidad->get($pk);
			if(empty($this->dataAcad_cursohabilidad)) {
				throw new Exception(JrTexto::_("Acad_cursohabilidad").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_cursohabilidad->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}