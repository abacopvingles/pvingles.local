<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-12-2018
 * @copyright	Copyright (C) 12-12-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_licencias', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatPersona_rol', RUTA_BASE,'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_licencias 
{
	protected $idlicencia;
	protected $idempresa;
	protected $costoxrol;
	protected $costoxdescarga;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $estado;
	
	protected $dataAcad_licencias;
	protected $oDatAcad_licencias;
	protected $oDatPersona_rol;	

	public function __construct()
	{
		$this->oDatAcad_licencias = new DatAcad_licencias;
		$this->oDatPersona_rol = new DatPersona_rol;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_licencias->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////
	private function compare($a,$b){
		return strcmp($a['idpersonal'],$b['idpersonal']);
	}
	public function licenciasEmpresas(){
		try{
			$result = array();
			$this->setLimite(0,10000);
			$asignado = $this->oDatAcad_licencias->buscar();
			
			if(!empty($asignado)){
				$i = 0; $c = count($asignado);
				do{
					$usado = $this->oDatPersona_rol->buscarcantidad(array('idempresa'=>$asignado[$i]['idempresa']));
					$used = array_fill(1,10,0);
					if(!empty($usado)){
						//$idnow = 0; $idrolnow = 0;
						foreach($usado as $v){
							//$_idrol = intval($v['idrol']);
							++$used[intval($v['idrol'])];
						}
					}
					$result[] = array('empresa'=>$asignado[$i]['empresa'],'assignado'=>json_decode($asignado[$i]['costoxrol'],true),'usado'=>$used);
					$i++;
				}while($i < $c);
			}
			return $result;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_licencias->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAcad_licencias->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_licencias->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_licencias->get($this->idlicencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_licencias', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_licencias->iniciarTransaccion('neg_i_Acad_licencias');
			$this->idlicencia = $this->oDatAcad_licencias->insertar($this->idempresa,$this->costoxrol,$this->costoxdescarga,$this->fecha_inicio,$this->fecha_final,$this->estado);
			$this->oDatAcad_licencias->terminarTransaccion('neg_i_Acad_licencias');	
			return $this->idlicencia;
		} catch(Exception $e) {	
		    $this->oDatAcad_licencias->cancelarTransaccion('neg_i_Acad_licencias');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_licencias', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_licencias->actualizar($this->idlicencia,$this->idempresa,$this->costoxrol,$this->costoxdescarga,$this->fecha_inicio,$this->fecha_final,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_licencias', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAcad_licencias->eliminar($this->idlicencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdlicencia($pk){
		try {
			$this->dataAcad_licencias = $this->oDatAcad_licencias->get($pk);
			if(empty($this->dataAcad_licencias)) {
				throw new Exception(JrTexto::_("Acad_licencias").' '.JrTexto::_("not registered"));
			}
			$this->idlicencia = $this->dataAcad_licencias["idlicencia"];
			$this->idempresa = $this->dataAcad_licencias["idempresa"];
			$this->costoxrol = $this->dataAcad_licencias["costoxrol"];
			$this->costoxdescarga = $this->dataAcad_licencias["costoxdescarga"];
			$this->fecha_inicio = $this->dataAcad_licencias["fecha_inicio"];
			$this->fecha_final = $this->dataAcad_licencias["fecha_final"];
			$this->estado = $this->dataAcad_licencias["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_licencias', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_licencias = $this->oDatAcad_licencias->get($pk);
			if(empty($this->dataAcad_licencias)) {
				throw new Exception(JrTexto::_("Acad_licencias").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_licencias->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}