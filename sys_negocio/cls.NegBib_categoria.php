<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_categoria', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_categoria 
{
	protected $id_categoria;
	protected $descripcion;
	
	protected $dataBib_categoria;
	protected $oDatBib_categoria;	

	public function __construct()
	{
		$this->oDatBib_categoria = new DatBib_categoria;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_categoria->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_categoria->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_categoria->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_categoria->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_categoria->get($this->id_categoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_categoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_categoria->iniciarTransaccion('neg_i_Bib_categoria');
			$this->id_categoria = $this->oDatBib_categoria->insertar($this->descripcion);
			//$this->oDatBib_categoria->terminarTransaccion('neg_i_Bib_categoria');	
			return $this->id_categoria;
		} catch(Exception $e) {	
		   //$this->oDatBib_categoria->cancelarTransaccion('neg_i_Bib_categoria');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_categoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBib_categoria->actualizar($this->id_categoria,$this->descripcion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_categoria', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_categoria->eliminar($this->id_categoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_categoria($pk){
		try {
			$this->dataBib_categoria = $this->oDatBib_categoria->get($pk);
			if(empty($this->dataBib_categoria)) {
				throw new Exception(JrTexto::_("Bib_categoria").' '.JrTexto::_("not registered"));
			}
			$this->id_categoria = $this->dataBib_categoria["id_categoria"];
			$this->descripcion = $this->dataBib_categoria["descripcion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_categoria', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_categoria = $this->oDatBib_categoria->get($pk);
			if(empty($this->dataBib_categoria)) {
				throw new Exception(JrTexto::_("Bib_categoria").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_categoria->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}