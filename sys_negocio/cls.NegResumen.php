<?php
set_time_limit(0);
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatResumen', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatUbigeo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatMin_dre', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatUgel', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatLocal', RUTA_BASE, 'sys_datos');

class NegResumen{
	protected $oDatResumen;
	protected $oDatUbigeo;
	protected $oDatMin_dre;
	protected $oDatUgel;
	protected $oDatLocal;

    public function __construct()
	{
		$this->oDatResumen = new DatResumen;
		$this->oDatUbigeo = new DatUbigeo;
		$this->oDatMin_dre = new DatMin_dre;
		$this->oDatUgel = new DatUgel;
		$this->oDatLocal = new DatLocal;
    }
    public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
    }
    public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
    }
    
    private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
    }
    public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatResumen->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function busqueda_filtrada($campos,$filtros){
		try{
			$result = array();
			if(!empty($campos)){
				$arr_camp = array();
                if(!empty($campos['general'])){
					foreach($campos['general'] as $k => $v){
						$campos['general'][$k] = 'rs.'.$v; 
					}
                    $arr_camp = array_merge($arr_camp,$campos['general']);
                }
                if(!empty($campos['habilidad'])){
					foreach($campos['habilidad'] as $k => $v){
						$campos['habilidad'][$k] = 'rsh.'.$v; 
					}
                    $arr_camp = array_merge($arr_camp,$campos['habilidad']);
				}
				if(!empty($arr_camp)){
					$filtros['filtro'] = $arr_camp;
				}
				$this->oDatResumen->setLimite(0,100000);
				$result = $this->oDatResumen->filtrar($filtros);
			}
			return $result;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscar($filtros = null){
		try{
			$result = array();
			$this->oDatResumen->setLimite(0,100000);
			if(isset($filtros['busqueda'])){
				if($filtros['busqueda'] === 'H'){
					$result = $this->oDatResumen->buscarhabilidad($filtros);
				}else{
					$result = $this->oDatResumen->buscar($filtros);
				}
			}else{
				$result = $this->oDatResumen->buscar($filtros);
			}
			return $result;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function regiones(){
		try{
			$resultado = array();
			$dre = $this->oDatMin_dre->buscar();
			if(!empty($dre)){
				foreach($dre as $key => $value){
					$prepare['dre'] = $value['descripcion'];
					$prepare['id_ubigeo'] = $value['ubigeo'];
					$prepare['iddre'] = $value['iddre'];
					$prepare['ugeles'] = array();
					$ugels = $this->oDatUgel->buscar(array('iddepartamento'=>$value['ubigeo']));
					if(!empty($ugels)){
						foreach($ugels as $v){
							$prepare['ugeles'][] = $v;
						}
					}
					$resultado[] = $prepare;
				}
			}
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_grado($data){
		try{
			$resultado = array();
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	private function getUbicacionLocal($ex){
		$r = array();
		if(!empty($ex)){
			$prepare = array('total'=>0,'A1' => 0, 'A2' => 0,'B1' => 0,'B2' => 0, 'C1' => 0,'L'=>0,'R'=>0,'W'=>0,'S'=>0);
			$hab_sum = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
			foreach($ex as $value){
				$nota = intval(floor($value['nota']));
				$habilidades = json_decode($value['habilidad_puntaje'],true);
				$Calificaciones = json_decode($value['calificacion_total'],true);
				$prepare['total']++;
				if(!empty($Calificaciones)){
					foreach($Calificaciones as $c){
						if($nota >= intval($c['min']) && $nota <= intval($c['max'])){
							$prepare[$c['nombre']]++;
							break;
						}
					}
				}
				if(!empty($habilidades)){
					$hab_sum['4'] = isset($habilidades['4']) ? ($hab_sum['4'] + $habilidades['4']) :$hab_sum['4'];
					$hab_sum['5'] = isset($habilidades['5']) ? ($hab_sum['5'] + $habilidades['5']) :$hab_sum['5'];
					$hab_sum['6'] = isset($habilidades['6']) ? ($hab_sum['6'] + $habilidades['6']) :$hab_sum['6'];
					$hab_sum['7'] = isset($habilidades['7']) ? ($hab_sum['7'] + $habilidades['7']) :$hab_sum['7'];
				}
			}//end foreach examenes
			$hab_sum['4'] = (!empty($prepare['total']) && !empty($hab_sum['4'])) ? $hab_sum['4'] / $prepare['total'] : $hab_sum['4'];
			$hab_sum['5'] = (!empty($prepare['total']) && !empty($hab_sum['5'])) ? $hab_sum['5'] / $prepare['total'] : $hab_sum['5'];
			$hab_sum['6'] = (!empty($prepare['total']) && !empty($hab_sum['6'])) ? $hab_sum['6'] / $prepare['total'] : $hab_sum['6'];
			$hab_sum['7'] = (!empty($prepare['total']) && !empty($hab_sum['7'])) ? $hab_sum['7'] / $prepare['total'] : $hab_sum['7'];
			
			$prepare['L'] = ($hab_sum['4'] > 100) ? 100 : round($hab_sum['4'],2);
			$prepare['R'] = ($hab_sum['5'] > 100) ? 100 : round($hab_sum['5'],2);
			$prepare['W'] = ($hab_sum['6'] > 100) ? 100 : round($hab_sum['6'],2);
			$prepare['S'] = ($hab_sum['7'] > 100) ? 100 : round($hab_sum['7'],2);

			$r = $prepare;
		}
		return $r;
	}
	public function getResultExamenLocal($ex, $prom){		

		$r = array('promedio' => 0,'cantidad'=>0,'L'=>0,'R'=>0,'W'=>0,'S'=>0);
		$r['promedio'] = (!empty($prom) && isset($prom[0])) ? ($prom[0]['resultado'] * 0.20) : 0;
		$r['cantidad'] = (!empty($prom) && isset($prom[0]) && !is_null($prom[0]['cantidad'])) ? ($prom[0]['cantidad']) : 0;
		if(!empty($ex)){
			$hab_sum = array('4'=>0,'5'=>0,'6'=>0,'7'=>0);
			foreach($ex as $value){
				$habilidades = json_decode($value['habilidad_puntaje'],true);
				if(!empty($habilidades)){
					$hab_sum['4'] = isset($habilidades['4']) ? ($hab_sum['4'] + $habilidades['4']) :$hab_sum['4'];
					$hab_sum['5'] = isset($habilidades['5']) ? ($hab_sum['5'] + $habilidades['5']) :$hab_sum['5'];
					$hab_sum['6'] = isset($habilidades['6']) ? ($hab_sum['6'] + $habilidades['6']) :$hab_sum['6'];
					$hab_sum['7'] = isset($habilidades['7']) ? ($hab_sum['7'] + $habilidades['7']) :$hab_sum['7'];
				}
			}
			
			$hab_sum['4'] = (!empty($r['cantidad']) && !empty($hab_sum['4'])) ? $hab_sum['4'] / $r['cantidad'] : $hab_sum['4'];
			$hab_sum['5'] = (!empty($r['cantidad']) && !empty($hab_sum['5'])) ? $hab_sum['5'] / $r['cantidad'] : $hab_sum['5'];
			$hab_sum['6'] = (!empty($r['cantidad']) && !empty($hab_sum['6'])) ? $hab_sum['6'] / $r['cantidad'] : $hab_sum['6'];
			$hab_sum['7'] = (!empty($r['cantidad']) && !empty($hab_sum['7'])) ? $hab_sum['7'] / $r['cantidad'] : $hab_sum['7'];
			// var_dump($hab_sum);
			$r['L'] = ($hab_sum['4'] > 100) ? 100 : round($hab_sum['4'],2);
			$r['R'] = ($hab_sum['5'] > 100) ? 100 : round($hab_sum['5'],2);
			$r['W'] = ($hab_sum['6'] > 100) ? 100 : round($hab_sum['6'],2);
			$r['S'] = ($hab_sum['7'] > 100) ? 100 : round($hab_sum['7'],2);
		}
		
		return $r;
	}
	private function getResultLocal($r){
		$_r = array();
		if(!empty($r)){
			$keys = array_keys($r[0]);
			foreach($keys as $k){
				$r[0][$k] = (!is_null($r[0][$k])) ? $r[0][$k] : 0;
			}
			$_r = $r[0];
		}
		return $_r;
	}
	public function getResultTiemposLocal($t){
		$r = array();
		if(!empty($t) && isset($t[0])){
			$r['tiempopv'] = (!empty($t[0]['total_tiempopv'])) ? $t[0]['tiempopv'] / $t[0]['total_tiempopv'] : 0;
			$r['tiempoe'] = (!empty($t[0]['total_tiempoe'])) ? $t[0]['tiempoe'] / $t[0]['total_tiempoe'] : 0;
			$r['tiempot'] = (!empty($t[0]['total_tiempot'])) ? $t[0]['tiempot'] / $t[0]['total_tiempot'] : 0;
			$r['tiempos'] = (!empty($t[0]['total_tiempos'])) ? $t[0]['tiempos'] / $t[0]['total_tiempos'] : 0;
			$r['tiempop'] = (!empty($t[0]['total_tiempop'])) ? $t[0]['tiempop'] / $t[0]['total_tiempop'] : 0;
		}
		return $r;
	}
	public function getResultProgresosLocal($p){
		$r = array();
		if(!empty($p) && isset($p[0])){
$prog_curso_A1 = (!is_null($p[0]['prog_curso_A1'])) ? $p[0]['prog_curso_A1'] : 0;
$prog_curso_A2 = (!is_null($p[0]['prog_curso_A2'])) ? $p[0]['prog_curso_A2'] : 0;
$prog_curso_B1 = (!is_null($p[0]['prog_curso_B1'])) ? $p[0]['prog_curso_B1'] : 0;
$prog_curso_B2 = (!is_null($p[0]['prog_curso_B2'])) ? $p[0]['prog_curso_B2'] : 0;
$prog_curso_C1 = (!is_null($p[0]['prog_curso_C1'])) ? $p[0]['prog_curso_C1'] : 0;
$prog_hab_L_A1 = (!is_null($p[0]['prog_hab_L_A1'])) ? $p[0]['prog_hab_L_A1'] : 0;
$prog_hab_R_A1 = (!is_null($p[0]['prog_hab_R_A1'])) ? $p[0]['prog_hab_R_A1'] : 0;
$prog_hab_W_A1 = (!is_null($p[0]['prog_hab_W_A1'])) ? $p[0]['prog_hab_W_A1'] : 0;
$prog_hab_S_A1 = (!is_null($p[0]['prog_hab_S_A1'])) ? $p[0]['prog_hab_S_A1'] : 0;
$prog_hab_L_A2 = (!is_null($p[0]['prog_hab_L_A2'])) ? $p[0]['prog_hab_L_A2'] : 0;
$prog_hab_R_A2 = (!is_null($p[0]['prog_hab_R_A2'])) ? $p[0]['prog_hab_R_A2'] : 0;
$prog_hab_W_A2 = (!is_null($p[0]['prog_hab_W_A2'])) ? $p[0]['prog_hab_W_A2'] : 0;
$prog_hab_S_A2 = (!is_null($p[0]['prog_hab_S_A2'])) ? $p[0]['prog_hab_S_A2'] : 0;
$prog_hab_L_B1 = (!is_null($p[0]['prog_hab_L_B1'])) ? $p[0]['prog_hab_L_B1'] : 0;
$prog_hab_R_B1 = (!is_null($p[0]['prog_hab_R_B1'])) ? $p[0]['prog_hab_R_B1'] : 0;
$prog_hab_W_B1 = (!is_null($p[0]['prog_hab_W_B1'])) ? $p[0]['prog_hab_W_B1'] : 0;
$prog_hab_S_B1 = (!is_null($p[0]['prog_hab_S_B1'])) ? $p[0]['prog_hab_S_B1'] : 0;
$prog_hab_L_B2 = (!is_null($p[0]['prog_hab_L_B2'])) ? $p[0]['prog_hab_L_B2'] : 0;
$prog_hab_R_B2 = (!is_null($p[0]['prog_hab_R_B2'])) ? $p[0]['prog_hab_R_B2'] : 0;
$prog_hab_W_B2 = (!is_null($p[0]['prog_hab_W_B2'])) ? $p[0]['prog_hab_W_B2'] : 0;
$prog_hab_S_B2 = (!is_null($p[0]['prog_hab_S_B2'])) ? $p[0]['prog_hab_S_B2'] : 0;
$prog_hab_L_C1 = (!is_null($p[0]['prog_hab_L_C1'])) ? $p[0]['prog_hab_L_C1'] : 0;
$prog_hab_R_C1 = (!is_null($p[0]['prog_hab_R_C1'])) ? $p[0]['prog_hab_R_C1'] : 0;
$prog_hab_W_C1 = (!is_null($p[0]['prog_hab_W_C1'])) ? $p[0]['prog_hab_W_C1'] : 0;
$prog_hab_S_C1 = (!is_null($p[0]['prog_hab_S_C1'])) ? $p[0]['prog_hab_S_C1'] : 0;
			$r = array(
				'prog_A1' => $prog_curso_A1,
				'prog_A2' => $prog_curso_A2,
				'prog_B1' => $prog_curso_B1,
				'prog_B2' => $prog_curso_B2,
				'prog_C1' => $prog_curso_C1,
				'prog_hab_A1_L' => $prog_hab_L_A1,
				'prog_hab_A1_R' => $prog_hab_R_A1,
				'prog_hab_A1_W' => $prog_hab_W_A1,
				'prog_hab_A1_S' => $prog_hab_S_A1,
				'prog_hab_A2_L' => $prog_hab_L_A2,
				'prog_hab_A2_R' => $prog_hab_R_A2,
				'prog_hab_A2_W' => $prog_hab_W_A2,
				'prog_hab_A2_S' => $prog_hab_S_A2,
				'prog_hab_B1_L' => $prog_hab_L_B1,
				'prog_hab_B1_R' => $prog_hab_R_B1,
				'prog_hab_B1_W' => $prog_hab_W_B1,
				'prog_hab_B1_S' => $prog_hab_S_B1,
				'prog_hab_B2_L' => $prog_hab_L_B2,
				'prog_hab_B2_R' => $prog_hab_R_B2,
				'prog_hab_B2_W' => $prog_hab_W_B2,
				'prog_hab_B2_S' => $prog_hab_S_B2,
				'prog_hab_C1_L' => $prog_hab_L_C1,
				'prog_hab_C1_R' => $prog_hab_R_C1,
				'prog_hab_C1_W' => $prog_hab_W_C1,
				'prog_hab_C1_S' => $prog_hab_S_C1
			);
		}
		return $r;
	}
	public function getlocales($filtros){
		try{
			$this->oDatLocal->setLimite(0,2000);
			return $this->oDatLocal->buscar($filtros);
		}catch(Exception $e){
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("locales").": " . $e->getMessage());
		}
	}
	public function seccion_ubicacion($data){
		try{
			$resultado = array();
			
			// $rowsUbicacionResumen = $this->oDatResumen->buscar(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal']));
			// $rowsUbicacionResumenHab = $this->oDatResumen->buscarhabilidad(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal']));
			$this->oDatResumen->setLimite(0,2000);
			$examenes = $this->oDatResumen->ubicacion(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'U','idrecurso' => 326,'idproyecto'=> 3));
			
			$this->oDatResumen->setLimite(0,2000);
			$examenes_doc = $this->oDatResumen->ubicacion_docente(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'U','idrecurso' => 363,'idproyecto'=> 3));
			//calculo ubicacion:
			$resultUbicacion = $this->getUbicacionLocal($examenes);
			$resultUbicacion_doc = $this->getUbicacionLocal($examenes_doc);
			if(!empty($resultUbicacion)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'alumno_ubicacion' => $resultUbicacion['total'], 
					'ubicacion_A1' => $resultUbicacion['A1'],
					'ubicacion_A2' => $resultUbicacion['A2'],
					'ubicacion_B1' => $resultUbicacion['B1'],
					'ubicacion_B2' => $resultUbicacion['B2'],
					'ubicacion_C1' => $resultUbicacion['C1']
				);
				$update = array(
							'alumno_ubicacion' => $resultUbicacion['total'], 
							'ubicacion_A1' => $resultUbicacion['A1'],
							'ubicacion_A2' => $resultUbicacion['A2'],
							'ubicacion_B1' => $resultUbicacion['B1'],
							'ubicacion_B2' => $resultUbicacion['B2'],
							'ubicacion_C1' => $resultUbicacion['C1']
						);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'ubicacion_hab_L' => $resultUbicacion['L'],
					'ubicacion_hab_R' => $resultUbicacion['R'],
					'ubicacion_hab_W' => $resultUbicacion['W'],
					'ubicacion_hab_S' => $resultUbicacion['S']
				);
				$updatehab = array(
					'ubicacion_hab_L' => $resultUbicacion['L'],
					'ubicacion_hab_R' => $resultUbicacion['R'],
					'ubicacion_hab_W' => $resultUbicacion['W'],
					'ubicacion_hab_S' => $resultUbicacion['S']
				);
				$exist = $this->oDatResumen->buscar(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
				$exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}

			if(!empty($resultUbicacion_doc)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tipo' => 'D',
					'alumno_ubicacion' => $resultUbicacion_doc['total'], 
					'ubicacion_A1' => $resultUbicacion_doc['A1'],
					'ubicacion_A2' => $resultUbicacion_doc['A2'],
					'ubicacion_B1' => $resultUbicacion_doc['B1'],
					'ubicacion_B2' => $resultUbicacion_doc['B2'],
					'ubicacion_C1' => $resultUbicacion_doc['C1']
				);
				$update = array(
							'alumno_ubicacion' => $resultUbicacion_doc['total'], 
							'ubicacion_A1' => $resultUbicacion_doc['A1'],
							'ubicacion_A2' => $resultUbicacion_doc['A2'],
							'ubicacion_B1' => $resultUbicacion_doc['B1'],
							'ubicacion_B2' => $resultUbicacion_doc['B2'],
							'ubicacion_C1' => $resultUbicacion_doc['C1']
						);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'tipo' => 'D',
					'ubicacion_hab_L' => $resultUbicacion_doc['L'],
					'ubicacion_hab_R' => $resultUbicacion_doc['R'],
					'ubicacion_hab_W' => $resultUbicacion_doc['W'],
					'ubicacion_hab_S' => $resultUbicacion_doc['S']
				);
				$updatehab = array(
					'ubicacion_hab_L' => $resultUbicacion_doc['L'],
					'ubicacion_hab_R' => $resultUbicacion_doc['R'],
					'ubicacion_hab_W' => $resultUbicacion_doc['W'],
					'ubicacion_hab_S' => $resultUbicacion_doc['S']
				);
				$exist = $this->oDatResumen->buscar(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo'=>'D'));
				$exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo'=>'D'));
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}

			// $this->oDatResumen->setLimite(0,100000);
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	private function setestados($result,$arrayAdd = null){
		$ar = array(
			'alumno_ubicacion' => $result['alumno_ubicacion'],
			'ubicacion_A1' => $result['ubicacion_A1'],
			'ubicacion_A2' => $result['ubicacion_A2'],
			'ubicacion_B1' => $result['ubicacion_B1'],
			'ubicacion_B2' => $result['ubicacion_B2'],
			'ubicacion_C1' => $result['ubicacion_C1'],
			'entrada_prom' => $result['entrada_prom'],
			'alumno_entrada' => $result['alumno_entrada'],
			'salida_prom' =>$result['salida_prom'],
			'alumno_salida' =>$result['alumno_salida'],
			'alumno_examen_b1' => $result['alumno_examen_b1'], 
			'examen_b1_prom' => $result['examen_b1_prom'],
			'alumno_examen_b2' => $result['alumno_examen_b2'], 
			'examen_b2_prom' => $result['examen_b2_prom'],
			'alumno_examen_b3' => $result['alumno_examen_b3'], 
			'examen_b3_prom' => $result['examen_b3_prom'],
			'alumno_examen_b4' => $result['alumno_examen_b4'], 
			'examen_b4_prom' => $result['examen_b4_prom'],
			'alumno_examen_t1' => $result['alumno_examen_t1'], 
			'examen_t1_prom' => $result['examen_t1_prom'],
			'alumno_examen_t2' => $result['alumno_examen_t2'], 
			'examen_t2_prom' => $result['examen_t2_prom'],
			'alumno_examen_t3' => $result['alumno_examen_t3'], 
			'examen_t3_prom' => $result['examen_t3_prom'],
			'tiempopv' => $result['tiempopv'], 
			'tiempo_exam' => $result['tiempo_exam'], 
			'tiempo_task' => $result['tiempo_task'], 
			'tiempo_smartbook' => $result['tiempo_smartbook'], 
			'tiempo_practice' => $result['tiempo_practice'],
			'prog_A1' => $result['prog_A1'], 
			'prog_A2' => $result['prog_A2'], 
			'prog_B1' => $result['prog_B1'], 
			'prog_B2' => $result['prog_B2'], 
			'prog_C1' => $result['prog_C1']
		);
		if(!is_null($arrayAdd)){
			$ar = array_merge($ar,$arrayAdd);
		}
		return $ar;
	}
	private function setestadoshab($result2,$arrayAdd = null){
		$ar = array(
			'ubicacion_hab_L' => $result2['ubicacion_hab_L'],
			'ubicacion_hab_R' => $result2['ubicacion_hab_R'],
			'ubicacion_hab_W' => $result2['ubicacion_hab_W'],
			'ubicacion_hab_S' => $result2['ubicacion_hab_S'],
			'entrada_hab_L' => $result2['entrada_hab_L'],
			'entrada_hab_R' => $result2['entrada_hab_R'],
			'entrada_hab_W' => $result2['entrada_hab_W'],
			'entrada_hab_S' => $result2['entrada_hab_S'],
			'salida_hab_L' => $result2['salida_hab_L'],
			'salida_hab_R' => $result2['salida_hab_R'],
			'salida_hab_W' => $result2['salida_hab_W'],
			'salida_hab_S' => $result2['salida_hab_S'],
			'examen_b1_hab_L' => $result2['examen_b1_hab_L'],
			'examen_b1_hab_R' => $result2['examen_b1_hab_R'],
			'examen_b1_hab_W' => $result2['examen_b1_hab_W'],
			'examen_b1_hab_S' => $result2['examen_b1_hab_S'],
			'examen_b2_hab_L' => $result2['examen_b2_hab_L'],
			'examen_b2_hab_R' => $result2['examen_b2_hab_R'],
			'examen_b2_hab_W' => $result2['examen_b2_hab_W'],
			'examen_b2_hab_S' => $result2['examen_b2_hab_S'],
			'examen_b3_hab_L' => $result2['examen_b3_hab_L'],
			'examen_b3_hab_R' => $result2['examen_b3_hab_R'],
			'examen_b3_hab_W' => $result2['examen_b3_hab_W'],
			'examen_b3_hab_S' => $result2['examen_b3_hab_S'],
			'examen_b4_hab_L' => $result2['examen_b4_hab_L'],
			'examen_b4_hab_R' => $result2['examen_b4_hab_R'],
			'examen_b4_hab_W' => $result2['examen_b4_hab_W'],
			'examen_b4_hab_S' => $result2['examen_b4_hab_S'],
			'examen_t1_hab_L' => $result2['examen_t1_hab_L'],
			'examen_t1_hab_R' => $result2['examen_t1_hab_R'],
			'examen_t1_hab_W' => $result2['examen_t1_hab_W'],
			'examen_t1_hab_S' => $result2['examen_t1_hab_S'],
			'examen_t2_hab_L' => $result2['examen_t2_hab_L'],
			'examen_t2_hab_R' => $result2['examen_t2_hab_R'],
			'examen_t2_hab_W' => $result2['examen_t2_hab_W'],
			'examen_t2_hab_S' => $result2['examen_t2_hab_S'],
			'examen_t3_hab_L' => $result2['examen_t3_hab_L'],
			'examen_t3_hab_R' => $result2['examen_t3_hab_R'],
			'examen_t3_hab_W' => $result2['examen_t3_hab_W'],
			'examen_t3_hab_S' => $result2['examen_t3_hab_S'],
			'prog_hab_A1_L' => $result2['prog_hab_A1_L'],
			'prog_hab_A1_R' => $result2['prog_hab_A1_R'],
			'prog_hab_A1_W' => $result2['prog_hab_A1_W'],
			'prog_hab_A1_S' => $result2['prog_hab_A1_S'],
			'prog_hab_A2_L' => $result2['prog_hab_A2_L'],
			'prog_hab_A2_R' => $result2['prog_hab_A2_R'],
			'prog_hab_A2_W' => $result2['prog_hab_A2_W'],
			'prog_hab_A2_S' => $result2['prog_hab_A2_S'],
			'prog_hab_B1_L' => $result2['prog_hab_B1_L'],
			'prog_hab_B1_R' => $result2['prog_hab_B1_R'],
			'prog_hab_B1_W' => $result2['prog_hab_B1_W'],
			'prog_hab_B1_S' => $result2['prog_hab_B1_S'],
			'prog_hab_B2_L' => $result2['prog_hab_B2_L'],
			'prog_hab_B2_R' => $result2['prog_hab_B2_R'],
			'prog_hab_B2_W' => $result2['prog_hab_B2_W'],
			'prog_hab_B2_S' => $result2['prog_hab_B2_S'],
			'prog_hab_B2_L' => $result2['prog_hab_B2_L'],
			'prog_hab_B2_R' => $result2['prog_hab_B2_R'],
			'prog_hab_B2_W' => $result2['prog_hab_B2_W'],
			'prog_hab_B2_S' => $result2['prog_hab_B2_S'],
			'prog_hab_C1_L' => $result2['prog_hab_C1_L'],
			'prog_hab_C1_R' => $result2['prog_hab_C1_R'],
			'prog_hab_C1_W' => $result2['prog_hab_C1_W'],
			'prog_hab_C1_S' => $result2['prog_hab_C1_S']
		);
		if(!is_null($arrayAdd)){
			$ar = array_merge($ar,$arrayAdd);
		}
		return $ar;
	}

	public function grado_ubicacion($data){
		try{
			
			$resultbd = $this->oDatResumen->getresumen(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado']));
			$resultbd2 = $this->oDatResumen->getresumenhab(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado']));
			$result = $this->getResultLocal($resultbd);
			$result2 = $this->getResultLocal($resultbd2);

			$resultbd_doc = $this->oDatResumen->getresumen(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'],'tipo'=>'D'));
			$resultbd2_doc = $this->oDatResumen->getresumenhab(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'],'tipo'=>'D'));
			$result_doc = $this->getResultLocal($resultbd_doc);
			$result2_doc = $this->getResultLocal($resultbd2_doc);

			$valores = array('SQL_GRADO' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado']);
			if(!empty($result)){
				$insert = $this->setestados($result,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal'],'idgrado' => $data['idgrado'],'grado' => $data['grado']));
				$update = $this->setestados($result);
				
				$exist = $this->oDatResumen->buscar($valores);
				// $exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_GRADO' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal'],'idgrado'=>$g['idgrado']));
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_grado",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_grado",$exist[0]['id'],$update);
				}
				// if(empty($exist2)){
				// 	$this->oDatResumen->insertar("resumen_grado",$inserthab);
				// }else{
				// 	$this->oDatResumen->actualizar("resumen_grado",$exist2[0]['id'],$updatehab);
				// }
			} //end if result alumno
			if(!empty($result2)){

				$inserthab = $this->setestadoshab($result2,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal'],'idgrado' => $data['idgrado']) );
				$updatehab = $this->setestadoshab($result2);	
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_grado_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_grado_habilidad",$exist2[0]['id'],$updatehab);
				}
			}//end if result 2 alumno
			$valores['tipo'] = 'D';
			if(!empty($result_doc)){
				$insert = $this->setestados($result_doc,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal'],'idgrado' => $data['idgrado'],'grado' => $data['grado'],'tipo' => 'D'));
				$update = $this->setestados($result_doc);
				$exist = $this->oDatResumen->buscar($valores);
				// $exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_GRADO' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal'],'idgrado'=>$g['idgrado']));
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_grado",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_grado",$exist[0]['id'],$update);
				}
				// if(empty($exist2)){
				// 	$this->oDatResumen->insertar("resumen_grado",$inserthab);
				// }else{
				// 	$this->oDatResumen->actualizar("resumen_grado",$exist2[0]['id'],$updatehab);
				// }
			}
			if(!empty($result2_doc)){

				$inserthab = $this->setestadoshab($result2_doc,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal'],'idgrado' => $data['idgrado'],'tipo' => 'D') );
				$updatehab = $this->setestadoshab($result2_doc);	
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_grado_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_grado_habilidad",$exist2[0]['id'],$updatehab);
				}
			}
			
			return false;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function local_resumen($data){
		try{
			
			$resultbd = $this->oDatResumen->getresumen(array('SQL_GRADO'=> true,'idugel' => $data['idugel'], 'idlocal'=>$data['idlocal']));
			$resultbd2 = $this->oDatResumen->getresumenhab(array('SQL_GRADO'=> true,'idugel' => $data['idugel'], 'idlocal'=>$data['idlocal']));
			$result = $this->getResultLocal($resultbd);
			$result2 = $this->getResultLocal($resultbd2);

			$resultbd_doc = $this->oDatResumen->getresumen(array('SQL_GRADO'=> true,'idugel' => $data['idugel'], 'idlocal'=>$data['idlocal'],'tipo'=>'D'));
			$resultbd2_doc = $this->oDatResumen->getresumenhab(array('SQL_GRADO'=> true,'idugel' => $data['idugel'], 'idlocal'=>$data['idlocal'],'tipo'=>'D'));
			$result_doc = $this->getResultLocal($resultbd_doc);
			$result2_doc = $this->getResultLocal($resultbd2_doc);

			$valores = array('SQL_LOCAL' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal']);
			if(!empty($result)){
				
				$insert = $this->setestados($result,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'idugel'=>$data['idugel'], 'idlocal' =>$data['idlocal'], 'local' => $data['local']));
				$update = $this->setestados($result);
				
				$exist = $this->oDatResumen->buscar($valores);
				// $exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_GRADO' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal'],'idgrado'=>$g['idgrado']));
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_local",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_local",$exist[0]['id'],$update);
				}
			} //end if result alumno
			if(!empty($result2)){

				$inserthab = $this->setestadoshab($result2,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal']) );
				$updatehab = $this->setestadoshab($result2);	
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_local_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_local_habilidad",$exist2[0]['id'],$updatehab);
				}
			}//end if result 2 alumno
			$valores['tipo'] = 'D';
			if(!empty($result_doc)){
				
				$insert = $this->setestados($result_doc,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'idugel'=>$data['idugel'], 'idlocal' =>$data['idlocal'], 'local' => $data['local'],'tipo'=>'D'));
				$update = $this->setestados($result_doc);
				
				$exist = $this->oDatResumen->buscar($valores);
				// $exist2 = $this->oDatResumen->buscarhabilidad(array('SQL_GRADO' => true, 'iddre' =>$data['iddre'], 'idugel' => $value['idugel'],'idlocal' => $colegio['idlocal'],'idgrado'=>$g['idgrado']));
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_local",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_local",$exist[0]['id'],$update);
				}
			}
			if(!empty($result2_doc)){

				$inserthab = $this->setestadoshab($result2_doc,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'idlocal' =>$data['idlocal'],'tipo'=>'D') );
				$updatehab = $this->setestadoshab($result2_doc);	
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_local_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_local_habilidad",$exist2[0]['id'],$updatehab);
				}
			}
			
			return false;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function ugel_resumen($data){
		try{
			$resultbd = $this->oDatResumen->getresumen(array('SQL_LOCAL' => true,'idugel'=>$data['idugel']));
			$resultbd2 = $this->oDatResumen->getresumenhab(array('SQL_LOCAL' => true,'idugel'=>$data['idugel']));
			$result = $this->getResultLocal($resultbd);
			$result2 = $this->getResultLocal($resultbd2);

			$resultbd_doc = $this->oDatResumen->getresumen(array('SQL_LOCAL' => true,'idugel'=>$data['idugel'],'tipo'=>'D'));
			$resultbd2_doc = $this->oDatResumen->getresumenhab(array('SQL_LOCAL' => true,'idugel'=>$data['idugel'],'tipo'=>'D'));
			$result_doc = $this->getResultLocal($resultbd_doc);
			$result2_doc = $this->getResultLocal($resultbd2_doc);
			
			$valores = array('SQL_UGEL' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel']);
			if(!empty($result)){
				$insert = $this->setestados($result,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'idugel'=>$data['idugel'], 'ugel' =>$data['ugel'] ));
				$update = $this->setestados($result);
				$exist = $this->oDatResumen->buscar($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_ugel",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_ugel",$exist[0]['id'],$update);
				}
			}//end if result alumno
			if(!empty($result2)){
				$inserthab = $this->setestadoshab($result2,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel']));
				$updatehab = $this->setestadoshab($result2);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_ugel_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_ugel_habilidad",$exist2[0]['id'],$updatehab);
				}
			}//end if result 2 alumno
			$valores['tipo'] = 'D';
			if(!empty($result_doc)){
				$insert = $this->setestados($result_doc,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'idugel'=>$data['idugel'], 'ugel' =>$data['ugel'],'tipo'=>'D'));
				$update = $this->setestados($result_doc);
				$exist = $this->oDatResumen->buscar($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_ugel",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_ugel",$exist[0]['id'],$update);
				}
			}//end if result docente
			if(!empty($result2_doc)){
				$inserthab = $this->setestadoshab($result2_doc,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'idugel'=>$data['idugel'],'tipo'=>'D'));
				$updatehab = $this->setestadoshab($result2_doc);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_ugel_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_ugel_habilidad",$exist2[0]['id'],$updatehab);
				}
			}//end if result 2 docente
			
			return false;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function dre_resumen($data){
		try{
			$resultbd = $this->oDatResumen->getresumen(array('SQL_UGEL' => true,'iddre'=>$data['iddre']));
			$resultbd2 = $this->oDatResumen->getresumenhab(array('SQL_UGEL' => true,'iddre'=>$data['iddre']));
			$result = $this->getResultLocal($resultbd);
			$result2 = $this->getResultLocal($resultbd2);
			$resultbd_doc = $this->oDatResumen->getresumen(array('SQL_UGEL' => true,'iddre'=>$data['iddre'],'tipo'=>'D'));
			$resultbd2_doc = $this->oDatResumen->getresumenhab(array('SQL_UGEL' => true,'iddre'=>$data['iddre'],'tipo'=>'D'));
			$result_doc = $this->getResultLocal($resultbd_doc);
			$result2_doc = $this->getResultLocal($resultbd2_doc);
			$valores = array('SQL_DRE' => true, 'iddre' =>$data['iddre']);
			if(!empty($result)){
				$insert = $this->setestados($result,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'dre' =>$data['dre'] ));
				$update = $this->setestados($result);
				$exist = $this->oDatResumen->buscar($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_dre",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_dre",$exist[0]['id'],$update);
				}
			}//end if result alumno
			if(!empty($result2)){
				$inserthab = $this->setestadoshab($result2,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre']));
				$updatehab = $this->setestadoshab($result2);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_dre_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_dre_habilidad",$exist2[0]['id'],$updatehab);
				}
			}//end result 2 alumno
			$valores['tipo']='D';
			if(!empty($result_doc)){
				$insert = $this->setestados($result_doc,array('id_ubigeo' => $data['id_ubigeo'], 'iddre'=> $data['iddre'], 'dre' =>$data['dre'],'tipo'=>'D' ));
				$update = $this->setestados($result_doc);
				$exist = $this->oDatResumen->buscar($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar("resumen_dre",$insert);
				}else{
					$this->oDatResumen->actualizar("resumen_dre",$exist[0]['id'],$update);
				}
			}
			if(!empty($result2_doc)){
				$inserthab = $this->setestadoshab($result2_doc,array('id_ubigeo' => $data['id_ubigeo'],'iddre'=> $data['iddre'],'tipo'=>'D'));
				$updatehab = $this->setestadoshab($result2_doc);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist2)){
					$this->oDatResumen->insertar("resumen_dre_habilidad",$inserthab);
				}else{
					$this->oDatResumen->actualizar("resumen_dre_habilidad",$exist2[0]['id'],$updatehab);
				}
			}
			
			return false;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_entrada($data){
		try{
			$resultado = array();
			
			$this->oDatResumen->setLimite(0,2000);
			
			$examenes = $this->oDatResumen->examenes(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','entrada'=> true, 'idproyecto'=> 3));
			$promExamen = $this->oDatResumen->promedio(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','entrada'=> true, 'idproyecto'=> 3));
			$examenes_doc = $this->oDatResumen->examenes_doc(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','entrada'=> true, 'idproyecto'=> 3));
			$promExamen_doc = $this->oDatResumen->promedio_doc(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','entrada'=> true, 'idproyecto'=> 3));
			
			$resultEntrada = $this->getResultExamenLocal($examenes,$promExamen);
			$resultEntrada_doc = $this->getResultExamenLocal($examenes_doc,$promExamen_doc);
			
			//inser or update
			if(!empty($resultEntrada)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'alumno_entrada' => $resultEntrada['cantidad'], 
					'entrada_prom' => $resultEntrada['promedio']
				);
				$update = array(
					'alumno_entrada' => $resultEntrada['cantidad'], 
					'entrada_prom' => $resultEntrada['promedio']
				);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'entrada_hab_L' => $resultEntrada['L'],
					'entrada_hab_R' => $resultEntrada['R'],
					'entrada_hab_W' => $resultEntrada['W'],
					'entrada_hab_S' => $resultEntrada['S']
				);
				$updatehab = array(
					'entrada_hab_L' => $resultEntrada['L'],
					'entrada_hab_R' => $resultEntrada['R'],
					'entrada_hab_W' => $resultEntrada['W'],
					'entrada_hab_S' => $resultEntrada['S']
				);
				
				$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']);
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
				
			}
			if(!empty($resultEntrada_doc)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tipo' => 'D',
					'alumno_entrada' => $resultEntrada_doc['cantidad'], 
					'entrada_prom' => $resultEntrada_doc['promedio']
				);
				$update = array(
					'alumno_entrada' => $resultEntrada_doc['cantidad'], 
					'entrada_prom' => $resultEntrada_doc['promedio']
				);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'tipo' => 'D',
					'entrada_hab_L' => $resultEntrada_doc['L'],
					'entrada_hab_R' => $resultEntrada_doc['R'],
					'entrada_hab_W' => $resultEntrada_doc['W'],
					'entrada_hab_S' => $resultEntrada_doc['S']
				);
				$updatehab = array(
					'entrada_hab_L' => $resultEntrada_doc['L'],
					'entrada_hab_R' => $resultEntrada_doc['R'],
					'entrada_hab_W' => $resultEntrada_doc['W'],
					'entrada_hab_S' => $resultEntrada_doc['S']
				);
				
				$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D');
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
				
			}
			
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_salida($data){
		try{
			$resultado = array();
			
			$this->oDatResumen->setLimite(0,2000);
			$filtros = array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','salida'=> true, 'idproyecto'=> 3);
			$examenes = $this->oDatResumen->examenes($filtros);
			$promExamen = $this->oDatResumen->promedio($filtros);
			$examenes_doc = $this->oDatResumen->examenes_doc($filtros);
			$promExamen_doc = $this->oDatResumen->promedio_doc($filtros);

			$resultSalida = $this->getResultExamenLocal($examenes,$promExamen);
			$resultSalida_doc = $this->getResultExamenLocal($examenes_doc,$promExamen_doc);
			//inser or update
			if(!empty($resultSalida)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'alumno_salida' => $resultSalida['cantidad'], 
					'salida_prom' => $resultSalida['promedio']
				);
				$update = array(
						'alumno_salida' => $resultSalida['cantidad'], 
						'salida_prom' => $resultSalida['promedio']
					);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'salida_hab_L' => $resultSalida['L'],
					'salida_hab_R' => $resultSalida['R'],
					'salida_hab_W' => $resultSalida['W'],
					'salida_hab_S' => $resultSalida['S']
				);
				$updatehab = array(
					'salida_hab_L' => $resultSalida['L'],
					'salida_hab_R' => $resultSalida['R'],
					'salida_hab_W' => $resultSalida['W'],
					'salida_hab_S' => $resultSalida['S']
				);
				$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']);
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}

			if(!empty($resultSalida_doc)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tipo' => 'D',
					'alumno_salida' => $resultSalida_doc['cantidad'], 
					'salida_prom' => $resultSalida_doc['promedio']
				);
				$update = array(
						'alumno_salida' => $resultSalida_doc['cantidad'], 
						'salida_prom' => $resultSalida_doc['promedio']
					);
				$inserthab = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'tipo' => 'D',
					'salida_hab_L' => $resultSalida_doc['L'],
					'salida_hab_R' => $resultSalida_doc['R'],
					'salida_hab_W' => $resultSalida_doc['W'],
					'salida_hab_S' => $resultSalida_doc['S']
				);
				$updatehab = array(
					'salida_hab_L' => $resultSalida_doc['L'],
					'salida_hab_R' => $resultSalida_doc['R'],
					'salida_hab_W' => $resultSalida_doc['W'],
					'salida_hab_S' => $resultSalida_doc['S']
				);
				$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D');
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}
			
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_bimestre($data){
		try{
			$resultado = array();
			
			for($i = 1; $i <= 4; $i++){
				$this->oDatResumen->setLimite(0,2000);
				$filtros = array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E','bimestre'=> $i, 'idproyecto'=> 3);
				$examenes = $this->oDatResumen->examenes($filtros);
				$promExamen = $this->oDatResumen->promedio($filtros);
				$examenes_doc = $this->oDatResumen->examenes_doc($filtros);
				$promExamen_doc = $this->oDatResumen->promedio_doc($filtros);
				$resultBimestre = $this->getResultExamenLocal($examenes,$promExamen);
				$resultBimestre_doc = $this->getResultExamenLocal($examenes_doc,$promExamen_doc);
				//inser or update
				if(!empty($resultBimestre)){
					$insert = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'seccion' => $data['sesion'],
						'alumno_examen_b'.$i => $resultBimestre['cantidad'], 
						'examen_b'.$i.'_prom' => $resultBimestre['promedio']
					);
					$update = array(
							'alumno_examen_b'.$i => $resultBimestre['cantidad'], 
							'examen_b'.$i.'_prom' => $resultBimestre['promedio']
						);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'examen_b'.$i.'_hab_L' => $resultBimestre['L'],
						'examen_b'.$i.'_hab_R' => $resultBimestre['R'],
						'examen_b'.$i.'_hab_W' => $resultBimestre['W'],
						'examen_b'.$i.'_hab_S' => $resultBimestre['S']
					);
					$updatehab = array(
						'examen_b'.$i.'_hab_L' => $resultBimestre['L'],
						'examen_b'.$i.'_hab_R' => $resultBimestre['R'],
						'examen_b'.$i.'_hab_W' => $resultBimestre['W'],
						'examen_b'.$i.'_hab_S' => $resultBimestre['S']
					);
					$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']);
					$exist = $this->oDatResumen->buscar($valores);
					$exist2 = $this->oDatResumen->buscarhabilidad($valores);
					if(empty($exist)){
						$this->oDatResumen->insertar_seccion($insert);
					}else{
						$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
					}
					if(empty($exist2)){
						$this->oDatResumen->insertar_seccion_hab($inserthab);
					}else{
						$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
					}
				}
				if(!empty($resultBimestre_doc)){
					$insert = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'seccion' => $data['sesion'],
						'tipo' => 'D',
						'alumno_examen_b'.$i => $resultBimestre_doc['cantidad'], 
						'examen_b'.$i.'_prom' => $resultBimestre_doc['promedio']
					);
					$update = array(
							'alumno_examen_b'.$i => $resultBimestre_doc['cantidad'], 
							'examen_b'.$i.'_prom' => $resultBimestre_doc['promedio']
						);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'tipo' => 'D',
						'examen_b'.$i.'_hab_L' => $resultBimestre_doc['L'],
						'examen_b'.$i.'_hab_R' => $resultBimestre_doc['R'],
						'examen_b'.$i.'_hab_W' => $resultBimestre_doc['W'],
						'examen_b'.$i.'_hab_S' => $resultBimestre_doc['S']
					);
					$updatehab = array(
						'examen_b'.$i.'_hab_L' => $resultBimestre_doc['L'],
						'examen_b'.$i.'_hab_R' => $resultBimestre_doc['R'],
						'examen_b'.$i.'_hab_W' => $resultBimestre_doc['W'],
						'examen_b'.$i.'_hab_S' => $resultBimestre_doc['S']
					);
					$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D');
					$exist = $this->oDatResumen->buscar($valores);
					$exist2 = $this->oDatResumen->buscarhabilidad($valores);
					if(empty($exist)){
						$this->oDatResumen->insertar_seccion($insert);
					}else{
						$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
					}
					if(empty($exist2)){
						$this->oDatResumen->insertar_seccion_hab($inserthab);
					}else{
						$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
					}
				}
			}
									
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_trimestre($data){
		try{
			$resultado = array();
			for($i = 1; $i <= 3; $i++){
				$this->oDatResumen->setLimite(0,2000);
				$filtros = array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'], 'tipo' => 'E', 'trimestre'=> $i, 'idproyecto'=> 3);
				$examenes = $this->oDatResumen->examenes($filtros);
				$promExamen = $this->oDatResumen->promedio($filtros);
				$examenes_doc = $this->oDatResumen->examenes_doc($filtros);
				$promExamen_doc = $this->oDatResumen->promedio_doc($filtros);
				$resultTrimestre = $this->getResultExamenLocal($examenes,$promExamen);
				$resultTrimestre_doc = $this->getResultExamenLocal($examenes_doc,$promExamen_doc);
				if(!empty($resultTrimestre)){
					$insert = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'seccion' => $data['sesion'],
						'alumno_examen_t'.$i => $resultTrimestre['cantidad'], 
						'examen_t'.$i.'_prom' => $resultTrimestre['promedio']
					);
					$update = array(
							'alumno_examen_t'.$i => $resultTrimestre['cantidad'], 
							'examen_t'.$i.'_prom' => $resultTrimestre['promedio']
						);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'examen_t'.$i.'_hab_L' => $resultTrimestre['L'],
						'examen_t'.$i.'_hab_R' => $resultTrimestre['R'],
						'examen_t'.$i.'_hab_W' => $resultTrimestre['W'],
						'examen_t'.$i.'_hab_S' => $resultTrimestre['S']
					);
					$updatehab = array(
						'examen_t'.$i.'_hab_L' => $resultTrimestre['L'],
						'examen_t'.$i.'_hab_R' => $resultTrimestre['R'],
						'examen_t'.$i.'_hab_W' => $resultTrimestre['W'],
						'examen_t'.$i.'_hab_S' => $resultTrimestre['S']
					);
					$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']);
					$exist = $this->oDatResumen->buscar($valores);
					$exist2 = $this->oDatResumen->buscarhabilidad($valores);
					if(empty($exist)){
						$this->oDatResumen->insertar_seccion($insert);
					}else{
						$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
					}
					if(empty($exist2)){
						$this->oDatResumen->insertar_seccion_hab($inserthab);
					}else{
						$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
					}
				}//end if alumno
				if(!empty($resultTrimestre_doc)){
					$insert = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'seccion' => $data['sesion'],
						'tipo' => 'D',
						'alumno_examen_t'.$i => $resultTrimestre_doc['cantidad'], 
						'examen_t'.$i.'_prom' => $resultTrimestre_doc['promedio']
					);
					$update = array(
							'alumno_examen_t'.$i => $resultTrimestre_doc['cantidad'], 
							'examen_t'.$i.'_prom' => $resultTrimestre_doc['promedio']
						);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'tipo' => 'D',
						'examen_t'.$i.'_hab_L' => $resultTrimestre_doc['L'],
						'examen_t'.$i.'_hab_R' => $resultTrimestre_doc['R'],
						'examen_t'.$i.'_hab_W' => $resultTrimestre_doc['W'],
						'examen_t'.$i.'_hab_S' => $resultTrimestre_doc['S']
					);
					$updatehab = array(
						'examen_t'.$i.'_hab_L' => $resultTrimestre_doc['L'],
						'examen_t'.$i.'_hab_R' => $resultTrimestre_doc['R'],
						'examen_t'.$i.'_hab_W' => $resultTrimestre_doc['W'],
						'examen_t'.$i.'_hab_S' => $resultTrimestre_doc['S']
					);
					$valores = array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D');
					$exist = $this->oDatResumen->buscar($valores);
					$exist2 = $this->oDatResumen->buscarhabilidad($valores);
					if(empty($exist)){
						$this->oDatResumen->insertar_seccion($insert);
					}else{
						$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
					}
					if(empty($exist2)){
						$this->oDatResumen->insertar_seccion_hab($inserthab);
					}else{
						$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
					}
				}//end if docente
			}
						
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_tiempo($data){
		try{
			$resultado = array();
			
			$this->oDatResumen->setLimite(0,2000);
			$tiempos = $this->oDatResumen->tiempos(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
			$tiempos_doc = $this->oDatResumen->tiempos_doc(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
			$result = $this->getResultTiemposLocal($tiempos);
			$result_doc = $this->getResultTiemposLocal($tiempos_doc);
			//aun
			//inser or update
			if(!empty($result)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tiempopv' => $result['tiempopv'], 
					'tiempo_exam' => $result['tiempoe'], 
					'tiempo_task' => $result['tiempot'], 
					'tiempo_smartbook' => $result['tiempos'], 
					'tiempo_practice' => $result['tiempop'] 
				);
				$update = array(
						'tiempopv' => $result['tiempopv'], 
						'tiempo_exam' => $result['tiempoe'], 
						'tiempo_task' => $result['tiempot'], 
						'tiempo_smartbook' => $result['tiempos'], 
						'tiempo_practice' => $result['tiempop'] 
					);
				
				
				$exist = $this->oDatResumen->buscar(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				
			}//end if alumno
			if(!empty($result_doc)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tipo' => 'D',
					'tiempopv' => $result_doc['tiempopv'], 
					'tiempo_exam' => $result_doc['tiempoe'], 
					'tiempo_task' => $result_doc['tiempot'], 
					'tiempo_smartbook' => $result_doc['tiempos'], 
					'tiempo_practice' => $result_doc['tiempop'] 
				);
				$update = array(
						'tiempopv' => $result_doc['tiempopv'], 
						'tiempo_exam' => $result_doc['tiempoe'], 
						'tiempo_task' => $result_doc['tiempot'], 
						'tiempo_smartbook' => $result_doc['tiempos'], 
						'tiempo_practice' => $result_doc['tiempop'] 
					);
				
				
				$exist = $this->oDatResumen->buscar(array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D'));
				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				
			}//end if docente
			
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function seccion_progreso($data){
		try{
			$resultado = array();
			
			$this->oDatResumen->setLimite(0,2000);
			$progresos = $this->oDatResumen->progresos(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
			$progresos_doc = $this->oDatResumen->progresos_doc(array('idlocal'=>$data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']));
			$result = $this->getResultProgresosLocal($progresos);
			$result_doc = $this->getResultProgresosLocal($progresos_doc);
			//aun
			//inser or update
			if(!empty($result)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'prog_A1' => $result['prog_A1'], 
					'prog_A2' => $result['prog_A2'], 
					'prog_B1' => $result['prog_B1'], 
					'prog_B2' => $result['prog_B2'], 
					'prog_C1' => $result['prog_C1'] 
				);
				$update = array(
					'prog_A1' => $result['prog_A1'], 
					'prog_A2' => $result['prog_A2'], 
					'prog_B1' => $result['prog_B1'], 
					'prog_B2' => $result['prog_B2'], 
					'prog_C1' => $result['prog_C1']  
				);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'prog_hab_A1_L' => $result['prog_hab_A1_L'],
						'prog_hab_A1_R' => $result['prog_hab_A1_R'],
						'prog_hab_A1_W' => $result['prog_hab_A1_W'],
						'prog_hab_A1_S' => $result['prog_hab_A1_S'],
						'prog_hab_A2_L' => $result['prog_hab_A2_L'],
						'prog_hab_A2_R' => $result['prog_hab_A2_R'],
						'prog_hab_A2_W' => $result['prog_hab_A2_W'],
						'prog_hab_A2_S' => $result['prog_hab_A2_S'],
						'prog_hab_B1_L' => $result['prog_hab_B1_L'],
						'prog_hab_B1_R' => $result['prog_hab_B1_R'],
						'prog_hab_B1_W' => $result['prog_hab_B1_W'],
						'prog_hab_B1_S' => $result['prog_hab_B1_S'],
						'prog_hab_B2_L' => $result['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result['prog_hab_B2_S'],
						'prog_hab_B2_L' => $result['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result['prog_hab_B2_S'],
						'prog_hab_C1_L' => $result['prog_hab_C1_L'],
						'prog_hab_C1_R' => $result['prog_hab_C1_R'],
						'prog_hab_C1_W' => $result['prog_hab_C1_W'],
						'prog_hab_C1_S' => $result['prog_hab_C1_S']
					);
					$updatehab = array(
						'prog_hab_A1_L' => $result['prog_hab_A1_L'],
						'prog_hab_A1_R' => $result['prog_hab_A1_R'],
						'prog_hab_A1_W' => $result['prog_hab_A1_W'],
						'prog_hab_A1_S' => $result['prog_hab_A1_S'],
						'prog_hab_A2_L' => $result['prog_hab_A2_L'],
						'prog_hab_A2_R' => $result['prog_hab_A2_R'],
						'prog_hab_A2_W' => $result['prog_hab_A2_W'],
						'prog_hab_A2_S' => $result['prog_hab_A2_S'],
						'prog_hab_B1_L' => $result['prog_hab_B1_L'],
						'prog_hab_B1_R' => $result['prog_hab_B1_R'],
						'prog_hab_B1_W' => $result['prog_hab_B1_W'],
						'prog_hab_B1_S' => $result['prog_hab_B1_S'],
						'prog_hab_B2_L' => $result['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result['prog_hab_B2_S'],
						'prog_hab_B2_L' => $result['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result['prog_hab_B2_S'],
						'prog_hab_C1_L' => $result['prog_hab_C1_L'],
						'prog_hab_C1_R' => $result['prog_hab_C1_R'],
						'prog_hab_C1_W' => $result['prog_hab_C1_W'],
						'prog_hab_C1_S' => $result['prog_hab_C1_S']
					);
				$valores =array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion']);
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);

				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}//end if alumno
			if(!empty($result_doc)){
				$insert = array(
					'id_ubigeo' => $data['id_ubigeo'],
					'iddre'=> $data['iddre'],
					'idugel'=>$data['idugel'],
					'idlocal' =>$data['idlocal'],
					'idgrado' => $data['idgrado'],
					'idseccion' => $data['idsesion'],
					'seccion' => $data['sesion'],
					'tipo' => 'D',
					'prog_A1' => $result_doc['prog_A1'], 
					'prog_A2' => $result_doc['prog_A2'], 
					'prog_B1' => $result_doc['prog_B1'], 
					'prog_B2' => $result_doc['prog_B2'], 
					'prog_C1' => $result_doc['prog_C1'] 
				);
				$update = array(
					'prog_A1' => $result_doc['prog_A1'], 
					'prog_A2' => $result_doc['prog_A2'], 
					'prog_B1' => $result_doc['prog_B1'], 
					'prog_B2' => $result_doc['prog_B2'], 
					'prog_C1' => $result_doc['prog_C1']  
				);
					$inserthab = array(
						'id_ubigeo' => $data['id_ubigeo'],
						'iddre'=> $data['iddre'],
						'idugel'=>$data['idugel'],
						'idlocal' =>$data['idlocal'],
						'idgrado' => $data['idgrado'],
						'idseccion' => $data['idsesion'],
						'tipo' => 'D',
						'prog_hab_A1_L' => $result_doc['prog_hab_A1_L'],
						'prog_hab_A1_R' => $result_doc['prog_hab_A1_R'],
						'prog_hab_A1_W' => $result_doc['prog_hab_A1_W'],
						'prog_hab_A1_S' => $result_doc['prog_hab_A1_S'],
						'prog_hab_A2_L' => $result_doc['prog_hab_A2_L'],
						'prog_hab_A2_R' => $result_doc['prog_hab_A2_R'],
						'prog_hab_A2_W' => $result_doc['prog_hab_A2_W'],
						'prog_hab_A2_S' => $result_doc['prog_hab_A2_S'],
						'prog_hab_B1_L' => $result_doc['prog_hab_B1_L'],
						'prog_hab_B1_R' => $result_doc['prog_hab_B1_R'],
						'prog_hab_B1_W' => $result_doc['prog_hab_B1_W'],
						'prog_hab_B1_S' => $result_doc['prog_hab_B1_S'],
						'prog_hab_B2_L' => $result_doc['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result_doc['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result_doc['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result_doc['prog_hab_B2_S'],
						'prog_hab_B2_L' => $result_doc['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result_doc['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result_doc['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result_doc['prog_hab_B2_S'],
						'prog_hab_C1_L' => $result_doc['prog_hab_C1_L'],
						'prog_hab_C1_R' => $result_doc['prog_hab_C1_R'],
						'prog_hab_C1_W' => $result_doc['prog_hab_C1_W'],
						'prog_hab_C1_S' => $result_doc['prog_hab_C1_S']
					);
					$updatehab = array(
						'prog_hab_A1_L' => $result_doc['prog_hab_A1_L'],
						'prog_hab_A1_R' => $result_doc['prog_hab_A1_R'],
						'prog_hab_A1_W' => $result_doc['prog_hab_A1_W'],
						'prog_hab_A1_S' => $result_doc['prog_hab_A1_S'],
						'prog_hab_A2_L' => $result_doc['prog_hab_A2_L'],
						'prog_hab_A2_R' => $result_doc['prog_hab_A2_R'],
						'prog_hab_A2_W' => $result_doc['prog_hab_A2_W'],
						'prog_hab_A2_S' => $result_doc['prog_hab_A2_S'],
						'prog_hab_B1_L' => $result_doc['prog_hab_B1_L'],
						'prog_hab_B1_R' => $result_doc['prog_hab_B1_R'],
						'prog_hab_B1_W' => $result_doc['prog_hab_B1_W'],
						'prog_hab_B1_S' => $result_doc['prog_hab_B1_S'],
						'prog_hab_B2_L' => $result_doc['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result_doc['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result_doc['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result_doc['prog_hab_B2_S'],
						'prog_hab_B2_L' => $result_doc['prog_hab_B2_L'],
						'prog_hab_B2_R' => $result_doc['prog_hab_B2_R'],
						'prog_hab_B2_W' => $result_doc['prog_hab_B2_W'],
						'prog_hab_B2_S' => $result_doc['prog_hab_B2_S'],
						'prog_hab_C1_L' => $result_doc['prog_hab_C1_L'],
						'prog_hab_C1_R' => $result_doc['prog_hab_C1_R'],
						'prog_hab_C1_W' => $result_doc['prog_hab_C1_W'],
						'prog_hab_C1_S' => $result_doc['prog_hab_C1_S']
					);
				$valores =array('SQL_SECCION' => true, 'iddre' =>$data['iddre'], 'idugel' => $data['idugel'],'idlocal' => $data['idlocal'],'idgrado'=>$data['idgrado'], 'idsesion' => $data['idsesion'],'tipo' => 'D');
				$exist = $this->oDatResumen->buscar($valores);
				$exist2 = $this->oDatResumen->buscarhabilidad($valores);

				if(empty($exist)){
					$this->oDatResumen->insertar_seccion($insert);
				}else{
					$this->oDatResumen->actualizar_seccion($exist[0]['id'],$update);
				}
				if(empty($exist2)){
					$this->oDatResumen->insertar_seccion_hab($inserthab);
				}else{
					$this->oDatResumen->actualizar_seccion_hab($exist2[0]['id'],$updatehab);
				}
			}//end if docente				
			
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	private function insertOrUpdate($container1,$container2,$cond,$d, $t,$in,$inhab,$up,$uphab){
		
		if(empty($container2)){
			$this->oDatResumen->insertar_seccion_hab($inserthab);
		}else{
			foreach($container2 as $key => $value){

			}
			$this->oDatResumen->actualizar_seccion_hab($updatehab);
		}
		// if(empty($rowsUbicacionResumen)){
		// 	$this->oDatResumen->insertar_seccion(array(
		// 		'id_ubigeo' => $data['id_ubigeo'],
		// 		'iddre'=> $data['iddre'],
		// 		'idugel'=>$value['idugel'],
		// 		'idlocal' =>$colegio['idlocal'],
		// 		'idgrado' => $g['idgrado'],
		// 		'idseccion' => $s['idsesion'],
		// 		'seccion' => $s['descripcion'],
		// 		'alumno_ubicacion' => $resultUbicacion['total'], 
		// 		'ubicacion_A1' => $resultUbicacion['A1'],
		// 		'ubicacion_A2' => $resultUbicacion['A2'],
		// 		'ubicacion_B1' => $resultUbicacion['B1'],
		// 		'ubicacion_B2' => $resultUbicacion['B2'],
		// 		'ubicacion_C1' => $resultUbicacion['C1']
		// 		)
		// 	);
		// }else{
		// 	$this->oDatResumen->actualizar_seccion(array(
		// 		'alumno_ubicacion' => $resultUbicacion['total'], 
		// 		'ubicacion_A1' => $resultUbicacion['A1'],
		// 		'ubicacion_A2' => $resultUbicacion['A2'],
		// 		'ubicacion_B1' => $resultUbicacion['B1'],
		// 		'ubicacion_B2' => $resultUbicacion['B2'],
		// 		'ubicacion_C1' => $resultUbicacion['C1']
		// 		)
		// 	);
		// }
	}
}
?>