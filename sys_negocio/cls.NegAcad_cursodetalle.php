<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-11-2017
 * @copyright	Copyright (C) 03-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_cursodetalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_cursodetalle 
{
	protected $idcursodetalle;
	protected $idcurso;
	protected $orden;
	protected $idrecurso;
	protected $tiporecurso;
	protected $idlogro;
	protected $idpadre;
	protected $url;	
	protected $color;
	protected $esfinal;

	protected $dataAcad_cursodetalle;
	protected $oDatAcad_cursodetalle;
	protected $oDatNiveles;	

	public function __construct()
	{
		$this->oDatAcad_cursodetalle = new DatAcad_cursodetalle;
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;			
			$this->oDatAcad_cursodetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_cursodetalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getMaxorden($idcurso,$idpadre)
	{
		try {
			return $this->oDatAcad_cursodetalle->maxorden($idcurso,$idpadre);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$this->oDatAcad_cursodetalle->setLimite(0,100000000);
			$datos=$this->oDatAcad_cursodetalle->buscar($filtros);			
			$data=array();
			$arrContextOptions=array("ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ),); 
			if(!empty($datos))
			foreach ($datos as $dt){
				$tipo=$dt["tiporecurso"];
				$infonivel=array('N','U','L');
				$infoexam =array('E');
				if(in_array($tipo,$infonivel)){
					$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
					$dt["nombre"]=@$nivel[0]["nombre"];
					$dt["estado"]=@$nivel[0]["estado"];
					$dt["idpersonal"]=@$nivel[0]["idpersonal"];
					$dt["descripcion"]=@$nivel[0]["descripcion"];
					$dt["imagen"]=@$nivel[0]["imagen"];
				}elseif(in_array($tipo,$infoexam)){
					$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
					$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';					
					$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $dt["nombre"] = $dataExam['data']['titulo'];
                        $dt["imagen"] = $dataExam['data']['portada'];
                    }
				}
				if($tipo=='N'){
					$unidades=$this->oDatAcad_cursodetalle->buscar(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'U'));
					$nexamenes=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'E'));
					$nunidad=0;
					$nactividad=0;
					if(!empty($unidades)){
						$nunidad=count($unidades);						
						foreach ($unidades as $unidad){
							$nactividad+=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$unidad["idcursodetalle"],'tiporecurso'=>'L'));
							$nexamenes+=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$unidad["idcursodetalle"],'tiporecurso'=>'E'));
						}
					}
					$dt["nunidad"]=$nunidad;
					$dt["nactividad"]=$nactividad;
					$dt["nexamenes"]=$nexamenes;
				}elseif($tipo=='U'){
					$nexamenes=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'E'));
					$nactividad=$this->oDatAcad_cursodetalle->getNumRegistros(array('idpadre'=>$dt["idcursodetalle"],'tiporecurso'=>'L'));
					$dt["nactividad"]=$nactividad;
					$dt["nexamenes"]=$nexamenes;
				}
				$data[]=$dt;				
			}
			return $data;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getprogresounidad($filtros=null){
		try{
			$this->setLimite(0,10000);
			return $this->oDatAcad_cursodetalle->getprogresounidad($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function unidades($idpadre = 0){
		try{
			//lol
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			if(!empty($datos)){
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					//hola
				}
			}//end if empty datos
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function sesiones($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			if(!empty($datos)){
				$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
						$dt["nombre"]=@$nivel[0]["nombre"];
						$dt["estado"]=@$nivel[0]["estado"];
						$dt["idpersonal"]=@$nivel[0]["idpersonal"];
						$dt["descripcion"]=@$nivel[0]["descripcion"];
						$dt["imagen"]=@$nivel[0]["imagen"];
					}elseif($tipo=='E'){
						$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
					}
					$datos3=$this->sesiones($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;			
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function sesiones2($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			if(!empty($datos)){
				$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 			
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					$tipo=$dt["tiporecurso"];
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
						$dt["nombre"]=@$nivel[0]["nombre"];
						$dt["estado"]=@$nivel[0]["estado"];
						$dt["idpersonal"]=@$nivel[0]["idpersonal"];
						$dt["descripcion"]=@$nivel[0]["descripcion"];
						$dt["imagen"]=@$nivel[0]["imagen"];
					}//elseif($tipo=='E'){
						/*$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
						$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
						$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                            $dataExam = json_decode($dataExam, true);
                            if($dataExam['code']==200){
                                $dt["nombre"] = $dataExam['data']['titulo'];
                                $dt["imagen"] = $dataExam['data']['portada'];
                            }
                            */
					//}
					$datos3=$this->sesiones2($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt["hijo"]=$datos3; }
					$datos2[]=$dt;
				}
			}else return null;			
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function sesiones3($idcurso,$idpadre){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar(array("idcurso"=>$idcurso,'idpadre'=>$idpadre));
			$datos2=array();
			if(!empty($datos)){						
				$infonivel=array('N','U','L','M');
				foreach ($datos as $dt){
					$dt1=array();
					$tipo=$dt["tiporecurso"];
					$dt1["tiporecurso"]=$dt["tiporecurso"];
					if(in_array($tipo,$infonivel)){
						$nivel=$this->oDatNiveles->buscar(array('idnivel'=>$dt["idrecurso"],'tipo'=>$tipo));
						$dt1["idcursodetalle"]=@$dt["idcursodetalle"];
						$dt["nombre"]=@$nivel[0]["nombre"];
						$dt1["idcurso"]=@$dt["idcurso"];
						$dt1["orden"]=@$dt["orden"];
						$dt1["idrecurso"]=@$dt["idrecurso"];
					}elseif($tipo=='E') continue;
					$datos3=$this->sesiones3($idcurso,$dt["idcursodetalle"]);
					if(!empty($datos3)){ $dt1["hijo"]=$datos3; }
					$datos2[]=$dt1;
				}
			}else return null;
			return $datos2;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function examenes($filtros){ // muestra toda la secuencia de sesiones segun el curso y el idpadre
		try{
			$datos=$this->oDatAcad_cursodetalle->buscar($filtros);
			/*$datos1=array();
			$arrContextOptions=array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false,),); 
			foreach ($datos as $dt){				
				$dt["nombre"]=ucfirst(JrTexto::_('Exam'));
				$dt["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';						
				$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$dt['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                    $dataExam = json_decode($dataExam, true);
                    if($dataExam['code']==200){
                        $dt["nombre"] = $dataExam['data']['titulo'];
                        $dt["imagen"] = $dataExam['data']['portada'];
                    }
				$datos1[]=$dt;
			}*/
			//var_dump(expression)
			return $datos;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}




	public function getSoloSesiones($idcurso, $idpadre)
	{
		try {
			$sesiones = $hijos = array();
			$arbol_sesiones = $this->sesiones($idcurso, $idpadre);

			if(!empty($arbol_sesiones)){
				foreach ($arbol_sesiones as $i=>$a) {
					$tipo=$a["tiporecurso"];
					if($tipo=='U' && !empty(@$a['hijo'])){ $hijos = array_merge($hijos, $a['hijo']); } 
					else if($tipo=='L'){ $hijos = $arbol_sesiones; }
				}
				
				/*//Extraer solo las que son Lecciones hijos (sesiones) sin exámenes:
				if(!empty($hijos)){
					foreach ($hijos as $i => $h) {
						if($h["tiporecurso"]=='L'){
							$sesiones[] = $h;
						}
					}
				}*/
				
				//Todas las lecciones y exámenes hijos.
				$sesiones = $hijos; 
			}
			//echo '<pre>'; print_r($sesiones); echo '</pre>';
			return $sesiones;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getTodosPadresXCursoDet($filtros=array(), $orden='')
	{
		/**
		* @param $orden:
		* 'ASC' : a partir del hijo $cursoDetalle obtenido de $filtros tendra dentro un campo para su padre y este 
		*		  último tendrá a su padre y asi sucesivamente hasta obtener el útlimo padre.
		* 'DESC': obtendrá desde el primer padre el cual contendrá a un hijo y este último a su hijo y así  
		* 		  sucesivamente hasta llegar al $cursoDetalle obtenido de $filtros.
		*/
		try {
			$arrTodosPadres = $arrPadresOrdenado = array();

			do {
				$detalle = array();
				$curso_det = $this->oDatAcad_cursodetalle->buscar($filtros);
				if(!empty($curso_det)){ 
					$detalle = $curso_det[0]; 
					$nivel = $this->oDatNiveles->buscar(array('idnivel'=>$detalle["idrecurso"],'tipo'=>$detalle["tiporecurso"]));
					$detalle["nombre"] = @$nivel[0]["nombre"];
					$detalle["descripcion"] = @$nivel[0]["descripcion"];
					$detalle["imagen"] = @$nivel[0]["imagen"];
				}
				$arrTodosPadres[] = $detalle;
				$filtros["idcursodetalle"] = $detalle["idpadre"];
			} while( (int)$filtros["idcursodetalle"]!=0 );

			if($orden=='ASC') {
				$arrPadresOrdenado = $this->ordenarCursoDet_ASC($arrTodosPadres);
			} else if($orden=='DESC') {
				$arrPadresOrdenado = $this->ordenarCursoDet_DESC($arrTodosPadres);
			} else if($orden=='') {
				$arrPadresOrdenado = $arrTodosPadres;
			}

			return $arrPadresOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function ordenarCursoDet_ASC($arrTodosPadres=array())
	{
		try {
			if(empty($arrTodosPadres)){ return null; }
			$arrOrdenado = array();

	        for ($i=count($arrTodosPadres)-1; $i>=0; $i--) { 
	        	if($i-1>=0) {
		        	$arrTodosPadres[$i-1]["cursodetalle_padre"] = $arrTodosPadres[$i];
	        	} else {
	        		$arrOrdenado = $arrTodosPadres[$i];
	        	}
	        	unset($arrTodosPadres[$i]);
	        }

			return $arrOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function ordenarCursoDet_DESC($arrTodosPadres=array())
	{
		try {
			if(empty($arrTodosPadres)){ return null; }
			$arrOrdenado = array();

	        for ($i=0; $i<count($arrTodosPadres); $i++) { 
	        	if($i+1<count($arrTodosPadres)) {
		        	$arrTodosPadres[$i+1]["cursodetalle_hijo"] = $arrTodosPadres[$i];
	        	} else {
	        		$arrOrdenado = $arrTodosPadres[$i];
	        	}
	        }

			return $arrOrdenado;
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar2($filtros=array())
	{
		try {
			return $this->oDatAcad_cursodetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatAcad_cursodetalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getNivelPadre($curso_det)
	{
		try {
			if(empty($curso_det)){throw new Exception(JrTexto::_('curso_det missing')); }
			$response = array();
			$padre = $this->oDatAcad_cursodetalle->get($curso_det['idpadre']);
			$nivel_padre = $this->oDatNiveles->get($padre['idrecurso']);
			if(!empty($padre) && !empty($nivel_padre)) {
				$response = array_merge($padre, $nivel_padre);
				$response["idpadre"] = $padre["idpadre"];
			}
			return $response;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_cursodetalle->get($this->idcursodetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregaromodificar($id, $nombre='',$idrecursoPadre='',$idpersonal='',$imagen='',$descripcion='',$ordenrecurso=0,$iscloonado=false){
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$nivel=$this->oDatNiveles->insertoupdate($this->idrecurso, $nombre,$this->tiporecurso,$idrecursoPadre,$idpersonal,1,$ordenrecurso,$imagen,$descripcion);
			$idrecurso=$nivel["idnivel"];
			$cursodetalle=$this->oDatAcad_cursodetalle->insertoupdate($this->idcursodetalle,$this->idcurso,$this->orden,$idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');
			$menu=array_merge($nivel,$cursodetalle);
			return $menu;
			}catch(Exception $e){
		    	$this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$this->oDatAcad_cursodetalle->iniciarTransaccion('neg_i_Acad_cursodetalle');
			$this->idcursodetalle = $this->oDatAcad_cursodetalle->insertar($this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
			$this->oDatAcad_cursodetalle->terminarTransaccion('neg_i_Acad_cursodetalle');	
			return $this->idcursodetalle;
		} catch(Exception $e) {	
		    $this->oDatAcad_cursodetalle->cancelarTransaccion('neg_i_Acad_cursodetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function agregarvarios($idcurso,$detallecursos,$idpadre=0){
		try {
			$univel=$uunidad=$ulession=$idpadre=$id=$idpadre;			
			foreach($detallecursos as $k=>$v){
				$tipo=$v->tipo;
				$idpadre=($tipo=='N')?0:(($tipo=='U')?$univel:$uunidad);
				$orden=$this->getMaxorden($idcurso,$idpadre);
				$orden++;
				$nombre=$v->nombre;
				$idrecurso=$v->idnivel;
				$color=!empty($v->color)?$v->color:'';
				$esfinal=!empty($v->esfinal)?$v->esfinal:'';
				$id=$this->oDatAcad_cursodetalle->insertar($idcurso,$orden,$idrecurso,$tipo,0,'',$idpadre,$color,$esfinal);
				if($tipo=='N'){
					$univel=$id;
				}elseif($tipo=='U'){
					$uunidad=$id;
				}
				
			}
		} catch(Exception $e) {			    		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			return $this->oDatAcad_cursodetalle->actualizar($this->idcursodetalle,$this->idcurso,$this->orden,$this->idrecurso,$this->tiporecurso,$this->idlogro,$this->url,$this->idpadre,$this->color,$this->esfinal);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatAcad_cursodetalle->cambiarvalorcampo($this->idcursodetalle,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar($idpadre)
	{
		try{				
			$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$idpadre));
			if(!empty($datos2)){
				foreach ($datos2 as $v1){
					$datos2=$this->oDatAcad_cursodetalle->buscar(array("idpadre"=>$v1["idcursodetalle"]));
					if(!empty($datos2)){
						foreach ($datos2 as $dt2){					
							$this->oDatAcad_cursodetalle->eliminar2("idpadre",$dt2["idcursodetalle"]);
						}
					}
					$this->oDatAcad_cursodetalle->eliminar2("idpadre",$v1["idcursodetalle"]);
				}
				$this->oDatAcad_cursodetalle->eliminar2("idpadre",$idpadre);	
			}				
			return $this->oDatAcad_cursodetalle->eliminar2("idcursodetalle",$idpadre);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function setIdcursodetalle($pk){
		try {
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)){throw new Exception(JrTexto::_("Acad_cursodetalle").' '.JrTexto::_("not registered"));}
			$this->idcursodetalle = $this->dataAcad_cursodetalle["idcursodetalle"];
			$this->idcurso = $this->dataAcad_cursodetalle["idcurso"];
			$this->orden = $this->dataAcad_cursodetalle["orden"];
			$this->idrecurso = $this->dataAcad_cursodetalle["idrecurso"];
			$this->tiporecurso = $this->dataAcad_cursodetalle["tiporecurso"];
			$this->idlogro = $this->dataAcad_cursodetalle["idlogro"];
			$this->url = $this->dataAcad_cursodetalle["url"];
			$this->idpadre = $this->dataAcad_cursodetalle["idpadre"];
			$this->color = $this->dataAcad_cursodetalle["color"];
			$this->esfinal = $this->dataAcad_cursodetalle["esfinal"];
			//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {			
			$this->dataAcad_cursodetalle = $this->oDatAcad_cursodetalle->get($pk);
			if(empty($this->dataAcad_cursodetalle)){throw new Exception(JrTexto::_("cursodetalle").' '.JrTexto::_("not registered"));}
			return $this->oDatAcad_cursodetalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}		
}