<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTarea', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTarea 
{
	protected $idtarea;
	protected $idnivel;
	protected $idunidad;
	protected $idactividad;
	protected $iddocente;
	protected $idcursodetalle;
	protected $idproyecto;
	protected $asignacion;
	protected $nombre;
	protected $descripcion;
	protected $foto;
	protected $habilidades;
	protected $habilidad_destacada;
	protected $puntajemaximo;
	protected $puntajeminimo;
	protected $estado='NA'; /* NA=No Asignado	|	A=Asignado */
	protected $eliminado=0;
	
	protected $dataTarea;
	protected $oDatTarea;	

	public function __construct()
	{
		$this->oDatTarea = new DatTarea;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTarea->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTarea->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function  actualizartabla($tabla,$campo){
		if(!empty($tabla)&&!empty($campo)){
			$this->setLimite(0, 5000);
		 return $this->oDatTarea->actualizartabla($tabla,$campo);		
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTarea->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTarea->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTarea->get($this->idtarea);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			if(!NegSesion::tiene_acceso('tarea', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatTarea->iniciarTransaccion('neg_i_Tarea');
			$this->idtarea = $this->oDatTarea->insertar($this->idnivel,$this->idunidad,$this->idactividad,$this->iddocente,$this->idcursodetalle,$this->idproyecto,$this->asignacion,$this->nombre,$this->descripcion,$this->foto,$this->habilidades,$this->habilidad_destacada,$this->puntajemaximo,$this->puntajeminimo,$this->estado,$this->eliminado);
			//$this->oDatTarea->terminarTransaccion('neg_i_Tarea');	
			return $this->idtarea;
		} catch(Exception $e) {	
		   //$this->oDatTarea->cancelarTransaccion('neg_i_Tarea');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tarea', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/		
			return $this->oDatTarea->actualizar($this->idtarea,$this->idnivel,$this->idunidad,$this->idactividad,$this->iddocente,$this->idcursodetalle,$this->idproyecto,$this->asignacion,$this->nombre,$this->descripcion,$this->foto,$this->habilidades,$this->habilidad_destacada,$this->puntajemaximo,$this->puntajeminimo,$this->estado,$this->eliminado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatTarea->cambiarvalorcampo($this->idtarea,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Tarea', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea->eliminar($this->idtarea);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}	

	public function eliminar_logica()
	{
		try {
			if(!NegSesion::tiene_acceso('Tarea', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->eliminado = 1;
			return $this->oDatTarea->actualizar($this->idtarea,$this->idnivel,$this->idunidad,$this->idactividad,$this->iddocente,$this->idcursodetalle,$this->idproyecto,$this->asignacion,$this->nombre,$this->descripcion,$this->foto,$this->habilidades,$this->habilidad_destacada,$this->puntajemaximo,$this->puntajeminimo,$this->estado,$this->eliminado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarea($pk){
		try {
			$this->dataTarea = $this->oDatTarea->get($pk);
			if(empty($this->dataTarea)) {
				throw new Exception(JrTexto::_("Tarea").' '.JrTexto::_("not registered"));
			}
			$this->idtarea = $this->dataTarea["idtarea"];
			$this->idnivel = $this->dataTarea["idnivel"];
			$this->idunidad = $this->dataTarea["idunidad"];
			$this->idactividad = $this->dataTarea["idactividad"];
			$this->iddocente = $this->dataTarea["iddocente"];
			$this->idcursodetalle = $this->dataTarea["idcursodetalle"];
			$this->idproyecto = $this->dataTarea["idproyecto"];
			$this->asignacion = $this->dataTarea["asignacion"];
			$this->nombre = $this->dataTarea["nombre"];
			$this->descripcion = $this->dataTarea["descripcion"];
			$this->foto = $this->dataTarea["foto"];
			$this->habilidades = $this->dataTarea["habilidades"];
			$this->habilidad_destacada = $this->dataTarea["habilidad_destacada"];
			$this->puntajemaximo = $this->dataTarea["puntajemaximo"];
			$this->puntajeminimo = $this->dataTarea["puntajeminimo"];
			$this->estado = $this->dataTarea["estado"];
			$this->eliminado = $this->dataTarea["eliminado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('tarea', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataTarea = $this->oDatTarea->get($pk);
			if(empty($this->dataTarea)) {
				throw new Exception(JrTexto::_("Tarea").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTarea->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function xTarea($filtros = array())
	{
		try {
			return $this->oDatTarea->xTarea($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
		
}