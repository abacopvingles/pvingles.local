<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		01-03-2018
 * @copyright	Copyright (C) 01-03-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamen_ubicacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegExamen_ubicacion 
{
	protected $idexamen_ubicacion;
	protected $idrecurso;
	protected $idcurso;
	protected $tipo;
	protected $activo;
	protected $rango_min;
	protected $rango_max;
	protected $idexam_prerequisito;
	
	protected $dataExamen_ubicacion;
	protected $oDatExamen_ubicacion;	

	public function __construct()
	{
		$this->oDatExamen_ubicacion = new DatExamen_ubicacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamen_ubicacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamen_ubicacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$exams_ubicacion = $this->oDatExamen_ubicacion->buscar($filtros);
			$examenes_ubic = array();
			foreach ($exams_ubicacion as $e) {
				$e["nombre"]=ucfirst(JrTexto::_('Exam'));
				$e["imagen"] = '__xRUTABASEx__/static/media/imagenes/examen_default.png';
				$arrContextOptions=array("ssl"=>array( "verify_peer"=>false, "verify_peer_name"=>false, ), ); 
                #echo "<p>Ruta : <b> ". URL_SMARTQUIZ.'service/exam_info?idexam='.$e['idrecurso'].'&pr='.IDPROYECTO ."</b></p>";
				$dataExam = file_get_contents(URL_SMARTQUIZ.'service/exam_info?idexam='.$e['idrecurso'].'&pr='.IDPROYECTO, false, stream_context_create($arrContextOptions));
                $dataExam = json_decode($dataExam, true);
                if($dataExam['code']==200){
                    $e["nombre"] = $dataExam['data']['titulo'];
                    $e["imagen"] = $dataExam['data']['portada'];
                }
                $examenes_ubic[] = $e;
			}
			return $examenes_ubic;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatExamen_ubicacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamen_ubicacion->get($this->idexamen_ubicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatExamen_ubicacion->iniciarTransaccion('neg_i_Examen_ubicacion');
			$this->idexamen_ubicacion = $this->oDatExamen_ubicacion->insertar($this->idrecurso,$this->idcurso,$this->tipo,$this->activo,$this->rango_min,$this->rango_max,$this->idexam_prerequisito);
			$this->oDatExamen_ubicacion->terminarTransaccion('neg_i_Examen_ubicacion');	
			return $this->idexamen_ubicacion;
		} catch(Exception $e) {	
		    $this->oDatExamen_ubicacion->cancelarTransaccion('neg_i_Examen_ubicacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatExamen_ubicacion->actualizar($this->idexamen_ubicacion,$this->idrecurso,$this->idcurso,$this->tipo,$this->activo,$this->rango_min,$this->rango_max,$this->idexam_prerequisito);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Examen_ubicacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatExamen_ubicacion->eliminar($this->idexamen_ubicacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexamen_ubicacion($pk){
		try {
			$this->dataExamen_ubicacion = $this->oDatExamen_ubicacion->get($pk);
			if(empty($this->dataExamen_ubicacion)) {
				throw new Exception(JrTexto::_("Examen_ubicacion").' '.JrTexto::_("not registered"));
			}
			$this->idexamen_ubicacion = $this->dataExamen_ubicacion["idexamen_ubicacion"];
			$this->idrecurso = $this->dataExamen_ubicacion["idrecurso"];
			$this->idcurso = $this->dataExamen_ubicacion["idcurso"];
			$this->tipo = $this->dataExamen_ubicacion["tipo"];
			$this->activo = $this->dataExamen_ubicacion["activo"];
			$this->rango_min = $this->dataExamen_ubicacion["rango_min"];
			$this->rango_max = $this->dataExamen_ubicacion["rango_max"];
			$this->idexam_prerequisito = $this->dataExamen_ubicacion["idexam_prerequisito"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataExamen_ubicacion = $this->oDatExamen_ubicacion->get($pk);
			if(empty($this->dataExamen_ubicacion)) {
				throw new Exception(JrTexto::_("Examen_ubicacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatExamen_ubicacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}