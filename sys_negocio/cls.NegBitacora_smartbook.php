<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		11-11-2017
 * @copyright	Copyright (C) 11-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBitacora_smartbook', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBitacora_smartbook 
{
	protected $idbitacora;
	protected $idbitacora_alum_smartbook;
	protected $idcurso;
	protected $idsesion;
	protected $idsesionB;
	protected $idusuario;
	protected $pestania;
	protected $total_pestanias;
	protected $fechahora;
	protected $progreso;
	protected $otros_datos;
	
	protected $dataBitacora_smartbook;
	protected $oDatBitacora_smartbook;	

	public function __construct()
	{
		$this->oDatBitacora_smartbook = new DatBitacora_smartbook;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBitacora_smartbook->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBitacora_smartbook->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getProgresoPromedio($filtros = array())
	{
		try {
			#return $this->oDatBitacora_smartbook->getProgresoPromedio($filtros);
			//$progreso = 0;
			$res = $this->oDatBitacora_smartbook->getSumaProgresos2($filtros);
			$sump=0;
			if(!empty($res[0])){
				$total_pestanias=(float)$res[0]["total_pestanias"];
				$sumprogreso=(float)$res[0]["sumprogreso"];
				$sump=$sumprogreso>0?($sumprogreso/$total_pestanias):0.0;
				$sump=$sump>100?100:$sump;
			}
			return round($sump,2);			
			/*$bitacoras = $this->oDatBitacora_smartbook->buscar($filtros);		
			if(!empty($bitacoras)){ 
				$total_pestanias= (int)$bitacoras[0]['total_pestanias']; 
				if($total_pestanias!=0){ $progreso=$suma/$total_pestanias;}
				$progreso=$progreso>=100?100:$progreso;
			}*/
			//return $progreso;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBitacora_smartbook->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBitacora_smartbook->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBitacora_smartbook->get($this->idbitacora);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_smartbook', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatBitacora_smartbook->iniciarTransaccion('neg_i_Bitacora_smartbook');
			$this->idbitacora = $this->oDatBitacora_smartbook->insertar(
				$this->idcurso,
				$this->idsesion,
				$this->idsesionB,
				$this->idusuario,
				$this->idbitacora_alum_smartbook,
				$this->pestania,
				$this->total_pestanias,
				$this->fechahora,
				$this->progreso,
				$this->otros_datos
			);
			$this->actualizarTotalPestanias();
			$this->oDatBitacora_smartbook->terminarTransaccion('neg_i_Bitacora_smartbook');	
			return $this->idbitacora;
		} catch(Exception $e) {	
		    $this->oDatBitacora_smartbook->cancelarTransaccion('neg_i_Bitacora_smartbook');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_smartbook', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			$this->idbitacora=$this->oDatBitacora_smartbook->actualizar(
				$this->idbitacora,
				$this->idcurso,
				$this->idsesion,
				$this->idsesionB,
				$this->idusuario,
				$this->idbitacora_alum_smartbook,
				$this->pestania,
				$this->total_pestanias,
				$this->fechahora,
				$this->progreso,
				$this->otros_datos
			);
			$this->actualizarTotalPestanias();
			return $this->idbitacora;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function actualizarTotalPestanias()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_smartbook', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/
			$bitacoras = $this->oDatBitacora_smartbook->buscar(array("idbitacora_alum_smartbook"=>$this->idbitacora_alum_smartbook));
			if(!empty($bitacoras)){
				foreach ($bitacoras as $i => $b) {
					$this->oDatBitacora_smartbook->set($b['idbitacora'], 'total_pestanias', $this->total_pestanias);
				}
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bitacora_smartbook', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBitacora_smartbook->eliminar($this->idbitacora);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdbitacora($pk){
		try {
			$this->dataBitacora_smartbook = $this->oDatBitacora_smartbook->get($pk);
			if(empty($this->dataBitacora_smartbook)) {
				throw new Exception(JrTexto::_("Bitacora_smartbook").' '.JrTexto::_("not registered"));
			}
			$this->idbitacora = $this->dataBitacora_smartbook["idbitacora"];
			$this->idcurso = $this->dataBitacora_smartbook["idcurso"];
			$this->idsesion = $this->dataBitacora_smartbook["idsesion"];
			$this->idusuario = $this->dataBitacora_smartbook["idusuario"];
			$this->idbitacora_alum_smartbook = $this->dataBitacora_smartbook["idbitacora_alum_smartbook"];
			$this->pestania = $this->dataBitacora_smartbook["pestania"];
			$this->total_pestanias = $this->dataBitacora_smartbook["total_pestanias"];
			$this->fechahora = $this->dataBitacora_smartbook["fechahora"];
			$this->progreso = $this->dataBitacora_smartbook["progreso"];
			$this->otros_datos = $this->dataBitacora_smartbook["otros_datos"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bitacora_smartbook', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBitacora_smartbook = $this->oDatBitacora_smartbook->get($pk);
			if(empty($this->dataBitacora_smartbook)) {
				throw new Exception(JrTexto::_("Bitacora_smartbook").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBitacora_smartbook->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}