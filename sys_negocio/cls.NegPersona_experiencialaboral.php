<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-01-2018
 * @copyright	Copyright (C) 04-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_experiencialaboral', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_experiencialaboral 
{
	protected $idexperiencia;
	protected $idpersona;
	protected $empresa;
	protected $rubro;
	protected $area;
	protected $cargo;
	protected $funciones;
	protected $fechade;
	protected $fechahasta;
	protected $actualmente;
	protected $mostrar;
	
	protected $dataPersona_experiencialaboral;
	protected $oDatPersona_experiencialaboral;	

	public function __construct()
	{
		$this->oDatPersona_experiencialaboral = new DatPersona_experiencialaboral;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_experiencialaboral->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_experiencialaboral->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_experiencialaboral->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_experiencialaboral->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_experiencialaboral->get($this->idexperiencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_experiencialaboral', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_experiencialaboral->iniciarTransaccion('neg_i_Persona_experiencialaboral');
			$this->idexperiencia = $this->oDatPersona_experiencialaboral->insertar($this->idpersona,$this->empresa,$this->rubro,$this->area,$this->cargo,$this->funciones,$this->fechade,$this->fechahasta,$this->actualmente,$this->mostrar);
			$this->oDatPersona_experiencialaboral->terminarTransaccion('neg_i_Persona_experiencialaboral');	
			return $this->idexperiencia;
		} catch(Exception $e) {	
		    $this->oDatPersona_experiencialaboral->cancelarTransaccion('neg_i_Persona_experiencialaboral');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_experiencialaboral', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_experiencialaboral->actualizar($this->idexperiencia,$this->idpersona,$this->empresa,$this->rubro,$this->area,$this->cargo,$this->funciones,$this->fechade,$this->fechahasta,$this->actualmente,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_experiencialaboral->cambiarvalorcampo($this->idexperiencia,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_experiencialaboral', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_experiencialaboral->eliminar($this->idexperiencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexperiencia($pk){
		try {
			$this->dataPersona_experiencialaboral = $this->oDatPersona_experiencialaboral->get($pk);
			if(empty($this->dataPersona_experiencialaboral)) {
				throw new Exception(JrTexto::_("Persona_experiencialaboral").' '.JrTexto::_("not registered"));
			}
			$this->idexperiencia = $this->dataPersona_experiencialaboral["idexperiencia"];
			$this->idpersona = $this->dataPersona_experiencialaboral["idpersona"];
			$this->empresa = $this->dataPersona_experiencialaboral["empresa"];
			$this->rubro = $this->dataPersona_experiencialaboral["rubro"];
			$this->area = $this->dataPersona_experiencialaboral["area"];
			$this->cargo = $this->dataPersona_experiencialaboral["cargo"];
			$this->funciones = $this->dataPersona_experiencialaboral["funciones"];
			$this->fechade = $this->dataPersona_experiencialaboral["fechade"];
			$this->fechahasta = $this->dataPersona_experiencialaboral["fechahasta"];
			$this->actualmente = $this->dataPersona_experiencialaboral["actualmente"];
			$this->mostrar = $this->dataPersona_experiencialaboral["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_experiencialaboral', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_experiencialaboral = $this->oDatPersona_experiencialaboral->get($pk);
			if(empty($this->dataPersona_experiencialaboral)) {
				throw new Exception(JrTexto::_("Persona_experiencialaboral").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_experiencialaboral->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}