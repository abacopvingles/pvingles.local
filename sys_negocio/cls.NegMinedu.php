<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-01-2018
 * @copyright	Copyright (C) 31-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMinedu', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatAcad_curso', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatMin_grado', RUTA_BASE, 'sys_datos');

class NegMinedu{
	protected $oDatMinedu;
	protected $oDatCurso;
	protected $oDatMin_grado;
	
    public function __construct()
	{
		$this->oDatMinedu = new DatMinedu;
		$this->oDatCurso = new DatAcad_curso;
		$this->oDatMin_grado = new DatMin_grado;
    }
	
    public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
    }
    public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
    }
    
    private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
    }
    public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMinedu->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	////////// Fin - Metodos magicos //////////
    
    public function buscarubigeo($filtros = null){
        try {
			$this->setLimite(0,10000);
			return $this->oDatMinedu->buscarubigeo($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscarubigeougel($filtros = null){
        try {
			$this->setLimite(0,10000);
			return $this->oDatMinedu->buscarubigeougel($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function unique_multidim_array($array, $key) { 
		$temp_array = array(); 
		$i = 0; 
		$key_array = array(); 
		
		foreach($array as $val) { 
			if (!in_array($val[$key], $key_array)) { 
				$key_array[$i] = $val[$key]; 
				$temp_array[$i] = $val; 
			} 
			$i++; 
		} 
		return $temp_array; 
	} 

	public function getdocente($idlocal){
		try{
			$docinfo = array();
			$docentes = array();
			$filtros = array('idlocal'=>$idlocal);
			$this->setLimite(0,10000);
			$result = $this->oDatMinedu->buscardocentes($filtros);
			if(!empty($result)){
				$cursos = $this->oDatCurso->buscar(array('estado'=>1, "idproyecto"=>'3'));
				$grados = $this->oDatMin_grado->buscar();

				if(!empty($cursos)){
					for($i = 0; $i < count($grados); $i++){

					}
					$info = array();
					foreach($cursos as $k => $v){
						$_idcurso = $v['idcurso']; 
						$docinfo[$_idcurso]=$grados;
						$search_docxcurso = array_keys(array_column($result,'idcurso'),$v['idcurso']);
						foreach($search_docxcurso as $sd){
							foreach($grados as $kg => $g){
								if(intval($g['idgrado']) == intval($result[$sd]['idgrado'])){
									$info[$_idcurso][$g['idgrado']][] = array('nombre'=>$result[$sd]['nombredocente'], 'id'=> $result[$sd]['iddocente']);
									$info[$_idcurso][$g['idgrado']] = $this->unique_multidim_array($info[$v['idcurso']][$g['idgrado']],'nombre');
									break;
								}
							}

						}
						foreach($info[$_idcurso] as $ki => $vi){
							foreach($vi as $vik => $viv){
								foreach($search_docxcurso as $sd){
									if( intval($ki) == intval($result[$sd]['idgrado']) && intval($viv['id']) == intval($result[$sd]['iddocente'])){
										$info[$_idcurso][$ki][$vik]['secciones'][$result[$sd]['idsesion']] = $result[$sd]['idgrupoauladetalle'];
									}
								}
							}
						}

						$docentes[$v['idcurso']] = null;
						$search_docxcurso = array_keys(array_column($result,'idcurso'),$v['idcurso']);
						if(!empty($search_docxcurso)){
							foreach($search_docxcurso as $key){
								$docentes[$v['idcurso']][] = $result[$key];
							}
						}
						
					}
						
				}
			}
			// var_dump($info);

			return $info;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscarentradasalida($filtros){
		try{
			$this->setLimite(0,100000);
			return $this->oDatMinedu->buscarentradasalida($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function GetnumRow(){
		try{
			return $this->oDatMinedu->GetnumRow();
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscartiempos($filtros = null){
		try{
			$this->setLimite(0,10000);
			return $this->oDatMinedu->buscartiempos($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscariiee($filtros = null){
        try {
			$this->setLimite(0,10000);
			return $this->oDatMinedu->buscariiee($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function buscar_competencias($filtros){
		try{
			$this->setLimite(0,10000);
			return $this->oDatMinedu->competencias($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function competencias(){
		try{
			$this->setLimite(0,10000);
			return $this->oDatMinedu->competencias(array('tipo' => 'CM', 'idpadre' => 0));
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function capacidades($idcompetencia){
		try{
			$this->setLimite(0,10000);
			return $this->oDatMinedu->competencias(array('tipo' => 'CA', 'idpadre' => $idcompetencia));
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function buscarexamen($filtros = null){
        try {
			$this->setLimite(0,10000);
			return $this->oDatMinedu->buscarexamen($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function dre_ubicacion(){
		/**
		 * select count(ma.idmatricula) from ugel u inner join local l on u.idugel = l.idugel inner join acad_grupoauladetalle gad on gad.idlocal = l.idlocal inner join acad_matricula ma on gad.idgrupoauladetalle= ma.idgrupoauladetalle
		 * where u.iddepartamento = '230000';

		 * select sum(nq.nota) from ugel u inner join local l on u.idugel = l.idugel inner join acad_grupoauladetalle gad on gad.idlocal = l.idlocal inner join acad_matricula ma on gad.idgrupoauladetalle= ma.idgrupoauladetalle inner join notas_quiz nq on nq.idalumno = ma.idalumno
 		 * where u.iddepartamento = '230000' and nq.tipo = 'E' and nq.datos like '%"tipo":"E"%';
			
		 * Ubicar todos los ugeles con el datos de ugel
		 * buscar por colegio
		 */
	}
	public function buscarbimestre($filtros = null){
		try {
			$this->setLimite(0,100000);
			return $this->oDatMinedu->buscarbimestre($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	// public function getubicacionxseccion_all($filtros = null){
	// 	try {
	// 		$resultado = array();
	// 		$this->setLimite(0,300000);
	// 		$result01 = $this->oDatMinedu->notasUbicacionxSeccion($filtros);
	// 		if(!empty($result01)){
	// 			$result02 = array();
	// 			if(!isset($filtros['idgrupoauladetalle'])){
	// 				$result02 = $this->o
	// 			}
	// 		}
	// 		return $resultado;
	// 	}catch(Exception $e) {
	// 		throw new Exception($e->getMessage());
	// 	}
	// }
	public function buscartrimestre($filtros){
		try {
			$this->setLimite(0,100000);
			return $this->oDatMinedu->buscartrimestre($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function getrecursos($curso = null,$filtros = null){
		try{
			return $this->oDatMinedu->recursos($curso,$filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function cantsesiones($filtros = null){
		try{
			$this->setLimite(0,100000);
			return $this->oDatMinedu->cantsesiones($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function matriculalocal($filtros = null){
		try{
			$this->setLimite(0,400000);
			return $this->oDatMinedu->matriculalocal($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function actividades($filtros = null){
		try{
			$this->setLimite(0,400000);
			return $this->oDatMinedu->actividades($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function matriculalocalxcurso($idlocal){
		try{
			$contador = array('A1'=>0,'A2'=>0,'B1'=>0,'B2'=>0,'C1'=>0);
			if($resultado = $this->oDatMinedu->countmatriculalocal(array('idlocal'=>$idlocal,'idcurso' => 31))){
				$contador['A1'] = $resultado[0]['total'];
			}
			if($resultado = $this->oDatMinedu->countmatriculalocal(array('idlocal'=>$idlocal,'idcurso' => 35))){
				$contador['A2'] = $resultado[0]['total'];
			}
			if($resultado = $this->oDatMinedu->countmatriculalocal(array('idlocal'=>$idlocal,'idcurso' => 36))){
				$contador['B1'] = $resultado[0]['total'];
			}
			if($resultado = $this->oDatMinedu->countmatriculalocal(array('idlocal'=>$idlocal,'idcurso' => 4))){
				$contador['B2'] = $resultado[0]['total'];
			}
			if($resultado = $this->oDatMinedu->countmatriculalocal(array('idlocal'=>$idlocal,'idcurso' => 5))){
				$contador['C1'] = $resultado[0]['total'];
			}
			return $contador;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function progresosresumen($filtros = null){
		try{
			return $this->oDatMinedu->progresosresumen($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function progresohabilidad($filtros = null){
		try{
			$this->setLimite(0,400000);
			return $this->oDatMinedu->progresohabilidad($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function getprogresolocal($filtros = null){
		try{
			$this->setLimite(0,100000);
			return $this->oDatMinedu->progresolocal($filtros);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	public function habilidadCompetencia_unidad($idcurso,$idrecurso,$idalumno){
		try{
			$resultado = array();
			if(!is_null($idcurso)){
				$filtros['idcurso'] = $idcurso;
			}
			if(!is_null($idrecurso)){
				$filtros['idrecurso'] = $idrecurso;
			}
			if(!is_null($idalumno)){
				$filtros['idalumno'] = $idalumno;
			}
			$filtros['habilidad'] = 4;
			$total_listen = $this->oDatMinedu->buscarTotalProgresoxUnidad($filtros);
			$now_listen = $this->oDatMinedu->buscarActualProgresoxUnidad($filtros);
			$filtros['habilidad'] = 5;
			$total_read = $this->oDatMinedu->buscarTotalProgresoxUnidad($filtros);
			$now_read = $this->oDatMinedu->buscarActualProgresoxUnidad($filtros);
			$filtros['habilidad'] = 6;
			$total_write = $this->oDatMinedu->buscarTotalProgresoxUnidad($filtros);
			$now_write = $this->oDatMinedu->buscarActualProgresoxUnidad($filtros);
			$filtros['habilidad'] = 7;
			$total_speak = $this->oDatMinedu->buscarTotalProgresoxUnidad($filtros);
			$now_speak = $this->oDatMinedu->buscarActualProgresoxUnidad($filtros);

			$r_l = (!empty($now_listen)) ? 	round(( $now_listen[0]['total'] * 100) / $total_listen[0]['total'],2) : 0;
			$r_r = (!empty($now_read)) 	?	round(( $now_read[0]['total'] * 100) / $total_read[0]['total'],2) : 0;
			$r_w = (!empty($now_write)) ? 	round(( $now_write[0]['total'] * 100) / $total_write[0]['total'],2)  : 0;
			$r_s = (!empty($now_speak)) ? 	round(( $now_speak[0]['total'] * 100) / $total_speak[0]['total'],2)  : 0;

			$resultado = array(
				'4' =>$r_l,
				'5'	=>$r_r,
				'6' =>$r_w,
				'7' =>$r_s
			);

			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}
	
}
?>