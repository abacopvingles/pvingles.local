<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		31-05-2017
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTarea_respuesta', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatTarea_archivos', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTarea_respuesta 
{
	protected $idtarea_respuesta;
	protected $idtarea_asignacion_alumno;
	protected $comentario;
	protected $fechapresentacion;
	protected $horapresentacion;
	protected $idpersona;
	protected $idgrupotarea;
	
	protected $dataTarea_respuesta;
	protected $oDatTarea_respuesta;	
	protected $oDatTarea_archivos;	

	public function __construct()
	{
		$this->oDatTarea_respuesta = new DatTarea_respuesta;
		$this->oDatTarea_archivos = new DatTarea_archivos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTarea_respuesta->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTarea_respuesta->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTarea_respuesta->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscarConArchivos($filtros = array(), $filtrosDetalle = array())
	{
		try {
			$tar_rsptas = $this->oDatTarea_respuesta->buscar($filtros);
			$i=0;
			foreach ($tar_rsptas as $tr) {
				$filtrosDet = array_merge($filtrosDetalle, array('tablapadre'=>'R', 'idpadre'=>$tr['idtarea_respuesta']));
				$tar_rsptas[$i]['respuesta_archivos'] = $this->oDatTarea_archivos->buscar($filtrosDet);
			}
			return $tar_rsptas;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTarea_respuesta->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTarea_respuesta->get($this->idtarea_respuesta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('tarea_respuesta', 'add') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			//$this->oDatTarea_respuesta->iniciarTransaccion('neg_i_Tarea_respuesta');
			$this->idtarea_respuesta = $this->oDatTarea_respuesta->insertar($this->idtarea_asignacion_alumno,$this->comentario,$this->fechapresentacion,$this->horapresentacion,$this->idpersona,$this->idgrupotarea);
			//$this->oDatTarea_respuesta->terminarTransaccion('neg_i_Tarea_respuesta');	
			return $this->idtarea_respuesta;
		} catch(Exception $e) {	
		   //$this->oDatTarea_respuesta->cancelarTransaccion('neg_i_Tarea_respuesta');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('tarea_respuesta', 'edit') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatTarea_respuesta->actualizar($this->idtarea_respuesta,$this->idtarea_asignacion_alumno,$this->comentario,$this->fechapresentacion,$this->horapresentacion,$this->idpersona,$this->idgrupotarea);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Tarea_respuesta', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea_respuesta->eliminar($this->idtarea_respuesta);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtarea_respuesta($pk){
		try {
			$this->dataTarea_respuesta = $this->oDatTarea_respuesta->get($pk);
			if(empty($this->dataTarea_respuesta)) {
				throw new Exception(JrTexto::_("Tarea_respuesta").' '.JrTexto::_("not registered"));
			}
			$this->idtarea_respuesta = $this->dataTarea_respuesta["idtarea_respuesta"];
			$this->idtarea_asignacion_alumno = $this->dataTarea_respuesta["idtarea_asignacion_alumno"];
			$this->comentario = $this->dataTarea_respuesta["comentario"];
			$this->fechapresentacion = $this->dataTarea_respuesta["fechapresentacion"];
			$this->horapresentacion = $this->dataTarea_respuesta["horapresentacion"];
			$this->idpersona = $this->dataTarea_respuesta["idpersona"];
			$this->idgrupotarea = $this->dataTarea_respuesta["idgrupotarea"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('tarea_respuesta', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataTarea_respuesta = $this->oDatTarea_respuesta->get($pk);
			if(empty($this->dataTarea_respuesta)) {
				throw new Exception(JrTexto::_("Tarea_respuesta").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTarea_respuesta->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdtarea_asignacion_alumno($idtarea_asignacion_alumno)
	{
		try {
			$this->idtarea_asignacion_alumno= NegTools::validar('todo', $idtarea_asignacion_alumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setComentario($comentario)
	{
		try {
			$this->comentario= NegTools::validar('todo', $comentario, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setFechapresentacion($fechapresentacion)
	{
		try {
			$this->fechapresentacion= NegTools::validar('todo', $fechapresentacion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 16));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setHorapresentacion($horapresentacion)
	{
		try {
			$this->horapresentacion= NegTools::validar('todo', $horapresentacion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 16));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}