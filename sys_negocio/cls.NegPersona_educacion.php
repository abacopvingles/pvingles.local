<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		03-01-2018
 * @copyright	Copyright (C) 03-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_educacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_educacion 
{
	protected $ideducacion;
	protected $idpersona;
	protected $institucion;
	protected $tipodeestudio;
	protected $titulo;
	protected $areaestudio;
	protected $situacion;
	protected $fechade;
	protected $fechahasta;
	protected $actualmente;
	protected $mostrar;
	
	protected $dataPersona_educacion;
	protected $oDatPersona_educacion;	

	public function __construct()
	{
		$this->oDatPersona_educacion = new DatPersona_educacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_educacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_educacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_educacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_educacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_educacion->get($this->ideducacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_educacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_educacion->iniciarTransaccion('neg_i_Persona_educacion');
			$this->ideducacion = $this->oDatPersona_educacion->insertar($this->idpersona,$this->institucion,$this->tipodeestudio,$this->titulo,$this->areaestudio,$this->situacion,$this->fechade,$this->fechahasta,$this->actualmente,$this->mostrar);
			$this->oDatPersona_educacion->terminarTransaccion('neg_i_Persona_educacion');	
			return $this->ideducacion;
		} catch(Exception $e) {	
		    $this->oDatPersona_educacion->cancelarTransaccion('neg_i_Persona_educacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_educacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_educacion->actualizar($this->ideducacion,$this->idpersona,$this->institucion,$this->tipodeestudio,$this->titulo,$this->areaestudio,$this->situacion,$this->fechade,$this->fechahasta,$this->actualmente,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_educacion->cambiarvalorcampo($this->ideducacion,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_educacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_educacion->eliminar($this->ideducacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdeducacion($pk){
		try {
			$this->dataPersona_educacion = $this->oDatPersona_educacion->get($pk);
			if(empty($this->dataPersona_educacion)) {
				throw new Exception(JrTexto::_("Persona_educacion").' '.JrTexto::_("not registered"));
			}
			$this->ideducacion = $this->dataPersona_educacion["ideducacion"];
			$this->idpersona = $this->dataPersona_educacion["idpersona"];
			$this->institucion = $this->dataPersona_educacion["institucion"];
			$this->tipodeestudio = $this->dataPersona_educacion["tipodeestudio"];
			$this->titulo = $this->dataPersona_educacion["titulo"];
			$this->areaestudio = $this->dataPersona_educacion["areaestudio"];
			$this->situacion = $this->dataPersona_educacion["situacion"];
			$this->fechade = $this->dataPersona_educacion["fechade"];
			$this->fechahasta = $this->dataPersona_educacion["fechahasta"];
			$this->actualmente = $this->dataPersona_educacion["actualmente"];
			$this->mostrar = $this->dataPersona_educacion["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_educacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_educacion = $this->oDatPersona_educacion->get($pk);
			if(empty($this->dataPersona_educacion)) {
				throw new Exception(JrTexto::_("Persona_educacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_educacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}