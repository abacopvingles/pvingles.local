<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-12-2017
 * @copyright	Copyright (C) 04-12-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAsistencia', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAsistencia 
{
	protected $idasistencia;
	protected $idgrupoauladetalle;
	protected $idalumno;
	protected $fecha;
	protected $estado;
	protected $observacion;
	protected $regusuario;
	protected $regfecha;
	
	protected $dataAsistencia;
	protected $oDatAsistencia;	

	public function __construct()
	{
		$this->oDatAsistencia = new DatAsistencia;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAsistencia->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAsistencia->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatAsistencia->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAsistencia->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAsistencia->get($this->idasistencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('asistencia', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAsistencia->iniciarTransaccion('neg_i_Asistencia');
			$this->idasistencia = $this->oDatAsistencia->insertar($this->idgrupoauladetalle,$this->idalumno,$this->fecha,$this->estado,$this->observacion,$this->regusuario,$this->regfecha);
			$this->oDatAsistencia->terminarTransaccion('neg_i_Asistencia');	
			return $this->idasistencia;
		} catch(Exception $e) {	
		    $this->oDatAsistencia->cancelarTransaccion('neg_i_Asistencia');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('asistencia', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}	*/		
			return $this->oDatAsistencia->actualizar($this->idasistencia,$this->idgrupoauladetalle,$this->idalumno,$this->fecha,$this->estado,$this->observacion,$this->regusuario,$this->regfecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Asistencia', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatAsistencia->eliminar($this->idasistencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdasistencia($pk){
		try {
			$this->dataAsistencia = $this->oDatAsistencia->get($pk);
			if(empty($this->dataAsistencia)) {
				throw new Exception(JrTexto::_("Asistencia").' '.JrTexto::_("not registered"));
			}
			$this->idasistencia = $this->dataAsistencia["idasistencia"];
			$this->idgrupoauladetalle = $this->dataAsistencia["idgrupoauladetalle"];
			$this->idalumno = $this->dataAsistencia["idalumno"];
			$this->fecha = $this->dataAsistencia["fecha"];
			$this->estado = $this->dataAsistencia["estado"];
			$this->observacion = $this->dataAsistencia["observacion"];
			$this->regusuario = $this->dataAsistencia["regusuario"];
			$this->regfecha = $this->dataAsistencia["regfecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('asistencia', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAsistencia = $this->oDatAsistencia->get($pk);
			if(empty($this->dataAsistencia)) {
				throw new Exception(JrTexto::_("Asistencia").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAsistencia->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}