<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-10-2018
 * @copyright	Copyright (C) 16-10-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatMin_unidad_capacidad', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');

class NegMin_unidad_capacidad
{
	protected $id;
	protected $idcurso;
	protected $unidad;
	protected $orden;
    protected $idcapacidad;
    
	protected $dataMin_unidad_capacidad;
	protected $oDatMin_unidad_capacidad;	

	public function __construct()
	{
		$this->oDatMin_unidad_capacidad = new DatMin_unidad_capacidad;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatMin_unidad_capacidad->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatMin_unidad_capacidad->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatMin_unidad_capacidad->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function capacidades(){
		try{
            $resultado = array();
            $this->setLimite(0,1000);
            $capacidades = $this->oDatMin_unidad_capacidad->buscar(array('multi_idcurso' => array(31,35),'estado' => true));

            if(!empty($capacidades)){
                $i = 0;
                $unidad = null;
                do{
                    $idcurso = $capacidades[$i]['idcurso'];
                    if($idcurso == $capacidades[$i]['idcurso'] && strcmp($unidad,$capacidades[$i]['unidad']) != 0 ){
                        $prepare_capacidad = array();
                        
                        $intentos = 0;
                        foreach($capacidades as $value){
                            if($value['idcurso'] === $capacidades[$i]['idcurso'] && $value['orden'] === $capacidades[$i]['orden'] && strcmp($value['unidad'],$capacidades[$i]['unidad']) == 0){
                                
                                $intentos = 0;
                                $prepare_capacidad[] = array(
                                    'idcapacidad' => $value['idcapacidad'],
                                    'nombre' => $value['titulo'],
                                    'idcompetencia' => $value['idcompetencia']
                                );
                            }
                        }
                        $resultado[] = array(
							'idcurso' => $capacidades[$i]['idcurso'],
							'idrecurso' => $capacidades[$i]['idcursodetalle'],
                            'unidad' => $capacidades[$i]['nombre_unidad'],
                            'orden' => $capacidades[$i]['orden'],
                            'capacidades' => $prepare_capacidad
                        );
                    }
                    
                    $idcurso = $capacidades[$i]['idcurso'];
                    $unidad = $capacidades[$i]['unidad'];
                    ++$i;
                }while($i < count($capacidades));
            }
			return $resultado;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}
	}

	// public function listar()
	// {
	// 	try {
	// 		return $this->oDatMin_unidad_capacidad->listarall();
	// 	} catch(Exception $e) {
	// 		throw new Exception($e->getMessage());
	// 	}
	// }

	// public function getXid()
	// {
	// 	try {
	// 		return $this->oDatMin_unidad_capacidad->get($this->id);
	// 	} catch(Exception $e) {
	// 		throw new Exception($e->getMessage());
	// 	}
	// }

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatMin_unidad_capacidad->iniciarTransaccion('neg_i_Min_unidad_capacidad');
			$this->id = $this->oDatMin_unidad_capacidad->insertar($this->idcurso,$this->unidad,$this->orden,$this->idcapacidad);
			$this->oDatMin_unidad_capacidad->terminarTransaccion('neg_i_Min_unidad_capacidad');	
			return $this->id;
		} catch(Exception $e) {	
		    $this->oDatMin_unidad_capacidad->cancelarTransaccion('neg_i_Min_unidad_capacidad');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatMin_unidad_capacidad->actualizar($this->id,$this->idcurso,$this->unidad,$this->orden,$this->idcapacidad);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Min_competencias', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatMin_unidad_capacidad->eliminar($this->idcompetencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdUnidadCapacidad($pk){
		try {
			$this->dataMin_competencias = $this->oDatMin_unidad_capacidad->get($pk);
			if(empty($this->dataMin_unidad_capacidad)) {
				throw new Exception(JrTexto::_("Min_unidad_capacidad").' '.JrTexto::_("not registered"));
			}
			$this->idcompetencia = $this->dataMin_unidad_capacidad["idcompetencia"];
			$this->titulo = $this->dataMin_unidad_capacidad["titulo"];
			$this->descripcion = $this->dataMin_unidad_capacidad["descripcion"];
			$this->estado = $this->dataMin_unidad_capacidad["estado"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('min_competencias', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataMin_competencias = $this->oDatMin_unidad_capacidad->get($pk);
			if(empty($this->dataMin_unidad_capacidad)) {
				throw new Exception(JrTexto::_("Min_unidad_capacidad").' '.JrTexto::_("not registered"));
			}

			return $this->oDatMin_unidad_capacidad->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}