<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		06-01-2018
 * @copyright	Copyright (C) 06-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatAcad_grupoaula', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatAcad_grupoauladetalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegAcad_grupoauladetalle 
{
	protected $idgrupoauladetalle;
	protected $idgrupoaula;
	protected $idcurso;
	protected $iddocente;
	protected $idlocal;
	protected $idambiente;
	protected $idgrado;
	protected $idseccion;
	protected $nombre;
	protected $fecha_inicio;
	protected $fecha_final;
	protected $oDatAcad_grupoaula;
	protected $dataAcad_grupoauladetalle;
	protected $oDatAcad_grupoauladetalle;	

	public function __construct()
	{
		$this->oDatAcad_grupoauladetalle = new DatAcad_grupoauladetalle;
		$this->oDatAcad_grupoaula = new DatAcad_grupoaula;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}


	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatAcad_grupoauladetalle->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoauladetalle->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function anios()
	{
		try {
			return $this->oDatAcad_grupoauladetalle->anios();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function mialumnos($filtros = null){
		try {
			$this->setLimite(0,100000);
			return $this->oDatAcad_grupoauladetalle->mialumnos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function micolegio($filtros = array())
	{
		try {
			$dt=$this->oDatAcad_grupoauladetalle->micolegio($filtros);
			$col=$haycol=$iee=array();
			$ies=array();
			if(!empty($dt)){
			$hayie=array();
			$txt='';
			foreach($dt as $ie){
				$iea=array();
				if(!in_array($ie["idlocal"],$hayie)){
					$hayie[]=$ie["idlocal"];
					$iea=array('idlocal'=>$ie["idlocal"],'iiee'=>$ie["striiee"],'idgrupoauladetalle'=>$ie["idgrupoauladetalle"]);
					$haycur=$curs=array();
					foreach($dt as $cu){
						if(!in_array($cu["idcurso"],$haycur) && $ie["idlocal"]==$cu["idlocal"]){
							$haycur[]=$cu["idcurso"];
							$cur=array("idcurso"=>$cu["idcurso"],"strcurso"=>$cu["strcurso"],'idgrupoauladetalle'=>$cu["idgrupoauladetalle"]);
							$haygrad=$grad=array();
							foreach($dt as $gr){
								if(!in_array($gr["idgrado"],$haygrad) && $cu["idlocal"]==$gr["idlocal"] && $cu["idcurso"]==$gr["idcurso"]){
									$haygrad[]=$gr["idgrado"];
									$gra=array("idgrado"=>$gr["idgrado"],'grado'=>$gr["grado"],'idgrupoauladetalle'=>$gr["idgrupoauladetalle"]);
									$haysec=$secs=array();
									foreach($dt as $se){
										if(!in_array($se["idsesion"],$haysec) && $gr["idlocal"]==$se["idlocal"]  && $gr["idcurso"]==$se["idcurso"]  && $gr["idgrado"]==$se["idgrado"]){
											$haysec[]=$se["idsesion"];
											$secs[]=$se;
										}
									}
									$gra["secciones"]=$secs;
									$grad[]=$gra;
								}
							}
							$cur["grados"]=$grad;
							$curs[]=$cur;
						}
					}
					$iea["cursos"]=$curs;
					$ies[]=$iea;
				}
			}			
			}
			return $ies;
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function filtrogrupos($filtros=array())
	{
		try {
			return $this->oDatAcad_grupoauladetalle->filtrogrupos($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function mis_ambientes($filtros=array())
	{
		try {
			return $this->oDatAcad_grupoauladetalle->mis_ambientes($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,100000);
			return $this->oDatAcad_grupoauladetalle->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function cursosDocente($filtros = array())
	{
		try {
			return $this->oDatAcad_grupoauladetalle->cursosDocente($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function vermarcacion($filtros = array())
	{
		try {
			$this->oDatAcad_grupoauladetalle->setLimite(0,1);
			return $this->oDatAcad_grupoauladetalle->vermarcacion($filtros);			
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatAcad_grupoauladetalle->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatAcad_grupoauladetalle->get($this->idgrupoauladetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoauladetalle', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatAcad_grupoauladetalle->iniciarTransaccion('neg_i_Acad_grupoauladetalle');
			$this->idgrupoauladetalle = $this->oDatAcad_grupoauladetalle->insertar($this->idgrupoaula,$this->idcurso,$this->iddocente,$this->idlocal,$this->idambiente,$this->nombre,$this->fecha_inicio,$this->fecha_final,$this->idgrado,$this->idseccion);
			$this->oDatAcad_grupoauladetalle->terminarTransaccion('neg_i_Acad_grupoauladetalle');	
			return $this->idgrupoauladetalle;
		} catch(Exception $e) {	
		    $this->oDatAcad_grupoauladetalle->cancelarTransaccion('neg_i_Acad_grupoauladetalle');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoauladetalle', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatAcad_grupoauladetalle->actualizar($this->idgrupoauladetalle,$this->idgrupoaula,$this->idcurso,$this->iddocente,$this->idlocal,$this->idambiente,$this->nombre,$this->fecha_inicio,$this->fecha_final,$this->idgrado,$this->idseccion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Acad_grupoauladetalle', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$det1=$this->oDatAcad_grupoauladetalle->buscar(array('idgrupoauladetalle'=>$this->idgrupoauladetalle));
			$id=$this->oDatAcad_grupoauladetalle->eliminar($this->idgrupoauladetalle);
			if(!empty($det1[0])){
			    $idgrupoaula=$det1[0]["idgrupoaula"];
			    $det2=$this->oDatAcad_grupoauladetalle->buscar(array('idgrupoaula'=>$this->idgrupoaula));		
				if(empty($det2))$this->oDatAcad_grupoaula->eliminar($idgrupoaula);
			}			
			return $id;
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdgrupoauladetalle($pk){
		try {
			$this->dataAcad_grupoauladetalle = $this->oDatAcad_grupoauladetalle->get($pk);
			if(empty($this->dataAcad_grupoauladetalle)) {
				throw new Exception(JrTexto::_("Acad_grupoauladetalle").' '.JrTexto::_("not registered"));
			}
			$this->idgrupoauladetalle = $this->dataAcad_grupoauladetalle["idgrupoauladetalle"];
			$this->idgrupoaula = $this->dataAcad_grupoauladetalle["idgrupoaula"];
			$this->idcurso = $this->dataAcad_grupoauladetalle["idcurso"];
			$this->iddocente = $this->dataAcad_grupoauladetalle["iddocente"];
			$this->idlocal = $this->dataAcad_grupoauladetalle["idlocal"];
			$this->idambiente = $this->dataAcad_grupoauladetalle["idambiente"];
			$this->nombre = $this->dataAcad_grupoauladetalle["nombre"];
			$this->fecha_inicio = $this->dataAcad_grupoauladetalle["fecha_inicio"];
			$this->fecha_final = $this->dataAcad_grupoauladetalle["fecha_final"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('acad_grupoauladetalle', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataAcad_grupoauladetalle = $this->oDatAcad_grupoauladetalle->get($pk);
			if(empty($this->dataAcad_grupoauladetalle)) {
				throw new Exception(JrTexto::_("Acad_grupoauladetalle").' '.JrTexto::_("not registered"));
			}

			return $this->oDatAcad_grupoauladetalle->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}