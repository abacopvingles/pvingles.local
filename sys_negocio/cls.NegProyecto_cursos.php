<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		19-12-2018
 * @copyright	Copyright (C) 19-12-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto_cursos', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegProyecto_cursos 
{
	protected $idproycurso;
	protected $idcurso;
	protected $idproyecto;
	
	protected $dataProyecto_cursos;
	protected $oDatProyecto_cursos;	

	public function __construct()
	{
		$this->oDatProyecto_cursos = new DatProyecto_cursos;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto_cursos->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto_cursos->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto_cursos->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatProyecto_cursos->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatProyecto_cursos->get($this->idproycurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursos', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatProyecto_cursos->iniciarTransaccion('neg_i_Proyecto_cursos');
			$this->idproycurso = $this->oDatProyecto_cursos->insertar($this->idcurso,$this->idproyecto);
			$this->oDatProyecto_cursos->terminarTransaccion('neg_i_Proyecto_cursos');	
			return $this->idproycurso;
		} catch(Exception $e) {	
		    $this->oDatProyecto_cursos->cancelarTransaccion('neg_i_Proyecto_cursos');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursos', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatProyecto_cursos->actualizar($this->idproycurso,$this->idcurso,$this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Proyecto_cursos', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatProyecto_cursos->eliminar($this->idproycurso);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdproycurso($pk){
		try {
			$this->dataProyecto_cursos = $this->oDatProyecto_cursos->get($pk);
			if(empty($this->dataProyecto_cursos)) {
				throw new Exception(JrTexto::_("Proyecto_cursos").' '.JrTexto::_("not registered"));
			}
			$this->idproycurso = $this->dataProyecto_cursos["idproycurso"];
			$this->idcurso = $this->dataProyecto_cursos["idcurso"];
			$this->idproyecto = $this->dataProyecto_cursos["idproyecto"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursos', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataProyecto_cursos = $this->oDatProyecto_cursos->get($pk);
			if(empty($this->dataProyecto_cursos)) {
				throw new Exception(JrTexto::_("Proyecto_cursos").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto_cursos->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}