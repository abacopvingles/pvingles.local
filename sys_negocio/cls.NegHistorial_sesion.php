<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-04-2017
 * @copyright	Copyright (C) 04-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatHistorial_sesion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegHistorial_sesion 
{
	protected $idhistorialsesion;
	protected $tipousuario;
	protected $idusuario;
	protected $lugar;
	protected $idcurso;
	protected $fechaentrada;
	protected $fechasalida;
	
	protected $dataHistorial_sesion;
	protected $oDatHistorial_sesion;	

	public function __construct()
	{
		$this->oDatHistorial_sesion = new DatHistorial_sesion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatHistorial_sesion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatHistorial_sesion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,100000000);
			return $this->oDatHistorial_sesion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function ultimavisita($filtros=null){
		try{
			$this->setLimite(0,100000000);
			return $this->oDatHistorial_sesion->ultimavisita($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function listar()
	{
		try {
			return $this->oDatHistorial_sesion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatHistorial_sesion->get($this->idhistorialsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sesion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatHistorial_sesion->iniciarTransaccion('neg_i_Historial_sesion');
			$this->idhistorialsesion = $this->oDatHistorial_sesion->insertar($this->tipousuario,$this->idusuario,$this->lugar,$this->idcurso,$this->fechaentrada,$this->fechasalida);
			//$this->oDatHistorial_sesion->terminarTransaccion('neg_i_Historial_sesion');	
			return $this->idhistorialsesion;
		} catch(Exception $e) {	
		   //$this->oDatHistorial_sesion->cancelarTransaccion('neg_i_Historial_sesion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('historial_sesion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatHistorial_sesion->actualizar($this->idhistorialsesion,$this->tipousuario,$this->idusuario,$this->lugar,$this->idcurso,$this->fechaentrada,$this->fechasalida);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Historial_sesion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatHistorial_sesion->eliminar($this->idhistorialsesion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdhistorialsesion($pk){
		try {
			$this->dataHistorial_sesion = $this->oDatHistorial_sesion->get($pk);
			if(empty($this->dataHistorial_sesion)) {
				throw new Exception(JrTexto::_("Historial_sesion").' '.JrTexto::_("not registered"));
			}
			$this->idhistorialsesion = $this->dataHistorial_sesion["idhistorialsesion"];
			$this->tipousuario = $this->dataHistorial_sesion["tipousuario"];
			$this->idusuario = $this->dataHistorial_sesion["idusuario"];
			$this->lugar = $this->dataHistorial_sesion["lugar"];
			$this->idcurso = $this->dataHistorial_sesion["idcurso"];
			$this->fechaentrada = $this->dataHistorial_sesion["fechaentrada"];
			$this->fechasalida = $this->dataHistorial_sesion["fechasalida"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}
	//Hice esto por si afectaba algo en la otra funcion
	public function setCampo2($pk, $propiedad, $valor){
		try {
			$this->dataHistorial_sesion = $this->oDatHistorial_sesion->get($pk);
			if(empty($this->dataHistorial_sesion)) {
				throw new Exception(JrTexto::_("Historial_sesion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_sesion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('historial_sesion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataHistorial_sesion = $this->oDatHistorial_sesion->get($pk);
			if(empty($this->dataHistorial_sesion)) {
				throw new Exception(JrTexto::_("Historial_sesion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatHistorial_sesion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function fechas_de_ingreso($filtros){
		try {
			$this->oDatHistorial_sesion->setLimite(0,6);
			return $this->oDatHistorial_sesion->frecuenciaAlumno($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function tiempo_fecha_ingreso($filtros){
		try {
			return $this->oDatHistorial_sesion->frecuenciaAlumno($filtros,true);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
		
}