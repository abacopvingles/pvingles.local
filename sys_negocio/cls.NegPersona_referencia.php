<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		04-01-2018
 * @copyright	Copyright (C) 04-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_referencia', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_referencia 
{
	protected $idreferencia;
	protected $idpersona;
	protected $nombre;
	protected $cargo;
	protected $relacion;
	protected $correo;
	protected $telefono;
	protected $mostrar;
	
	protected $dataPersona_referencia;
	protected $oDatPersona_referencia;	

	public function __construct()
	{
		$this->oDatPersona_referencia = new DatPersona_referencia;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_referencia->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_referencia->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_referencia->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_referencia->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_referencia->get($this->idreferencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_referencia', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_referencia->iniciarTransaccion('neg_i_Persona_referencia');
			$this->idreferencia = $this->oDatPersona_referencia->insertar($this->idpersona,$this->nombre,$this->cargo,$this->relacion,$this->correo,$this->telefono,$this->mostrar);
			$this->oDatPersona_referencia->terminarTransaccion('neg_i_Persona_referencia');	
			return $this->idreferencia;
		} catch(Exception $e) {	
		    $this->oDatPersona_referencia->cancelarTransaccion('neg_i_Persona_referencia');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_referencia', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_referencia->actualizar($this->idreferencia,$this->idpersona,$this->nombre,$this->cargo,$this->relacion,$this->correo,$this->telefono,$this->mostrar);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatPersona_referencia->cambiarvalorcampo($this->idreferencia,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_referencia', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_referencia->eliminar($this->idreferencia);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdreferencia($pk){
		try {
			$this->dataPersona_referencia = $this->oDatPersona_referencia->get($pk);
			if(empty($this->dataPersona_referencia)) {
				throw new Exception(JrTexto::_("Persona_referencia").' '.JrTexto::_("not registered"));
			}
			$this->idreferencia = $this->dataPersona_referencia["idreferencia"];
			$this->idpersona = $this->dataPersona_referencia["idpersona"];
			$this->nombre = $this->dataPersona_referencia["nombre"];
			$this->cargo = $this->dataPersona_referencia["cargo"];
			$this->relacion = $this->dataPersona_referencia["relacion"];
			$this->correo = $this->dataPersona_referencia["correo"];
			$this->telefono = $this->dataPersona_referencia["telefono"];
			$this->mostrar = $this->dataPersona_referencia["mostrar"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_referencia', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_referencia = $this->oDatPersona_referencia->get($pk);
			if(empty($this->dataPersona_referencia)) {
				throw new Exception(JrTexto::_("Persona_referencia").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_referencia->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}