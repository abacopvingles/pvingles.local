<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		01-03-2018
 * @copyright	Copyright (C) 01-03-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatExamen_ubicacion_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegExamen_ubicacion_alumno 
{
	protected $idexam_alumno;
	protected $idexamen;
	protected $idalumno;
	protected $estado;
	protected $resultado;
	protected $fecha;
	
	protected $dataExamen_ubicacion_alumno;
	protected $oDatExamen_ubicacion_alumno;	

	public function __construct()
	{
		$this->oDatExamen_ubicacion_alumno = new DatExamen_ubicacion_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatExamen_ubicacion_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatExamen_ubicacion_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatExamen_ubicacion_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatExamen_ubicacion_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatExamen_ubicacion_alumno->get($this->idexam_alumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatExamen_ubicacion_alumno->iniciarTransaccion('neg_i_Examen_ubicacion_alumno');
			$this->idexam_alumno = $this->oDatExamen_ubicacion_alumno->insertar($this->idexamen,$this->idalumno,$this->estado,$this->resultado,$this->fecha);
			$this->oDatExamen_ubicacion_alumno->terminarTransaccion('neg_i_Examen_ubicacion_alumno');	
			return $this->idexam_alumno;
		} catch(Exception $e) {	
		    $this->oDatExamen_ubicacion_alumno->cancelarTransaccion('neg_i_Examen_ubicacion_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatExamen_ubicacion_alumno->actualizar($this->idexam_alumno,$this->idexamen,$this->idalumno,$this->estado,$this->resultado,$this->fecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Examen_ubicacion_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatExamen_ubicacion_alumno->eliminar($this->idexam_alumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdexam_alumno($pk){
		try {
			$this->dataExamen_ubicacion_alumno = $this->oDatExamen_ubicacion_alumno->get($pk);
			if(empty($this->dataExamen_ubicacion_alumno)) {
				throw new Exception(JrTexto::_("Examen_ubicacion_alumno").' '.JrTexto::_("not registered"));
			}
			$this->idexam_alumno = $this->dataExamen_ubicacion_alumno["idexam_alumno"];
			$this->idexamen = $this->dataExamen_ubicacion_alumno["idexamen"];
			$this->idalumno = $this->dataExamen_ubicacion_alumno["idalumno"];
			$this->estado = $this->dataExamen_ubicacion_alumno["estado"];
			$this->resultado = $this->dataExamen_ubicacion_alumno["resultado"];
			$this->fecha = $this->dataExamen_ubicacion_alumno["fecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('examen_ubicacion_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataExamen_ubicacion_alumno = $this->oDatExamen_ubicacion_alumno->get($pk);
			if(empty($this->dataExamen_ubicacion_alumno)) {
				throw new Exception(JrTexto::_("Examen_ubicacion_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatExamen_ubicacion_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}