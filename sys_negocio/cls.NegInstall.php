<?php
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatUbigeo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatLocal', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatInstall', RUTA_BASE, 'sys_datos');

class NegInstall {
    protected $oDatUbigeo;
    protected $oDatLocal;
    protected $oDatInstall;
	public function __construct(){
        $this->oDatUbigeo = new DatUbigeo;
        $this->oDatLocal = new DatLocal;
        $this->oDatInstall = new DatInstall;
    }
    public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatUbigeo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
    }
    public function getdepartamentos(){
        try{
            $resultado = array();
            $this->setLimite(0,1000);
            $resultado = $this->oDatUbigeo->getciudad(array('alldepartamentos'=>true,'pais'=>'PE'));
            if(!empty($resultado)){
                $i = 0; $c = count($resultado);
                do{
                    $resultado[$i]['id_ubigeo'] = substr($resultado[$i]['id_ubigeo'],0,2);
                    ++$i;
                }while($i<$c);
            }
            return $resultado;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function getprovincia($departamento){
        try{
            $resultado = array();
            $this->setLimite(0,1000);
            $resultado = $this->oDatUbigeo->getciudad(array('allprovincias'=>true,'departamento'=>$departamento,'pais'=>'PE'));
            if(!empty($resultado)){
                $i = 0; $c = count($resultado);
                do{
                    $resultado[$i]['id_ubigeo'] = substr($resultado[$i]['id_ubigeo'],2,2);
                    ++$i;
                }while($i<$c);
            }
            return $resultado;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function getlocal($ubigeo){
        try{
            $resultado = array();
            $this->oDatLocal->setLimite(0,100000);
            $resultado = $this->oDatLocal->buscar(array('id_ubigeo' => $ubigeo));
            return $resultado;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
    public function transacInsert($lista){
        try{
            $this->oDatInstall->transacInsert($lista);
            return true;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}
?>