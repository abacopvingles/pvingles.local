<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-04-2017
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatActividad_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatActividad_detalle', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatActividades', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_datos::DatNiveles', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegActividad_alumno
{

	protected $idactalumno;
	protected $iddetalleactividad;
	protected $idalumno;
	protected $fecha;
	protected $porcentajeprogreso;
	protected $habilidades;
	protected $estado;
	protected $html_solucion;
	protected $file;
	protected $tipofile;

	protected $dataActividad_alumno;
	protected $oDatActividad_alumno;
 	protected $oDatActividad_detalle;
 	protected $oDatActividades;
	protected $oDatNiveles;

	public function __construct()
	{
		$this->oDatActividad_alumno = new DatActividad_alumno;
 		$this->oDatActividad_detalle = new DatActividad_detalle;
 		$this->oDatActividades = new DatActividades;
		$this->oDatNiveles = new DatNiveles;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);

		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}

		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;

			$this->oDatActividad_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////
	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatActividad_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			$this->setLimite(0,10000);
			return $this->oDatActividad_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatActividad_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatActividad_alumno->get($this->idactalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function setId($pk){
		try{
			$result = $this->oDatActividad_alumno->get($pk);
			$this->idactalumno = $result['idactalumno'];
			$this->iddetalleactividad = $result['iddetalleactividad'];
			$this->idalumno = $result['idalumno'];
			$this->fecha = $result['fecha'];
			$this->porcentajeprogreso = $result['porcentajeprogreso'];
			$this->habilidades = $result['habilidades'];
			$this->estado = $result['estado'];
			$this->html_solucion = $result['html_solucion'];
			return true;
		}catch(Exception $e){
			throw new Exception($e->getMessage());
			return false;
		}
	}

	public function agregar()
	{
		try {
			//if(!NegSesion::tiene_acceso('actividad_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			//$this->oDatActividad_alumno->iniciarTransaccion('neg_i_Actividad_alumno');
			$this->idactalumno = $this->oDatActividad_alumno->insertar($this->iddetalleactividad,$this->idalumno,$this->fecha,$this->porcentajeprogreso,$this->habilidades,$this->estado,$this->html_solucion,$this->tipofile,$this->file);
			//$this->oDatActividad_alumno->terminarTransaccion('neg_i_Actividad_alumno');
			return $this->idactalumno;
		} catch(Exception $e) {
		   //$this->oDatActividad_alumno->cancelarTransaccion('neg_i_Actividad_alumno');
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/* if(!NegSesion::tiene_acceso('actividad_alumno', 'edit')){	throw new Exception(JrTexto::_('Restricted access').'!!'); }*/
			return $this->oDatActividad_alumno->actualizar($this->idactalumno,$this->iddetalleactividad,$this->idalumno,$this->fecha,$this->porcentajeprogreso,$this->habilidades,$this->estado,$this->html_solucion,$this->tipofile,$this->file);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}


	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatActividad_alumno->eliminar($this->idactalumno);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('actividad_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataActividad_alumno = $this->oDatActividad_alumno->get($pk);
			if(empty($this->dataActividad_alumno)) {
				throw new Exception(JrTexto::_("Actividad_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatActividad_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}

	public function ultimaActividadxAlum($idalumno){
		try {
			$ultima_activ = $this->oDatActividad_alumno->ultimaActividad($idalumno);
			if(empty($ultima_activ)) return null;
            $det_act=$this->oDatActividad_detalle->get($ultima_activ['iddetalleactividad']);
            $activ=$this->oDatActividades->get($det_act['idactividad']);

            $filtroN=array('idnivel'=>$activ['nivel']);
            $filtroU=array('idnivel'=>$activ['unidad']);
            $filtroS=array('idnivel'=>$activ['sesion']);
            $nivel  = $this->oDatNiveles->buscar($filtroN);
            $unidad = $this->oDatNiveles->buscar($filtroU);
            $sesion = $this->oDatNiveles->buscar($filtroS);

			return array('nivel'=>$nivel[0], 'unidad'=>$unidad[0], 'sesion'=>$sesion[0]);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	public function countprogreso($filtros = null){
		try{
			$this->setLimite(0,10000);
			return $this->oDatActividad_alumno->countprogreso($filtros);
		}catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	private function ubicartrimestre($key,$key2,$value,$hab,&$arreglo){
		
		if($key == 1){
			$arreglo[$key][strval($hab)] += $value;
		}elseif($key == 2){
			if($key2 == 0){
				$arreglo[1][strval($hab)] += $value;
			}else{
				$arreglo[2][strval($hab)] += $value;
			}
		}elseif($key == 3){
			$arreglo[2][strval($hab)] += $value;
		}else{
			$arreglo[3][strval($hab)] += $value;
		}
	}
	public function totalactividades($idcurso,$unidades,$contrimestre = true){
		try{
			$act = array();
			$tri = array();
			$totales = 0;

			$filtros = null;

			if(!is_null($idcurso) && !is_null($unidades)){
				$tri[1] = array('4'=>0,'5'=>0,'6'=>0 ,'7'=>0);
				$tri[2] = array('4'=>0,'5'=>0,'6'=>0 ,'7'=>0);
				$tri[3] = array('4'=>0,'5'=>0,'6'=>0 ,'7'=>0);
				foreach($unidades as $key => $value){
					if(!is_null($value)){
						$sum4 = 0;$sum5 = 0;$sum6 = 0;$sum7 = 0;
						$sumTri4 = 0;$sumTri5 = 0;$sumTri6 = 0;$sumTri7 = 0;
						foreach($value as $key2 => $value2){
							if($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true,'idcurso' =>$idcurso,'idrecurso'=>$value2,'listen'=> true ))){
								$sum4 += $resultado[0]['total'];
								$this->ubicartrimestre($key,$key2,$resultado[0]['total'],'4',$tri);
							}
							if($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true,'idcurso' =>$idcurso,'idrecurso'=>$value2,'read'=> true ))){
								$sum5 += $resultado[0]['total'];
								$this->ubicartrimestre($key,$key2,$resultado[0]['total'],'5',$tri);
							}
							if($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true,'idcurso' =>$idcurso,'idrecurso'=>$value2,'write'=> true ))){
								$sum6 += $resultado[0]['total'];
								$this->ubicartrimestre($key,$key2,$resultado[0]['total'],'6',$tri);
							}
							if($resultado = $this->oDatActividad_alumno->actividadestotal(array('xunidad' => true,'idcurso' =>$idcurso,'idrecurso'=>$value2,'speak'=> true ))){
								$sum7 += $resultado[0]['total'];
								$this->ubicartrimestre($key,$key2,$resultado[0]['total'],'7',$tri);
							}
						}
						$act[$key] = array('4'=>$sum4,'5'=>$sum5,'6'=>$sum6 ,'7'=>$sum7);
					}
				}
			}
			$totales = array('4'=>0,'5'=>0,'6'=>0 ,'7'=>0);
			foreach($act as $value){
				foreach($value as $k => $v){
					$totales[$k] += $v;
				}
			}
			return array('total' => $totales, 'bimestre' => $act, 'trimestre' => $tri);
		}catch(Exception $e) {
			
			throw new Exception($e->getMessage());
		}
	}
}
