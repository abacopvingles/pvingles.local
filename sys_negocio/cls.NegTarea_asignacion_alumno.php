<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		15-06-2017
 * @copyright	Copyright (C) 15-06-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTarea_asignacion_alumno', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTarea_asignacion_alumno 
{
	protected $iddetalle;
	protected $idtarea_asignacion;
	protected $idalumno;
	protected $mensajedevolucion;
	protected $notapromedio;
	protected $estado;
	protected $idgrupotarea;
	protected $iddocente;
	
	protected $dataTarea_asignacion_alumno;
	protected $oDatTarea_asignacion_alumno;	

	public function __construct()
	{
		$this->oDatTarea_asignacion_alumno = new DatTarea_asignacion_alumno;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTarea_asignacion_alumno->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTarea_asignacion_alumno->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTarea_asignacion_alumno->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTarea_asignacion_alumno->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTarea_asignacion_alumno->get($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('tarea_asignacion_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatTarea_asignacion_alumno->iniciarTransaccion('neg_i_Tarea_asignacion_alumno');
			$this->iddetalle = $this->oDatTarea_asignacion_alumno->insertar($this->idtarea_asignacion,$this->idalumno,$this->mensajedevolucion,$this->notapromedio,$this->estado,$this->idgrupotarea,$this->iddocente);
			//$this->oDatTarea_asignacion_alumno->terminarTransaccion('neg_i_Tarea_asignacion_alumno');	
			return $this->iddetalle;
		} catch(Exception $e) {	
		   //$this->oDatTarea_asignacion_alumno->cancelarTransaccion('neg_i_Tarea_asignacion_alumno');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			$usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('tarea_asignacion_alumno', 'edit') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}			
			return $this->oDatTarea_asignacion_alumno->actualizar($this->iddetalle,$this->idtarea_asignacion,$this->idalumno,$this->mensajedevolucion,$this->notapromedio,$this->estado);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			if(!NegSesion::tiene_acceso('Tarea_asignacion_alumno', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			return $this->oDatTarea_asignacion_alumno->eliminar($this->iddetalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIddetalle($pk){
		try {
			$this->dataTarea_asignacion_alumno = $this->oDatTarea_asignacion_alumno->get($pk);
			if(empty($this->dataTarea_asignacion_alumno)) {
				throw new Exception(JrTexto::_("Tarea_asignacion_alumno").' '.JrTexto::_("not registered"));
			}
			$this->iddetalle = $this->dataTarea_asignacion_alumno["iddetalle"];
			$this->idtarea_asignacion = $this->dataTarea_asignacion_alumno["idtarea_asignacion"];
			$this->idalumno = $this->dataTarea_asignacion_alumno["idalumno"];
			$this->mensajedevolucion = $this->dataTarea_asignacion_alumno["mensajedevolucion"];
			$this->notapromedio = $this->dataTarea_asignacion_alumno["notapromedio"];
			$this->estado = $this->dataTarea_asignacion_alumno["estado"];
			$this->idgrupotarea=$this->dataTarea_asignacion_alumno["idgrupotarea"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			if(!NegSesion::tiene_acceso('tarea_asignacion_alumno', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->dataTarea_asignacion_alumno = $this->oDatTarea_asignacion_alumno->get($pk);
			if(empty($this->dataTarea_asignacion_alumno)) {
				throw new Exception(JrTexto::_("Tarea_asignacion_alumno").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTarea_asignacion_alumno->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	private function setIdtarea_asignacion($idtarea_asignacion)
	{
		try {
			$this->idtarea_asignacion= NegTools::validar('todo', $idtarea_asignacion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setIdalumno($idalumno)
	{
		try {
			$this->idalumno= NegTools::validar('todo', $idalumno, false, JrTexto::_("Please enter a valid value"), array("longmax" => 11));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setMensajedevolucion($mensajedevolucion)
	{
		try {
			$this->mensajedevolucion= NegTools::validar('todo', $mensajedevolucion, false, JrTexto::_("Please enter a valid value"), array("longmax" => 999999999));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setNotapromedio($notapromedio)
	{
		try {
			$this->notapromedio= NegTools::validar('todo', $notapromedio, false, JrTexto::_("Please enter a valid value"), array("longmax" => 4));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
	private function setEstado($estado)
	{
		try {
			$this->estado= NegTools::validar('todo', $estado, false, JrTexto::_("Please enter a valid value"), array("longmax" => 1));
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}	
	}
		
}