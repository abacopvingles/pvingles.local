<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		29-01-2019
 * @copyright	Copyright (C) 29-01-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTest', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTest 
{
	protected $idtest;
	protected $titulo;
	protected $puntaje;
	protected $mostrar;
	protected $imagen;
	
	protected $dataTest;
	protected $oDatTest;	

	public function __construct()
	{
		$this->oDatTest = new DatTest;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTest->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTest->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTest->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTest->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTest->get($this->idtest);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTest->iniciarTransaccion('neg_i_Test');
			$this->idtest = $this->oDatTest->insertar($this->titulo,$this->puntaje,$this->mostrar,$this->imagen);
			$this->oDatTest->terminarTransaccion('neg_i_Test');	
			return $this->idtest;
		} catch(Exception $e) {	
		    $this->oDatTest->cancelarTransaccion('neg_i_Test');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTest->actualizar($this->idtest,$this->titulo,$this->puntaje,$this->mostrar,$this->imagen);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function cambiarvalorcampo($campo,$valor){
		try {
			return $this->oDatTest->cambiarvalorcampo($this->idtest,$campo,$valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
						
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Test', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTest->eliminar($this->idtest);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtest($pk){
		try {
			$this->dataTest = $this->oDatTest->get($pk);
			if(empty($this->dataTest)) {
				throw new Exception(JrTexto::_("Test").' '.JrTexto::_("not registered"));
			}
			$this->idtest = $this->dataTest["idtest"];
			$this->titulo = $this->dataTest["titulo"];
			$this->puntaje = $this->dataTest["puntaje"];
			$this->mostrar = $this->dataTest["mostrar"];
			$this->imagen = $this->dataTest["imagen"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('test', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTest = $this->oDatTest->get($pk);
			if(empty($this->dataTest)) {
				throw new Exception(JrTexto::_("Test").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTest->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}