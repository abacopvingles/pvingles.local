<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		01-02-2019
 * @copyright	Copyright (C) 01-02-2019. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatTest_asignacion', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegTest_asignacion 
{
	protected $idtestasigancion;
	protected $idtest;
	protected $idcurso;
	protected $idrecurso;
	protected $situacion;
	
	protected $dataTest_asignacion;
	protected $oDatTest_asignacion;	

	public function __construct()
	{
		$this->oDatTest_asignacion = new DatTest_asignacion;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatTest_asignacion->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatTest_asignacion->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatTest_asignacion->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatTest_asignacion->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatTest_asignacion->get($this->idtestasigancion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_asignacion', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatTest_asignacion->iniciarTransaccion('neg_i_Test_asignacion');
			$this->idtestasigancion = $this->oDatTest_asignacion->insertar($this->idtest,$this->idcurso,$this->idrecurso,$this->situacion);
			$this->oDatTest_asignacion->terminarTransaccion('neg_i_Test_asignacion');	
			return $this->idtestasigancion;
		} catch(Exception $e) {	
		    $this->oDatTest_asignacion->cancelarTransaccion('neg_i_Test_asignacion');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('test_asignacion', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatTest_asignacion->actualizar($this->idtestasigancion,$this->idtest,$this->idcurso,$this->idrecurso,$this->situacion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Test_asignacion', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatTest_asignacion->eliminar($this->idtestasigancion);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdtestasigancion($pk){
		try {
			$this->dataTest_asignacion = $this->oDatTest_asignacion->get($pk);
			if(empty($this->dataTest_asignacion)) {
				throw new Exception(JrTexto::_("Test_asignacion").' '.JrTexto::_("not registered"));
			}
			$this->idtestasigancion = $this->dataTest_asignacion["idtestasigancion"];
			$this->idtest = $this->dataTest_asignacion["idtest"];
			$this->idcurso = $this->dataTest_asignacion["idcurso"];
			$this->idrecurso = $this->dataTest_asignacion["idrecurso"];
			$this->situacion = $this->dataTest_asignacion["situacion"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('test_asignacion', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataTest_asignacion = $this->oDatTest_asignacion->get($pk);
			if(empty($this->dataTest_asignacion)) {
				throw new Exception(JrTexto::_("Test_asignacion").' '.JrTexto::_("not registered"));
			}

			return $this->oDatTest_asignacion->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}