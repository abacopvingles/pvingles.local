<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		18-09-2018
 * @copyright	Copyright (C) 18-09-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatPersona_setting', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegPersona_setting 
{
	protected $idpersonasetting;
	protected $idproyecto;
	protected $idpersona;
	protected $datos;
	
	protected $dataPersona_setting;
	protected $oDatPersona_setting;	

	public function __construct()
	{
		$this->oDatPersona_setting = new DatPersona_setting;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatPersona_setting->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatPersona_setting->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatPersona_setting->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatPersona_setting->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatPersona_setting->get($this->idpersonasetting);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_setting', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatPersona_setting->iniciarTransaccion('neg_i_Persona_setting');
			$this->idpersonasetting = $this->oDatPersona_setting->insertar($this->idproyecto,$this->idpersona,$this->datos);
			$this->oDatPersona_setting->terminarTransaccion('neg_i_Persona_setting');	
			return $this->idpersonasetting;
		} catch(Exception $e) {	
		    $this->oDatPersona_setting->cancelarTransaccion('neg_i_Persona_setting');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('persona_setting', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatPersona_setting->actualizar($this->idpersonasetting,$this->idproyecto,$this->idpersona,$this->datos);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Persona_setting', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatPersona_setting->eliminar($this->idpersonasetting);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdpersonasetting($pk){
		try {
			$this->dataPersona_setting = $this->oDatPersona_setting->get($pk);
			if(empty($this->dataPersona_setting)) {
				throw new Exception(JrTexto::_("Persona_setting").' '.JrTexto::_("not registered"));
			}
			$this->idpersonasetting = $this->dataPersona_setting["idpersonasetting"];
			$this->idproyecto = $this->dataPersona_setting["idproyecto"];
			$this->idpersona = $this->dataPersona_setting["idpersona"];
			$this->datos = $this->dataPersona_setting["datos"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('persona_setting', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataPersona_setting = $this->oDatPersona_setting->get($pk);
			if(empty($this->dataPersona_setting)) {
				throw new Exception(JrTexto::_("Persona_setting").' '.JrTexto::_("not registered"));
			}

			return $this->oDatPersona_setting->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}