// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute','pascalprecht.translate']);
// Configuración de las rutas
angularRoutingApp.config(function($routeProvider,$translateProvider) {
	$routeProvider
		.when('/', {
			templateUrl	: 'pages/slide.html',
			controller 	: 'mainController'
		})
		.when('/buscar', {
			templateUrl	: 'pages/home.html',
			controller 	: 'mainController'
		})
		.when('/libros', {
			templateUrl	: 'app/libros/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/libros/ver/:id', {
			templateUrl	: 'app/libros/views/index.html',
			controller 	: 'LibroVerController'
		})
		.when('/libros/verpdf/:id', {
			templateUrl	: 'app/libros/views/verPdf.html',
			controller 	: 'LibroVerController'
		})
		.when('/audio', {
			templateUrl	: 'app/audio/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/audio/ver/:id', {
			templateUrl	: 'app/audio/views/index.html',
			controller 	: 'LibroVerController'
		})
		.when('/video', {
			templateUrl	: 'app/video/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/video/ver/:id', {
			templateUrl	: 'app/video/views/index.html',
			controller 	: 'LibroVerController'
		})
		.when('/musica', {
			templateUrl	: 'app/musica/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/musica/ver/:id', {
			templateUrl	: 'app/musica/views/index.html',
			controller 	: 'LibroVerController'
		})
		.when('/biblioteca', {
			templateUrl	: 'app/biblioteca/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/biblioteca/ver/:id', {
			templateUrl	: 'app/biblioteca/views/index.html',
			controller 	: 'LibroController'
		})
		.when('/museo', {
			templateUrl	: 'app/museo/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/museo/ver/:id', {
			templateUrl	: 'app/museo/views/index.html',
			controller 	: 'LibroController'
		})
        .when('/favoritos', {
			templateUrl	: 'pages/favoritos.html',
			controller 	: 'FavoritoController'
		})
        .when('/notas/agregar', {
			templateUrl	: 'app/notas/views/index-principal.html',
			controller 	: 'NotasController'
		})
        .when('/notas', {
			templateUrl	: 'app/notas/views/index-principal.html',
			controller 	: 'NotasController'
		})
		.when('/grabacion/listar', {
			templateUrl	: 'app/grabacion/views/index-principal.html',
			controller 	: 'GrabacionController'
		})
		.otherwise({
			redirectTo: '/'
		});

		//=============	Translate=================
		$translateProvider.translations('en',{
			TITLE:'Digital Library',
			inicio:'Home',
			tesis:'Thesis',
			trabajos:'Works',
			inglés:'English',
			relajación:'Relaxation',
			sonidos:'Sounds',
			infantil:'Childish',
			libros:'Books',
			audiolibros:'Audiobooks',
			videos:'Videos',
			musica:'Music',
			biblioteca:'e-Library',
			museo:'e-Museums',
			investigacion:'Investigation',
			favoritos:'My favorites',
			descargados:'Most downloaded',
			nuevos:'New',
			nuevo:'New',
			cerrar:'Sign off',
			buscar:'Search',
			atras:'Back',
			titulo:'Title',
			codigo:'Code',
			resumen:'Summary',
			descargar:'Download',
			primera:'First',
			ultima:'Last',
			anterior:'Previous',
			siguiente:'Next',
			capitulos:'See chapters',
			categorias:'Categories',
			seleccione:'Select',
			autor:'Author',
			idioma:'Language',
			cancelar:'Cancel',
			contain:'Contain the word',
			grabar:'Record',
			grabacion:'Recording',
			palabra:'Write a word'
		});
		$translateProvider.translations('es',{
			TITLE:'Biblioteca Digital',
			inicio:'Inicio',
			libros:'Libros',
			tesis:'Tesis',
			trabajos:'Trabajos',
			inglés:'Inglés',
			relajación:'relajación',
			sonidos:'Sonidos',
			infantil:'Infantil',
			audiolibros:'Audiolibros',
			videos:'Videos',
			musica:'Música',
			biblioteca:'Bibliotecas Digitales',
			museo:'Museos Virtuales',
			investigacion:'Investigación',
			favoritos:'Mis favoritos',
			descargados:'Más Descargados',
			nuevos:'Nuevos',
			nuevo:'Nuevo',
			cerrar:'Salir',
			buscar:'Buscar',
			atras:'Atrás',
			titulo:'Título',
			codigo:'Código',
			resumen:'Resumen',
			descargar:'Descargar',
			primera:'Primera',
			ultima:'Última',
			anterior:'Anterior',
			siguiente:'Siguiente',
			capitulos:'Ver Capítulos',
			categorias:'Categorías',
			seleccione:'Seleccionar',
			autor:'Autor',
			idioma:'Idioma',
			cancelar:'Cancelar',
			contain:'Contiene la palabra',
			grabar:'Grabar',
			grabacion:'Grabación',
			palabra:'Escribe una palabra'
		});
		$translateProvider.preferredLanguage('en');
		$translateProvider.useSanitizeValueStrategy('escapeParameters');
});

angularRoutingApp.controller('mainController', function($scope,$translate,$http,$routeParams,$rootScope,$window,$anchorScroll,$q,$location) {
	var id=$routeParams.id;
	$scope.categorias;
	$scope.subcategorias;
	$scope.autor;
	$scope.buscar;

	$scope.lang=function(trans){
		if(trans=='es'){
			document.getElementById('ingles').style.display = 'block';
			document.getElementById('espanol').style.display = 'none';
			$translate.use(trans);
		}else if(trans=='en'){
			document.getElementById('ingles').style.display = 'none';
			document.getElementById('espanol').style.display = 'block';
			$translate.use(trans);
		}
		
	}
	
	$scope.lateral=function(){
		document.getElementById('cuadros').style.display = 'none';
		document.getElementById('menu-left').style.display = 'block';
		document.getElementById('cerrar-lateral').style.display = 'inline-block';
	}
	$scope.lateralcerrar=function(){
		document.getElementById('cuadros').style.display = 'inline-block';
		document.getElementById('menu-left').style.display = 'none';
		document.getElementById('cerrar-lateral').style.display = 'none';
	}
	$http.get('../biblioteca/getVarsGlobales')
		.success(function(resp){
			if(resp.code=='ok'){
				$rootScope._sysUsuario_ = resp.data.usuario;
				$rootScope._sysUrlStatic_=resp.data.url_static;
				$rootScope._sysUrlBase_=resp.data.url_base;
				if(resp.data.usuario.length==0){
					$window.location.href = resp.data.url_base;
				}
			} else {
				$window.location.href = '../';
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	$scope.resultado = function(){
		var databuscar={
					'nombre':$scope.buscar,
				};
		$http.post('../bib_estudio/xLibro',databuscar)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.estudio=resp.data;
					//console.log('$scope.estudio: ',$scope.estudio);
					document.getElementById('nuevoante').style.display = 'none';
					document.getElementById('nuevo25').style.display = 'block';
					document.getElementById('museo').style.display = 'block';

					//$location.path('/resultado');
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});

	}

	$http.get('../bib_estudio/xGetEstudio/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$rootScope.historial={
		agregar: function(data){
			var deferred = $q.defer();
			$http.post($rootScope._sysUrlBase_+'/bib_historial/xAgregar', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$rootScope.historial_usu=resp.data;
					}else{
						console.log('hay error rootScope.historial.post: '+resp.msj);
					}
				}).error(function(resp){
					console.log('error al cargar datos: '+resp.msj);
				});
			return deferred.promise;
		},
		listar:function(params=''){
			var deferred = $q.defer();
			$http.get($rootScope._sysUrlBase_+'/bib_historial/xListar/'+params)
			.success(function(resp){
				if(resp.code=='ok'){
					$rootScope.historial_usu=resp.data;
					//console.log($rootScope.historial_usu);
				} else {
					console.log('error al cargar datos historial: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos historial');
			});
			return deferred.promise;
		}
	};
	//========================
		
	$http.get('../bib_categoria/xCategoria')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.categorias=resp.data;
				//console.log($scope.categorias);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_portada/listadojson')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.portada=resp.data;
				if($scope.portada==null){
					$scope.portada.foto='Biblioteca.jpg';
				}
				//console.log($scope.portada);
				if($scope.portada.foto==null){
					$scope.portada.foto='Biblioteca.jpg';
				}
			} else {
					console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_subcategoria/xSubCategoria')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.subcategorias=resp.data;
				//console.log($scope.subcategorias);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_autor/xAutor')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.autor=resp.data;
				//console.log($scope.autor);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});
	//=========================================SLIDER====================================
	$http.get('../bib_estudio/buscarFavoritos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudioFavoritos=resp.data;
				//console.log($scope.estudioFavoritos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});
	$http.get('../bib_estudio/nuevosEstudios')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudioNuevos=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});
	$http.get('../bib_estudio/descargados')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudioDescargados=resp.data;
				//console.log($scope.estudioDescargados);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});
	//===================================================================================
	$scope.cancelar = function(){
		$scope.buscar={};
	}
	
	$scope.atras= function(){
		document.getElementById('nuevoante').style.display = 'block';
		document.getElementById('nuevo25').style.display = 'none';
	}
	$scope.aceptar = function(){
			//$scope.buscar="mi";
		$scope.buscar.subcategoria_id = $scope.buscar.subcategoria_id || 0
		$scope.buscar.autor_id = $scope.buscar.autor_id || 0 
		$scope.buscar.texto = $scope.buscar.texto || "" 
			var data={	
				'id_categoria':$scope.buscar.categorias_id,
				'id_autor':$scope.buscar.autor_id,
				'texto':$scope.texto,
			};
			//console.log('busqueda avanzada');
			//console.log(data);
			$http.post($rootScope._sysUrlBase_+'/bib_estudio/xBusquedaAvanzada/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.estudio=resp.data;
						document.getElementById('nuevoante').style.display = 'none';
						document.getElementById('nuevo25').style.display = 'block';
						document.getElementById('museo').style.display = 'block';
					}else{
						console.log('hay error'+resp.msj);
					}
				}).error(function(){
					console.log('error al cargar datos: '+resp.msj);
				});
	}

	$scope.verLibro = function(id){
		$location.path('/libros/ver/'+id);		
	}
	$scope.verAudio = function(id){
		$location.path('/audio/ver/'+id);		
	}
	$scope.verMusica = function(id){
		$location.path('/musica/ver/'+id);		
	}
	$scope.verVideo = function(id){
		$location.path('/video/ver/'+id);		
	}
	$scope.irLibro = function(){
		$location.path('/libros/');		
	}
	$scope.irAudio = function(){
		$location.path('/audio/');		
	}
	$scope.irMusica = function(){
		$location.path('/musica/');		
	}
	$scope.irVideo = function(id){
		$location.path('/video/');		
	}
	$scope.irBiblioteca = function(id){
		$location.path('/biblioteca/');		
	}
	$scope.irMuseo = function(id){
		$location.path('/museo/');		
	}
	$scope.verGeneral= function($id_tipo,id){
		//console.log('$id_tipo:', $id_tipo);
		if($id_tipo==1){
			$scope.verLibro(id);
		}else if($id_tipo==2){
			$scope.verLibro(id);
		}else if($id_tipo==3){
			$scope.verLibro(id);
		}else if($id_tipo==4){
			$scope.verAudio(id);
		}else if($id_tipo==5){
			$scope.verMusica(id);
		}else if($id_tipo==6){
			$scope.verVideo(id);
		}
	}
});

angularRoutingApp.controller('LibroController', function($scope,$rootScope,$http,$routeParams,$location,$anchorScroll,$q) {
	var id=$routeParams.id;
	$scope.tipo;
	$scope.estudio;
	$scope.tipo=0;
	$scope.selectEstudios=[];
	$scope.bib_estudio=[];
	$scope.banderapaginacion = true;

	$scope.lang=function(trans){
		//console.log(trans);
		$translate.use(trans);
	}

	$scope.herramientas = {
        her01 : {nombre : "Scielo",descripcion:"Es un proyecto de biblioteca electrónica, que permite la publicación electrónica de ediciones completas de las revistas científicas que posibilita el acceso a través de distintos mecanismos, incluyendo listas de títulos y por materia,  índices de autores y materias y un motor de búsqueda.", link:"http://www.scielo.org.pe/"},
        her02 : {nombre : "Jurn",descripcion:"Cuenta con más de 3,000 revistas especializadas en artes y humanidades, es un motor de búsquedas que indexa títulos de artículos académicos, tesis doctorales de disciplinas, entre otros.", link:"http://www.jurn.org"},
        her03 : {nombre : "Teseo",descripcion:"Es un buscador de Tesis doctorales, creado por el Ministerio de Educación, Cultura y Deporte de España",link:"https://www.educacion.gob.es/teseo/irGestionarConsulta.do;jsessionid=D8D9FFDC8F597ABBBB9EE46CF1A149D0"},
        her04 : {nombre : "Redalyc",descripcion:"Es una hemeroteca científica a la que cualquiera tiene opción de acceder.",link:"http://www.redalyc.org/"},
        her05 : {nombre : "Pdf SB",descripcion:"Es un sitio web desde el que puedes leer y descargar libros electrónicos gratuitamente en formato PDF, cuenta con contenidos muy específicos, entre los que hay trabajos de investigación de diversas temáticas.",link:"http://biblioteca.ucundinamarca.edu.co/biblioteca/tag/pdf-sb/"},
        her06 : {nombre : "Cern Document Server",descripcion:"Archivo digital que brinda reportajes, artículos y el resto de contenido multimedia que encontramos en esta base de datos sobre física.",link:"https://cds.cern.ch/?ln=es"},
        her07 : {nombre : "Highbeam Research",descripcion:"Cuenta con una base de datos especializada para profesionales y estudiantes de diversos sectores.",link:"https://www.highbeam.com/"},
        her08 : {nombre : "Science",descripcion:"Es un motor de búsqueda que indexa hasta 60 bases de datos y 200 millones de sitios especializados en información científica.",link:"http://www.sciencemag.org/"},
        her09 : {nombre : "Microsoft Academic Search",descripcion:"Es capaz de mostrar cómo se encuentran relacionados determinados elementos.",link:"https://academic.microsoft.com"},
        her10 : {nombre : "Google Shoolar",descripcion:"Integra tesis, resúmenes, libros y demás. Asimismo permite averiguar citas relacionadas, así como las referencias bibliográficas de textos determinados, y más.",link:"https://scholar.google.com.pe/"}
    }

	$scope.resultado = function(){
		var databuscar={
			'nombre':$scope.buscar,
		};
		$http.post('../bib_estudio/xLibro',databuscar)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.estudio=resp.data;
					//console.log('$scope.estudio: ',$scope.estudio);
					document.getElementById('nuevoante').style.display = 'none';
					document.getElementById('nuevo25').style.display = 'block';
					document.getElementById('museo').style.display = 'block';
					document.getElementById('btnHistorial').style.display = 'none';
					//$location.path('/resultado');
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
	}

	/*$scope.resultadoAvanzado = function(){
		/*if($scope.buscar.subcategoria_id !=0 || 
		$scope.buscar.autor_id !=0 ||  
		$scope.buscar.idioma_id !=0  ){*/
		//$scope.buscar.texto = $scope.buscar.texto || null 
			/*var data={	
				'id_categoria':$scope.buscar.categorias_id,
				'id_autor':$scope.buscar.autor_id,|
				'idioma':$scope.buscar.idioma_id,
				'texto':$scope.texto,
			};
			console.log('busqueda avanzada');
			console.log(data);
			$http.post($rootScope._sysUrlBase_+'/bib_estudio/xBusquedaAvanzada/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						//if(resp.data=[]) alert('No hay datos para mostrar');
						$scope.estudio=resp.data;
						document.getElementById('nuevoante').style.display = 'none';
						document.getElementById('nuevo25').style.display = 'block';
					}else{
						console.log('hay error'+resp.msj);
					}
				}).error(function(){
					console.log('error al cargar datos: '+resp.msj);
				});
	}*/

	$http.get('../bib_recursos/xRecursos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				//console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	
	$scope.resultadoAvanzado = function(){
		$scope.buscar.subcategoria_id = $scope.buscar.subcategoria_id || 0;
		$scope.buscar.autor_id = $scope.buscar.autor_id || 0 ;
		$scope.buscar.idioma = $scope.buscar.idioma || 0 ;
		//$scope.buscar.texto = $scope.buscar.texto || null 
		var data={	
			'id_categoria':$scope.buscar.categorias_id,
			'id_autor':$scope.buscar.autor_id,
			'idioma':$scope.buscar.idioma_id,
			'texto':$scope.texto,
		};
		//console.log('busqueda avanzada');
		//console.log(data);
		$http.post($rootScope._sysUrlBase_+'/bib_estudio/xBusquedaAvanzada/', data)
			.success(function(resp){
				if(resp.code=='ok'){
					//if(resp.data=[]) alert('No hay datos para mostrar');
					$scope.estudio=resp.data;
					document.getElementById('nuevoante').style.display = 'none';
					document.getElementById('nuevo25').style.display = 'block';
					document.getElementById('museo').style.display = 'block';
					document.getElementById('btnHistorial').style.display = 'none';
				}else{
					console.log('hay error'+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
	};

	$scope.cargarLibros = function (){
		$http.get('../bib_tipo/Tipojson')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.tipo=resp.data;
					//console.log($scope.tipo);
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
	};

	$scope.init = function (){
		$scope.cargarLibros();
	};
	
	$scope.init();

	$http.get('../bib_idioma/xIdioma')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.idioma=resp.data;
				//console.log($scope.idioma);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	if (id!=undefined) {
		$http.get('../bib_estudio/xGetEstudio/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}
    
	$http.get('../bib_estudio/xEstudio')
		.success(function(resp){
			//console.log('xEstudio');
			//console.log('xEstudio: ',resp.data);
			if(resp.code=='ok'){
				$scope.estudio=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$http.get('../bib_estudio/Audiolibros')
		.success(function(resp){
			//console.log('xEstudio');
			//console.log(resp.data);
			if(resp.code=='ok'){
				$scope.audios=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$http.get('../bib_recursos/xRecursosCompartido')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				//console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	
	$scope.selecionarTipo = function(id){
		$scope.tipo=id;
		//console.log('$scope.tipo');
		//console.log('$scope.tipo: ',$scope.tipo);
		if($scope.tipo===0){
			document.getElementById('tipoAtras').style.display='none';
			document.getElementById('btnHistorial').style.display='block';
			$scope.selectEstudios=[];
			$scope.cargarLibros();
			document.getElementById('hola').style.display='none';
			document.getElementById('nuevaPage').style.display='none';				
		}else{
			if ($scope.estudio!=undefined) {
				for (var i = $scope.estudio.length - 1; i >= 0; i--) {
					if($scope.estudio[i].id_tipo ===$scope.tipo){
						if($scope.tipo==2){			
							document.getElementById('hola').style.display='block';
							document.getElementById('tipoAtras').style.display='block';
							document.getElementById('btnHistorial').style.display='none';
							$scope.selectEstudios.push($scope.estudio[i]);
						}else if($scope.tipo==3){
							document.getElementById('tipotres').style.display='block';
						}else{
							$scope.selectEstudios.push($scope.estudio[i]);
							document.getElementById('tipoAtras').style.display='block';
							document.getElementById('btnHistorial').style.display='none';
						}
					}
				} 
			}

		}
		$scope.setPage(1);
	}

	$scope.selecionarTipoMusica = function(id){
		$scope.tipo=id;
		if($scope.tipo==0){
			document.getElementById('tipoAtras').style.display='none';
			document.getElementById('btnHistorial').style.display='block';
			$scope.selectEstudios=[];
			$scope.cargarLibros();
		}else{
			for (var i = $scope.estudio.length - 1; i >= 0; i--) {
				if($scope.estudio[i].id_tipo==$scope.tipo){
					$scope.selectEstudios.push($scope.estudio[i]);
					document.getElementById('tipoAtras').style.display='block';
					document.getElementById('btnHistorial').style.display='none';
				}
				//document.getElementById('mostrarLib').style.display = 'block';
			}
		}
		//$scope.setPage(1);
	}
    $scope.irLibro = function(){
		$location.path('/libros/');		
	}
	$scope.irAudio = function(){
		$location.path('/audio/');		
	}
	$scope.irMusica = function(){
		$location.path('/musica/');		
	}
	$scope.irVideo = function(id){
		$location.path('/video/');		
	}
	$scope.irMuseo = function(id){
		$location.path('/museo/');		
	}
	$scope.irBiblioteca = function(id){
		$location.path('/biblioteca/');		
	}
	$scope.verLibro = function(id){
		$location.path('/libros/ver/'+id);		
	}
	$scope.verAudio = function(id){
		$location.path('/audio/ver/'+id);
		//console.log(id);
			var data={	
				'id_estudio':id,
			};
			$http.post($rootScope._sysUrlBase_+'/bib_estudio/xMusica/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.musica=resp.data;
						//console.log('musica: ',$scope.musica);
					}else{
						console.log('hay error'+resp.msj);
					}
				}).error(function(){
					console.log('error al cargar datos: '+resp.msj);
				});
	}
	$scope.verMusica = function(id){
		$location.path('/musica/ver/'+id);		
	}
	$scope.verVideo = function(id){
		$location.path('/video/ver/'+id);		
	}
	
	$scope.agregarFavorito=function(id,tipo){  
		console.log(id);

		var data={
			'id_estudio': id,
			'tipo':tipo
		};
		$http.post('../bib_estudio/xAgregar', data)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.estudio=resp.data;
					alert('Se ha agregado a favoritos');
				}else{
					alert(resp.msj);
					//console.log('hay error agregarFavorito: '+resp.msj);
				}
			}).error(function(resp){
				console.log('error al cargar datos: '+resp.msj);
			});
	}

	$scope.guardar = function(){
		//console.log($scope.nombre);
	}

	//==============================PAGINACION=================================
	 $scope.pager = {};
     $scope.pages = [];
	 $scope.setPage= function(page){
	 		
            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }
            

            // get pager object from service
             $scope.GetPager($scope.selectEstudios.length, page);
            if ($scope.pager.currentPage>1) {
             $scope.pager.pages=$scope.pages;
            // get current page of items
           $scope.items = $scope.selectEstudios.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
	 		}else{
	 			$scope.pages = [];
	 			 $scope.GetPager($scope.selectEstudios.length, page);

             $scope.pager.pages=$scope.pages;
            // get current page of items
           $scope.items = $scope.selectEstudios.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
	 		}
             
	 }

        // service implementation
        $scope.GetPager=function(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 12;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 12) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 12;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
            
            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
            //console.log('totalItems');
            //console.log(totalItems);


            // create an array of pages to ng-repeat in the pager control
            //var pages = _.range(startPage, endPage + 1);
            var max = (totalItems / 12)+1 ;
            for (var i=startIndex+1; i<(max); i++) {
		      $scope.pages.push(i);
		    }

            $scope.pager.currentPage = currentPage;
            $scope.pager.totalPages = totalPages;

            $scope.pager.startIndex = startIndex;
            $scope.pager.endIndex = endIndex;
        }      

	//=========================================================================
});

angularRoutingApp.controller('LibroVerController', function($scope,$rootScope,$http,$routeParams,$location,$anchorScroll,$q){

	var id=$routeParams.id;
	$scope.bib_historial=[];
	$scope.historial={};
	$scope.lang=function(trans){
		$translate.use(trans);
	}
	$scope.capitulos=function(id){
		//console.log('cap');
		//console.log(id);

		var data={	
			'id_estudio':id,
		};
		$http.post($rootScope._sysUrlBase_+'/bib_estudio/xMusica/',data)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.musica=resp.data;
					//console.log('musica:',$scope.musica);
				}else{
					console.log('hay error'+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
	}
	$scope.mostrarVentana=function(obj){
		document.getElementById('pnl-formulario').style.width='70%';
		//console.log($(obj));
	}

	$http.get('../bib_estudio/xGetEstudio/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$http.get('../bib_historial/xListar/?id_estudio='+id+'&descripcion=ver')
		.success(function(resp){
			if(resp.code=='ok'){
				//console.log(resp);
				if(resp.data.length!==0) $scope.historial['ver']= resp.data[0].id_historial ;
			} else {
				console.log('error al cargar datos historial: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos historial');
		});	

	var data={
		'id_historial' : $scope.historial.ver,
		'id_estudio':id,
		'descripcion':'ver',
	};

	$http.post('../bib_historial/xAgregar', data)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.historial={'ver':resp.data};
			}else{
				console.log('hay error rootScope.historial.post::ver: '+resp.msj);
			}
		}).error(function(resp){
			//console.log('error al cargar datos: '+resp.msj);
		});

	$scope.descargar=function(id,cantidad_descarga,historial_cantidad,tipo){
		historial_cantidad=historial_cantidad||1;
		// console.log(cantidad_descarga);
		// console.log(historial_cantidad);
		//console.log(tipo);

		$http.get('../bib_historial/xListar/?id_estudio='+id+'&descripcion=descargar')
			.success(function(resp){
				if(resp.code=='ok'){

					if(resp.data.length!==0) {
						$scope.historial['descargar']= resp.data[0].id_historial;
						$scope.historial['cant']= resp.data[0].cantidad;
					}
				} else {
					console.log('error al cargar datos historial: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos historial');
			});	

		//if(historial_cantidad<=3){
			if (tipo=='L') {
				/*...descargar..*/
				var a=document.createElement('a');
				a.href='../static/libreria/pdf/'+$scope.bib_estudio.archivo;
				a.download=$scope.bib_estudio.archivo;
				a.target='_blank';
				a.click();
				var data={
					'id_historial' : $scope.historial.descargar,
					'id_estudio':id,
					'descripcion':'descargar',
				};
			}else{
				if (tipo=='V') {
					/*...descargar..*/
					var a=document.createElement('a');
					a.href='../static/libreria/video/'+$scope.bib_estudio.archivo;
					a.download=$scope.bib_estudio.archivo;
					a.target='_blank';
					a.click();
					var data={
						'id_historial' : $scope.historial.descargar,
						'id_estudio':id,
						'descripcion':'descargar',
					};
				}
			}
			$http.post('../bib_historial/xAgregar', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.historial={'descargar':resp.data};
					}else{
						console.log('hay error rootScope.historial.post::descargar: '+resp.msj);
					}
				}).error(function(resp){
					console.log('error al cargar datos: '+resp.msj);
				});
		// }else{
		// 	alert("Usted alcanzó su máximo de descargas permitidas");
		// }
	}

	$scope.banderaVerPDF=false;
	$scope.verPDF=function(id){
		document.getElementById('pdf').style.display = 'block';
		var iframe=document.createElement('iframe');
		var selector = document.getElementById('contenedor_frame')
			iframe.target='_blank';
			iframe.width='100%';
			iframe.height='842px';
			iframe.margin='auto';
			iframe.setAttribute("src", '../static/libreria/pdf/viewer.html?file='+$scope.bib_estudio.archivo);
			selector.appendChild(iframe);
			$scope.banderaVerPDF=true;
	}

	$scope.atrasLibros=function(){
		document.getElementById('pdf').style.display = 'none';
		$scope.banderaVerPDF=false;
	}

	var audio = document.getElementById("myAudio"); 
	$scope.playAudio=function() {
		audio.src="../static/libreria/audio/"+$scope.bib_estudio.archivo;
		//console.log(audio.src);
    	audio.play();
	}
	$scope.playNewAudio=function(archivo) {
		//audio.src="../static/libreria/audio/"+$scope.bib_estudio.archivo;
		//console.log(audio.src);
		//console.log('archivo');
		//console.log(archivo);
    	return '../static/libreria/audio/' + archivo;
	}
	$scope.pauseAudio=function() {
		audio.pause();	
	}
	$scope.escucharAudio=function(){
		document.getElementById('contenedor_frame').style.display = 'flex';

	}
	$scope.escucharVideo=function(){
		document.getElementById('contenedor_frame').style.display = 'flex';

	}
	var video = document.getElementById("myVideo"); 
	$scope.playVideo=function() {
		video.src="../static/libreria/video/"+$scope.bib_estudio.archivo;
		//console.log(video.src);
    	video.play();
    	document.getElementById('imagen').style.display = 'none';
    	document.getElementById('ocultar1').style.display = 'none';
    	document.getElementById('ocultar2').style.display = 'none';
    	document.getElementById('botonCancelar').style.display = 'block';
    	document.getElementById('vid').style.display = 'block';
    	document.getElementById('vid').style.margin = '0 auto';
    	video.width='600';
    	video.style.display='block';

	}
	$scope.playVideoNew=function(id) {
		var audios = document.getElementsByTagName('audio');
		for (var i = 0; i <= audios.length-1; i++) {
			if( audios[i].getAttribute("id")!='myAudio_'+id ) {
				audios[i].pause();
			}
		}
		/*actualAudio = document.getElementById("myAudio_"+id); 
		if (actualAudio.duration > 0 && !actualAudio.paused) {
			actualAudio.pause();
		}*/
		/*
		var data={	
			'id':id,
		};
		$http.post($rootScope._sysUrlBase_+'/bib_estudio/play/',data)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.music=resp.data;
					//console.log('music: ',$scope.music);
					
					//audio.src="../static/libreria/audio/"+$scope.music['archivo'];
					//console.log(audio.src);
		        	//audio.play();
				}else{
					console.log('hay error'+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
		*/
	}
	$scope.pauseVideo=function() {
		video.pause();	
	}
	$scope.cancelarVideo=function() {
		video.pause();	
		document.getElementById('imagen').style.display = 'block';
    	document.getElementById('ocultar1').style.display = 'block';
    	document.getElementById('ocultar2').style.display = 'flex';
    	document.getElementById('vid').style.display = 'none';
    	document.getElementById('botonCancelar').style.display = 'none';
    	video.style.display='none';
	}		
});

angularRoutingApp.controller('AudioController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Audio';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.multimedia;
	$scope.tipo=0;
	$scope.selectMultimedia=[];
	$scope.bib_Multimedia=[];
	$scope.cargarMultimedia = function (){
		$http.get('../bib_tipo/Tipojson')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				//console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarMultimedia();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_multimedia/xGetMultimedia/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_multimedia=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_multimedia/xMultimedia')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.multimedia=resp.data;
				//console.log($scope.multimedia);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		//console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectMultimedia=[];
			$scope.cargarMultimedia();
		}else{
			if ($scope.multimedia!=undefined) {
			for (var i = $scope.multimedia.length - 1; i >= 0; i--) {
				if($scope.multimedia[i].idtipo ===$scope.tipo){
					$scope.selectMultimedias.push($scope.multimedia[i]);
				}
			}
		}
		}
	}
	$scope.verMultimedia = function(id){
		//console.log("Entre + "+id);
		$location.path('/audio/ver/'+id);	
	}	
});

angularRoutingApp.controller('VideoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Video';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.multimedia;
	$scope.tipo=0;
	$scope.selectMultimedia=[];
	$scope.bib_Multimedia=[];
	$scope.cargarMultimedia = function (){
		$http.get('../bib_tipo/Tipojson')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				//console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarMultimedia();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_multimedia/xGetMultimedia/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_multimedia=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_multimedia/xMultimedia')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.multimedia=resp.data;
				//console.log($scope.multimedia);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		//console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectMultimedia=[];
			$scope.cargarMultimedia();
		}else{
			if ($scope.multimedia!=undefined) {
			for (var i = $scope.multimedia.length - 1; i >= 0; i--) {
				if($scope.multimedia[i].idtipo ===$scope.tipo){
					$scope.selectMultimedias.push($scope.multimedia[i]);
				}
			}
		}
		}
	}

	$scope.verMultimedia = function(id){
		//console.log("Entre + "+id);
		$location.path('/video/ver/'+id);	
	}
	  
				
	$scope.guardar = function(){
		//console.log($scope.nombre);
	}
});

angularRoutingApp.controller('BibliotecaController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Biblioteca';
	$scope.nombre;
   
	var id=$routeParams.id;
		$scope.tipo;
		$scope.video;
		$scope.tipo=0;
		$scope.selectServicioss=[];
		$scope.bib_servicios=[];
		$scope.cargarServicios = function (){
			$http.get('../bib_tipo/Tipojson')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.tipo=resp.data;
					//console.log($scope.tipo);
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
		}

		$scope.init = function (){
			$scope.cargarServicios();
		}
		$scope.init();

		if (id!=undefined) {
			$http.get('../bib_servicios/xGetServicios/?id='+id)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.bib_servicios=resp.data;
				}else{
					console.log('hay error'+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
		}

		$http.get('../bib_servicios/xServicios')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.servicios=resp.data;
					//console.log($scope.servicios);
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});

		$scope.selecionarTipo = function(id){
			//console.log(id);
			$scope.tipo=id;
			if($scope.tipo===0){
				$scope.selectServicios=[];
				$scope.cargarServicios();
			}else{
				if ($scope.servicios!=undefined) {
				for (var i = $scope.servicios.length - 1; i >= 0; i--) {
					if($scope.servicios[i].idtipo ===$scope.tipo){
						$scope.selectServicios.push($scope.servicios[i]);
					}
				}
			}
			}


		}
		$scope.verServicios = function(id){
			//console.log("Entre + "+id);
			$location.path('/biblioteca/ver/'+id);	
		}
		 
					
		$scope.guardar = function(){
			//console.log($scope.nombre);

		}
});

angularRoutingApp.controller('MuseoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Museo';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.video;
	$scope.tipo=0;
	$scope.selectServicioss=[];
	$scope.bib_servicios=[];
	$scope.cargarServicios = function (){
		$http.get('../bib_tipo/Tipojson')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				//console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarServicios();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_servicios/xGetServicios/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_servicios=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_servicios/xServicios')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.servicios=resp.data;
				//console.log($scope.servicios);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		//console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectServicios=[];
			$scope.cargarServicios();
		}else{
			if ($scope.servicios!=undefined) {
			for (var i = $scope.servicios.length - 1; i >= 0; i--) {
				if($scope.servicios[i].idtipo ===$scope.tipo){
					$scope.selectServicios.push($scope.servicios[i]);
				}
			}
		}
		}
	}
	$scope.verServicios = function(id){
		//console.log("Entre + "+id);
		$location.path('/museo/ver/'+id);	
	}
	 
				
	$scope.guardar = function(){
		//console.log($scope.nombre);

	}
});

angularRoutingApp.controller('NotasController',function($scope,$rootScope,$http,$routeParams,$location) {
    $scope.bib_recursos=[];
    
    $http.get('../bib_recursos/xRecursos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				//console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
   
    $scope.eliminar=function($dni,$orden){  
        $http.get('../bib_recursos/xEliminarRecursos/?dni='+$dni+'&orden='+$orden)
		.success(function(resp){
            //console.log(resp);
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				//console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
        
    }
       
    $scope.guardar = function(){
		//console.log($scope.nombre);   
    }
});
angularRoutingApp.controller('GrabacionController',function($scope,$rootScope,$http,$routeParams,$location) {
    $scope.bib_grabacion=[];
    
    $http.get('../bib_grabacion/xGrabacion')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.grabacion=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
   
    $scope.eliminar=function($id){  
		//console.log($id);
		var data={
			'id': $id,
		};
	$http.post('../bib_grabacion/Eliminar2', data)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.grabacion=resp.data;
				//console.log($scope.grabacion);
			}else{
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(resp){
			//console.log('error al cargar datos: '+resp.msj);
		});
		location.reload();
    }
       
    $scope.guardar = function(){
		//console.log($scope.nombre);   
    }
});
angularRoutingApp.controller('FavoritoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.dni;
	var id=$routeParams.id;

	$http.get('../bib_detalle_usuario_estudio/xFavoritos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudioFav=resp.data;
				//console.log($scope.estudioFav);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$scope.verEstudio = function(id){
		$location.path('/libros/ver/'+id);		
	};
});
