<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_matricula extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_matricula";
			
			$cond = array();		
			
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
					$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_matricula";			
			
			$cond = array();		
					
			
			if(isset($filtros["idmatricula"])) {
					$cond[] = "idmatricula = " . $this->oBD->escapar($filtros["idmatricula"]);
			}
			if(isset($filtros["idgrupoauladetalle"])) {
					$cond[] = "idgrupoauladetalle = " . $this->oBD->escapar($filtros["idgrupoauladetalle"]);
			}
			if(isset($filtros["idalumno"])) {
					$cond[] = "idalumno = " . $this->oBD->escapar($filtros["idalumno"]);
			}
			if(isset($filtros["fecha_registro"])) {
					$cond[] = "fecha_registro = " . $this->oBD->escapar($filtros["fecha_registro"]);
			}
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["idusuario"])) {
					$cond[] = "idusuario = " . $this->oBD->escapar($filtros["idusuario"]);
			}
			if(isset($filtros["fecha_matricula"])) {
					$cond[] = "fecha_matricula = " . $this->oBD->escapar($filtros["fecha_matricula"]);
			}
			if(isset($filtros["idproyecto"])) {
				$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}		
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idgrupoauladetalle,$idalumno,$fecha_registro,$estado,$idusuario,$fecha_matricula,$idproyecto)
	{
		try {
			
			$this->iniciarTransaccion('dat_acad_matricula_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idmatricula) FROM acad_matricula");
			++$id;
			
			$estados = array('idmatricula' => $id
							
							,'idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha_registro'=>$fecha_registro
							,'estado'=>$estado
							,'idusuario'=>$idusuario
							,'fecha_matricula'=>$fecha_matricula
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->insert('acad_matricula', $estados);			
			$this->terminarTransaccion('dat_acad_matricula_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_matricula_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idgrupoauladetalle,$idalumno,$fecha_registro,$estado,$idusuario,$fecha_matricula,$idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_acad_matricula_update');
			$estados = array('idgrupoauladetalle'=>$idgrupoauladetalle
							,'idalumno'=>$idalumno
							,'fecha_registro'=>$fecha_registro
							,'estado'=>$estado
							,'idusuario'=>$idusuario
							,'fecha_matricula'=>$fecha_matricula
							,'idproyecto'=>$idproyecto								
							);
			
			$this->oBD->update('acad_matricula ', $estados, array('idmatricula' => $id));
		    $this->terminarTransaccion('dat_acad_matricula_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_matricula  "
					. " WHERE idmatricula = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_matricula', array('idmatricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_matricula', array($propiedad => $valor), array('idmatricula' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_matricula").": " . $e->getMessage());
		}
	}
   
		
}