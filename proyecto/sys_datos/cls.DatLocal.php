<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-08-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatLocal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM local";
			
			$cond = array();		
			
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM local";			
			
			$cond = array();		
					
			
			if(isset($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(isset($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(isset($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(isset($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(isset($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(isset($filtros["idproyecto"])) {
					$cond[] = "idproyecto = " . $this->oBD->escapar($filtros["idproyecto"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto)
	{
		try {
			
			$this->iniciarTransaccion('dat_local_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idlocal) FROM local");
			++$id;
			
			$estados = array('idlocal' => $id
							
							,'nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel
							,'idproyecto'=>$idproyecto							
							);
			
			$this->oBD->insert('local', $estados);			
			$this->terminarTransaccion('dat_local_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_local_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel,$idproyecto)
	{
		try {
			$this->iniciarTransaccion('dat_local_update');
			$estados = array('nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel
							,'idproyecto'=>$idproyecto								
							);
			
			$this->oBD->update('local ', $estados, array('idlocal' => $id));
		    $this->terminarTransaccion('dat_local_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM local  "
					. " WHERE idlocal = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('local', array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('local', array($propiedad => $valor), array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
   
		
}