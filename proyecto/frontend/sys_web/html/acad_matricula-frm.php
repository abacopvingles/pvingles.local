<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Acad_matricula"); ?></li>
  </ol>
</nav>
<?php } ?><div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="Idmatricula" id="idmatricula<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idgrupoauladetalle');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idgrupoauladetalle<?php echo $idgui; ?>" name="idgrupoauladetalle" required="required" class="form-control" value="<?php echo @$frm["idgrupoauladetalle"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idalumno');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idalumno<?php echo $idgui; ?>" name="idalumno" required="required" class="form-control" value="<?php echo @$frm["idalumno"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecha registro');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="fecha_registro<?php echo $idgui; ?>" name="fecha_registro" required="required" class="form-control" value="<?php echo @$frm["fecha_registro"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Estado');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="estado<?php echo $idgui; ?>" name="estado" required="required" class="form-control" value="<?php echo @$frm["estado"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idusuario');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="idusuario<?php echo $idgui; ?>" name="idusuario" required="required" class="form-control" value="<?php echo @$frm["idusuario"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecha matricula');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="fecha_matricula<?php echo $idgui; ?>" name="fecha_matricula" required="required" class="form-control" value="<?php echo @$frm["fecha_matricula"];?>">
                          </div>
            
    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-saveAcad_matricula" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('acad_matricula'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'acad_matricula', 'saveAcad_matricula', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
       <?php if(!empty($fcall)){ ?> if(typeof <?php echo $fcall?> == 'function'){
          <?php echo $fcall?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php } ?>  return redir('<?php echo JrAplicacion::getJrUrl(array("Acad_matricula"))?>');
      }
     }
  }); 
  
});

</script>