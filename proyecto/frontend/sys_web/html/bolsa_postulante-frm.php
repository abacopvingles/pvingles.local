<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$file='<img src="'.$this->documento->getUrlStatic().'/media/nofoto.jpg" class="img-responsive center-block">';
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Bolsa_postulante"); ?></li>
  </ol>
</nav>
<?php } ?><div class="row"  id="vent-<?php echo $idgui;?>"  >
  <div class="col">
    <div id="msj-interno"></div>
    <form method="post" id="frm-<?php echo $idgui;?>"  target="" enctype="" class="form-horizontal form-label-left" >
    <input type="hidden" name="Idpostular" id="idpostular<?php echo $idgui; ?>" value="<?php echo $this->pk;?>">
    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
    <div class="row">
          <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Fecharegistro');?> <span class="required"> (*) </span></label>              
                <input name="fecharegistro<?php echo $idgui; ?>" class="verdate form-control" required="required" type="date" value="<?php echo @$frm["fecharegistro"];?>">   
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Nombrecompleto');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="nombrecompleto<?php echo $idgui; ?>" name="nombrecompleto" required="required" class="form-control" value="<?php echo @$frm["nombrecompleto"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Telefono');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="telefono<?php echo $idgui; ?>" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Correo');?> <span class="required"> (*) </span></label>              
                <input type="email" id="correo<?php echo $idgui; ?>" name="correo" required="required" class="form-control" value="<?php echo @$frm["correo"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Descripcion');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="descripcion<?php echo $idgui; ?>" name="descripcion" required="required" class="form-control" value="<?php echo @$frm["descripcion"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Mostrar');?> <span class="required"> (*) </span></label>              
                <input type="text"  id="mostrar<?php echo $idgui; ?>" name="mostrar" required="required" class="form-control" value="<?php echo @$frm["mostrar"];?>">
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idpublicacion');?> <span class="required"> (*) </span></label>              
               <input type="number" id="idpublicacion<?php echo $idgui; ?>"  name="idpublicacion" step="1" value="<?php echo !empty($frm["idpublicacion"])?$frm["idpublicacion"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                          </div>
            <div class="col-12 col-sm-6 col-md-4 form-group">
              <label><?php echo JrTexto::_('Idpostulante');?> <span class="required"> (*) </span></label>              
               <input type="number" id="idpostulante<?php echo $idgui; ?>"  name="idpostulante" step="1" value="<?php echo !empty($frm["idpostulante"])?$frm["idpostulante"]:1;?>"  min="1" class="form-control col-md-7 col-xs-12" required />
                          </div>
            
    </div>
        <div class="row"> 
            <div class="col-12 form-group text-center">
              <hr>
              <button id="btn-saveBolsa_postulante" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_postulante'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button>              
            </div>
        </div>
        </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  
  $('#fecharegistro<?php echo $idgui; ?>').datetimepicker({ /*lang:'es', timepicker:false,*/ format:'YYYY/MM/DD'}); 
            
$('#frm-<?php echo $idgui;?>').bind({    
     submit: function(ev){
      ev.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'bolsa_postulante', 'saveBolsa_postulante', xajax.getFormValues('frm-<?php echo $idgui;?>'));
      if(res){
       <?php if(!empty($fcall)){ ?> if(typeof <?php echo $fcall?> == 'function'){
          <?php echo $fcall?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else <?php } ?>  return redir('<?php echo JrAplicacion::getJrUrl(array("Bolsa_postulante"))?>');
      }
     }
  }); 
  
});

</script>