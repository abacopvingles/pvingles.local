<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?>
<style>
.topnom{
    padding:0.5ex 1ex;
    color:#fff;
}
.bloqmenu{
    position:absolute;
    top:1ex;
    right:1ex;
}
.item-curso{
    height: 170px;
    display: block;
    margin: 1em 1.5em;
    border-radius: 1ex;
    -webkit-box-shadow: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);
    -moz-box-shadow: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);
    background-color: 4px -7px 5px 3px rgba(19, 18, 18, 0.7803921568627451);
    position:relative;
    cursor:pointer;
}
.item-curso a{
    /*color: #fff;
    position: absolute;
    right:0px;*/
    display:none;

}
.item-curso:hover a{
    display:inline-block;
    position:relative;
    z-index:999;
    /*position: absolute;
    color:#ff9;*/
}
.cursoimage{
    position: absolute;   
    width: 100%;
    height: 90%;
    background-position: center;
    background-size: 80% 70%;
    background-repeat: no-repeat;
    top: -10px;
}
.nomcurso{
    position: absolute;
    bottom: -5px;
    width: 100%;
    background: #6799e62e;
    color: #b70707;
    padding: 0.25ex;
    border-bottom-left-radius: 0.8ex;
    border-bottom-right-radius: 0.8ex;
}


</style>
<div class="row bg-primary">
    <div class="col-6 text-left topnom"><h3>Mis Cursos</h3></div>
    <!--div class="col-6 text-right topnom">
        <a href="<?php echo $this->documento->getUrlBase();?>/cursos/crear" class="btn btn-success">
			<i class="fa fa-plus "></i> <span class="bolder">Nuevo Curso</span>
		</a>
        <a href="" class="btn btn-warning">
			<i class="fa fa-plus "></i> <span class="bolder">ver categorias</span>
		</a>        
    </div-->
</div>
<div class="row" id="lscursos">
    
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var idproyecto=parseInt('<?php echo $this->idproyecto;?>');
$(document).ready(function(){
    var cargarcursos=function(turl){
        var tmpurl=url_media+'/acad_grupoauladetalle/buscarjson';
        __sysajax({ // cargar informacion de usuario;
            fromdata:{idproyecto:idproyecto,iddocente:parseInt('<?php echo $this->curusuario["idpersona"];?>')},
            showmsjok:false,
            url:tmpurl,
            callback:function(rs){
               dt=rs.data;
               html='';
               $.each(dt,function(i,v){
                  // console.log(v);
                   html+='<div class="col-md-3 col-sm-4 col-xs-6 text-center"><div class="">';
                   html+='<div class="item-curso hvr-grow" style="background-color:'+v.color+'" idcurso="'+v.idcurso+'">'
                   html+='<div class="cursoimage" style="background-image:url('+url_media+v.imagen+');"></div>';
                   //html+='<a href="#" class="btn btn-sm btn-warning edit" idcurso="'+v.idcurso+'" alt="Editar curso"><i class="fa fa-pencil"></i></a>';
                   //html+='<a href="#" class="btn btn-sm btn-danger remove" idcurso="'+v.idcurso+'" alt="Eliminar curso"><i class="fa fa-trash"></i></a>';
                   html+='<h4 class="nomcurso">'+v.strcurso+'</h4></div>';
                   html+='</div></div>';
               });
               $('#lscursos').html(html);
            }
        });
    }

    $('#lscursos').on('click','.item-curso .edit',function(ev){
        ev.preventDefault();
        ev.stopPropagation();
        var id=$(this).attr('idcurso');
        window.location.href=_sysUrlBase_+'/cursos/crear?idcurso='+id;
    })
    $('#lscursos').on('click','.item-curso',function(ev){        
        var id=$(this).attr('idcurso');
        window.open(url_media+'/smartcourse/cursos/ver/?idcurso='+id, '_blank');
    })
    cargarcursos();
})
</script>