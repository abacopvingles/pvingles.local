<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
tr.trclone{
    display:none;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%">25% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
                <li class="active "><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="active selected" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="" ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active" role="tabpanel" id="pasocate">
                <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">
                    <div class="card shadow">
                        <div class="card-body">	
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label style=""><?php echo JrTexto::_('Categorias');?></label> 
                                    <div class="cajaselect">
                                    <select name="categorias" id="categoria" class="form-control">                                       
                                    </select>
                                    </div>		            					           
                                </div>
                                <div class="col-md-4 form-group">							        	
                                    <label style=""><?php echo JrTexto::_('Subcategoria');?></label> 
                                    <div class="cajaselect">
                                    <select name="subcategorias" id="subcategoria" class="form-control">
                                        <option value="0">Todas las subcategorias</option>                                        
                                    </select>
                                    </div>			           
                                </div>
                                <div class="col-md-4 form-group">
                                    <div><br></div> 
                                    <span class="btn btn-primary addcategoria"><i class="fa fa-plus"></i> Agregar</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <table class="table table-striped tablecategorias">
                                <tr><th colspan="2">Categorias Agregadas</th></tr>
                                <tr class="trclone"><td class="text-left">aaa</td><td><i class="fa fa-trash removetcat"></i></td></tr>          
                                </table>    		
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-center">                            
                            <button type="button" onclick="history.back();" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
                            <a class="btn btn-primary" href="<?php echo $this->documento->getUrlBase() ?>/cursos/crear/?pg=paso3&idcurso=<?php echo $this->idcurso; ?>" >Continuar <i class=" fa fa-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>              
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:''},
    infoindice:'top',
    infoavance:10
}

$(document).ready(function(){
    var idcurso=$('#idcurso').val();
    var tmpcategorias={};
    $('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			var sel=$('ul#ultabs').find('li.selected').children('a');
			var lia=$(this);
			var liatxt=lia.attr('data-show');
			var liav=parseInt(lia.attr('data-value')||0);
			var avtxt=sel.attr('data-show');			
			var av=parseInt(sel.attr('data-value')||0);
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})
    $('select#categoria').on('change',function(ev){
        var id=parseInt($(this).val());	
        $.each(tmpcategorias,function(i,v){
            var idcat=parseInt(v.idcategoria);
            if(idcat==id){
                var cat=v.hijos;
                $('select#subcategoria').find('option').remove();
                if(cat.length>0){
                    $.each(cat,function(i,v){							
                        var op='<option value="'+v.idcategoria+'">'+v.nombre+'</option>';
                        $('select#subcategoria').append(op);
                    })
                }else{
                    var op='<option value="0">Todas las subcategorias</option>';
                    $('select#subcategoria').append(op);
                }
            }
        })
    });

    $('.addcategoria').on('click',function(ev){
			ev.preventDefault();
			var pnlcat=$('table.tablecategorias');
			var trclone=pnlcat.find('.trclone').clone(true);
			var cat=$('select#categoria');
			var scat=$('select#subcategoria');
			var idcate=parseInt(cat.val()||0);
			var idscat=parseInt(scat.val()||0);
			var txt='';
			if(idscat>0){
				txt=cat.find('option:selected').text()+' -> '+scat.find('option:selected').text();
				idcate=idscat;
				txtcat=scat.find('option:selected').text();
			}else{
				txt=cat.find('option:selected').text();
				idcate=parseInt(cat.val());
				txtcat=cat.find('option:selected').text();
			}			
			__sysajax({
            	fromdata:{idcat:idcate,idcur:idcurso,acc:'I'},				
				showmsjok:false,
	            url:url_media+'/smartcourse/acad_curso/guardarcategorias',
	            callback:function(rs){          	
	            	var id=rs.data;	            	        	
					trclone.removeClass('trclone');
					trclone.children('td:first').text(txt);
					trclone.attr('idcursocategoria',id);
					pnlcat.append(trclone);
                    updateavance(parseInt($('ul#ultabs a[data-show="paso2"]').attr('data-value')));			
				}	            
			});					
		})
        $('table.tablecategorias').on('click','.removetcat',function(ev){
			var tr=$(this).closest('tr');
			var idcat=tr.attr('idcursocategoria');
			 __sysajax({
            	fromdata:{id:idcat,acc:'D'},
				showmsjok:false,
	            url:url_media+'/smartcourse/acad_curso/guardarcategorias',
	            callback:function(rs){	            	
	            	tr.remove();
	            }	            
			});
		});

     var updatetabs=false;
    var updateavance=function(newval){
        var vactual=$('.progresscurso').first().attr('aria-value');
        vactual=parseFloat(vactual);
        newval=parseFloat(newval);
        if(vactual<newval){
            $('.progresscurso').first().attr('aria-value',newval);
            $('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
            $('.progresscurso').siblings().css({width:(100 - newval)+'%'});            
            jsoncurso.infoavance=newval;
            var txtjson=JSON.stringify(jsoncurso);
            __sysajax({   
                fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
                url:url_media+'/smartcourse/acad_curso/setCampojson',  
                showmsjok:false 
            })            
        }        
        if(updatetabs==false){
            updatetabs=true;
            $.each($('ul#ultabs').find('a'),function(i,v){
                var va=parseInt($(v).attr('data-value')||0);
                if(va<=newval) $(v).closest('li').addClass('active');
            })
        }
    }


    __sysajax({
        fromdata:{rjson:'si',idcurso:idcurso},
        showmsjok:false,
        url:url_media+'/smartcourse/cursos/paso1categorias',					
        callback:function(rs){
            var dt=rs.categorias;
            var catcurso=rs.categoriascurso;
            tmpcategorias=dt;
            if(dt.length>0){
                $('select#categoria').find('option').remove();
                $.each(dt,function(i,v){                       							
                    var op='<option value="'+v.idcategoria+'">'+v.nombre+'</option>';
                    $('select#categoria').append(op);
                })
                $('select#categoria').trigger('change');
            }else{
                var op='<option value="0">Todas las categorias</option>';
                $('select#categoria').html(op);
            }
            if(catcurso.length>0){               
                var tbcategorias=$('table.tablecategorias');
                $.each(catcurso,function(i,v){                  					
                    $.each(tmpcategorias,function(ii,vv){
                        if(v.idcategoria==vv.idcategoria){										
                            var trclone=tbcategorias.find('.trclone').clone(true);
                            trclone.removeClass('trclone');
                            trclone.children('td').first().text(v.categoria);
                            trclone.attr('idcursocategoria',v.idcursocategoria);
                            tbcategorias.append(trclone);
                        }else if(vv.hijos.length>0){
                            $.each(vv.hijos,function(hi,hv){
                                if(hv.idcategoria==v.idcategoria){
                                    var trclone=tbcategorias.find('.trclone').clone(true);
                                    trclone.removeClass('trclone');
                                    trclone.children('td:first').text(vv.nombre +' -> '+v.categoria);
                                    trclone.attr('idcursocategoria',v.idcursocategoria);
                                    tbcategorias.append(trclone);
                                }
                            })
                        }
                    });
                });
            }
        }
    });


    if(parseInt(idcurso)>0){
        __sysajax({
            fromdata:{idcurso:parseInt(idcurso)},
            showmsjok:false,
            url:url_media+'/smartcourse/acad_curso/buscarjson',					
            callback:function(rs){
               var dt=rs.data;
                if(dt.length>0){
                    var info=rs.data[0];                    
                    var jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
                    $.extend(jsoncurso,jsontmp);
                    updateavance(jsoncurso.infoavance);
                } 
            }
        });
    }
})
</script>