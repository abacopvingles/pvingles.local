<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
	.cicon {
    font-size: 1em;
    padding: 1ex;
    text-align: center;
    position: relative;
}
.cicon span{
	font-size:0.65em;
}
.minicolors-theme-default.minicolors{
	display:block !important;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 1.5ex; margin-top: 1ex;">
            <div class="progress">
                <div class="progresscurso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-value="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">75% completado</div>
                <div class="progress-bar bg-warning" role="progressbar" aria-value="100" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="wizard">
                <ul class="nav nav-wizard" id="ultabs">
				<li class="active"><a href="#" data-value="10" data-show="paso1"><b class="number">1.</b> Datos del curso </a></li>
                    <li class="active" ><a href="#" data-value="20" data-show="paso2"><b class="number">2.</b> Categorias </a></li>
                    <li class="active" ><a href="#" data-value="30" data-show="paso3"><b class="number">3.</b> Estructura </a></li>
                    <li class="active " ><a href="#" data-value="50" data-show="paso4"><b class="number">4.</b> Sesiones </a></li>
                    <li class="active" ><a href="#" data-value="60" data-show="portada"><b class="number">5.</b> Portada </a></li>
                    <li class="active" ><a href="#" data-value="70" data-show="indice"><b class="number">6.</b> Indice </a></li>
                    <li class="active selected" ><a href="#" data-value="80" data-show="contenido"><b class="number">7.</b> Contenidos </a></li>
                    <li class="" ><a href="#" data-value="100" data-show="publicar"><b class="number">8.</b> Publicar </a></li>
                </ul>
            </div>
            <div class="tab-content shadow" id="panelesultabs">
                <div class="tab-pane active" role="tabpanel" id="pasocontenidos">
				<input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idcurso;?>">
				<input type="hidden" name="idcursodetalle" id="idcursodetalle" value="<?php echo @$_REQUEST["idcursodetalle"];?>">
                <div class="card text-center" id="cardcontenidos">
                    <div class="card-body" id="showtemaidedit">
                        <div class="row btnmanagermenu">
							<div class="col-md-12">
								<h4 class="nomtema"></h4>
							</div>
                            <div class="col-md-12 _selcontenido">
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<a href="javascript:void(0)" data-showp="#showpaddcontenido" class="btntemaaddinfo btn btn-block btn-secondary" style="color:#fff">
											<div><i class="fa fa-cog fa-2x "></i></div>												
											<p class="bolder">Agregar y ver contenido</p>
										</a>
									</div>                              
									<div class="col-md-4 col-sm-6 col-xs-12">
										<a href="javascript:void(0)" data-showp="#showpaddopciones" class="btntemaaddinfo btn btn-block btn-secondary" style="color:#fff">
											<div><i class="fa fa-cogs fa-2x "></i></div>												
											<p class="bolder">Agregar y ver opciones</p>
										</a>
									</div>									                                 
								</div>
                            </div>
                            <div class="col-md-12 _addopciones" id="showpaddopciones" style="display:none">
								<div class="row _opcionespestania active" id="opciontipomenu">
									<div class="col-md-12"><br>Selecione tipo de opciones<hr></div>
									<div class="col-md-4 col-sm-6 col-xs-12 text-center">
										<h6>Pestañas Arriba</h6>
										<a href="javascript:void(0)" data-pestania="arriba" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
											<img src="<?php echo URL_MEDIA; ?>/smartcourse/static/media/web/indice/oparriba.png" class="border img-thumbnail img-responsive">
										</a>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12 text-center">
										<h6>Pestañas Derecha</h6>
										<a href="javascript:void(0)" data-pestania="derecha" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
											<img src="<?php echo URL_MEDIA; ?>/smartcourse/static/media/web/indice/opderecha.png" class="border img-thumbnail img-responsive">
										</a>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12 text-center">
										<h6>Pestañas Abajo</h6>
										<a href="javascript:void(0)" data-pestania="abajo" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
											<img src="<?php echo URL_MEDIA; ?>/smartcourse/static/media/web/indice/opabajo.png" class="border img-thumbnail img-responsive">
										</a>
									</div>
									<div class="col-md-12"><br></div>
									<div class="col-md-4 col-sm-6 col-xs-12 text-center">
										<h6>Circulos</h6>
										<a href="javascript:void(0)"  data-pestania="circulo" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
											<img src="<?php echo URL_MEDIA; ?>/smartcourse/static/media/web/indice/opcirculo.png" class="border img-thumbnail img-responsive">
										</a>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12 text-center">
										<h6>Menus</h6>
										<a href="javascript:void(0)"  data-pestania="menus" data-showp="#opcionlistadopestanias" data-pclase="_opcionespestania" class="btn btnopcionespestania">
											<img src="<?php echo URL_MEDIA; ?>/smartcourse/static/media/web/indice/opmenu.png" class="border img-thumbnail img-responsive">
										</a>
									</div>
									<div class="col-md-12 text-center text-muted"><hr></div>
									<div class="col-md-12 text-center text-muted">
										<button type="button" class="returnaddcontenido btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
										<button type="button" class="btnnexshowopciones btn btn-primary">Continuar <i class="fa fa-arrow-right"></i></button>	
									</div>
								</div>
								<div class="col-md-12 _opcionespestania" id="opcionlistadopestanias" style="display:none">
									<div class="row" style="width: 100%;">
										<div class="col-md-12 table-responsive" id="verlistadoopciones">								
											<table class="table table-striped">
												<tr class="nocontar"><th colspan="2" class="text-center">Opciones</th></tr>
											</table>
											<div class="col-md-12 text-center" style="margin: 1.5ex auto;"><button type="button" id="btnaddpestania"  class=" btn btn-warning"><i class="fa fa-plus"></i> Agregar </button></div>
											<div class="col-md-12 text-center"><hr></div>
											<div class="col-md-12 text-center text-muted">
												<button type="button" class="btnreturnopciontipomenu btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
												<button type="button" class="btnnexcontenido btn btn-primary">Continuar <i class="fa fa-arrow-right"></i></button>	
											</div>								
										</div>
										<div class="col-md-12" style="display: none;" id="editlistadoopcion">
											<div class="row">
											<div class="col-md-12"><hr></div>
												<div class="col-md-6 col-sm-12">
													<div class="row">
														<div class="col-md-12">
															<h4>Menu</h4>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 form-group text-left">
															<label style=""><?php echo JrTexto::_('Nombre');?></label> 
															<input type="text" name="nombre" class="form-control" placeholder="Objetivos/sesion/video/manual" >			           
														</div>
														<div class="col-md-6 col-sm-12">
															<div class="row">
																<div class="col-md-12 col-sm-12 form-group text-left">						
																	<label style=""><?php echo JrTexto::_('Color de texto');?> </label>		<br>												
																	<div><input type="" name="color" value="rgba(0,0,0,1)" class="vercolor colortexto form-control"></div>
																</div>														
																<div class="col-md-12  text-left ">		        	
																	<label style=""><?php echo JrTexto::_('Color de Fondo');?> </label> <br>
																	<div><input type="" name="colorfondo" value="rgba(0,0,0,1)" class="vercolor colorfondo form-control" ></div>			       
																</div>
															</div>
														</div>
														<div class="col-md-6 col-sm-12">
														<label><?php echo JrTexto::_("Imagen de menu") ?> </label>                
															<div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
																<div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
																<div id="sysfilelogo">
																<img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfiletema img-fluid center-block" data-type="imagen"  id="imagenopcion" style="max-width: 180px; max-height: 80px;">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-12">
													<div class="row">
														<div class="col-md-12">
															<h4>Pagina</h4>
														</div>
													</div>
													<div class="row">
													<div class="col-md-12">
														<label><?php echo JrTexto::_("Imagen de fondo") ?> </label>                
															<div style="position: relative;" class="frmchangeimage text-center" id="contenedorimagen">
																<div class="toolbarmouse text-center"><span class="btn btnremoveimage"><i class="fa fa-trash"></i></span></div>
																<div id="sysfilelogo">
																<img src="<?php echo URL_MEDIA.$imgcursodefecto;?>" class="__subirfiletema img-fluid center-block" data-type="imagen"  id="imagenpagina" style="max-width: 200px; max-height: 150px;">
																</div>
															</div>
														</div>
														<div class="col-md-12">		        	
															<label style=""><?php echo JrTexto::_('Color de Fondo');?> </label> <br>
															<div><input type="" name="colorfondopagina" value="rgba(0,0,0,0)" class="vercolor colorfondopagina form-control" ></div>			       
														</div>
													</div>														
												</div>																					
											</div>
											<div class="row" >										
												<div class="col-md-12" id="aquitambienaddcontent">
													
												</div>
												<div class="col-md-12 text-center text-muted">
													<hr>
													<button type="button" id="editpestaniacancel" class="btn btn-warning"><i class="fa fa-cancel"></i> Cancelar </button>
													<button type="button" id="editpestaniasave" class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>	
												</div>
											</div>								
										</div>								
									</div>
								</div>						
							</div>
							<div class="col-md-12 _addcontenido" id="showpaddcontenido" style="display:none">
								<div class="col-md-12">
									<div class="row" id="addcontentclone">
										<div class="col-md-12 text-left ">		        	
											<br><label style="">Seleccione plantilla de contenido</label>
											<hr>			       
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="texto" title="Texto" data-content="Agregar texto" data-acc="texto">				
											<i class="fa btn btn-block btn-primary fa-text-width fa-2x "> <br><span>Texto</span> </i>
										</div>				
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="imagen" title="Imagen" data-content="Agregar una imagen" data-acc="imagen">
											<i class="fa btn btn-block btn-primary fa-file-image-o fa-2x "> <br><span>Imagen</span> </i>
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="audio" title="Audio" data-content="Agregar un audio" data-acc="audio">
											<i class="fa btn btn-block btn-primary fa-file-audio-o fa-2x "> <br><span>Audio</span></i> 
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="video" title="Video" data-content="agregar un video" data-acc="video">
											<i class="fa btn btn-block btn-primary  fa-file-movie-o fa-2x "> <br><span>Video</span> </i>
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="pdf" title="PDF" data-content="Agregar un archivo pdf" data-acc="pdf">
											<i class="fa btn btn-block btn-primary  fa-file-pdf-o fa-2x "> <br><span>PDF</span></i> 
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="html" title="HTML" data-content="Agregar contenido HTML - seleccione un zip si incluye mas contenido" data-acc="html">
											<i class="fa btn btn-block btn-primary fa-file-code-o fa-2x "> <br><span>Html</span></i> 
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover" data-placement="top" data-type="flash" title="Flash" data-content="Agregar SWF - archivo Flash" data-acc="flash">
											<i class="fa btn btn-block btn-primary fa-file-archive-o fa-2x "> <br><span>flash</span></i>
										</div>				
										<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlock" title="SmartTic Look" data-content="Look" data-acc="smarticlock">
											<i class="fa btn btn-block btn-primary fa-eye fa-2x "><br> <span> Mirar </span> </i>
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticlookPractice" title="SmartTic Practice" data-content="Practice" data-acc="smarticlookPractice">
											<i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> Practicar</span></i>
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="smarticDobyyourselft" title="SmartTic Do by yourselft" data-content="Do it by yourself" data-acc="smarticDobyyourselft">
											<i class="fa btn btn-block btn-primary fa-cogs fa-2x "><br> <span> hazlo tu mismo</span></i>
										</div>
										<div class="col-md-3 col-sm-6 cicon verpopover " data-placement="top" data-type="game" title="Juegos" data-content="Agregar juegos interactivos" data-acc="game">
											<i class="fa btn btn-block btn-primary fa-gamepad fa-2x "><br><span>Juegos</span> </i>
										</div>
										<div class="col-md-3 col-sm-6  cicon verpopover " data-placement="top" data-type="smartquiz" title="SmartQuiz" data-content="Agregar Examenes" data-acc="smartquiz">
											<i class="fa btn btn-block btn-primary fa-tasks fa-2x "> <br><span>Examenes</span></i>
										</div>				
									</div>
									<div class="row" style="margin-top: 1ex;">								
										<div class="col-md-12 text-center text-muted" >
											<button type="button" data-showp="#addtemacontenido" class="returnaddcontenido btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
											<button type="button" data-showp="#showppublicar" data-elegir="btnopcionespestania" class="btnnexcontenido btn btn-primary"> Continuar <i class="fa fa-arrow-right"></i></button>
										</div>
									</div>
								</div>		
							</div>
                        </div>
                        
                    </div>
                    <div class="card-footer text-muted pnlfooter">
                        <button type="button" onclick="history.back()" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Anterior </button>
                        <button type="button" class="btnnexttab btn btn-primary">Continuar <i class="fa fa-arrow-right"></i></button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');

var jsoncurso={
    plantilla:{id:0,nombre:'blanco'},
    estructura:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    estilopagina:{'font-family':'arial','font-size':'12px',color:'rgba(0,0,0,1)','background-color':'rgba(0,0,0,0)','background-image':'',image:''},
    infoportada:{titulo:'',descripcion:'',image:'',colorfondo:'',imagenfondo:''},
    infoindice:'top',
    infoavance:70
}
$(document).ready(function(){
	var idcurso=parseInt($('#idcurso').val());
	var idcursodetalle=parseInt($('#idcursodetalle').val()||0);
	var curtema={};
	var temas={};
	$('ul#ultabs').on('click','a',function(ev){
		ev.preventDefault();
		if(idcurso>0){
			var sel=$('ul#ultabs').find('li.selected').children('a');
			var lia=$(this);
			var liatxt=lia.attr('data-show');
			var liav=parseInt(lia.attr('data-value')||0);
			var avtxt=sel.attr('data-show');			
			var av=parseInt(sel.attr('data-value')||0);
			if(jsoncurso.infoavance >= liav && liatxt!=avtxt){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg='+liatxt+'&idproyecto='+idproyecto+'&idcurso='+idcurso;
			}
		}
	})

	$('.btntemaaddinfo').on('click',function(){
		$('.btntemaaddinfo').removeClass('btn-success').addClass('btn-secondary');
		$(this).addClass('btn-success');
		$('.pnlfooter').hide();
		var showp=$(this).attr('data-showp');
		if(curtema.txtjson.tipo=='#showpaddcontenido'){
			$('.cicon[data-type]').children('i').removeClass('btn-success').addClass('btn-primary')
			var it=$('.cicon[data-type="'+curtema.txtjson.typelink+'"]');
				it.children('i').addClass('btn-success').removeClass('btn-primary');
				it.attr('data-link',curtema.txtjson.link||'');				
				it.attr('data-oldmedia',curtema.txtjson.link||'');
		}else{
			var tipofile=curtema.txtjson.tipofile||'arriba';
			$('#opciontipomenu').find('a[data-pestania]').removeClass('active btn-success');
			$('#opciontipomenu').find('a[data-pestania="'+tipofile+'"]').addClass('active btn-success');
			curtema.txtjson.tipofile=tipofile;
			$('#opcionlistadopestanias').hide();
			$('#opciontipomenu').show();
		}
		curtema.txtjson.tipo=showp;
		$('._selcontenido').hide();				
		$(showp).fadeIn();
	})
	$('.returnaddcontenido').on('click',function(){
		$('._selcontenido').fadeIn();
		$('#showpaddcontenido').hide();		
		$('#opciontipomenu').hide();
		$('.pnlfooter').show();
	})
	$('.btnnexcontenido').on('click',function(){
		_updatejsontema();
		var nexttema=_siguienteteam();		
		if(nexttema==false){			
			updateavance(90)
			setTimeout(function(){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg=publicar&idproyecto='+idproyecto+'&idcurso='+idcurso;
			},1000);
		}else{		
			$('#showtemaidedit ._selcontenido').show();
			$('#showtemaidedit .nomtema').html('<b>Tema :</b>'+curtema.nombre);
			$('#showpaddopciones').hide();
			$('#showpaddcontenido').hide();
			$('.btntemaaddinfo').removeClass('btn-success');
			if(curtema.txtjson.tipo!=undefined){
				$('.btntemaaddinfo[data-showp="'+curtema.txtjson.tipo+'"]').removeClass('btn-secondary').addClass('btn-success');
			}
			$('.pnlfooter').show();
		}
	})
	$('.btnnexshowopciones').on('click',function(){
		curtema.txtjson.infoavancetema=0;		
		var pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
		var edittmp=pnltmp.children('#editlistadoopcion');
		var pnltable=$('#verlistadoopciones').children('table').children('tbody');
		var options=curtema.txtjson.options||'';
		pnltable.children('tr.nocontar').siblings('tr').remove();
		if(options!='')				
			$.each(options,function(i,v){
				var trclone='<tr data-imagenfondopagina="'+v.imagenfondopagina+'"  data-imagenfondo="'+v.imagenfondo+'"  data-colorfondopagina="'+v.colorfondopagina+'" data-id="'+v.id+'" data-link="'+v.link+'" data-type="'+v.type+'" data-color="'+v.color+'" data-colorfondo="'+v.colorfondo+'"><td class="nombre">'+v.nombre+'</td><td><span class="temadetedit" style="padding:0.5ex;"><i class=" fa fa-pencil btn btn-sm btn-warning "></i></span><span class="temadetremove" style="padding:0.5ex;"><i class="btn btn-sm btn-danger fa fa-trash"></i></span></td></tr>';
				pnltable.append(trclone);
			})
		$('#aquitambienaddcontent').html('<div class="row">'+$('#addcontentclone').html()+'</div>');
		//console.log(curtema);
		$('#opciontipomenu').hide();
		$('#opcionlistadopestanias').fadeIn();
	})
	$('.btnopcionespestania').on('click',function(){
		$(this).closest('#opciontipomenu').find('.btnopcionespestania').removeClass('active btn-success');
		$(this).addClass('active btn-success');
		var tipofile=$(this).attr('data-pestania');			
		curtema.txtjson.tipofile=tipofile;
		$('#aquitambienaddcontent').html('<div class="row">'+$('#addcontentclone').html()+'</div>');
		//console.log(curtema);
		_updatejsontema();		
	})

	$('#btnaddpestania').on('click',function(ev){	
		var pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
		pnltmp.children('div').fadeOut().hide();
		var edittmp=pnltmp.children('#editlistadoopcion');			
		edittmp.attr('data-idedit',__idgui());
		edittmp.find('input[name="nombre"]').val('');
		edittmp.find('input[name="color"]').val('rgba(0,0,0,1)');
		edittmp.find('input[name="color"]').minicolors('settings',{value:'rgba(0,0,0,1)'});
		edittmp.find('input[name="colorfondo"]').val('rgba(255, 198, 83,1)');
		edittmp.find('input[name="colorfondo"]').minicolors('settings',{value:'rgba(255, 198, 83,1)'});
		edittmp.find('.cicon').removeClass('active').children('i').removeClass('btn-success').addClass('btn-primary');
		edittmp.fadeIn().show();
	})
	$('.btnreturnopciontipomenu').on('click',function(){
		$('#opcionlistadopestanias').hide();
		$('#opciontipomenu').fadeIn();
	})
	$('#editpestaniacancel').on('click',function(ev){		
		$('#verlistadoopciones').fadeIn();
		$('#editlistadoopcion').hide();
	})
	$('#editpestaniasave').on('click',function(ev){
		var pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
		var edittmp=pnltmp.children('#editlistadoopcion');
		var pnltable=$('#verlistadoopciones').children('table').children('tbody');
		var btnfileactive=edittmp.find('.cicon.active');
		var option={
			id:edittmp.attr('data-idedit'),
			nombre:edittmp.find('input[name="nombre"]').val(),
			color:edittmp.find('input[name="color"]').val(),
			colorfondo:edittmp.find('input[name="colorfondo"]').val(),
			imagenfondo:  edittmp.find('img#imagenopcion').attr('data-src')||'',
			colorfondopagina: edittmp.find('input.colorfondopagina').val(),
			imagenfondopagina:edittmp.find('img#imagenpagina').attr('data-src')||'',
			link:btnfileactive.attr('data-link')||'',
			type:btnfileactive.attr('data-type')||''
		}
		var trexite=pnltable.children('tr[data-id="'+option.id+'"]');
		if(trexite.length==0){
			var trclone='<tr data-colorfondopagina="'+option.colorfondopagina+'" data-imagenfondopagina="'+option.imagenfondopagina+'" data-imagenfondo="'+option.imagenfondo+'" data-id="'+option.id+'" data-link="'+option.link+'" data-type="'+option.type+'" data-color="'+option.color+'" data-colorfondo="'+option.colorfondo+'"><td class="nombre">'+option.nombre+'</td><td><span class="temadetedit" style="padding:0.5ex;"><i class=" fa fa-pencil btn btn-sm btn-warning "></i></span><span class="temadetremove" style="padding:0.5ex;"><i class="btn btn-sm btn-danger fa fa-trash"></i></span></td></tr>';
			pnltable.append(trclone);
		}else {
			trexite.attr('data-link',option.link);
			trexite.attr('data-type',option.type);
			trexite.attr('data-color',option.color);
			trexite.attr('data-colorfondo',option.colorfondo);
			trexite.children('td.nombre').text(option.nombre);
			trexite.attr('data-colorfondopagina',option.colorfondopagina);
			trexite.attr('data-imagenfondopagina',option.imagenfondopagina);
			trexite.attr('data-imagenfondo',option.imagenfondo);
		}

		var txtoptionjson={};
		pnltable.find('tr').each(function(i,v){
			tmptr=$(v);
			if(i>0)
			txtoptionjson[i]={
				nombre:tmptr.children('td.nombre').text(),
				id:tmptr.attr('data-id'),
				link:tmptr.attr('data-link'),
				type:tmptr.attr('data-type'),
				color:tmptr.attr('data-color'),
				colorfondo:tmptr.attr('data-colorfondo'),
				colorfondopagina:tmptr.attr('data-colorfondopagina'),
				imagenfondo:tmptr.attr('data-imagenfondo'),
				imagenfondopagina:tmptr.attr('data-imagenfondopagina')
			}
		})
		curtema.txtjson.options=txtoptionjson;				
		_updatejsontema();
		pnltmp.children('div').fadeOut().hide();
		pnltmp.children('#verlistadoopciones').fadeIn().show();
	})


	$('#verlistadoopciones').on('click','.temadetedit',function(ev){
		var pnltmp=$(this).closest('#opcionlistadopestanias').children('div');
		var edittmp=pnltmp.children('#editlistadoopcion');
		pnltmp.children('div').fadeOut().hide();
		var tr=$(this).closest('tr');
		edittmp.attr('data-idedit',tr.attr('data-id'));
		edittmp.find('input[name="nombre"]').val(tr.children('.nombre').text());
		edittmp.find('input[name="color"]').val(tr.attr('data-color'));
		edittmp.find('input[name="color"]').minicolors('settings',{value:tr.attr('data-color')});
		edittmp.find('input[name="colorfondo"]').val(tr.attr('data-colorfondo'));
		edittmp.find('input[name="colorfondo"]').minicolors('settings',{value:tr.attr('data-colorfondo')});
		var img=tr.attr('data-imagenfondo')||imgdefecto;
		img=img.replace(url_media,'');
		img=img=='undefined'?imgdefecto:img;
		var tmpimg=img.replace(url_media,'');
		var tmpimg2=url_media+tmpimg;
		edittmp.find('img#imagenopcion').attr('data-src',tmpimg).attr('src',tmpimg2);
		edittmp.find('input.colorfondopagina').val(tr.attr('data-colorfondopagina'));
		edittmp.find('input.colorfondopagina').minicolors('settings',{value:tr.attr('data-colorfondopagina')});

		var img2=tr.attr('data-imagenfondopagina')||imgdefecto;
		img2=img2.replace(url_media,'');
		img2=img2=='undefined'?imgdefecto:img2;
		var tmpimg2a=img2.replace(url_media,'');
		var tmpimg22=url_media+tmpimg2a;
		
		edittmp.find('img#imagenpagina').attr('data-src',tmpimg2a).attr('src',tmpimg22),

		edittmp.find('.cicon').removeClass('active').children('i').removeClass('btn-success').addClass('btn-primary');
		edittmp.find('.cicon[data-type="'+tr.attr('data-type')+'"]').addClass('active').children('i').addClass('btn-success').removeClass('btn-primary');
		edittmp.find('.cicon[data-type="'+tr.attr('data-type')+'"]').attr('data-link',tr.attr('data-link'));
		edittmp.fadeIn().show();
	}).on('click','.temadetremove',function(ev){			
		$(this).closest('tr').remove();
		var pnltable=$('#verlistadoopciones').children('table').children('tbody');
		var txtoptionjson={};
		pnltable.find('tr').each(function(i,v){
			tmptr=$(v);
			if(i>0)
			txtoptionjson[i]={
				nombre:tmptr.children('td.nombre').text(),
				id:tmptr.attr('data-id'),
				link:tmptr.attr('data-link'),
				type:tmptr.attr('data-type'),
				color:tmptr.attr('data-color'),
				colorfondo:tmptr.attr('data-colorfondo'),
				colorfondopagina:tmptr.attr('data-colorfondopagina'),
				imagenfondo:tmptr.attr('data-imagenfondo'),
				imagenfondopagina:tmptr.attr('data-imagenfondopagina')
			}
		})		
		curtema.txtjson.options=txtoptionjson;			
		_updatejsontema();
	})

	var addtemas=function(donde,temas){
			$.each(temas,function(i,t){
				//console.log(t);
				var dt={hijos:{},imagen:imgdefecto};
				$.extend(dt,t);				
				//if(.hijos==undefined) t.hijos={};
				var hijos=dt.hijos||[];
				var liclonado=$('#plnindice').children('li.paraclonar').clone(true);
				liclonado.removeAttr('style').removeClass('paraclonar');
				liclonado.attr('id','me_'+dt.idcursodetalle);
				liclonado.attr('data-idrecurso',dt.idrecurso);
				liclonado.attr('data-idcursodetalle',dt.idcursodetalle);
				liclonado.attr('data-idpadre',dt.idpadre);
				liclonado.attr('data-orden',dt.orden);
				liclonado.attr('data-tipo',dt.tiporecurso);
				liclonado.attr('data-imagen',dt.imagen);
				liclonado.attr('data-esfinal',dt.esfinal);
				liclonado.children('div').children('span.mnombre').text(dt.nombre);				
				if(hijos.length){
					liclonado.addClass('hijos');
					liclonado.append('<ul id="me_hijo'+dt.idcursodetalle+'"></ul>');					
					addtemas(liclonado.find('#me_hijo'+dt.idcursodetalle),hijos);
				}
				donde.append(liclonado);
			})
			_fnordenar();
		}
		var _buscartemaremove=function(itemas,idcurdet){
			var icont=false;
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==idcurdet){		
						itemas.splice(i, i);
						icont=true;
						return true;
					}else{
						var ic=_buscartemaremove(cur.hijos,idcurdet);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
		var _modificartema=function(itemas,iddet,dt,edit){
			var icont=false;
			if(itemas!=undefined)
			if(itemas.length>0){				
				itemas.forEach(function(cur,i,v){
					if(icont==false)
					if(itemas[i].idcursodetalle==iddet){
						if(edit) $.extend(itemas[i],dt);
						else{
							if(itemas[i]['hijos']==undefined)itemas[i]['hijos']=[];
							//console.log('eee',itemas[i]['hijos'],cur,i);
							itemas[i]['hijos'].push(dt);
						}
						icont=true;
						return true;
					}else{
						var ic=_modificartema(itemas[i].hijos,iddet,dt,edit);
						if(ic==true){
							icont=true;
							return true;
						}
					}
				})
			}
		}
	$('.__subirfiletema').on('click',function(){
        var $img=$(this);            
        __subirfile({file:$img,typefile:'imagen',uploadtmp:true,guardar:true,dirmedia:'cursos/curso_'+idcurso+'/'},function(rs){                    
			var f=rs.media.replace(url_media, '');
			img2=f=='/static/media/nofoto.jpg'?'':f;
			$img.attr('data-src',f);
        });
    });
	$('.btnremoveimage').on('click',function(){
        var $img= $(this).closest('#contenedorimagen').find('img');        
        $img.attr('src',imgdefecto).attr('data-src','');
        $('input#imagen').val('');
    })
	
	$('#pasocontenidos').on('click','.btnnexttab',function(ev){
		updateavance(parseInt($('ul#ultabs a[data-show="contenido"]').attr('data-value')));
		var txtjson=JSON.stringify(jsoncurso);		
		__sysajax({
			fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
			url:url_media+'/smartcourse/acad_curso/setCampojson',  
			showmsjok:false,
			callback:function(rs){				
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg=publicar&idproyecto='+idproyecto+'&idcurso='+idcurso;
			} 
		})	
	})

	var __asiganacionfile=function(it){			
		it.siblings('.cicon').removeClass('active').children('i').removeClass('btn-success btn-primary').addClass('btn-primary');
		it.addClass('active').children('i').addClass('btn-success');
		if(curtema.txtjson.tipo=='#showpaddcontenido'){			
			curtema.txtjson.link=it.attr('data-link');
			curtema.txtjson.typelink=it.attr('data-type');
			_updatejsontema();
		}			
	}
	var _updatejsontema=function(){			
		curtema.txtjson.infoavancetema=100;
		updatetema_(temas,curtema.idcursodetalle,curtema);
		var data=new FormData();
		data.append('tb','sesionhtml');
		data.append('id',parseInt(curtema.idcursodetalle));
		data.append('campo','txtjson');
		data.append('valor',JSON.stringify(curtema.txtjson));
		__sysajax({	fromdata:data,	showmsjok:false, url:url_media+'/smartcourse/acad_cursodetalle/setCampojson'});
	}

	var updatejsoncurso=function(){
		var txtjson=JSON.stringify(jsoncurso);
		__sysajax({
			fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
			url:url_media+'/smartcourse/acad_curso/setCampojson',  
			showmsjok:false,
			callback:function(rs){
				window.location.href=_sysUrlBase_+'/cursos/crear/?pg=contenido&idproyecto='+idproyecto+'&idcurso='+idcurso;
			} 
		}) 
	}

	var updatetema_=function(itemas,iddet,tmptema){
		var encontrado=false;
		var rdt={dato:itemas,encontrado:false};			
		if(itemas.length>0){
			itemas.forEach(function(cur,i,v){
				if(encontrado==false){
					if(cur.idcursodetalle==iddet){
						encontrado=true;							
						$.extend(itemas[i],tmptema);							
						rdt.dato=itemas;
						rdt.encontrado=true;
					}else{
						var hijos=cur.hijos||[];
						dt=updatetema_(hijos,iddet,tmptema);
						if(dt.encontrado==true){
							itemas[i].hijos=dt.dato;															
							rdt.dato=itemas;
							rdt.encontrado=true;		
						}
					}
				}
			})
		}			
		return rdt;
	}

	var _nexttema=function(t,id){
		var otrotema=false;
		$.each(t,function(i,cur){			
			if(otrotema==false){
				var curid=parseInt(cur.idcursodetalle);
				if(curid==id){
					var index=parseInt(i)+1;
					if(t[index]!=undefined){
						curtema=t[index];									
						otrotema=true;
					}else if(parseInt(cur.idpadre)>0){
						otrotema=_nexttema(temas,parseInt(cur.idpadre));
					}
				}else if(cur.hijos.length>0){
					otrotema=_nexttema(cur.hijos,id);
				}
			}
		})
		return otrotema;		
	}
	
	var _siguienteteam=function(){
		var tmpcurtema=curtema;
		index=curtema.index;
		var hijos=curtema.hijos||[];
		var iddet=parseInt(curtema.idcursodetalle);
		var curtemaindex=tmpcurtema.index;
		var otrotema=false;			
		if(hijos.length>0){				
			curtema=hijos[0];			
			otrotema=true;
		}else{			
			otrotema=_nexttema(temas,iddet);
		}
		var tmpjson=curtema.txtjson;
		if(typeof(tmpjson)=='string'){				
			tmpjson=JSON.parse(tmpjson||'{}');
			if(typeof(tmpjson)=='string') tmpjson={tipofile:'',tipo:'',infoavance:100};
		}
		curtema.txtjson=tmpjson;			
		return otrotema;
	}

	var __addexamen=function(obj,fncall){
		//let md=__sysmodal({titulo:'Examenes',html:$('#config_plantilla').html()});
		obj.off('addassesment');
		var lstmp='btnaddexamen'+__idgui();
		obj.addClass(lstmp);
		var url=url_media+'/smartcourse/quiz/buscar/?plt=modal&idproyecto=PY'+idproyecto+'&fcall='+lstmp;
		var md=__sysmodal({titulo:'Quiz',url:url});		    
		obj.on('addassesment',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idexamen=$(this).attr('data-idexamen');
			var link=url_media+'/smartcourse/quiz/ver/?idexamen='+idexamen+'&idproyecto=PY'+idproyecto;			
			if(_isFunction(fncall)) fncall(link);	
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}
	var __addgame=function(obj,fncall){
		obj.off('addgame');
		var lstmp='addgame'+__idgui();
		obj.addClass(lstmp);
		var url=url_media+'/smartcourse/games/buscar/?plt=modal&fcall='+lstmp;		
		var md=__sysmodal({titulo:'Games',url:url});
		obj.on('addgame',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idgame=$(this).attr('data-idgame');
			var link=url_media+'/game/ver/?idgame='+idgame+'&tmp='+lstmp;
			if(_isFunction(fncall)) fncall(link); 		
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}
	var __addtics=function(obj,met,fncall){
		obj.off('addtics');
		var lstmp='addtics'+__idgui();
		obj.addClass(lstmp);
		var url=url_media+'/smartcourse/tics/buscar/?plt=modal&fcall='+lstmp+'&met='+met;
		var md=__sysmodal({titulo:'Games',url:url});
		obj.on('addtics',function(ev){
			$(this).off(lstmp);
			$(this).removeClass(lstmp);
			var idtic=$(this).attr('data-idtic'); // idactividad
			var idsesion=$(this).attr('data-idsesion');
			var link=url_media+'/actividad/ver/?idactividad='+idtic+"&met="+met+"&idsesion="+idsesion+'&tmp='+lstmp;	
			obj.attr('data-link',link);
			if(_isFunction(fncall)) fncall(link); 
			//__cargarplantilla({url:link,donde:'contentpages'});			
		})
	}

	$('#showtemaidedit').on('click',' .cicon',function(ev){
		ev.preventDefault();
		var it=$(this);
		var type=it.attr('data-type')||'';
		var datalink=it.attr('data-link')||'';
		//if(type===''||type==datatype) return false;
		it.attr('data-oldmedia',datalink)
		it.siblings().removeAttr('data-oldmedia').children('i.btn-success').removeClass('btn-success').addClass('btn-primary');	
		var _cont=$('#bookplantilla').children('#contentpages');
		switch (type){
			case 'smarticlock':
				__addtics(it,1,function(link){ 
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-link',link);
					it.attr('data-type','smarticlock');
					it.attr('data-oldmedia',link);				    		
					__asiganacionfile(it);
				});
				break;
			case 'smarticlookPractice':
				__addtics(it,2,function(link){ 
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-link',link);
					it.attr('data-type','smarticlookPractice');
					it.attr('data-oldmedia',link);				    		
					__asiganacionfile(it);
				});
				break;
			case 'smarticDobyyourselft':
				__addtics(it,3,function(link){ 
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-link',link);
					it.attr('data-type','smarticDobyyourselft');
					it.attr('data-oldmedia',link);				    		
					__asiganacionfile(it); 
				});
				break;
			case 'game':
				__addgame(it,function(link){ 
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-link',link);
					it.attr('data-type','game');
					it.attr('data-oldmedia',link);				    		
					__asiganacionfile(it); 
				});
				break;
			case 'smartquiz':
				__addexamen(it,function(link){ 
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-link',link);
					it.attr('data-type','smartquiz');
					it.attr('data-oldmedia',link);				    		
					__asiganacionfile(it);
				});
				break;			    
			case 'pdf':
				__subirfile({ file:it.children('i'), typefile:'pdf',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						it.attr('data-type','pdf');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);				    		
						__asiganacionfile(it);
					});
				break;
			case 'html':
				__subirfile({ file:it.children('i'), typefile:'html',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						it.attr('data-type','html');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);				    		
						__asiganacionfile(it);
					});
				break;
			case 'flash':
				__subirfile({ file:it.children('i'), typefile:'flash',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						it.attr('data-type','flash');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);				    		
						__asiganacionfile(it);
					});
				break;
			case 'audio':
				__subirfile({ file:it.children('i'), typefile:'audio',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						it.attr('data-type','audio');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);				    		
						__asiganacionfile(it);
					});			    	
				break;
			case 'video':
				__subirfile({ file:it.children('i'), typefile:'video',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						console.log(rs);
						it.attr('data-type','video');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);				    		
						__asiganacionfile(it);
					});		    	
				break;
			case 'imagen':
				__subirfile({ file:it.children('i'), typefile:'imagen',guardar:true,dirmedia:'curso_'+idcurso+'/'},
					function(rs){
						it.attr('data-type','imagen');
						it.attr('data-link',rs.media);
						it.attr('data-oldmedia',rs.media);
						__asiganacionfile(it);
					});
				break;				 
			case 'texto':
					it.children('i').addClass('btn-success').removeClass('btn-primary');
					it.attr('data-idinfotmp','txt'+__idgui());				       
					break;
			}
		ev.stopPropagation();
	})


	var updatetabs=false;
	var updateavance=function(newval){
		var vactual=$('.progresscurso').first().attr('aria-value');
		vactual=parseFloat(vactual);
		newval=parseFloat(newval);
		if(vactual<newval){
			$('.progresscurso').first().attr('aria-value',newval);
			$('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
			$('.progresscurso').siblings().css({width:(100 - newval)+'%'});
			jsoncurso.infoavance=newval;
		}		
		if(updatetabs==false){
			updatetabs=true;
			$.each($('ul#ultabs').find('a'),function(i,v){
				var va=parseInt($(v).attr('data-value')||0);
				if(va<=newval) $(v).closest('li').addClass('active');
			})
		}
	}

	if(parseInt(idcurso)>0){
		__sysajax({
			donde:$('#plnindice'),
			url:url_media+'/smartcourse/cursos/jsontemas',
			fromdata:{idcurso:idcurso},
			mostrarcargando:true,
			showmsjok:false,
			callback:function(rs){
				dts=rs.data;				
				if(dts.length>0){
					temas={}					
					$.each(dts,function(i,dt){
						dt.index=i;
						dt.txtjson=JSON.parse(dt.txtjson||'{}');
						if(idcursodetalle==0) idcursodetalle=dt.idcursodetalle;
						if(idcursodetalle==dt.idcursodetalle){
							$('h4.nomtema').html('<b>Tema: </b> '+ dt.nombre);
							curtema=dt;	
							if(curtema.txtjson.tipo!=undefined){
								$('.btntemaaddinfo[data-showp="'+curtema.txtjson.tipo+'"]').removeClass('btn-secondary').addClass('btn-success');
							}
						}
						temas[i]=dt;					
					})
				}else{
					alert('no existen temas agregados.');
				}				
			}
		});

		
		__sysajax({
			fromdata:{idcurso:parseInt(idcurso)},
			showmsjok:false,
			url:url_media+'/smartcourse/acad_curso/buscarjson',					
			callback:function(rs){
				var dt=rs.data;
				if(dt.length>0){
					var info=rs.data[0];                    
					var jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
					$.extend(jsoncurso,jsontmp);
					updateavance(jsoncurso.infoavance);
				} 
			}
		});

		$('.pnlfooter').show();
    }
})
</script>