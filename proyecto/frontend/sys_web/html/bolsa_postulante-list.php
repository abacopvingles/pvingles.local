<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Bolsa_postulante"); ?></li>
  </ol>
</nav>
<?php } ?><div class="form-view" id="vent_<?php echo $idgui; ?>" >
  <div class="row b1 border-primary">
                                                     <div class="col-12 col-sm-6 col-md-4 form-group">
                      <label><?php echo ucfirst(JrTexto::_("fecharegistro"));?></label>                          
                          <div class='input-group date datetimepicker1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fecharegistro"))?> </span>
                            <input type='text' class="form-control " name="fecharegistro" id="fecharegistro<?php echo $idgui;?>" />           
                          </div>                     
                    </div>
                                                                                                                    

    <div class="col-12 col-sm-6 col-md-4 form-group"><br>
      <div class="input-group">
      <input type="text" name="texto" id="texto<?php echo $idgui; ?>" class="form-control" placeholder="<?php echo  ucfirst(JrTexto::_("Text to search"))?>">
      <div class="input-group-append"><button class="btn btnbuscar<?php echo $idgui; ?> btn-primary"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></button></div>
      <div class="input-group-append"><a class="btn btn-warning btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Bolsa_postulante", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Bolsa_postulante").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add')); ?> </a></div>


    </div>
    </div>    
</div>

  <div class="row">
    <div class="col table-responsive b1 border-primary">
        <table class="table table-striped  table-responsive table-hover">
              <thead>
                <tr class="bg-primary">
                  <th scope="col">#</th>
                  <th scope="col"><?php echo JrTexto::_("Fecharegistro") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Nombrecompleto") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Telefono") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Correo") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Descripcion") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Idpublicacion") ;?></th>
                    <th scope="col"><?php echo JrTexto::_("Idpostulante") ;?></th>
                    <th scope="col" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5a6c8f5f0e9b5='';
function refreshdatos5a6c8f5f0e9b5(){
    tabledatos5a6c8f5f0e9b5.ajax.reload();
}
$(document).ready(function(){  
  var estados5a6c8f5f0e9b5={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5a6c8f5f0e9b5='<?php echo ucfirst(JrTexto::_("bolsa_postulante"))." - ".JrTexto::_("edit"); ?>';
  var draw5a6c8f5f0e9b5=0;

  
  $('#fecharegistro<?php echo $idgui;?>').change(function(ev){
    refreshdatos5a6c8f5f0e9b5();
  });  
  $(".btnbuscar<?php echo $idgui; ?>").click(function(ev){
    refreshdatos5a6c8f5f0e9b5();
  }).keyup(function(ev){
    var code = ev.which;
    if(code==13){
      ev.preventDefault();
      refreshdatos5a6c8f5f0e9b5();
    }
  });
  tabledatos5a6c8f5f0e9b5=$('#vent_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Fecharegistro") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombrecompleto") ;?>'},
            {'data': '<?php echo JrTexto::_("Telefono") ;?>'},
            {'data': '<?php echo JrTexto::_("Correo") ;?>'},
            {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            {'data': '<?php echo JrTexto::_("Idpublicacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Idpostulante") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/bolsa_postulante/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.fecharegistro=$('#fecharegistro<?php echo $idgui;?>').val(),
             //d.texto=$('#texto').val(),
                        
            draw5a6c8f5f0e9b5=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5a6c8f5f0e9b5;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Fecharegistro") ;?>': data[i].fecharegistro,
              '<?php echo JrTexto::_("Nombrecompleto") ;?>': data[i].nombrecompleto,
                '<?php echo JrTexto::_("Telefono") ;?>': data[i].telefono,
              '<?php echo JrTexto::_("Correo") ;?>': data[i].correo,
              '<?php echo JrTexto::_("Descripcion") ;?>': data[i].descripcion,
                '<?php echo JrTexto::_("Mostrar") ;?>': data[i].mostrar,
              '<?php echo JrTexto::_("Idpublicacion") ;?>': data[i].idpublicacion,
                '<?php echo JrTexto::_("Idpostulante") ;?>': data[i].idpostulante,
                
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/bolsa_postulante/editar/?id='+data[i].idpostular+'" data-titulo="'+tituloedit5a6c8f5f0e9b5+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idpostular+'" ><i class="fa fa-trash-o"></i></a>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#vent_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'bolsa_postulante', 'setCampo', id,campo,data);
          if(res) tabledatos5a6c8f5f0e9b5.ajax.reload();
        }
      });
  });

  $('#vent_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5a6c8f5f0e9b5';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('vent_')||'Bolsa_postulante';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,true,claseid,{header:true,footer:false,borrarmodal:false}); 
  });
  
  $('#vent_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'bolsa_postulante', 'eliminar', id);
        if(res) tabledatos5a6c8f5f0e9b5.ajax.reload();
      }
    }); 
  });
});
</script>