<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
tr.trclone{
    display:none;
}
.breadcrumb{
    padding:1ex;
}
.breadcrumb>li{
    text-transform: capitalize;
    color:#fff;
    padding-left:1ex;
}
.breadcrumb>li+li:before {
    color: #e4dddd;
    content: "\f101";
    font: normal normal normal 16px/1 FontAwesome;
}
.breadcrumb>li.active {
    color: #383434;
}
.thead{ color:#fff}
</style>
<div class="container-fluid">
    <!--div class="row" id="breadcrumb" style="padding:1ex;">
        <div class="col-12">
            <ol class="breadcrumb bg-primary">
                <li><a style="color:#fff" href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academico"); ?></a></li> 
                <li class="active" style="color:"> <i class="fa fa-building"></i>&nbsp;<?php echo JrTexto::_("locales"); ?></li>       
            </ol>
        </div>
    </div-->
    <div class="row" id="pnllocal">        
        <div class="col-md-12">
            <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idproyecto;?>">
            <div class="card shadow">
                <div class="card-body">	
                <table class="table table-striped table-hover">
                    <thead>
                        <tr class="bg-primary">
                            <th scope="col">#</th>
                            <th scope="col"><?php echo JrTexto::_("Local") ;?></th>
                            <th scope="col"><?php echo JrTexto::_("Direccion") ;?></th>                   
                            <th scope="col" class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Acciones');?><span class="btnverfrmlocal btn btn-warning pull-right btn-sm"><i class="fa fa-plus"></i> Agregar</span></span></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>                           
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pnlfrmlocal" style="display:none">        
        <div class="col-md-12">
            <div id="msj-interno"></div>
            <form method="post" id="frmlocal"  target="" enctype="" class="form-horizontal form-label-left" >
                <input type="hidden" name="idlocal" id="idlocal" value="0">
                <input type="hidden" name="accion" id="accion" value="add">
                <input type="hidden" name="id_ubigeo" id="id_ubigeo" value="1">
                <input type="hidden" name="tipo" id="tipo" value="L">
                <input type="hidden" name="vacantes" id="vacantes" value="1000">
                <input type="hidden" name="idugel" id="idugel" value="1">
                <input type="hidden" name="idproyecto" id="idproyecto" value="<?php echo $this->idproyecto;?>">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 form-group">
                        <label><?php echo JrTexto::_('Local');?> <span class="required"> (*) </span></label>              
                        <input type="text"  id="nombre" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>">
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 form-group">
                        <label><?php echo JrTexto::_('Direccion');?> <span class="required"> (*) </span></label>              
                        <input type="text"  id="direccion" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>">
                    </div>          
                </div>
                <div class="row"> 
                    <div class="col-12 form-group text-center">
                    <hr>
                    <button id="btn-saveLocal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Guardar'));?> </button>
                    <button type="button" class="btn btn-warning btn-cancel"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancelar'));?></button>              
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');
$(document).ready(function(){
    $('table').on('click','.btnverfrmlocal',function(ev){
        $('#pnlfrmlocal').show();
        $('#pnllocal').fadeOut('fast');
    }).on('click','.btneditar',function(ev){
        let idlocal=parseInt($(this).attr('data-id')||0);
        if(idlocal>0)
        __sysajax({   
            fromdata:{idproyecto:idproyecto,idlocal:idlocal},
            url:_sysUrlBase_+'/local/buscarjson', 
            showmsjok:false,          
            callback:function(rs){
                if(rs.code=='ok'){
                    let dt=rs.data[0];
                   console.log(dt);
                    $('#idlocal').val(dt.idlocal);
                    $('#accion').val('edit');
                    $('#id_ubigeo').val(dt.id_ubigueo);
                    $('#tipo').val(dt.tipo);
                    $('#vacantes').val(dt.vacantes);
                    $('#idugel').val(dt.idugel);
                    $('#nombre').val(dt.nombre);
                    $('#direccion').val(dt.direccion);
                    $('#pnlfrmlocal').show();
                    $('#pnllocal').fadeOut('fast');
                }
            }
        })       
    }).on('click','.btn-eliminar',function(ev){
        let idlocal=parseInt($(this).attr('data-id')||0);
        var id=$(this).attr('data-id');
        $.confirm({
        title: '<?php echo JrTexto::_('Confirmar acción');?>',
        content: '<?php echo JrTexto::_('Esta seguro que desea eliminar este registro ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Aceptar');?>',
        cancelButton: '<?php echo JrTexto::_('Cancelar');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){             
            var res = xajax__('', 'local', 'eliminar', idlocal);
            if(res) tabledatos.ajax.reload();
        }
        });      
    });
    $('#frmlocal').on('submit',function(ev){ 
        ev.preventDefault();
        var data=__formdata('frmlocal');
        $('#btn-saveLocal').attr('disabled', true).addClass('disabled');
        __sysajax({   
            fromdata:data,
            url:_sysUrlBase_+'/local/guardarLocal',           
            callback:function(rs){
                $('#btn-saveLocal').removeAttr('disabled', true).removeClass('disabled');
                tabledatos.ajax.reload();
                $('#pnlfrmlocal').hide();
                $('#pnllocal').fadeIn('fast');
            }
        })
    }).on('click','.btn-cancel',function(ev){
        $('#pnlfrmlocal').fadeOut();
        $('#pnllocal').show('fast');
    }); 
     
    var tabledatos=$('table.table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},        
        {'data': '<?php echo JrTexto::_("Local") ;?>'},
        {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
        {'data': '<?php echo JrTexto::_("Acciones") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/local/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.idproyecto=idproyecto,                        
            draw=d.draw;
        },
        "dataSrc":function(json){
            var data=json.data;             
            json.draw = draw;
            json.recordsTotal = json.data.length||0;
            json.recordsFiltered = json.data.length||0;
            var datainfo = new Array();
            for(var i=0;i< data.length; i++){                   
                datainfo.push({
                    '#':(i+1),
                    '<?php echo JrTexto::_("Local") ;?>': data[i].nombre,
                    '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
                    '<?php echo JrTexto::_("Acciones") ;?>'  :'<a class="btn btn-xs btneditar" data-id="'+data[i].idlocal+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idlocal+'" ><i class="fa fa-trash-o"></i></a>'
                });
            }
            return datainfo;                
        }, error: function(d){console.log(d)}
      },
      language:{url:_sysUrlStatic_+'/libs/datatable1.10/idiomas/ES.json'}
      <?php //echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });
    



/*
    let updatetabs=false;
    let updateavance=function(newval){
        let vactual=$('.progresscurso').first().attr('aria-value');
        vactual=parseFloat(vactual);
        newval=parseFloat(newval);
        if(vactual<newval){
            $('.progresscurso').first().attr('aria-value',newval);
            $('.progresscurso').css({width:newval+'%'}).text('Curso '+newval+'% completado');
            $('.progresscurso').siblings().css({width:(100 - newval)+'%'});            
            jsoncurso.infoavance=newval;
            let txtjson=JSON.stringify(jsoncurso);
            __sysajax({   
                fromdata:{id:idcurso,campo:'txtjson',valor:txtjson},
                url:url_media+'/smartcourse/acad_curso/setCampojson',  
                showmsjok:false,
            })            
        }        
        if(updatetabs==false){
            updatetabs=true;
            $.each($('ul#ultabs').find('a'),function(i,v){
                let va=parseInt($(v).attr('data-value')||0);
                if(va<=newval) $(v).closest('li').addClass('active');
            })
        }
    }

    if(parseInt(idcurso)>0){
        __sysajax({
            fromdata:{idcurso:parseInt(idcurso)},
            showmsjok:false,
            url:url_media+'/smartcourse/acad_curso/buscarjson',					
            callback:function(rs){
               let dt=rs.data;
                if(dt.length>0){
                    let info=rs.data[0];                    
                    let jsontmp=JSON.parse(info.txtjson.trim()==''?'{}':info.txtjson);                   
                    $.extend(jsoncurso,jsontmp);
                    updateavance(jsoncurso.infoavance);
                } 
            }
        });
    }
*/
})
</script>