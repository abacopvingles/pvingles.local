<?php 
$idgui=uniqid(); 
$imgcursodefecto='/static/media/nofoto.jpg';
?>
<style>
tr.trclone{
    display:none;
}
.breadcrumb{
    padding:1ex;
}
.breadcrumb>li{
    text-transform: capitalize;
    color:#fff;
    padding-left:1ex;
}
.breadcrumb>li+li:before {
    color: #e4dddd;
    content: "\f101";
    font: normal normal normal 16px/1 FontAwesome;
}
.breadcrumb>li.active {
    color: #383434;
}
</style>
<div class="container-fluid">
    <div class="row" id="breadcrumb" style="padding:1ex;">
        <div class="col-12">
            <ol class="breadcrumb bg-primary">
                <li><a style="color:#fff" href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i>&nbsp;<?php echo JrTexto::_("Academico"); ?></a></li> 
                <li class="active" style="color:"> <i class="fa fa-building"></i>&nbsp;<?php echo JrTexto::_("ambientes"); ?></li>       
            </ol>
        </div>
    </div>
    <div class="row" id="pnl-data">        
        <div class="col-md-12">
            <input type="hidden" name="idcurso" id="idcurso" value="<?php echo $this->idproyecto;?>">
            <div class="card shadow">
                <div class="card-body">	
                    <div class="row">                    
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="javascript:void(0)" data-link="<?php echo $this->documento->getUrlBase();?>/academico/ambientes/?page=local" class="btnverlocales btnlink btn btn-block bg-primary" style="color:#fff">
                                <div><i class="fa fa-building fa-3x"></i></div>												
                                <p class="bolder">Locales</p>
                            </a>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <a href="javascript:void(0)" data-link="<?php echo $this->documento->getUrlBase();?>/academico/ambientes/?page=ambientes" class="btnlink btn btn-block bg-success" style="color:#fff">
                                <div><i class="fa fa-building-o fa-3x "></i></div>
                                <p class="bolder">Ambientes</p>
                            </a>
                        </div>                                  
                    </div>                           
                </div>
            </div>
        </div>        
    </div>
    <div class="row" id="pnl-cargainfo">
    </div>
</div>
<script type="text/javascript">
var url_media='<?php echo URL_MEDIA;?>';
var imgdefecto='<?php echo $imgcursodefecto; ?>';
var idproyecto=parseInt('<?php echo $this->idproyecto; ?>');

$(document).ready(function(){

   
    let tmpcategorias={};    
    $('.btnlink').on('click',function(ev){
        let url=$(this).attr('data-link');
        let donde=$('#pnl-cargainfo');
        __sysajax({
            donde:donde,
            type:'html', 
            fromdata:{plt:'man-listado'},
            url:url,  
            showmsjok:true,
            callback:function(rs){
                donde.html(rs);
            }
        })
    })
    $('.btnverlocales').trigger('click');
})
</script>