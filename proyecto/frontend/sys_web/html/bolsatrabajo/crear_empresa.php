<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
$ismodal=$this->documento->plantilla=="modal"||$this->documento->plantilla=="blanco"?true:false;
$fcall=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:"";
if(!empty($this->datos)) $frm=$this->datos;
$ruta=$this->documento->getUrlStatic()."/media/nofoto.jpg";
if(!empty($frm["logo"])) $ruta=$this->documento->getUrlBase().$frm["logo"];
$file='<img src="'.$ruta.'" class="img-responsive center-block" style="max-width: 200px; max-height: 150px;">';
?>
<div class="container">
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema(); ?>/css/frm.css">
<?php if(!$ismodal){?><nav aria-label="breadcrumb">
  <ol class="breadcrumb bg-primary">
    <li class="breadcrumb-item" ><a href="<?php echo $this->documento->getUrlBase();?>/../"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_("Home"); ?></a></li>
    <!--li><a href="<?php echo $this->documento->getUrlBase();?>/academico"><i class="fa fa-graduation-cap"></i> &nbsp;<?php echo JrTexto::_("Academic"); ?></a></li-->
    <li class="breadcrumb-item active" >&nbsp;<?php echo JrTexto::_("Empresa"); ?></li>
  </ol>
</nav>
<?php } ?>
<div class="row" id="vent-<?php echo $idgui;?>">
	<div class="col-12">
		<div id="msj-interno"></div>
	    <form method="post" id="frmempresa-<?php echo $idgui;?>"  target="" enctype="multipart/form-data" class="form-horizontal form-label-left" >
	    <input type="hidden" name="idempresa" id="idempresa<?php echo $idgui; ?>" value="<?php echo @$frm["idempresa"];?>">
	    <input type="hidden" name="accion" id="accion<?php echo $idgui; ?>" value="<?php echo JrTexto::_($this->frmaccion);?>">
	    <div class="row">
		    <div class="col-12 col-sm-7 col-md-7 form-group">
		    	<div class="row">
		    		<div class="col-12 form-group">
	              		<label><?php echo JrTexto::_('Nombre');?> <span class="required"> (*) </span></label>              
	                	<input type="text"  id="nombre<?php echo $idgui; ?>" name="nombre" required="required" class="form-control" value="<?php echo @$frm["nombre"];?>" placeholder="<?php echo JrTexto::_('Nombre de la empresa') ?>">
	            	</div>
	            	<div class="col-12 form-group">
		            	<label><?php echo JrTexto::_('Razon social');?> <span class="required"> (*) </span></label>              
		                <input type="text"  id="rason_social<?php echo $idgui; ?>" name="rason_social" required="required" class="form-control" value="<?php echo @$frm["rason_social"];?>"  placeholder="<?php echo JrTexto::_('Razon social de la empresa') ?>">
		            </div>
		            <div class="col-12 form-group">
		            	<label><?php echo JrTexto::_('Dirección');?> <span class="required"> (*) </span></label>              
		                <input type="text"  id="direccion<?php echo $idgui; ?>" name="direccion" required="required" class="form-control" value="<?php echo @$frm["direccion"];?>"  placeholder="<?php echo JrTexto::_('Dirección de la empresa') ?>">
		            </div>
		    	</div>
		    </div>
		    <div class="col-12 col-sm-5 col-md-5 form-group">
		    	<div class="row">
		    		<div class="col form-group text-center">
		              	<label><?php echo JrTexto::_('Logo');?> </label>              
		             	<div style="position: relative;" class="frmchangeimage">
		                	<div class="toolbarmouse text-center">                  
		                  		<span class="btn btnaddfile"><input type="file" class="input-file-invisible" name="logo" id="logo<?php echo $idgui; ?>"><i class="fa fa-pencil"></i></span>
		                	</div>
		                <div id="sysfilelogo<?php echo $idgui; ?>"><?php echo @$file; ?></div>                
		              </div>
		            </div>	    		
		    	</div>
		    </div>
            <div class="col-12 form-group">
            	<div class="row">
            		<div class="col-12 col-sm-6 col-md-6 form-group">
              			<label><?php echo JrTexto::_('Ruc');?> <span class="required"> (*) </span></label>              
               			<input type="text"  id="ruc<?php echo $idgui; ?>" name="ruc" required="required" class="form-control" value="<?php echo @$frm["ruc"];?>" placeholder="<?php echo JrTexto::_('RUC Ej:12345678901') ?>">
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 form-group">
              			<label><?php echo JrTexto::_('Telefono');?> <span class="required"> (*) </span></label>              
                		<input type="text"  id="telefono<?php echo $idgui; ?>" name="telefono" required="required" class="form-control" value="<?php echo @$frm["telefono"];?>" placeholder="<?php echo JrTexto::_('Telefono de la empresa Ej: 2014 215 1215') ?>">
                    </div>
            	</div>
            	<hr>
            	<div class="row">
            		<div class="col-12  form-group">
              			<label><?php echo JrTexto::_('Representante');?> <span class="required"> (*) </span></label>              
                		<input type="text"  id="representante<?php echo $idgui; ?>" name="representante" required="required" class="form-control" value="<?php echo @$frm["representante"];?>" placeholder="<?php echo JrTexto::_('Representante de  la empresa Ej. Abel chingo') ?>">
                    </div>
                    <div class="col-12  form-group">
              			<label><?php echo JrTexto::_('Correo');?> <span class="required"> (*) </span></label>              
                		<input type="email" id="correo<?php echo $idgui; ?>" name="correo" required="required" class="form-control" value="<?php echo @$frm["correo"];?>"    placeholder="<?php echo JrTexto::_('Correo de la empresa o representante Ej. soyusuario@miempresa.com') ?>">
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 form-group">
		              	<label><?php echo JrTexto::_('Usuario');?> <span class="required"> (*) </span></label>              
		                <input type="text"  id="usuario<?php echo $idgui; ?>" name="usuario" required="required" class="form-control" value="<?php echo @$frm["usuario"];?>"   placeholder="<?php echo JrTexto::_('Usuario o alias Ej: miempresa17') ?>">
		            </div>
            		<div class="col-12 col-sm-6 col-md-6 form-group">
              			<label><?php echo JrTexto::_('Clave');?> <span class="required"> (*) </span></label>              
               			<input type="password" autocomplete="false" id="clave<?php echo $idgui; ?>" <?php echo empty($frm["idempresa"])?'required="required"':''?> name="clave" class="form-control" placeholder="<?php echo JrTexto::_('clave de usuario Ej: miclave123') ?>"/> 
                    </div>
            	</div>
            </div>
            <div class="col-12 form-group text-center">
            	<hr>
              <button type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo ucfirst(JrTexto::_('Save'));?> </button>
              <button type="button" class="btn btn-warning btn-close" data-dismiss="modal" href="<?php echo JrAplicacion::getJrUrl(array('bolsa_empresas'))?>"> <i class=" fa fa-repeat"></i> <?php echo ucfirst(JrTexto::_('Cancel'));?></button> 
              <br><br>
            </div>
	    </div>
			</form>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmempresa-<?php echo $idgui;?>').bind({    
     		submit: function(ev){
     			ev.preventDefault();
     			var tmpfrm=document.getElementById('frmempresa-<?php echo $idgui;?>');	
				var formData = new FormData(tmpfrm);			
				__sysajax({
					fromdata:formData,
					showmsjok:true,
					url:_sysUrlBase_+'/bolsa_empresas/guardarempresas',					
					callback:function(rs){
						var idempresa=rs.newid;
						$('#idempresa<?php echo $idgui; ?>').val(idempresa);
						setTimeout(function(){ redir(_sysUrlBase_+'/defecto/publicar');},500);
					}
				})
				return false;
     		}
		});

		<?php if($this->documento->plantilla!='modal'){?>
			$('.btn-close').click(function(ev){
				redir(_sysUrlBase_);
			});
		<?php } ?>
		

		$('#logo<?php echo $idgui; ?>').click(function(ev){  
		    $(this).attr('accept','image/*');      
		});

		var inputlogo<?php echo $idgui; ?> = document.getElementById('logo<?php echo $idgui; ?>');  		
		inputlogo<?php echo $idgui; ?>.addEventListener('change', addfilelogo<?php echo $idgui; ?>, false);
		function addfilelogo<?php echo $idgui; ?>(ev) {
		  var file=ev.target.files[0];
		  var donde=$('#sysfilelogo<?php echo $idgui; ?>');
		  var reader=new FileReader();      
		  reader.onload=function(ev_){
		    var filetmp=document.createElement('img');          
		    filetmp.style.width='60%';
		    filetmp.style.height='170px';
		    filetmp.classList.add("img-responsive");
		    filetmp.classList.add("center-block");
		    filetmp.src=ev_.target.result;		          
		    if(filetmp!='') donde.html('').append(filetmp);
		  }
		  reader.readAsDataURL(file);
		}
	});
</script>