<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegProyecto', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegHistorial_sesion', RUTA_BASE, 'sys_negocio');
class WebLogin extends JrWeb
{
    private $oNegBolsa_empresas;
    private $oNegProyecto;
    public $oNegHistorialSesion;
	public function __construct()
	{
		parent::__construct();
        $this->oNegBolsa_empresas = new NegBolsa_empresas;
        $this->oNegProyecto = new NegProyecto;
        $this->oNegHistorialSesion = new NegHistorial_sesion;
	}

	public function defecto(){
		return $this->login();		
	}

	public function login(){
		try{
			global $aplicacion;
			$filtros=array();			
            $idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;
			$idproyecto=(isset($_REQUEST["idproyecto"])&&@$_REQUEST["idproyecto"]!='')?$_REQUEST["idproyecto"]:0;
			
			@session_start();
			if(!empty($idempresa))$_SESSION["idempresa"]=$idempresa;
			else $idempresa=@$_SESSION["idempresa"];
			if(!empty($idproyecto))$_SESSION["idproyecto"]=$idproyecto;
			else  $idproyecto=@$_SESSION["idproyecto"];
            $this->error=array();
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
			
            if(empty($idempresa)) { $this->error[]='Su empresa no esta registrada'; }
            else{	
				if(true === NegSesion::existeSesion()){
					$usu=NegSesion::getUsuario();
					if($idproyecto==$usu["idproyecto"]){
						return $aplicacion->redir('instalar/paso3');
					}else { 
						$this->oNegSesion = new NegSesion;
						$this->oNegSesion->salir();
					}
				}			
				$this->oNegBolsa_empresas->idempresa=$idempresa;
				$this->empresa=$this->oNegBolsa_empresas->getXid();				
                $this->proyectos=$this->oNegProyecto->buscar(array('idempresa'=>$idempresa));
				if(!empty($this->proyectos[0])) $this->proyecto = $this->proyectos[0];
				else $this->error[]='Su proyecto no esta registrado';
                $this->documento->setTitulo( $this->empresa["nombre"],true);
			}			
			$this->documento->plantilla ='login';
			$this->esquema = 'instalador/login';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function salir(){
		try{
			$this->documento->plantilla = 'blanco';
			global $aplicacion;
			JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
			$this->oNegSesion = new NegSesion;
			if(true === NegSesion::existeSesion()){		
				$this->terminarHistorialSesion('P');		
				$this->oNegSesion->salir();
				echo json_encode(array('code'=>200,'urlredir'=>URL_BASE.'/login','msj'=>ucfirst(JrTexto::_('Sesion terminada'))));				
			}else{
				echo json_encode(array('code'=>200,'urlredir'=>URL_BASE.'/login','msj'=>ucfirst(JrTexto::_('Sesion terminada'))));
			}			
			exit(0);
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	protected function terminarHistorialSesion($lugar)
	{
		$usuarioAct = NegSesion::getUsuario();
		$this->oNegHistorialSesion->idhistorialsesion = $usuarioAct['idHistorialSesion'];
		$this->oNegHistorialSesion->fechasalida = date('Y-m-d H:i:s');
		$resp = $this->oNegHistorialSesion->editar();
	}
}