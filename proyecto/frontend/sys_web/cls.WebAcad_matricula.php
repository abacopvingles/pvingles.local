<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		28-08-2018 
 * @copyright	Copyright (C) 28-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_matricula', RUTA_BASE, 'sys_negocio');
class WebAcad_matricula extends JrWeb
{
	private $oNegAcad_matricula;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAcad_matricula = new NegAcad_matricula;
				
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
			
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Acad_matricula'), true);
			$this->esquema = 'acad_matricula-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'edit')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$this->frmaccion='Editar';
			$this->oNegAcad_matricula->idmatricula = @$_GET['id'];
			$this->datos = $this->oNegAcad_matricula->dataAcad_matricula;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Acad_matricula').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'acad_matricula-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Acad_matricula', 'list')) {
			//	echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
			//	exit(0);
			//}
			$filtros=array();
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idgrupoauladetalle"])&&@$_REQUEST["idgrupoauladetalle"]!='')$filtros["idgrupoauladetalle"]=$_REQUEST["idgrupoauladetalle"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["fecha_registro"])&&@$_REQUEST["fecha_registro"]!='')$filtros["fecha_registro"]=$_REQUEST["fecha_registro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["idusuario"])&&@$_REQUEST["idusuario"]!='')$filtros["idusuario"]=$_REQUEST["idusuario"];
			if(isset($_REQUEST["fecha_matricula"])&&@$_REQUEST["fecha_matricula"]!='')$filtros["fecha_matricula"]=$_REQUEST["fecha_matricula"];
						
			$this->datos=$this->oNegAcad_matricula->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function guardarAcad_matricula(){
		$this->documento->plantilla = 'blanco';
		try {
			global $aplicacion;
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            @extract($_POST);
            $accion='_add';            
            if(!empty($idmatricula)) {
				$this->oNegAcad_matricula->idmatricula = $idmatricula;
				$accion='_edit';
			}
           	$usuarioAct = NegSesion::getUsuario();
           	
	           					$this->oNegAcad_matricula->idgrupoauladetalle=@$idgrupoauladetalle;
					$this->oNegAcad_matricula->idalumno=@$idalumno;
					$this->oNegAcad_matricula->fecha_registro=@$fecha_registro;
					$this->oNegAcad_matricula->estado=@$estado;
					$this->oNegAcad_matricula->idusuario=@$idusuario;
					$this->oNegAcad_matricula->fecha_matricula=@$fecha_matricula;
					
            if($accion=='_add') {
            	$res=$this->oNegAcad_matricula->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Acad_matricula')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegAcad_matricula->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Acad_matricula')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAcad_matricula->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}