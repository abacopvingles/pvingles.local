<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebCursos extends JrWeb
{
	
	public function __construct()
	{
		parent::__construct();
	}

	public function defecto(){
        try{
			global $aplicacion;
			$this->curusuario = NegSesion::getUsuario();

			$filtros=array();			
            $this->idempresa=$this->curusuario["idempresa"];
            $this->idproyecto=$this->curusuario["idproyecto"];
            $this->rol=$this->curusuario["idrol"];
            $this->documento->setTitulo(JrTexto::_('cursos'),true);
			$this->documento->plantilla ='proyecto/cursos';
            $this->esquema = 'smartcourse/curso'.(abs($this->rol));
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}			
	}

	//pasos para crear cursos

	public function crear(){
        try{
			global $aplicacion;
			$filtros=array();
			$this->curusuario = NegSesion::getUsuario();
			$this->idproyecto=$this->curusuario["idproyecto"];
			$this->idcurso=(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')?$_REQUEST["idcurso"]:0;
			$pg=(isset($_REQUEST["pg"])&&@$_REQUEST["pg"]!='')?$_REQUEST["pg"]:'paso1';
			
            $this->rol=$this->curusuario["idrol"];
            $this->documento->setTitulo(JrTexto::_('cursos'),true);
            $this->documento->plantilla ='proyecto/cursos';
            $this->esquema = 'smartcourse/curso_crear'.$pg;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}			
	}

}