<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBolsa_empresas', RUTA_BASE, 'sys_negocio');
class WebDefecto extends JrWeb
{
	private $oNegBolsa_empresas;
	public function __construct()
	{
		parent::__construct();
		$this->oNegBolsa_empresas = new NegBolsa_empresas;
	}

	public function defecto(){
		return $this->filtro();		
	}

	public function filtro(){
		try{
			global $aplicacion;
			$filtros=array();
			$idempresa=(isset($_REQUEST["idempresa"])&&@$_REQUEST["idempresa"]!='')?$_REQUEST["idempresa"]:0;			
			$this->documento->setTitulo(JrTexto::_('Proyectos'),true);
			$this->documento->plantilla ='proyecto/instalador';
	
			$this->esquema = 'instalador/bienvenida';			
			if(!empty($idempresa)){
				$this->oNegBolsa_empresas->idempresa=$idempresa;
				$this->datos=$this->oNegBolsa_empresas->getXid();
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}

}