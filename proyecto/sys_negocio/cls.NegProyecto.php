<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		07-08-2018
 * @copyright	Copyright (C) 07-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegProyecto 
{
	protected $idproyecto;
	protected $idempresa;
	protected $tipologin;
	protected $jsonlogin;
	protected $fecha;
	
	protected $dataProyecto;
	protected $oDatProyecto;	

	public function __construct()
	{
		$this->oDatProyecto = new DatProyecto;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatProyecto->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatProyecto->get($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatProyecto->iniciarTransaccion('neg_i_Proyecto');
			$this->idproyecto = $this->oDatProyecto->insertar($this->idempresa,$this->jsonlogin,$this->fecha);
			$this->oDatProyecto->terminarTransaccion('neg_i_Proyecto');	
			return $this->idproyecto;
		} catch(Exception $e) {	
		    $this->oDatProyecto->cancelarTransaccion('neg_i_Proyecto');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatProyecto->actualizar($this->idproyecto,$this->idempresa,$this->jsonlogin,$this->fecha);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Proyecto', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatProyecto->eliminar($this->idproyecto);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdproyecto($pk){
		try {
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}
			$this->idproyecto = $this->dataProyecto["idproyecto"];
			$this->idempresa = $this->dataProyecto["idempresa"];		
			$this->jsonlogin = $this->dataProyecto["jsonlogin"];
			$this->fecha = $this->dataProyecto["fecha"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('proyecto', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataProyecto = $this->oDatProyecto->get($pk);
			if(empty($this->dataProyecto)) {
				throw new Exception(JrTexto::_("Proyecto").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}