<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		16-08-2018
 * @copyright	Copyright (C) 16-08-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatProyecto_cursoscategoria', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegProyecto_cursoscategoria 
{
	protected $idproycursocat;
	protected $idproyecto;
	protected $idcurso;
	protected $categoria;
	
	protected $dataProyecto_cursoscategoria;
	protected $oDatProyecto_cursoscategoria;	

	public function __construct()
	{
		$this->oDatProyecto_cursoscategoria = new DatProyecto_cursoscategoria;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatProyecto_cursoscategoria->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatProyecto_cursoscategoria->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatProyecto_cursoscategoria->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatProyecto_cursoscategoria->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatProyecto_cursoscategoria->get($this->idproycursocat);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursoscategoria', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatProyecto_cursoscategoria->iniciarTransaccion('neg_i_Proyecto_cursoscategoria');
			$this->idproycursocat = $this->oDatProyecto_cursoscategoria->insertar($this->idproyecto,$this->idcurso,$this->categoria);
			$this->oDatProyecto_cursoscategoria->terminarTransaccion('neg_i_Proyecto_cursoscategoria');	
			return $this->idproycursocat;
		} catch(Exception $e) {	
		    $this->oDatProyecto_cursoscategoria->cancelarTransaccion('neg_i_Proyecto_cursoscategoria');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursoscategoria', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatProyecto_cursoscategoria->actualizar($this->idproycursocat,$this->idproyecto,$this->idcurso,$this->categoria);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Proyecto_cursoscategoria', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatProyecto_cursoscategoria->eliminar($this->idproycursocat);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdproycursocat($pk){
		try {
			$this->dataProyecto_cursoscategoria = $this->oDatProyecto_cursoscategoria->get($pk);
			if(empty($this->dataProyecto_cursoscategoria)) {
				throw new Exception(JrTexto::_("Proyecto_cursoscategoria").' '.JrTexto::_("not registered"));
			}
			$this->idproycursocat = $this->dataProyecto_cursoscategoria["idproycursocat"];
			$this->idproyecto = $this->dataProyecto_cursoscategoria["idproyecto"];
			$this->idcurso = $this->dataProyecto_cursoscategoria["idcurso"];
			$this->categoria = $this->dataProyecto_cursoscategoria["categoria"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('proyecto_cursoscategoria', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataProyecto_cursoscategoria = $this->oDatProyecto_cursoscategoria->get($pk);
			if(empty($this->dataProyecto_cursoscategoria)) {
				throw new Exception(JrTexto::_("Proyecto_cursoscategoria").' '.JrTexto::_("not registered"));
			}

			return $this->oDatProyecto_cursoscategoria->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}