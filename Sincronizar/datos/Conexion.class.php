<?php

require_once 'conf.php';

class Conexion {

    protected $dbLink;

    /* La funcion es con dos guiones */

    public function __construct() {
        $this->abrirConexion();
    }

    public function __destruct() {
        $this->dbLink = NULL;
    }

    protected function abrirConexion() {
        /* Concatenar e sel punto */
        $servidor = BD_TIPO . ":host=" . BD_SERVIDOR . ";port=" . BD_PUERTO . ";dbname=" . BD_NOMBRE_BD;
        $usuario = BD_USUARIO;
        $clave = BD_CLAVE;

        try {
            /* php data object */
            $this->dbLink = new PDO($servidor, $usuario, $clave);
            $this->dbLink->exec("SET NAMES 'utf8';");
            $this->dbLink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        return $this->dbLink;
    }

}
