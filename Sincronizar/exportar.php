<?php
	define('_http_',!empty($_SERVER['HTTPS'])?'https://':'http://');
	define('SD', DIRECTORY_SEPARATOR);
	define('RUTA_BASE',	dirname(dirname(__FILE__)) . SD);
	$ruta_ini=dirname(dirname(__FILE__));	
	define('RUTA_RAIZ',$ruta_ini.SD);
	$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
	define('URL_BASE',_http_.$host.'/pvingles.local');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Exportacion SmartEnglish</title>
	<link href="../static/tema/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="logica/css/form.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">  
	  <form id="contact" action="" enctype="multipart/form-data">
	    <h3 align="center">Exportacion SmartEnglish</h3>
	    <fieldset>
	      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">DESCARGAR	</button>
	      <!-- <a href="data:application/xml;charset=utf-8,your code here" download="filename.html">Save</a> -->
	    </fieldset>
	    <div class="progress progress-striped" id ="barra">
		  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%" id='progreso'>
		    <span class="sr-only"></span>
		  </div>
		</div>
	  </form>
	</div>

	<script src="../static/media/woorkbooks/resources/js/jquery.js"></script>
	<script src="../static/tema/js/bootstrap.min.js"></script>
	<!-- <script src="../../static/libs/multi-download-master/browser.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function() {  
			// $('body').css("background","#4c5caf");  
		    function changeColor(){
		    	
		    	console.log($('body').css("background"));
		    	if ($('body').css("background") == 'rgb(76, 92, 175) none repeat scroll 0% 0% / auto padding-box border-box') {
		    		$('body').css("background","#563e65");
		    	}else{
		    		if ($('body').css("background")=='rgb(86, 62, 101) none repeat scroll 0% 0% / auto padding-box border-box') {
		    			$('body').css("background","#4c5caf");
		    		}
		    		
		    	}
		    }

		    setInterval(changeColor, 5000);
		    $('#contact').on('submit',function(e){
		    	e.preventDefault();
		    	$.ajax({
					type: 'POST',
					url: 'descarga.php',
					success: function(resp){
						var valor = 0;
						var cont = 0;
						for (var i = 0; i < 100; i++) {
							valor = i+1;
							setTimeout(function(){ 
								$('#progreso').css('width',valor+"%");
								if (valor === 100) {
									
									
									if (cont ==0) {
										console.log("sadsa");
										var datos = JSON.parse(resp);
										var dato = datos;
										var a=document.createElement('a');
										a.setAttribute('download',dato);
				  						a.style.display = 'none';
				  						a.setAttribute('href', 'prueba/'+dato);
				  						document.body.appendChild(a);
										a.click();
										document.body.removeChild(a);
										cont++;
									}

									
									i=100;
									return 0;
								}
							},1000);


						}
					}
				});
		    });
		});
		
	</script>
</body>
</html>