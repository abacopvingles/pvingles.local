<?php 
$mysqli = new mysqli("127.0.0.1","root","","cursos_postgres");
$http = !empty($_SERVER['HTTPS'])?'https://':'http://';
$host= !empty($_SERVER["HTTP_HOST"])?$_SERVER["HTTP_HOST"]:'abacoeducacion.org';
$URL_BASE = $http.$host.'/pvingles.local';
$server_dir = dirname(dirname(__FILE__));
$dir_cursos = $server_dir.'/static/media/cursos';

$dir_tmpl = "./tpl_gerenciales";
$dir_Gerenciales = $server_dir.'/static/media/cursos/Habilidades Gerenciales';

if(!is_dir($dir_Gerenciales)) {
	exit($dir_Gerenciales." no es un directorio valido");
}
if ($mysqli->connect_errno){
	exit("No se pudo conectar :(");
}

/* Registrando BD :  Acad_Curso */
$idCurso = getNewId('idcurso', 'acad_curso', $mysqli);
$addCurso=$mysqli->query("INSERT INTO acad_curso (idcurso, nombre, imagen, descripcion, estado, fecharegistro, idusuario, costo) values('".$idCurso."','Habilidades Gerenciales','','','1','".date('Y-m-d')."','1','0')");
$nuevo_dir_curso = $dir_cursos.'/curso_'.$idCurso;
mkdir($nuevo_dir_curso, 0777);

/* * * * 		main()		* * * */
$arrTemas = scan_directorio($dir_Gerenciales);
limpiarArrDir($arrTemas);
foreach ($arrTemas as $t) { echo "<div style='margin-left:30px'>";
	mensaje(null, "Carpeta : ".utf8_decode($t) );

	$arrCursos = buscar_db("SELECT * FROM curso WHERE nombre LIKE '%".utf8_decode($t)."%' ", $mysqli);
	
	if(!empty($arrCursos )) {
		$curso = $arrCursos[0];

		/* Registrando BD :  Niveles */
		$idNivel = getNewId('idnivel', 'niveles', $mysqli);
		$addNivel=$mysqli->query("INSERT INTO niveles VALUES ('".$idNivel."', '".utf8_decode($t)."','S','0','1','1','".$curso['objetivos']."','','')");

		/* Registrando BD :  Acad_CursoDetalle */
		$idCursoDet = getNewId('idcursodetalle', 'acad_cursodetalle', $mysqli);
		$addNivel=$mysqli->query("INSERT INTO acad_cursodetalle (`idcursodetalle`, `idcurso`, `orden`, `idrecurso`, `tiporecurso`, `idlogro`, `url`, `idpadre`) VALUES ('".$idCursoDet."', '".$idCurso."', '".$curso['objetivos']."', '".$idNivel."','S','0','".$curso['idcurso']."-".$curso['objetivos']."', 0)");

		/* Crear la carpeta del nuevo Acad_CursoDetalle */
		$nuevo_dir_detalle = $nuevo_dir_curso.'/detalle_'.$idCursoDet;
		mkdir($nuevo_dir_detalle, 0777);
		
		$carpeta_actual = $dir_Gerenciales.'/'.$t;
		$carpeta_actual_parsed = iconv("UTF-8", "windows-1254", $carpeta_actual);
		recurse_copy($carpeta_actual_parsed , $nuevo_dir_detalle);

		$nuevo_dir_detalle_contenido = $nuevo_dir_detalle.'/ses1';
		$arrPestanias = scan_directorio($nuevo_dir_detalle_contenido);
		limpiarArrDir($arrPestanias);
		foreach ($arrPestanias as $p) { echo "<div style='margin-left:30px'>";
			$source = $dir_tmpl.'/libs';
			$destination = $nuevo_dir_detalle_contenido.'/'.$p;
			$destination = iconv("UTF-8", "windows-1254", $destination);
			recurse_copy($source, $destination);

			create_index($destination, $dir_tmpl);
			#break;
		echo "</div>"; }
	} else {
		mensaje('gray', "La consulta : <b>SELECT * FROM curso WHERE nombre LIKE '%".$t."%'</b> No encontro resultados en BD ");
	}

	#break;
echo "</div>"; }


function mensaje($color='#38cfd6', $textShow='', $array=array()) {
	if(empty($color)) { $color = '#38cfd6'; }
	echo "<pre class='msj-debug'> <span style='background:".$color."; color: white;'>";
	echo $textShow;
	echo "</span></pre>";
	if(!empty($array)){ var_dump($array); }
}

function scan_directorio($dir='') {
	mensaje( 'green', 'LEYENDO : '.$dir );
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arr = scandir( $dir );
		foreach ($arr as $d) {
			$arrDirectorio[] = iconv("windows-1254", "UTF-8", $d);
		}
		var_dump($arrDirectorio);
		return $arrDirectorio;
	} else {
		mensaje( 'red', '"'.$dir.'" : No es un directorio válido.' );
		return null;
	}
}

function scanear_directorio($dir='') { /* para create_index()::index.php */
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arrDirectorio = scandir( $dir );
		return $arrDirectorio;
	} else {
		return null;
	}
}

function limpiarArrDir(&$arr) {
	$i = array_search('.', $arr);
	unset($arr[$i]);
	$j = array_search('..', $arr);
	unset($arr[$j]);
	$arr = array_values($arr);
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src);
    if(!is_dir($dst)) {
    	mensaje( 'brown', 'CREANDO DIR : '. $dst );
    	mkdir($dst, 0777);
    }
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
            	mensaje( 'orange', 'COPIANDO : '. $src . '/' . $file ."     ====>>>     ".$dst . '/' . $file );
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

function create_index($dst, $dir_tmpl) {
	try {
		$archivo = $dst.'/' . 'index' . '.html';
		mensaje( 'brown', 'CREANDO FILE : '.$archivo );
		if(file_exists($archivo)) @unlink($archivo);
		$fp = fopen($archivo, "a");
		$tpl_esquema = $dir_tmpl.'/index.php';

		ob_start();
		require($tpl_esquema);
		$datos = ob_get_contents();
		ob_end_clean();

		$write = fputs($fp, $datos);
		fclose($fp);
		@chmod($archivo,0777);
		//return true;
	} catch (Exception $e) {
		mensaje( 'red', 'Error Creando index.hmtl : ' .$e->getMessage() );
		//return null;
	}
}

function getNewId($pk, $table, $mysqli) {
	$cursonew=$mysqli->query("SELECT MAX($pk) AS newid FROM $table");
	$curnew=$cursonew->fetch_assoc();
	$new_id=intval($curnew["newid"])+1;
	return $new_id;
}

function buscar_db($strQuery, $mysqli) {
	$arrData = array();
	$mysqli->set_charset("utf8");
	$cursos=$mysqli->query($strQuery);
	while ($c=$cursos->fetch_assoc()) {
		$arrData[] = $c ;
	}
	return $arrData;
}