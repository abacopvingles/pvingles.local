INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0000', 'Sistemas de Computación Hardware', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Reconocer los componentes de hardware de un
computador', 'Definiciones previas
Clasificación de las computadoras
El case
La placa base
Microprocesador
Memoria principal
Tarjeta de video
Unidades de almacenamiento
Periféricos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0001', 'Descripción de Windows', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Descripción de Windows', 'Conocer e identificar los elementos que conforman el entorno de trabajo de Windows Vista.

Así como poner en funcionamiento los procesos básicos de trabajo.', 'El escritorio de Windows
El menú de inicio
Uso del mouse
Ejecución de programas
Manejo de ventanas');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0002', 'Introducción a Microsoft Word', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Introducción a Microsoft Word', 'Conocer el entorno de trabajo de Microsoft Word
Realizar las operaciones básicas con archivos', 'Definición de procesador de texto
Novedades de Microsoft Word
Ejecución de Microsoft Word
Descripción del entorno de trabajo
Formas de presentación de un documento
Gestión de documentos: Como iniciar un nuevo documento
Configuración de página
Edición de un documento
Grabar un documento
Abrir un documento
Impresión
Salir de Word
Uso de la ayuda');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0003', 'Introducción a Power Point 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Conocer el entorno de trabajo
Utilizar los modos de visualización y creación de presentaciones', 'Introducción al Power Point 2007
Descripción del entorno de trabajo
Elementos de Power Point
Modos de creación de presentaciones
Modos de visualización de presentaciones
Operaciones con archivos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0004', 'Introducción a Microsoft Excel 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Efectos de animación
Crear una presentación en pantalla', 'Introducción Microsoft Excel
Ingreso de datos
Tipos de datos
Los operadores
Edición de fórmulas
Administración de archivos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0005', 'Introducción a Internet Explorer', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Navegar por el WW
Buscar información en el Web
Utilizar un correo electrónico público.', 'Definiciones generales
Concepto de red local (LAN) y protocolo
Principales servicios de Internet: Servidores de información, Correos
electrónicos, listas de interés, comunicación en tiempo real, programas
de acceso remoto, Navegadores.
Ms Internet Explorer: Entorno de trabajo, Navegación
Principales páginas Web
Bloquear cookies, borrar archivos innecesarios, organizar sus favoritos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0006', 'Introducción a Macromedia Flash 8', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'En esta sesión aprenderá a configurar el espacio de trabajo de Flash 8, reconocer los elementos de la pantalla principal y herramientas que usará durante todo el desarrollo del curso.', 'Descripcion de Entorno.
Trabajando con Paletas.
Gráficos Vectoriales y mapa de Bits.
Las Peliculas de Flash.
Cambiando Zoom.
Cuadrículas, guías y reglas.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0007', 'Trabajando con Múltiples hojas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 10, 50.00, NULL, 'Desarrollar aplicaciones que utilicen múltiples hojas de cálculo', 'Creación de grupos de hojas de cálculo.
Manejo de nombres de rango.
Uso de ventanas.
Inmovilizar rangos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0008', 'Repaso de Word Inicial', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Repasar temas de Word Inicial.
Usar la herramienta de autocorrección.
Utilizar hipervínculos y marcadores.', 'Pegado Especial.
Emcabezado y piez de pagina.
Formato de imágenes insertadas.
Hipervículos y marcas.
Tablas');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0009', 'Introducción a Microsoft Proyect 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Indentificar los elementos del entorno de trabajo.
Establecer configuraciones básicas para la creación de proyectos.
Iniciar un sesión de trabajo.', 'Acceso a Microsoft Proyect.
Visualización del Proyect.
Configuración de escalas');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0010', 'Introducción a Freemind 0.9.0', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'LOGRAR UN ACCESO A FREEMIND Y UN CONOCIMIENTO BASICO SOBRE LA INSTACION.', 'INTRODUCCION A  FREEMIND 0.9.0.
INSTALACION.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0011', 'Introducción a Cmap Tools', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LOS CONCEPTOS BÁSICOS QUE PERMITAN DEFINIR UN MAPA CONCEPTUAL.
LOGRAR EL ACCESO A LA APLICACIÓN DE CMAP TOOLS Y ESTABLCER UN RECONOCIMIENTO BASICO DE SU ENTORNO.', 'MAPAS CONCEPTUALES.
ELEMENTOS DEL ENTORNO.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0012', 'Introdución a Publisher 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'LOGRAR UN ACCESO A PUBLISHER 2007 Y ESTABLECER UN RECONOCIMEINTO BASICO DE LOS ELEMENTOS DEL ENTORNO', 'INTRODUCCION A  MICROSOFT PUBLISHER 2007
ELEMENTOS DEL ENTORNO
UTILIZANDO EL ASISTENTE');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0013', 'Introduccion a Microsoft Visio 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'LOGRAR UN ACCESO A VISIO 2007 Y ESTABLECER UN RECONOCIMIENTO Y MANEJO  BÁSICO DE LOS ELEMENTOS DE SU ENTORNO', 'INTRODUCCION A  MICROSOFT VISIO 2007
ELEMENTOS DEL ENTORNO
RECONOCIMIENTO DEL ENTORNO
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0014', 'Introduccion a InfoPath 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'COMPRENDER LA NATURALEZA DE LA APLICACIÓN DE INFOPATH 2007 Y ESTABLECER UN RECONOCIMIENTO DE SU ENTORNO IDENTIFICANDO SUS COMPONENTES BÁSICOS.', 'PRESENTACIÓN DE INFOPATH
ACCESO A INFOPATH
ELEMENTOS DEL ENTORNO');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0015', 'Introduccion a microsoft onenote 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'LOGRAR EL ACCESO A ONENOTE 2007 Y ESTABLECER UN RECONOCIMIENTO BÁSICO DE SU ENTORNO, ASI COMO COMPRENDER LA ESTRUCTURA FUNCIONAL DE LA APLICACIÓN.', 'ACCESO A MICROSOFT OneNote 2007
RECONOCIMIENTO DEL ENTORNO
ESTRUCTURA DE UN DOCUMENTO EN OneNote 2007
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0016', 'Introduccion a Jclic 0.2.0.5', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER UN RECONOCIMIENTO DE LOS ELEMENTOS BÁSICOS DEL ENTORNO DEL JCLIC.', 'INTRODUCCION.
CONCEPTOS BÁSICOS.
DESCRIPCION DEL ENTORNO JCLIC AUTHOR .');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0017', 'INTRODUCCION A OPEN OFFICE CALC 3.2', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0018', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 10, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0019', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0020', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0021', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0022', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0023', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0024', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0025', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0026', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0027', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0028', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0029', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0030', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0031', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0032', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0033', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0034', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0035', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0036', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0037', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0038', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0039', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0040', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0041', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0042', 'Descripción de Windows', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0043', 'Introducción a Word 2010', '', NULL, NULL, '', NULL, 'A', 5, 0, 30, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0044', 'Introducción a Excel 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0045', 'Introducción a Power Point 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0046', 'Introducción a Internet 2.0', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0047', 'Ardora y Actividades Educativas I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0048', 'Formularios en Word 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0049', 'Consulta Avanzada', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0050', 'Libros Interactivos Multimedia Edilim', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '• Conocer el entorno de trabajo de Edilim.
• Administrar archivos en Edilim.', '• Concepto y ventajas.
• Descarga e instalación.
• Entorno de Edilim.
• Crear un libro con Edilim.
• Administrar libros en Edilim.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0051', 'Introducción a Quizfaber', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0052', 'Introducción a Scratch', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0053', 'Introducción a WebQuest', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0054', 'Introducción Educaplay', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0055', 'Introducción a ThatQuiz', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0056', 'Retos de La educación para el siglo XXI', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0057', 'La Modernización ', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0058', 'La Gestión Educativa ', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0059', 'Identidad y Diagnóstico Institucional', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0060', 'Propuesta Pedagógica de la Institución', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0061', 'Propuesta de Gestión', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0062', 'Proyecto de Mejora Educativa', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0063', 'Calidad y Mejora Continua en las Instituciones', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0064', 'Evaluación y Acreditación', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0065', 'Pestaña insertar, estilos y ecuaciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0066', 'Sesión 1', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0067', 'Sesión 1', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0068', 'Definición e Interfaz de Access 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0069', 'Vinculación e Incrustación de Objetos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0070', 'Introducción a Prezi', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0071', 'Introducción y Aspectos Generales', NULL, NULL, NULL, '', NULL, 'A', 2, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0072', 'Introduccion a Robomind', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0073', 'Introduccion a Geogebra', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0074', 'Como Exceder las expectativas del Cliente', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0075', 'Como ser un Lider que Transforme', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0076', 'Como destruir su propio Stress', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0077', 'La ventaja de saber Escuchar', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0078', 'Proyectando una Auto imagen mas Positiva', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0079', 'Como reclutar, entrenar y recompensar a los Empleados', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0080', 'Creando lideres siendo un buen mentor', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0081', 'Utilizando el pensamiento estrategico para mejorar', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0082', 'Siete pasos para la delegacion Efectiva', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0083', 'Como Automotivarse y motivar a otros aplicando la comunicaci', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0084', 'Como Tratar con clientes Dificiles', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0085', 'Como conectarse: Comunicacion al mas alto nivel', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0086', 'Como apoyar a los empleados a dar lo mejor de si mismos', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0087', 'La hora de 70 Minutos', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0088', 'El Arte y la Ciencia de la Venta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0094', 'Crear y Visualizar Documentos', '58', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0095', 'Manejar el Ambiente', '117', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0096', 'Crear y Administrar Hoja de Cálculo y Libros', '83', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0097', 'Gestión de Base de Datos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0098', 'Definición de los Requisitos de los Proyectos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0099', 'Definición de los Requisitos de los Proyectos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0100', 'Definición de los Requisitos de los Proyectos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0101', 'Definición de los Requisitos del Proyecto', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0102', 'Introducción a Windows 8.1', '204', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0103', 'Sistemas de Computación Hardware', '200', NULL, NULL, '', NULL, 'A', 4, 0, 20, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0104', 'Definición, Instalación y Entorno de Trabajo', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, 'Conocer las características del programa y el entorno de trabajo de XMind', '- Definición de XMind
- Características
- Instalación
- Entorno de Trabajo');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0105', 'Instalación y Entorno de Trabajo', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, '- Conocer las características y el entorno de trabajo del programa', '- Definición del programa
- Características
- Instalación
- Entorno de Trabajo');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0106', 'Partes de una computadora', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 0, 1.00, NULL, '- Introducción a Mantenimiento
- Conocer las partes principales de un computador', '- Mantenimiento Preventivo
- Partes de una computadora');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0107', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0108', 'Encuesta', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0109', 'Uso y manejo de botones de acción', '127', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- APRENDER A UTILIZAR BOTONES DE ACCIÓN PARA EJECUTAR DIFERENTES TIPOS DE TAREAS
- APRENDER A CONFIGURAR CORRECTAMENTE LA TAREA DE CADA BOTÓN DE ACCIÓN', 'USO Y MANEJO DE BOTONES DE ACCIÓN');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0110', 'Consultas Avanzadas', '89', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a usar las funcioens BuscarV, BuscarH, Si y esError', '- Combinación de funciones: BUSCARV, BUSCARH, SI, ESERROR
- Búsqueda múltiple');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0111', 'Creación de una tabla dinámica', '91', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0113', 'Conceptos Básicos', '161', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0114', 'Crear hojas de cálculo y libros de trabajo', '237', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0115', 'Insertar Datos en Celdas y Rangos', '250', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0116', 'Resumir datos mediante el uso de funciones', '258', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (1, '0117', 'Crear un documento', '', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0000', 'Sistemas de Computación Software', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Conocer la utilidad de los diferentes software que existen en el mercado', 'Definición
Tipos de software');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0001', 'Manejo de Archivos', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Manejo de Archivos', 'Establecer los métodos necesario para organizar y manejar los elementos de una unidad de almacenamiento de datos.', 'Descripción del explorador de Windows.
Administrar carpetas y archivos.
Operaciones básicas con archivos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0002', 'Escritura y Edición de Texto', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Escritura y Edición de Texto', 'Dar a conocer las técnicas de escritura y edición de texto

Realizar las operaciones básicas con texto: Copiar, mover, eliminar', 'Edición y escritura de texto
Teclas de funciones generales
Ingreso de texto
Formas de desplazarse por el documento
Selección de textos o gráficos
Operaciones con texto');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0003', 'Trabajando con diapositivas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Manejar las diapositivas
Aplicar diseño y fondo a la diapositiva', 'Trabajando con diapositivas
Operaciones con diapositivas
Diseño de la diapositiva
Insertar de comentarios');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0004', 'Operaciones básicas con las celdas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Manipular las celdas para realizar operaciones básicas como: copiar, mover, insertar, etc.', 'Generación de series, series automáticas
Operaciones con las celdas: Copiar datos, escribir datos en un rango de
celdas, copiar un bloque de datos, mover un bloque de celdas, insertar un
bloque de celdas, copia celdas para obtener resultados, especiales, insertar
/ eliminar filas o columnas, buscar una expresión en una hoja de cálculo,
cambiar el ancho de las columnas, ajustar la altura de las filas, oculta/
muestra las columnas / filas
El Botón Deshacer');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0005', 'Uso de buscadores', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Navegar por el WW
Buscar información en el Web
Utilizar un correo electrónico público.', 'Uso de buscadores
Técnica para el ingreso de datos a buscar
Buscadores de información: Google');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0006', 'Dibujando Pintando', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'En esta sesión aprenderá a utilizar las herramientas de dibujo de flash, usar y modificar sus atributos, deformar los objetos creados y demas', 'Lápiz.
Linea, elipses, rectángulos y polígonos.
Pincel.
Bezier.
Borrador.
Usando la paleta Mezclador.
Bordes y rellenos de objetos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0007', 'Trabajando con funciones avanzadas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Utilizar la función condicional si.
Utilizar las funciones de búsqueda buscarV, buscarH', 'Funciones Lógicas.
Funciones de búsqueda y referencia: buscarV, buscarH.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0008', 'Personalización del entorno de trabajo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Configurar Microsoft Word a nuestras necesidades.', 'Personalizar opciones más frecuentes.
Personalizar: Mostrar, revisión, Guardar, Avanzadas.
Personalizar barra de herramientas de acceso rápido');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0009', 'Manejo de Tareas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Establecer la Programación de un proyecto.
Conocer y aplicar los criterios fundamentales para la asignación de tareas.
Definir la creación de un esquema.', 'Programación de un  proyecto.
Dependencia de tareas.
Creación de un esquema.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0010', 'Elementos Básicos de Freemind', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER UN RECONOCIMIENTO BÁSICO DE LOS ELEMENTOS DEL ENTORNO.
REALIZAR  LA CREACION DE MAPAS Y NODOS.', 'ELEMENTOS DEL ENTORNO.
CREACION DE MAPAS Y NODOS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0011', 'Manejo de Mapas Conceptuales', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LOS LOS ELEMENTOS QUE INTEGRAN UN CMAP.
INICIAR  EL DESARROLLO DE UN CMAP ASI COMO SU POSTERIOR ALMACENAMIENTO EN UNA CARPTA DE TRABAJO.', 'CREAR UN MAPA CONCEPTUAL.
ALMACENAMIENTO Y APERTURA DE UN MAPA.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0012', 'Configurar Página', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ASIGNAR DIVERSAS PROPIEDADES Y FORMATOS A UNA HOJA DE TRABAJO EN PUBLISHER 2007', 'CONFIGURAR PAGINA
ESTILOS PERSONALIZADOS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0013', 'Creación de un Diagrama de Flujo Basico', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'INICIAR EL DESARROLLO DE UN DIAGRAMA DE FLUJO Y LOGRAR EL RECONOCIMIENTO Y UTILIZACIÓN DE SUS ELEMENTOS BÁSICOS.', 'DIAGRAMAS DE FLUJO
FORMAS,GALERÍAS DE SIMBOLOS Y PLANTILLAS
EDICION Y FORMATO DE TEXTO
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0014', 'Diseño de Formularios', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'INICIAR LA CREACIÓN DE UN FORMULARIO UTILIZANDO LA HERRAMIENTA DE DISEÑO
CONOCER Y UTILIZAR LOS CONTROLES MÁS COMUNES PARA EL DISEÑO.
', 'PANEL DE TAREAS DE DISEÑO
CREACIÓN DE UN FORMULARIO
GUARDAR UN FORMULARIO');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0015', 'Creacion y manejo de un bloc de notas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CREAR UN BLOC DE NOTAS MEDIANTE EL ASISTENTE DE ONENOTE.
REALIZAR LA ADMINISTRACIÓN EFICAZ DE LOS ELEMENTOS QUE INTEGRAN UN BLOC DE NOTAS.', 'CREACION DE UN BLOC DE NOTAS
CREACIÓN Y MANEJO DE SECCIONES
GUARDAR Y MANEJO DE PÁGINAS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0016', 'Creacion de proyectos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LAS HERRAMIENTAS QUE PERMITAN LA CREACION DE PROYECTOS EN JCLIC AUTHOR.', 'CREAR UN PROYECTO.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0017', 'Manejo de Filas y Columnas', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0018', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0019', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0020', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0021', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0022', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0023', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0024', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0025', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0026', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0027', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0028', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0029', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0030', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0031', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0032', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0033', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0034', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0035', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0036', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0037', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0038', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0039', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0040', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0041', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 35, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0042', 'Accesos directos y Accesorios de Windows', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0043', 'Visualización y Aplicaciones de Word', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0044', 'Aplicaciones de Cuadros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0045', 'Barra de herramientas y Dibujo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0046', 'Principios de Internet 2.0', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0047', 'Actividades Educativas II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0048', 'Documentos Maestros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0049', 'Acceso a datos externos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0050', 'Creación de Páginas Edilim', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0051', 'Insertar Preguntas y Objetos Multimedia', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0052', 'Primeros pasos con Scratch', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0053', 'Construyendo un MiniQuest', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0054', 'Actividades Educaplay I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0055', 'Exámenes I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0065', 'Datos, gráficos y configuraciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0068', 'Generar base de datos en Access 2010', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0069', 'Cartas Personalizadas en Word con datos Externos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0070', 'Insertar objetos I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0071', 'Creación y Gestión de Mensajes de Correo', NULL, NULL, NULL, '', NULL, 'A', 2, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0072', 'Comandos Basicos', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0073', 'Herramientas Geogebra I', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0094', 'Insertar y Editar Textos', '59', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0095', 'Crear una  Presentación con Diapositivas', '120', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0096', 'Crear Celdas y Rangos de Celdas', '84', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0097', 'Crear Tablas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0098', 'Identificación de los Elementos de Diseño al Preparar Diseño de Páginas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0099', 'Comprensión de la Interfaz', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0100', 'Identificar los Elementos del Diseño', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0101', 'Identificación de los Elementos de Diseño', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0102', 'Conociendo a Windows', '205', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0103', 'Sistemas de Computación Software', '201', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0104', 'Funciones básicas y uso de las propiedades', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, 'Describir las funcionalidades básicas del programa XMind', '- Elaboración del Mapa Mental
- Resaltar la idea principal
- Centrar la idea
- Componentes del mapa
- Crear límites
- Grabación del mapa mental');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0105', 'Estructura de trabajo de Exelearning', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, 'Conocer las herramientas disponibles que ofrece el programa', '- Empezar a crear contenido
- Los iDevices
- Tipos de iDevices');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0106', 'Armado de una computadora y la BIOS', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 0, 1.00, NULL, '- Orientar a la práctica del armado y desarmado de un computador
- Conocer las características y el funcionamiento de la BIOS', '- Desarmado del CASE
- Armado del CASE
- BIOS - Setup
- Estructura Básica de la BIOS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0107', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0108', 'Indicadores', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0109', 'Macros', '128', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a utilizar macros
- Programar una macro', '- Manejo de macros');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0110', 'Acceso a Datos Externos', '90', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a acceder a distintos datos de Access, de una página web, archivo de texto y otras conexiones existentes', '- Obtener datos desde: Microsoft Access, Página Web, Archivo de Texto, Conexiones Existentes');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0111', 'Filtrar tablas dinámicas', '96', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0113', 'Funciones Financieras', '163', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0114', 'Navegar en Hojas de Cálculo y Libros de Trabajo', '238', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0115', 'Dar formato a celdas y rangos', '251', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0116', 'Realizar operaciones condicionales mediante el uso de funciones', '259', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (2, '0117', 'Navegar a través de un documento', '', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0001', 'Recurso de Carpetas', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Recurso de Carpetas', 'Establecer criterios lógicos para la utilización de carpetas y archivos.

Asi como su localización y acceso atreves de distintos niveles.', 'Compartir carpetas
Buscar archivos en la red
Y buscar equipos en la red');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0002', 'Corrección de Documentos', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Corrección de Documentos', 'Conocer la correcta utilización de las herramientas de búsqueda y reemplazo de texto, ortografía, sinónimos y la división de palabras.', 'Búsqueda y reemplazo de texto
Ortografía
Sinónimos
División de palabras');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0003', 'Manejo de objetos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Manejar imágenes
Manejar formas', 'Manejo de imágenes
Insertar imágenes
Edición de imágenes
Insertar formas
Operaciones con objetos
Microsoft WordArt
SmartArt');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0004', 'Formato de las celdas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprender a diseñar y aplicar formato a las celdas de un libro', 'Formatos
Estilos
Bordes y sombreado');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0005', 'Microsoft Outlook 2007', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Manejar el correo electrónico Microsoft Outlook 2007', 'Conceptos básicos
Descripción de la pantalla
Los contactos
Listas de distribución
Seguimiento de contactos
El correo electrónico
Crear un nuevo mensaje de correo electrónico
Envió y recepción de los mensajes
Envió de correo desde los contactos
Respuesta a los mensajes
Búsqueda de los mensajes');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0006', 'Trabajo con Objetos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'En esta Sesión aprenderá a seleccionar objetos, agruparlos y ordenarlos asi como transformarlos de manera adecuada.', 'Seleccionar Objetos.
Los Grupos.
Apilar y ordenar objetos.
Mover y copiar.
Transformación de Objetos.
Alinear y destribuir objetos.
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0007', 'Trabajando con fórmulas animadas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Utilizar fórmulas animadas, para generar fórmulas más elaboradas.', 'Fórmulas animadas.
Función Sumar.Si.
Función Contar.Si.
Fórmulas matriciales.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0008', 'Ortografía, grámatica y sinónimos ', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Utilizar los diccionarios personalizados.', 'Ortografía y grámatica.
Corrector multilenguaje.
Sinónimos.
Insercción de guiones.
Diccionario personalizado: crear, activar, agregar palabras, quitar.
Traducción de idiomas.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0009', 'Utilización del calendario.', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Establecer la programación de un proyecto.
Conocer y aplicar los criterios fundamentales para la asignación de tareas.
Definir la creación de esquema.', 'Programación de un proyecto.
Dependecias de tareas.
Creación de un esquema.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0010', 'Formatos y Estilos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APLICAR DIVERSOS FORMATOS A NODOS.
REALIZAR  EL MANEJO DE OBJETOS.', 'FORMATOS DE TEXTO.
FORMATOS DE NODOS.
INSERCION DE SIMBOLOS E IMÁGENES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0011', 'Manejo de recursos y Enlaces', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LOS Y UTILIZAR RECURSOS EN UN MAPA.
REFINAR UN CMAP MEDIANTE LA IMPLEMENTACION DE RECURSOS.
ESTABLECER DIVERSOS DISEÑOS DE ENLACES A RECURSOS.', 'MODOS DE ASIGNAR RECURSOS.
IMPORTAR RECURSOS.
ADICIONAR Y EDITAR ENLACES A RECURSOS.
MANIPULAR ENLACES.
MODIFICAR  LINEAS DE ENLACE.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0012', 'Formato de Texto, Colores y Autoformas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LAS HERRAMIENTAS DE TEXTO, COLORES Y AUTOFORMAS
REFINAR UNA PRESENTACION  MEDIANTE FORMATOS PERSONALIZADOS', 'CUADROS DE TEXTO
EFECTOS DE RELLENO
AUTOFORMAS Y DIBUJOS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0013', 'Creacion de Organigramas ', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LAS HERRAMIENTAS NECESARIAS PARA CREAR, DISEÑAR Y ESTABLECER ENLACES EN LA CREACIÓN DE ORGANIGRAMAS.', 'ORGANIGRAMAS
AGREGAR  IMAGENES E HIPERVINCULOS
ESTABLECER FORMATOS
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0014', 'Vinculación de Datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'MEJORAR Y REFINAR EL DISEÑO DEL FORMULARIO, ESTABLECIENDO LOS FORMATOS DISPONIBLES DE VALIDACIÓN.', 'VALIDACIÓN DE DATOS.
VALIDACIÓN: PARA QUE UN CONTROL SEA REQUERIDO.
VALIDACIÓN: PARA QUE UN VALOR SEA ESPECÍFICO.
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0015', 'Insertar notas, imágenes y objetos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'REALIZAR LA EDICIÓN DE TEXTO Y APLICAR DIVERSOS FORMATOS QUE PERMITAN LA ESCRITURA DE DATOS.
INSERTAR OBJETOS QUE GRAFIQUEN LAS NOTAS INGRESADAS EN EL BLOG ASI COMO ESTABLECER UN ACCESO A ELEMENTOS EXTERNOS.', 'Escribiendo notas en una pagina
Insertando imágenes
Insertando objetos
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0016', 'Creacion de actividades', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LOS ELELMENTOS NECESARIOS QUE PERMTAN LA CREACION DE UNA ACTIVIDAD SIMPPLE.', 'CREACION DE UNA ACTIVIDAD SIMPLE.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0017', 'Manejo de Celdas', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0018', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0019', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0020', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0021', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0022', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0023', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0024', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0025', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 3, 0, 5, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0026', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0027', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0028', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 20, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0029', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0030', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0031', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0032', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0033', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0034', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0035', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0036', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0037', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0038', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0039', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0040', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0041', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0042', 'Configuraciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0043', 'Aplicaciones de Estetica en el Documento', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0044', 'Tratamiento de Datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0045', 'Trabajando con Diapositivas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0046', 'Configuraciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0047', 'Actividades Educativas III', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0048', 'Seguridad', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0049', 'Formularios y controles', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0050', 'Creación de Páginas Edilim II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0051', 'Administrar Quiz', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0052', 'Agregando Interactividad', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0053', 'Construyendo una WebQuest', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0054', 'Actividades Educaplay II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0055', 'Exámenes II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0065', 'Presentaciones y macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0068', 'Usar base de datos - Formatos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0069', 'Creación de estadísticas y Gráficos en Excel', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0070', 'Insertar objetos II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0071', 'Gestión de Contactos, calendarios, tareas y notas', NULL, NULL, NULL, '', NULL, 'A', 2, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0072', 'Estructura de Programacion I', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0073', 'Herramientas Geogebra II', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0094', 'Dar Formato al Texto', '60', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0095', 'Trabajando con Gráficos y Elementos Multimedia', '121', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0096', 'Crear  Tablas', '85', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0097', 'Crear Formularios', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0098', 'Comprensión de la Interfaz', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0099', 'Identificación de los Elementos de Diseño para Preparar Imagenes', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0100', 'Comprensión de la Interfaz', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0101', 'Compresión de la Interfaz', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0102', 'Acceso directo y Explorador de Windows', '206', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0104', 'Insertar imágenes, relaciones y links', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, 'Describir la funcionalidad del programa al insertar recursos', '- Insertar imágens
- Relaciones
- Añadir etiquetas
- Notas del elemento
- Hipervínculo');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0105', 'Editor de iDevices y Gestor de Estilos', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, '- Conocer las  posiblidades que nos ofrece la creación de nuevos iDevices
- Conocer las preferencias adicionales que maneja el programa', '- Creación de un iDevice
- Gestor de Estilos
- Preferencias
- Informes sobre Recursos
- Visualización Previa');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0106', 'Sistemas Operativos e Instalación', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 0, 1.00, NULL, '- Tener conocimiento sobre los diferentes sistemas operativos existentes
- Aplicar la instalación de las principales versiones de Windows', '- Sistemas Operativos
- Windows XP
- Windows 7');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0107', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0108', 'Monitoreo', NULL, NULL, NULL, '', NULL, 'A', 1, 0, 30, 30.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0109', 'Colaborar en una presentación', '129', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a guardar archivos y presentaciones en Skydrive
- Invitar amigos para compartir presentaciones', '- Colaborar en una presentación
- Guardar presentaciones en Skydrive');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0110', 'Formularios y Cotroles', '91', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a trabajar con formularios y controles', '- Ingreso de datos a una hoja desde un formulario
- Trabajando con USERFORMS, LISTBOX, conjuntamente con macros
- Trabajando con COMBOBOX conjuntamente con la función BUSCARV');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0111', 'Diseño de tablas dinámicas', '97', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0113', 'Evaluación de Proyectos', '164', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0114', 'Dar formato a hojas de cálculo y libros de trabajo', '239', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0115', 'Resumir y  organizar datos', '252', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0116', 'Dar formato y modificar texto mediante el uso de Funciones', '260', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (3, '0117', 'Dar formato a un documento', '', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0001', 'Panel de Control', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Mantenimiento del Sistema', 'Conocer los conceptos básicos de configuración de los elementos principales del equipo.
Conocer y asignar diversos modos de configuración.', 'Panel de Control.
Configuración del sistema.
Configuración del teclado.
Configuración de pantalla.
Configuración del mouse.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0002', 'Formato de Texto y Parráfo', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Formato de Texto y Parráfo', 'Conocer cómo aplicar los formatos de texto y párrafo', 'Formato de texto
Eliminar copiar formato de texto
Formato de párrafo
Espacio entre párrafos
Interlineado
Sangría
Letra capital
Bordes y sombreado
Numeración y viñetas
Tabulaciones
Líneas y saltos de página');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0003', 'Efectos de animación multimedia I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Agregar elementos multimedia a su presentación', 'Aplicar formato a los objetos
Insertar video y sonido');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0004', 'Manejo de fórmulas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprender a utilizar las fórmulas básicas disponibles en Microsoft Excel en forma correcta.', 'Fórmulas
Los operadores
Referencias relativas y absolutas
Funciones
Tipos de funciones');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0006', 'Trabajando con Textos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'En esta Sesión aprenderá usar la herramienta texto.administrar sus diferentes tipos, a separar texto en formasy crear vinculos a correos electronicos, páginas webs y demas', 'La Herramienta Texto.
Separar Texto.
Vincular Texto.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0007', 'Base de Datos: Ordenación y filtros.', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Ordenar registros de una lista de datos.
Filtrar registros que cumplen una determinada condición.', 'Introducción a Base de Datos.
Validación de la información.
Utilizar formularios para incluir datos.
Buscar registros en una lista, utilizando un formulario de datos.
Eliminar registros de una lista, usando un formulario de datos.
Ordenación de Base de Datos.
Filtros de Datos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0008', 'Administrar Documento', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Usar comentario en el Documento.
Utilizar la herramienta de auto corrección, autotexto y autoformato.
Crear y editar estilos y plantillas.', 'Utilizar comentarios en documentos de word.
Comparar y combinar documentos.
Autocorección: configurar, crear/eliminar entradas.
Insertar una entrada de autotexto.
Usar autoformato: revisar los cambios en el autoformato, autoformato mientras escribe.
Estilos.
Planitllas.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0009', 'Gestión de Programación de recursos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Asignar los elementos complementarios al proyecto base.
Normalizar el desarrollo de tareas asignadas.', 'Asignación de recursos.
Disponibilidad de recursos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0010', 'Personalizando Mapas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APLICAR DIVERSOS FORMATOS QUE PERMITAN MEJORAR LA APARIENCIA Y UTILIDAD DE UN MAPA.', 'NUBES.
HIPERVINCULOS.
ICONOS Y ENLACES GRAFICOS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0011', 'Asignación de colores y formato de fuentes', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'UTILIZAR LA PALETA DE COLORES PARA ASIGNAR NUEVOS COLORES A LOS ELEMENTOS DE UN CMAP.
APLICAR DIVERSAS PROPIEDADES DE TEXTO UTILIZANDO LA VENTANA ESTILOS.', 'CAMBIAR COLORES.
USO DE LA PALETA DE COLORES.
FONDOS Y ESTILOS.
CAMBIAR FUENTE Y TAMAÑO.
USO DE VENTANA ESTILOS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0012', 'Manejo de Imágenes y Objetivos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'INSERTAR DIVERSOS TIPOS DE IMÁGENES  A UNA PUBLICACION
ESTABLECER DISTINTAS PROPIEDADES A OBJETOS DE DISEÑO', 'TIPOS DE IMÁGENES
OBJETOS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0013', 'Diagramas Dinamicos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Realizar la creación de diagramas en visio 2007,  estableciendo enlaces externos  con Microsoft office Excel 2007', 'CREACION DE DIAGRAMAS DINAMICOS
ELEMENTOS DE SU ENTORNO
AGREGANDO VALORES Y FORMAS
ACTUALIZACION DE DATOS
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0014', 'Controles y Datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER ENLACES Y VINCULOS DE DATOS  EXTERNOS A CONTROLES DE UN FORMULARIO, ASÍ COMO LOGRAR  UNA  PRUEBA DEL FORMULARIO MEDIANTE VISTA PREVIA.', 'ENLACE DE DATOS
VISTA PREVIA DE FORMULARIOS
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0015', 'Grabación de Audio y Video', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'UTILIZAR LAS HERRAMIENTAS NECESARIAS PARA ASIGNAR ARCHIVOS DE AUDIO Y VIDEO EN UNA PÁGINA DE ONENOTE 2007.', 'FORMATOS Y CARACTERISTICAS DE AUDIO Y VÍDEO.
GRABACION DE AUDIO.
GRABACION DE VIDEO.
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0016', 'Creacion de una actividad sopa de letras', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LOS ELEMENTOS Y HERRAMIENTAS QUE PERMITAN LA CREACION DE UNA ACTIVIDAD DE TIPO SOPA DE LETRAS.', 'CREACION DE UNA ACTIVIDAD SOPA DE LETRAS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0017', 'Funciones Básicas', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0042', 'Mi PC y Explorador de Windows', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0043', 'Aplicaciones para mejorar la Edición', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0044', 'Funciones I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0045', 'Animaciones, Transiciones y Multimedia', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0046', 'Uso de buscadores', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0047', 'Actividades Educativas IV', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0048', 'Macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0049', 'Funciones personalizadas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0050', 'Creación de Páginas Edilim III', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0051', 'Propiedades y Publicación en HTML', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0052', 'Programando Scratch', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0053', 'Cazatesoros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0054', 'Colecciones y grupos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0055', 'Asignar exámenes y notas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0065', 'Opciones de publicación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0068', 'Crear y modificar tablas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0069', 'Control de Facturas desde Excel', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0070', 'Temas y recursos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0071', 'Seguridad y privacidad', NULL, NULL, NULL, '', NULL, 'A', 2, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0072', 'Estructura de Programacion II', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0073', 'Construcciones geometricas y la barra de navegacio', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0094', 'Insertar y Modificar Gráficos', '61', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0095', 'Crear gráficos y Tablas', '122', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0096', 'Aplicar  Fórmulas  y  Funciones', '87', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0097', 'Crear y Manejar Preguntas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0098', 'Creación de Diseño de Página', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0099', 'Manipulación de Imágenes', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0100', 'Edición de una Secuencia de Video', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0101', 'Creación de Diseños de Página', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0102', 'Configuraciones', '207', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0104', 'Tipos, Estilos y Propiedades de Diagramas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, '- Describir los tipos de diagramas
- Describir las propiedades y los estilos de los diagramas', '- Tipos de Diagramas
- Propiedades de texto y forma
- Estilo de mapa, límites, relaciones y hojas
- Propiedades de conectores y línea');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0105', 'Estilos y Propiedades de Exelearning', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, '- Conocer los estilos y propiedades que ofrece el programa', '- Estilos de Exelearning
- Propiedades de Exelearning');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0106', 'Actualización y Herramientas de Diagnóstico', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 0, 1.00, NULL, '- Tener conocimiento de los ajustes y controles que se le puede brindar al sistema operativo.
- Conocer las herramientas de diagnóstico de Windows.', '- Panel de Control
- Controladores
- Herramientas de Diagnóstico');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0109', 'Preparar una presentación para su entrega', '130', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a preparar una presentación correctamente para su entrega
- Aprender a empaquetar una presentación para CD-ROM', '- Preparar una presentación para su entrega
- Empaquetar para cd-rom
- Exportar a otros formatos
- Consejos a la hora de la proyección');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0110', 'Funciones Personalizadas', '92', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a crear funciones personalizadas', '- Por qué crear funciones personalizadas
- Creación de una función personalizada
- Usar una función personalizada
- Complementos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0111', 'Formato de valores', '98', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0113', 'Análisis de Estados Financieros', '165', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0114', 'Personalizar opciones y vistas', '240', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0115', 'Crear y Administrar Tablas', '253', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0116', 'Crear Gráficos', '261', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (4, '0117', 'Personalizar opciones y vistas para documentos', '', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0001', 'Mantenimiento del Sistema', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Conocer y aplicar los métodos necesarios, para realizar una buena conservación del equipo.', 'El centro de seguridad
Mantenimiento del disco duro
Información del Sistema');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0002', 'Bordes y Sombreados', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Bordes y Sombreados', 'Dar a conocer las técnicas para establecer formato a párrafos
Aplicar bordes y sombreado, numeración y viñetas
complementando con el manejo de tabulaciones.', 'Bordes y Sombreado
Numeración y viñetas
Tabulaciones
Lineas y Saltos de página');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0003', 'Efectos de animación multimedia II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aplicar formato a los objetos
Insertar video y sonido', 'Efectos de animación
Crear una presentación en pantalla');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0004', 'Impresión', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Enseñar a administrar la impresora desde Microsoft Excel.', 'Impresión
Vista preliminar
Definiendo el área de impresión
Configuración de la página
imprimir rótulos de fila y columna en cada pagina');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0006', 'Introducción a la Animación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Una Animación es una sucesión de dibujos y figuras en el tiempo, de tal manera que se simula el movimiento de estos objetos. en esta lección aprenderemos los principios de la animacion de Flash.', 'Línea de Tiempo.
Símbolos.
La biblioteca.
Efectos de Instancias de Símbolos.
Los filtros.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0007', 'Base de Datos Subtotales', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Agrupar registros  de Datos y Esquemas', 'Subtotales');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0008', 'Macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Automatizar tareas utilizando macros.', 'Introducción a macros.
Creación de una macro.
Ejecución de una macro.
Eliminación de una macro.
Modificación de una macro.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0009', 'Rutas y tareas críticas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'RECONOCER UNA TAREA CRITICA.
CAMBIAR LA DEFINICION DE UNA TAREA CRITICA.
APLICAR INFORMES DE VISUALIZACION DE TAREAS CRITICAS.', 'TAREA CRITICA.
MODIFICACIONES.
INFORMES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0010', 'Multiples Operaciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y DESARROLLAR MULTIPLES OPERACIONES DE FREEMIND, QUE PERMITEN REDUCIR AL MINIMO SU TRABAJO.', 'SELECCIÓN Y MANEJO DE NODOS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0011', 'Flechas, Nodos Anidados y Asociaciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APLICAR ELEMETOS DE DISEÑO DE MAPAS PARA ESTABLECER RELACIONES.', 'AÑADIR FLECHAS.
NODOS Y ASOCIACIONES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0012', 'Diseño de Tablas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APRENDER A UTILIZAR LAS HERRAMEINTAS QUE PERMITAN DISEÑAR DIVERSOS TIPOS DE TABLAS EN UNA PUBLICACION.', 'DISEÑO DE UNA TABLA
FORMATOS DE TABLAS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0013', 'Creación de Diagra mas de Red', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APRENDER A UTILIZAR LOS ELEMENTOS NECESARIOS PARA LA CREACIÓN DE DIAGRAMAS DE RED.
ESTABLECER VÍNCULOS DE DATOS EXTERNOS HACIA LAS FORMAS DE UN DIAGRAMA DE RED.
', 'CONCEPTUALIZACIÓN DE UN DIAGRAMA DE RED
CREACIÓN DE UN DIAGRAMA DE RED
VINCULAR DATOS AL DIAGRAMA
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0014', 'Elementos Extensibles y Asignación de Formulas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LOS MÉTODOS Y PROPIEDADES  PARA EL DISEÑO DE ELEMENTOS  EXTENSIBLES.
APLICAR FUNCIONES A CAMPOs EXTENSIBLES, PARA LA REALIZACIÓN DE PROCESOS EN UN FORMULARIO', 'SECCIONES Y CONTROLES EXTENSIBLES
ASIGNACIÓN DE FUNCIONES');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0015', 'Administración e impresión de notas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER CONFIGURACIONES DE CONSERVACIÓN PARA EL BUEN MANTENIMIENTO DE NOTAS
CONOCER Y UTILIZAR LOS DIVERSOS MEDIOS DE DISTRIBUCIÓN.', 'ENVIO DE NOTAS
SEGURIDAD Y CONTROL DE ACCESO DE NOTAS
BUSQUEDA Y LOCALIZACION DE NOTAS
IMPRESIÓN DE NOTAS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0016', 'Canción de una actividad compleja', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APLICAR LOS METODOS Y TECNICAS PARA LA CREACION DE UNA ACTIVIDAD COMPLEJA.', 'ACTIVIDAD COMPLEJA.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0017', 'Funciones Intermedias', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0042', 'Instalación rápida de Microsoft Office 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0043', 'Uso del Menu Insertar', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0044', 'Funciones II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0045', 'Hipervínculos, Impresión y Publicación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0046', 'Participación en la Web', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0047', 'Multimedia y Publicación I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0048', 'Creación de Índices', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0049', 'Tablas de Datos de una y dos Entradas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0050', 'Creación de Páginas Edilim IV', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0052', 'Importación y Publicación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0054', 'Herramientas Educaplay', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0068', 'Definir relaciones', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0069', 'Gráficos Vinculados de Excel en Word', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0070', 'Administrar presentaciones, carpetas y comentarios', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0072', 'Mapas', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0073', 'Entrada algebraica funciones y comandos', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0094', 'Buscar y Reemplazar Contenido', '70', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0095', 'Transiciones y Animaciones', '123', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0096', 'Crear Cuadros y Objetos', '87', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0097', 'Diseñar Reportes', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0098', 'Publicación, Exportación y Archivado de Diseño de Página', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0099', 'Publicacion de Imágenes Digitales', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0100', 'Exportación de Video', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0101', 'Publicación, Exportación y Archivado de Diseños de Página', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0104', 'Resumen, temas y exportación', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, '- Describir los estilos de resumen del mapa
- Aprender a insertar temas y exportar el mapa', '- Crear un resumen
- Agregar un tema al mapa
- Exportando nuestro mapa
- Imprimir mapa');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0105', 'Guardar, exportar y fusionar', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 0, 1.00, NULL, 'Conocer sobre las distintas alternativas de exportación y la combinación de los recursos didácticos', '- Guardar y exportar
- Formatos de exportación
- Fusionar');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0106', 'Virus Informáticos e Introducción a Redes', NULL, NULL, NULL, '', NULL, 'A', 10, 0, 0, 1.00, NULL, '- Conocer las aplicaciones que puedan optimizar nuestro sistema operativo.
- Conocer las distintas amenazas que se esparcen en nuestro ordenador.
- Compartir archivos y periféricos a través de una red local.', '- Programas Utilitarios
- Virus informáticos
- Introducción a Redes de Computadoras ');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0109', 'Opciones de publicación e impresión', '131', NULL, NULL, '', NULL, 'A', 5, 0, 0, 1.00, NULL, '- Aprender a publicar una presentación
- Aprender a publicar en línea
- Aprender a publicar en redes sociales
- Aprender a imprimir correctamente nuestra presentación', '- Opciones de publicación
- Opciones de impresión');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0111', 'Campos calculados', '99', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0113', 'Escenarios de Negocios', '166', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0114', 'Configurar Hojas de Cálculo y Libros de Trabajo', '241', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0115', 'Administrar estilos y opciones de tabla', '254', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0116', 'Dar Formato a los Gráficos', '262', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (5, '0117', 'Imprimir y guardar documentos', '', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0001', 'Reproductor de Windows Multimedia', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Conocer y utilizar de windows media para la ejecución de archivos multimedias, asi mismo genera una lista de producción mediante la selección de archivos.', 'Reproductor de Windows Media.
Uso de las caracteristicas.
Crear favoritos.
Reproducción de video.
Fondos.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0002', 'Columnas y Tablas', '', '', '', '', NULL, 'A', 5, 0, 15, 10.00, 'Sabes usted Columnas y Tablas', 'Diseñar columnas de texto
Diseñar tablas de texto y aplicar formato de presentación
Inserción de imágenes', 'Columnas
Tablas
Creación de una tabla
Seleccionar celdas, filas y columnas
Inserción de celdas, filas y columnas
Eliminación de celdas, filas y columnas
Mostrar la cuadrícula y las marcas de fin de celda y fila
Convertir líneas de texto con formato de tabulación en
tabla
Convertir tablas en líneas de texto
Aplicar formato a las tablas
Trabajando con imágenes');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0004', 'Gráficos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Crear, modificar y personalizar gráficos con Microsoft Excel.', 'Gráficos
Asistente para gráficos
Opciones e gráficos
Ubicación del gráfico
Haciendo modificaciones a un grafico definido');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0006', 'Animaciones I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprederemos a realizar animaciones del tipo fotograma a fotograma (animaciones paso a paso) y animaciones por interpolación de movimiento (animaciones lineales).', 'Aminación Fotograma.
Aminación de Interpolación de movimiento.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0007', 'Tablas Dinamicas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'Generar Matrices de datos consolidados, con la herramienta tabla dinámica.', 'Tabla dinámica en Excel.
Aplicar filtros a la tabla.
Cambiar diseño de la tabla.
Insertar campo calculado.
Actualizar datos en la tablas dinámicas.
Graficar una tabla dinámica.
Eliminar una tabla dinámica.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0009', 'Planeamiento de costos de los recursos y tareas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'DEFINIR Y APLICAR COSTOS.
MODIFCAR ASIGNACIONES DE RECURSOS.
ANALIZAR Y PLANIFICAR LOS COSTOS FIJOS DE TAREAS.', 'COSTOS DE RECURSOS.
MODIFICACION DE ASIGNACIONES.
COSTOS FIJOS DE TAREAS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0010', 'Exportar e Importar Mapas Mentales', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'APRENDER A UTILIZAR LAS OPERACIONES AVANZADAS QUE PERMITAN LA IMPORTACION Y EXPORTACION DE LOS MAPAS.', 'EXPORTACION E IMPORTACION DE DATOS.
IMPRESIONES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0011', 'Ubicando un Cmap en un sitio Web', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'LOGRAR UBICAR UN CMAP A NIVEL WEB Y ESTABLECER PERMISOS DE ACCESO.', 'COPIAR UN CMAP A UN SITIO.
PERMISOS.
EXPORTAR UN CMAP.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0012', 'Alamacenamiento y distribución de Archivos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LAS DISTINTAS FORMA DE GUARDAR UN ARCHIVO EN PUBLISHER 2007', 'FORMATO TIFF Y MDI
Y finalmente FORMATO DOCUMENTO DE WORD');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0013', 'Diagramas de Flujo de Funciones Cruzadas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'UTILIZAR LAS HERRAMIENTAS DE GRAFICO DE FLUJO DE FUNCIONES CRUZADAS, PARA REPRESENTAR PROCESOS Y ACTIVIDADES DE LAS AREAS DE UNA EMPRESA', 'FUNCIONES CRUZADAS
CREACION DE UN DIAGRAMA DE FLUJO DE FUNCIONES CRUZADAS
ADICIONAR GALERIAS DE FORMAS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0014', 'Publicación de Formularios', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'GENERAR Y DISTRIBUIR FORMULARIOS DE DATOS MEDIANTE UN ENTORNO DE RED', 'GUARDAR PLANTILLAS Y PUBLICAR UN FORMULARIO
GUARDAR DATOS EN FORMULARIOS PUBLICOS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0016', 'Creacion de una actividad completando texto', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'DESARROLLAR UNA ACTIVIDAD QUE PERMITA AL USUARIO COMPLETAR FRASES.', 'ACTIVIDAD COMPLETANDO TEXTO.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0017', 'Funciones Financieras I', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0042', 'Arrancar una Aplicación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0043', 'Tablas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0044', 'Funciones III', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0047', 'Multimedia y Publicación II', NULL, NULL, NULL, NULL, NULL, 'A', 5, 0, 30, 20.00, NULL, NULL, NULL);
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0048', 'Tablas de Contenido', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0049', 'Creación y Consolidación de tablas Dinámicas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0091', 'Botones de Acción', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0092', 'Crear y modificar consultas', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0093', 'Vinculación con presentaciones en PowerPoint', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0094', 'Crear y Modificar Listas', '62', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0095', 'Colaborar en una Presentación', '124', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0111', 'Origen de datos', '100', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0113', 'Valoración de Inversiones en situación de Riesgo', '167', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0115', 'Crear y Administrar Tablas II', '255', NULL, NULL, '', NULL, 'A', 1, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (6, '0116', 'Insertar y dar formato a objetos', '263', NULL, NULL, '', NULL, 'A', 1, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0002', 'Diseño de Encabezado y pie de página', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Crear secciones
Diseñar encabezados y pie de página
Crear estilos', 'Secciones
Encabezados y pies de páginas
Estilos');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0004', 'Base de datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Administrar una base de datos con Microsoft Excel
Crear tablas dinámicas
Crear tablas de datos consolidadas', 'Base de datos
Ordenación
Validar datos
Filtros automáticos
Subtotales
Tablas dinámicas
Vincular hojas Externas');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0006', 'Animaciones II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprenderemos a relaizar animaciones a través de trayectos dibujados por el usuario (guía de movimiento) y a realizar animaciones de tipos transformaciones de objetos (formas).', 'Animaciones por guía de movimiento.
Animaciones por formas.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0009', 'Informes', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER LOS DIVEROS TIPOS DE  INFOMES EN MICROSOFT PROJECT.
GENERAR INFORMES Y UTILIZAR LAS HERRAMIENTAS DE VISUALIZACION.', 'INFORMES INTEGRADOS.
TIPOS DE INFORMES.
HERRAMIENTAS DE INFORMES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0011', 'Impresiones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER LAS DIVERSAS  OPCIONES DE SALIDA DE UN CMAP.', 'IMPRIMIR UN CMAP.
VER UN MAPA EN UNA WEB.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0012', 'Impresiones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER DIVERSAS CARACTERISTICAS DE IMPRESIÓN A UNA PUBLICACION PUBLISHER ', 'IMPRIMIR SEPARACIONES 
IMPRIMIR COMPOSICION ');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0013', 'Impresión y Distribución de Archivos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'COMPRENDER Y APLICAR LAS DISTINTAS FORMAS DE SALIDA Y VISUALIZACIÓN DE LOS  ARCHIVOS GENERADOS EN MICROSOFT VISIO 2007. ', 'IMPRESIÓN DE ARCHIVOS 
DISTRIBUCION DE ARCHIVOS
');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0016', 'Configuracion de secuencia de actividades', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER SUS CRITERIOS NECESARIOS PARA LA ORGANIZACIÓN DE ACTIVIDADES.', 'SECUENCIA DE ACTIVIDADES.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0017', 'Funciones Financieras II', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0042', 'Novedades en MicroSoft Office 2010', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0043', 'Combinar Correspondencia', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0044', 'Vínculos y Consolidación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0048', 'Documentos Maestros y Subdocumentos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0049', 'Gráficos Dinámicos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0091', 'Configuración de la Acción', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0092', 'Crear y modificar formularios', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0093', 'Integración Word - Excel - Access', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0094', 'Crear y Modificar Tablas', '64', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0095', 'Preparar una Presentación para su entrega', '125', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0111', 'Gráficos dinámicos', '101', NULL, NULL, '', NULL, 'A', 5, 0, 0, 0.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (7, '0113', 'Operaciones de Préstamo', '168', NULL, NULL, '', NULL, 'A', 5, 0, 0, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0002', 'Combinar correspondencia', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprender el proceso de la combinación de correspondencia', 'Definición
Creación del documento modelo
Generación de la fuente de datos
Inserción de los campos de combinación
Ejecución de la combinación de correspondencia');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0006', 'Animaciones III', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprederemos a realizar animaciones de tipo foco llamadas máscaras y a crear botones con animaciones simples en sus estados.', 'Animacion con máscara.
Botones.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0012', 'Publicaciones Web', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'CONOCER Y UTILIZAR LAS HERRAMEINTAS DE DISEÑO WEB EN PUBLISHER 2007', 'PUBLICACION WEB
HIPERVINCULOS Y VISTAS');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0016', 'Utilidad de jclic player', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 50.00, NULL, 'ESTABLECER UN MANEJO Y CONTROL DE LA APLICACIÓN DEL JCLIC PLAYER, QUE PERMITA LA CONFIGURACION Y EJECUCION DE LAS ACTIVIDADES  DESARROLLADAS EN JCLIC AUTHOR.', 'DESCRIPCION DEL ENTORNO.
CREACION DE BIBLIOTECAS.
CREACION DE CARPETAS.
ABRIR PROYECTOS.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0017', 'Funciones Animadas', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0042', 'Portapapeles de Office', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0043', 'Acciones Complementarias', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0044', 'Tablas Dinámicas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0048', 'Crear y Modificar Formularios', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0049', 'Colaboración con un grupo de trabajo', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0091', 'Personalizar la Presentación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0092', 'Ver y organizar información', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0093', 'Gestión Integrada desde OutLook', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0095', 'Entregar una Presentación', '126', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (8, '0112', 'Crear Vínculos', '65', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0006', 'Introducción a Action Script', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprenderemos conceptos básicos de Action Scriptsus eventos y funcionescomo utilizarlos y relacionarloscon los simbolos y botón', 'Action Script.
Botón.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0017', 'Creación de Gráficos', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0043', 'Crear Plantillas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0044', 'Gráficos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0048', 'Personalización de Word', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0049', 'Compartir Documentos en la red SKYDRIVE', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0091', 'Impresión de la Presentación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0092', 'Creación de informes', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0093', 'Colaboración en la red SharePoint', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (9, '0112', 'Dar Formato a los Documentos', '66', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0006', 'Sonido y Audio', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprenderemos a crear,reproducir sonidos e insertar videos en tus aplicaciones.', 'Trabajo con sonido.
Trabajo con video.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0017', 'Listas e Impresiones', NULL, NULL, NULL, '', NULL, 'A', 5, 1, 0, 10.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0044', 'Acciones Complementarias I', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0048', 'Versiones de Word', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0049', 'Auditoría de Formularios', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0089', 'Usar plantillas PreEstablecidas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0090', 'Funciones de Base de Datos', NULL, NULL, NULL, NULL, NULL, 'A', 5, 0, 30, 20.00, NULL, NULL, NULL);
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0091', 'Impresión de Componentes', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0092', 'Personalización con macros', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (10, '0112', 'Guardar e Imprimir Documentos', '67', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0006', 'Publicación de Archivos.', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 15, 10.00, NULL, 'Aprenderemos a publicar archivos flash y a conocer elementos básicos de diseño web.', 'Consideraciones sobre el tamaño de las peliculas.
Publicar una pelicula.');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0044', 'Acciones Complementarias II', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0048', 'Compartir Documentos en red SKYDRIVE', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0049', 'Personalización de Excel', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0089', 'Campos de Texto', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0090', 'Creación de Vinculos', NULL, NULL, NULL, NULL, NULL, 'A', 5, 0, 30, 20.00, NULL, NULL, NULL);
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0091', 'Efectos de Transición', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0092', 'Integrar Access con otras aplicaciones', NULL, NULL, NULL, '', NULL, 'A', 4, 0, 5, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (11, '0112', 'Insertar y Administrar Objetos de Contenido', '71', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0044', 'Macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0048', 'Creación y Ejecución de Macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0049', 'Versiones de Documentos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0089', 'Crear campos de casilla de verificación', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0090', 'Uso de Excel como Base de Datos', NULL, NULL, NULL, NULL, NULL, 'A', 5, 0, 30, 20.00, NULL, NULL, NULL);
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0091', 'Patrón de Diapositivas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (12, '0112', 'Insertar y Administrar las Referencias', '72', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (13, '0048', 'Gestión de Datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (13, '0049', 'Automatización de tareas Macros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (13, '0089', 'Crear campos de listas Desplegables', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (13, '0090', 'Uso de Filtros', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (13, '0112', 'Personalizar la Funcionalidad del Programa', '73', NULL, NULL, '', NULL, 'A', 4, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (14, '0048', 'Realización de Gráficos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (14, '0089', 'Uso de la barra de Herramientas de campo Formulario', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (14, '0090', 'Subtotales', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (15, '0089', 'Interacción de Documentos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (15, '0090', 'Tablas Dinámicas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (16, '0089', 'Inserción de Información desde otras Aplicaciones', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (16, '0090', 'Crear una tabla dinamica a partir de una Base de datos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (17, '0089', 'Fusión de Documentos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (17, '0090', 'Tablas de una Variable y dos Variables', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (18, '0089', 'Sobres y Etiquetas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (18, '0090', 'Calcular Automaticamente Excepto Tablas', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (19, '0089', 'Tablas de Contenido', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');
INSERT INTO tema (idtema, idcurso, descripcion, swf, pdf, doc, regusuario, regfecha, estado, examenpreguntas, examenhoras, examenminutos, costo, preguntadiagnostico, objetivos, contenido) VALUES (19, '0090', 'Buscar Objetivos', NULL, NULL, NULL, '', NULL, 'A', 5, 0, 30, 20.00, NULL, '', '');


-- Completed on 2018-04-18 13:03:47

--
-- PostgreSQL database dump complete
--

