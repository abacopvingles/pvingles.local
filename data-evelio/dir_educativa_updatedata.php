<?php 
$mysqli = new mysqli("127.0.0.1","root","","ingenioydata");

if ($mysqli->connect_errno){
	exit("No se pudo conectar :(");
}

$arrTxtJsonUnits = array(
	"options" => array(),
	"tipo" => "#showpaddopciones",
	"tipofile" => "menus",
	"imagenfondo" => "",
	"infoavancetema" => 100,
	"hijos" => true
);

mensaje('orange' , "SELECT * FROM acad_cursodetalle WHERE idcurso=13 AND tiporecurso='U' ORDER BY orden" );
$mysqli->set_charset("utf8");
$cursosDetallesU = $mysqli->query("SELECT * FROM acad_cursodetalle WHERE idcurso=13 AND tiporecurso='U' ORDER BY orden");
while($cur_det=$cursosDetallesU->fetch_assoc()){
	$mysqli->set_charset("utf8");
	$updateDetU = $mysqli->query("UPDATE acad_cursodetalle SET txtjson = '".json_encode($arrTxtJsonUnits)."' WHERE tiporecurso='U' AND idcursodetalle=".$cur_det["idcursodetalle"]);
	echo "<pre> UPDATING : "; print_r($cur_det); echo "</pre>";
	mensaje('orange' , "UPDATE acad_cursodetalle SET txtjson = '".json_encode($arrTxtJsonUnits)."' WHERE  tiporecurso='U' AND idcursodetalle=".$cur_det["idcursodetalle"] );


	mensaje('orange' , "SELECT * FROM acad_cursodetalle WHERE idcurso=13 AND tiporecurso='S' AND idpadre=".$cur_det['idcursodetalle']." ORDER BY orden" );
	$mysqli->set_charset("utf8");
	$cursosDetallesS = $mysqli->query("SELECT * FROM acad_cursodetalle WHERE idcurso=13 AND tiporecurso='S' AND idpadre=".$cur_det['idcursodetalle']." ORDER BY orden");
	while($cur_detSes=$cursosDetallesS->fetch_assoc()){ echo "<div style='margin-left:30px'>";
		if( (int)$cur_det["orden"]<=2 ) {
			$options = opt_01("detalle_".$cur_detSes['idpadre']."/detalle_".$cur_detSes['idcursodetalle']);
		} else {
			$options = opt_02("detalle_".$cur_detSes['idpadre']."/detalle_".$cur_detSes['idcursodetalle']);
		}

		$arrTxtJsonSes = array(
			"options" => $options,
			"tipo" => "#showpaddopciones",
			"tipofile" => "arriba",
			"imagenfondo" => "",
			"infoavancetema" => 100
		);
		
		echo "<pre> UPDATING : "; print_r($cur_detSes); echo "</pre>";
		mensaje('orange' , "UPDATE acad_cursodetalle SET txtjson = '".json_encode($arrTxtJsonSes)."' WHERE tiporecurso='S' AND idcursodetalle=".$cur_detSes["idcursodetalle"]);
		$mysqli->set_charset("utf8");
		$updateDetS = $mysqli->query("UPDATE acad_cursodetalle SET txtjson = '".json_encode($arrTxtJsonSes)."' WHERE tiporecurso='S' AND idcursodetalle=".$cur_detSes["idcursodetalle"]);
	echo '</div>'; }
}


function opt_01($estruc_dir = '_no_dir_')
{
	return array(
		"1" => array(
			"nombre" => "Sesión",
			"id" => uniqid(),
			"link" => "/static/media/cursos/curso_13/".$estruc_dir."/ses1/index.html",
			"type" => "html",
			"color" => "rgba(255, 255, 255, 1)",
			"colorfondo" => "rgba(187, 68, 68, 1)"
		),
		"2" => array(
			"nombre" => "Manual",
			"id" => uniqid(),
			"link" => "/static/media/cursos/curso_13/".$estruc_dir."/pdf/index.html",
			"type" => "html",
			"color" => "rgba(255, 255, 255, 1)",
			"colorfondo" => "rgba(187, 68, 68, 1)"
		)
	);
}

function opt_02($estruc_dir = '_no_dir_')
{
	return array(
	    "1" => array(
	      "nombre" => "Presentación",
	      "id" => uniqid(),
	      "link" => "/static/media/cursos/curso_13/".$estruc_dir."/ses1/index.html",
	      "type" => "html",
	      "color" => "rgba(255, 255, 255, 1)",
	      "colorfondo" => "rgba(187, 68, 68, 1)"
	    ),
	    "2" => array(
	      "nombre" => "Video",
	      "id" => uniqid(),
	      "link" => "/static/media/cursos/curso_13/".$estruc_dir."/video/index.html",
	      "type" => "html",
	      "color" => "rgba(255, 255, 255, 1)",
	      "colorfondo" => "rgba(187, 68, 68, 1)"
	    ),
	    "3" => array(
	      "nombre" => "Sesión",
	      "id" => uniqid(),
	      "link" => "/static/media/cursos/curso_13/".$estruc_dir."/ses2/index.html",
	      "type" => "html",
	      "color" => "rgba(255, 255, 255, 1)",
	      "colorfondo" => "rgba(187, 68, 68, 1)"
	    ),
	    "4" => array(
	      "nombre" => "Enciclopedia Electrónica",
	      "id" => uniqid(),
	      "link" => "/static/media/cursos/curso_13/".$estruc_dir."/pdf/index.html",
	      "type" => "html",
	      "color" => "rgba(255, 255, 255, 1)",
	      "colorfondo" => "rgba(187, 68, 68, 1)"
	    )
	);
}


function mensaje($color='#38cfd6', $textShow='', $array=array()) {
	if(empty($color)) { $color = '#38cfd6'; }
	echo "<pre class='msj-debug'> <span style='background:".$color."; color: white;'>";
	echo $textShow;
	echo "</span></pre>";
	if(!empty($array)){ var_dump($array); }
}

function scan_directorio($dir='') {
	mensaje( 'green', 'LEYENDO : '.$dir );
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arr = scandir( $dir );
		foreach ($arr as $d) {
			$arrDirectorio[] = iconv("windows-1254", "UTF-8", $d);
		}
		var_dump($arrDirectorio);
		return $arrDirectorio;
	} else {
		mensaje( 'red', '"'.$dir.'" : No es un directorio válido.' );
		return null;
	}
}

function scanear_directorio($dir='') { /* para create_index()::index.php */
	if(is_dir($dir)) {
		$arrDirectorio = array();
		$arrDirectorio = scandir( $dir );
		return $arrDirectorio;
	} else {
		return null;
	}
}

function limpiarArrDir(&$arr) {
	$i = array_search('.', $arr);
	unset($arr[$i]);
	$j = array_search('..', $arr);
	unset($arr[$j]);
	$arr = array_values($arr);
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src);
    if(!is_dir($dst)) {
    	mensaje( 'brown', 'CREANDO DIR : '. $dst );
    	mkdir($dst, 0777);
    }
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
            	mensaje( 'orange', 'COPIANDO : '. $src . '/' . $file ."     ====>>>     ".$dst . '/' . $file );
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}

function create_index($dst, $dir_tmpl) {
	try {
		$archivo = $dst.'/' . 'index' . '.html';
		mensaje( 'brown', 'CREANDO FILE : '.$archivo );
		if(file_exists($archivo)) @unlink($archivo);
		$fp = fopen($archivo, "a");
		$tpl_esquema = $dir_tmpl.'/index.php';

		ob_start();
		require($tpl_esquema);
		$datos = ob_get_contents();
		ob_end_clean();

		$write = fputs($fp, $datos);
		fclose($fp);
		@chmod($archivo,0777);
		//return true;
	} catch (Exception $e) {
		mensaje( 'red', 'Error Creando index.hmtl : ' .$e->getMessage() );
		//return null;
	}
}

function getNewId($pk, $table, $mysqli) {
	$cursonew=$mysqli->query("SELECT MAX($pk) AS newid FROM $table");
	$curnew=$cursonew->fetch_assoc();
	$new_id=intval($curnew["newid"])+1;
	return $new_id;
}

function buscar_db($strQuery, $mysqli) {
	$arrData = array();
	$mysqli->set_charset("utf8");
	$cursos=$mysqli->query($strQuery);
	while ($c=$cursos->fetch_assoc()) {
		$arrData[] = $c ;
	}
	return $arrData;
}