<?php 
set_time_limit(0);
$mysqli = new mysqli("127.0.0.1","root","","ingenioydata");

if ($mysqli->connect_errno){
	exit("No se pudo conectar :(");
}

$strSql = "SELECT * from acad_curso where (e_ini IS NOT NULL AND e_ini<>'') AND e_fin IS NOT NULL";
$cursos = buscar_db($strSql, $mysqli);
mensaje('blue', 'EJECUTANDO: '. $strSql);
mensaje('blue', 'RESULTADOS: ', $cursos);

foreach ($cursos as $cur) {
	$strSql = "SELECT * from acad_cursodetalle WHERE idcurso=".$cur['idcurso']." ORDER BY orden DESC LIMIT 1";
	$ultimo_det = buscar_db($strSql, $mysqli);
	$ultimo_orden = (int)$ultimo_det[0]['orden'];
	mensaje('blue', 'EJECUTANDO: '. $strSql);
	mensaje('blue', 'RESULTADOS: ', $ultimo_det);

	/******** Examen inicial **********/
		/* Agregar Nivel */
		$new_idNivel = getNewId('idnivel', 'niveles', $mysqli);
		$strSql = "INSERT INTO niveles values (".$new_idNivel.", 'Exámen Inicial', 'M', 0, 1, 1, 0, '', '')";
		$addnivel1=$mysqli->query($strSql);
		mensaje('red', 'EJECUTANDO: '. $strSql);

		/* Agregar Cursodetalle */
		$arrTxtJSON = txtJsonExam($cur["e_ini"]);
		$new_idCursoDet = getNewId('idcursodetalle', 'acad_cursodetalle', $mysqli);
		$strSql = "INSERT INTO acad_cursodetalle values (".$new_idCursoDet.", ".$cur['idcurso'].", 0, ".$new_idNivel.", 'M', 0, '', 0, '', 0, '".json_encode($arrTxtJSON)."' )";
		$adddetalle1=$mysqli->query($strSql);
		mensaje('orange', 'EJECUTANDO: '. $strSql);
	
	/******** Examen final **********/
		/* Agregar Nivel */
		$new_idNivelF = getNewId('idnivel', 'niveles', $mysqli);
		$strSql = "INSERT INTO niveles values (".$new_idNivelF.", 'Exámen Final', 'M', 0, 1, 1, ".($ultimo_orden+1).", '', '')";
		$addnivel2=$mysqli->query($strSql);
		mensaje('red', 'EJECUTANDO: '. $strSql);

		/* Agregar Cursodetalle */
		$arrTxtJSON = txtJsonExam($cur["e_fin"]);
		$new_idCursoDetF = getNewId('idcursodetalle', 'acad_cursodetalle', $mysqli);
		$strSql = "INSERT INTO acad_cursodetalle values (".$new_idCursoDetF.", ".$cur['idcurso'].", ".($ultimo_orden+1).", ".$new_idNivelF.", 'M', 0, '', 0, '', 0, '".json_encode($arrTxtJSON)."' )";
		$adddetalle2=$mysqli->query($strSql);
		mensaje('orange', 'EJECUTANDO: '. $strSql);

	/******** Actualizar orden *******/
	$strSql = "SELECT * from acad_cursodetalle WHERE idcurso=".$cur['idcurso']." ORDER BY orden ASC";
	$detalles = buscar_db($strSql, $mysqli);
	mensaje('green', 'EJECUTANDO: '. $strSql);
	mensaje('green', 'RESULTADOS: ', $detalles);
	foreach ($detalles as $det) {
		/******** Actualizar orden DETALLE *******/
		$strSql = "UPDATE acad_cursodetalle SET orden=".((int)$det['orden'] + 1)." WHERE idcursodetalle= ".$det['idcursodetalle'];
		$editdetalle1=$mysqli->query( $strSql );
		mensaje('brown', 'EJECUTANDO: '. $strSql);

		/******** Actualizar orden NIVEL *******/
		$strSql = "UPDATE niveles SET orden=".((int)$det['orden'] + 1)." WHERE idnivel= ".$det['idrecurso'];
		$editdetalle1=$mysqli->query( $strSql );
		mensaje('brown', 'EJECUTANDO: '. $strSql);
	}
}



function mensaje($color='#38cfd6', $textShow='', $array=array()) {
	if(empty($color)) { $color = '#38cfd6'; }
	echo "<pre class='msj-debug'> <span style='background:".$color."; color: white;'>";
	echo $textShow;
	echo "</span></pre>";
	if(!empty($array)){ echo "<pre>"; print_r($array); echo "</pre>"; }
}

function txtJsonExam($idExamen) {
	return array(
	  "tipo" => "#showpaddcontenido",
	  "imagenfondo" => "",
	  "infoavancetema" => 100,
	  "colorfondo" => "rgba(0, 0, 0, 0)",
	  "link" => "http://ingenioytalento.com/smartcourse/quiz/ver/?idexamen=".$idExamen,
	  "typelink" => "smartquiz"
	);
}

function getNewId($pk, $table, $mysqli) {
	$cursonew=$mysqli->query("SELECT MAX($pk) AS newid FROM $table");
	$curnew=$cursonew->fetch_assoc();
	$new_id=intval($curnew["newid"])+1;
	return $new_id;
}

function buscar_db($strQuery, $mysqli) {
	$arrData = array();
	$mysqli->set_charset("utf8");
	$cursos=$mysqli->query($strQuery);
	while ($c=$cursos->fetch_assoc()) {
		$arrData[] = $c ;
	}
	return $arrData;
}