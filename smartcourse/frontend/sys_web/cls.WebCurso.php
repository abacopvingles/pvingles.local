<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebCurso extends JrWeb
{
	private $oNegCurso;

	public function __construct()
	{
		parent::__construct();		
        //$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;*/
        $this->oNegCurso = new NegAcad_curso;
	}

	public function defecto(){
		global $aplicacion;
		$acc=!empty($_REQUEST['acc']) ? $_REQUEST['acc'] : '';
		$this->documento->script('tinymce.min', '/libs/tinymce/');
        $this->documento->script('savehtml', '/libs/tinymce/plugins/chingo/');
		if(empty($acc))
			return $this->verlistado();
		elseif($acc=='crear'){
			return $this->crear();
		}else{
			return $this->vercurso();
		}
	}

	public function crear(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Smartcourse'),true);
			$this->esquema = 'inicio';
			$this->documento->plantilla ='general';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])) $this->curso=$curso[0];
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function verlistado()
	{
		try{
			global $aplicacion;	
			$filtro=array();
			@extract($_REQUEST);
			$this->cursos=$this->oNegCurso->buscar($filtro);
			$vista=!empty($_REQUEST["vista"])?$_REQUEST["vista"]:'vista01';
			$this->esquema = 'listado-cursos-'.$vista;
			$this->documento->plantilla = !empty($_REQUEST['plt']) ? $_REQUEST['plt'] : 'general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function vercurso(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Curso'),true);
			$this->esquema = 'curso-ver';
			$this->documento->plantilla ='general';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					$this->curso=$curso[0];
					var_dump($this->curso);
				}
				
			}			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}