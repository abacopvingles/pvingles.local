<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegHerramientas', RUTA_BASE, 'sys_negocio');
class WebTools extends JrWeb
{
    private $oNegHerramientas;

    public function __construct()
    {
        parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegHerramientas = new NegHerramientas;
    }

    public function defecto(){
        return false;
    }

    public function json_buscarGames(){
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $filtros=array();

            $filtros["titulo"]=!empty($_REQUEST["txtBuscar"])?$_REQUEST["txtBuscar"]:'';
            $filtros["idnivel"]=!empty($_REQUEST["nivel"])?$_REQUEST["nivel"]:'';
            $filtros["idunidad"]=!empty($_REQUEST["unidad"])?$_REQUEST["unidad"]:'';
            $filtros["idactividad"]=!empty($_REQUEST["actividad"])?$_REQUEST["actividad"]:'';
            $juegos=$this->oNegHerramientas->buscarJuegos($filtros);
            $data=array('code'=>'ok','data'=>$juegos);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }
}