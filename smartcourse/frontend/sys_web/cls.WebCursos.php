<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		21-05-2017 
 * @copyright	Copyright (C) 21-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
/*JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');*/
JrCargador::clase('sys_negocio::NegAcad_categorias', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_curso', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegSesion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAcad_cursodetalle', RUTA_BASE, 'sys_negocio');
class WebCursos extends JrWeb
{
	private $oNegCurso;

	public function __construct()
	{
		parent::__construct();		
        $this->oNegAcad_categorias = new NegAcad_categorias;
        $this->oNegCursodetalle = new NegAcad_cursodetalle;
        $this->oNegCurso = new NegAcad_curso;
	}

	public function agregarhijos($m){
		try{	
			$ht='';
			$mh=!empty($m["hijos"])?true:false;
			if($mh==true){
				$hth='';
				foreach ($m["hijos"] as $mj){
					$hth.=$this->agregarhijos($mj);
				}				
			}
			$mht=!empty($m["html"])?'data-link="'.$m["html"].'"':'';
			$ht='<li class="'.($mh?'hijos':'').'" id="me_'.$m["idcursodetalle"].'" data-idrecurso="'.$m["idrecurso"].'" data-idcursodetalle="'.$m["idcursodetalle"].'" data-idpadre="'.$m["idpadre"].'" data-orden="'.$m["orden"].'" data-tipo="'.$m["tiporecurso"].'" '.$mht.' data-imagen="'.$m["imagen"].'" data-esfinal="'.$m["esfinal"].'" >
				<div>
					<span class="mnombre">'.$m["nombre"].'</span>
					<span class="macciones">
						<i class="mmsubmenu fa fa-sort-down verpopoverm" data-content="Ocultar Submenus" ></i>
						<i class="mmsubmenu fa fa-sort-up verpopoverm" data-content="Mostrar Submenus" ></i>
						<i class="addcontent fa fa-eye verpopoverm" data-content="Ver contenido" data-placement="top"></i>
						<i class="magregar fa fa-plus verpopoverm" data-content="Agregar Submenu" data-placement="top"></i>
						<i class="meditar fa fa-pencil verpopoverm" data-content="Editar Menu" data-placement="top"></i>							
						<i class="meliminar fa fa-trash verpopoverm" data-content="Eliminar" data-placement="top"></i>
						<i class="mmove fa fa-arrows-alt verpopoverm" data-content="Mover Menu" data-placement="right"></i>
						<!--i class="mcopylink fa fa-link verpopoverm" data-content="Mover Menu" data-placement="right"></i-->
						<!--span style="display:inline-block">								
							<i class="mmoveup fa fa-angle-double-up verpopoverm" data-content="Mover Arriba"></i>
							<i class="mmovedown fa fa-angle-double-down verpopoverm" data-content="Mover abajo"></i>
						</span-->
					</span>
				</div>
				'.($mh?('<ul id="me_hijo'.$m["idcursodetalle"].'">'.$hth.'</ul>'):'').'
			</li>';
			return $ht;
		}catch(Exception $e) {
				return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function defecto(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/inicio';
			$this->documento->plantilla ='general';

			$this->usuActivo = NegSesion::getUsuario();
			//var_dump($this->usuActivo);
			$rolActual=$this->usuActivo["idrol"];
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->usuarionombre=$this->usuActivo["nombre_full"];
			$curso=array('idcurso'=>0);
			$categoriasdelcurso=array();
			$temas=array();
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					$curso=$curso[0];
					$categoriasdelcurso=$this->oNegCurso->buscarxcategoria(array('idcurso'=>$curso["idcurso"],'solocategoria'=>true));
					$temas=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$curso["idcurso"]));
				}
			}
			$this->curso=json_encode(array('datos'=>$curso,'categorias'=>$categoriasdelcurso,'temas'=>$temas));
			//$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));			
			//$this->idcurso=$idcurso;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/inicio-ver';
			$this->documento->plantilla ='general';
			$this->usuActivo = NegSesion::getUsuario();
			//var_dump($this->usuActivo);
			$rolActual=$this->usuActivo["idrol"];
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:'';
			$this->usuarionombre=$this->usuActivo["nombre_full"];
			$curso=array('idcurso'=>0);
			$categoriasdelcurso=array();
			$temas=array();
			if(!empty($idcurso)){
				$curso=$this->oNegCurso->buscar(array('idcurso'=>$idcurso));
				if(!empty($curso[0])){
					$curso=$curso[0];
					$categoriasdelcurso=$this->oNegCurso->buscarxcategoria(array('idcurso'=>$curso["idcurso"],'solocategoria'=>true));
					$temas=$this->oNegCursodetalle->buscarconnivel(array('idcurso'=>$curso["idcurso"]));
				}
			}

			$this->curso=json_encode(array('datos'=>$curso,'categorias'=>$categoriasdelcurso,'temas'=>$temas));
			$this->returnlink=!empty($_REQUEST["returnlink"])?$_REQUEST["returnlink"]:$this->documento->getUrlBase();

			//$this->categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));			
			//$this->idcurso=$idcurso;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function paso1(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/1datosdelcurso';
			$this->documento->plantilla ='blanco';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function paso1categorias(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/2categorias';
			$this->documento->plantilla ='blanco';
			$categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
			$adcategoria=array();
			if(!empty($categorias))
				foreach($categorias as $cat){
					$cat['hijos']=$this->oNegAcad_categorias->buscar(array('idpadre'=>$cat["idcategoria"]));					
					array_push($adcategoria,$cat);
				}
			$this->categorias=$adcategoria;
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$this->categoriasdelcurso=array();
			if(!empty($idcurso)){
				$this->categoriasdelcurso=$this->oNegCurso->buscarxcategoria(array('idcurso'=>$idcurso,'solocategoria'=>true));
			}
			$returnjson=!empty($_REQUEST["rjson"])?$_REQUEST["rjson"]:false;
			if($returnjson=='si'){
				echo json_encode(array('code'=>200,'categorias'=>$this->categorias,'categoriascurso'=>$this->categoriasdelcurso));
				exit(0);
			}
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function paso1temas(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/3temas';
			$this->documento->plantilla ='blanco';
			/*$categorias=$this->oNegAcad_categorias->buscar(array('idpadre'=>0));
			$adcategoria=array();
			if(!empty($categorias))
				foreach($categorias as $cat){
					$cat['hijos']=$this->oNegAcad_categorias->buscar(array('idpadre'=>$cat["idcategoria"]));					
					array_push($adcategoria,$cat);
				}*/
			//$this->categorias=$adcategoria;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function paso1addtemas(){
		try{
			global $aplicacion;
			$this->esquema = 'cursos/3temas-frm';
			$this->documento->plantilla ='blanco';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/*funciones json*/
	public function jsontemas(){
		try{
			global $aplicacion;			
			$this->documento->plantilla ='blanco';
			$idcurso=!empty($_REQUEST["idcurso"])?$_REQUEST["idcurso"]:0;
			$filtros=array();
			$filtros["idcurso"]=$idcurso;
			$temas=$this->oNegCursodetalle->buscarconnivel($filtros);
			echo json_encode(array('code'=>200,'data'=>$temas));
			}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function jsoncategorias(){
		try{
			global $aplicacion;			
			$this->documento->plantilla ='blanco';
			$filtros=array();
			$filtros["idpadre"]=isset($_REQUEST["idcatpadre"])?$_REQUEST["idcatpadre"]:0;
			//if(!empty($_REQUEST["idcategoria"]))
			$categorias=$this->oNegAcad_categorias->buscar($filtros);
			echo json_encode(array('code'=>200,'data'=>$categorias));
			}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function jsoncursos($filtros=array()){
		try{
			global $aplicacion;
			$this->documento->plantilla ='blanco';
			$filtros=array();
			$this->cursos=$this->oNegCurso->buscarxcategoria($filtros);
			return $this->cursos;
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function jsonbuscarxcategoria(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;
			$filtros=array();
			if(isset($_REQUEST["idcategoria"])&&@$_REQUEST["idcategoria"]!='')$filtros["idcategoria"]=$_REQUEST["idcategoria"];
			if(isset($_REQUEST["texto"])&&@$_REQUEST["texto"]!='')$filtros["texto"]=$_REQUEST["texto"];			
			$this->datos=$this->oNegCurso->buscarxcategoria($filtros);
			if(empty($this->datos)) echo json_encode(array('code'=>'ok','data'=>''));
			else{
				$cat=array();
				foreach ($this->datos as $cur){
					if(empty($cat[$cur["idcategoria"]])){
						$cat[$cur["idcategoria"]]=array();
						$cat[$cur["idcategoria"]]["nombre"]=$cur["categoria"];
						$cat[$cur["idcategoria"]]["hijos"]=array();
					}
					$imgcursodefecto=URL_MEDIA.'/static/media/cursos/nofoto.jpg';
					$imgtmp=substr(RUTA_MEDIA,0,-1).$cur["imagen"];                 
                    $cur["imagen"]=is_file($imgtmp)?URL_MEDIA.$cur["imagen"]:$imgcursodefecto; 
					$cat[$cur["idcategoria"]]["hijos"][$cur["idcurso"]]=$cur;
				}
				$this->datos=$cat;
			}
			echo json_encode(array('code'=>200,'data'=>$this->datos));
			exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>JrTexto::_($e->getMessage())));
            exit(0);
        }
	}
// vistas para proyectos.
	public function listar(){
		try{
			global $aplicacion;
			$this->esquema = 'cur_proyecto/inicio';
			$this->documento->plantilla ='general';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}

	}
}