<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-10-2017 
 * @copyright	Copyright (C) 18-10-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebNiveles extends JrWeb
{
	private $oNegNiveles;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegNiveles = new NegNiveles;	
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"]; else $filtros["tipo"]='N';
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
		
			$this->datos=$this->oNegNiveles->buscar($filtros);
			//$this->fkidpadre=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Niveles'), true);
			$this->esquema = 'niveles';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function todos(){
		try{
            global $aplicacion;  
            $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $idunidad_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($idunidad_)?$idunidad_:($_idunidad);  

            $this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
           
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->setTitulo(JrTexto::_('All Activities'), true);            
            $this->esquema = 'alumno/sesiones';
            return parent::getEsquema();
        }catch(Exception $e) {
           return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function ordernarActividad(){
		try{
			global $aplicacion; 
				$niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
				foreach ($niveles as $nivel){
					$unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$nivel["idnivel"]));
					$ordenU=0;
					foreach ($unidades as $unidad){
						$ordenU++;	
						$this->oNegNiveles->setCampo($unidad["idnivel"],'orden',$ordenU);
						$ordenA=0;
						$actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$unidad["idnivel"]));
						foreach ($actividades as $act){
							$ordenA++;							
							$res=$this->oNegNiveles->setCampo($act["idnivel"],'orden',$ordenA);
							echo $res."-".$act["nombre"]."-".$ordenA."<br>";
						}
					}
				}			
			$this->esquema = 'returnjosn';
			return parent::getEsquema();
		}catch(Exception $e) {
           return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function getxPadrejson($filtro)
	{
		try {               
			$datos=$this->oNegNiveles->buscar($filtro);		
			$this->documento->plantilla='returnjson';
			echo json_encode($datos);
			return parent::getEsquema();
		} catch(Exception $e) {
			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
		} 

	}

	public function agregar()
	{
		try {
			global $aplicacion;	
			$this->frmaccion='Nuevo';
			$tipo=!empty($_REQUEST["tipo"])?$_REQUEST["tipo"]:'N';
			$idpadre=!empty($_REQUEST["idpadre"])?$_REQUEST["idpadre"]:0;
			$this->orden=$this->oNegNiveles->maxorden($tipo,$idpadre);
			$this->documento->setTitulo(JrTexto::_('Level').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;			
			$this->frmaccion='Editar';
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->idnivel = @$_GET['idnivel'];			
			$this->datos = $this->oNegNiveles->dataNiveles;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Level').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			//$this->fkidpadre=$this->oNegNiveles->buscar();			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');
			$this->esquema = 'niveles-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function nivelesedit(){
		try {
			global $aplicacion;	
			$ver=!empty($_REQUEST['ver'])?$_REQUEST['ver']:'';
			$idnivel=!empty($_REQUEST['idnivel'])?$_REQUEST['idnivel']:'';
			if(empty($ver)||empty($idnivel)){
				$this->esquema = 'academico/nivel-400';
			}else{
				$this->oNegNiveles->idnivel = $idnivel;
				$this->datos = $this->oNegNiveles->dataNiveles;
				if(empty($this->datos)){
					$this->esquema = 'academico/nivel-400';
				}else{
					if($ver==='N'){
						$this->esquema = 'academico/nivel-frm';
					}elseif($ver==='U'){
						$this->esquema = 'academico/unidad-frm';
					}elseif($ver==='L'){
						$this->esquema = 'academico/actividad-frm';
					}
				}
				//var_dump($this->esquema);
			}
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
	// ========================== Funciones ajax ========================== //
	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			$filtros=array();
			if(isset($_REQUEST["idnivel"])&&@$_REQUEST["idnivel"]!='')$filtros["idnivel"]=$_REQUEST["idnivel"];
			if(isset($_REQUEST["nombre"])&&@$_REQUEST["nombre"]!='')$filtros["nombre"]=$_REQUEST["nombre"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["idpadre"])&&@$_REQUEST["idpadre"]!='')$filtros["idpadre"]=$_REQUEST["idpadre"];
			if(isset($_REQUEST["idpersonal"])&&@$_REQUEST["idpersonal"]!='')$filtros["idpersonal"]=$_REQUEST["idpersonal"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
			if(isset($_REQUEST["orden"])&&@$_REQUEST["orden"]!='')$filtros["orden"]=$_REQUEST["orden"];
						
			$this->datos=$this->oNegNiveles->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarNiveles(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkIdnivel)) {
				$this->oNegNiveles->idnivel = $frm['pkIdnivel'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	JrCargador::clase("sys_negocio::sistema::NegTools", RUTA_SITIO, "sys_negocio/sistema");
				$this->oNegNiveles->nombre=@$txtNombre;
				$this->oNegNiveles->tipo=@$txtTipo;
				$this->oNegNiveles->idpadre=@$txtIdpadre;
				$this->oNegNiveles->idpersonal=@$txtIdpersonal;
				$this->oNegNiveles->estado=@$txtEstado;
				$this->oNegNiveles->orden=@$txtOrden;
				$this->oNegNiveles->descripcion=@$txtDescripcion;
					
            if($accion=='_add') {
            	$res=$this->oNegNiveles->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Niveles')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNiveles->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Niveles')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function ordenarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;	
			$filtros=array();
			if(empty($_REQUEST["idNivel"])||empty($_REQUEST["ordenaa"])) {
				echo json_encode(array('code'=>'ok','data'=>null)); exit();
			}
			$idnivel=$_REQUEST["idNivel"];
			$ordenaa=$_REQUEST["ordenaa"];
			$res=$this->oNegNiveles->ordenar2($idnivel,$ordenaa);
			echo json_encode(array('code'=>'ok','data'=>$res));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }		
	}

	public function addcampo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            } 
            @extract($_POST); 
            if(!empty($idnivel)&&!empty($campo)){            	
				$res=$this->oNegNiveles->setCampo(intval($idnivel), $campo, @$valor);
				echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Course')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
				exit(0);			
			}         	
			echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Error inesperado')));           	
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
    }

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNiveles(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnivel'])) {
					$this->oNegNiveles->idnivel = $frm['pkIdnivel'];
				}
				$usuarioAct = NegSesion::getUsuario();
				$imagen=str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$frm["imagen"]);
					$this->oNegNiveles->nombre=@$frm["txtNombre"];
					$this->oNegNiveles->tipo=@$frm["txtTipo"];
					$this->oNegNiveles->idpadre=@$frm["txtIdpadre"];
					$this->oNegNiveles->idpersonal=@$usuarioAct["dni"];
					$this->oNegNiveles->estado=@$frm["txtEstado"];
					$this->oNegNiveles->orden=@$frm["txtOrden"];
					$this->oNegNiveles->imagen=@$imagen;
					$this->oNegNiveles->descripcion=@$descripcion;
					
				   if(@$frm["accion"]=="Nuevo"){
						$res=$this->oNegNiveles->agregar();
					}else{
						$res=$this->oNegNiveles->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNiveles->idnivel);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNiveles(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNiveles->__set('idnivel', $pk);
				$this->datos = $this->oNegNiveles->dataNiveles;
				$res=$this->oNegNiveles->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNiveles->__set('idnivel', $pk);
				$res=$this->oNegNiveles->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$this->oNegNiveles->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegNiveles->buscar($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xOrdenar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$frm=$args[0];
				$res=$this->oNegNiveles->ordenar($frm);
				$oRespAjax->setReturnValue($res);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}