<?php 
$idgui=uniqid(); 
?>
<style type="text/css">
	.trclone{ display: none; }
	.removetcat{ cursor: pointer; }
</style>
<div class="card text-center shadow">				 
  <div class="card-body">   
	<div class="row" id="categorias<?php echo $idgui; ?>">
		<div class="col-md-4 form-group">
            <label style=""><?php echo JrTexto::_('Categorias');?></label> 
			<div class="cajaselect">
              <select name="categorias" id="categoria" class="form-control">
              	<?php $cat1=array(); $i=0;
              	if(!empty($this->categorias))
              		foreach ($this->categorias as $cat){ $i++;
              			if($i==1) $cat1=$cat["hijos"]; ?>
              			<option value="<?php echo @$cat["idcategoria"];?>"><?php echo @$cat["nombre"];?></option>
              	<?php } ?>
              </select>
          	</div>		            					           
        </div>
        <div class="col-md-4 form-group">							        	
            <label style=""><?php echo JrTexto::_('Subcategoria');?></label> 
            <div class="cajaselect">
              <select name="subcategorias" id="subcategoria" class="form-control">
              	<option value="0">Todas las subcategorias</option>
              	<?php $cat1=array();
              	if(!empty($cat1))
              		foreach ($cat1 as $cat){ ?>
              			<option value="<?php echo @$cat["idcategoria"];?>"><?php echo @$cat["nombre"];?></option>
              	<?php } ?>
              </select>
          	</div>			           
        </div>
        <div class="col-md-4 form-group">
            <div><br></div> 
            <span class="btn btn-primary addcategoria"><i class="fa fa-plus"></i> Agregar</span>
        </div>	       
    </div>		       
    <div class="row">
    	<div class="col-md-6">
        <table class="table table-striped tablecategorias">
        <tr><th colspan="2">Categorias Agregadas</th></tr>
        <tr class="trclone"><td class="text-left">aaa</td><td><i class="fa fa-trash removetcat"></i></td></tr>          
        </table>    		
    	</div>
    </div>			   
  </div>
  <div class="card-footer text-muted">
  	<button type="button" class="btn btn-warning btnback"><i class=" fa fa-arrow-left"></i> Anterior</button>
    <button type="submit" class="btn btn-primary btnnext">Continuar <i class=" fa fa-arrow-right"></i></button>
  </div>
</div>
<script type="text/javascript">
	var categorias=<?php echo json_encode($this->categorias); ?>;
	$(document).ready(function(ev){
		let idgui='<?php echo $idgui; ?>';
		$('#categorias'+idgui).on('change','#categoria',function(ev){
			let id=parseInt($(this).val());			
			$.each(categorias,function(i,v){
				let idcat=parseInt(v.idcategoria);
				if(idcat==id){
					let cat=v.hijos;
					$('#categorias'+idgui).find('#subcategoria').find('option').remove();
					if(cat.length>0){
						$.each(cat,function(i,v){							
							let op='<option value="'+v.idcategoria+'">'+v.nombre+'</option>';
							$('#categorias'+idgui).find('#subcategoria').append(op);
						})
					}else{
						let op='<option value="0">Todas las subcategoria</option>';
						$('#categorias'+idgui).find('#subcategoria').append(op);
					}
				}
			})
		});
	})
</script>