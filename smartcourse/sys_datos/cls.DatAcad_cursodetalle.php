<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		09-02-2018  
  * @copyright	Copyright (C) 2018. Todos los derechos reservados.
 */ 
class DatAcad_cursodetalle extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM acad_cursodetalle";
			
			$cond = array();		
			
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["tiporecurso"])) {
					$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if(isset($filtros["idlogro"])) {
					$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(isset($filtros["esfinal"])) {
					$cond[] = "esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}


	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM acad_cursodetalle";			
			
			$cond = array();
			
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["tiporecurso"])) {
					$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if(isset($filtros["idlogro"])) {
					$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			
			if(isset($filtros["esfinal"])) {
					$cond[] = "esfinal = " . $this->oBD->escapar($filtros["esfinal"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function buscarconnivel($filtros=null){
		try {
			$sql = "SELECT cd.*, ni.idnivel, ni.nombre, ni.idpadre as padrenivel, descripcion, imagen, (SELECT count(*) from acad_cursodetalle acd WHERE acd.idpadre=cd.idcursodetalle ) as nhijos,txtjson FROM acad_cursodetalle cd LEFT JOIN niveles ni ON idrecurso=idnivel ";

			//LEFT JOIN acad_sesionhtml sh ON cd.idcursodetalle=sh.idcursodetalle";				
			$cond = array();
			
			if(isset($filtros["idcursodetalle"])) {
					$cond[] = "cd.idcursodetalle = " . $this->oBD->escapar($filtros["idcursodetalle"]);
			}
			if(isset($filtros["idcurso"])) {
					$cond[] = "cd.idcurso = " . $this->oBD->escapar($filtros["idcurso"]);
			}
			if(isset($filtros["orden"])) {
					$cond[] = "cd.orden = " . $this->oBD->escapar($filtros["orden"]);
			}
			if(isset($filtros["idrecurso"])) {
					$cond[] = "idrecurso = " . $this->oBD->escapar($filtros["idrecurso"]);
			}
			if(isset($filtros["tiporecurso"])) {
					$cond[] = "tiporecurso = " . $this->oBD->escapar($filtros["tiporecurso"]);
			}
			if(isset($filtros["idlogro"])) {
					$cond[] = "idlogro = " . $this->oBD->escapar($filtros["idlogro"]);
			}
			if(isset($filtros["url"])) {
					$cond[] = "url = " . $this->oBD->escapar($filtros["url"]);
			}
			if(isset($filtros["idpadre"])) {
					$cond[] = "cd.idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			/*if(isset($filtros["htmlnotnull"])) {
					$cond[] = "( html IS NOT NULL AND html!='') ";
			}*/

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}

			if(!empty($filtros["orderBy"]))
			  $sql.=" ORDER BY cd.orden ASC , ".$filtros["orderBy"];
			else		
				$sql .= " ORDER BY cd.orden ASC ";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function maxorden($idcurso,$idpadre){
		try{
			$sql = "SELECT max(orden) as orden FROM acad_cursodetalle";
			$cond = array();
			if(isset($idcurso)) {
				$cond[] = "idcurso = " . $this->oBD->escapar($idcurso);
			}
			if(isset($idpadre)){
				$cond[] = "idpadre = " . $this->oBD->escapar($idpadre);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}


	public function insertoupdate($id, $idcurso,$orden,$idrecurso,$tiporecurso,$idlogro,$url,$idpadre=0,$color,$esfinal=0){		
		try {			
			$this->iniciarTransaccion('dat_acad_insertupdate');
			$acc='add';
			if(!empty($id)){
				$datos=$this->get($id);
				if(!empty($datos)){
					$acc='editar';
					$id=$this->actualizar($id, $idcurso,$orden,$idrecurso,$tiporecurso,$idlogro,$url,$idpadre,$color,$esfinal);
				}
			}
			if($acc=='add'){
				$idpadre=!empty($idpadre)?$idpadre:0;
				$orden=$this->maxorden($idcurso,$idpadre);
				$orden++;
				$id=$this->insertar($idcurso,$orden,$idrecurso,$tiporecurso,$idlogro,$url,$idpadre,$color,$esfinal);			
			}
			$this->terminarTransaccion('dat_acad_insertupdate');
			return array('idcursodetalle' => $id,'idpadre'=>$idpadre,'orden'=>$orden );
		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_niveles_insertupdate');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Niveles").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idcurso,$orden,$idrecurso,$tiporecurso,$idlogro,$url,$idpadre,$color,$esfinal)
	{
		try{			
			$this->iniciarTransaccion('dat_acad_cursodetalle_insert');			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM acad_cursodetalle");
			++$id;			
			$estados = array('idcursodetalle' => $id							
							,'idcurso'=>$idcurso
							,'orden'=>$orden
							,'idrecurso'=>$idrecurso
							,'tiporecurso'=>$tiporecurso
							,'idlogro'=>$idlogro
							,'url'=>$url
							,'idpadre'=>!empty($idpadre)?$idpadre:0
							,'color'=>$color
							,'esfinal'=>!empty($esfinal)?$esfinal:0
							,'txtjson'=>''
							);
			
			$this->oBD->insert('acad_cursodetalle', $estados);			
			$this->terminarTransaccion('dat_acad_cursodetalle_insert');			
			return $id;
		}catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursodetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function duplicar($idcursodetalle,$data)
	{		
		try{
			$this->iniciarTransaccion('dat_acad_cursodetalle_insert');
			$dt=$this->get($idcursodetalle);
			if($dt==null){
				throw new Exception("ERROR\n ".JrTexto::_("curso detalle no existe"));
				return false;
			}
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idcursodetalle) FROM acad_cursodetalle");
			++$id;
			$estados = array('idcursodetalle' => $id					
							,'idcurso'=>!empty($data["idcurso"])?$data["idcurso"]:$dt["idcurso"]
							,'orden'=>!empty($data["orden"])?$data["orden"]:$dt["orden"]
							,'idrecurso'=>!empty($data["idrecurso"])?$data["idrecurso"]:$dt["idrecurso"]  
							,'tiporecurso'=>!empty($data["tiporecurso"])?$data["tiporecurso"]:$dt["tiporecurso"]  
							,'idlogro'=>!empty($data["idlogro"])?$data["idlogro"]:$dt["idlogro"]   
							,'url'=>!empty($data["url"])?$data["url"]:$dt["url"]
							,'idpadre'=>!empty($data["idpadre"])?$data["idpadre"]:$dt["idpadre"]
							,'color'=>!empty($data["color"])?$data["color"]:$dt["color"]
							,'esfinal'=>!empty($data["esfinal"])?$data["esfinal"]:$dt["esfinal"]
							);			
			$this->oBD->insert('acad_cursodetalle', $estados);			
			$this->terminarTransaccion('dat_acad_cursodetalle_insert');			
			return $id;
		}catch(Exception $e) {
			$this->cancelarTransaccion('dat_acad_cursodetalle_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function actualizar($id, $idcurso,$orden,$idrecurso,$tiporecurso,$idlogro,$url,$idpadre,$color,$esfinal)
	{
		try {
			$this->iniciarTransaccion('dat_acad_cursodetalle_update');
			$estados = array('idcurso'=>$idcurso
							,'orden'=>$orden
							,'idrecurso'=>$idrecurso
							,'tiporecurso'=>$tiporecurso
							,'idlogro'=>$idlogro
							,'url'=>$url
							,'idpadre'=>!empty($idpadre)?$idpadre:0
							,'color'=>$color
							,'esfinal'=>!empty($esfinal)?$esfinal:0
							);
			
			$this->oBD->update('acad_cursodetalle ', $estados, array('idcursodetalle' => $id));
		    $this->terminarTransaccion('dat_acad_cursodetalle_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM acad_cursodetalle  "
					. " WHERE idcursodetalle = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('acad_cursodetalle', array('idcursodetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function eliminar2($campo,$valor)
	{
		try{			
			return $this->oBD->delete('acad_cursodetalle', array($campo => $valor));

		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('acad_cursodetalle', array($propiedad => $valor), array('idcursodetalle' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Acad_cursodetalle").": " . $e->getMessage());
		}
	} 
		
}