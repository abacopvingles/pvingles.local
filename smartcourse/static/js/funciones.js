var swi_keys_neu = Array(0, 8, 9, 16, 17, 18, 46);
var swi_keys_int = Array(0, 8, 9, 16, 17, 18);
var nom_dia_semana = Array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
var _sysrdtjson='';
Array.prototype.existe = function(element) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == element) {
            return true;
        }
    }
    return false;
}

function redir(pg){
	window.location.href=pg;	
}

function irpage(){
	document.location=document.location;
}

$(document).ready(function(){
	$('body').on('keypress', '.moneda', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_neu.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	
	$('body').on('keypress', '.entero', function(event) {
		var keynum = window.event ? window.event.keyCode : event.which;
		if(true == swi_keys_int.existe(keynum)) { return true;}
		return /\d/.test(String.fromCharCode(keynum));
	});
	if( typeof datepicker !== 'undefined' && jQuery.isFunction(datepicker) ) {
		$('.fecha').datepicker({'format' : 'yyyy-mm-dd'});
	}
	
	$('body').on('click', '.fecha', function () {
        $(this).datepicker({autoclose: true});
		$(this).datepicker('show');
    });
	
	$('.fecha').keydown(function(event){
		if(9 == event.keyCode) {
			$(this).datepicker('hide');
		}
	});
	
	$(document).ajaxStart(function() {
		oncargandoxajax();
	});
	
	$(document).ajaxStop(function() {
		offcargandoxajax();
	});
});

function agregar_msj_interno(tipo, mensaje) {
	html = '<div class="alert alert-' + tipo + '" role="alert">'
			+ '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> &nbsp;'
			+ '<span class="sr-only">Error: </span>' + mensaje
			+ '</div>';
	
	$('#msj-interno').empty().html(html);
}

function moneda(value, decimals) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
	separators = [',', "'", '.'];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function mostrar_notificacion(titulo, mensaje, tipo) {
	new PNotify({title: titulo, text: mensaje, type: tipo, hide: true});
}

function diff_horas_AMPM(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(pasar_12_24(hora_i));
	h_f = pasar_hora_a_horas(pasar_12_24(hora_f));	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	} else {
		horas = 24 - h_i + h_f;
	}	
	return horas;
}

function pasar_12_24(hora) {
	if(!hora || '' == hora) {
		return;
	}
	
	if('PM' == hora.toString().toUpperCase().substr(-2)) {
		hora = hora.substr(0, 5).trim();
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) < 12) ? parseInt(hora[0]) + 12 : parseInt(hora[0]);
		hora = hora[0] + ':' + (parseInt(hora[1])<10?('0'+hora[1]):hora[1])+':00';
	} else {
		hora = hora.substr(0, 5).trim();
		hora = hora.split(':');
		hora[0] = (parseInt(hora[0]) == 12) ? '00' : hora[0];
		hora = (parseInt(hora[0])<10?('0'+hora[0]):hora[0]) + ':' +hora[1]+':00';
	}
	return hora;
}

function diff_horas(hora_i, hora_f) {
	h_i = pasar_hora_a_horas(hora_i);
	h_f = pasar_hora_a_horas(hora_f);	
	horas = 0;
	if(h_i <= h_f) {
		horas = h_f - h_i;
	}else{
		horas = 24 - h_i + h_f;
	}	
	return horas;
}

function pasar_hora_a_horas(hora) {
	hora = hora.split(':');
	horas = parseInt(hora[0]);
	horas += Math.abs((parseInt(hora[1])/60));
	
	return horas;
}

function dia_semana(fecha) {
	var dia = new Date(fecha);	
	return nom_dia_semana[dia.getDay()];
}

function smkGMap(lat, long, zoom, ele) {
	var mapOptions = {
    	zoom: zoom,
		center: new google.maps.LatLng(lat, long),
		panControl: true,
		zoomControl: true,
		scaleControl: true,
		streetViewControl: false
	}	
	var mapa = new google.maps.Map(document.getElementById(ele), mapOptions);  	
	return mapa;
}

function smkGMapMarca_add(mapa, posicion, titulo, editable, ir) {
	//var imagen = 'images/beachflag.png';
	var marker = new google.maps.Marker({
		position: posicion,
		map: mapa,
		//icon: imagen,
		draggable:editable,
    	title: titulo
	});
	
	if(true == ir) {
		smkGMapPos_centrar(mapa, posicion);
	}
	
	/*google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(mapa, marker);
	});*/
	
	//marker.setMap(null);
	
	return marker;
}

function smkGMapPos_centrar(mapa, posicion) {
	mapa.setCenter(posicion);
}

function smkGMapPos_crear(lat, long) {
	var pos = new google.maps.LatLng(lat, long);
	return pos;
}

function genHorasDia(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;
}

function genHorasDiaInc(hora_inicio, espacio_horas) {
	hora_inicio = moment(hora_inicio, 'YYYY-MM-DD hh:mmA');	
	horas = [];	
	if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
		horas.push('00:' + hora_inicio.format('mmA'));
	} else {
		horas.push(hora_inicio.format('hh:mmA'));
	}	
	for(i=0;i<(24-espacio_horas);i+=espacio_horas) {
		hora_inicio.add(espacio_horas, 'hours');
		
		if('12' == hora_inicio.format('hh') && 'AM' == hora_inicio.format('A')) {
			horas.push('00:' + hora_inicio.format('mmA'));
		} else {
			horas.push(hora_inicio.format('hh:mmA'));
		}
	}	
	return horas;
}

function formatoHora12(time) {
	return time.match(/^(0?[0-9]|1[012])(:[0-5]\d)[APap][mM]$/);
}

function oncargandoxajax() {
	$('.m3c-cargando').css('display', 'block');
}

function offcargandoxajax() {
	$('.m3c-cargando').css('display', 'none');
}

function dias_entre_fechas(fecha_i, fecha_f) {//01.05.15
	fecha_i = Date.UTC(fecha_i.getFullYear(), fecha_i.getMonth(), fecha_i.getDate(), fecha_i.getHours(), fecha_i.getMinutes());
	fecha_f = Date.UTC(fecha_f.getFullYear(), fecha_f.getMonth(), fecha_f.getDate(), fecha_f.getHours(), fecha_f.getMinutes());
	var ms = Math.abs(fecha_i - fecha_f);
	return Math.floor(ms/1000/60/60/24);
}

function getValRadio(selector) {
	return ($("."+ selector +":checked").length > 0) ? $("."+ selector +":checked").val() : '';
}

function setValId(id, valor) {
	$('#' + id).val(valor);
}

function textToFloat(text) {
	return ('' == text) ? 0 : parseFloat(text.replace(',', ''));
}

function isNumber(n) {
    n = n.replace(/\./g, '').replace(',', '.');
    return !isNaN(parseFloat(n)) && isFinite(n);
}

var modal_id = 0;
var __stopmedios=function(cnt){
	if(cnt.length){
		var audio=$(cnt.find('audio'));
	    	if(audio.length) audio.trigger('pause');
	    var video=$(cnt.find('video'));
	    	if(video.length) video.trigger('pause');
	}
	return true;
}
var __cerrarmodal=function(_md,sborrar){
	if(_md.length){
    	__stopmedios(_md)
		_md.modal('hide');
		if(sborrar)_md.on('hidden.bs.modal', function(){_md.remove();});
		//$('body').removeAttr('style');
	}		
}
var __sysmodal=function(infodata){
	var dt=infodata||{};
	var sheader=dt.header||false;
	var sfooter=dt.footer||false;
	var sborrar=dt.borrar||true;
	var tam=dt.tam||'lg';
	var anchomodal=dt.modalancho||'';
	var clase=dt.clase||'';
	var url=dt.url||'';
	var html=dt.html||'';
	var method=dt.method||'POST';
	var fromdata=dt.fromdata||{};
	var backdrop=dt.backdrop||'static';
	var keyboard=dt.keyboard||true;
	var titulo=dt.titulo||'';	
	var _md = $('#modalclone').clone(true,true);
	if(!sheader) _md.find('.modal-header').hide();
	if(!sfooter) _md.find('.modal-footer').hide();
	_md.attr('id', 'modal-' + modal_id);
    _md.addClass(clase);
    _md.children('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-'+tam)
    if(anchomodal!='')_md.children('.modal-dialog').css('width',anchomodal);    
    _md.modal({backdrop: backdrop, keyboard: keyboard});    
    $('body').on('click', '#modal-' + modal_id + ' .close' , function (ev){ __cerrarmodal(_md , sborrar)});
    $('body').on('click', '#modal-' + modal_id + ' .cerrarmodal' , function (ev){ __cerrarmodal(_md , sborrar)});
    ++modal_id;
    _md.find('#modaltitle').html(titulo);
    if(url!=''){
    	__sysajax({
    		url:url,
    		method:method,
    		fromdata:fromdata,
    		processData: false,
    		type:'html',
    		callback:function(rs){    			
	            _md.find('#modalcontent').html(rs);
    		}
    	});
    }else if(html!='') _md.find('#modalcontent').html(html);
    return _md;
}

$(document).ready(function(){ 
	$('.changeidioma').click(function(){
        var idi=$(this).attr('idioma');
        localStorage.setItem("sysidioma", idi);
        xajax__('', 'idioma', 'cambiaridioma', idi);        
    });
    $('.changerol').click(function(){
        xajax__('', 'Sesion', 'cambiarRol');
    });
});

function _sysisFile(url){
  	url=url.substr(1 + url.lastIndexOf("/"));
  	index=url.indexOf('?');
  	if(index>-1) url=url.substr(0,index);
  	if(url==''||url=='undefined'||url==undefined) return false;
  	indexpunto=url.lastIndexOf(".");
  	if(indexpunto==-1) return false;
  	return true;  
}

function _sysfileExists(url){
	if(!_sysisFile(url)) return false;
    var http = new XMLHttpRequest();
    http.open('HEAD', url, true);
    http.send();
    if(http.status!=404) return true;
    return false;
}

function _isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
var animacssin=["bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","flip","flipInX","flipInY","lightSpeedIn","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","slideInUp","slideInDown","slideInLeft","slideInRight","zoomIn","zoomInDown","zoomInLeft","zoomInRight","zoomInUp","jackInTheBox","rollIn"]; //,"hinge"
var animacssout=["bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flipOutX","flipOutY","lightSpeedOut","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideOutUp","slideOutDown","slideOutLeft","slideOutRight","zoomOut","zoomOutDown","zoomOutLeft","zoomOutRight","zoomOutUp","rollOut"];
var animaccsenf=["bounce","flash","pulse","rubberBand","shake","swing","tada","wobble","jello"];

$.fn.extend({
    animateCss: function(dt){   
        var dtani=dt||{};
        var aninombre=dt.animationName||'';
        var de=dt.de||'in';
        var callback=dt.callback||'';
        var hide=dt.hide||false;
    	if(aninombre==''){
    		if(de=='out') { 
    			var l=animacssout.length-1; 
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssout[index];
    		}else if(de=='enf'){
    			var l=animacssenf.length-1;
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssenf[index];
    		}else{    			
    			var l=animacssin.length-1;
    			var index=Math.floor(Math.random() * l);
    			aninombre=animacssin[index];
    		}
    	}
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + aninombre).one(animationEnd, function(){ 
            $(this).removeClass('animated ' + aninombre);            
            if(_isFunction(callback))callback();            
        });
        return this;
    }
});

function _isFunction(object){ if (typeof object != 'function') return false; else return true;}

var __formdata=function(frmele){
	 var tmpfrm='';
	if(frmele!='') var tmpfrm=document.getElementById(frmele);
	else tmpfrm='';
	var dt=new FormData(tmpfrm);
	dt.append('idioma',_sysIdioma_);
	return dt;
}

var __formDataToJSON=function (frmele){
	var fd=__formdata(frmele);
	convertedJSON = {};
    fd.forEach(function(value, key){ convertedJSON[key] = value; });
    convertedJSON.idioma=_sysIdioma_;    
	return convertedJSON;
}

var __sysajax=function(infodata){
	try{
		var opt={donde:false,url:false,type:'json',mostrarcargando:false,method:'POST',fromdata:{},msjatencion:''};
		$.extend(opt,infodata);		
		var datasend=dts=opt.fromdata;	

		var showmsjok=opt.showmsjok==false?false:true;		
		var callback=opt.callback||false;
		var callbackerror=opt.callbackerror||false;

		var typesend=(dts.constructor.name||'object').toString().toLowerCase();
		var _prd=(typesend=="formdata")?false:true;
		var $progressavance=$(opt.iduploadtmp).length?$(opt.iduploadtmp+' #progressavance '):'';		
		var $divuploadp=$progressavance.length?$progressavance.find('span#cantidadupload'):'';
		var $iduploadtmp=$(opt.iduploadtmp)||'';
		var hayprogress=$progressavance.length?true:false;
		var _donde=opt.donde||false;

		//console.log(mostrarcargando,_donde);
		if(!opt.url) return;
		$.ajax({
		  url: opt.url,
		  type: opt.method,
		  dataType:opt.type,
		  data:  datasend,
		  processData: _prd,
		  contentType: _prd==true?'application/x-www-form-urlencoded; charset=UTF-8':false, 
		  cache: false,
		  xhr:function(){
		  	var xhr = new window.XMLHttpRequest();
		  	try{
	         	xhr.upload.addEventListener("progress", function(evt){ //Upload progress
		          if (evt.lengthComputable && hayprogress){
		            var percentComplete = Math.floor((evt.loaded*100) / evt.total);	           
		            	$progressavance.width(percentComplete+'%');
		            	$divuploadp.text(percentComplete+'%');	        	
		            //console.log(percentComplete);
		          }
		        }, false);
		        
		        xhr.addEventListener("progress", function(evt){//Download progress
		          if (evt.lengthComputable) {
		            var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
		            //console.log(percentComplete);
		          }
		        }, false);
            }catch(err){
            	console.log(err);
        	}
	        return xhr;
	      },		  		  
		  beforeSend: function(XMLHttpRequest){ 		  	
		  	if((_donde!=false&& opt.type!='json')&&opt.mostrarcargando==true){
		  		$cl=$('#cargando_clone').clone();
		  		$cl.removeAttr('style');
		  		_donde.html($cl.html()); 
		  	}
		  },
		  success: function(data)
		  { if(opt.type=='json'){
		        if(data.code=='Error'){
		          mostrar_notificacion(opt.msjatencion,data.msj,'warning');
		          if(_isFunction(callbackerror))callbackerror(data);
		        }else{		        	
		          if(showmsjok) mostrar_notificacion(opt.msjatencion,data.msj,'success');
		          if(_isFunction(callback))callback(data);
		        }
		    }else{
		    	if(_isFunction(callback))callback(data);
		    }
		    if($iduploadtmp.length) $iduploadtmp.remove();
		  },
		  error: function(e){ console.log(e); if($iduploadtmp.length) $iduploadtmp.remove();},
		  complete: function(xhr){ if($iduploadtmp.length) $iduploadtmp.remove(); }
		});
	}catch(err) {
    	console.log(err.message);
    	return false;
	}
}