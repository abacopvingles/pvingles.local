<?php
/**
 * @autor		Jos� Ricardo Burga Mori
 * @fecha		08/09/2009
 * @copyright	Copyright (C) 2009. Todos los derechos reservados.
 */

class JrExcepcion extends Exception
{
	public function __construct($msj)
	{
		echo '<h3>'.$msj.'</h3>';
		exit();
	}
}
