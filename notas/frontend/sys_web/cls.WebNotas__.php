<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		27-11-2017 
 * @copyright	Copyright (C) 27-11-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
#JrCargador::clase('sys_negocio::NegNotas', RUTA_BASE, 'sys_negocio');
class WebNotas extends JrWeb
{
	#private $oNegNotas;
		
	public function __construct()
	{
		parent::__construct();		
		#$this->oNegNotas = new NegNotas;
		$this->usuarioAct = NegSesion::getUsuario();
				
	}

	public function defecto(){
		return $this->listar();
	}

	public function listar()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Scores', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('listar', '/js/notas/');

			#...
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) /*, 'link'=> $this->urlBtnRetroceder*/ ],
            ];

			$this->documento->plantilla = 'notas/general';
			$this->documento->setTitulo(JrTexto::_('List').' - '.JrTexto::_('Scores'), true);
			$this->esquema = 'notas/listar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Scores', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('editar', '/js/notas/');

			#...
			
			$this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('Scores')) , 'link'=> '/' ],
                [ 'texto'=> ucfirst(JrTexto::_('Edit'))  ],
            ];

			$this->documento->plantilla = 'notas/general';
			$this->documento->setTitulo(JrTexto::_('Edit').' - '.JrTexto::_('Scores'), true);
			$this->esquema = 'notas/editar';
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Scores', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Scores').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	/*public function editar()
	{
		try {
			global $aplicacion;
			
			#if(!NegSesion::tiene_acceso('Scores', 'edit')) {
			#	throw new Exception(JrTexto::_('Restricted access').'!!');
			#}
			$this->frmaccion='Editar';
			$this->oNegNotas->idnota_detalle = @$_GET['id'];
			$this->datos = $this->oNegNotas->dataNotas;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Scores').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}*/

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'nota_detalle-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Scores', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			if(isset($_REQUEST["idnota_detalle"])&&@$_REQUEST["idnota_detalle"]!='')$filtros["idnota_detalle"]=$_REQUEST["idnota_detalle"];
			if(isset($_REQUEST["idcurso"])&&@$_REQUEST["idcurso"]!='')$filtros["idcurso"]=$_REQUEST["idcurso"];
			if(isset($_REQUEST["idmatricula"])&&@$_REQUEST["idmatricula"]!='')$filtros["idmatricula"]=$_REQUEST["idmatricula"];
			if(isset($_REQUEST["idalumno"])&&@$_REQUEST["idalumno"]!='')$filtros["idalumno"]=$_REQUEST["idalumno"];
			if(isset($_REQUEST["iddocente"])&&@$_REQUEST["iddocente"]!='')$filtros["iddocente"]=$_REQUEST["iddocente"];
			if(isset($_REQUEST["idcursodetalle"])&&@$_REQUEST["idcursodetalle"]!='')$filtros["idcursodetalle"]=$_REQUEST["idcursodetalle"];
			if(isset($_REQUEST["nota"])&&@$_REQUEST["nota"]!='')$filtros["nota"]=$_REQUEST["nota"];
			if(isset($_REQUEST["tipo"])&&@$_REQUEST["tipo"]!='')$filtros["tipo"]=$_REQUEST["tipo"];
			if(isset($_REQUEST["observacion"])&&@$_REQUEST["observacion"]!='')$filtros["observacion"]=$_REQUEST["observacion"];
			if(isset($_REQUEST["fecharegistro"])&&@$_REQUEST["fecharegistro"]!='')$filtros["fecharegistro"]=$_REQUEST["fecharegistro"];
			if(isset($_REQUEST["estado"])&&@$_REQUEST["estado"]!='')$filtros["estado"]=$_REQUEST["estado"];
						
			$this->datos=$this->oNegNotas->buscar($filtros);
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarNotas(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkIdnota_detalle)) {
				$this->oNegNotas->idnota_detalle = $frm['pkIdnota_detalle'];
				$accion='_edit';
			}

        	global $aplicacion;         
        	$usuarioAct = NegSesion::getUsuario();
        	@extract($_POST);
        	
			$this->oNegNotas->idcurso=@$txtIdcurso;
			$this->oNegNotas->idmatricula=@$txtIdmatricula;
			$this->oNegNotas->idalumno=@$txtIdalumno;
			$this->oNegNotas->iddocente=@$txtIddocente;
			$this->oNegNotas->idcursodetalle=@$txtIdcursodetalle;
			$this->oNegNotas->nota=@$txtNota;
			$this->oNegNotas->tipo=@$txtTipo;
			$this->oNegNotas->observacion=@$txtObservacion;
			$this->oNegNotas->fecharegistro=@$txtFecharegistro;
			$this->oNegNotas->estado=@$txtEstado;
					
            if($accion=='_add') {
            	$res=$this->oNegNotas->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Scores')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegNotas->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Scores')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveNotas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnota_detalle'])) {
					$this->oNegNotas->idnota_detalle = $frm['pkIdnota_detalle'];
				}
				
				$this->oNegNotas->idcurso=@$frm["txtIdcurso"];
				$this->oNegNotas->idmatricula=@$frm["txtIdmatricula"];
				$this->oNegNotas->idalumno=@$frm["txtIdalumno"];
				$this->oNegNotas->iddocente=@$frm["txtIddocente"];
				$this->oNegNotas->idcursodetalle=@$frm["txtIdcursodetalle"];
				$this->oNegNotas->nota=@$frm["txtNota"];
				$this->oNegNotas->tipo=@$frm["txtTipo"];
				$this->oNegNotas->observacion=@$frm["txtObservacion"];
				$this->oNegNotas->fecharegistro=@$frm["txtFecharegistro"];
				$this->oNegNotas->estado=@$frm["txtEstado"];
				
			   if(@$frm["accion"]=="Nuevo"){
								    $res=$this->oNegNotas->agregar();
				}else{
								    $res=$this->oNegNotas->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNotas->idnota_detalle);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNotas(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas->__set('idnota_detalle', $pk);
				$this->datos = $this->oNegNotas->dataNotas;
				$res=$this->oNegNotas->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNotas->__set('idnota_detalle', $pk);
				$res=$this->oNegNotas->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNotas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}