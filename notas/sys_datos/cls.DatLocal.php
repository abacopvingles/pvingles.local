<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-01-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatLocal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM local";
			
			$cond = array();		
			
			if(!empty($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(!empty($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(!empty($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT l.*, u.descripcion as ugel
			FROM local l inner join ugel u on u.idugel=l.idugel";
			
			$cond = array();		
					
			
			if(!empty($filtros["idlocal"])) {
					$cond[] = "idlocal = " . $this->oBD->escapar($filtros["idlocal"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["direccion"])) {
					$cond[] = "direccion = " . $this->oBD->escapar($filtros["direccion"]);
			}
			if(!empty($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "l.tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["vacantes"])) {
					$cond[] = "vacantes = " . $this->oBD->escapar($filtros["vacantes"]);
			}
			if(!empty($filtros["idugel"])) {
					$cond[] = "l.idugel like " . $this->oBD->escapar($filtros["idugel"]."%");
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//var_dump($sql);
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM local  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel)
	{
		try {
			
			$this->iniciarTransaccion('dat_local_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idlocal) FROM local");
			++$id;
			
			$estados = array('idlocal' => $id
							
							,'nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel							
							);
			
			$this->oBD->insert('local', $estados);			
			$this->terminarTransaccion('dat_local_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_local_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$direccion,$id_ubigeo,$tipo,$vacantes,$idugel)
	{
		try {
			$this->iniciarTransaccion('dat_local_update');
			$estados = array('nombre'=>$nombre
							,'direccion'=>$direccion
							,'id_ubigeo'=>$id_ubigeo
							,'tipo'=>$tipo
							,'vacantes'=>$vacantes
							,'idugel'=>$idugel								
							);
			
			$this->oBD->update('local ', $estados, array('idlocal' => $id));
		    $this->terminarTransaccion('dat_local_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM local  "
					. " WHERE idlocal = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('local', array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('local', array($propiedad => $valor), array('idlocal' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Local").": " . $e->getMessage());
		}
	}
   
		
}