-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2018 a las 01:12:38
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvingles.local`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `idnota` int(11) NOT NULL,
  `idhoja` int(11) NOT NULL COMMENT 'idpestania de tipo=''H''',
  `idcolumna` int(11) DEFAULT NULL COMMENT 'idpestania de tipo=''C'' . Cuando tipo_nota=''P'', entonces idcolumna=NULL',
  `idalumno` int(11) NOT NULL,
  `idarchivo` int(11) NOT NULL,
  `nota_num` float(5,2) DEFAULT NULL,
  `nota_txt` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `observacion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`idnota`, `idhoja`, `idcolumna`, `idalumno`, `idarchivo`, `nota_num`, `nota_txt`, `observacion`, `fechareg`) VALUES
(1, 1, 3, 2, 1, NULL, 'En proceso', NULL, '2018-02-15 16:32:31'),
(2, 1, 2, 2, 1, 15.00, NULL, NULL, '2018-02-15 16:33:35'),
(3, 1, 3, 14, 1, NULL, 'Logrado', NULL, '2018-02-16 15:31:31'),
(11, 1, 2, 10, 1, 19.00, NULL, NULL, '2018-02-16 23:58:47'),
(10, 1, 3, 11, 1, 18.00, 'Logrado', NULL, '2018-02-16 23:58:43'),
(9, 1, 3, 6, 1, 12.00, 'En proceso', NULL, '2018-02-16 23:57:11'),
(8, 1, 3, 1, 1, NULL, 'En proceso', NULL, '2018-02-16 23:53:41'),
(7, 1, 3, 16, 1, NULL, 'Logrado', NULL, '2018-02-16 23:53:33'),
(6, 1, 2, 14, 1, 16.00, NULL, NULL, '2018-02-16 23:53:23'),
(5, 1, 3, 10, 1, NULL, 'En proceso', NULL, '2018-02-16 23:53:15'),
(4, 1, 3, 8, 1, NULL, 'En inicio', NULL, '2018-02-16 23:35:57'),
(12, 1, 2, 8, 1, 13.00, NULL, NULL, '2018-02-16 23:58:50'),
(13, 1, 2, 16, 1, 10.00, NULL, NULL, '2018-02-16 23:58:53'),
(14, 1, 3, 5, 1, 10.00, 'En proceso', NULL, '2018-02-16 23:58:59'),
(15, 1, 3, 9, 1, 19.00, 'Logrado', NULL, '2018-02-16 23:59:42'),
(16, 1, 4, 2, 1, 15.00, NULL, NULL, '2018-02-17 00:18:18'),
(17, 1, 4, 8, 1, 18.00, NULL, NULL, '2018-02-17 00:18:49'),
(18, 1, 4, 14, 1, 20.00, NULL, NULL, '2018-02-17 00:18:51'),
(19, 1, 4, 10, 1, 15.00, NULL, NULL, '2018-02-17 00:19:51'),
(20, 1, 4, 16, 1, 16.00, NULL, NULL, '2018-02-17 00:34:53'),
(21, 1, 4, 1, 1, 13.00, NULL, NULL, '2018-02-17 00:34:57'),
(22, 1, 2, 1, 1, 11.00, NULL, NULL, '2018-02-17 00:34:59'),
(23, 1, 3, 15, 1, 16.00, 'Logrado', NULL, '2018-02-17 15:30:53'),
(24, 5, 6, 18, 2, 19.00, NULL, NULL, '2018-06-01 15:46:48'),
(25, 5, 7, 18, 2, 10.00, NULL, NULL, '2018-06-01 16:52:08'),
(26, 5, 7, 17, 2, 15.00, NULL, NULL, '2018-06-01 16:52:13'),
(27, 5, 6, 17, 2, 16.00, NULL, NULL, '2018-06-01 16:52:26'),
(28, 5, 8, 18, 2, NULL, 'esc 2', NULL, '2018-06-01 17:00:29'),
(29, 5, 8, 17, 2, NULL, 'esc 3', NULL, '2018-06-01 17:00:32'),
(30, 5, 10, 18, 2, 13.00, 'Escala II', NULL, '2018-06-01 22:25:14'),
(31, 5, 10, 17, 2, 15.00, 'Escala III', NULL, '2018-06-01 22:25:18'),
(32, 5, 9, 18, 2, NULL, 'anotaciones de alumno 1 larga para comprobar el espacio  que ocupa dentro de la celda', NULL, '2018-06-01 23:10:55'),
(33, 5, 9, 19, 2, NULL, 'Anotación para alumno Abel', NULL, '2018-06-02 00:45:23'),
(34, 5, 11, 20, 2, NULL, 'Eder eder edr er e', NULL, '2018-06-02 15:58:09'),
(43, 18, 22, 17, 2, 14.00, NULL, NULL, '2018-06-14 21:27:07'),
(36, 5, 12, 17, 2, 15.00, NULL, NULL, '2018-06-02 16:09:36'),
(42, 18, 22, 18, 2, 12.00, NULL, NULL, '2018-06-14 21:27:02'),
(38, 14, 15, 17, 2, 5.00, NULL, NULL, '2018-06-04 20:57:08'),
(39, 14, 15, 19, 2, 6.00, NULL, NULL, '2018-06-04 20:57:10'),
(40, 14, 15, 20, 2, 10.00, NULL, NULL, '2018-06-04 20:57:12'),
(41, 14, 15, 18, 2, 16.00, NULL, NULL, '2018-06-04 20:57:17'),
(44, 18, 22, 19, 2, 10.00, NULL, NULL, '2018-06-14 21:27:10'),
(45, 18, 22, 20, 2, 15.00, NULL, NULL, '2018-06-14 21:27:14'),
(46, 18, 24, 18, 2, 13.00, NULL, NULL, '2018-06-14 21:27:49'),
(47, 18, 24, 17, 2, 13.00, NULL, NULL, '2018-06-14 21:28:44'),
(48, 18, 24, 19, 2, 11.00, NULL, NULL, '2018-06-14 21:28:47'),
(49, 18, 24, 20, 2, 16.00, NULL, NULL, '2018-06-14 21:28:51'),
(50, 18, 25, 18, 2, NULL, 'e2', NULL, '2018-06-14 21:47:11'),
(51, 18, 25, 17, 2, NULL, 'e3', NULL, '2018-06-14 21:47:13'),
(52, 18, 25, 19, 2, NULL, 'e1', NULL, '2018-06-14 21:47:14'),
(53, 18, 25, 20, 2, NULL, 'e1', NULL, '2018-06-14 21:47:15'),
(54, 18, 26, 18, 2, 13.00, 'PROGRESS', NULL, '2018-06-14 21:48:54'),
(55, 18, 26, 17, 2, 19.00, 'DONE', NULL, '2018-06-14 21:49:01'),
(56, 18, 26, 19, 2, 10.00, 'PROGRESS', NULL, '2018-06-14 21:49:07'),
(57, 18, 26, 20, 2, 8.00, 'START', NULL, '2018-06-14 21:49:15'),
(58, 18, 27, 18, 2, NULL, 'qwerqwerqwrqwr', NULL, '2018-06-14 22:02:48'),
(59, 18, 27, 17, 2, NULL, 'sfdsadfsff sd fsdfs ', NULL, '2018-06-14 22:02:51'),
(60, 18, 27, 19, 2, NULL, 'dfasdfsadzxvcvtqwetwet', NULL, '2018-06-14 22:02:55'),
(61, 18, 27, 20, 2, NULL, 'asdfwe weew cvfsdfsdf  sdfsd fas werfwe werew rw e rwe we rwe wee ewecxzv sdasd fsdf', NULL, '2018-06-14 22:03:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_alumno`
--

CREATE TABLE `notas_alumno` (
  `idalumno` int(11) NOT NULL,
  `nombres` varchar(128) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidos` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL,
  `identificador` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'ID de alumno si es que es importado de otra plataforma',
  `idarchivo` int(11) NOT NULL,
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `notas_alumno`
--

INSERT INTO `notas_alumno` (`idalumno`, `nombres`, `apellidos`, `identificador`, `idarchivo`, `fechareg`) VALUES
(20, ' Eder', 'Figueroa Piscoya', '72042592', 2, '2018-06-01 15:39:19'),
(19, ' Abel', 'Chingo Tello', '43831104', 2, '2018-06-01 15:39:19'),
(18, ' Prueba', 'Alumno 1 PVIngles', '55555555', 2, '2018-06-01 15:39:19'),
(17, ' Manuel', 'Castro Alzamora', '9678965', 2, '2018-06-01 15:39:19'),
(16, 'Ryan', 'Cruiff', '88991122', 1, '2018-02-09 14:57:17'),
(15, 'Erika', 'Heredia', '77889911', 1, '2018-02-09 14:57:17'),
(14, 'Fernanda', 'Bracamonte', '66778899', 1, '2018-02-09 14:57:17'),
(13, 'Esteban', 'Torres', '55667788', 1, '2018-02-09 14:57:17'),
(12, 'Raul', 'Prada', '44556677', 1, '2018-02-09 14:57:17'),
(11, 'Lucía', 'Gonzales', '33445566', 1, '2018-02-09 14:57:17'),
(10, 'Rosa', 'Castillo', '22334455', 1, '2018-02-09 14:57:17'),
(9, 'Jose', 'Pérez', '11223344', 1, '2018-02-09 14:57:17'),
(8, 'Clint', 'Barton Renner', '51984623', 1, '2018-02-09 14:57:17'),
(7, 'Natasha', 'Romanoff Johanson', '34185296', 1, '2018-02-09 14:57:17'),
(6, 'Nick', 'Fury Jackson', '75428691', 1, '2018-02-09 14:57:17'),
(5, 'Thor', 'Odinson Hermsworth', '11111111', 1, '2018-02-09 14:57:17'),
(4, 'Steve', 'Rogers Evans', '10000082', 1, '2018-02-09 14:57:17'),
(3, 'Tony', 'Stark Downey', '77777777', 1, '2018-02-09 14:57:17'),
(2, 'Bruce', 'Banner Ruffalo', '87654321', 1, '2018-02-09 14:57:17'),
(1, 'Francis', 'Favreau Perez', '72345678', 1, '2018-02-09 14:57:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_archivo`
--

CREATE TABLE `notas_archivo` (
  `idarchivo` int(11) NOT NULL,
  `nombre` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` text COLLATE utf8_spanish_ci,
  `identificador` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'ID del curso, si es que viene importado',
  `iddocente` int(11) NOT NULL COMMENT 'idusuario',
  `fechareg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='puede representar al Curso, Institución o archivo que el docente pueda identificar.';

--
-- Volcado de datos para la tabla `notas_archivo`
--

INSERT INTO `notas_archivo` (`idarchivo`, `nombre`, `imagen`, `identificador`, `iddocente`, `fechareg`) VALUES
(2, 'A1 - Institucion educativa 1154', 'https://192.168.0.57/pvingles.local/static/media/image/N1-U1-A1-20180116045437.png', '1', 22222222, '2018-06-01 15:39:19'),
(1, 'Archivo - Excel', '__xRUTABASEx__/static/img/notas_archivos/20180209095714_cS-10.jpg', '', 22222222, '2018-02-09 14:57:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas_pestania`
--

CREATE TABLE `notas_pestania` (
  `idpestania` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `abreviatura` varchar(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo_pestania` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '[C]olumna ; [H]oja',
  `tipo_info` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de inform. que guardará la columna: [N]úmero, [R]ango ; [E]scala ; [T]exto ; [A]sistencia',
  `info_valor` text COLLATE utf8_spanish_ci,
  `color` varchar(7) COLLATE utf8_spanish_ci NOT NULL DEFAULT '#ffffff',
  `orden` int(11) NOT NULL,
  `idarchivo` int(11) NOT NULL,
  `idpestania_padre` int(11) DEFAULT NULL COMMENT 'idpestania, cuando una columna pertenece a una Hoja'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `notas_pestania`
--

INSERT INTO `notas_pestania` (`idpestania`, `nombre`, `abreviatura`, `tipo_pestania`, `tipo_info`, `info_valor`, `color`, `orden`, `idarchivo`, `idpestania_padre`) VALUES
(1, 'Hoja 1', NULL, 'H', NULL, NULL, '#ffffff', 1, 1, NULL),
(2, 'Rango de 0-20', 'RNG', 'C', 'R', '{"min":0,"max":20}', '#ffffff', 2, 1, 1),
(3, 'Escala I-II-III ', 'SCL', 'C', 'E', '{"tiene_intervalos":true,"escalas":[{"nombre":"En inicio","min":0,"max":10},{"nombre":"En proceso","min":10,"max":15},{"nombre":"Logrado","min":15,"max":20}]}', '#ffffff', 6, 1, 1),
(4, 'Numerico', 'NUM', 'C', 'N', NULL, '#ffffff', 9, 1, 1),
(5, 'Sheet 1', NULL, 'H', NULL, NULL, '#ffffff', 2, 2, NULL),
(6, 'Nota 01', 'NUM', 'C', 'N', NULL, '#abcdef', 1, 2, 5),
(7, 'Nota 02', 'RANGE', 'C', 'R', '{"min":0,"max":18}', '#c4ff6a', 2, 2, 5),
(8, 'Nota 03', 'SCALe', 'C', 'E', '{"tiene_intervalos":false,"escalas":[{"nombre":"esc 1"},{"nombre":"esc 2"},{"nombre":"esc 3"}]}', '#ffc679', 3, 2, 5),
(9, 'aNotacion 04', 'TEXT', 'C', 'T', NULL, '#fc838c', 4, 2, 5),
(10, 'Nota 05', 'SCAL2', 'C', 'E', '{"tiene_intervalos":true,"escalas":[{"nombre":"Escala I","min":0,"max":10},{"nombre":"Escala II","min":10,"max":15},{"nombre":"Escala III","min":15,"max":20}]}', '#f8bf14', 5, 2, 5),
(11, 'aNotacion 06', 'TXT2', 'C', 'T', NULL, '#c61a1a', 6, 2, 5),
(12, 'Nota 07', 'NUM2', 'C', 'N', NULL, '#8973cc', 7, 2, 5),
(16, 'hoja 03', 'H3', 'H', NULL, NULL, '#fdbb57', 4, 2, NULL),
(14, 'Hoja 02', 'H2', 'H', NULL, NULL, '#3149f7', 3, 2, NULL),
(15, 'nota-hoja02', 'N1-H2', 'C', 'N', NULL, '#9cfaa4', 1, 2, 14),
(24, 'col2-hoj5', 'c2-h5', 'C', 'N', NULL, '#a7cfaa', 2, 2, 18),
(18, 'Hojita 05', 'ha5', 'H', NULL, NULL, '#dfe92e', 1, 2, NULL),
(20, 'col1-hoja6', 'C1-H6', 'C', 'N', NULL, '#ffffff', 1, 2, 19),
(21, 'col2-hoj6', 'c2-h6', 'C', 'R', '{"min":0,"max":18}', '#8eeacc', 2, 2, 19),
(22, 'colum1-hoja5', 'c1-h5', 'C', 'R', '{"min":0,"max":15}', '#ffffff', 1, 2, 18),
(23, 'Promedio 01', NULL, 'C', 'R', '{"min":0,"max":18}', '#69c567', 1, 2, 16),
(25, 'col3-hoj5', 'c3-h5', 'C', 'E', '{"tiene_intervalos":false,"escalas":[{"nombre":"e1"},{"nombre":"e2"},{"nombre":"e3"}]}', '#b7334a', 3, 2, 18),
(26, 'col4-hoj5', 'c4-h5', 'C', 'E', '{"tiene_intervalos":true,"escalas":[{"nombre":"START","min":0,"max":10},{"nombre":"PROGRESS","min":10,"max":15},{"nombre":"DONE","min":15,"max":20}]}', '#fa9c5f', 4, 2, 18),
(27, 'aNota5-hoj5', 'n5-h5', 'C', 'T', NULL, '#dfdfdf', 5, 2, 18),
(28, 'Ponderado', 'AVGh5', 'C', 'P', '[{"idpestania":"22","peso":"2"},{"idpestania":"24","peso":"1"},{"idpestania":"26","peso":"3"}]', '#0c7dfc', 6, 2, 18);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`idnota`);

--
-- Indices de la tabla `notas_alumno`
--
ALTER TABLE `notas_alumno`
  ADD PRIMARY KEY (`idalumno`);

--
-- Indices de la tabla `notas_archivo`
--
ALTER TABLE `notas_archivo`
  ADD PRIMARY KEY (`idarchivo`);

--
-- Indices de la tabla `notas_pestania`
--
ALTER TABLE `notas_pestania`
  ADD PRIMARY KEY (`idpestania`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
