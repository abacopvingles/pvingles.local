<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		30-01-2018
 * @copyright	Copyright (C) 30-01-2018. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatNotas_archivo', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegNotas_archivo 
{
	protected $idarchivo;
	protected $nombre;
	protected $imagen;
	protected $identificador;
	protected $iddocente;
	protected $fechareg;
	
	protected $dataNotas_archivo;
	protected $oDatNotas_archivo;	

	public function __construct()
	{
		$this->oDatNotas_archivo = new DatNotas_archivo;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatNotas_archivo->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatNotas_archivo->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatNotas_archivo->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatNotas_archivo->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatNotas_archivo->get($this->idarchivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_archivo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->oDatNotas_archivo->iniciarTransaccion('neg_i_Notas_archivo');
			$this->idarchivo = $this->oDatNotas_archivo->insertar($this->nombre,$this->imagen,$this->identificador,$this->iddocente,$this->fechareg);
			$this->oDatNotas_archivo->terminarTransaccion('neg_i_Notas_archivo');	
			return $this->idarchivo;
		} catch(Exception $e) {	
		    $this->oDatNotas_archivo->cancelarTransaccion('neg_i_Notas_archivo');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('notas_archivo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatNotas_archivo->actualizar($this->idarchivo,$this->nombre,$this->imagen,$this->identificador,$this->iddocente,$this->fechareg);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_archivo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_archivo->eliminar($this->idarchivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function eliminar_cascada()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Notas_archivo', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatNotas_archivo->eliminar_cascada($this->idarchivo);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setIdarchivo($pk){
		try {
			$this->dataNotas_archivo = $this->oDatNotas_archivo->get($pk);
			if(empty($this->dataNotas_archivo)) {
				throw new Exception(JrTexto::_("Notas_archivo").' '.JrTexto::_("not registered"));
			}
			$this->idarchivo = $this->dataNotas_archivo["idarchivo"];
			$this->nombre = $this->dataNotas_archivo["nombre"];
			$this->imagen = $this->dataNotas_archivo["imagen"];
			$this->identificador = $this->dataNotas_archivo["identificador"];
			$this->iddocente = $this->dataNotas_archivo["iddocente"];
			$this->fechareg = $this->dataNotas_archivo["fechareg"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('notas_archivo', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataNotas_archivo = $this->oDatNotas_archivo->get($pk);
			if(empty($this->dataNotas_archivo)) {
				throw new Exception(JrTexto::_("Notas_archivo").' '.JrTexto::_("not registered"));
			}

			return $this->oDatNotas_archivo->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
		
}