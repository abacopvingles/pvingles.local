-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-02-2019 a las 23:32:35
-- Versión del servidor: 5.7.11
-- Versión de PHP: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pvinlges.local-17-01-2019`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE `test` (
  `idtest` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `puntaje` varchar(500) NOT NULL,
  `mostrar` tinyint(4) NOT NULL,
  `imagen` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`idtest`, `titulo`, `puntaje`, `mostrar`, `imagen`) VALUES
(1, 'aaaaaaaa', '[{\"code\":\"1\",\"nombre\":\"a\",\"description\":\"asdasd\"},{\"code\":\"2\",\"nombre\":\"b\",\"description\":\"adasasd\"},{\"code\":\"3\",\"nombre\":\"c\",\"description\":\"asdasdasd\"}]', 1, '/static/media/test_1.png'),
(2, 'iiiiiiiiii', '[{\"code\":\"1\",\"nombre\":\"a\",\"description\":\"asdasd\"},{\"code\":\"2\",\"nombre\":\"b\",\"description\":\"adasasd\"}]', 1, '/static/media/test_2.gif'),
(3, 'ooooooooooo', '[{\"code\":\"1\",\"nombre\":\"a\",\"description\":\"asdasd\"},{\"code\":\"2\",\"nombre\":\"b\",\"description\":\"adasasd\"},{\"code\":\"3\",\"nombre\":\"c\",\"description\":\"asdasdasd\"}]', 1, ''),
(4, 'ooooooooooo', '[{\"code\":\"1\",\"nombre\":\"a\",\"description\":\"asdasd\"},{\"code\":\"2\",\"nombre\":\"b\",\"description\":\"adasasd\"},{\"code\":\"3\",\"nombre\":\"c\",\"description\":\"asdasdasd\"}]', 1, ''),
(5, 'ooooooooooo', '[{\"code\":\"1\",\"nombre\":\"a\",\"description\":\"asdasd\"},{\"code\":\"2\",\"nombre\":\"b\",\"description\":\"adasasd\"},{\"code\":\"3\",\"nombre\":\"c\",\"description\":\"asdasdasd\"}]', 1, ''),
(6, '', '[]', 0, ''),
(7, 'tttttt', '[{\"code\":\"1\",\"nombre\":\"222\",\"description\":\"2222\"}]', 1, '/static/media/test_7.png'),
(8, '', '[]', 0, '/static/media/test_8.png'),
(9, '', '[]', 0, '/static/media/test_9.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test_alumno`
--

CREATE TABLE `test_alumno` (
  `idtestalumno` bigint(20) NOT NULL,
  `idalumno` bigint(20) NOT NULL,
  `idtest` int(11) NOT NULL,
  `idtestcriterio` bigint(20) NOT NULL,
  `idtestasigancion` int(11) NOT NULL,
  `puntaje` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `test_alumno`
--

INSERT INTO `test_alumno` (`idtestalumno`, `idalumno`, `idtest`, `idtestcriterio`, `idtestasigancion`, `puntaje`) VALUES
(1, 1, 2, 1, 1, 'b');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test_asignacion`
--

CREATE TABLE `test_asignacion` (
  `idtestasigancion` int(11) NOT NULL,
  `idtest` int(11) NOT NULL,
  `idcurso` int(11) NOT NULL,
  `idrecurso` int(11) NOT NULL,
  `situacion` int(11) NOT NULL COMMENT '0 al inicio,1 siempre,2 al final, '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `test_asignacion`
--

INSERT INTO `test_asignacion` (`idtestasigancion`, `idtest`, `idcurso`, `idrecurso`, `situacion`) VALUES
(1, 2, 31, 881, 1),
(2, 1, 31, 797, 1),
(3, 1, 31, 804, 1),
(4, 1, 31, 878, 1),
(5, 9, 31, 878, 1),
(6, 9, 35, 887, 1),
(7, 7, 31, 797, 1),
(8, 7, 31, 804, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test_criterios`
--

CREATE TABLE `test_criterios` (
  `idtestcriterio` bigint(20) NOT NULL,
  `idtest` int(11) NOT NULL,
  `criterio` varchar(300) NOT NULL,
  `mostrar` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `test_criterios`
--

INSERT INTO `test_criterios` (`idtestcriterio`, `idtest`, `criterio`, `mostrar`) VALUES
(1, 2, 'hola', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`idtest`);

--
-- Indices de la tabla `test_alumno`
--
ALTER TABLE `test_alumno`
  ADD PRIMARY KEY (`idtestalumno`);

--
-- Indices de la tabla `test_asignacion`
--
ALTER TABLE `test_asignacion`
  ADD PRIMARY KEY (`idtestasigancion`);

--
-- Indices de la tabla `test_criterios`
--
ALTER TABLE `test_criterios`
  ADD PRIMARY KEY (`idtestcriterio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `test`
--
ALTER TABLE `test`
  MODIFY `idtest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `test_criterios`
--
ALTER TABLE `test_criterios`
  MODIFY `idtestcriterio` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
